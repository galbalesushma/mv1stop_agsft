import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { SearchService } from 'app/service/search.service';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SubscriptionService } from '../../service/subscription.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthUserService } from '../../service/auth-user.service';
import { SaveSearchList } from 'app/shared/model/subscription.model';
import { carrier } from '../../shared/interceptor/service-url';
@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {
  carrier_id: any;
  public checkPrimaryCompany: boolean = false;
  public checkIsCarrier: boolean = false;
  public comapareBuyZip: boolean;
  // public isModalOpen = "true";
  // public isDisclaimerOpen = "true";
  public buyZip;
  public packageId;
  public agenData;
  public searchList = [];
  public isSubmit = true;
  public currentPage = 1;
  public totalElements = 0;
  public maxSize = 3;
  public pageSize = 10;
  public loader = false;

  public totalPages;
  public callingFlag = false;
  public epbaxNo;
  public phoneNo: any;
  public searchType = '';
  public dynamicHeight;
  public loaderFlag = false;
  public isLoggedIn = false;
  public loginUser;
  public currentDate;
  public emptyLabel = " "
  public currentUserType;
  public langFlag = true;
  public notifyFlag = false;
  public totalRecords = 0;
  public backFlag = 'true'
  public OopsFlag = false;
  public selectedFlag = false;
  public selectedFlag1 = false;
  public selectedFlag2 = false;
  public selectedFlag3 = false;
  public selectedFlag4 = false;
  public homeAddress = '';
  public emailAddress = '';
  public phoneText;
  public lpaAddress;
  public dmvAddress;
  public lpaValue = null;
  public dmvValue = null;
  dynamicWidth;
  show = 1;
  showAgentListFlag = true;
  public isCollapsed = true;
  public isCollapsed1 = true;
  public dataArray = [

  ];
  public dataArray1 = [
  ]
  public saveSearchList = new SaveSearchList();
  public searchId;
  dropdownList = [];
  selectedItems = [];
  carriers = [];
  selectedId;
  selectedCarrier = [];
  array1 = [];
  firstFlag = false;
  secondFlag = false;
  thirdFlag = false;
  thankyouFlag = false;
  customerServiceFlag = false;


  @ViewChild('prompt') prompt: NgbModal;
  // @ViewChild('promptToCheckPage') promptToCheckPage: NgbModal;
  @ViewChild('alertmodal') alertmodal: NgbModal
  @ViewChild('box1') box1: ElementRef;
  // @ViewChild('closeButton') closeButton: ElementRef;
  listOb: any;
  ind: any;
  selectedOption: string;
  selectedValue: string;
  selectedLpaValue: any;
  selectedDmvValue: any;
  selectedLocationValue: any
  public logoImage = 'assets/logo/logo.ico';
  public checkPackageId:boolean=false;


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.dynamicHeight = window.innerHeight;
    this.dynamicWidth = window.innerWidth;
  }

  constructor(
    public el: ElementRef,
    public router: Router,
    public searchService: SearchService,
    public fbBuilder: FormBuilder,
    public toastr: ToastrService,
    public subscriptionService: SubscriptionService,
    private modalService: NgbModal,
    public translate: TranslateService,
    public authUserService: AuthUserService
  ) {
    if (localStorage.getItem('token')) {
      this.isLoggedIn = true;
      this.loginUser = localStorage.getItem('firstName');
    } else {
      this.isLoggedIn = false;
    }
  }

  ngOnInit() {
    this.getListOfCarrier();
    localStorage.removeItem('backFlag');
    // localStorage.removeItem('isModalOpen');

    // localStorage.setItem('isModalOpen', this.isModalOpen);
    // localStorage.setItem('isDisclaimerOpen', this.isDisclaimerOpen);
    this.currentUserType = localStorage.getItem('type')
    this.currentDate = new Date();
    this.dynamicHeight = window.innerHeight;
    this.dynamicWidth = window.innerWidth
    this.agenData = JSON.parse(localStorage.getItem('agentString'));
    if (this.agenData.checked == true) {
      this.firstFlag = true;
    }
    else if (this.agenData.companyName != '') {
      this.secondFlag = true
    }
    else if (this.agenData.agentName != '') {
      this.thirdFlag = true;
    }

    if (this.agenData == null) {
      this.router.navigate([''])
    }
    if (this.agenData != null && this.agenData != '') {
      this.phoneNo = this.agenData.phone;
      this.searchType = this.agenData.type;


    }

    if(this.agenData.type == 'AGENT'){
      setTimeout(() => {
         this.alert();
      }, 200)
    }
    else if(this.agenData.type == "FACILITY"){
      this.search();
    }
    this.getLang();
  }


  search() {

    this.loader = true
    this.isSubmit = true;
    this.OopsFlag = false;
    this.searchService.searchList(this.agenData, this.currentPage).subscribe(
      data => {

        this.carriers = [];
        let getZip = JSON.parse(localStorage.getItem('agentString'));

        for (let key in data.result) {
          this.carriers = data.result[key].carriers;
          this.packageId = data.result[key].packageId;
          this.buyZip = data.result[key].buyZip;



            if((this.agenData.companyName != data.result[key].insurance_company)){
              // console.log(data.result[key].extension);
              if(this.agenData.type == 'AGENT' && this.agenData.companyName){
                this.checkPrimaryCompany = true;
              }
            }


          for(let i in this.carriers){
            let k = this.carriers[i];
            for(let j in  this.dropdownList){
              if(k === this.dropdownList[j].id){
               this.carrier_id = this.dropdownList[j].carrier;
               if((this.carrier_id == this.agenData.companyName) && (this.agenData.type == 'AGENT' && this.agenData.companyName) ){
                 this.carriers[i] = null;
               }
              }
            }
          }


          // if(data.result[key].buyZip !== getZip.zip){
          //   this.comapareBuyZip = true;
          // }

          // if(data.result[key].packageId === undefined){
          //   this.checkPackageId =true;
          // }

        }

        window.scrollTo(0, 0)

        // if (this.agenData.type == 'AGENT'  ){
        //   // console.log("company");

        //    if((this.checkPackageId || this.comapareBuyZip) && localStorage.getItem('isModalOpen') === "true"){
        //   localStorage.setItem('isModalOpen','false')
        //   this.openModalToCall();
        // }
        // }



        let total = 40
        this.loader = false;
        this.totalRecords = data.total;


        if (this.totalRecords == 0) {
          this.OopsFlag = true;
        }

        switch (this.currentPage) {
          case 1:
            this.totalElements = 30;
            if (data.totalPages == 1) {
              this.totalElements = 10;
            }
            break;
          case 2:
            this.totalElements = 50;
            if (data.totalPages == 2) {
              this.totalElements = 30;
            }
            break;
          case 3:
            this.totalElements = 20 + total + data.total;
            break;
          default:
            this.totalElements = (total + data.total)-total + 20 ;
        }
        if (data.result == '') {
          let page = this.currentPage - 1
          if (page != 0 && page <= 0) {
            this.pageChanged(page)
          }
        }

        if (this.agenData.type == 'INSURANCE') {
          this.totalElements = data.total;
          this.maxSize = (data.total / 20)
        }
        if (this.agenData.type == 'FACILITY') {
          this.totalElements = data.total;
          this.maxSize = (data.total / 20)
        }

        this.searchList = data['result'];

        for (let key in this.searchList) {
          this.searchList[key]['carrierList'] = this.dropdownList

        }
        this.searchId = data.searchId


        this.isSubmit = false;
        if (this.searchList == null) {
          this.isSubmit = false;
        }
      },
      error => {
        this.OopsFlag = true;
        this.isSubmit = false;
        this.loader = false;

      }
    );
  }

  customerServiceList(){
    this.loader = true
    this.isSubmit = true;
    this.OopsFlag = false;

    this.searchService.customerService(this.agenData, this.currentPage).subscribe(
      data => {
        window.scrollTo(0, 0);


        let total = 40
        this.loader = false;
        this.totalRecords = data.total;




        if (this.totalRecords == 0) {
          this.OopsFlag = true;
        }

        switch (this.currentPage) {
          case 1:
            this.totalElements = 30;
            if (data.totalPages == 1) {
              this.totalElements = 10;
            }
            break;
          case 2:
            this.totalElements = 50;
            if (data.totalPages == 2) {
              this.totalElements = 30;
            }
            break;
          case 3:
            this.totalElements =  data.total;
            break;
          default:
            this.totalElements = data.total ;
        }
        if (data.result == '') {
          let page = this.currentPage - 1
          if (page != 0 && page <= 0) {
            this.pageChanged(page)
          }
        }
        this.searchList = data['result'];

        for (let key in this.searchList) {
          this.searchList[key]['carrierList'] = this.dropdownList

        }
        this.searchId = data.searchId

        this.isSubmit = false;
        if (this.searchList == null) {
          this.isSubmit = false;
        }
      },
      error => {
        this.OopsFlag = true;
        this.isSubmit = false;
        this.loader = false;

      }
    );

  }


  displaySearchList(funcRef){
    setTimeout(() => {
      if (this.agenData.type == 'AGENT' || this.agenData.type == 'FACILITY') {
        this.search();
      }

    }, 500);

    funcRef('Cross click');
  }


  swap(theArray, indexA, indexB) {
    let temp = new Object(theArray[indexA]);
    theArray = theArray.splice(indexA, 1);
    this.searchList.push(temp);
  }

  // callNowGinAgent(){
  //       this.epbaxNo = "1001";
  //       this.callingFlag = true;
  //       this.notifyFlag = true;
  //       this.subscriptionService.callingService(this.epbaxNo, this.phoneNo).subscribe(
  //           data => {
  //             this.callingFlag = false;
  //             this.notifyFlag = false;
  //           },
  //           error => {
  //             this.callingFlag = false;
  //           }
  //       );

  // }

  callNow(index, listObj) {
    if (this.searchType == 'AGENT' && listObj.ce_compliance == "false") {
      this.ind = index;
      this.listOb = listObj;
      this.openModal();
    }
    else {
      this.swap(this.searchList, index, this.searchList.length - 1);
      if (this.searchType == 'AGENT') {
        this.epbaxNo = listObj.extension;
        let npn = listObj.npn;
        this.saveAgentDetails(this.epbaxNo, npn);
        this.callToAgent(listObj);

      } else {
        this.epbaxNo = listObj.extension;
        this.callToAgent(listObj);
      }
    }

  }
  confirmCall() {
    let element = document.getElementById('closeButton') as HTMLElement;

    element.click();

    this.swap(this.searchList, this.ind, this.searchList.length - 1);
    this.epbaxNo = this.listOb.extension;

    let npn = this.listOb.npn;
    // this.callNowGinAgent();
    this.saveAgentDetails(this.epbaxNo, npn);
    this.callToAgent(this.listOb);
  }

  insurancePageChanged(event) {
    this.pageSize = 20;
    this.currentPage = event;
    this.search();
  }

  pageChanged(event) {
    // window.scrollTo(0, 0)
    this.currentPage = event;

    switch (this.currentPage) {
      case 1:
        this.pageSize = 10;
        break;
      case 2:
        this.pageSize = 20;
        break;
      case 3:
        this.pageSize = 20;
        break;
      default:
        this.pageSize = 20;
        break;
    }
    // window.scrollTo(0, 0)



    if( this.customerServiceFlag){
        this.customerServiceList();
    }else{
        this.search();
    }





  }

  showNotification(statusCode, message) {
    switch (statusCode) {
      case 200:
        this.showSucccess(message);
        break;

      default:
        this.showError(message);
        break;
    }
  }
  showSucccess(message) {
    this.toastr.success(message);
  }
  showError(message) {
    this.toastr.error(message);
  }
  showWarning() {
    this.toastr.warning('You are being warned.', 'Alert!');
  }

  showInfo() {
    this.toastr.info('Just some information for you.');
  }
  callToAgent(listObj) {
    this.callingFlag = true;
    this.notifyFlag = true;
    this.subscriptionService.callingService(this.epbaxNo, this.phoneNo).subscribe(
      data => {
        this.callingFlag = false;
        this.notifyFlag = false;

      },
      error => {
        this.callingFlag = false;
      }
    );
  }
  hideNotification() {
    this.notifyFlag = false;
  }

  navigateProfile() {
    switch (this.currentUserType) {
      case 'AGENT':
        if (this.currentUserType == 'AGENT') {
          this.router.navigate(['/agent-profile/viewProfile']);
        }
        break;
      case 'FACILITY':
        if (this.currentUserType == 'FACILITY') {
          this.router.navigate(['/facility-profile/viewFacility']);
        }

        break;
      case 'ADMIN':
        if (this.currentUserType == 'ADMIN') {
          this.router.navigate(['/user/agent/listAgent']);
        }

        break;
      default:
        this.router.navigate(['/home']);
        break;
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/home']);
  }

  switchLanguage(lang) {
    localStorage.setItem('lang', lang)
    this.translate.use(lang);
    if (lang == 'en') {
      this.langFlag = true;
    }
    else if (lang == 'es') {
      this.langFlag = false;
    }
  }
  getLang() {
    let lang = localStorage.getItem('lang')
    switch (lang) {
      case 'en':
        this.langFlag = true;
        this.translate.use(lang);
        break;
      case 'es':
        this.langFlag = false;
        this.translate.use(lang);
        break;

      default:
        break;
    }
  }
  goBack() {
    let type = this.agenData.type;
    localStorage.setItem('backFlag', this.backFlag)
    switch (type) {
      case 'AGENT':
        this.router.navigate(['/home']);
        break;
      case 'FACILITY':
        this.router.navigate(['/home']);
        break;
      case 'INSURANCE':
        this.router.navigate(['/home']);
        break;
      default:
        this.router.navigate(['/home'])
        break;
    }
  }
  confirmSelection(funcRef) {
    if(this.thirdFlag && !this.secondFlag){
        this.thankyouFlag = true;
    }else{
      funcRef('Cross click');
      this.customerServiceFlag = true;
      this.customerServiceList();
    }
  }
  closeThankYou(funcRef){
    funcRef('Cross click');
    this.goBack();
  }
  terms() {
    this.router.navigate(['terms-conditions'])
  }
  selectProof(type) {
    switch (type) {
      case 'HOME':
        this.el.nativeElement.focus();
        this.selectedFlag = true;
        this.selectedFlag1 = false
        this.selectedFlag2 = false
        this.selectedFlag3 = false;
        this.selectedFlag4 = false;
        this.saveSearchList = new SaveSearchList();

        break;
      case 'PHONE':
        this.selectedFlag = false
        this.selectedFlag1 = true
        this.selectedFlag2 = false
        this.selectedFlag3 = false;
        this.selectedFlag4 = false;
        this.saveSearchList = new SaveSearchList()
        break;
      case 'LPA':
        this.selectedFlag = false
        this.selectedFlag1 = false
        this.selectedFlag2 = true
        this.selectedFlag3 = false;
        this.selectedFlag4 = false;
        this.saveSearchList = new SaveSearchList()

        break;
      case 'DMV':
        this.selectedFlag3 = true;
        this.selectedFlag = false
        this.selectedFlag1 = false
        this.selectedFlag2 = false
        this.selectedFlag4 = false;
        this.saveSearchList = new SaveSearchList()
        break;
      case 'VEH':
        this.selectedFlag3 = false;
        this.selectedFlag4 = true;
        this.selectedFlag = false
        this.selectedFlag1 = false
        this.selectedFlag2 = false
        this.saveSearchList = new SaveSearchList()
        break;
      default:
        break;
    }
  }
  getLpa() {
    this.loader = true
    this.authUserService.getLpA(this.lpaAddress).subscribe(
      data => {
        this.dataArray = data.value;
        this.collapseAll(0, true);
        if (this.dataArray && this.dataArray.length > 0) {
          this.dataArray[0].isCollapsed = true;
        }


        this.loader = false
      },
      error => {

        this.loader = false

      }
    )
  }
  getDMv() {
    this.loader = true
    this.authUserService.getDMV(this.dmvAddress).subscribe(
      data => {
        this.dataArray1 = data.value;
        this.collapseAllDrivers(0, true);
        if (this.dataArray1 && this.dataArray1.length > 0) {
          this.dataArray1[0].isCollapsed1 = true;
        }
        this.loader = false

      },
      error => {

        this.loader = false
      }
    )
  }
  selectLpa(value) {
    this.lpaValue = value
  }
  selectDmv(value) {
    this.dmvValue = value
  }

  saveInsuranceSent() {

    this.saveSearchList.agentId = localStorage.getItem('agentId');
    this.saveSearchList.searchLogId = this.searchId;
    this.saveSearchList.home = this.saveSearchList.home;
    this.saveSearchList.phone = this.saveSearchList.phone;
    this.saveSearchList.dmvId = this.dmvValue
    this.saveSearchList.lpaId = this.lpaValue
    this.saveSearchList.profileId = localStorage.getItem('compId');
    this.showAgentListFlag = false;

    this.selectedOption = "";
    this.selectedValue = "";
    if (this.selectedFlag) {
      this.selectedOption = "Email Address "
      this.selectedValue = this.saveSearchList.home;
    }
    if (this.selectedFlag1) {
      this.selectedOption = "Sent as text to your phone: "
      this.selectedValue = this.saveSearchList.phone;
    }
    if (this.selectedFlag2) {
      this.selectedOption = "Licence Plate Agency "
      this.selectedValue = this.selectedLpaValue;
    }
    if (this.selectedFlag3) {
      this.selectedOption = "Driver's Licence Office"
      this.selectedValue = this.selectedDmvValue;
    }
    if (this.selectedFlag4) {
      this.selectedOption = "Vehicle Sales Location"
      this.selectedValue = this.saveSearchList.location;
    }



    localStorage.setItem("selectedOption", this.selectedOption);
    localStorage.setItem("selectedValue", this.selectedValue);
    localStorage.setItem('callType', 'agent');


    this.search();
    this.authUserService.saveInsurance(this.saveSearchList).subscribe(
      data => {

      },
      error => {

      }
    )
  }

  openModal() {
    this.modalService.open(this.prompt, { size: 'lg' });
  }


// open modal on close disclaimer modal
  // onCloseDisclaimer(){
  //   localStorage.setItem('isDisclaimerOpen','false')
  //   setTimeout(() => {
  //     this.openModalToCheckPage();

  //   }, 100)
  // }

  // show modal if page1 and page2 are not present
  // openModalToCheckPage(){
  //   let getZip = JSON.parse(localStorage.getItem('agentString'));

  //   if(((this.checkPackageId) || (this.comapareBuyZip)) &&  this.searchList.length != 0){
  //     this.modalService.open(this.promptToCheckPage, { size: 'lg' });

  //   }

  // }



  // show modal if page1 and page2 are present
  // openModalToCall(){
  //      if( localStorage.getItem('isDisclaimerOpen') === 'false'){


  //       this.modalService.open(this.promptToCheckPage, { size: 'lg' });

  //     }

  // }

  collapseAll(index, obj) {

    for (let i in this.dataArray) {
      if (this.dataArray != null && this.dataArray.length > 0) {
        let ind = parseInt(i);
        if (ind != index) {
          this.dataArray[ind].isCollapsed = true;
        } else {
          this.dataArray[ind].isCollapsed = !(obj.isCollapsed);
          if (!this.dataArray[ind].isCollapsed) {
            this.selectLpa(obj.id);
            this.selectedLpaValue = obj;

          }
        }
      }

    }
  }

  collapseAllDrivers(index, obj) {

    for (let i in this.dataArray1) {
      if (this.dataArray1 != null && this.dataArray1.length > 0) {
        let ind = parseInt(i);
        if (ind != index) {
          this.dataArray1[ind].isCollapsed1 = true;
        } else {
          this.dataArray1[ind].isCollapsed1 = !(obj.isCollapsed1);
          if (!this.dataArray1[ind].isCollapsed1) {
            this.selectDmv(obj.id);
            this.selectedDmvValue = obj;
          }
        }
      }

    }
  }

  saveAgentDetails(agentNumber, npn) {
    let agentDetails = JSON.parse(localStorage.getItem('agentString'));
    if (agentDetails.hasOwnProperty('type') && agentDetails['type'].toLowerCase() == 'agent') {
      let data = {
        "userNumber": agentDetails.phone,
        "agentNumber": agentNumber,
        "type": 'agent',
        "userName": agentDetails.name,
        "callDate": new Date(),
        "npn": npn
      }
      this.subscriptionService.saveAgentCallLog(data).subscribe(
        data => {

        },
        error => {
          this.callingFlag = false;
        }
      );
    }

  }
  getListOfCarrier() {
    this.authUserService.getCarrierList().subscribe(
      data => {


        if (data.statusCode == 200) {
          this.dropdownList = data.value;

        }
        else {

        }
      },
      error => {
      }
    )
  }

  alert() {
    this.modalService.open(this.alertmodal, { size: 'lg' });
  }


}
