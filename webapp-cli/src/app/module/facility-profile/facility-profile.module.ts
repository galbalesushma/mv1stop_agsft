import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacilityProfileRoutingModule } from './facility-profile-routing.module';
import { FacilityProfileComponent } from './facility-profile.component';

@NgModule({
    imports: [CommonModule, FacilityProfileRoutingModule],
    declarations: [FacilityProfileComponent]
})
export class FacilityProfileModule {}
