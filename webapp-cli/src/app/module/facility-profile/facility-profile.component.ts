import { Component, OnInit } from '@angular/core';
import { AuthUserService } from 'app/service/auth-user.service';

@Component({
    selector: 'app-facility',
    templateUrl: './facility-profile.component.html',
    styleUrls: ['./facility-profile.component.css']
})
export class FacilityProfileComponent implements OnInit {
    constructor(private loginService: AuthUserService) {}

    ngOnInit() {
        this.getProfileInfo();
    }

    getProfileInfo() {
        const userName = localStorage.getItem('userName');
        this.loginService.getProfile(userName).subscribe(
            data => {
               

                if (data['statusCode'] == 200) {
                    let value = data['value'];
                    localStorage.setItem('firstName', value['firstName']);
                    localStorage.setItem('profileId', value['profileId']);
                    localStorage.setItem('firstName', value['firstName']);
                    localStorage.setItem('type', value['type']);
                    localStorage.setItem('userId', value['userId']);
                }
            },
            error => {
                
            }
        );
    }
}
