import { Component, OnInit, ViewChild } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DropdownSettingModel } from 'app/shared/model/dropdown.model';
import { UploadFacilityService } from 'app/service/upload-facility.service';
import { SubscriptionService } from 'app/service/subscription.service';


@Component({
  selector: 'app-view-facility-profile',
  templateUrl: './view-facility-profile.component.html',
  styleUrls: ['./view-facility-profile.component.css'],
  providers: [ToastrService]
})
export class ViewFacilityProfileComponent implements OnInit {
  searchText;
  facilityDataList = [];
  facilityDataObj = {};

  stateList = [];

  public uploadFacilityForm: FormGroup;
  registerForm: FormGroup;
  public dropdownSettingState: DropdownSettingModel;
  public dropdownSettingCity: DropdownSettingModel;
  public dropdownSettingZip: DropdownSettingModel;
  public dropdownSettingLStatus: DropdownSettingModel;
  public dropdownSettingInc: DropdownSettingModel;
  public dropdownSettingCertified: DropdownSettingModel;
  public dropdownSettingPCertified: DropdownSettingModel;
  public dropdownSettingCompany: DropdownSettingModel;

  public dropdownList = [];
  public selectedItems;
  public formSubmmit = false;
  page = 1;
  city = '';
  zip = ''
  incStatus = '';
  incAB = '';
  fName = '';
  lName = '';
  isn = '';
  editProfileId;
  subscribeData=[];
  prefPhone:'';
  prefCompany:'';
  prefMobile:'';
  comments:'';
  speaks_spanish:'';
  mechDuty:'';
  public sumbitFacilityData = {
    isn:'',
    phone:'',
    weekDayHours:'',
    saturdayHours:'',
    sundayHours:'',
    city:'',
    state:'',
    county:'',
    zipCode:'',
    name:'',
    address:'',
    bPhone:'',
    bEmail:'',
    insp_type:'',
    mech_duty:'',
    mobile:'',
    prefEmail: '',
  };
  @ViewChild('content') content: NgbModal;
  constructor(
    public uploadFacilityService: UploadFacilityService,
    public fbuilder: FormBuilder,
    public toastr: ToastrService,
    private modalService: NgbModal,
    public subscriptionService: SubscriptionService

  ) {
    this.initFormData();
  }

  onItemSelect(item: any, x) {
   
  }
  OnItemDeSelect(item: any, x) {
    
  }
  onSelectAll(items: any, x) {
   
  }
  onDeSelectAll(items: any, x) {
   
  }

  ngOnInit() {
    let profileId = localStorage.getItem('profileId');
    this.editFacility(profileId);
  }
  getFacilityList() {
    let data = [];
    this.uploadFacilityService.getFacilityList(data, this.page, this.isn, this.city, this.zip, this.incStatus, this.incAB, this.fName, this.lName,"").subscribe(
      data => {
       
        if (data) {
          this.facilityDataObj = data;
          this.facilityDataList = this.facilityDataObj['result'];
        } else {
          this.facilityDataList = [];
        }
      },
      error => { }
    );
  }

  showNotification(statusCode, message) {
  
    switch (statusCode) {
      case 200:
        this.showSucccess(message);
       
        break;

      default:
        this.showError(message);
        break;
    }
  }
  showSucccess(message) {
    this.toastr.success(message);
  }
  showError(message) {
    this.toastr.error(message);
  }
  showWarning() {
    this.toastr.warning('You are being warned.', 'Alert!');
  }

  editFacility(id) {
 
    this.uploadFacilityService.getFacilityById(id).subscribe(
      data => {
       
        this.facilityDataList = data;
        this.sumbitFacilityData =data;
        this.prefPhone = data.prefPhone;
        this.prefMobile = data.prefMobile;
        this.speaks_spanish = data.speaks_spanish;
        this.prefCompany = data.prefCompany;
        this.comments = data.comments;
        this.mechDuty = data.mech_duty;
        this.editProfileId = id;
        this.getSubsription(id);
      },
      error => {
        if (error) {
         
          this.showError(error.message);
        }
      }
    );
  }

  submitFacility(language) {
    let lang;
    let langKey;
    if (language == 'false') {
      lang = "US";
      langKey = 'en';
    }
    else if (language == 'true') {
      lang = "ES";
      langKey = 'es';
    }

    const data = {
      "preferredMobile": this.prefMobile,
      "phone": this.prefPhone,
      "langKey": langKey,
      "lang": lang,
      "prefCompany": this.prefCompany,
      "comments": this.comments,
      "mechDuty":this.mechDuty
    }


    this.uploadFacilityService.saveFacility(data, this.editProfileId)
      .subscribe(data => {
        if (data.statusCode == 200) {
          this.showNotification(200, data.message)
        }
        else if (data.statusCode == 400) {
          this.showError(data.message);
        }
      }, error => {
        this.showError(error.message);
      })
  }

  getSubsription(profileId) {
    this.subscriptionService.getPackSubscription(profileId).subscribe(
      data => {
        this.subscribeData = [];
        this.subscribeData = data;
        if (this.subscribeData.length == 0) {
          // this.noFlag = true;
        }

        
      },
      error => {
        // this.noFlag = false;
      }
    );
  }

  deleteFacility(npn) {
    this.uploadFacilityService.deleteFacilityById(parseInt(npn, 10)).subscribe(
      data => {
       
        this.showNotification(data.statusCode, data.message);
        if (data.statusCode == 200) {
        }
      },
      error => {
       
        this.showError(error.message);
      }
    );
  }

  open2(content) {
   
    this.modalService.open(content, { size: 'lg', backdrop: 'static' }).result.then(
      result => { 
      },
      reason => {
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public updateForm(dataObj) {
    Object.keys(dataObj).forEach(dataKey => {
      if (this.uploadFacilityForm.controls[dataKey]) {
        this.uploadFacilityForm.controls[dataKey].setValue(dataObj[dataKey]);
      }
    });
  }
  initFormData() {
    this.uploadFacilityForm = this.fbuilder.group({
      name: [null, [Validators.required]],
      address: ['', [Validators.required]],
      city: ['', [Validators.required]],
      zipCode: ['', [Validators.required]],
      weekDayHours: ['', [Validators.required]],
      saturdayHours: ['', [Validators.required]],
      sundayHours: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      isn: ['', [Validators.required]],
      county: ['', [Validators.required]],
      state: ['', [Validators.required]],
      bPhone: ['', [Validators.required]],
      bEmail: ['', [Validators.required]],
      insp_type: ['', [Validators.required]],
      mech_duty: ['', [Validators.required]]
    });
  }

  showFormError() {
    this.showError('Please Fill all the required field Before submitting');
  }
  facilityFormSubmit(formData) {
   
    this.formSubmmit = true;
    const facilityData = this.uploadFacilityForm.value;

    if (this.uploadFacilityForm.invalid) {
      this.showError('Please fill all mandatory values.');
    } else {
      facilityData.name = this.uploadFacilityForm.value['name'];
      facilityData.address = this.uploadFacilityForm.value['address'];
      facilityData.city = this.uploadFacilityForm.value['city'];
      facilityData.zipCode = this.uploadFacilityForm.value['zipCode'];
      facilityData.weekDayHours = this.uploadFacilityForm.value['weekDayHours'];
      facilityData.saturdayHours = this.uploadFacilityForm.value['saturdayHours'];
      facilityData.sundayHours = this.uploadFacilityForm.value['sundayHours'];
      facilityData.phone = this.uploadFacilityForm.value['phone'];

      this.uploadFacilityService.addFacility(facilityData).subscribe(
        data => {
         
          this.showNotification(data.statusCode, data.message);
        },
        error => {
         
          this.showError(error.message);
        }
      );
    }
  }
}
