import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewFacilityProfileComponent } from './view-facility-profile.component';

const routes: Routes = [
    {
        path: '',
        component: ViewFacilityProfileComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ViewFacilityProfileRoutingModule {}
