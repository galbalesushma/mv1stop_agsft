import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewFacilityProfileRoutingModule } from './view-facility-profile-routing.module';
import { ViewFacilityProfileComponent } from './view-facility-profile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SearchFilterPipeModule } from 'app/shared/pipes/searchFilter/searchFilter.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchModule } from 'app/shared/search/search.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

@NgModule({
    imports: [
        CommonModule,
        ViewFacilityProfileRoutingModule,
        ReactiveFormsModule,
        NgbModule,
        // ToastrModule.forRoot(),
        SearchFilterPipeModule,
        FormsModule,
        SearchModule,
        AngularMultiSelectModule
    ],
    declarations: [ViewFacilityProfileComponent]
})
export class ViewFacilityProfileModule {}
