import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FacilityProfileComponent } from './facility-profile.component';

const routes: Routes = [
    {
        path: '',
        component: FacilityProfileComponent,
        children: [
            { path: '', redirectTo: 'viewFacility', pathMatch: 'full' },
            {
                path: 'viewFacility',
                loadChildren: './view-facility-profile/view-facility-profile.module#ViewFacilityProfileModule'
            }
            //   {
            //     path: 'subscription',
            //     loadChildren: '../subscription/subscription.module#SubscriptionModule'
            //     // loadChildren: '../subscription/subscription.module#SubscriptionModule'
            // }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FacilityProfileRoutingModule {}
