import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModifyFacilityComponent } from './modify-facility.component';

const routes: Routes = [
    {
        path: '',
        component: ModifyFacilityComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ModifyFacilityRoutingModule {}
