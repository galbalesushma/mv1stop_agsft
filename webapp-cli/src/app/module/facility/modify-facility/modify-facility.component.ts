import { Component, OnInit, HostListener } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpEventType } from '@angular/common/http';
import { UploadFacilityService } from 'app/service/upload-facility.service';

@Component({
    selector: 'app-modify-facility',
    templateUrl: './modify-facility.component.html',
    styleUrls: ['./modify-facility.component.css'],
    providers: [UploadFacilityService, ToastrService]
})
export class ModifyFacilityComponent implements OnInit {
    dynamicHeight;

    public fileSelected: File;

    public fileUploadProgress = 0;

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.dynamicHeight = window.innerHeight;
    }
    constructor(private uploadFacilityService: UploadFacilityService, public toastr: ToastrService) {}

    ngOnInit() {
        this.dynamicHeight = window.innerHeight;
    }

    selectFile(fileInput, inputInstance: HTMLInputElement) {
        this.fileUploadProgress = 0;
        const files = fileInput.target.files;
        if (files !== undefined && files.length > 0) {
            this.fileSelected = files[0];

            this.uploadCSV();
            inputInstance.value = '';
        }
    }

    uploadCSV() {
        if (this.fileSelected !== undefined) {
            const uploadForm: FormData = new FormData();
            uploadForm.append('file', this.fileSelected, this.fileSelected.name);
            this.uploadFacilityService.uploadCSV(uploadForm).subscribe(event => {
              
                if (event) {
                    switch (event.type) {
                        case HttpEventType.DownloadProgress:
                           
                            break;
                        case HttpEventType.UploadProgress:
                            const progress = Math.round(100 * event.loaded / event.total);
                            if (progress < 90) {
                                this.fileUploadProgress = progress;
                            } else if (progress >= 90) {
                                this.fileUploadProgress = 90;
                            }
                            break;
                        case HttpEventType.Response:
                          

                            this.showNotification(event.body.statusCode, event.body.message);
                            this.fileUploadProgress = 100;

                            break;
                        default:
                            break;
                    }
                }
            });
        }
    }

    showNotification(statusCode, message) {
       
        switch (statusCode) {
            case 200:
                this.toastr.success(message);
               
                break;

            default:
                this.toastr.error(message);
                break;
        }
    }

    showWarning(message) {
        this.toastr.warning(message);
    }

    showInfo(message) {
        this.toastr.info(message);
    }
}
