import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ModifyFacilityRoutingModule } from './modify-facility-routing.module';
import { ModifyFacilityComponent } from './modify-facility.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    imports: [
        CommonModule,
        ModifyFacilityRoutingModule,
        ReactiveFormsModule,
        NgbModule
        // ToastrModule.forRoot()
    ],
    declarations: [ModifyFacilityComponent]
})
export class ModifyFacilityModule {}
