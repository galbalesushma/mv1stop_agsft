import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UploadFacilityRoutingModule } from './upload-facility-routing.module';
import { UploadFacilityComponent } from './upload-facility.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbProgressbar, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    imports: [
        CommonModule,
        UploadFacilityRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        NgbModule
        // ToastrModule.forRoot()
    ],
    declarations: [UploadFacilityComponent]
})
export class UploadFacilityModule {}
