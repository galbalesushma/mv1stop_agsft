import { Component, OnInit, HostListener } from '@angular/core';
import { HttpEventType, HttpResponse, HttpRequest, HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UploadFacilityService } from '../../../service/upload-facility.service';
@Component({
    selector: 'app-upload-facility',
    templateUrl: './upload-facility.component.html',
    styleUrls: ['./upload-facility.component.css'],
    providers: [ToastrService]
})
export class UploadFacilityComponent implements OnInit {
    dynamicHeight;
    public formSubmmit = false;
    public fileSelected: File;

    public fileUploadProgress = 0;

    public uploadFacilityForm: FormGroup;
    registerForm: FormGroup;

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.dynamicHeight = window.innerHeight;
    }
    constructor(private uploadFacilityService: UploadFacilityService, public fbuilder: FormBuilder, public toastr: ToastrService) {
        this.initFormData();
    }

    ngOnInit() {
        this.dynamicHeight = window.innerHeight;
    }
    showFormError() {
        this.showError('Please Fill all the required field Before submitting');
    }
    facilityFormSubmit(formData) {
       
        this.formSubmmit = true;
        const facilityData = this.uploadFacilityForm.value;

        if (this.uploadFacilityForm.invalid) {
            this.showError('Please fill all mandatory values.');
        } else {
            facilityData.name = this.uploadFacilityForm.value['name'];
            facilityData.address = this.uploadFacilityForm.value['address'];
            facilityData.city = this.uploadFacilityForm.value['city'];
            facilityData.zipCode = this.uploadFacilityForm.value['zipCode'];
            facilityData.weekDayHours = this.uploadFacilityForm.value['weekDayHours'];
            facilityData.saturdayHours = this.uploadFacilityForm.value['saturdayHours'];
            facilityData.sundayHours = this.uploadFacilityForm.value['sundayHours'];
            facilityData.phone = this.uploadFacilityForm.value['phone'];

            this.uploadFacilityService.addFacility(facilityData).subscribe(
                data => {
                   
                    this.showNotification(data.statusCode, data.message);
                },
                error => {
                   
                    this.showError(error.message);
                }
            );
        }
    }
    initFormData() {
        this.uploadFacilityForm = this.fbuilder.group({
            name: [null, [Validators.required]],
            address: ['', [Validators.required]],
            city: ['', [Validators.required]],
            zipCode: ['', [Validators.required]],
            weekDayHours: ['', [Validators.required]],
            saturdayHours: ['', [Validators.required]],
            sundayHours: ['', [Validators.required]],
            phone: ['', [Validators.required]]
        });
    }

    selectFile(fileInput, inputInstance: HTMLInputElement) {
        this.fileUploadProgress = 0;
        const files = fileInput.target.files;
        if (files !== undefined && files.length > 0) {
            this.fileSelected = files[0];

            this.uploadCSV();
            inputInstance.value = '';
        }
    }

    uploadCSV() {
        if (this.fileSelected !== undefined) {
            const uploadForm: FormData = new FormData();
            uploadForm.append('file', this.fileSelected, this.fileSelected.name);
            this.uploadFacilityService.uploadCSV(uploadForm).subscribe(event => {
               
                if (event) {
                    switch (event.type) {
                        case HttpEventType.DownloadProgress:
                           
                            break;
                        case HttpEventType.UploadProgress:
                            const progress = Math.round(100 * event.loaded / event.total);
                            if (progress < 90) {
                                this.fileUploadProgress = progress;
                            } else if (progress >= 90) {
                                this.fileUploadProgress = 90;
                            }

                            
                            break;
                        case HttpEventType.Response:
                            

                            this.showNotification(event.body.statusCode, event.body.message);
                            this.fileUploadProgress = 100;

                            break;
                        default:
                            break;
                    }
                }
            });
        }
    }

    showNotification(statusCode, message) {
       
        switch (statusCode) {
            case 200:
                this.showSucccess(message);
               
                break;

            default:
                this.showError(message);
                break;
        }
    }
    showSucccess(message) {
        this.toastr.success(message);
    }
    showError(message) {
        this.toastr.error(message);
    }
    showWarning() {
        this.toastr.warning('You are being warned.', 'Alert!');
    }

    showInfo() {
        this.toastr.info('Just some information for you.');
    }
}
