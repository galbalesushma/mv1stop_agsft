import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadFacilityComponent } from './upload-facility.component';

const routes: Routes = [
    {
        path: '',
        component: UploadFacilityComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UploadFacilityRoutingModule {}
