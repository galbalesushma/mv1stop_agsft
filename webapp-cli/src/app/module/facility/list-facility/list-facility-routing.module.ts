import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListFacilityComponent } from './list-facility.component';

const routes: Routes = [
    {
        path: '',
        component: ListFacilityComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ListFacilityRoutingModule {}
