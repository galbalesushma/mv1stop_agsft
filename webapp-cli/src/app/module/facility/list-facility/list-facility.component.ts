import { Component, OnInit, ViewChild } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DropdownSettingModel } from 'app/shared/model/dropdown.model';
import { UploadFacilityService } from 'app/service/upload-facility.service';
import { FileDownloadUtility } from '../../../shared/interceptor/fileDownload';
import { SubscriptionService } from '../../../service/subscription.service';



@Component({
  selector: 'app-list-facility',
  templateUrl: './list-facility.component.html',
  styleUrls: ['./list-facility.component.css'],
  providers: [ToastrService, FileDownloadUtility]
})
export class ListFacilityComponent implements OnInit {
  searchText1 = "";
  facilityDataList = [];
  facilityDataObj = {};

  stateList = [
    {
      state_value: 'maharashtra',
      state_id: 1
    },
    {
      state_value: 'himachal',
      state_id: 6
    }
  ];

  public uploadFacilityForm: FormGroup;
  registerForm: FormGroup;
  public dropdownSettingState: DropdownSettingModel;
  public dropdownSettingCity: DropdownSettingModel;
  public dropdownSettingZip: DropdownSettingModel;
  public dropdownSettingLStatus: DropdownSettingModel;
  public dropdownSettingInc: DropdownSettingModel;
  public dropdownSettingCertified: DropdownSettingModel;
  public dropdownSettingPCertified: DropdownSettingModel;
  public dropdownSettingCompany: DropdownSettingModel;

  public dropdownList = [];
  agentDataObj = {};
  public selectedItems;
  public formSubmmit = false;
  public totalElements = 1;
  page = 1;
  fCity = '';
  fZipcode = '';
  fincStatus = '';
  fincAB = '';
  fName = '';
  lName = ''
  showFilterFlag = false;
  totalPages = 1;

  cityData;
  cityHide = false

  getZipData;
  zipHide = false;

  fnameData;
  fnamehide = false

  lnameData;
  lnamehide = false;
  isn = '';
  isnData;
  isnHide = false;
  sortData = [];
  public sortValue = '';
  public colname = '';
  isnFlag = false;
  stateData = [];
  public countyName = '';
  public countyHide = false;
  options;
  public sumbitFacilityData = {
    isn:'',
    phone:'',
    weekDayHours:'',
    saturdayHours:'',
    sundayHours:'',
    city:'',
    state:'',
    county:'',
    zipCode:'',
    name:'',
    address:'',
    bPhone:'',
    bEmail:'',
    insp_type:'',
    mech_duty:'',
    prefEmail: '',
  };
  prefPhone:'';
  prefMobile:'';
  speaks_spanish:'';
  prefCompany:'';
  comments:'';
  mechDuty:'';
  editProfileId;
  subscribeData=[];
  listLoader = false;
  mv1loader = false;
  modalLoader = false


  @ViewChild('content') content: NgbModal;
  constructor(
    public uploadFacilityService: UploadFacilityService,
    public fbuilder: FormBuilder,
    public toastr: ToastrService,
    private modalService: NgbModal,
    public fileDownloadUtility: FileDownloadUtility,
    public subscriptionService: SubscriptionService,

  ) {
    this.initFormData();
  }

  onItemSelect(item: any, x) {
   
  }
  OnItemDeSelect(item: any, x) {
   
  }
  onSelectAll(items: any, x) {
   
  }
  onDeSelectAll(items: any, x) {
   
  }

  ngOnInit() {
    this.getFacilityList();
    this.uploadFacilityService.getState().subscribe(data => {
      this.stateData = data;
    });
  }
  getFacilityList() {
    this.listLoader = true

    let data = [];
    this.uploadFacilityService.getFacilityList(data, this.page, this.isn, this.fCity, this.fZipcode, this.fincStatus, this.fincAB, this.fName, this.countyName,this.searchText1).subscribe(
      data => {
       
        if (data) {
          this.listLoader = false
          this.facilityDataObj = data;
          this.facilityDataList = this.facilityDataObj['result'];
          let falicityList = data.total;
          this.totalElements = data.total;
          this.facilityDataList = this.facilityDataObj['result'];
          this.totalPages = Math.ceil(data.total / 50);
        } else {
          this.facilityDataList = [];
          this.listLoader = false
        }
      },
      error => {
        this.listLoader = false
       }
    );
  }
  getCityList() {
    this.uploadFacilityService.getCity(this.fCity).subscribe(data => {
      this.cityData = data;
      this.cityHide = false;
    });
  }
  changeCity(city) {
    this.fCity = city;
    this.cityHide = true;
  }
  emptyCity() {
    if (this.cityData == '') {
      this.fCity = ''
    }
  }

  getZipList() {
    this.uploadFacilityService.getZip(this.fZipcode).subscribe(data => {
      this.getZipData = data;
      this.zipHide = false
    });
  }

  changezip(zipcode) {
    this.fZipcode = zipcode;
    this.zipHide = true;
  }

  emptyZip() {
    if (this.getZipData == '') {
      this.fZipcode = ''
    }
  }

  emptyFname() {
    if (this.fnameData == '') {
      this.fName = ''
    }
  }
  getFirstName() {
    this.uploadFacilityService.getfName(this.fName).subscribe(data => {
      this.fnameData = data;
      this.fnamehide = false;
    });
  }
  changeFname(fname) {
    this.fName = fname;
    this.fnamehide = true;
  }

  getCountyList() {
    this.uploadFacilityService.getCounties(this.countyName).subscribe(data => {
      this.options = data;
     
      this.countyHide = false;
    });
  }
  changeCounty(name) {
    this.countyName = name;
    this.countyHide = true;
  }
  emptyCounty() {
    if (this.options == '') {
      this.countyName = ''
    }
  }

  emptyLname() {
    if (this.lnameData == '') {
      this.lName = ''
    }
  }
  changeLname(lname) {
    this.lName = lname;
    this.lnamehide = true;
  }

  getisnList() {
    this.uploadFacilityService.getIsn(this.isn).subscribe(data => {
      this.isnData = data;
      this.isnHide = false;
    });
  }
  changeisn(isn) {
    this.isn = isn;
    this.isnHide = true;
  }

  resetAfterFilter() {
    this.isnHide = true;
    this.lnamehide = true;
    this.fnamehide = true;
    this.zipHide = true;
    this.cityHide = true;
    this.countyHide = true;
  }
  emptyIsn() {
    if (this.isnData == '') {
      this.isn = ''
    }
  }

  showFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }
  hideFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }

  showNotification(statusCode, message) {
   
    switch (statusCode) {
      case 200:
        this.showSucccess(message);
       
        break;

      default:
        this.showError(message);
        break;
    }
  }
  showSucccess(message) {
    this.toastr.success(message);
  }
  showError(message) {
    this.toastr.error(message);
  }
  showWarning() {
    this.toastr.warning('You are being warned.', 'Alert!');
  }

  editFacility(id) {
    this.listLoader = true
    this.uploadFacilityService.getFacilityById(id).subscribe(
      data => {
        this.listLoader = false
        this.open2(this.content);
        setTimeout(() => {

          this.sumbitFacilityData = data;
          this.prefPhone = data.prefPhone;
          this.prefMobile = data.prefMobile;
          this.speaks_spanish = data.speaks_spanish;
          this.prefCompany = data.prefCompany;
          this.comments = data.comments;
          this.mechDuty = data.mech_duty
          this.editProfileId = id;
          this.getSubsription(id);
         
          // this.updateForm(data);

        }, 500);
      },
      error => {
        this.listLoader = false
        this.showError(error.message);
      }
    );
  }
  submitFacility(language) {
    this.modalLoader = true
    let lang;
    let langKey;
    if (language == 'false') {
      lang = "US";
      langKey = 'en';
    }
    else if (language == 'true') {
      lang = "ES";
      langKey = 'es';
    }

    const data = {
      "preferredMobile": this.prefMobile,
      "phone": this.prefPhone,
      "langKey": langKey,
      "lang": lang,
      "prefCompany": this.prefCompany,
      "comments": this.comments,
      "mechDuty":this.mechDuty
    }


    this.uploadFacilityService.saveFacility(data, this.editProfileId)
      .subscribe(data => {
        if (data.statusCode == 200) {
          this.modalLoader = false
          this.showNotification(200, data.message)
        }
        else if (data.statusCode == 400) {
          this.modalLoader = false
          this.showError(data.message);
        }
      }, error => {
        this.modalLoader = false
        this.showError(error.message);
      })
  }

  getSubsription(profileId) {
    this.subscriptionService.getPackSubscription(profileId).subscribe(
      data => {
        this.subscribeData = [];
        this.subscribeData = data;
        if (this.subscribeData.length == 0) {
        }

        
      },
      error => {
      }
    );
  }

  deleteFacility(npn) {
    this.uploadFacilityService.deleteFacilityById(parseInt(npn, 10)).subscribe(
      data => {
      
        this.showNotification(data.statusCode, data.message);
        if (data.statusCode == 200) {
        }
      },
      error => {
    
        this.showError(error.message);
      }
    );
  }

  open2(content) {
  
    this.modalService.open(content, { size: 'lg', backdrop: 'static' }).result.then(
      result => {

      },
      reason => {
        
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public updateForm(dataObj) {
    Object.keys(dataObj).forEach(dataKey => {
      if (this.uploadFacilityForm.controls[dataKey]) {
        this.uploadFacilityForm.controls[dataKey].setValue(dataObj[dataKey]);
      }
    });
  }
  initFormData() {
    this.uploadFacilityForm = this.fbuilder.group({
      name: [null, [Validators.required]],
      address: ['', [Validators.required]],
      city: ['', [Validators.required]],
      zipCode: ['', [Validators.required]],
      weekDayHours: ['', [Validators.required]],
      saturdayHours: ['', [Validators.required]],
      sundayHours: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      isn: ['', [Validators.required]],
      county: ['', [Validators.required]],
      state: ['', [Validators.required]],
      bPhone: ['', [Validators.required]],
      bEmail: ['', [Validators.required]],
      insp_type: ['', [Validators.required]],
      mech_duty: ['', [Validators.required]]

    });
  }

  showFormError() {
    this.showError('Please Fill all the required field Before submitting');
  }
  facilityFormSubmit(formData) {
  
    this.formSubmmit = true;
    const facilityData = this.uploadFacilityForm.value;

    if (this.uploadFacilityForm.invalid) {
      this.showError('Please fill all mandatory values.');
    } else {
      facilityData.name = this.uploadFacilityForm.value['name'];
      facilityData.address = this.uploadFacilityForm.value['address'];
      facilityData.city = this.uploadFacilityForm.value['city'];
      facilityData.zipCode = this.uploadFacilityForm.value['zipCode'];
      facilityData.weekDayHours = this.uploadFacilityForm.value['weekDayHours'];
      facilityData.saturdayHours = this.uploadFacilityForm.value['saturdayHours'];
      facilityData.sundayHours = this.uploadFacilityForm.value['sundayHours'];
      facilityData.phone = this.uploadFacilityForm.value['phone'];

      this.uploadFacilityService.addFacility(facilityData).subscribe(
        data => {
      
          this.showNotification(data.statusCode, data.message);
        },
        error => {
         
          this.showError(error.message);
        }
      );
    }
  }

  pageChanged(event) {
    this.page = event;
    this.getFacilityList();
  }

  downloadCSV(fileType) {
    this.mv1loader = true
    if (this.sortValue == '' && this.colname == '') {
      this.sortData = [];
    } else {
      this.sortNpn(this.sortValue, this.colname);
      this.sortData = [
        {
          colName: this.colname,
          sortType: this.sortValue
        }
      ];
    }
    this.uploadFacilityService
      .downloadfacility(this.sortData, this.fCity, this.isn, this.fZipcode, this.fincStatus, this.fincAB, this.fName, this.countyName, fileType,this.searchText1

      )
      .subscribe(
        data => {
          this.mv1loader = false
          if (fileType == 'CSV') {
            this.fileDownloadUtility.downloadCsv(data, 'Facility');
            this.mv1loader = false
          }
          else if (fileType == 'XLS') {
            this.mv1loader = false
            this.fileDownloadUtility.downloadFacXls(data);
          }
        },
        error => {
          this.mv1loader = false
        }
      );
  }
  searchEnter(event) {
    if (event.keyCode == 13) {
      this.FilterData();
    }
  }

  FilterData() {
    this.listLoader = true
    let data = [];
    this.uploadFacilityService
      .getFacilityList(
        data,
        this.page,
        this.isn,
        this.fCity,
        this.fZipcode,
        this.fincStatus,
        this.fincAB,
        this.fName,
        this.countyName,
        this.searchText1
      )
      .subscribe(
        data => {
          if (data) {
            this.listLoader = false
            this.facilityDataObj = data;
            this.facilityDataList = this.facilityDataObj['result'];
            let falicityList = data.total;
            this.totalElements = data.total;
            this.facilityDataList = this.facilityDataObj['result'];
            this.totalPages = Math.ceil(data.total / 50);
            this.resetAfterFilter();
          } else {
            this.facilityDataList = [];
            this.listLoader = false
          }
        },
        error => {
          this.listLoader = false
          this.resetAfterFilter();
          
        }
      );
  }
  resetFilterData() {
    this.clearFormData();
    this.getFacilityList();
  }
  clearFormData() {
    this.fZipcode = '';
    this.fCity = '';
    this.fName = '';
    this.lName = '';
    this.fincStatus = '';
    this.fincAB = '';
    this.isn = '';
    this.countyName = '';
  }

  sortNpn(sort, name) {
    this.sortValue = sort;
    this.colname = name;
    if (sort == 'asc' && name == 'isn') {
      this.isnFlag = true;
    } else if (sort == 'desc' && name == 'isn') {
      this.isnFlag = false;
      const data = [
        {
          colName: name,
          sortType: sort
        }
      ];
      this.sortData = data;
      this.uploadFacilityService
        .getFacilityList(
          data,
          this.page,
          this.isn,
          this.fCity,
          this.fZipcode,
          this.fincStatus,
          this.fincAB,
          this.fName,
          this.countyName,
          this.searchText1
        )
        .subscribe(
          data => {
            if (data) {
             
              let agentList = data.total;
              this.agentDataObj = data;
              this.totalElements = data.total;
              this.facilityDataList = this.agentDataObj['result'];
              this.totalPages = Math.ceil(data.total / 50)
            } else {
              this.facilityDataList = [];
            }
          },
          error => { }
        );
    }

  }

}
