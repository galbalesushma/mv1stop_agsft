import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FacilityComponent } from './facility.component';

const routes: Routes = [
    {
        path: '',
        component: FacilityComponent,
        children: [
            { path: '', redirectTo: '/facility/listFacility', pathMatch: 'full' },
            {
                path: 'uploadFacility',
                loadChildren: './upload-facility/upload-facility.module#UploadFacilityModule'
            },
            {
                path: 'modifyFacility',
                loadChildren: './modify-facility/modify-facility.module#ModifyFacilityModule'
            },
            {
                path: 'listFacility',
                loadChildren: './list-facility/list-facility.module#ListFacilityModule'
            }
            //   {
            //     path: 'subscription',
            //     loadChildren: '../subscription/subscription.module#SubscriptionModule'
            //     // loadChildren: '../subscription/subscription.module#SubscriptionModule'
            // }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FacilityRoutingModule {}
