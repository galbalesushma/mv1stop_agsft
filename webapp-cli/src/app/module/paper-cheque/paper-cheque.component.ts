import { Component, OnInit, ViewChild } from '@angular/core';
import { UploadAgentService } from 'app/service/upload-agent.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DropdownSettingModel, DropdownSettingSearchModel } from 'app/shared/model/dropdown.model';
import { AuthUserService } from 'app/service/auth-user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paper-cheque',
  templateUrl: './paper-cheque.component.html',
  styleUrls: ['./paper-cheque.component.css'],
  providers: [ToastrService]
})
export class PaperChequeComponent implements OnInit {
  pathHref: any;
  searchText1 = '';
  agentDataList = [];
  agentDataObj = {};

  stateList = [];

  showFilterFlag = false;
  page = 1;
  totalPages = 1;
  listLoader = false

  // public uploadAgentForm: FormGroup;
  // registerForm: FormGroup;
  // public dropdownList = [];
  // selectedItems = [];
  // dropdownSettings = {};
  // public phoneNo;
  // public epbaxNo;
  public totalElements = 1;
  public countyName = '';
  // public selectedstateId = '';
  public selectedStateName = '';
  // public options;
  // public selectedOptions;
  public countyHide = false;
  public zipHide = false;
  public cityHide = false;
  public fnamehide = false;
  public lnamehide = false;
  public npnhide = false;
  public companyhide = false;
  // public prefCompany = '';
  // public comments = '';
  // public insuranceCompany = ''
  public fZipcode = '';
  public fCity = '';
  // public getZipData;
  // public cityData;
  public fName = '';
  public fnameData;
  public lName = '';
  public lnameData = '';
  // public npnData;
  public fnpn = '';
  // public companyData;
  public fcompany = '';
  public incStatus = '';
  public licStatus = '';
  public disabled = false;
  public npnFlag = false;
  public nameFlag = false;
  public dateFlag = false;
  public preCertFlag = false;
  public certFlag = false;
  // public subscribeData = [];
  public noFlag = false;
  public sortData = [];
  public sortValue = '';
  public colname = '';
  // public downloader = false;
  // public stateData = [];


  constructor(
    public authUserService: AuthUserService,
    public fbuilder: FormBuilder,
    public uploadAgentService: UploadAgentService,
    public toastr: ToastrService,
    private router: Router,
    private modalService: NgbModal) {
    // this.pathHref = this.router.url;
    // console.log(this.pathHref);
  }

  ngOnInit() {
    this.getAgentList();
    // this.getAgentListPaperCheque();

  }
  searchEnter(event) {
    if (event.keyCode == 13) {
      this.FilterData();
    }
  }

  FilterData() {
    // alert(this.searchText);
    this.listLoader = true
    let data = [];
    this.uploadAgentService
      .getFilterAgentList(
        data,
        this.page,
        this.selectedStateName,
        this.countyName,
        this.fCity,
        this.fZipcode,
        this.fnpn,
        this.licStatus,
        this.incStatus,
        this.fcompany,
        this.fName,
        this.lName,
        this.searchText1
      )
      .subscribe(
        data => {
          if (data) {
            this.listLoader = false
            let agentList = data.total;
            this.agentDataObj = data;
            this.totalElements = data.total;
            this.totalPages = Math.ceil(data.total / 50)
            this.agentDataList = this.agentDataObj['result'];
            // this.hideafterFilter()
          } else {
            this.agentDataList = [];
            this.listLoader = false

          }
        },
        error => {
          // this.hideafterFilter()
          this.listLoader = false
          this.showError(error.message)
        }
      );
  }

  // hideafterFilter() {
  //   this.npnhide = true;
  //   this.companyhide = true;
  //   this.lnamehide = true;
  //   this.fnamehide = true;
  //   this.cityHide = true;
  //   this.zipHide = true;
  //   this.countyHide = true;
  // }

  pageChanged(event) {
    // this.getAgentListPaperCheque();

    // this.page = event;
    // if(this.totalPages > this.page){
    this.getAgentList();
    // }
  }

  getAgentList() {
    this.listLoader = true
    this.uploadAgentService
      .getAgentAllAgentList(this.page, this.selectedStateName, this.countyName, this.fCity, this.fZipcode, this.fnpn, this.licStatus, this.incStatus, this.fcompany, this.fName, this.lName)
      .subscribe(
        data => {
          if (data) {
            this.listLoader = false
            let agentList = data.total;
            this.agentDataObj = data;
            this.totalElements = data.total;
            this.totalPages = Math.ceil(data.total / 50)
            this.agentDataList = this.agentDataObj['result'];
          } else {
            this.listLoader = false
            this.agentDataList = [];
          }
        },
        error => {
          this.listLoader = false
        }
      );
  }

  // getAgentListPaperCheque(){
  //   this.uploadAgentService.getAgentAllPaperCheque(this.page, this.selectedStateName, this.countyName, this.fCity, this.fZipcode, this.fnpn, this.licStatus, this.incStatus, this.fcompany, this.fName, this.lName).subscribe(
  //     data => {
  //       if(data){
  //         let agentList = data.total;
  //           this.agentDataObj = data;
  //           this.totalElements = data.total;
  //           this.totalPages = Math.ceil(data.total / 50)
  //           this.agentDataList = this.agentDataObj['result'];
  //         } else {
  //           this.agentDataList = [];
  //         }
  //     },
  //     error => { }
  //   );
  // }

  sortNpn(sort, name) {
    this.listLoader = true
    this.sortValue = sort;
    this.colname = name;
    if (sort == 'asc' && name == 'npn') {
      this.npnFlag = true;
    } else if (sort == 'desc' && name == 'npn') {
      this.npnFlag = false;
    } else if (sort == 'asc' && name == 'firstName') {
      this.nameFlag = true;
    } else if (sort == 'desc' && name == 'firstName') {
      this.nameFlag = false;
    } else if (sort == 'asc' && name == 'effDate') {
      this.dateFlag = true;
    } else if (sort == 'desc' && name == 'effDate') {
      this.dateFlag = false;
    } else if (sort == 'asc' && name == 'preCertified') {
      this.preCertFlag = true;
    } else if (sort == 'desc' && name == 'preCertified') {
      this.preCertFlag = false;
    } else if (sort == 'asc' && name == 'certified') {
      this.certFlag = true;
    } else if (sort == 'desc' && name == 'certified') {
      this.certFlag = false;
    }
    const data = [
      {
        colName: name,
        sortType: sort
      }
    ];
    this.sortData = data;
    this.uploadAgentService
      .getFilterAgentList(
        data,
        this.page,
        this.selectedStateName,
        this.countyName,
        this.fCity,
        this.fZipcode,
        this.fnpn,
        this.licStatus,
        this.incStatus,
        this.fcompany,
        this.fName,
        this.lName,
        this.searchText1
      )
      .subscribe(
        data => {
          if (data) {
            this.listLoader = false
            let agentList = data.total;
            this.agentDataObj = data;
            this.totalElements = data.total;
            this.totalPages = Math.ceil(data.total / 50)
            this.agentDataList = this.agentDataObj['result'];
          } else {
            this.agentDataList = [];
            this.listLoader = false
          }
        },
        error => {
          this.listLoader = false
        }
      );
  }

  redirectToCart(profileId, isRegistered,npn) {
    if (isRegistered) {
      localStorage.setItem('profileId', profileId);
      localStorage.setItem('npn',npn)
      this.router.navigate(["/agent-profile/subscription/pricing"]);

    } else {
      this.showPaperCheckWarning();
    }

  }


  showSucccess(message) {
    this.toastr.success(message);
  }
  showError(message) {
    this.toastr.error(message);
  }
  showWarning() {
    this.toastr.warning('You are being warned.', 'Alert!');
  }

  showPaperCheckWarning() {
    this.toastr.warning('Agent not registered, please contact to Admin', 'Alert!');
  }

}
