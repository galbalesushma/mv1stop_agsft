import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaperChequeComponent } from './paper-cheque.component';

const routes: Routes = [
  {
      path: '',
      component: PaperChequeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class PaperChequeRoutingModule { }
 