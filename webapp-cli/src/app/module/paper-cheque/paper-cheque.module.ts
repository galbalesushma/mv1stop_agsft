import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PaperChequeRoutingModule } from './paper-cheque-routing.module';
import { PaperChequeComponent } from './paper-cheque.component';
import { SearchModule } from '../../shared/search/search.module';
import { SearchFilterPipeModule } from '../../shared/pipes/searchFilter/searchFilter.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    PaperChequeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    // ToastrModule.forRoot(),
    SearchFilterPipeModule,
    SearchModule,
  ],
  declarations: [PaperChequeComponent]
})
export class PaperChequeModule { }
