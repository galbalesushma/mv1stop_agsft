
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdmininvoicelogRoutingModule } from './admininvoicelog-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchModule } from 'app/shared/search/search.module';
import { SearchFilterPipeModule } from 'app/shared/pipes/searchFilter/searchFilter.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdmininvoicelogComponent } from './admininvoicelog.component';

@NgModule({
  imports: [
    CommonModule,
    AdmininvoicelogRoutingModule,
    FormsModule,
    SearchModule,
    SearchFilterPipeModule,
    NgbModule
  ],
  declarations: [AdmininvoicelogComponent]
})
export class AdmininvoicelogModule { }
