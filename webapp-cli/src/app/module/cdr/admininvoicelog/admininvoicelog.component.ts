import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../service/reports.service';
import { FileDownloadUtility } from '../../../shared/interceptor/fileDownload';

@Component({
  selector: 'app-admininvoicelog',
  templateUrl: './admininvoicelog.component.html',
  styleUrls: ['./admininvoicelog.component.css']
})
export class AdmininvoicelogComponent implements OnInit {

  historyData = []
  searchText1 = "";
  showFilterFlag = false;
  downloader = false;
  totalPages = 1;
  totalElements = 0;
  page = 1
  time = new Date();
  listLoader = false
  constructor(public reportsService: ReportsService, public fileDownloadUtility: FileDownloadUtility) { }

  ngOnInit() {
    this.getAdminInvoiceList()
  }


  FilterData() {
    this.page = 1;
    this.getAdminInvoiceList();
  }

  getAdminInvoiceList() {
    this.listLoader = true
    this.reportsService.getAdminInvoiceList(this.page, 20,this.searchText1)
      .subscribe(data => {
        this.listLoader = false
        this.historyData = data.result;
        this.totalElements = data.total;
       
        this.totalPages = Math.ceil(this.totalElements / 20);
      },
        error => {
          this.listLoader = false

        });
  }
  pageChanged(event) {
    this.page = event;
    this.getAdminInvoiceList();
  }

}
