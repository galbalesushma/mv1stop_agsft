import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdmininvoicelogComponent } from './admininvoicelog.component';

const routes: Routes = [
    {
        path: '',
        component: AdmininvoicelogComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmininvoicelogRoutingModule {}
