import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmininvoicelogComponent } from './admininvoicelog.component';

describe('AdmininvoicelogComponent', () => {
  let component: AdmininvoicelogComponent;
  let fixture: ComponentFixture<AdmininvoicelogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmininvoicelogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmininvoicelogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
