import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../../../service/reports.service';
import { FileDownloadUtility } from '../../../shared/interceptor/fileDownload';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
  providers: [FileDownloadUtility]
})
export class HistoryComponent implements OnInit {
  historyData = []
  searchText1 = "";
  showFilterFlag = false;
  downloader = false;
  totalPages = 1;
  totalElements = 0;
  page = 1
  time = new Date();
  listLoader = false;
  constructor(public reportsService: ReportsService, public fileDownloadUtility: FileDownloadUtility) { }

  ngOnInit() {
    this.getCdrLog()
  }

  searchEnter(event) {
    if (event.keyCode == 13) {
      this.FilterData();
    }
  }
  FilterData() {
    this.getCdrLog();
  }

  getCdrLog() {
    this.listLoader = true
    this.reportsService.getCdrLogList(this.page, this.searchText1)
      .subscribe(data => {
        this.listLoader = false
        this.historyData = data.result;
        this.totalElements = data.total;
       
        this.totalPages = Math.ceil(this.totalElements / 20);
      },
        error => {
          this.listLoader = false
        });
  }
  pageChanged(event) {
    this.page = event;
    this.getCdrLog();
  }
  download(id) {
    this.reportsService.downloadLog(id, this.searchText1)
      .subscribe(data => {
       
        this.fileDownloadUtility.downloadCsv(data, 'CDR_LOG');

      },
        error => {

        })
  }
}
