import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryComponent} from '../history/history.component';
import { HistoryRoutingModule } from '../history/history-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchModule } from 'app/shared/search/search.module';
import { SearchFilterPipeModule } from 'app/shared/pipes/searchFilter/searchFilter.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    HistoryRoutingModule,
    FormsModule,
    SearchModule,
    SearchFilterPipeModule,
    NgbModule
  ],
  declarations: [HistoryComponent]
})
export class HistoryModule { }
