import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdrRoutingModule } from './cdr-routing.module';
import { CdrComponent } from './cdr.component';

@NgModule({
  imports: [
    CommonModule,
    CdrRoutingModule
  ],
  declarations: [CdrComponent]
})
export class CdrModule { }

