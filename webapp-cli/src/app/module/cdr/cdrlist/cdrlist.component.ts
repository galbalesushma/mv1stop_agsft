import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ReportsService } from '../../../service/reports.service';
import { FileDownloadUtility } from '../../../shared/interceptor/fileDownload';
@Component({
  selector: 'app-cdrlist',
  templateUrl: './cdrlist.component.html',
  styleUrls: ['./cdrlist.component.css'],
  providers: [FileDownloadUtility]
})
export class CdrlistComponent implements OnInit {
  searchText1 = "";
  showFilterFlag = false;
  downloader = false;
  cdrData = [];
  totalPages = 1;
  totalElements = 0;
  page = 1
  time = new Date();
  modalData = {}
  type='all';
  listLoader = false;
  mv1loader = false;
  @ViewChild('cdrContent') cdrContent: NgbModal;


  constructor(public reportsService: ReportsService, public fileDownloadUtility: FileDownloadUtility, private modalService: NgbModal) { }

  ngOnInit() {

    this.getCdrList();
  }
  hideFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }
  showFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }
  downloadCSV(filetype) {
    this.mv1loader = true
    this.reportsService.getCdrListDownload(this.page, this.type,filetype,this.searchText1)
    .subscribe(data => {
      if (filetype == 'CSV') {
        this.mv1loader = false
        this.fileDownloadUtility.downloadCsv(data, 'agent');
      }
      else if (filetype == 'XLS') {
        this.mv1loader = false
        this.fileDownloadUtility.downloadagentXls(data);
      }
    },
      error => {
        this.mv1loader = false
      });
  }
  resetFilterData() { }
  searchEnter(event) {
    if (event.keyCode == 13) {
      this.FilterData();
    }
  }
  FilterData() { 
    this.getCdrList();
  }
  getCdrList() {
    this.listLoader = true
    this.reportsService.getCdrList(this.page, this.type,this.searchText1)
      .subscribe(data => {
        this.listLoader = false
        this.cdrData = data.result;
        this.totalElements = data.total;
       
        this.totalPages = Math.ceil(this.totalElements / 50);
      },
        error => {
          this.listLoader = false

        });

  }
  pageChanged(event) {
    this.page = event;
    this.getCdrList();
  }

  open2(content) {
   
    this.modalService.open(content, { size: 'lg', backdrop: 'static' }).result.then(
      result => {
      },
      reason => {

      }
    );
  }
  editCdr(extension) {
    this.open2(this.cdrContent);
    for (let i in this.cdrData) {
      if (this.cdrData[i].id == extension) {
        this.modalData = this.cdrData[i];
       
      }
    }
  }
  changeCdr(type) {
    this.type = type;
    this.listLoader = true
    this.reportsService.getCdrList(this.page, this.type,this.searchText1)
      .subscribe(data => {
        this.listLoader = false
        this.cdrData = data.result;
        this.totalElements = data.total;
       
        this.totalPages = Math.ceil(this.totalElements / 50);
      },
        error => {
          this.listLoader = false
        });
  }
}
