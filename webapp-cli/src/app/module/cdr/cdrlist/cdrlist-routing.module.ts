import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CdrlistComponent } from './cdrlist.component';

const routes: Routes = [
    {
        path: '',
        component: CdrlistComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CdrlistRoutingModule {}
