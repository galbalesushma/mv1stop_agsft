import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdrlistComponent } from './cdrlist.component';

describe('CdrlistComponent', () => {
  let component: CdrlistComponent;
  let fixture: ComponentFixture<CdrlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdrlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdrlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
