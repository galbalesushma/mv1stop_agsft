
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdrlistComponent} from './cdrlist.component';
import { CdrlistRoutingModule } from './cdrlist-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchModule } from 'app/shared/search/search.module';
import { SearchFilterPipeModule } from 'app/shared/pipes/searchFilter/searchFilter.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    CdrlistRoutingModule,
    FormsModule,
    SearchModule,
    SearchFilterPipeModule,
    NgbModule
  ],
  declarations: [CdrlistComponent]
})
export class CdrlistModule { }
