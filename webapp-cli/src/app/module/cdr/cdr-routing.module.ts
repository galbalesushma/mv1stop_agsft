import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CdrComponent } from './cdr.component';

const routes: Routes = [
  {
      path: '',
      component: CdrComponent,
      children: [
          { path: '', redirectTo: '/user/cdrlist', pathMatch: 'full' },
          {
              path: 'cdrlist',
              loadChildren: './cdrlist/cdrlist.module#CdrlistModule'
          },
          {
              path: 'cdrlog',
              loadChildren: './history/history.module#HistoryModule'
          },
          {
            path: 'paymentlog',
            loadChildren: './admininvoicelog/admininvoicelog.module#AdmininvoicelogModule'
        }
      ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CdrRoutingModule {}
