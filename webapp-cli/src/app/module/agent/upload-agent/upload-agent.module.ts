import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UploadAgentRoutingModule } from './upload-agent-routing.module';
import { UploadAgentComponent } from './upload-agent.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbProgressbar, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    imports: [
        CommonModule,
        UploadAgentRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        NgbModule
        // ToastrModule.forRoot()
    ],
    declarations: [UploadAgentComponent]
})
export class UploadAgentModule {}
