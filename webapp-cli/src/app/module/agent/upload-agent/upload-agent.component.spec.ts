import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadAgentComponent } from './upload-agent.component';

describe('UploadAgentComponent', () => {
    let component: UploadAgentComponent;
    let fixture: ComponentFixture<UploadAgentComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [UploadAgentComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(UploadAgentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
