import { Component, OnInit, HostListener } from '@angular/core';
import { UploadAgentService } from 'app/service/upload-agent.service';
import { HttpEventType, HttpResponse, HttpRequest, HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
    selector: 'app-upload-agent',
    templateUrl: './upload-agent.component.html',
    styleUrls: ['./upload-agent.component.css'],
    providers: [UploadAgentService, ToastrService]
})
export class UploadAgentComponent implements OnInit {
    dynamicHeight;

    public fileSelected: File;

    public fileUploadProgress = 0;

    public uploadAgentForm: FormGroup;
    registerForm: FormGroup;

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.dynamicHeight = window.innerHeight;
    }
    constructor(private uploadAgentService: UploadAgentService, public fbuilder: FormBuilder, public toastr: ToastrService) {
        this.initFormData();
    }

    ngOnInit() {
        this.dynamicHeight = window.innerHeight;
    }
    showFormError() {
        this.showError('Please Fill all the required field Before submitting');
    }
    agentFormSubmit(formData) {
        let agentData = this.uploadAgentForm.value;

        agentData.firstActiveDate = new Date(this.uploadAgentForm.value['firstActiveDate']).toISOString();
        agentData.effectiveDate = new Date(this.uploadAgentForm.value['effectiveDate']).toISOString();
        agentData.expDate = new Date(this.uploadAgentForm.value['expDate']).toISOString();
        agentData.resState = this.uploadAgentForm.value['resState'] == 'true' ? true : false;
        agentData.isCECompliant = this.uploadAgentForm.value['isCECompliant'] == 'true' ? true : false;
        agentData.speaksSpanish = this.uploadAgentForm.value['speaksSpanish'] == 'true' ? true : false;

        this.uploadAgentService.addAgent(agentData).subscribe(
            data => {
                this.showNotification(data.statusCode, data.message);
            },
            error => {
                this.showError(error.message);
            }
        );
    }
    initFormData() {
        this.uploadAgentForm = this.fbuilder.group({
            licNumber: [null, [Validators.required]],
            npn: ['', [Validators.required]],
            naicno: ['', [Validators.required]],
            firstName: ['', [Validators.required]],
            middleName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            suffix: ['', [Validators.required]],
            businessName: ['', [Validators.required]],
            fein: ['', [Validators.required]],
            bAddr1: ['', [Validators.required]],
            bAddr2: ['', [Validators.required]],
            bAddr3: ['', [Validators.required]],
            bCity: ['', [Validators.required]],
            bCounty: ['', [Validators.required]],
            bState: ['', [Validators.required]],
            bZip: ['', [Validators.required]],
            bCountry: ['', [Validators.required]],
            bPhone: ['', [Validators.required]],
            bMobile: ['', [Validators.required]],
            bEmail: ['', [Validators.required]],
            licType: ['', [Validators.required]],
            lineAuth: ['', [Validators.required]],
            licStatus: ['', [Validators.required]],
            firstActiveDate: ['', [Validators.required]],
            effectiveDate: ['', [Validators.required]],
            expDate: ['', [Validators.required]],
            domState: ['', [Validators.required]],
            resState: ['', [Validators.required]],
            pAddr1: ['', [Validators.required]],
            pAddr2: ['', [Validators.required]],
            pAddr3: ['', [Validators.required]],
            pCity: ['', [Validators.required]],
            pCounty: ['', [Validators.required]],
            pState: ['', [Validators.required]],
            pZip: ['', [Validators.required]],
            pCountry: ['', [Validators.required]],
            isCECompliant: ['', [Validators.required]],
            speaksSpanish: ['', [Validators.required]]
        });
    }

    selectFile(fileInput, inputInstance: HTMLInputElement) {
        this.fileUploadProgress = 0;
        const files = fileInput.target.files;
        if (files !== undefined && files.length > 0) {
            this.fileSelected = files[0];

            this.uploadCSV();
            inputInstance.value = '';
        }
    }

    uploadCSV() {
        if (this.fileSelected !== undefined) {
            const uploadForm: FormData = new FormData();
            uploadForm.append('file', this.fileSelected, this.fileSelected.name);
            this.uploadAgentService.uploadCSV(uploadForm).subscribe(
                event => {
                    if (event) {
                        switch (event.type) {
                            case HttpEventType.DownloadProgress:
                               
                                break;
                            case HttpEventType.UploadProgress:
                                const progress = Math.round(100 * event.loaded / event.total);
                                if (progress < 90) {
                                    this.fileUploadProgress = progress;
                                } else if (progress >= 90) {
                                    this.fileUploadProgress = 90;
                                }

                               
                                break;
                            case HttpEventType.Response:
                                

                                this.showNotification(event.body.statusCode, event.body.message);
                                this.fileUploadProgress = 100;

                                break;
                            default:
                                break;
                        }
                    }
                },
                error => {
                    this.fileUploadProgress = 0;
                   
                    this.showError(error.message);
                    
                    if (error) {
                       
                        let errorList = error.errorList;

                        for (let errorObj of errorList) {
                            this.showError(errorObj.message);
                        }
                    }
                }
            );
        }
    }

    showNotification(statusCode, message) {
       
        switch (statusCode) {
            case 200:
                this.showSucccess(message);
                
                break;

            default:
                this.showError(message);
                break;
        }
    }
    showSucccess(message) {
        this.toastr.success(message);
    }
    showError(message) {
        this.toastr.error(message);
    }
    showWarning() {
        this.toastr.warning('You are being warned.', 'Alert!');
    }

    showInfo() {
        this.toastr.info('Just some information for you.');
    }
}
