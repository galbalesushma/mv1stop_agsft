import { UploadAgentModule } from './upload-agent.module';

describe('UploadAgentModule', () => {
    let uploadAgentModule: UploadAgentModule;

    beforeEach(() => {
        uploadAgentModule = new UploadAgentModule();
    });

    it('should create an instance', () => {
        expect(uploadAgentModule).toBeTruthy();
    });
});
