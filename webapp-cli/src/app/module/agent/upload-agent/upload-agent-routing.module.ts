import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadAgentComponent } from './upload-agent.component';

const routes: Routes = [
    {
        path: '',
        component: UploadAgentComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UploadAgentRoutingModule {}
