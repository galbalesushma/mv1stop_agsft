import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentComponent } from './agent.component';

const routes: Routes = [
    {
        path: '',
        component: AgentComponent,
        children: [
            { path: '', redirectTo: '/agent/listAgent', pathMatch: 'full' },
            {
                path: 'uploadAgent',
                loadChildren: './upload-agent/upload-agent.module#UploadAgentModule'
            },
            {
                path: 'modifyAgent',
                loadChildren: './modify-agent/modify-agent.module#ModifyAgentModule'
            },
            {
                path: 'listAgent',
                loadChildren: './list-agent/list-agent.module#ListAgentModule'
            }
            //   {
            //     path: 'subscription',
            //     loadChildren: '../subscription/subscription.module#SubscriptionModule'
            //     // loadChildren: '../subscription/subscription.module#SubscriptionModule'
            // }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AgentRoutingModule {}
