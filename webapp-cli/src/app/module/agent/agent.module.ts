import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgentRoutingModule } from './agent-routing.module';
import { AgentComponent } from './agent.component';

@NgModule({
    imports: [CommonModule, AgentRoutingModule],
    declarations: [AgentComponent]
})
export class AgentModule {}
