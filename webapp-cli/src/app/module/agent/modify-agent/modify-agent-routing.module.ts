import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModifyAgentComponent } from './modify-agent.component';

const routes: Routes = [
    {
        path: '',
        component: ModifyAgentComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ModifyAgentRoutingModule {}
