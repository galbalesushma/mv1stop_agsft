import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ModifyAgentRoutingModule } from './modify-agent-routing.module';
import { ModifyAgentComponent } from './modify-agent.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    imports: [
        CommonModule,
        ModifyAgentRoutingModule,
        ReactiveFormsModule,
        NgbModule // ,ToastrModule.forRoot()
    ],
    declarations: [ModifyAgentComponent]
})
export class ModifyAgentModule {}
