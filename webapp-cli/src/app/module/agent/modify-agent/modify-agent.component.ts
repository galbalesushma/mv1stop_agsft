import { Component, OnInit, HostListener } from '@angular/core';
import { UploadAgentService } from 'app/service/upload-agent.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpEventType } from '@angular/common/http';

@Component({
    selector: 'app-modify-agent',
    templateUrl: './modify-agent.component.html',
    styleUrls: ['./modify-agent.component.css'],
    providers: [UploadAgentService, ToastrService]
})
export class ModifyAgentComponent implements OnInit {
    dynamicHeight;

    public fileSelected: File;

    public fileUploadProgress = 0;

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.dynamicHeight = window.innerHeight;
    }
    constructor(private uploadAgentService: UploadAgentService, public toastr: ToastrService) {}

    ngOnInit() {
        this.dynamicHeight = window.innerHeight;
    }

    selectFile(fileInput, inputInstance: HTMLInputElement) {
        this.fileUploadProgress = 0;
        const files = fileInput.target.files;
        if (files !== undefined && files.length > 0) {
            this.fileSelected = files[0];

            this.uploadCSV();
            inputInstance.value = '';
        }
    }

    uploadCSV() {
        if (this.fileSelected !== undefined) {
            const uploadForm: FormData = new FormData();
            uploadForm.append('file', this.fileSelected, this.fileSelected.name);
            this.uploadAgentService.uploadCSV(uploadForm).subscribe(
                event => {
                    if (event) {
                        switch (event.type) {
                            case HttpEventType.DownloadProgress:
                               
                                break;
                            case HttpEventType.UploadProgress:
                                const progress = Math.round(100 * event.loaded / event.total);
                                if (progress < 90) {
                                    this.fileUploadProgress = progress;
                                } else if (progress >= 90) {
                                    this.fileUploadProgress = 90;
                                }
                                break;
                            case HttpEventType.Response:
                                

                                this.showNotification(event.body.statusCode, event.body.message);
                                this.fileUploadProgress = 100;

                                break;
                            default:
                                break;
                        }
                    }
                },
                error => {
                    this.fileUploadProgress = 0;
                    this.showError(error.message);
                    if (error) {
                        let errorList = error.errorList;

                        for (let errorObj of errorList) {
                            this.showError(errorObj.message);
                        }
                    }
                }
            );
        }
    }

    showNotification(statusCode, message) {
        switch (statusCode) {
            case 200:
                this.toastr.success(message);
                break;

            default:
                this.toastr.error(message);
                break;
        }
    }

    showWarning(message) {
        this.toastr.warning(message);
    }

    showError(message) {
        this.toastr.error(message);
    }

    showInfo(message) {
        this.toastr.info(message);
    }
}
