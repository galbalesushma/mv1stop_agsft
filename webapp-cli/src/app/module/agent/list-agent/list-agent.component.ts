import { Component, OnInit, ViewChild } from '@angular/core';
import { UploadAgentService } from 'app/service/upload-agent.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DropdownSettingModel, DropdownSettingSearchModel } from 'app/shared/model/dropdown.model';
import { SubscriptionService } from '../../../service/subscription.service';
import { FileDownloadUtility } from '../../../shared/interceptor/fileDownload';
import { AuthUserService } from 'app/service/auth-user.service';
import Swal from 'sweetalert2'
import 'sweetalert2/src/sweetalert2.scss'
@Component({
  selector: 'app-list-agent',
  templateUrl: './list-agent.component.html',
  styleUrls: ['./list-agent.component.css'],
  providers: [ToastrService, FileDownloadUtility]
})
export class ListAgentComponent implements OnInit {
  totalAmount: number = 0;
  agentData: any;
  dataCompany = [];
  searchText1 = '';
  agentDataList = [];
  agentDataObj = {};
  mv1loader = false;
  stateList = [];
  listLoader = false;
  modalLoader = false;

  showFilterFlag = false;
  page = 1;
  totalPages = 1;

  public uploadAgentForm: FormGroup;
  registerForm: FormGroup;
  public dropdownSettingState: DropdownSettingModel;
  public dropdownSettingCounty: DropdownSettingSearchModel;
  public dropdownSettingCity: DropdownSettingModel;
  public dropdownSettingZip: DropdownSettingModel;
  public dropdownSettingLStatus: DropdownSettingModel;
  public dropdownSettingInc: DropdownSettingModel;
  public dropdownSettingCertified: DropdownSettingModel;
  public dropdownSettingPCertified: DropdownSettingModel;
  public dropdownSettingCompany: DropdownSettingModel;

  public dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  public phoneNo;
  public epbaxNo;
  public totalElements = 1;
  public countyName = '';
  public selectedstateId = '';
  public selectedStateName = '';
  public options;
  public selectedOptions;
  public countyHide = false;
  public zipHide = false;
  public cityHide = false;
  public fnamehide = false;
  public lnamehide = false;
  public npnhide = false;
  public companyhide = false;
  public prefCompany = null;
  public comments = null;
  public insuranceCompany = null
  public fZipcode = '';
  public fCity = '';
  public getZipData;
  public cityData;
  public fName = '';
  public fnameData;
  public lName = '';
  public lnameData = '';
  public npnData;
  public fnpn = '';
  public companyData;
  public fcompany = '';
  public incStatus = '';
  public licStatus = '';
  public disabled = false;
  public npnFlag = false;
  public nameFlag = false;
  public dateFlag = false;
  public preCertFlag = false;
  public certFlag = false;
  public subscribeData = [];
  public noFlag = false;
  public sortData = [];
  public sortValue = '';
  public colname = '';
  public downloader = false;
  public stateData = [];
  selectedCompanyId;
  selectedCompanyName =false
  public submitAgentData = {
    prefPhone: '',
    prefEmail: '',
    prefMobile: '',
    speaksSpanish: '',
    licNumber: '',
    npn: '',
    firstName: '',
    middleName: '',
    lastName: '',
    suffix: '',
    businessName: '',
    bAddr1: '',
    bAddr2: '',
    bAddr3: '',
    bCityName: '',
    bStateName: '',
    bZip: '',
    bCountyName: '',
    bCountry: '',
    bPhone: '',
    bMobile: '',
    bEmail: '',
    licType: '',
    lineAuth: '',
    licStatus: '',
    firstActiveDate: '',
    effectiveDate: '',
    expDate: '',
    dStateName: '',
    resState: '',
    pAddr1: '',
    pAddr2: '',
    pAddr3: '',
    pCityName: '',
    pStateName: '',
    pCountry: '',
    pCountyName: '',
    pZip: '',
    isCECompliant: '',
    secondPhone: ''
  };

  // proofModel = {
  //   insuranceCompany: '',
  // }

  public editProfileId;
  public prefPhone =null;
  public prefMobile = null;
  public speaksSpanish;
  public carriers = [];
  public validFlag: boolean

  @ViewChild('content') content: NgbModal;
  @ViewChild('infoView') infoView: NgbModal
  @ViewChild('alertModal') alertModal: NgbModal
  constructor(
    public authUserService: AuthUserService,
    public fileDownloadUtility: FileDownloadUtility,
    public uploadAgentService: UploadAgentService,
    public subscriptionService: SubscriptionService,
    public fbuilder: FormBuilder,
    public toastr: ToastrService,
    private modalService: NgbModal
  ) {
    this.initFormData();
  }

  onItemSelect(item: any, x) {
    this.selectedstateId = item.id;
    this.selectedStateName = item.itemName;
    if (this.selectedstateId != null || this.selectedstateId != '' || this.selectedstateId != null) {
      this.disabled = true;
    } else {
      this.disabled = false;
    }
  }
  OnItemDeSelect(item: any, x) {
  }

  onItemSelectInc(item) {
  }

  ngOnInit() {
    this.getListOfCarrier();
    this.settings();
    this.zipHide = true;
    this.countyHide = true;
    this.cityHide = true;
    this.fnamehide = true;
    this.lnamehide = true;
    this.npnhide = true;
    this.companyhide = true;
    this.countyName = '';
    setTimeout(() => {
      this.selectedOptions = [];
    }, 500);
    // this.agentDataList = this.agentDataObj.result;
    this.getAgentList();

    this.initDropDown();

    // this.downloadCSV();
  }

  showFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }
  changeValue(event: any) {
  }

  showFormError() { }
  hideFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }
  initDropDown() {
    this.dropdownSettingState = new DropdownSettingModel({
      text: 'STATE'
    });
    this.dropdownSettingCounty = new DropdownSettingModel({
      text: 'COUNTY'
    });

    this.dropdownSettingCity = new DropdownSettingModel({
      text: 'CITY'
    });
    this.dropdownSettingZip = new DropdownSettingModel({
      text: 'ZIP'
    });
    this.dropdownSettingLStatus = new DropdownSettingModel({
      text: 'INC STATUS'
    });
    this.dropdownSettingInc = new DropdownSettingModel({
      text: 'INC A/B'
    });
    this.dropdownSettingCertified = new DropdownSettingModel({
      text: 'CERTIFIED'
    });
    this.dropdownSettingPCertified = new DropdownSettingModel({
      text: 'P CERTIF'
    });
    this.dropdownSettingCompany = new DropdownSettingModel({
      text: 'COMPANY'
    });
    let searchText1;
    let zipcode;
    let company;
    this.uploadAgentService.getState().subscribe(data => {
      this.stateData = data;
    });
    this.dropdownSettingLStatus.data = [{ id: 1, itemName: 'ACTIVE' }, { id: 2, itemName: 'INACTIVE' }];
    this.dropdownSettingInc.data = [{ id: 1, itemName: 'YES' }, { id: 2, itemName: 'NO' }];
    this.dropdownSettingCertified.data = [{ id: 1, itemName: 'YES' }, { id: 2, itemName: 'NO' }];
    this.dropdownSettingPCertified.data = [{ id: 1, itemName: 'YES' }, { id: 2, itemName: 'NO' }];
  }
  getCountyList() {
    this.uploadAgentService.getCounties(this.selectedstateId, this.countyName).subscribe(data => {
      this.options = data;
      this.countyHide = false;
    });
  }
  getZipList() {
    this.uploadAgentService.getZip(this.fZipcode).subscribe(data => {
      this.getZipData = data;
      this.zipHide = false;
    });
  }
  getCityList() {
    this.uploadAgentService.getCity(this.selectedstateId, this.fCity).subscribe(data => {
      this.cityData = data;
      this.cityHide = false;
    });
  }
  getFirstName() {
    this.uploadAgentService.getFname(this.fName).subscribe(data => {
      this.fnameData = data;
      this.fnamehide = false;
    });
  }
  getLastName() {
    this.uploadAgentService.getLname(this.lName).subscribe(data => {
      this.lnameData = data;
      this.lnamehide = false;
    });
  }
  getnpnList() {
    this.uploadAgentService.getNpn(this.fnpn).subscribe(data => {
      this.npnData = data;
      this.npnhide = false;
    });
  }
  getCompanyList() {
    this.uploadAgentService.getCompany(this.fcompany).subscribe(data => {
      this.companyData = data;
      this.companyhide = false;
    });
  }

  changeCounty(name) {
    this.countyName = name;
    this.countyHide = true;
  }

  changezip(zipcode) {
    this.fZipcode = zipcode;
    this.zipHide = true;
  }
  changeCity(city) {
    this.fCity = city;
    this.cityHide = true;
  }
  changeFname(fname) {
    this.fName = fname;
    this.fnamehide = true;
  }
  changeLname(lname) {
    this.lName = lname;
    this.lnamehide = true;
  }
  changenpn(npn) {
    this.fnpn = npn;
    this.npnhide = true;
  }
  changeCompany(company) {
    this.fcompany = company;
    this.companyhide = true;
  }

  hideafterFilter() {
    this.npnhide = true;
    this.companyhide = true;
    this.lnamehide = true;
    this.fnamehide = true;
    this.cityHide = true;
    this.zipHide = true;
    this.countyHide = true;
  }
  searchEnter(event) {
    if (event.keyCode == 13) {
      this.FilterData();
    }
  }
  FilterData() {
    // alert(this.searchText);
    this.listLoader = true
    let data = [];
    this.uploadAgentService
      .getFilterAgentList(
        data,
        this.page,
        this.selectedStateName,
        this.countyName,
        this.fCity,
        this.fZipcode,
        this.fnpn,
        this.licStatus,
        this.incStatus,
        this.fcompany,
        this.fName,
        this.lName,
        this.searchText1
      )
      .subscribe(
        data => {
          if (data) {
            this.listLoader = false
            let agentList = data.total;
            this.agentDataObj = data;
            this.totalElements = data.total;
            this.totalPages = Math.ceil(data.total / 50)
            this.agentDataList = this.agentDataObj['result'];
            this.hideafterFilter()
          } else {
            this.listLoader = false
            this.agentDataList = [];

          }
        },
        error => {
          this.listLoader = false
          this.hideafterFilter()
          this.showError(error.message)
        }
      );
  }
  resetFilterData() {
    this.clearFormData();
    this.getAgentList();
  }

  sortNpn(sort, name) {
    this.listLoader = true
    this.sortValue = sort;
    this.colname = name;
    if (sort == 'asc' && name == 'npn') {
      this.npnFlag = true;
    } else if (sort == 'desc' && name == 'npn') {
      this.npnFlag = false;
    } else if (sort == 'asc' && name == 'firstName') {
      this.nameFlag = true;
    } else if (sort == 'desc' && name == 'firstName') {
      this.nameFlag = false;
    } else if (sort == 'asc' && name == 'effDate') {
      this.dateFlag = true;
    } else if (sort == 'desc' && name == 'effDate') {
      this.dateFlag = false;
    } else if (sort == 'asc' && name == 'preCertified') {
      this.preCertFlag = true;
    } else if (sort == 'desc' && name == 'preCertified') {
      this.preCertFlag = false;
    } else if (sort == 'asc' && name == 'certified') {
      this.certFlag = true;
    } else if (sort == 'desc' && name == 'certified') {
      this.certFlag = false;
    }
    const data = [
      {
        colName: name,
        sortType: sort
      }
    ];
    this.sortData = data;
    this.uploadAgentService
      .getFilterAgentList(
        data,
        this.page,
        this.selectedStateName,
        this.countyName,
        this.fCity,
        this.fZipcode,
        this.fnpn,
        this.licStatus,
        this.incStatus,
        this.fcompany,
        this.fName,
        this.lName,
        this.searchText1
      )
      .subscribe(
        data => {
          if (data) {
            this.listLoader = false
            let agentList = data.total;
            this.agentDataObj = data;
            this.totalElements = data.total;
            this.totalPages = Math.ceil(data.total / 50)
            this.agentDataList = this.agentDataObj['result'];
          } else {
            this.agentDataList = [];
            this.listLoader = false
          }
        },
        error => {
          this.listLoader = false
        }
      );
  }

  getAgentList() {
    this.listLoader = true;
    this.uploadAgentService
      .getAgentAllAgentList(this.page, this.selectedStateName, this.countyName, this.fCity, this.fZipcode, this.fnpn, this.licStatus, this.incStatus, this.fcompany, this.fName, this.lName)
      .subscribe(
        data => {
          if (data) {
            this.listLoader = false
            let agentList = data.total;
            this.agentDataObj = data;
            this.totalElements = data.total;
            this.totalPages = Math.ceil(data.total / 50)
            this.agentDataList = this.agentDataObj['result'];
          } else {
            this.agentDataList = [];
            this.listLoader = false
          }
        },
        error => {
          this.listLoader = false
        }
      );
  }

  showNotification(statusCode, message) {
    switch (statusCode) {
      case 200:
        this.showSucccess(message);
        break;

      default:
        this.showError(message);
        break;
    }
  }
  showSucccess(message) {
    this.toastr.success(message);
  }
  showError(message) {
    this.toastr.error(message);
  }
  showWarning() {
    this.toastr.warning('You are being warned.', 'Alert!');
  }

  editAgent(npn, profileId) {
    this.listLoader = true
    this.uploadAgentService.getAgentById(npn).subscribe(
      data => {
        this.listLoader = false
        if (data.statusCode == 200) {

          this.open2(this.content);
          setTimeout(() => {
            this.submitAgentData = data.value;
            this.prefPhone = data.value.prefPhone;
            this.prefMobile = data.value.prefMobile;
            this.speaksSpanish = data.value.speaksSpanish;
            this.prefCompany = data.value.prefCompany;
            this.comments = data.value.comments;
            this.insuranceCompany = data.value.insuranceCompany
            this.editProfileId = profileId;
            this.carriers = data.value.carriers;
            this.agentData = data.value.carriers.length;
            this.validFlag = data.validToUpdateCarrier
            this.getSubsription(profileId);
            this.getSelectedItems();

          }, 500);
        }
        else if (data.statusCode == 400) {
          this.listLoader = false
          // this.showError(data.message);
        }
      },
      error => {
        this.showError(error.message);
        this.listLoader = false
      }
    );
  }
  submitAgent(language,funref) {
    this.modalLoader = true
    let lang ;
    let langKey;
    if (language == 'false') {
      lang = "US";
      langKey = 'en';
    }
    else if (language == 'true') {
      lang = "ES";
      langKey = 'es';
    }
    let id;
    let array = []
    for (let key in this.selectedItems) {
      id = this.selectedItems[key].id;
      array.push(id)
    }
  
    const data = {
      "preferredMobile": this.prefMobile,
      "phone": this.prefPhone,
      "langKey": langKey,
      "lang": lang,
      "prefCompany": this.prefCompany,
      "speaksSpanish": this.speaksSpanish,
      "comments": this.comments,
      "insuranceCompany": this.insuranceCompany,
      "carriers": array,
      "secondPhone": this.submitAgentData.secondPhone
    }
    console.log('dta',data)

    this.uploadAgentService.saveAgent(data, this.editProfileId)
      .subscribe(data => {
        this.modalLoader = false
        if (data.statusCode == 200) {
          this.showNotification(200, data.message)
          funref('Cross click');
        }
        else if (data.statusCode == 400) {
          this.showError(data.message);
          this.modalLoader = false
          
        }
      }, error => {
        this.showError(error.detail);
        this.modalLoader = false
      })
  }
  deleteAgent(npn) {
    this.uploadAgentService.deleteAgentById(parseInt(npn, 10)).subscribe(
      data => {
        if (data.statusCode == 200) {

        }
      },
      error => {
        this.showError(error.message);
      }
    );
  }

  open2(content) {
    this.modalService.open(content, { size: 'lg', backdrop: 'static' }).result.then(
      result => {
      },
      reason => {
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public updateForm(dataObj) {
    Object.keys(dataObj).forEach(dataKey => {
      if (this.uploadAgentForm.controls[dataKey]) {
        this.uploadAgentForm.controls[dataKey].setValue(dataObj[dataKey]);
      }
    });
  }
  initFormData() {
    this.uploadAgentForm = this.fbuilder.group({
      licNumber: [null, [Validators.required]],

      npn: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      middleName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      suffix: ['', [Validators.required]],
      businessName: ['', [Validators.required]],
      bAddr1: ['', [Validators.required]],
      bAddr2: ['', [Validators.required]],
      bAddr3: ['', [Validators.required]],
      bCity: ['', [Validators.required]],
      bState: ['', [Validators.required]],
      bCityName: ['', [Validators.required]],
      bStateName: ['', [Validators.required]],
      bZip: ['', [Validators.required]],
      bCountry: ['', [Validators.required]],
      bPhone: ['', [Validators.required]],
      bMobile: ['', [Validators.required]],
      bEmail: ['', [Validators.required]],
      licType: ['', [Validators.required]],
      lineAuth: ['', [Validators.required]],
      licStatus: ['', [Validators.required]],
      firstActiveDate: ['', [Validators.required]],
      effectiveDate: ['', [Validators.required]],
      expDate: ['', [Validators.required]],
      domState: ['', [Validators.required]],
      resState: ['', [Validators.required]],
      pAddr1: ['', [Validators.required]],
      pAddr2: ['', [Validators.required]],
      pAddr3: ['', [Validators.required]],
      pCity: ['', [Validators.required]],
      pCityName: ['', [Validators.required]],
      pStateName: ['', [Validators.required]],
      pCounty: ['', [Validators.required]],
      pState: ['', [Validators.required]],
      pZip: ['', [Validators.required]],
      pCountry: ['', [Validators.required]],
      isCECompliant: ['', [Validators.required]],
      speaksSpanish: ['', [Validators.required]],
      prefMobile: ['', [Validators.required]],
      prefPhone: ['', [Validators.required]],
      prefEmail: ['', [Validators.required]],
      profileId: ['', [Validators.required]],
      bCountyName: ['', [Validators.required]],
      pCountyName: ['', [Validators.required]],
      dStateName: ['', [Validators.required]],
    });
  }
  call(clickToCall, epbaxNo) {
    this.openLg(clickToCall);
    this.epbaxNo = epbaxNo;
  }
  openLg(clickToCall) {
    this.modalService.open(clickToCall, { size: 'sm' });
  }

  callToAgent(epbax) {
    this.subscriptionService.callingService(this.epbaxNo, this.phoneNo).subscribe(data => { }, error => { });
  }
  pageChanged(event) {

    this.page = event;
    // if(this.totalPages > this.page){
    this.getAgentList();
    // }
  }
  clearFormData() {
    this.countyName = '';
    this.fZipcode = '';
    this.fnpn = '';
    this.fCity = '';
    this.fName = '';
    this.lName = '';
    this.fcompany = '';
    this.incStatus = '';
    this.licStatus = '';
    this.selectedStateName = '';
  }

  getSubsription(profileId) {
    this.subscriptionService.getPackSubscription(profileId).subscribe(
      data => {
        this.subscribeData = [];
        this.subscribeData = data;
        if (this.subscribeData.length == 0) {
          this.noFlag = true;
        }
      },
      error => {
        this.noFlag = false;
      }
    );
  }
  downloadCSV(fileType) {
    this.downloader = true;
    this.mv1loader = true
    if (this.sortValue == '' && this.colname == '') {
      this.sortData = [];
    } else {
      this.sortNpn(this.sortValue, this.colname);
      this.sortData = [
        {
          colName: this.colname,
          sortType: this.sortValue
        }
      ];
    }
    this.uploadAgentService
      .downloadAgentCSV(
        fileType,
        this.sortData,
        this.selectedStateName,
        this.fCity,
        this.fZipcode,
        this.fnpn,
        this.licStatus,
        this.incStatus,
        this.fcompany,
        this.fName,
        this.lName,
        this.searchText1
      )
      .subscribe(
        data => {
          this.mv1loader = false

          if (fileType == 'CSV') {
            this.fileDownloadUtility.downloadCsv(data, 'agent');
            this.mv1loader = false
          }
          else if (fileType == 'XLS') {
            this.fileDownloadUtility.downloadagentXls(data);
            this.mv1loader = false
          }
        },
        error => {
          this.mv1loader = false
        }
      );
  }

  emptyCounty() {
    if (this.options == '') {
      this.countyName = ''
    }
  }
  emptyZip() {
    if (this.getZipData == '') {
      this.fZipcode = ''
    }
  }
  emptyCity() {
    if (this.cityData == '') {
      this.fCity = ''
    }
  }
  emptyFname() {
    if (this.fnameData == '') {
      this.fName = ''
    }
  }
  emptyLname() {
    if (this.lnameData == '') {
      this.lName = ''
    }
  }
  emptyNpn() {
    if (this.npnData == '') {
      this.fnpn = ''
    }
  }
  emptyCompany() {
    if (this.companyData == '') {
      this.fcompany = ''
    }
  }

  getListOfCarrier() {
    this.authUserService.getCarrierList().subscribe(
      data => {
        if (data.statusCode == 200) {
          this.dropdownList = data.value;
        }
        else {
        }
      },
      error => {
      }
    )
  }

  getSelectedItems() {
    this.selectedItems = []
    this.carriers.forEach((e1) => this.dropdownList.forEach((e2) => {
      let temobj = {
        id: Number,
        carrier: String
      }
      if (e1 === e2.id) {
        temobj.id = e1;
        temobj.carrier = e2.carrier;
        this.selectedItems.push(temobj)
      }
    }))
  }
  settings() {

    this.selectedItems = []
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'carrier',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true,
      enableCheckAll: false,
      limitSelection: 5,
      disabled: true
    };
  }
  openInfo() {
    Swal.fire({
      html:
        "<p>Adding 'additional carriers' to your profile will increase the per slot rates for Page1 and Page 2.</p>" +
        "<table class='table'><thead><tr><th>No</th><th>0</th><th>1-3</th><th>4-5</th></tr></thead><tbody><tr><td>Page1</td><td>$100/slot</td><td>$300/slot</td><td>$400/slot</td></tr><tr><td>Page 2</td><td>$50/slot</td><td>$150/slot</td><td>$200/slot</td></tr></tbody></table>"
    })

  }
  dropdownEvent() {
    // if (!this.validFlag) {
      // this.showPaperCheckWarning();
    // }
  }
  showPaperCheckWarning() {
    Swal.fire({
      position: 'top',
           html: "<p>The last update for additional companies was less than 90 day's ago kindly try later.</p>",
    })
      
  }


  getCompanyNameArray(e){ 

    if(this.insuranceCompany == ""){
      this.selectedCompanyName= true;
    }else{
      this.selectedCompanyName= false;
    }

    this.dataCompany =[];
    this.authUserService.getComp(this.insuranceCompany).subscribe(
      data=>{
        this.dataCompany = data.value;
      },
      error=>{

      }
    )
  }


  selectCompnayName(value){
    this.insuranceCompany = value.name;
    this.selectedCompanyId = value.id;
    localStorage.setItem('compId',this.selectedCompanyId);
    this.selectedCompanyName= true;
  }


// Purchased Subscription : total

  calculateTotal(){
    this.totalAmount = 0;
    for (let i in this.subscribeData) {
        // console.log(i,"cart data");
        
        let j = parseInt(i);
        if(this.agentData <= 0){
            if(this.subscribeData[j].rank == 'ONE'){
                this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 100);
            }else{
                this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 50);
            }
        }

        if(this.agentData <= 3 && this.agentData > 0){
            if(this.subscribeData[j].rank== 'ONE'){
                this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 300);
            }else{
                this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 150);
            }
        }

        if((this.agentData == 4 || this.agentData == 5) && this.agentData > 0){
            if(this.subscribeData[j].rank == 'ONE'){
                this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 400);
            }else{
                this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 200);
            }
        }
    }
}


}
