import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListAgentComponent } from './list-agent.component';

const routes: Routes = [
    {
        path: '',
        component: ListAgentComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ListAgentRoutingModule {}
