import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { SearchService } from 'app/service/search.service';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SubscriptionService } from '../../service/subscription.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthUserService } from '../../service/auth-user.service';
import {NgSelectModule, NgOption} from '@ng-select/ng-select';

type Language = 'en' | 'de';

declare var $: any;
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  public logoImage = 'assets/logo/logo.ico'
  public searchForm: FormGroup;
  public isSubmit = false;
  public searchList = [];
  public langFlag = true;
  public speInsData = [

  ];
  public agentNameData = [

  ];
  public setCompanyName;
  public setAgentName;
  public currentPage = 1;
  public totalElements = 0;
  public maxSize = 5;
  public pageSize = 10;
  public searchType = '';
  public epbaxNo;
  public phoneNo;
  public dynamicHeight;
  public agentColorFlag = false;
  public facilityColorFlag = false;
  public getProofColorFlag = false;
  public callingFlag = false;
  public isLoggedIn = false;
  public loginUser;
  public agefirstName;
  public agePhone;
  public ageZip;
  public ageEmail;
  public agentgreyFlag = false;
  public facgreyFlag = false;
  public getgreyFlag = false;
  public currentDate;
  public insCounty = '';
  public getInspected = false;
  public prfLastName;
  public prfComName;
  public prfZipcode;
  public nextFacFlag = false;
  public hideFac = false;
  public backFacFlag = false;
  public checkedAgent = false;
  public nextProofFlag = false;
  public hideProof = false;
  public backProofFlag = false;
  public currentUserType = ''
  public errorphoneFlag = false;
  public errorZip = false;
  public errorEmail = false;
  public errorvYear = false;
  public errorCounty = false;
  public agentErrorFlag = false;
  public vehicileTypeFlag = false;
  public lastNameFlag = false;
  public companyNameFlag = false;
  public exceedYear = false;
  public currentYear;
  public agentData;
  public mileageFlag = false;
  public emiisionFlag = false;
  public emissionFlag2 = false;
  public diseselFlag = false;
  public diseselFlag2 = false;
  public vFarFlag = false;
  public vFarFlag2 = false;
  public checkCountyFound = false;
  public checkEmissionMileage = false;
  public mileageyearFlag = false;
  public chooseFlag = true;
  public chooseFlag1 = true;
  public chooseFlag2 = true;
  public firstButtonFlag = true;
  public secondButtonFlag = false;
  public thirdButtonFlag = false;
  public fourthButtonFlag = false;
  public fifthButtonFlag = false;
  public chooseError = false;
  hideButtonFlag = false;
  checkCountyFlag = false;
  mileagebutton = false;
  checkMileageFlag = false;
  selectedCounty = false;
  dataArray = [];
  selectId;
  findButtonFlag = true;
  findButtonFlag1 = true;
  findButtonFlag2 = true;
  findButtonFlag3 = true;
  companydataArray = []
  selectDisabled = false
  selectedCompany = false;
  agentdataArray = [];
  selectedagentName = false;
  dataLastName=[];
  selectedLastName =false;
  selectedCompanyName =false
  dataCompany =[]
  selectedCompanyId;
  selectedAgentId;


  model = {
    zip: '',
    phone: '',
    type: '',
    email: '',
    vehicleType: '',
    vehicleYear: '',
    county: '',
    name: '',
    companyName: '',
    agentName: '',
    checked: false

  };
  faclitymodel = {
    name: '',
    phone: '',
    zip: '',
    email: '',
    type: '',
    vRegistration: '',
    vDiesel: '',
    vFarm: '',
    county: '',
    vehicleYear: '',
    mileage: ''
  };
  proofModel = {
    name: '',
    phone: '',
    email: '',
    lastname: '',
    company: '',
    zip: '',
    type: ''
  }
  public CountyArray =
    [{ id: true, name: 'alamance' },
    { id: true, name: 'buncombe' },
    { id: true, name: 'cabarrus' },
    { id: true, name: 'cumberland' },
    { id: true, name: 'davidson' },
    { id: true, name: 'durham' },
    { id: true, name: 'forsyth' },
    { id: true, name: 'franklin' },
    { id: true, name: 'gaston' },
    { id: true, name: 'guilford' },
    { id: true, name: 'iredell' },
    { id: true, name: 'johnston' },
    { id: true, name: 'lee' },
    { id: true, name: 'lincoln' },
    { id: true, name: 'mecklenburg' },
    { id: true, name: 'new hanover' },
    { id: true, name: 'onslow' },
    { id: true, name: 'randolph' },
    { id: true, name: 'rockingham' },
    { id: true, name: 'rowan' },
    { id: true, name: 'wake' },
    { id: true, name: 'union' },
    { id: false, name: 'nacy' },


    ]
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.dynamicHeight = window.innerHeight;
  }

  @ViewChild('content') content: NgbModal;
  constructor(
    public router: Router,
    public searchService: SearchService,
    public fbBuilder: FormBuilder,
    public toastr: ToastrService,
    public subscriptionService: SubscriptionService,
    private modalService: NgbModal,
    public translate: TranslateService,
    public authUserService: AuthUserService
  ) {
    this.getLanguage();
    this.initSearchForm();
    if (localStorage.getItem('token')) {
      this.isLoggedIn = true;
      this.loginUser = localStorage.getItem('firstName');
    } else {
      this.isLoggedIn = false;
    }
  }

  ngOnInit() {

    this.currentUserType = localStorage.getItem('type')
    this.currentDate = new Date();
    this.dynamicHeight = window.innerHeight;
    // localStorage.removeItem('agentString');
    this.getLang();
    let bFlag = localStorage.getItem('backFlag')
    switch (bFlag) {
      case 'true':
        this.getBackData();
        break;

      default:
        break;
    }
    localStorage.removeItem('backFlag')
  }

  initSearchForm() {
    this.searchForm = this.fbBuilder.group({
      zip: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      type: [null, [Validators.required]]
    });

    this.searchForm.valueChanges.subscribe(data => {
     
    });
  }

  searchFormSubmit(formData) {
    this.isSubmit = true;

    this.searchType = this.searchForm.value['type'];
    this.search();
  }

  search() {
    for (let key in this.searchForm.value) {
      this.phoneNo = this.searchForm.value.phone;
      if (this.searchForm.value.hasOwnProperty(key)) {
        this.searchForm.value[key] = '' + this.searchForm.value[key];
      }
    }
    this.searchService.searchList(this.searchForm.value, this.currentPage).subscribe(
      data => {
        this.totalElements = data['total'];
        this.searchList = data['result'];
        if (this.searchList != null) {
          if (this.searchList[0]) {
            this.searchList[0]['epbax'] = '1001';
          }
          if (this.searchList[1]) {
            this.searchList[1]['epbax'] = '1002';
          }
          if (this.searchList[2]) {
            this.searchList[2]['epbax'] = '1003';
          }
        }
     
        this.isSubmit = false;
        if (this.searchList == null) {
          this.isSubmit = false;
        }
      },
      error => {
        this.isSubmit = false;
      }
    );
  }

  swap(theArray, indexA, indexB) {
    let temp = new Object(theArray[indexA]);
    theArray = theArray.splice(indexA, 1);
    // theArray[indexA] = theArray[indexB];
    // theArray.push(temp);
    this.searchList.push(temp);
  
  }

  callNow(index, listObj, modal, epbaxNo) {
    this.swap(this.searchList, index, this.searchList.length - 1);
    if (this.searchType == 'AGENT') {
      this.epbaxNo = epbaxNo;
      this.callToAgent(listObj);
   
    } else {
      this.showSucccess(' Calling Facility ' + listObj['name']);
      this.openLg(modal);
      this.epbaxNo = epbaxNo;
    }
  }
  showFormError() {
   
  }

  pageChanged(event) {
   

    this.currentPage = event;
    switch (this.currentPage) {
      case 1:
        this.pageSize = 10;
        break;
      default:
        this.pageSize = 20;
        break;
    }
   
    this.search();
  }

  showNotification(statusCode, message) {
   
    switch (statusCode) {
      case 200:
        this.showSucccess(message);
       
        break;

      default:
        this.showError(message);
        break;
    }
  }
  showSucccess(message) {
    this.toastr.success(message);
  }
  showError(message) {
    this.toastr.error(message);
  }
  showWarning() {
    this.toastr.warning('You are being warned.', 'Alert!');
  }

  showInfo() {
    this.toastr.info('Just some information for you.');
  }
  callToAgent(listObj) {
    this.callingFlag = true;
    this.subscriptionService.callingService(this.epbaxNo, this.phoneNo).subscribe(
      data => {
        this.callingFlag = false;
        this.showSucccess(' Calling Agent ' + listObj['first_name'] + ' ' + listObj['last_name']);
      },
      error => {
        this.callingFlag = false;
      }
    );
  }
  openLg(clickToCall) {
    this.modalService.open(clickToCall, { size: 'sm' });
  }
  viewAgent() {
    window.scrollTo(0, 300)
    this.agentgreyFlag = false;
    this.facgreyFlag = true;
    this.getgreyFlag = true;
    this.agentColorFlag = true;
    this.facilityColorFlag = false;
    this.getProofColorFlag = false;
    this.errorphoneFlag = false;
    this.errorEmail = false;
    this.errorZip = false;
    this.agentErrorFlag = false;
    this.selectedCompany = true;
    this.selectedagentName = true
    this.clearForms();
    localStorage.removeItem('agentString')
  

  }

  viewFacility() {
    window.scrollTo(0, 300);
    this.facgreyFlag = false;
    this.agentgreyFlag = true;
    this.getgreyFlag = true;
    this.agentColorFlag = false;
    this.facilityColorFlag = true;
    this.getProofColorFlag = false;
    this.hideFac = false;
    this.errorphoneFlag = false;
    this.errorEmail = false;
    this.errorZip = false;
    this.agentErrorFlag = false;
    this.errorCounty = false;
    this.errorvYear = false;
    this.mileageFlag = false;
    this.selectedCounty = true

    this.clearForms();
    this.clearFacilityForms()
    localStorage.removeItem('agentString')
    this.clearFacilty()

 


  }
  getProof() {
    window.scrollTo(0, 300);
    this.facgreyFlag = true;
    this.agentgreyFlag = true;
    this.getgreyFlag = false;
    this.agentColorFlag = false;
    this.facilityColorFlag = false;
    this.getProofColorFlag = true;
    this.hideProof = false;
    this.errorphoneFlag = false;
    this.errorEmail = false;
    this.errorZip = false;
    this.lastNameFlag = false;
    this.companyNameFlag = false;
    this.agentErrorFlag = false;
    this.selectedCompanyName = true
    this.selectedLastName =true
    this.clearProofForms();
    localStorage.removeItem('agentString')

   
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['/home']);
    window.location.reload();
  }
  checkName(value) {
    if (value == '') {
      this.agentErrorFlag = true;
      this.findButtonFlag = false
    }
    else {
      this.agentErrorFlag = false;
      this.findButtonFlag = true
    }
  }
  checklName(value) {
    if (value == '') {
      this.lastNameFlag = true;
    }
    else {
      this.lastNameFlag = false;
    }
  }
  checkCompanyName(value) {
    if (value == '') {
      this.companyNameFlag = true;
    }
    else {
      this.companyNameFlag = false;
    }
  }


  findAgent(type) {

    if (this.model.name == '')  {
      this.agentErrorFlag = true;
    } 
     else if (this.model.phone == '' || this.model.phone.length < 10) {
      this.errorphoneFlag = true;
    }
    else if ((this.model.agentName == '' ) && (this.model.zip == '' || this.model.zip.length < 5)) {
      this.errorZip = true;
    }
    else if (this.model.checked == false && this.model.companyName == '' && this.model.agentName == '') {
      this.chooseError = true
    }
    else {
      this.router.navigate(['/searchlist']);
      this.model.type = type;
      localStorage.setItem('agentString', JSON.stringify(this.model));
      localStorage.setItem('callType','agent' );
    }
  }

  checkEmailVa(value) {
    if (value != '') {
      let vaildEmail = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
      if (vaildEmail.test(value)) {
        this.errorEmail = false;
        return true
      }
      else {
        this.errorEmail = true;
        return false
      }
    }
  }
  clearForms() {
    this.model.email = '';
    this.model.name = '';
    this.model.phone = '';
    this.model.type = '';
    this.model.zip = '';
  }
  clearFacilityForms() {
    this.faclitymodel.email = '';
    this.faclitymodel.name = '';
    this.faclitymodel.phone = '';
    this.faclitymodel.type = '';
    this.faclitymodel.zip = '';
  }
  clearProofForms() {
    this.proofModel.email = '';
    this.proofModel.name = '';
    this.proofModel.phone = '';
    this.proofModel.type = '';
    this.proofModel.zip = '';
    this.proofModel.lastname = '';
    this.proofModel.company = '';
  }
  findFacility() {
   
    if (this.faclitymodel.name == '') {
      this.agentErrorFlag = true;
    }
    if (this.faclitymodel.phone == '' || this.faclitymodel.phone.length < 10) {
      this.errorphoneFlag = true;
    }
    if (this.faclitymodel.zip == '' || this.faclitymodel.zip.length < 5) {
      this.errorZip = true;
    }

    else {
      this.nextFac();
    }
  }

  openModal(modal) {
    this.modalService.open(modal, { size: 'sm' });
  }




  findProof() {
    if (this.proofModel.name == '') {
      this.agentErrorFlag = true;
    }
    if (this.proofModel.phone == '' || this.proofModel.phone.length < 10) {
      this.errorphoneFlag = true;
    }
    if (this.proofModel.email == '') {
      this.errorEmail = true;
    }
    else {
      this.nextProof();
    }
  }
  searchProof(type) {
    if (this.proofModel.lastname != '') {
      this.proofModel.type = type
      this.router.navigate(['/searchlist']);
      localStorage.setItem('callType','insurance' );
      localStorage.setItem('agentString', JSON.stringify(this.proofModel));
    }
    else if (this.proofModel.company != '') {
      this.proofModel.type = type
      this.router.navigate(['/searchlist']);
      localStorage.setItem('callType','insurance' );
      localStorage.setItem('agentString', JSON.stringify(this.proofModel));
    }
    else if (this.proofModel.zip != '') {
      this.proofModel.type = type
      this.router.navigate(['/searchlist']);
      localStorage.setItem('callType','insurance' );
      localStorage.setItem('agentString', JSON.stringify(this.proofModel));
    }
    else {
      this.lastNameFlag = true;
    }
  }
  searchFacility(type) {
    this.faclitymodel.type = type
    localStorage.setItem('agentString', JSON.stringify(this.faclitymodel));
    localStorage.setItem('callType','facility' );
    this.router.navigate(['/searchlist']);
  }
  nextFac() {
    this.nextFacFlag = true
    setTimeout(() => {
      this.nextFacFlag = false;
      this.hideFac = true
    }, 200)
  }
  backFac() {
    this.backFacFlag = true
    setTimeout(() => {
      this.backFacFlag = false;
      this.hideFac = false
    }, 200)
  }

  nextProof() {
    this.nextProofFlag = true
    setTimeout(() => {
      this.nextProofFlag = false;
      this.hideProof = true
    }, 200)
  }
  backProof() {
    this.backProofFlag = true
    setTimeout(() => {
      this.backProofFlag = false;
      this.hideProof = false
    }, 200)
  }

  navigateProfile() {
    switch (this.currentUserType) {
      case 'AGENT':
        if (this.currentUserType == 'AGENT') {
          this.router.navigate(['/agent-profile/viewProfile']);
        }
        break;
      case 'FACILITY':
        if (this.currentUserType == 'FACILITY') {
          this.router.navigate(['/facility-profile/viewFacility']);
        }

        break;
      case 'ADMIN':
        if (this.currentUserType == 'ADMIN') {
          this.router.navigate(['/user/agent/listAgent']);
        }

        break;
      default:
        this.router.navigate(['/home']);
        break;
    }
  }

  checkValidation(value) {
    if (value == '' || value.length < 10) {
      this.errorphoneFlag = true;
      this.findButtonFlag = false;
    }
    else {
      this.errorphoneFlag = false
      this.findButtonFlag = true
    }
  }
  checkZip(value) {
    if (value == '' || value.length < 5) {
      this.errorZip = true;
      this.findButtonFlag = false;
    }
    else {
      this.errorZip = false;
      this.findButtonFlag = true
    }
  }
  checkEmail(value) {
    if (value == '') {
      this.errorEmail = true;
    }
    else {
      this.errorEmail = false;
    }
  }
  checkvYear(value) {
    let currentYear = new Date();
    this.currentYear = currentYear.getFullYear() + 1;
    
    if (value == '' || value.length < 4) {
      this.errorvYear = true;
    }
    else if (this.currentYear < value) {
      this.exceedYear = true;
    }
    else {
      this.errorvYear = false;
      this.exceedYear = false;
    }
  }
  
  checkMileage(value) {
    let enteryear = parseInt(this.faclitymodel.vehicleYear)
  
    if (value != '') {
      if ((this.currentYear - enteryear) <= 2 && value <= 70000) {
      
        this.checkEmissionMileage = true;
        this.hideButtonFlag = true;
        this.mileageyearFlag = true;
        this.mileagebutton = true;
        this.emiisionFlag = false;
        this.checkMileageFlag = false;
        this.fifthButtonFlag = false;
      }
      else {
       
        this.checkEmissionMileage = false;
        
        this.diseselFlag2 = false;
        this.hideButtonFlag = true;
        this.mileageyearFlag = false;
        this.mileagebutton = true;
        this.emiisionFlag = false;
        this.checkMileageFlag = true;
        this.fifthButtonFlag = false;

      }
    }
  }
  checkCounty() {
    let county = this.faclitymodel.county.toLowerCase();
    for (let key in this.CountyArray) {
      if (this.CountyArray[key].id != this.selectId) {
        this.checkCountyFound = true;
        this.checkCountyFlag = false;
        this.hideButtonFlag = false;
        this.mileagebutton = false;
        this.fifthButtonFlag = true;
        this.fourthButtonFlag = false;

      }
      else {
        this.checkCountyFound = false;
        this.hideButtonFlag = true;
        this.checkCountyFlag = true;
        this.mileagebutton = true;
        this.mileageFlag = false;
        this.exceedYear = false;
        this.errorvYear = false;
        this.checkMileageFlag = false;
        this.fifthButtonFlag = false;
        this.fourthButtonFlag = false;
      }
    }

  }
  getLanguage() {
    
    this.translate.setDefaultLang('en');
   
    
  }
  switchLanguage(lang) {
    localStorage.setItem('lang', lang)
    this.translate.use(lang);
    if (lang == 'en') {
      this.langFlag = true;
    }
    else if (lang == 'es') {
      this.langFlag = false;
    }
  }
  getLang() {
    let lang = localStorage.getItem('lang')
    switch (lang) {
      case 'en':
        this.langFlag = true;
        break;
      case 'es':
        this.langFlag = false;
        break;

      default:
        break;
    }
  }
  getBackData() {
    this.agentData = JSON.parse(localStorage.getItem('agentString'));
  
    let type = this.agentData.type
    switch (type) {
      case 'AGENT':
      
    
        this.agentColorFlag = true;
        this.model = this.agentData;
        this.getList();

        break;
      case 'FACILITY':
    
        this.facilityColorFlag = true;
        this.hideFac = true
        this.faclitymodel = this.agentData;
        break;
      case 'INSURANCE':
       
        this.getProofColorFlag = true;
        this.hideProof = true;
        this.proofModel = this.agentData;
        break;

      default:
        break;
    }
  }
  nextButton() {
    if (this.faclitymodel.vRegistration != '') {
      if (this.faclitymodel.vRegistration == 'yes') {
        this.emiisionFlag = false;
        this.emissionFlag2 = true;
        this.hideButtonFlag = false;
        this.diseselFlag2 = false;
        this.vFarFlag2 = false;
        this.faclitymodel.vDiesel = '';
        this.faclitymodel.county = '';
        this.checkCountyFlag = false;
        this.checkEmissionMileage = false;
        this.checkMileageFlag = false;
        this.mileageyearFlag = false;
        this.firstButtonFlag = false;
        this.secondButtonFlag = true;
        this.thirdButtonFlag = false;
        this.fourthButtonFlag = false;
        this.fifthButtonFlag = false;
      }
      else if (this.faclitymodel.vRegistration == 'no') {
        this.emiisionFlag = true;
        this.emissionFlag2 = false;
        this.checkCountyFound = false
        this.vFarFlag = false
        this.diseselFlag = false;
        this.hideButtonFlag = true;
        this.diseselFlag2 = false;
        this.vFarFlag2 = false;
        this.faclitymodel.vDiesel = '';
        this.faclitymodel.county = '';
        this.checkCountyFlag = false;
        this.checkEmissionMileage = false;
        this.checkMileageFlag = false;
        this.mileageyearFlag = false;
        this.firstButtonFlag = false;

      }
    }


  }
  secondButton() {
    if (this.faclitymodel.vDiesel != '') {
      if (this.faclitymodel.vDiesel == 'no') {
        this.diseselFlag = true;
        this.diseselFlag2 = false;
        this.checkCountyFound = false;
        this.hideButtonFlag = false;
        this.faclitymodel.vFarm = '';
        this.faclitymodel.county = '';
        this.firstButtonFlag = false;
        this.secondButtonFlag = false;
        this.thirdButtonFlag = true;
        this.fourthButtonFlag = false;
        this.fifthButtonFlag = false;
      }
      else if (this.faclitymodel.vDiesel == 'yes') {
        this.diseselFlag = false;
        this.diseselFlag2 = true;
        this.vFarFlag = false;
        this.checkCountyFound = false;
        this.hideButtonFlag = true;
        this.faclitymodel.vFarm = '';
        this.faclitymodel.county = '';
        this.secondButtonFlag = false;

      }
    }
  }
  thirdButton() {
    if (this.faclitymodel.vFarm != '') {
      if (this.faclitymodel.vFarm == 'no') {
        this.vFarFlag = true;
        this.vFarFlag2 = false;
        this.hideButtonFlag = false;
        this.faclitymodel.county = '';
        this.faclitymodel.vehicleYear = '';
        this.faclitymodel.mileage = '';
        this.firstButtonFlag = false;
        this.secondButtonFlag = false;
        this.thirdButtonFlag = false;
        this.fourthButtonFlag = true;
        this.fifthButtonFlag = false;
      }
      else if (this.faclitymodel.vFarm == 'yes') {
        this.vFarFlag = false;
        this.vFarFlag2 = true;
        this.hideButtonFlag = true;
        this.checkCountyFound = false
        this.checkMileageFlag = false;
        this.emiisionFlag = false;
        this.thirdButtonFlag = false
      }

    }

  }
  fourthButton() {
    if (this.faclitymodel.county != '') {
      this.checkCounty();
    }
  }

  backButton() {
    this.faclitymodel.vRegistration = '';
    this.emissionFlag2 = false;
    this.secondButtonFlag = false;
    this.firstButtonFlag = true;
  }
  backthirdButton() {
    this.faclitymodel.vFarm = '';
    this.diseselFlag = false;
    this.thirdButtonFlag = false;
    this.secondButtonFlag = true;
    this.firstButtonFlag = false;
  }
  backFourthButton() {
    this.faclitymodel.county = '';
    this.vFarFlag = false;
    this.thirdButtonFlag = true;
    this.firstButtonFlag = false;
    this.secondButtonFlag = false;
    this.fourthButtonFlag = false;
  }
  backFifthButton() {
    this.fourthButtonFlag = true;
    this.firstButtonFlag = false;
    this.secondButtonFlag = false;
    this.thirdButtonFlag = false;
    this.fifthButtonFlag = false;
    this.checkCountyFound = false;
    this.mileageFlag = false;
    this.exceedYear = false
    this.errorvYear = false;

  }
  checkEmission() {

    if (this.faclitymodel.vRegistration == 'yes') {
      this.firstButtonFlag = true;
     

    }
    else if (this.faclitymodel.vRegistration == 'no') {
     
    }
  }
  checkDiesel() {
    if (this.faclitymodel.vDiesel == 'yes') {
     

    }
    else if (this.faclitymodel.vDiesel == 'no') {
    
    }
  }
  checkFarm() {
    if (this.faclitymodel.vFarm == 'yes') {
    
    }
    else if (this.faclitymodel.vFarm == 'no') {
    

    }
  }

  navToPrivacy() {
    this.router.navigate(['privacy-policy'])
  }
  checkAgent(event) {
    this.checkedAgent = event.target.checked;
    if (this.checkedAgent == true) {
      this.model.companyName = '';
      this.model.agentName = '';
      this.findButtonFlag1 = true;
      this.findButtonFlag2 = false;
      this.findButtonFlag3 = false;
    }
    else {
     
      this.findButtonFlag2 = true;
      this.findButtonFlag3 = true;
    }
    
  }
  getList() {
    this.model.companyName = ''
    this.model.agentName = ''
    this.getCompanyList();
    this.getAgentList();
  }
  selectCompany(event) {
    this.model.companyName = event.target.value;
    if (this.model.companyName != '') {
      this.findButtonFlag2 = true;
      this.findButtonFlag1 = false;
      this.findButtonFlag3 = false;
      this.model.checked = false
      this.model.agentName = '';
      
    }
    else {

      this.findButtonFlag1 = true;
      this.findButtonFlag3 = true;
      
    }
 
  }
  selectAgent(event) {
    this.model.agentName = event.target.value;
    if (this.model.agentName != '') {
      this.findButtonFlag3 = true;
      this.findButtonFlag1 = false;
      this.findButtonFlag2 = false;
      this.model.companyName = '';
      this.model.checked = false
    }
    else {

      this.findButtonFlag1 = true;
      this.findButtonFlag2 = true;
    }
    
  }
  getCompanyList() {
    if(this.model.zip != ''){
        this.authUserService.getCompanyList(this.model.zip).subscribe(
            data => {
              this.speInsData = data.value;
              
            },
            error => {
      
            }
          )
    }
  }
  getAgentList() {
    if(this.model.zip != ''){
      this.authUserService.getAgentList(this.model.zip).subscribe(
        data => {
          this.agentNameData = data.value;
          
        },
        error => {
  
        }
      )
    }
  }
  firstOption(event) {
    let type = event.target.value;
    switch (type) {
      case 'first':
        this.chooseFlag = false;
        this.chooseFlag1 = true
        this.chooseFlag2 = true
        this.model.companyName = '';
        this.model.agentName = ''
        break;
      case 'second':
        this.chooseFlag1 = false
        this.chooseFlag = true;
        this.chooseFlag2 = true
        this.model.checked = false;
        this.model.agentName = ''
        break;
      case 'third':
        this.chooseFlag1 = true
        this.chooseFlag2 = false;
        this.chooseFlag = true;
        this.model.checked = false;
        this.model.companyName = ''
        break;

      default:
        this.chooseFlag = true;
        this.chooseFlag1 = true;
        this.chooseFlag2 = true
        this.model.checked = false;
        this.model.companyName = ''
        break;
    }
  }
  clearFacilty() {
    this.firstButtonFlag = true;
    this.secondButtonFlag = false;
    this.thirdButtonFlag = false;
    this.fourthButtonFlag = false;
    this.fifthButtonFlag = false;
    this.emissionFlag2 = false;
    this.diseselFlag = false;
    this.vFarFlag = false;
    this.checkCountyFound = false;
    this.checkCountyFound = false;
    this.faclitymodel.mileage = '';
    this.faclitymodel.vRegistration = '';
    this.faclitymodel.vDiesel = '';
    this.faclitymodel.vFarm = ''
    this.faclitymodel.vehicleYear = ''
  }
  getCountyList(event) {

    if(this.faclitymodel.county == ""){
      this.selectedCounty = true
    }
    else{
      this.selectedCounty = false
    }
   

    this.dataArray = []

    let str = event.target.value.toLowerCase();
    
    for (let key in this.CountyArray) {
      if (this.CountyArray[key].name.includes(str)) {
        this.dataArray.push(this.CountyArray[key]);
        
      }
      else {
      }
    }


  }
  selectCounty(value, id) {
    this.faclitymodel.county = value;
    this.selectedCounty = true;
    this.selectId = id;
  }
  terms() {
    this.router.navigate(['terms-conditions'])
  }

  getCompList() {

    if(this.model.companyName == ""){
      this.selectedCompany = true;
      this.companydataArray = [];
    }else{
      this.selectedCompany = false;
    }
    
    this.companydataArray = [];
    this.authUserService.getComList(this.model.zip,this.model.companyName).subscribe(
      data=>{
        this.companydataArray = data.value;
      },
      error=>{

      }
    )
  }
  getAgeList() {
    this.selectedagentName = false;
    if(this.model.agentName == ""){
      this.selectedagentName = true;
      this.agentdataArray =[
      ]
    }
    this.agentdataArray =[
    ]
    this.authUserService.getAgeList(this.model.zip,this.model.agentName).subscribe(
      data=>{
        this.agentdataArray = data.value;
      },
      error=>{

      }
    )
  }
  getLastNameList(){
    if(this.proofModel.lastname == ""){
      this.selectedLastName= true;
    }else{
      this.selectedLastName= false;
    }
    
    this.dataLastName =[];
    this.authUserService.getLastName(this.proofModel.lastname).subscribe(
      data=>{
        this.dataLastName = data.value;
      },
      error=>{

      }
    )
  }
  getCompanyNameArray(){
    if(this.proofModel.company == ""){
      this.selectedCompanyName= true;
    }else{
      this.selectedCompanyName= false;
    }
   
    this.dataCompany =[];
    this.authUserService.getComp(this.proofModel.company).subscribe(
      data=>{
        this.dataCompany = data.value;
      },
      error=>{

      }
    )
  }
  selectComapny(value) {
    this.model.companyName = value;
    this.selectedCompany = true;
    if (this.model.companyName != '') {
      this.findButtonFlag2 = true;
      this.findButtonFlag1 = false;
      this.findButtonFlag3 = false;
      this.model.checked = false
      this.model.agentName = '';
    }
    else {

      this.findButtonFlag1 = true;
      this.findButtonFlag3 = true;
    }
  }
  selectAgentName(value) {
    this.model.agentName = value;
    this.selectedagentName = true;
    if (this.model.agentName != '') {
      this.findButtonFlag3 = true;
      this.findButtonFlag1 = false;
      this.findButtonFlag2 = false;
      this.model.companyName = '';
      this.model.checked = false
    }
    else {
      this.findButtonFlag1 = true;
      this.findButtonFlag2 = true;
    }
  }
  selectLastName(value) {
    this.proofModel.lastname = value.name;
    this.selectedAgentId =value.id;
    localStorage.setItem('agentId',this.selectedAgentId);
    this.selectedLastName= true;
  }
  selectCompnayName(value){
    this.proofModel.company = value.name;
    this.selectedCompanyId = value.id;
    localStorage.setItem('compId',this.selectedCompanyId);
    this.selectedCompanyName= true;
  }

  openHelpPopup(){
    this.open2(this.content);
  }


  open2(content) {
    this.modalService.open(content, { size: 'lg', backdrop: 'static' }).result.then(
      result => {
        
      },
      reason => {
       
      }
    );
  }
}
