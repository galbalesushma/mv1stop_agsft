import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResetPasswordComponent } from './reset-password.component'
import { resetPasswordRoutingModule } from './reset-password-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,resetPasswordRoutingModule,FormsModule
  ],
  declarations: [ResetPasswordComponent]
})
export class ResetPasswordModule { }
