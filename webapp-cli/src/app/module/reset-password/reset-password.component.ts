import { Component, OnInit } from '@angular/core';
import { AuthUserService } from 'app/service/auth-user.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  public model = {
    key: '',
    newPassword: ''
  };
  public password = '';
  public errorFlag = false;
  constructor(public router: Router, public activatedRoute: ActivatedRoute, public resetPasswordService: AuthUserService, public toastr: ToastrService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      const key = params['key'] || 'NONE';

      if (key == 'NONE') {
      } else {
        this.model.key = key;
       
      }
    });
  }

  submit() {
    this.resetPasswordService.resetUserPassword(this.model)
      .subscribe(data => {
        if (data.statusCode == 200) {
          this.showNotification(200, data.message);
          this.router.navigate([''])
        }
        else if (data.statusCode == 400) {
          this.showError(data.message)
        }
      }
        , error => {

        })
  }
  matchPass(){
    if(this.password != this.model.newPassword){
       this.errorFlag = true;
    }
    else{
      this.errorFlag = false;
    }
  }

  showNotification(statusCode, message) {
    switch (statusCode) {
      case 200:
        this.toastr.success(message);
        break;

      default:
        this.toastr.error(message);
        break;
    }
  }

  showWarning(message) {
    this.toastr.warning(message);
  }

  showError(message) {
    this.toastr.error(message);
  }

  showInfo(message) {
    this.toastr.info(message);
  }
}
