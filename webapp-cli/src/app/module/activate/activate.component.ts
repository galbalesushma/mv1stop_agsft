import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthUserService } from 'app/service/auth-user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-activate',
    templateUrl: './activate.component.html',
    styleUrls: ['./activate.component.css']
})
export class ActivateComponent implements OnInit {
    isActivated = 'pending';
    constructor(public activatedRoute: ActivatedRoute, public activatUSerService: AuthUserService, public toastr: ToastrService) {}

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(params => {
            
            const key = params['key'] || 'NONE';

            if (key == 'NONE') {
                
            } else {
                

                this.activatUSerService.activateUser(key).subscribe(
                    data => {
                       

                        if (data['statusCode'] == 200) {
                            this.showNotification(200, data['message']);
                            this.isActivated = 'success';
                        } else {
                            this.showNotification(400, data['message']);
                            this.isActivated = 'fail';
                        }
                    },
                    error => {
                       
                        
                        this.showError(error.message);
                    }
                );
            }
        });
    }

    showNotification(statusCode, message) {
        switch (statusCode) {
            case 200:
                this.toastr.success(message);
                break;

            default:
                this.toastr.error(message);
                break;
        }
    }

    showWarning(message) {
        this.toastr.warning(message);
    }

    showError(message) {
        this.toastr.error(message);
    }

    showInfo(message) {
        this.toastr.info(message);
    }
}
