import { Component, OnInit, ViewChild } from '@angular/core';
import {Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  public isLoggedIn = false;
  public loginUser;
  public logoImage = 'assets/logo/logo.ico'
  @ViewChild('content') content: NgbModal;

  constructor(public router:Router,private modalService: NgbModal) { }

  ngOnInit() {
    if (localStorage.getItem('token')) {
      this.isLoggedIn = true;
      this.loginUser = localStorage.getItem('firstName');
    } else {
      this.isLoggedIn = false;
    }
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['/home']);
    window.location.reload();
  }
  terms() {
    this.router.navigate(['terms-conditions'])
  }
  openHelpPopup(){
    this.open2(this.content);
  }
  open2(content) {
    this.modalService.open(content, { size: 'lg', backdrop: 'static' }).result.then(
      result => {
        
      },
      reason => {
       
      }
    );
  }
  

}
