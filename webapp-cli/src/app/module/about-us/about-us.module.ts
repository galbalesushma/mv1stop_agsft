import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutUsRountingModule } from './about-us-routing.module';
import { AboutUsComponent } from './about-us.component';
@NgModule({
  imports: [
    CommonModule,
    AboutUsRountingModule
  ],
  declarations: [AboutUsComponent]
})
export class AboutUsModule { }
