import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LoginUserModel } from 'app/shared/model/login.model';
import { AuthUserService } from 'app/service/auth-user.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
    public userLogin: LoginUserModel;
    public currentUserType = '';
    public loading = true;
    public errorMsg;
    public resetEmail = '';
    public hideResetForm =false;
    public loginFormFlag = true;
    constructor(
        public loginService: AuthUserService,
        public toastr: ToastrService,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.userLogin = new LoginUserModel();

        this.activateRoute.params.subscribe(routeParam => {
           
            if (routeParam.hasOwnProperty('type')) {
                this.currentUserType = routeParam['type'];

                switch (this.currentUserType) {
                    case 'agent-profile':
                        break;
                    case 'facility-profile':
                        break;
                    case 'user':
                        break;
                    default:
                        this.router.navigate(['/home']);
                        break;
                }
            }
        });
    }

    ngOnInit() {
          this.hideResetForm = false;
          this.loginFormFlag = true;
    }

    loginSubmit() {
       
        this.loading = false;
        this.loginService.loginUser(this.userLogin).subscribe(
            data => {
               

                if (data.statusCode == 200) {
                    this.loading = true;
                    localStorage.setItem('userName', this.userLogin.username);
                    localStorage.setItem('token', data.value['id_token']);
                    this.getProfileInfo();
                } else if (data.statusCode == 400) {
                    this.showError(data.message);
                    this.loading = true;
                }
            },
            error => {
                if (error.status == 401) {
                    this.errorMsg = 'Bad Credentials';
                    this.showError(this.errorMsg);
                }
                this.loading = true;
               
            }
        );
    }

    getProfileInfo() {
        const userName = localStorage.getItem('userName');
        this.loginService.getProfile(userName).subscribe(
            data => {
                

                if (data['statusCode'] == 200) {
                    let value = data['value'];
                    localStorage.setItem('firstName', value['firstName']);
                    localStorage.setItem('profileId', value['profileId']);
                    localStorage.setItem('firstName', value['firstName']);
                    localStorage.setItem('type', value['type']);
                    localStorage.setItem('userId', value['userId']);
                }
                let type = localStorage.getItem('type');
                switch (this.currentUserType) {
                    case 'agent-profile':
                        if (type == 'AGENT') {
                            this.router.navigate(['/agent-profile/viewProfile']);
                        } else {
                            this.navigateTo(type);
                        }
                        break;
                    case 'facility-profile':
                        if (type == 'FACILITY') {
                            this.router.navigate(['/facility-profile/viewFacility']);
                        } else {
                            this.navigateTo(type);
                        }

                        break;
                    case 'user':
                        if (type == 'ADMIN') {
                            this.router.navigate(['/user/agent/listAgent']);
                        } else {
                            this.navigateTo(type);
                        }

                        break;
                    default:
                        this.router.navigate(['/home']);
                        break;
                }
            },
            error => {
               
                // this.showError(error.message);
            }
        );
    }

    navigateTo(type) {
        switch (type) {
            case 'AGENT':
                this.router.navigate(['/agent-profile/viewProfile']);
                break;
            case 'ADMIN':
                this.router.navigate(['/user/agent/listAgent']);
                break;
            case 'FACILITY':
                this.router.navigate(['/facility-profile/viewFacility']);
                break;
        }
    }

    ngAfterViewInit() {
        $(function() {
            $('.preloader').fadeOut();
        });
        $('#to-recover').on('click', function() {
            $('#loginform').slideUp();
            $('#recoverform').fadeIn();
        });
    }

    showNotification(statusCode, message) {
       
        switch (statusCode) {
            case 200:
                this.toastr.success(message);
                
                break;

            default:
                this.toastr.error(message);
                break;
        }
    }

    showWarning(message) {
        this.toastr.warning(message);
    }

    showError(message) {
        this.toastr.error(message);
    }

    showInfo(message) {
        this.toastr.info(message);
    }
    forgotPassword(){
      this.loginService.resetPassword(this.resetEmail)
      .subscribe(data=>{

        if(data.statusCode == 200){
          this.showNotification(200,data.message);
          this.hideResetForm = false;
          this.loginFormFlag = true;
          this.resetEmail = '';
        }
        else if(data.statusCode ==400){
          this.hideResetForm = true;
          this.loginFormFlag = false;
          this.showError(data.message);
        }

      },
      error=>{
        this.showError(error.message);

      })
    }
    recover(){
      this.hideResetForm = true
      this.loginFormFlag = false;
    }
}
