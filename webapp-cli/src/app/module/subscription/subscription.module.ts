import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubscriptionRoutingModule } from './subscription-routing.module';
import { SubscriptionComponent } from './subscription.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, SubscriptionRoutingModule, ReactiveFormsModule, FormsModule, NgbModule],
    declarations: [SubscriptionComponent]
})
export class SubscriptionModule {}
