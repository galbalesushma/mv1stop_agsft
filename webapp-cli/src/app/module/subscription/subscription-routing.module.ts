import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubscriptionComponent } from './subscription.component';
const routes: Routes = [
    {
        path: '',
        component: SubscriptionComponent,
        children: [
            { path: '', redirectTo: 'pricing', pathMatch: 'full' },
            {
                path: 'pricing',
                loadChildren: './pricing/pricing.module#PricingModule'
            },
            {
                path: 'cart',
                loadChildren: './cart/cart.module#CartModule'
            },
            {
                path: 'payment',
                loadChildren: './payment/payment.module#PaymentModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SubscriptionRoutingModule {}
