import { Component, OnInit } from '@angular/core';
import { SubscriptionService } from 'app/service/subscription.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { SUBSCRIPTION } from 'app/shared/store/storeManager';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SubscriptionModel, PackageModel } from '../../../shared/model/subscription.model';
@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
    public cartInfo;
    public zipAreaId;
    public zipCode;
    public duration;
    public slots;
    public packageId;
    public rank;
    public sampleObj;
    public total;
    public cartId;
    public subsId;
    public resubsId;
    public recartId;
    public repackageId;
    public reslots;
    public reduration;
    public rezipAreaId;
    public currentUserType;
    public placeOrderData;
    public itemQuantity;
    public tableData;
    public profileId;
    public zipId;
    public slotList: any;
    public slot;
    public editZipcode;
    public key = {
        newSlots: 0
    };
    public editModalData = [];
    public cartData;
    public cartEmptyFlag = false;
    public subcriptionObservable: Observable<any>;
    public storeSubscription: Subscription;
    constructor(
        public cartService: SubscriptionService,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private router: Router,
        public store: Store<any>,
        private modalService: NgbModal
    ) {
        this.subcriptionObservable = store.pipe(select('subscriptionDataStore'));
    }

    ngOnInit() {
        this.getCartInfo();

        this.storeSubscription = this.subcriptionObservable.subscribe(data => {
         

            if (data.hasOwnProperty('userType')) {
                this.currentUserType = data['userType'];
            }
        });
    }

    getCartInfo() {
        const profileId = localStorage.getItem('profileId');
        this.cartService.getCartInfo(profileId).subscribe(
            data => {
              
                if (data.statusCode == 200) {
                    this.placeOrderData = data;
                    this.cartInfo = data.value.items;
                    this.total = data.value.total;
                    this.itemQuantity = data.value.items.length;
                    for (let i in this.cartInfo) {
                        this.zipAreaId = this.cartInfo[i].zipAreaId;
                        this.duration = this.cartInfo[i].duration;
                        this.slots = this.cartInfo[i].slots;
                        this.packageId = this.cartInfo[i].packageId;
                        this.cartId = this.cartInfo[i].cartId;
                        this.subsId = this.cartInfo[i].subsId;
                    }
                    if (this.cartInfo.length == 0) {
                        this.cartEmptyFlag = true;
                    }
                    this.dispatchData({
                        cartInfo: this.placeOrderData
                    });

                    this.getZipById();
                    this.getRankByPackage();
                    setTimeout(() => {
                        this.getSampleObj();
                    }, 500);
                } else if (data.statusCode == 400) {
             
                }
            },
            error => {
                
            }
        );
    }

    navigateToCart() {
        this.router.navigate(['../../pricing'], { relativeTo: this.route });
    }

    showNotification(statusCode, message) {
        switch (statusCode) {
            case 200:
                this.toastr.success(message);
                break;

            default:
                this.toastr.error(message);
                break;
        }
    }

    showWarning(message) {
        this.toastr.warning(message);
    }

    showError(message) {
        this.toastr.error(message);
    }

    showInfo(message) {
        this.toastr.info(message);
    }
    getZipById() {
        this.cartService.getZipbyId(this.zipAreaId).subscribe(
            data => {
                this.zipCode = data.zip;
            },
            error => {
           
            }
        );
    }

    getRankByPackage() {
        this.cartService.getRankByPackage(this.packageId).subscribe(
            data => {
         
                this.rank = data.rank;
            },
            error => {
          
            }
        );
    }

    getSampleObj() {
        this.sampleObj = [];
        let obj = {
            zipcode: this.zipCode,
            duration: this.duration,
            rank: this.rank,
            slots: this.slots
        };
        this.sampleObj.push(obj);
    }
    getTotol() {}

    dispatchData(data: any) {
        this.store.dispatch({
            type: SUBSCRIPTION,
            payload: {
                cart: data
            }
        });
    }

    removeCart(i) {
        let items = [];
        let itemsObj = {
            subsId: i.subsId,
            cartId: i.cartId,
            packageId: i.packageId,
            zipAreaId: i.zipAreaId,
            slots: i.slots,
            duration: i.duration
        };
        items.push(itemsObj);
        let obj = {
            cartId: i.cartId,
            items: []
        };
        obj.items.push(itemsObj);
        this.cartService.removeCart(obj).subscribe(
            data => {
                if (data.statusCode == 200) {
                    this.showNotification(200, data.message);
                    this.getCartInfo();
                }
            },
            error => {
                this.showError(error.message);
            }
        );
    }

    placeOrder() {
        this.router.navigate(['/' + this.currentUserType, 'subscription', 'payment']);
    }
    editCart(i, content) {
        this.openLg(content);
        this.editZipcode = i.zip;
        this.getZip();
        this.editModalData = [];
        this.editModalData.push(i);
    }
    openLg(content) {
        this.modalService.open(content, { size: 'lg' });
    }

    getAllPackage(profileId, zipId) {
        this.cartService.getContentDataPar1(this.profileId, this.zipId).subscribe(
            data => {
                this.tableData = data;
            },
            error => {
                this.showError(error.message);
            }
        );
    }

    getAvailableSlot() {
        this.profileId = localStorage.getItem('profileId');
        this.cartService.getAvailableSlot(this.profileId, this.zipId).subscribe(
            data => {
                

                this.slotList = [];

                this.slotList = data;

               
                for (const slot of this.slotList) {
                    if (this.rank == slot['rank']) {
                        this.slot = slot['slot'];
                    }
                }
               
                this.getAllPackage(this.profileId, this.zipId);
            },
            error => {
               
                this.showError(error.message);
            }
        );
    }

    getZip() {
        this.cartService.getZip(this.editZipcode).subscribe(
            data => {
               
                if (data) {
                    this.zipId = data.id;
                    this.getAvailableSlot();
                } else if (data == null) {
                }
            },
            error => {
               
                this.showError(error.message);
            }
        );
    }

    updateCart(funref, data) {
      
        let sModel = new SubscriptionModel();
        let pModel = new PackageModel();
        (pModel.slots = parseInt(data.slots)),
            (pModel.zipAreaId = this.zipId),
            (pModel.packageId = data.packageId),
            (pModel.duration = 'YEARLY');
        sModel.profileId = this.profileId;
        sModel.items = [];
        sModel.items.push(pModel);
        this.cartData = sModel;

       

        this.cartService.updateToCart(this.cartData).subscribe(
            data => {
              
                this.showNotification(200, 'Updated Sucessfully to cart');
                funref('Cross click');
            },
            error => {
               
                this.showError(error.message);
            }
        );
    }
}
