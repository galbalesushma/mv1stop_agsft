import { Component, OnInit } from '@angular/core';
import { PaymentModel } from '../../../shared/model/subscription.model';
import { SubscriptionService } from 'app/service/subscription.service';
import { UploadAgentService } from 'app/service/upload-agent.service';
import { Subscription, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { SUBSCRIPTION } from 'app/shared/store/storeManager';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ViewChildren } from '@angular/core';
@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
    public check1:boolean = true;
    public check2:boolean = false;
    public check3:boolean = false;
    public accountType:any;
    public isCardPayment: boolean = true;
    public isEChequePayment: boolean = false;
    @ViewChildren('inputFocus') getFoucus;
    public isAdmin: boolean = false;
    model: PaymentModel;
    // txnType :string = "Paper Cheque";
    public subcriptionObservable: Observable<any>;
    public storeSubscription: Subscription;
    public totalAmount;
    public paymentData;
    public cartId;
    public total;
    public loading = true;
    public currentUserType;
    public monthFlag = false;
    public yearFlag = false;
    public statesList = [
        { id: 0, name: 'North Carolina' },
        { id: 1, name: 'Alabama' },
        { id: 2, name: 'Alaska' },
        { id: 3, name: 'Arizona' },
        { id: 4, name: 'Arkansas' },
        { id: 5, name: 'California' },
        { id: 6, name: 'Colorado' },
        { id: 7, name: 'Connecticut' },
        { id: 8, name: 'Delaware' },
        { id: 9, name: 'District of Columbia' },
        { id: 10, name: 'Florida' },
        { id: 11, name: 'Georgia' },
        { id: 12, name: 'Hawaii' },
        { id: 13, name: 'Idaho' },
        { id: 14, name: 'Illinois' },
        { id: 15, name: 'Indiana' },
        { id: 16, name: 'Iowa' },
        { id: 17, name: 'Kansas' },
        { id: 18, name: 'Kentucky' },
        { id: 19, name: 'Louisiana' },
        { id: 20, name: 'Maine' },
        { id: 21, name: 'Maryland' },
        { id: 22, name: 'Massachusetts' },
        { id: 23, name: 'Michigan' },
        { id: 24, name: 'Minnesota' },
        { id: 25, name: 'Mississippi' },
        { id: 26, name: 'Missouri' },
        { id: 27, name: 'Montana' },
        { id: 28, name: 'Nebraska' },
        { id: 29, name: 'Nevada' },
        { id: 30, name: 'New Hampshire' },
        { id: 31, name: 'New Jersey' },
        { id: 32, name: 'New Mexico' },
        { id: 33, name: 'New York' },
        { id: 34, name: 'North Carolina' },
        { id: 35, name: 'North Dakota' },
        { id: 36, name: 'Ohio' },
        { id: 37, name: 'Oklahoma' },
        { id: 38, name: 'Oregon' },
        { id: 39, name: 'Pennsylvania' },
        { id: 40, name: 'Puerto Rico' },
        { id: 41, name: 'Rhode Island' },
        { id: 42, name: 'South Carolina' },
        { id: 43, name: 'South Dakota' },
        { id: 44, name: 'Tennessee' },
        { id: 45, name: 'Texas' },
        { id: 46, name: 'Utah' },
        { id: 47, name: 'Vermont' },
        { id: 48, name: 'Virginia' },
        { id: 49, name: 'Virgin Islands' },
        { id: 50, name: 'Washington' },
        { id: 51, name: 'West Virginia' },
        { id: 52, name: 'Wisconsin' },
        { id: 53, name: 'Wyoming' }
    ];

    constructor(
        public paymentService: SubscriptionService,
        public uploadAgentService: UploadAgentService,
        public router: Router,
        public store: Store<any>,
        public toastr: ToastrService
    ) {
        
        this.model = new PaymentModel();
        this.model.country = 'USA';
        this.model.accountType = 'C';
        this.subcriptionObservable = store.pipe(select('subscriptionDataStore'));

        let type = localStorage.getItem('type');
        if (type == 'ADMIN') {
            this.isAdmin = true;
        }
    }

    ngOnInit() {
        this.model.amount =  JSON.parse(localStorage.getItem('totalAmount'));
        this.currentUserType = localStorage.getItem('userType');
        this.loading = true;
        this.getCartInfo();
        this.getAgent();
        this.storeSubscription = this.subcriptionObservable.subscribe(data => {
           

            if (data.hasOwnProperty('cartInfo')) {
               
                this.cartId = data['cartInfo'].value.cartId;
            }
        });
    }

    onSubmit() {
        // if(!this.isAdmin){
        //     this.model.txnType == '';
        // }
        if(!this.isAdmin && this.isCardPayment){
            this.model.txnType = "card";
        }else if(!this.isAdmin && this.isEChequePayment){
            this.model.txnType = "ACH";
        }

        this.loading = false;
        const profileId = localStorage.getItem('profileId');
       
        this.model.profileId = parseInt(profileId, 10);
       
        this.model.validTill = this.model.month + this.model.year;

        
        this.paymentService.payment(this.model).subscribe(
            data => {
               
                if (data.status == 'APPROVED' || data.status == 'SUCCESS') {
                    this.loading = true;
                    this.model.amount = this.model.amount;
                    this.showNotification(200, data.message);
                    // if admin redirect to paper cheque
                    if(this.isAdmin){
                        this.router.navigate(['/' + 'paper-cheque']);
                    }else{
                        this.router.navigate(['/' + 'agent-profile', 'viewProfile']);
                    }
                   
                   
                } else if (data.status == 'MISSING') {
                    this.showError(data.message);
                    this.loading = true;
                    this.model.amount = this.model.amount;
                } else if (data.status == 'DECLINED') {
                    this.showError(data.message);
                    this.loading = true;
                    this.model.amount = this.model.amount;
                } else if (data.status == 'ERROR') {
                    this.showError(data.message);
                    this.loading = true;
                    this.model.amount = this.model.amount;
                }
            },
            error => {
               
                this.showError(error.message);
                this.loading = true;
            }
        );
    }

    getCartInfo() {
        const profileId = localStorage.getItem('profileId');
        this.paymentService.getCartInfo(profileId).subscribe(
            data => {
                if (data.statusCode == 200) {
                    this.model.cartId = data.value.cartId;
                    // let doller = '$';
                    // this.model.amount = data.value.total
                    
                }
            },
            error => {
               
            }
        );
    }
    showNotification(statusCode, message) {
       
        switch (statusCode) {
            case 200:
                this.toastr.success(message);
               
                break;

            default:
                this.toastr.error(message);
                break;
        }
    }

    showWarning(message) {
        this.toastr.warning(message);
    }

    showError(message) {
        this.toastr.error(message);
    }

    showInfo(message) {
        this.toastr.info(message);
    }

    getAgent() {
        let profileId = localStorage.getItem('profileId');
        this.uploadAgentService.getAgentByProfile(profileId).subscribe(
            data => {

                if (data.statusCode == 200) {
                    let newData;
                    newData = data.value;
                    this.model.firstName = newData.firstName;
                    this.model.lastName = newData.lastName;
                    this.model.phone = newData.prefMobile;
                    this.model.email = newData.bEmail;
                }
            },
            error => {

            }
        );
    }
    checkMonth(month){
      if(month > 12){
        this.monthFlag = true;
      }
      else{
        this.monthFlag = false;
      }
    }
    checkYear(year){
      if(year < 18 || year > 36){
        this.yearFlag = true;
      }
      else{
        this.yearFlag = false;
      }
    }

    makeCardPayment(f: NgForm){
        // f.resetForm();
        this.model.cardNo = '';
        this.model.routingNumber = '';
        this.model.chequeNo = '';
        this.model.accountNumber = '';
        this.model.cvv = '';
        this.model.month = '';
        this.model.year = '';
        this.isCardPayment = true;
        this.isEChequePayment = false;
        // this.getFoucus.first.nativeElement.focus();
    }

    makeChequePayment(f: NgForm){
        // f.resetForm();
        this.model.cardNo = '';
        this.model.routingNumber = '';
        this.model.chequeNo = '';
        this.model.accountNumber = '';
        this.model.cvv = '';
        this.model.month = '';
        this.model.year = '';
        this.isEChequePayment = true;
        this.isCardPayment = false;
        // this.getFoucus.first.nativeElement.focus();
    }

    // ngAfterViewInit() {            
    //     this.getFoucus.first.nativeElement.focus();
    // }

    getFocus1(){
        this.check1 = false;
        this.check2 = true;
        this.check3 = false;
    }  
     getFocus2(){
        this.check1 = false;
        this.check2 = false;
        this.check3 = true;
    }
    
    blur(){
        this.check1 = true;
        this.check2 = false;
        this.check3 = false;
    }

}
