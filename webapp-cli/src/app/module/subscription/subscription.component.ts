import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { SUBSCRIPTION } from 'app/shared/store/storeManager';
import { SubscriptionService } from 'app/service/subscription.service';
@Component({
    selector: 'app-subscription',
    templateUrl: './subscription.component.html',
    styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit, OnDestroy {
    languageToggleFlag = false;
    subscriptionToggleFlag = false;
    public storeSubscription: Subscription;
    public subcriptionObservable: Observable<any>;

    currentUserType = null;
    constructor(
        public router: Router,
        public cartService: SubscriptionService,
        public activateRoute: ActivatedRoute,
        private store: Store<any>
    ) {
        this.activateRoute.params.subscribe(routeParam => {
       
            if (routeParam.hasOwnProperty('type')) {
                this.currentUserType = routeParam['type'];
                this.dispatchData({
                    userType: this.currentUserType
                });
                switch (this.currentUserType) {
                    case 'agent-profile':
                        localStorage.setItem('userType', 'agent');
                        
                        break;
                    case 'facility-profile':
                        localStorage.setItem('userType', 'facility');
                       
                        break;
                    default:
                        break;
                }
            }
        });

    }

    getCartInfo() {
        const profileId = localStorage.getItem('profileId');
        this.cartService.getCartInfo(profileId).subscribe(
            data => {
              
            },
            error => {
           
            }
        );
    }

    dispatchData(data: any) {
        this.store.dispatch({
            type: SUBSCRIPTION,
            payload: {
                cart: data
            }
        });
    }

    ngOnInit() {
    }
    ngOnDestroy() {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }

    initAgent() {}
    toggleMenu(type) {
        switch (type) {
            case 'lang':
                this.languageToggleFlag = !this.languageToggleFlag;
                break;
            case 'sub':
                this.subscriptionToggleFlag = !this.subscriptionToggleFlag;
                break;
            default:
                break;
        }
    }
    selectSubscription() {
        this.subscriptionToggleFlag = false;
        this.router.navigate(['/home/subscription']);
    }

    changeLanguage(val) {
        this.languageToggleFlag = false;
        switch (val) {
            case 'US':
                break;
            case 'ES':
                break;
            default:
                break;
        }
    }
}
