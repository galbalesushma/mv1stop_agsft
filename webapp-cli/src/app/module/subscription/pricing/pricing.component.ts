import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionService } from '../../../service/subscription.service';
import { SubscriptionModel, PackageModel } from '../../../shared/model/subscription.model';
import { ToastrService } from 'ngx-toastr';

import { Subscription, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { SUBSCRIPTION } from '../../../shared/store/storeManager';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthUserService } from 'app/service/auth-user.service';
import { UploadAgentService } from 'app/service/upload-agent.service';

@Component({
    selector: 'app-pricing',
    templateUrl: './pricing.component.html',
    styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit, OnDestroy {
    totalAmount: number = 0;
    public isAdmin: boolean = false;
    pathHref: string;
    public cartData: SubscriptionModel;

    public slotList: any;
    public packageList;
    public zipcode;
    public profileId;
    public slots;
    public name;
    public rank;
    public price;
    public desc;
    public slot;
    public zipId;
    public pid;
    public pduration;
    public disableFlag = false;
    public durationFlag = false;
    public disabledButton = false;
    public newZipcode;
    public tableData;
    public nodataFlag = false;
    public dataFound = false;
    public incMember = 25;
    public showZipFlag = true;
    public placeOrderData;
    public cartInfo;
    public total;
    public itemQuantity;
    public zipAreaId;
    public zipCode;
    public duration;
    public packageId;
    public cartId;
    public subsId;
    public cartEmptyFlag = false;
    public sampleObj;
    public editZipcode;
    public editModalData = [];
    public currentUserType;
    public emptycartFlag = false;
    public zip;
    public totalSum = 0;
    public limitFlag = false;
    public subscribeData;
    purchaseslot = 0;
    public newSlotsFlag = false;
    public key = {
        newSlots: 0
    };
    public listLoader = false

    public availableSlot = [1, 2, 3, 4, 5];

    public storeSubscription: Subscription;
    public subcriptionObservable: Observable<any>;
    public agentData:any;

    constructor(
        public subscriptionService: SubscriptionService,
        public toastr: ToastrService,
        private store: Store<any>,
        private modalService: NgbModal,
        public router: Router,
        public authService: AuthUserService,
        public uploadService :UploadAgentService
    ) {
        this.cartData = new SubscriptionModel();

        this.subcriptionObservable = store.pipe(select('subscriptionDataStore'));

        this.storeSubscription = this.subcriptionObservable.subscribe(data => {

        });

        // this.pathHref = this.router.url;
        let type = localStorage.getItem('type');
        if (type == 'ADMIN') {
            this.isAdmin = true;
        }
        
        
       
    }

    dispatchData(data: any) {
        this.store.dispatch({
            type: SUBSCRIPTION,
            payload: {
                cart: data
            }
        });
    }
    ngOnDestroy() {
        this.storeSubscription.unsubscribe();
    }

    ngOnInit() {
        this.currentUserType = localStorage.getItem('type');
        this.slotList = [];
        switch (localStorage.getItem('userType')) {
            case 'agent':
                this.initAgent();
                break;
            case 'facility':
                this.initFacility();
                break;
            default:
                break;
        }
        setTimeout(()=>{
            // this.getAgentById()
        },100);

    }

    initAgent() {
        this.getAgentById()
        setTimeout(()=>{
            let profileId = localStorage.getItem('profileId');
            this.getPackSubscription(profileId);
            this.getCartInfo();
            this.getCartDetails();
        }, 100);
    }

    initFacility() {
    }

    changeEvent(event) {
        this.slots = event.target.value;
        this.disabledButton = true;
    }
    changeRank(event) {
        for (let packageObj of this.packageList) {
            if (event.target.value == packageObj['rank']) {
                this.name = packageObj['name'];
                this.rank = packageObj['rank'];
                this.price = packageObj['price'];
                this.desc = packageObj['desc'];
                this.slot = packageObj['slot'];
                this.pid = packageObj['id'];
                this.disableFlag = true;
            }
        }

        this.getAvailableSlot();
    }
    changeDuration(event) {
        this.pduration = event.target.value;
        this.durationFlag = true;
    }

    addToCart(data) {
        if (data.rank == 'ONE') {
            data.id = 1
        }
        else if (data.rank == 'TWO') {
            data.id = 2;
        }
        let sModel = new SubscriptionModel();
        let pModel = new PackageModel();
        (pModel.slots = data.newSlots),
            (pModel.zipAreaId = this.zipId),
            (pModel.packageId = data.id),
            (pModel.duration = 'YEARLY');
        sModel.profileId = this.profileId;
        sModel.items = [];
        sModel.items.push(pModel);
        this.cartData = sModel;


        this.subscriptionService.addToCart(this.cartData).subscribe(
            data => {

                if (data.statusCode == 200) {
                    this.showNotification(data.statusCode, data.message);
                    this.getCartDetails();
                    this.getAllPackage(this.profileId, this.zipId);
                    this.getZip();
                } else if (data.statusCode == 400) {
                    this.showError(data.message);
                }
            },
            error => {

                this.showError(error.message);
            }
        );
    }
    addIncCart() {
        let sModel = new SubscriptionModel();
        let pModel = new PackageModel();
        let slots = 1;
        let packageId = 3;
        let zipId = 1;
        (pModel.slots = slots), (pModel.zipAreaId = zipId), (pModel.packageId = packageId), (pModel.duration = 'YEARLY');
        sModel.profileId = parseInt(localStorage.getItem('profileId'));
        sModel.items = [];
        sModel.items.push(pModel);
        this.cartData = sModel;


        this.subscriptionService.addToINCCart(this.cartData).subscribe(
            data => {
                if (data.statusCode == 200) {
                    this.showNotification(data.statusCode, data.message);

                    this.getCartDetails();
                } else if (data.statusCode == 400) {
                    this.showError(data.message);

                }
            },
            error => {
                this.showError(error.message);

            }
        );
    }

    getZip() {
        this.listLoader = true
        localStorage.setItem('newZip', this.zipcode)
        this.subscriptionService.getZip(this.zipcode).subscribe(
            data => {
                this.listLoader = false
                if (data) {
                    this.zipId = data.id;
                    this.newZipcode = data.zip;

                    this.dataFound = true;
                    this.nodataFlag = false;
                    this.getCartData();
                    this.getAvailableSlot();
                } else if (data == null) {
                    this.nodataFlag = true;
                    this.dataFound = false;
                }

            },
            error => {
                this.showError(error.message);
                this.listLoader = false
            }
        );
    }

    getAvailableSlot() {
        this.profileId = localStorage.getItem('profileId');
        this.subscriptionService.getAvailableSlot(this.profileId, this.zipcode).subscribe(
            data => {


                this.slotList = [];
                this.slotList = data;





                for (const slot of this.slotList) {
                    if (this.rank == slot['rank']) {
                        this.slot = slot['slot'];
                    }
                }


                this.getAllPackage(this.profileId, this.zipId);
            },
            error => {

                this.showError(error.message);
            }
        );
    }

    getArray(int) {
        if (int) {
            return Array(parseInt(int, 10));
        }
        return [];
    }

    showNotification(statusCode, message) {

        switch (statusCode) {
            case 200:
                this.toastr.success(message);

                break;

            default:
                this.toastr.error(message);
                break;
        }
    }

    showWarning(message) {
        this.toastr.warning(message);
    }

    showError(message) {
        this.toastr.error(message);
    }

    showInfo(message) {
        this.toastr.info(message);
    }

    getCartData() {
        let newArray = [];
        let newObj;
        for (let key in this.packageList) {
            newObj = {
                price: this.packageList[key].price,
                rank: this.packageList[key].rank,
                name: this.packageList[key].name
            };
            newArray.push(newObj);
        }


    }

    getAllPackage(profileId, zipId) {
        this.subscriptionService.getSlotsAvilable(profileId, zipId).subscribe(
            data => {
                this.tableData = data;

                this.getSubsription();
            },
            error => {
                this.showError(error.message);
            }
        );
    }

    getPackSubscription(prfoileId) {
        this.subscriptionService.getPackSubscription(prfoileId).subscribe(
            data => {

                let subscriptionData = data;

                for (let key in subscriptionData) {
                    if (subscriptionData[key].packageId == 3) {

                        // this.showZipFlag = true;
                    } else {

                    }
                }

                for (let i in subscriptionData) {
                    if (subscriptionData[i].packageId == 1) {
                        let slot = subscriptionData[i].slot;

                    }
                    if (subscriptionData[i].packageId == 2) {
                        let slot = subscriptionData[i].slot;

                    }
                }
            },
            error => {

                this.showError(error.message);
            }
        );
    }

    getCartInfo() {
        const profileId = localStorage.getItem('profileId');
        this.subscriptionService.getCartInfo(profileId).subscribe(
            data => {
                if (data.statusCode == 200) {

                    for (let key in data.value.items) {
                        if (data.value.items[key].packageId == 3) {
                            // this.showZipFlag = true;
                        } else {

                        }
                    }
                    this.emptycartFlag = false;
                } else if (data.statusCode == 400) {
                    this.emptycartFlag = true;
                }
            },
            error => {
                this.emptycartFlag = true;
            }
        );
    }

    getCartDetails() {
        const profileId = localStorage.getItem('profileId');
        this.totalAmount = 0;
        this.subscriptionService.getCartInfo(profileId).subscribe(
            data => {

                if (data.statusCode == 200) {
                    this.placeOrderData = data;
                    this.cartInfo = data.value.items;
                    this.total = data.value.total;
                    this.itemQuantity = data.value.items.length;
                    this.totalAmount = 0;
                    for (let i in this.cartInfo) {
                        this.zipAreaId = this.cartInfo[i].zipAreaId;
                        this.duration = this.cartInfo[i].duration;
                        this.slots = this.cartInfo[i].slots;
                        this.packageId = this.cartInfo[i].packageId;
                        this.cartId = this.cartInfo[i].cartId;
                        this.subsId = this.cartInfo[i].subsId;
                        this.zip = this.cartInfo[i].zip;

                      
                    }
                    this.calculateTotal();
                    this.emptycartFlag = false;

                    this.dispatchData({
                        cartInfo: this.placeOrderData
                    });

                    this.getZipById();
                    this.getRankByPackage();
                    setTimeout(() => {
                        this.getSampleObj();
                    }, 500);
                } else if (data.statusCode == 400) {
                    this.emptycartFlag = true;
                }
            },
            error => {
            }
        );
    }

    calculateTotal(){
        this.totalAmount = 0;
        for (let i in this.cartInfo) {
            let j = parseInt(i);
            if(this.agentData <= 0){
                if(this.cartInfo[j].rank == 'ONE'){
                    this.totalAmount = this.totalAmount + (this.cartInfo[j].slots* 100);
                }else{
                    this.totalAmount = this.totalAmount + (this.cartInfo[j].slots* 50);
                }
            }
    
            if(this.agentData <= 3 && this.agentData > 0){
                if(this.cartInfo[j].rank== 'ONE'){
                    this.totalAmount = this.totalAmount + (this.cartInfo[j].slots* 300);
                }else{
                    this.totalAmount = this.totalAmount + (this.cartInfo[j].slots* 150);
                }
            }
    
            if((this.agentData == 4 || this.agentData == 5) && this.agentData > 0){
                if(this.cartInfo[j].rank == 'ONE'){
                    this.totalAmount = this.totalAmount + (this.cartInfo[j].slots* 400);
                }else{
                    this.totalAmount = this.totalAmount + (this.cartInfo[j].slots* 200);
                }
            }
        }
    }


    getZipById() {
        this.subscriptionService.getZipbyId(this.zipAreaId).subscribe(
            data => {

                this.zipCode = data.zip;
            },
            error => {

            }
        );
    }

    getRankByPackage() {
        this.subscriptionService.getRankByPackage(this.packageId).subscribe(
            data => {

                this.rank = data.rank;
            },
            error => {
            }
        );
    }

    getSampleObj() {
        this.sampleObj = [];
        let obj = {
            zipcode: this.zipCode,
            duration: this.duration,
            rank: this.rank,
            slots: this.slots
        };
        this.sampleObj.push(obj);

    }

    removeCart(i) {

        let items = [];
        let itemsObj = {
            subsId: i.subsId,
            cartId: i.cartId,
            packageId: i.packageId,
            zipAreaId: i.zipAreaId,
            slots: i.slots,
            duration: i.duration
        };
        items.push(itemsObj);
        let obj = {
            cartId: i.cartId,
            items: []
        };
        obj.items.push(itemsObj);

        this.subscriptionService.removeCart(obj).subscribe(
            data => {

                if (data.statusCode == 200) {
                    this.showNotification(200, data.message);
                    this.getCartDetails();
                    let profileId = localStorage.getItem('profileId')
                    let zipCode = localStorage.getItem('newZip')
                    this.getAllPackage(profileId, zipCode);
                    this.getZip();
                }
            },
            error => {

                this.showError(error.message);
            }
        );
    }

    editCart(i, content) {
        this.openLg(content);

        this.editModalData = [];
        this.editModalData.push(i);

        setTimeout(() => {
            this.getNewSlot(this.editModalData[0].zip)

        }, 500)


    }
    close(funref) {
        funref('Cross click');
        this.editModalData = []
    }

    getNewSlot(zip) {
        this.profileId = localStorage.getItem('profileId');
        this.subscriptionService.getAvailableSlot(this.profileId, zip).subscribe(
            data => {

            },
            error => {

                this.showError(error.message);
            }
        );
    }
    openLg(content) {
        this.modalService.open(content, { size: 'lg' });
    }

    updateCart(funref, data) {
        let sModel = new SubscriptionModel();
        let pModel = new PackageModel();
        (pModel.slots = parseInt(data.slots)),
            (pModel.zipAreaId = this.zipId),
            (pModel.packageId = data.packageId),
            (pModel.duration = 'YEARLY');
        sModel.profileId = this.profileId;
        sModel.items = [];
        sModel.items.push(pModel);
        this.cartData = sModel;

        this.subscriptionService.updateToCart(this.cartData).subscribe(
            data => {

                this.showNotification(200, 'Updated Sucessfully to cart');
                funref('Cross click');
                this.getAllPackage(this.profileId, this.zipId);
            },
            error => {

                this.showError(error.message);
            }
        );
    }

    placeOrder() {
        localStorage.setItem('totalAmount',JSON.stringify(this.totalAmount))
        this.router.navigate(['/' + this.currentUserType, 'subscription', 'payment']);
    }

    getSubsription() {
        let profileId = localStorage.getItem('profileId');
        this.subscriptionService.getPackSubscription(profileId).subscribe(
            data => {
                this.subscribeData = data;

            },
            error => { }
        );
    }

    checkPurchaseSlot(availSlot, newSlots, selectedIndex, key) {

        let profileId = localStorage.getItem('profileId')
        this.subscriptionService.getPackSubscription(profileId).subscribe(
            data => {
                let subscriptionData = data;
                for (let key in subscriptionData) {
                    if (subscriptionData[key].packageId == 3) {
                    } else {
                    }
                }
                let slot = 0;
                let cartSlot = 0;
                switch (selectedIndex) {
                    case 0:
                        for (let i in subscriptionData) {

                            if (subscriptionData[i].packageId == 1 && subscriptionData[i].zipcode == this.zipcode) {
                                slot += subscriptionData[i].slots;
                            }
                            if ((newSlots + slot) > 1 || newSlots > 1) {
                                key.newSlotsFlag = true;
                            }
                            else {
                                key.newSlotsFlag = false;
                            }
                        }
                        for (let k in this.cartInfo) {
                            if (this.cartInfo[k].packageId == 1 && this.cartInfo[k].zip == this.zipcode) {
                                cartSlot += this.cartInfo[k].slots;
                                if ((cartSlot + newSlots) > 1) {
                                    key.newSlotsFlag = true;
                                }
                                else {
                                    key.newSlotsFlag = false;
                                }
                            }
                        }
                        break;
                    case 1:
                        for (let i in subscriptionData) {
                            if (subscriptionData[i].packageId == 2 && subscriptionData[i].zipcode == this.zipcode) {
                                slot += subscriptionData[i].slots;
                            }
                            if ((newSlots + slot) > 1 || newSlots > 1) {
                                key.newSlotsFlag = true;
                            }
                            else {
                                key.newSlotsFlag = false;
                            }
                        }
                        for (let k in this.cartInfo) {
                            if (this.cartInfo[k].packageId == 2 && this.cartInfo[k].zip == this.zipcode) {
                                cartSlot += this.cartInfo[k].slots;
                                if ((cartSlot + newSlots) > 1) {
                                    key.newSlotsFlag = true;
                                }
                                else {
                                    key.newSlotsFlag = false;
                                }
                            } 
                        }
 
                        break;
                }
            },
            error => {

                this.showError(error.message);
            }
        );
    }

    checkEditCart(editSlots, selectedIndex, key) {
        let profileId = localStorage.getItem('profileId')
        this.subscriptionService.getPackSubscription(profileId).subscribe(
            data => {
                let subscriptionData = data;
                for (let key in subscriptionData) {
                    if (subscriptionData[key].packageId == 3) {
                    } else {
                    }
                }
                let slot = 0;
                let cartSlot = 0;

                for (let i in subscriptionData) {
                    if (subscriptionData[i].packageId == 1) {
                        slot += subscriptionData[i].slots;
                    }
                    else if (subscriptionData[i].packageId == 2) {
                        slot += subscriptionData[i].slots;
                    }
                    if ((editSlots + slot) > 1 || editSlots > 1) {
                        key.newSlotsFlag = true;
                    }
                    else {
                        key.newSlotsFlag = false;
                    }


                }
                for (let k in this.cartInfo) {
                    if (this.cartInfo[k].packageId == 1) {
                        cartSlot += this.cartInfo[k].slots;
                        if ((cartSlot + editSlots) > 1) {
                            key.newSlotsFlag = true;
                        }
                        else {
                            key.newSlotsFlag = false;
                        }
                    }

                }


                error => {
                    this.showError(error.message);
                }
            });
    }

    getProfileInfo() {
        this.listLoader = true
        let emailId = localStorage.getItem('prefEmail')
        this.authService.getProfile(emailId).subscribe(
            data => {
                this.agentData;
                this.agentData = data.value.carriers.length;
                // console.log('count',this.agentData)
                this.listLoader = false
            },
            error => {
                this.listLoader = false
            }
        )
    }
    getAgentById(){
        this.listLoader = true
       let npn = localStorage.getItem('npn')
        this.uploadService.getAgentById(npn).subscribe(
            data=>{
                this.listLoader = false
                // console.log('data1',data)
                
                    this.agentData = data.value.carriers.length;
                    // console.log('count222',this.agentData)
                
               
              
            },
            error=>{
                this.listLoader = false
            }
        )
    }
}
