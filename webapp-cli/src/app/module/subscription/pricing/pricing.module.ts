import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PricingRoutingModule } from './pricing-routing.module';
import { PricingComponent } from './pricing.component';
import { FormsModule } from '@angular/forms';
import { CounterFilterPipeModule } from '../../../shared/pipes/counter/counterFilter.module';

@NgModule({
    imports: [CommonModule, PricingRoutingModule,FormsModule , CounterFilterPipeModule],
    declarations: [PricingComponent]
})
export class PricingModule {}
