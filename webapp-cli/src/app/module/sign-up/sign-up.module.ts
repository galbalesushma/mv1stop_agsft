import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { SignUpRoutingModule } from './sign-up-routing.module';
import { SignUpComponent } from './sign-up.component';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
@NgModule({
    imports: [
        CommonModule,
        SignUpRoutingModule,
        FormsModule,
        NgMultiSelectDropDownModule.forRoot()
        // ToastrModule.forRoot()
    ],
    declarations: [SignUpComponent]
})
export class SignUpModule {}
