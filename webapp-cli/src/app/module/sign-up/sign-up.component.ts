import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthUserService } from '../../service/auth-user.service';
import { ToastrService } from 'ngx-toastr';

import { SignUpUserModel, SignUpFacilityModel } from 'app/shared/model/signup.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.css'],
    providers: [ToastrService, AuthUserService]
})
export class SignUpComponent implements OnInit {
    model: SignUpUserModel;
    facilityModel: SignUpFacilityModel;
    isSignUpFlag = false;
    isSignUpSuccess = false;
    isSignUpFacSuccess = false;
    isSignUpFacFlag = false;
    currentUserType;
    public npmVerifyFlag = false;
    public npmverification;
    public isnVerification;
    public showNpn = true;
    public npnstatus;
    public loading = true;
    public verifyloading = true;
    public nameVerfiyFlag = false;
    public nameVerifyFac = false;
    public signUpFlag = true;
    public signUpFacFlag = true;
    public selectFlag = true;
    public facilityVerify = false;
    public facLoading = true;
    passNotMatchFlag: boolean;
    facpassNotMatchFlag: boolean;
    dropdownList = [];
    selectedItems = [];
    dropdownSettings = {};

    constructor(
        public authUserService: AuthUserService,
        public toastr: ToastrService,
        public router: Router,
        public activateRoute: ActivatedRoute
    ) {
        this.model = new SignUpUserModel();
        this.model.lang = 'US';
        this.facilityModel = new SignUpFacilityModel();
        this.facilityModel.lang = 'US';
        this.model.comments = 'no';
        this.facilityModel.mechDuty = 'no';

        this.activateRoute.params.subscribe(routeParam => {
            if (routeParam.hasOwnProperty('type')) {
                this.currentUserType = routeParam['type'];

                switch (this.currentUserType) {
                    case 'agent-profile':
                        break;
                    case 'facility-profile':
                        break;
                    case 'user':
                        break;
                    default:
                        this.router.navigate(['/home']);
                        break;
                }
            }
        });
    }

    ngOnInit() {
        this.passNotMatchFlag = true;
        this.facpassNotMatchFlag = true;
        this.loading = true;
        this.facLoading = true;
        this.settings();
        this.getListOfCarrier();
    }

    checkpassword() {
        if (this.model.password == this.model.confpassword) {
            this.passNotMatchFlag = false;
        } else {
            this.passNotMatchFlag = true;
        }
    }
    checkfacpassword() {
        if (this.facilityModel.password == this.facilityModel.confpassword) {
            this.facpassNotMatchFlag = false;
        } else {
            this.facpassNotMatchFlag = true;
        }
    }

    onSubmit() {
        this.loading = false;
        if (this.model.lang == 'US') {
            this.model.langKey = 'en';
        }
        else if (this.model.lang == 'ES') {
            this.model.langKey = 'es';
        }
        let id;
        let array=[]
        for(let key in this.selectedItems){
            id = this.selectedItems[key].id;
            array.push(id)
        }
        this.model.carriers = array;
        this.model.npn = this.npmverification;
        this.authUserService.signUp(this.model).subscribe(
            data => {
                this.isSignUpFlag = true;

                if (data['statusCode'] == 200) {
                    this.isSignUpSuccess = true;
                    this.loading = true;
                } else {
                    this.isSignUpSuccess = false;
                    this.loading = true;
                }

                this.showNotification(data['statusCode'], data['message']);
            },
            error => {
                this.loading = true;
                this.isSignUpFlag = true;
                this.isSignUpSuccess = false;
                this.showError(error.message);
            }
        );
    }

    agentverify() {
        this.authUserService.verifySignUp(this.model).subscribe(
            data => {
                if (data.statusCode == 200) {
                    this.nameVerfiyFlag = true;
                } else if (data.statusCode == 400) {
                    this.nameVerfiyFlag = false;
                    this.showError(data.message);
                }
                
            },
            error => {
                this.nameVerfiyFlag = false;
                
                this.showError(error.message);
            }
        );
    }

    onFacilitySubmit() {
        this.loading = false;
        this.facilityModel.isn = this.isnVerification;
        if (this.facilityModel.lang == 'US') {
            this.facilityModel.langKey = 'en';
        }
        else if (this.facilityModel.lang == 'ES') {
            this.facilityModel.langKey = 'es';
        }

        this.authUserService.signUpFacility(this.facilityModel).subscribe(
            data => {
                this.isSignUpFlag = true;
                if (data['statusCode'] == 200) {
                    this.isSignUpSuccess = true;
                    this.loading = true;
                } else {
                    this.isSignUpSuccess = false;
                    this.loading = true;
                }

                this.showNotification(data['statusCode'], data['message']);
            },
            error => {
                this.loading = true;
                this.isSignUpFlag = true;
                this.isSignUpSuccess = false;
                this.showError(error.message);
            }
        );
    }

    activateSignup() {
        this.isSignUpFlag = false;
    }
    activateFacSignup() {
        this.isSignUpFacFlag = false;
    }
    showNotification(statusCode, message) {
        switch (statusCode) {
            case 200:
                this.toastr.success(message);
                break;

            default:
                this.toastr.error(message);
                break;
        }
    }

    showWarning(message) {
        this.toastr.warning(message);
    }

    showError(message) {
        this.toastr.error(message);
    }

    showInfo(message) {
        this.toastr.info(message);
    }

    verifyNpn() {
        this.verifyloading = false;
        this.authUserService.verifynpn(this.npmverification).subscribe(
            data => {
                if (data.success == true) {
                    this.model.npn = this.npmverification;
                    this.showNpn = false;
                    this.npmVerifyFlag = true;
                    this.verifyloading = true;
                    this.signUpFlag = true;
                    this.selectFlag = false;
                    this.npnstatus = data.success;
                    this.showNotification(200, data.message);
                } else if (data.success == false) {
                    this.npmVerifyFlag = false;
                    this.verifyloading = true;
                    this.signUpFlag = false;
                    this.showNpn = true;
                    this.selectFlag = true;
                    this.showError(data.message);
                }
            },
            error => {
                this.verifyloading = true;
                this.npmVerifyFlag = false;
                this.showError(error.message);
            }
        );
    }

    verifyIsn() {
        this.verifyloading = false;
        this.facilityModel.isn = this.isnVerification;
        this.authUserService.verifyisn(this.isnVerification).subscribe(
            data => {
                if (data.success == true) {
                    this.verifyloading = true;
                    this.facilityVerify = true;
                    this.signUpFacFlag = true;
                    this.selectFlag = false;
                    this.facilityModel.isn = this.isnVerification;
                    this.showNotification(200, data.message);
                } else if (data.success == false) {
                    this.selectFlag = true;
                    this.facilityVerify = false;
                    this.signUpFacFlag = false;
                    this.verifyloading = true;
                    this.facilityModel.isn = this.isnVerification;
                    this.showError(data.message);
                }
            },
            error => {
                this.facilityVerify = false;
                this.verifyloading = true;
                this.npmVerifyFlag = false;
                this.showError(error.message);
            }
        );
    }
    changeSignup(value) {
       
        if (value == 'agent-profile') {
            this.signUpFlag = false;
            this.signUpFacFlag = true;
        } else if (value == 'facility-profile') {
            this.signUpFacFlag = false;
            this.signUpFlag = true;
            this.currentUserType == 'facility-profile';
        }
    }

    facilityverify() {
        this.authUserService.verifySignUp(this.model).subscribe(
            data => {
                if (data.statusCode == 200) {
                   
                    this.facilityVerify = true;
                } else if (data.statusCode == 400) {
                    this.facilityVerify = false;
                    this.showError(data.message);
                }
            },
            error => {
                this.facilityVerify = false;
              
                this.showError(error.message);
            }
        );
    }

    settings() {
        this.dropdownList = [];
        this.selectedItems = [];
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'carrier',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true,
            enableCheckAll: false,
            limitSelection: 5
        };
    }

    onItemSelect(item: any) {
       
    }
    onItemDeSelect(item: any) {
       
    }
    
    getListOfCarrier(){
        this.authUserService.getCarrierList().subscribe(
            data=>{
                if(data.statusCode == 200){
                    this.dropdownList = data.value;
                   
                }
                else{
                    this.dropdownList =[];
                }
            },
            error=>{
                
            }
        )
    }
}
