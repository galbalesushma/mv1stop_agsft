import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchaseSubscriptionComponent } from './purchase-subscription.component';

const routes: Routes = [
    {
        path: '',
        component: PurchaseSubscriptionComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PurchaseSubscriptionRoutingModule {}
