import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchaseSubscriptionRoutingModule } from './purchase-subscription-routing.module';
import { PurchaseSubscriptionComponent } from './purchase-subscription.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SearchFilterPipeModule } from 'app/shared/pipes/searchFilter/searchFilter.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchModule } from 'app/shared/search/search.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

@NgModule({
    imports: [
        CommonModule,
        PurchaseSubscriptionRoutingModule,
        ReactiveFormsModule,
        NgbModule,
        // ToastrModule.forRoot(),
        SearchFilterPipeModule,
        FormsModule,
        SearchModule,
        AngularMultiSelectModule
    ],
    declarations: [PurchaseSubscriptionComponent]
})
export class PurchaseSubscriptionModule {}
