import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewSubscriptionRoutingModule } from './view-subscription-routing.module';
import { ViewSubscriptionComponent } from './view-subscription.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SearchFilterPipeModule } from 'app/shared/pipes/searchFilter/searchFilter.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchModule } from 'app/shared/search/search.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

@NgModule({
    imports: [
        CommonModule,
        ViewSubscriptionRoutingModule,
        ReactiveFormsModule,
        NgbModule,
        // ToastrModule.forRoot(),
        SearchFilterPipeModule,
        FormsModule,
        SearchModule,
        AngularMultiSelectModule
    ],
    declarations: [ViewSubscriptionComponent]
})
export class ViewSubscriptionModule {}
