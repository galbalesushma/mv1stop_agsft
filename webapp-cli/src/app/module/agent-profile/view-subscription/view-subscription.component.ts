import { Component, OnInit } from '@angular/core';
import { SubscriptionService } from '../../../service/subscription.service';

@Component({
    selector: 'app-view-subscription',
    templateUrl: './view-subscription.component.html',
    styleUrls: ['./view-subscription.component.css']
})
export class ViewSubscriptionComponent implements OnInit {
    public subscribeData;
    public noFlag = false;
    constructor(public subscriptionService: SubscriptionService) {}

    ngOnInit() {
        this.getSubsription();
    }

    getSubsription() {
        let profileId = localStorage.getItem('profileId');
        this.subscriptionService.getPackSubscription(profileId).subscribe(
            data => {
                this.subscribeData = data;
                if (this.subscribeData.length == 0) {
                    this.noFlag = true;
                }

                
            },
            error => {
                this.noFlag = false;
            }
        );
    }
}
