import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgentCoreProfileRoutingModule } from './agent-core-profile-routing.module';
import { AgentCoreProfileComponent } from './agent-core-profile.component';

@NgModule({
    imports: [CommonModule, AgentCoreProfileRoutingModule],
    declarations: [AgentCoreProfileComponent]
})
export class AgentCoreProfileModule {}
