import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgentProfileRoutingModule } from './agent-profile-routing.module';
import { AgentProfileComponent } from './agent-profile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SearchFilterPipeModule } from 'app/shared/pipes/searchFilter/searchFilter.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchModule } from 'app/shared/search/search.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
    imports: [
        CommonModule,
        AgentProfileRoutingModule,
        ReactiveFormsModule,
        NgbModule,
        // ToastrModule.forRoot(),
        SearchFilterPipeModule,
        FormsModule,
        SearchModule,
        AngularMultiSelectModule,
        NgMultiSelectDropDownModule.forRoot()

    ],
    declarations: [AgentProfileComponent]
})
export class AgentProfileModule {}
