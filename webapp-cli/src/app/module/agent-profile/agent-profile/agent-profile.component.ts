import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UploadAgentService } from 'app/service/upload-agent.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DropdownSettingModel } from 'app/shared/model/dropdown.model';
import { SubscriptionService } from '../../../service/subscription.service';
import { AuthUserService } from 'app/service/auth-user.service';
import Swal from 'sweetalert2'
import 'sweetalert2/src/sweetalert2.scss'
import{Router} from '@angular/router';
@Component({
    selector: 'app-agent-profile',
    templateUrl: './agent-profile.component.html',
    styleUrls: ['./agent-profile.component.css'],
    providers: [ToastrService]
})
export class AgentProfileComponent implements OnInit {
    totalAmount: number = 0;
    agentData: any;
    dataCompany = [];
    selectedCompanyId;
    selectedCompanyName =false
    searchText;
    agentDataList = [];
    agentDataObj = {};

    stateList = [];
    carriers = []
    dropdownFlag = false;
    showFilterFlag = false;
    public insuranceCompany = ''
    public uploadAgentForm: FormGroup;
    registerForm: FormGroup;
    public dropdownSettingState: DropdownSettingModel;
    public dropdownSettingCity: DropdownSettingModel;
    public dropdownSettingZip: DropdownSettingModel;
    public dropdownSettingLStatus: DropdownSettingModel;
    public dropdownSettingInc: DropdownSettingModel;
    public dropdownSettingCertified: DropdownSettingModel;
    public dropdownSettingPCertified: DropdownSettingModel;
    public dropdownSettingCompany: DropdownSettingModel;
    public prefCompany = '';
    public comments: '';
    public dropdownList = [];
    dropdownSettings = {};
    public selectedItems;
    public subscribeData;
    public noFlag = false;
    public submitAgentData = {
        prefPhone: '',
        prefEmail: '',
        prefMobile: '',
        speaksSpanish: '',
        licNumber: '',
        npn: '',
        firstName: '',
        middleName: '',
        lastName: '',
        suffix: '',
        businessName: '',
        bAddr1: '',
        bAddr2: '',
        bAddr3: '',
        bCityName: '',
        bStateName: '',
        bZip: '',
        bCountyName: '',
        bCountry: '',
        bPhone: '',
        bMobile: '',
        bEmail: '',
        licType: '',
        lineAuth: '',
        licStatus: '',
        firstActiveDate: '',
        effectiveDate: '',
        expDate: '',
        dStateName: '',
        resState: '',
        pAddr1: '',
        pAddr2: '',
        pAddr3: '',
        pCityName: '',
        pStateName: '',
        pCountry: '',
        pCountyName: '',
        pZip: '',
        isCECompliant: '',
        secondPhone: ''



    };
    public editProfileId;
    public prefPhone;
    public prefMobile;
    public speaksSpanish;
    public validFlag: boolean
    public listLoader = false;
    public arryaFlag = false;
    @ViewChild('content') content: NgbModal;
    @ViewChild('infoView') infoView: NgbModal
    @ViewChild('alertModal') alertModal: NgbModal
    @ViewChild('multiSelect') multiSelect: ElementRef;

    constructor(
        public el: ElementRef,
        public authUserService: AuthUserService,
        public uploadAgentService: UploadAgentService,
        public fbuilder: FormBuilder,
        public toastr: ToastrService,
        private modalService: NgbModal,
        public subscriptionService: SubscriptionService,
        private loginService: AuthUserService,
        public router:Router
    ) {
        this.initFormData();
    }
    showFormError() { }
    onItemSelect(item: any, x) {

    }
    OnItemDeSelect(item: any) {
    }
    onItemSelectInc(item) {
    }
    ngOnInit() {
        this.getListOfCarrier();
        this.settings();
        this.initDropDown();
        let profileId = localStorage.getItem('profileId');
        this.getAgent();
        setTimeout(() => {
            this.getProfileInfo();
        }, 500);

        this.getSubsription();
    }
    showFilter() {
        this.showFilterFlag = !this.showFilterFlag;
    }
    hideFilter() {
        this.showFilterFlag = !this.showFilterFlag;
    }
    initDropDown() {
        this.dropdownSettingState = new DropdownSettingModel({
            text: 'STATE'
        });

        this.dropdownSettingCity = new DropdownSettingModel({
            text: 'CITY'
        });
        this.dropdownSettingZip = new DropdownSettingModel({
            text: 'ZIP'
        });
        this.dropdownSettingLStatus = new DropdownSettingModel({
            text: 'LICENSE STATUS'
        });
        this.dropdownSettingInc = new DropdownSettingModel({
            text: 'INC A/B'
        });
        this.dropdownSettingCertified = new DropdownSettingModel({
            text: 'CERTIFIED'
        });
        this.dropdownSettingPCertified = new DropdownSettingModel({
            text: 'P CERTIF'
        });
        this.dropdownSettingCompany = new DropdownSettingModel({
            text: 'COMPANY'
        });

        this.uploadAgentService.getState().subscribe(data => {
            this.dropdownSettingState.data = data;
        });
        this.dropdownSettingLStatus.data = [{ id: 1, itemName: 'ACTIVE' }, { id: 2, itemName: 'INACTIVE' }];
        this.dropdownSettingInc.data = [{ id: 1, itemName: 'YES' }, { id: 2, itemName: 'NO' }];
        this.dropdownSettingCertified.data = [{ id: 1, itemName: 'YES' }, { id: 2, itemName: 'NO' }];
        this.dropdownSettingPCertified.data = [{ id: 1, itemName: 'YES' }, { id: 2, itemName: 'NO' }];
    }
    showNotification(statusCode, message) {
        switch (statusCode) {
            case 200:
                this.showSucccess(message);
                break;

            default:
                this.showError(message);
                break;
        }
    }
    showSucccess(message) {
        this.toastr.success(message);
    }
    showError(message) {
        this.toastr.error(message);
    }
    showWarning() {
        this.toastr.warning('You are being warned.', 'Alert!');
    }

    getAgent() {
        let profileId = localStorage.getItem('profileId');
        this.uploadAgentService.getAgentByProfile(profileId).subscribe(
            data => {

                if (data.statusCode == 200) {
                    localStorage.setItem('ce', data.value.isCECompliant)
                    setTimeout(() => {
                        this.submitAgentData = data.value;
                        this.prefPhone = data.value.prefPhone;
                        this.prefMobile = data.value.prefMobile;
                        this.speaksSpanish = data.value.speaksSpanish;
                        this.prefCompany = data.value.prefCompany;
                        this.comments = data.value.comments;
                        this.insuranceCompany = data.value.insuranceCompany
                        this.editProfileId = profileId;
                        localStorage.setItem('npn', data.value.npn)


                    }, 500);
                }
            },
            error => {

            }
        );
    }

    submitAgent(language) {
        this.arryaFlag = true
        this.listLoader = true
        let lang;
        let langKey;
        if (language == 'false') {
            lang = "US";
            langKey = 'en';
        }
        else if (language == 'true') {
            lang = "ES";
            langKey = 'es';
        }
        let id;
        let array = []
        for (let key in this.selectedItems) {
            id = this.selectedItems[key].id;
            array.push(id)
        }

        const data = {
            "preferredMobile": this.prefMobile,
            "phone": this.prefPhone,
            "langKey": langKey,
            "lang": lang,
            "prefCompany": this.prefCompany,
            "comments": this.comments,
            "insuranceCompany": this.insuranceCompany,
            "carriers": array,
            "secondPhone": this.submitAgentData.secondPhone
        }


        this.uploadAgentService.saveAgent(data, this.editProfileId)
            .subscribe(data => {
                this.listLoader = false
                if (data.statusCode == 200) {
                    this.showNotification(200, data.message)
                    setTimeout(()=>{
                        // document.location.reload(true)
                        this.arryaFlag = false
                    },300)
                    
                }
                else if (data.statusCode == 400) {
                    this.showError(data.message);
                    this.listLoader = false
                    this.arryaFlag = false
                }
            }, error => {
                this.showError(error.message);
                this.listLoader = false
                this.arryaFlag = false
            })
    }

    deleteAgent(npn) {
        this.uploadAgentService.deleteAgentById(parseInt(npn, 10)).subscribe(
            data => {
                this.showNotification(data.statusCode, data.message);
                if (data.statusCode == 200) {
                }
            },
            error => {
                this.showError(error.message);
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    public updateForm(dataObj) {
        Object.keys(dataObj).forEach(dataKey => {
            if (this.uploadAgentForm.controls[dataKey]) {
                this.uploadAgentForm.controls[dataKey].setValue(dataObj[dataKey]);
            }
        });
    }
    initFormData() {
        this.uploadAgentForm = this.fbuilder.group({
            licNumber: [null, [Validators.required]],

            naicno: ['', [Validators.required]],
            npn: ['', [Validators.required]],
            firstName: ['', [Validators.required]],
            middleName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            suffix: ['', [Validators.required]],
            businessName: ['', [Validators.required]],
            fein: ['', [Validators.required]],
            bAddr1: ['', [Validators.required]],
            bAddr2: ['', [Validators.required]],
            bAddr3: ['', [Validators.required]],
            bCity: ['', [Validators.required]],
            bState: ['', [Validators.required]],
            bZip: ['', [Validators.required]],
            bCountry: ['', [Validators.required]],
            bPhone: ['', [Validators.required]],
            bMobile: ['', [Validators.required]],
            bEmail: ['', [Validators.required]],
            licType: ['', [Validators.required]],
            lineAuth: ['', [Validators.required]],
            licStatus: ['', [Validators.required]],
            firstActiveDate: ['', [Validators.required]],
            effectiveDate: ['', [Validators.required]],
            expDate: ['', [Validators.required]],
            domState: ['', [Validators.required]],
            resState: ['', [Validators.required]],
            pAddr1: ['', [Validators.required]],
            pAddr2: ['', [Validators.required]],
            pAddr3: ['', [Validators.required]],
            pCity: ['', [Validators.required]],
            pCounty: ['', [Validators.required]],
            pState: ['', [Validators.required]],
            pZip: ['', [Validators.required]],
            pCountry: ['', [Validators.required]],
            isCECompliant: ['', [Validators.required]],
            speaksSpanish: ['', [Validators.required]],
            prefEmail: ['', [Validators.required]],
            prefMobile: ['', [Validators.required]],
            prefPhone: ['', [Validators.required]],
            bCountyName: ['', [Validators.required]],
            pCountyName: ['', [Validators.required]],
            bCityName: ['', [Validators.required]],
            bStateName: ['', [Validators.required]],
            pCityName: ['', [Validators.required]],
            pStateName: ['', [Validators.required]],
            dStateName: ['', [Validators.required]],

        });
    }
    getSubsription() {
       this.listLoader = true
        let profileId = localStorage.getItem('profileId');
        this.subscriptionService.getPackSubscription(profileId).subscribe(
            data => {
                this.listLoader = false
                // console.log(data,"data");
                this.subscribeData = data;
                if (this.subscribeData.length == 0) {
                    this.noFlag = true;
                }


            },
            error => {
                this.noFlag = false;
                this.listLoader = false
            }
        );
    }
    getListOfCarrier() {
        this.authUserService.getCarrierList().subscribe(
            data => {
                if (data.statusCode == 200) {
                    this.dropdownList = data.value;
                }
                else {

                }
            },
            error => {
            }
        )
    }
    getSelectedItems() {
        this.selectedItems = []
        this.carriers.forEach((e1) => this.dropdownList.forEach((e2) => {
            let temobj = {
                id: Number,
                carrier: String
            }
            if (e1 === e2.id) {
                temobj.id = e1;
                temobj.carrier = e2.carrier;
                this.selectedItems.push(temobj)
            }
        }))
    }
    settings() {

        this.selectedItems = []
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'carrier',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 5,
            allowSearchFilter: true,
            enableCheckAll: false,
            limitSelection: 5,
            disabled: true
        };
    }

    getProfileInfo() {
        this.listLoader = true
        const userName = localStorage.getItem('userName');
        this.loginService.getProfile(userName).subscribe(
            data => {
                this.listLoader = false
                if (data['statusCode'] == 200) {
                    let value = data['value'];
                    this.carriers = value.carriers;
                    this.agentData = data.value.carriers.length;
                    this.validFlag = data.validToUpdateCarrier
                    console.log('flag', this.validFlag)
                    this.getSelectedItems();
                }
            },
            error => {
                this.listLoader = false
            }
        );
    }

    openInfo() {
        Swal.fire({
             html:
            "<p>Adding 'additional carriers' to your profile will increase the per slot rates for Page1 and Page 2.</p>" +
            "<table class='table'><thead><tr><th>No</th><th>0</th><th>1-3</th><th>4-5</th></tr></thead><tbody><tr><td>Page1</td><td>$100/slot</td><td>$300/slot</td><td>$400/slot</td></tr><tr><td>Page 2</td><td>$50/slot</td><td>$150/slot</td><td>$200/slot</td></tr></tbody></table>"
        })
    
      }
      dropdownEvent() {
        if (!this.validFlag) {
          this.showPaperCheckWarning();
        }
      }
      showPaperCheckWarning() {
        Swal.fire({
           position: 'top',
           html: "<p>The last update for additional companies was less than 90 day's ago kindly try later.</p>",
         } )
      }


      getCompanyNameArray(e){ 

        if(this.insuranceCompany == ""){
          this.selectedCompanyName= true;
        }else{
          this.selectedCompanyName= false;
        }
    
        this.dataCompany =[];
        this.authUserService.getComp(this.insuranceCompany).subscribe(
          data=>{
            this.dataCompany = data.value;
          },
          error=>{
    
          }
        )
      }
    
    
      selectCompnayName(value){
        this.insuranceCompany = value.name;
        this.selectedCompanyId = value.id;
        localStorage.setItem('compId',this.selectedCompanyId);
        this.selectedCompanyName= true;
      }


      // Purchased Subscription : total

      calculateTotal(){
        this.totalAmount = 0;
        for (let i in this.subscribeData) {
            // console.log(i,"cart data");
            
            let j = parseInt(i);
            if(this.agentData <= 0){
                if(this.subscribeData[j].rank == 'ONE'){
                    this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 100);
                }else{
                    this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 50);
                }
            }
    
            if(this.agentData <= 3 && this.agentData > 0){
                if(this.subscribeData[j].rank== 'ONE'){
                    this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 300);
                }else{
                    this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 150);
                }
            }
    
            if((this.agentData == 4 || this.agentData == 5) && this.agentData > 0){
                if(this.subscribeData[j].rank == 'ONE'){
                    this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 400);
                }else{
                    this.totalAmount = this.totalAmount + (this.subscribeData[j].slots* 200);
                }
            }
        }
    }

}
