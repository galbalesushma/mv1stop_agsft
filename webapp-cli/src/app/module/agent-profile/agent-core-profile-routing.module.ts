import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentCoreProfileComponent } from './agent-core-profile.component';

const routes: Routes = [
    {
        path: '',
        component: AgentCoreProfileComponent,
        children: [
            { path: '', redirectTo: '/agent-profile/viewProfile', pathMatch: 'full' },
            {
                path: 'viewProfile',
                loadChildren: './agent-profile/agent-profile.module#AgentProfileModule'
            },
            {
                path: 'viewSubscription',
                loadChildren: './view-subscription/view-subscription.module#ViewSubscriptionModule'
            },
            {
                path: 'purchaseSubscription',
                loadChildren: './purchase-subscription/purchase-subscription.module#PurchaseSubscriptionModule'
            }
            //   {
            //     path: 'subscription',
            //     loadChildren: '../subscription/subscription.module#SubscriptionModule'
            //     // loadChildren: '../subscription/subscription.module#SubscriptionModule'
            // }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AgentCoreProfileRoutingModule {}
