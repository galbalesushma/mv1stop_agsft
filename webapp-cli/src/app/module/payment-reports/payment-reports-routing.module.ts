import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentReportsComponent } from './payment-reports.component';

const routes: Routes = [
    {
        path: '',
        component: PaymentReportsComponent,
        children: [
            // { path: '', redirectTo: '/payment-reports/reports', pathMatch: 'full' },
            {
              path: 'reports',
              loadChildren: './reports/reports.module#ReportsModule'
            },
            {
              path: 'user-logs',
              loadChildren: './user-logs/user-logs.module#UserLogsModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PaymentReportRoutingModule {}
