import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserLogsComponent } from './user-logs.component';

const routes: Routes = [
    {
        path: '',
        component: UserLogsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserLogsRoutingModule {}
