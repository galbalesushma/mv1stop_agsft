import { Component, OnInit } from '@angular/core';
import { UploadAgentService } from 'app/service/upload-agent.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DropdownSettingModel, DropdownSettingSearchModel } from 'app/shared/model/dropdown.model';
import { ReportsService } from '../../../service/reports.service';
import { FileDownloadUtility } from '../../../shared/interceptor/fileDownload';

@Component({
  selector: 'app-user-logs',
  templateUrl: './user-logs.component.html',
  styleUrls: ['./user-logs.component.css'],
  providers: [FileDownloadUtility]
})
export class UserLogsComponent implements OnInit {
  totalElements = 0;
  page = 1;
  maxSize = 50;
  totalPages = 1;
  public zip = '';
  public phone = '';
  public searctType = '';
  public email = '';
  public fullName = '';
  public vehicleType = '';
  public vehicleYear = '';
  public county = '';
  public searchedDate = '';
  public company = '';
  public endDate = '';
  public startDate = ''
  public uType = '';
  userData = [];
  showFilterFlag = false;
  searchText1 = "";
  public downloader = false;

  public fName = '';
  public fnameData;
  public fnamehide = false;

  public fEmail = '';
  public fEmailData;
  public fEmailhide = false;

  public fPhone = '';
  public fPhoneData;
  public fPhonehide = false;

  public fZip = '';
  public fZipData;
  public fZiphide = false;

  public fvYear = '';
  public fvYearData;
  public fvYearhide = false;

  public fCounty = '';
  public fCountyData;
  public fCountyhide = false;

  public fCompany = '';
  public fCompanyData;
  public fCompanyhide = false;

  public npnFlag =false;
  public sortValue = '';
  public colname = '';
  public sortData = [];
  public userTypeFlag = false;
  public zipFlag =false;
  public nameTypeFlag =false;
  listLoader = false;
  mv1loader = false;



  constructor(public reportsService: ReportsService, public fileDownloadUtility: FileDownloadUtility) { }

  ngOnInit() {
    this.getUserLog();
  }

  pageChanged(event) {
    this.page = event;
    this.getUserLog()
  }

  getUserLog() {
    this.listLoader = true
    let data=[]
    this.reportsService.getUserLogs(data,this.page, this.fZip, this.fPhone, this.searctType, this.fEmail, this.fName, this.vehicleType, this.fvYear, this.fCounty, this.fCompany, this.searchedDate,this.startDate,this.endDate,this.searchText1)
      .subscribe(data => {
        this.listLoader = false
        this.userData = data.result;
        this.totalElements = data.total;
        this.totalPages = Math.ceil(data.total / 50);

      },
        error => {
          this.listLoader = false

        })
  }
  showFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }
  hideFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }
  downloadCSV(fileType) {
    this.mv1loader = true;
    if (this.sortValue == '' && this.colname == '') {
      this.sortData = [];
  } else {
      this.sortNpn(this.sortValue, this.colname);
      this.sortData = [
          {
              colName: this.colname,
              sortType: this.sortValue
          }
      ];
  }
    this.reportsService.downloadUser(this.sortData,this.fZip, this.fPhone, this.searctType, this.fEmail, this.fName, this.vehicleType, this.fvYear, this.fCounty, this.fCompany, this.searchedDate, this.endDate, this.startDate, fileType, this.searchText1)
      .subscribe(data => {
        this.mv1loader = false;
        this.downloader = false;
        if (fileType == 'CSV') {
          this.mv1loader = false;
          this.fileDownloadUtility.downloadCsv(data, 'User_Log');
        }
        else if (fileType == 'XLS') {
          this.mv1loader = false;
          this.fileDownloadUtility.downloadXls(data);
        }
      },
        error => {
          this.mv1loader = false;

        })

  }
  resetAfterFilter(){
    this.fnamehide = true;
    this.fEmailhide = true;
    this.fPhonehide = true;
    this.fZiphide = true;
    this.fvYearhide = true;
    this.fCountyhide = true;
    this.fCompanyhide = true;
  }
  searchEnter(event) {
    if (event.keyCode == 13) {
      this.FilterData();
    }
  }
  FilterData() {
    this.listLoader = true
    let data=[]
    this.reportsService.getUserLogs(data,this.page, this.fZip, this.fPhone, this.searctType, this.fEmail, this.fName, this.vehicleType, this.fvYear, this.fCounty, this.fCompany, this.searchedDate,this.startDate,this.endDate, this.searchText1)
      .subscribe(data => {
        this.listLoader = false
        this.userData = data.result;
        this.totalElements = data.total;
        this.totalPages = Math.ceil(data.total / 50);
        this.resetAfterFilter();
      },
        error => {
          this.listLoader = false
        })
  }
  resetFilterData() {
    this.clearFilters();
    this.getUserLog();
  }

  clearFilters() {
    this.searctType = '';
    this.searchedDate = '';
    this.startDate = '';
    this.endDate = ''
    this.fEmail = '';
    this.fPhone = '';
    this.fZip = '';
    this.fvYear = '';
    this.fCounty = '';
    this.fCompany = '';
    this.fName =''
  }
  changeFname(fname) {
    this.fName = fname;
    this.fnamehide = true;
  }
  changeEmail(fEmail) {
    this.fEmail = fEmail;
    this.fEmailhide = true;
  }
  changePhone(fPhone) {
    this.fPhone = fPhone;
    this.fPhonehide = true;
  }
  changeZip(fZip) {
    this.fZip = fZip;
    this.fZiphide = true;
  }

  changevYear(fvYear) {
    this.fvYear = fvYear;
    this.fvYearhide = true;
  }

  changeCounty(fCounty) {
    this.fCounty = fCounty;
    this.fCountyhide = true;
  }

  changeCompany(fCompany) {
    this.fCompany = fCompany;
    this.fCompanyhide = true;
  }
  getFirstName() {
    this.reportsService.getFname(this.fName).subscribe(data => {
      this.fnameData = data;
      this.fnamehide = false;
    });
  }

  getEmail() {
    this.reportsService.getEmail(this.fEmail).subscribe(data => {
      this.fEmailData = data;
      this.fEmailhide = false;
    });
  }

  getPhone() {
    this.reportsService.getPhone(this.fPhone).subscribe(data => {
      this.fPhoneData = data;
      this.fPhonehide = false;
    });
  }

  getZip() {
    this.reportsService.getZip(this.fZip).subscribe(data => {
      this.fZipData = data;
      this.fZiphide = false;
    });
  }

  getvYear() {
    this.reportsService.getvYear(this.fvYear).subscribe(data => {
      this.fvYearData = data;
      this.fvYearhide = false;
    });
  }

  getCounty() {
    this.reportsService.getCounty(this.fCounty).subscribe(data => {
      this.fCountyData = data;
      this.fCountyhide = false;
    });
  }

  getCompany() {
    this.reportsService.getCompany(this.fCompany).subscribe(data => {
      this.fCompanyData = data;
      this.fCompanyhide = false;
    });
  }

  emptyName() {
    if (this.fnameData == '') {
      this.fName = ''
    }
  }

  emptyEmail() {
    if (this.fEmailData == '') {
      this.fEmail = ''
    }
  }
  emptyPhone() {
    if (this.fPhoneData == '') {
      this.fPhone = ''
    }
  }
  emptyZip() {
    if (this.fZipData == '') {
      this.fZip = ''
    }
  }
  emptyYear() {
    if (this.fvYearData == '') {
      this.fvYear = ''
    }
  }
  emptyCounty() {
    if (this.fCountyData == '') {
      this.fCounty = ''
    }
  }

  sortNpn(sort, name){
    this.listLoader = true
    this.sortValue = sort;
    this.colname = name;
    if (sort == 'asc' && name == 'searchedDate') {
      this.npnFlag = true;
    } else if (sort == 'desc' && name == 'searchedDate') {
      this.npnFlag = false;
    }
    else if (sort == 'asc' && name == 'searchType') {
      this.userTypeFlag = true;
    } else if (sort == 'desc' && name == 'searchType') {
      this.userTypeFlag = false;
    }
    else if (sort == 'asc' && name == 'zip') {
      this.zipFlag = true;
    } else if (sort == 'desc' && name == 'zip') {
      this.zipFlag = false;
    }
    else if (sort == 'asc' && name == 'name') {
      this.nameTypeFlag = true;
    } else if (sort == 'desc' && name == 'name') {
      this.nameTypeFlag = false;
    }
    const data = [
      {
        colName: name,
        sortType: sort
      }
    ];
    this.sortData = data;
    this.reportsService.getUserLogs(this.sortData,this.page, this.fZip, this.fPhone, this.searctType, this.fEmail, this.fName, this.vehicleType, this.fvYear, this.fCounty, this.fCompany, this.searchedDate,this.startDate,this.endDate, this.searchText1)
      .subscribe(data => {
        this.listLoader = false
        this.userData = data.result;
        this.totalElements = data.total;
        this.totalPages = Math.ceil(data.total / 50);

      },
        error => {
          this.listLoader = false
        })
  }
}
