import { Component, OnInit, ViewChild } from '@angular/core';
import { UploadAgentService } from 'app/service/upload-agent.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DropdownSettingModel, DropdownSettingSearchModel } from 'app/shared/model/dropdown.model';
import { ReportsService } from '../../../service/reports.service';
import { FileDownloadUtility } from '../../../shared/interceptor/fileDownload';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
  providers: [FileDownloadUtility]
})
export class ReportsComponent implements OnInit {
  page = 1;
  totalElements = 1;
  maxSize = 50;
  totalPages = 1;
  toDate = '';
  iNCStatus = '';
  county = '';
  incAB = '';
  lname = '';
  isn = '';
  srep = '';
  reportsData = [];

  searchText1 = "";
  showFilterFlag = false;
  public downloader = false;
  public dropdownSettingState: DropdownSettingModel;
  public selectedItems;
  public selectedstateId = '';
  public disabled = false;
  public selectedStateName = '';

  public countyName = '';
  public zipHide = false;
  public cityHide = false;
  public fnamehide = false;
  public lnamehide = false;
  public buisnesshide = false;
  public npnhide = false;
  public isnhide = false;
  public companyhide = false;
  public countyHide = false;
  public options;
  public fZipcode = '';
  public fCity = '';
  public getZipData;
  public cityData;
  public fName = '';
  public fnameData;
  public lName = '';
  public lnameData = '';
  public npnData;
  public isnData;
  public fnpn = '';
  public fisn = '';
  public companyData;
  public buisnessData;
  public fcompany = '';
  public licStatus = '';
  public incStatus = '';
  public uType = ''
  public startDate = '';
  public endDate = '';
  public refby = ''
  public uTypeFlag = false;
  public facType = false;
  public payment_date = '';
  public stateData = [];
  public npnFlag = false;
  public sortValue = '';
  public colname = '';
  public sortData = [];
  public nameFlag = false;
  public lastnameFlag = false;
  public userTypeFlag = false;
  public amountFlag = false;
  listLoader = false;
  mv1loader = false;




  constructor(public reportsService: ReportsService, public uploadAgentService: UploadAgentService, public fileDownloadUtility: FileDownloadUtility) { }

  ngOnInit() {
    this.getReports();
    this.init();
    this.reset()
  }
  reset() {
    this.zipHide = true;
    this.countyHide = true;
    this.cityHide = true;
    this.fnamehide = true;
    this.lnamehide = true;
    this.npnhide = true;
    this.companyhide = true;
    this.countyName = '';
    this.uType = '';
    this.fisn = '';
  }
  onItemSelect(item: any, x) {
    this.selectedstateId = item.id;
    this.selectedStateName = item.itemName;
    if (this.selectedstateId != null || this.selectedstateId != '' || this.selectedstateId != null) {
      this.disabled = true;
    } else {
      this.disabled = false;
    }
  }
  OnItemDeSelect(item: any, x) {
   
  }
  onSelectAll(items: any, x) {
   
  }
  onDeSelectAll(items: any, x) {
    
  }

  getReports() {
    this.listLoader = true
    let data = [];
    this.reportsService
      .getReports(data, this.maxSize, this.page, this.selectedStateName,
        this.county, this.fZipcode, this.fCity, this.incStatus,
        this.licStatus, this.uType, this.fnpn, this.fisn, this.fName,
        this.lName, this.fcompany, this.startDate, this.endDate,
        this.payment_date, this.refby, this.searchText1)
      .subscribe(
        data => {
          this.listLoader = false
          this.reportsData = data.result;
          this.totalElements = data.total;
          this.totalPages = Math.ceil(data.total / 50);
        },
        error => {
          this.listLoader = false
         }
      );
  }
  pageChanged(event) {
    this.page = event;
    this.getReports();
  }
  showFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }
  hideFilter() {
    this.showFilterFlag = !this.showFilterFlag;
  }
  init() {
    this.dropdownSettingState = new DropdownSettingModel({
      text: 'STATE'
    });
    this.uploadAgentService.getState().subscribe(data => {
      this.stateData = data;
    });
  }

  getCountyList() {
    this.uploadAgentService.getCounties(this.selectedstateId, this.countyName).subscribe(data => {
      this.options = data;
      this.countyHide = false;
    });
  }
  getZipList() {
    this.uploadAgentService.getZip(this.fZipcode).subscribe(data => {
      this.getZipData = data;
      this.zipHide = false;
    });
  }
  getCityList() {
    this.uploadAgentService.getCity(this.selectedstateId, this.fCity).subscribe(data => {
      this.cityData = data;
      this.cityHide = false;
    });
  }
  getFirstName() {
    this.uploadAgentService.getFname(this.fName).subscribe(data => {
      this.fnameData = data;
      this.fnamehide = false;
    });
  }
  getLastName() {
    this.uploadAgentService.getLname(this.lName).subscribe(data => {
      this.lnameData = data;
      this.lnamehide = false;
    });
  }
  getnpnList() {

    this.uploadAgentService.getNpn(this.fnpn).subscribe(data => {
      this.npnData = data;
      this.npnhide = false;
    });
  }
  getisnList() {
    this.uploadAgentService.getIsn(this.fisn).subscribe(data => {
      this.isnData = data;
      this.isnhide = false;
    });
  }
  getBuisnessList() {
    this.uploadAgentService.getBuisnessList(this.fcompany).subscribe(data => {
      this.buisnessData = data;
      this.buisnesshide = false;
    });
  }

  changeCounty(name) {
    this.countyName = name;
    this.countyHide = true;
  }

  changezip(zipcode) {
    this.fZipcode = zipcode;
    this.zipHide = true;
  }
  changeCity(city) {
    this.fCity = city;
    this.cityHide = true;
  }
  changeFname(fname) {
    this.fName = fname;
    this.fnamehide = true;
  }
  changeLname(lname) {
    this.lName = lname;
    this.lnamehide = true;
  }
  changenpn(npn) {
    this.fnpn = npn;
    this.npnhide = true;
  }
  changeisn(isn) {
    this.fisn = isn;
    this.isnhide = true;
  }
  changeCompany(bname) {
    this.fcompany = bname;
    this.buisnesshide = true;
  }
  userType(type) {
    if (type == 'AGENT') {
      this.uTypeFlag = true
      this.facType = false;
      this.fisn = '';
    }
    else if (type == 'FACILITY') {
      this.uTypeFlag = false;
      this.facType = true
      this.fnpn = '';
    }
    else {
      this.uTypeFlag = false;
      this.facType = false

    }
  }
  downloadCSV(fileType) {
    this.mv1loader = true
    this.downloader = true;
    if (this.sortValue == '' && this.colname == '') {
      this.sortData = [];
  } else {
      this.sortNpn(this.sortValue, this.colname);
      this.sortData = [
          {
              colName: this.colname,
              sortType: this.sortValue
          }
      ];
  }
    this.reportsService
      .download(
        this.sortData,
        this.startDate,
        this.toDate,
        this.fZipcode,
        this.licStatus,
        this.selectedstateId,
        this.countyName,
        this.fCity,
        this.incStatus,
        this.fName,
        this.lName,
        this.fnpn, this.fisn,
        this.fcompany,
        this.payment_date,
        this.uType, this.srep,
        fileType,
        this.searchText1

      )
      .subscribe(
        data => {
          this.mv1loader = false;
          this.downloader = false;
          if (fileType == 'CSV') {
            this.fileDownloadUtility.downloadCsv(data, 'Payment_Report');
            this.mv1loader = false
          }
          else if (fileType == 'XLS') {
            this.fileDownloadUtility.downloadXls(data);
            this.mv1loader = false
          }
        },
        error => {
          this.downloader = false;
          this.mv1loader = false
        }
      );
  }
  resetAfterFilter() {
    this.buisnesshide = true;
    this.isnhide = true;
    this.npnhide = true;
    this.lnamehide = true;
    this.fnamehide = true;
    this.cityHide = true;
    this.zipHide = true;
    this.countyHide = true;
  }
  searchEnter(event) {
    if (event.keyCode == 13) {
      this.FilterData();
    }
  }
  FilterData() {
    this.listLoader = true
    let data = [];
    let sDate = '';
    let eDate = '';
    let pDate = '';
    if (this.startDate != '') {
      sDate = this.startDate
    }
    if (this.endDate != '') {
      eDate = this.endDate
    }
    if (this.payment_date != '') {
      pDate = this.payment_date;
      
    }
    this.reportsService
      .getReports(
        data,
        this.maxSize,
        this.page,
        this.selectedstateId,
        this.countyName,
        this.fZipcode,
        this.fCity,
        this.incStatus,
        this.licStatus,
        this.uType,
        this.fnpn,
        this.fisn,
        this.fName,
        this.lName,
        this.fcompany,
        sDate,
        eDate,
        pDate,
        this.refby,
        this.searchText1
      )
      .subscribe(
        data => {
          if (data) {
            this.listLoader = false
            this.reportsData = data.result;
            let reportData = data.total;
            this.totalElements = data.total;
            this.totalPages = Math.ceil(data.total / 50);
            this.resetAfterFilter()
          } else {
            this.reportsData = [];
            this.listLoader = false
          }
        },
        error => {
          this.listLoader = false
         }
      );
  }
  resetFilterData() {
    this.clearFormData();
    this.getReports();
  }

  clearFormData() {
    this.countyName = '';
    this.fZipcode = '';
    this.fnpn = '';
    this.fCity = '';
    this.fName = '';
    this.lName = '';
    this.fcompany = '';
    this.incStatus = '';
    this.licStatus = '';
    this.uType = '';
    this.fisn = '';
    this.payment_date = '';
    this.startDate = '';
    this.endDate = ''
    this.selectedStateName = ''
  }

  emptyCounty() {
    if (this.options == '') {
      this.countyName = ''
    }
  }
  emptyZip() {
    if (this.getZipData == '') {
      this.fZipcode = ''
    }
  }
  emptyCity() {
    if (this.cityData == '') {
      this.fCity = ''
    }
  }
  emptyNpn() {
    if (this.npnData == '') {
      this.fnpn = ''
    }
  }

  emptyIsn() {
    if (this.isnData == '') {
      this.fisn = ''
    }
  }
  emptyFname() {
    if (this.fnameData == '') {
      this.fName = ''
    }
  }
  emptyLname() {
    if (this.lnameData == '') {
      this.lName = ''
    }
  }
  emptyCompany() {
    if (this.buisnessData == '') {
      this.fcompany = ''
    }
  }

  sortNpn(sort, name) {
    this.listLoader = true
    this.sortValue = sort;
    this.colname = name;
    if (sort == 'asc' && name == 'paymentDate') {
      this.npnFlag = true;
    } else if (sort == 'desc' && name == 'paymentDate') {
      this.npnFlag = false;
    }
    else if (sort == 'asc' && name == 'firstName') {
      this.nameFlag = true;
    } else if (sort == 'desc' && name == 'firstName') {
      this.nameFlag = false;
    }
    else if (sort == 'asc' && name == 'lastName') {
      this.lastnameFlag = true;
    } else if (sort == 'desc' && name == 'lastName') {
      this.lastnameFlag = false;
    }
    else if (sort == 'asc' && name == 'uType') {
      this.userTypeFlag = true;
    } else if (sort == 'desc' && name == 'uType') {
      this.userTypeFlag = false;
    }
    else if (sort == 'asc' && name == 'amount') {
      this.amountFlag = true;
    } else if (sort == 'desc' && name == 'amount') {
      this.amountFlag = false;
    }

    const data = [
      {
        colName: name,
        sortType: sort
      }
    ];
    this.sortData = data;

    this.reportsService
      .getReports(this.sortData, this.maxSize, this.page, this.selectedStateName,
        this.county, this.fZipcode, this.fCity, this.incStatus,
        this.licStatus, this.uType, this.fnpn, this.fisn, this.fName,
        this.lName, this.fcompany, this.startDate, this.endDate,
        this.payment_date, this.refby,this.searchText1)
      .subscribe(
        data => {
          this.listLoader = false
          this.reportsData = data.result;
          this.totalElements = data.total;
          this.totalPages = Math.ceil(data.total / 50);
        },
        error => {
          this.listLoader = false
         }
      );
  }
}
