import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentReportRoutingModule } from './payment-reports-routing.module';
import { PaymentReportsComponent } from './payment-reports.component';

@NgModule({
    imports: [CommonModule, PaymentReportRoutingModule],
    declarations: [PaymentReportsComponent]
})
export class PaymentReportsModule {}
