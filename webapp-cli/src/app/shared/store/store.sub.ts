import { Subscription, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { SUBSCRIPTION } from 'app/shared/store/storeManager';

export class SharedStore {
    public activeStore: Observable<any[]>;

    constructor(private store: Store<any>) {
        this.activeStore = store.pipe(select('subscriptionDataStore'));

        
    }

    // public dispatchData(data: any) {
    //     this.store.dispatch({
    //         type: SUBSCRIPTION,
    //         payload: {
    //             cart: data
    //         }
    //     });
    // }
}
