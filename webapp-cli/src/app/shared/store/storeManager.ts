import { Action } from '@ngrx/store';
import { SubscriptionData } from './storeData';

// Cart

export const SUBSCRIPTION = 'SUBSCRIPTION';

// File Info Tab
export class StoreSubscriptionData implements Action {
    readonly type;
    constructor(public payload: SubscriptionData) {}
}

// landing tab
export function subscriptionDataStore(state: any, action: StoreSubscriptionData) {
   
    switch (action.type) {
        case SUBSCRIPTION:
            return (state = action.payload.cart);
        default:
            return state;
    }
}

export interface Action {
    type: any[];
    payload?: any;
}
