export const login = 'authenticate';
export const uploadCsv = 'csv/upload';
export const bulkUploadCsv = 'csv/bulk-edit';
export const getAllAgent = 'agent/all/1';
export const getAllAgents = 'agent/all';
export const getAgentById = 'agent/get';
export const getAgentByProfile = 'agentProfile/get';
export const deleteAgentById = 'agent/delete';
export const addAgent = 'agent/enroll';
export const packSubscriptions = 'pack-subscriptions/by-profile';
export const callingService = 'clickToCall';
export const getAllStates = 'all/states';
export const getAllCounty = 'state/counties';
export const getZipcode = 'agent/zipcode';
export const getCities = 'state/cities';
export const getAllFliter = 'agent/all/filter';
export const getNpn = 'agent/by/npn';
export const getIsn = 'facility/by/isn';
export const carrier = 'open/get/available/carriers'

export const getFirstName = 'agent/firstName';
export const getLastName = 'agent/lastName';
export const getCompany = 'search/company';
export const downloadAgent = 'agent/download/csv';
export const downloadFacilityCSV ='facility/download';
// export const downloadFacilityXLS ='facility/download/xlsx'


export const uploadFacilityCsv = 'csv/upload/facility';
export const getAllFacility = 'facility/all';
export const getFacilityById = 'facility/get';
export const deleteFacilityById = 'facility/delete';
export const addFacility = 'facility/save';
export const pricePackage = '/price-packages';
export const profile = 'profile';
export const slotsAvailable = '/slots/available';
export const cart = 'api/cart';
export const zipAreaGet = 'zip-area/get';
export const zipAreas = 'zip-areas';
export const pricePackages = 'price-packages';
export const payment = 'payment';

export const report = 'admin/payment/report';
export const downloadReport = 'download/payment/report';
export const downloadUser='download/log/report';

export const getUserLog = 'admin/log/report';
export const getLogName = 'search/log/name';
export const getFacCity = 'facility/cities';
export const getFacZip = 'facility/zip';
export const getFacfname = 'facility/fname';
export const getFaclname = 'facility/lname';
export const fIsn = 'facility/isn';
export const fbNname='facility/name';
export const fCounty = 'facility/county';
export const saveAgent='agentProfile/save'
export const facbusiness ='facility/business'
export const saveFacility='facilityProfile'
export const getfacState='facility/state';
export const getCdrList = 'cdr/list';
export const getCdrLog = 'cdr/log/list';
export const downloadCDRLog = 'cdr/download/file';
export const downloadCDRlist  = 'api/cdr/list/download';
export const getInsuranceCompany = 'insurance/companyList'
export const getInsuranceAgent = 'agent/companyList'

export const getAdminInvoice = 'api/admin/invoice/report'


