import * as serviceUrls from './service-url';

export const openApis = [
    serviceUrls.login,
    serviceUrls.uploadCsv,
    serviceUrls.bulkUploadCsv,
    serviceUrls.getAllAgent,
    serviceUrls.getAgentById,
    serviceUrls.deleteAgentById,
    serviceUrls.addAgent,
    serviceUrls.uploadFacilityCsv,
    serviceUrls.getAllFacility,
    serviceUrls.getFacilityById,
    serviceUrls.deleteFacilityById,
    serviceUrls.addFacility,
    serviceUrls.pricePackage,
    serviceUrls.profile,
    serviceUrls.slotsAvailable,
    serviceUrls.cart,
    serviceUrls.zipAreaGet,
    serviceUrls.zipAreas,
    serviceUrls.pricePackages,
    serviceUrls.payment,
    serviceUrls.getAgentByProfile,
    serviceUrls.packSubscriptions,
    serviceUrls.callingService,
    serviceUrls.getAllAgents,
    serviceUrls.getAllStates,
    serviceUrls.getAllCounty,
    serviceUrls.getZipcode,
    serviceUrls.getCities,
    serviceUrls.getAllFliter,
    serviceUrls.getNpn,
    serviceUrls.getLastName,
    serviceUrls.getFirstName,
    serviceUrls.getCompany,
    serviceUrls.downloadAgent,
    serviceUrls.report,
    serviceUrls.getIsn,
    serviceUrls.getUserLog,
    serviceUrls.downloadReport,
    serviceUrls.getUserLog,
    serviceUrls.downloadUser,
    serviceUrls.getLogName,
    serviceUrls.getFacCity,
    serviceUrls.getFacZip,
    serviceUrls.getFacfname,
    serviceUrls.getFaclname,
    serviceUrls.fIsn,
    serviceUrls.downloadFacilityCSV,
    serviceUrls.fbNname,
    serviceUrls.fCounty,
    serviceUrls.saveAgent,
    serviceUrls.facbusiness,
    serviceUrls.saveFacility,
    serviceUrls.getfacState,
    serviceUrls.getCdrList,
    serviceUrls.getCdrLog,
    serviceUrls.downloadCDRLog,
    serviceUrls.downloadCDRlist,
    serviceUrls.getInsuranceCompany,
    serviceUrls.getInsuranceAgent,
    serviceUrls.getAdminInvoice,
    serviceUrls.carrier
];
