export class FileDownloadUtility {
    downloadCsv(data: any, filename: string) {
        // let parsedResponse = data.text();
        let blob = new Blob([data], { type: 'text/csv' });
        let url = window.URL.createObjectURL(blob);

        if (navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, filename + '.csv');
        } else {
            let a = document.createElement('a');
            a.href = url;
            a.download = filename + '.csv';
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        }
        window.URL.revokeObjectURL(url);
    }

    downloadXls(data) {

      let blob = new Blob([data],{type: 'application/vnd.ms-excel'});
      let link = document.createElement('a');
      link.setAttribute('href', 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(data));
      link.href = window.URL.createObjectURL(blob);
      link.download =  'Payment_Report.xls';
      link.click();

    }
    downloadFacXls(data) {

      let blob = new Blob([data],{type: 'application/vnd.ms-excel'});
      let link = document.createElement('a');
      link.setAttribute('href', 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(data));
      link.href = window.URL.createObjectURL(blob);
      link.download =  'Facility.xls';
      link.click();

    }
    downloadagentXls(data) {

      let blob = new Blob([data],{type: 'application/vnd.ms-excel'});
      let link = document.createElement('a');
      link.setAttribute('href', 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(data));
      link.href = window.URL.createObjectURL(blob);
      link.download =  'Agent.xls';
      link.click();

    }



}
