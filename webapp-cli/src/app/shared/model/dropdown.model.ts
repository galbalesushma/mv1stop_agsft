export class DropdownSettingModel {
    public setting = {
        singleSelection: true,
        text: 'SEARCH',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        enableFilterSelectAll: false,
        classes: 'myclass custom-class',
        maxHeight: 150,
        badgeShowLimit: 1,
        disabled: false
    };
    public data;
    public selectedItems;

    constructor(data) {
        if (data) {
            for (const key in data) {
                if (data.hasOwnProperty(key)) {
                    this.setting[key] = data[key];
                }
            }
        }

        this.data = [];
    }
}

export class DropdownSettingSearchModel {
    public setting = {
        singleSelection: false,
        text: 'SEARCH',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        enableFilterSelectAll: false,
        classes: 'myclass custom-class',
        maxHeight: 150,
        badgeShowLimit: 1,
        disabled: false
    };
    public data;
    public selectedItems;

    constructor(data) {
        if (data) {
            for (const key in data) {
                if (data.hasOwnProperty(key)) {
                    this.setting[key] = data[key];
                }
            }
        }

        this.data = [];
    }
}
