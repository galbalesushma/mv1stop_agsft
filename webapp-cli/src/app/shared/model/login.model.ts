export class LoginUserModel {
    public username: string;
    public password: string;
    public rememberMe: boolean;

    constructor() {
        this.username = '';
        this.password = null;
        this.rememberMe = false;
    }
}
