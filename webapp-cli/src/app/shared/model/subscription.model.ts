export class PackageModel {
    public packageId: Number;
    public zipAreaId: Number;
    public slots: Number;
    public duration: string;
}
export class SubscriptionModel {
    public profileId: Number;
    public items: Array<PackageModel>;

    constructor() { }
}

export class PaymentModel {
    public profileId: any = '';
    public amount: any = '';
    public cartId: any = '';
    public cardNo: any = '';
    public cvv: any = '';
    public validTill: any = '';
    public firstName: any = '';
    public lastName: any = '';
    public addr: any = '';
    public street: any = '';
    public city: any = '';
    public zip: any = '';
    public state: any;
    public country: any;
    public phone: any = '';
    public email: any = '';
    public month: any = '';
    public year: any = '';
    public chequeNo: any = '';
    public txnType: any = 'paper_cheque';
    public routingNumber:any = '';
    public accountNumber:any = '';
    public accountType:any = '';

}

export class SaveSearchList {
    agentId: any = null
    profileId:any =null
    lpaId: any =null
    dmvId: any =null
    searchLogId: any =null
    home: any =null
    phone: any =null
    location:any =null
}