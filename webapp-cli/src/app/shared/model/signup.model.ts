export class SignUpUserModel {
    public login: String;
    public email: String;
    public password: String;
    public confpassword: String;
    public lang: String;
    public npn: String;
    public firstName: String;
    public lastName: String;
    public preferredEmail: String;
    public preferredMobile: String;
    public refBy: String;
    public phone: string;
    public langKey:string;
    public prefCompany:string;
    public comments:string;
    public insuranceCompany:string;
    public carriers:any
    public secondPhone:String

    constructor() {
        this.lang = '';
        this.password = null;
        this.confpassword = null;
        this.email = '';
        this.npn = '';
        this.firstName = '';
        this.lastName = '';
        this.preferredEmail = '';
        this.preferredMobile = '';
        this.refBy = '';
        this.phone = '';
        this.langKey ='';
        this.prefCompany='';
        this.comments='';
        this.insuranceCompany ='';
        this.carriers=[];
        this.secondPhone =''
    }
}

export class SignUpFacilityModel {
    public login: String;
    public email: String;
    public password: String;
    public confpassword: String;
    public lang: String;
    public isn: String;
    public firstName: String;
    public lastName: String;
    public preferredEmail: String;
    public preferredMobile: String;
    public refBy: String;
    public phone: string;
    public langKey:string;
    public prefCompany:string;
    public comments:string;
    public mechDuty:string


    constructor() {
        this.lang = '';
        this.password = null;
        this.confpassword = null;
        this.email = '';
        this.isn = '';
        this.firstName = '';
        this.lastName = '';
        this.preferredEmail = '';
        this.preferredMobile = '';
        this.refBy = '';
        this.phone = '';
        this.langKey ='';
        this.prefCompany='';
        this.comments='';
        this.mechDuty ='';
    }
}
