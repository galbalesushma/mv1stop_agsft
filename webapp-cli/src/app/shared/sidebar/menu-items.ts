import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    {
        path: '',
        title: 'Agent',
        icon: 'mdi mdi-account-card-details',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        isAgent: false,
        isFacility: false,
        isAdmin: true,
        submenu: [
            {
                path: '/user/agent/listAgent',
                title: 'Agents',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: [],
                isAgent: false,
                isFacility: false,
                isAdmin: true
            },
            {
                path: '/user/agent/modifyAgent',
                title: 'Update Agents',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: [],
                isAgent: false,
                isFacility: false,
                isAdmin: true
            },
            // {
            //     path: '/user/agent/uploadAgent',
            //     title: 'Add Agent',
            //     icon: '',
            //     class: '',
            //     label: '',
            //     labelClass: '',
            //     extralink: false,
            //     submenu: [],
            //     isAgent: false,
            //     isFacility: false,
            //     isAdmin: true
            // }
        ]
    },
    {
        path: '',
        title: 'My Profile',
        icon: 'mdi mdi-account',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        isAgent: true,
        isFacility: false,
        isAdmin: false,
        submenu: [
            {
                path: '/agent-profile/viewProfile',
                title: 'My Profile',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: [],
                isAgent: true,
                isFacility: false,
                isAdmin: false
            },
            // {
            //     path: '/agent-profile/viewSubscription',
            //     title: 'View Subscription',
            //     icon: '',
            //     class: '',
            //     label: '',
            //     labelClass: '',
            //     extralink: false,
            //     submenu: [],
            //     isAgent: true,
            //     isFacility: false,
            //     isAdmin: false
            // },
            {
                path: '/agent-profile/subscription',
                title: 'Purchase Subscription',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: [],
                isAgent: true,
                isFacility: false,
                isAdmin: false
            }
        ]
    },
    {
        path: '',
        title: 'My Profile',
        icon: 'mdi mdi-account',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        isAgent: false,
        isFacility: true,
        isAdmin: false,
        submenu: [
            {
                path: '/facility-profile/viewFacility',
                title: 'View Profile',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: [],
                isAgent: false,
                isFacility: true,
                isAdmin: false
            },
            // {
            //     path: '/facility-profile/viewSubscription',
            //     title: 'View Subscription',
            //     icon: '',
            //     class: '',
            //     label: '',
            //     labelClass: '',
            //     extralink: false,
            //     submenu: [],
            //     isAgent: false,
            //     isFacility: true,
            //     isAdmin: false
            // },
            // {
            //     path: '/facility-profile/subscription',
            //     title: 'Purchase Subscription',
            //     icon: '',
            //     class: '',
            //     label: '',
            //     labelClass: '',
            //     extralink: false,
            //     submenu: [],
            //     isAgent: false,
            //     isFacility: true,
            //     isAdmin: false
            // }
        ]
    },
    {
        path: '',
        title: 'Facility',
        icon: 'mdi mdi-account-multiple',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        isAgent: false,
        isFacility: false,
        isAdmin: true,
        submenu: [
            {
                path: '/user/facility/listFacility',
                title: 'Facilities',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: [],
                isAgent: false,
                isFacility: false,
                isAdmin: true
            },
            {
                path: '/user/facility/modifyFacility',
                title: 'Update Facilities',
                icon: '',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: [],
                isAgent: false,
                isFacility: false,
                isAdmin: true
            },
            // {
            //     path: '/user/facility/uploadFacility',
            //     title: 'Add Facility',
            //     icon: '',
            //     class: '',
            //     label: '',
            //     labelClass: '',
            //     extralink: false,
            //     submenu: [],
            //     isAgent: false,
            //     isFacility: false,
            //     isAdmin: true
            // }
        ]
    },
    {
      path: '',
      title: 'Reports',
      icon: 'mdi mdi-book-multiple',
      class: 'has-arrow',
      label: '',
      labelClass: '',
      extralink: false,
      isAgent: false,
      isFacility: false,
      isAdmin: true,
      submenu: [
          {
              path: '/user/payment-reports/reports',
              title: 'Payments',
              icon: '',
              class: '',
              label: '',
              labelClass: '',
              extralink: false,
              submenu: [],
              isAgent: false,
              isFacility: false,
              isAdmin: true
          },
          {
            path: '/user/payment-reports/user-logs',
            title: 'Leads',
            icon: '',
            class: '',
            label: '',
            labelClass: '',
            extralink: false,
            submenu: [],
            isAgent: false,
            isFacility: false,
            isAdmin: true
        }
      ]
  },
  {
    path: '/user/cdr',
    title: 'CDR',
    icon: 'mdi mdi-book',
    class: 'has-arrow',
    label: '',
    labelClass: '',
    extralink: false,
    isAgent: false,
    isFacility: false,
    isAdmin: true,
    submenu: [
      {
        path: '/user/cdr/cdrlist',
        title: 'CDR',
        icon: '',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        isAgent: false,
        isFacility: false,
        isAdmin: true,
        submenu: []
      },
      {
        path: '/user/cdr/cdrlog',
        title: 'CDR Log',
        icon: '',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        isAgent: false,
        isFacility: false,
        isAdmin: true,
        submenu: []
      },
      {
        path: '/user/cdr/paymentlog',
        title: 'Agent Invoice Report',
        icon: '',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        isAgent: false,
        isFacility: false,
        isAdmin: true,
        submenu: []
      }
    ]
},
{
    path: '/paper-cheque',
    title: 'Paper Cheque',
    icon: 'mdi mdi-newspaper',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    isAgent: false,
    isFacility: false,
    isAdmin: true,
    submenu: []
},
    // ,{
    //     path: '', title: 'Personal', icon: '', class: 'nav-small-cap', label: '', labelClass: '', extralink: true, submenu: []
    // },
    // {
    //     path: '/starter', title: 'Starter Page', icon: 'mdi mdi-gauge', class: '', label: '', labelClass: '', extralink: false, submenu: []
    // },
    // {
    //     path: '', title: 'UI Components', icon: '', class: 'nav-small-cap', label: '', labelClass: '', extralink: true, submenu: []
    // },{
    //     path: '', title: 'Component', icon: 'mdi mdi-bullseye', class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/component/accordion', title: 'Accordion', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/alert', title: 'Alert', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/carousel', title: 'Carousel', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/dropdown', title: 'Dropdown', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/modal', title: 'Modal', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/pagination', title: 'Pagination', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/poptool', title: 'Popover & Tooltip', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/progressbar', title: 'Progressbar', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/rating', title: 'Ratings', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/tabs', title: 'Tabs', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/timepicker', title: 'Timepicker', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/buttons', title: 'Button', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/cards', title: 'Card', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Menu Levels', icon: 'mdi mdi-arrange-send-backward', class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: 'javascript:void(0);', title: 'Second Level', icon: '', class: '', label: '', labelClass: '', extralink: true, submenu: [] },
    //         {
    //             path: '', title: 'Second Child', icon: '', class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //             submenu: [
    //                 { path: 'javascript:void(0);', title: 'Third 1.1', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //                 { path: 'javascript:void(0);', title: 'Third 1.2', icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //             ]
    //         },
    //     ]
    // }
];
