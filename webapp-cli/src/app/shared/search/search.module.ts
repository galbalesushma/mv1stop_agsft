import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchRoutingModule } from './search-routing.module';

import { SearchComponent } from '../search/search.component';
import { SearchFilterPipeModule } from '../pipes/searchFilter/searchFilter.module';

@NgModule({
    imports: [CommonModule, SearchRoutingModule, NgbModule, FormsModule, ReactiveFormsModule, SearchFilterPipeModule],
    declarations: [SearchComponent],
    providers: [],
    exports: [SearchComponent]
})
export class SearchModule {}
