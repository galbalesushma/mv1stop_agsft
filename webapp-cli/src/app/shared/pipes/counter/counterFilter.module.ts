﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { counterFilter } from './counterFilter.pipe';

@NgModule({
    declarations: [counterFilter],
    imports: [CommonModule],
    exports: [counterFilter]
})
export class CounterFilterPipeModule {}
