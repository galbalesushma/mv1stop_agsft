﻿import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
    name: 'counter',
    pure: false
})
@Injectable()
export class counterFilter implements PipeTransform {
    transform(items: any[], total: any , max): any[] {


          let totalItem = items.filter((item)=>{

            if(item <= total && item <= max){
              return true;
            } else {
              return false;
            }
          });


          return totalItem;
    }
}
