import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
@Injectable()
export class Authguard implements CanActivate {
    constructor(private router: Router) {}
    canActivate() {
        if (localStorage.getItem('token')) {
            return true;
        } else {
            this.router.navigate(['home']);
            return false;
        }
    }
}
