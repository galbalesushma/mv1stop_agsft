import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  public httpOptions;
  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  search(data): Observable<any> {
    const body = data;
    return this.http.get('assets/json/agent.json', this.httpOptions).pipe(catchError(this.handlError));
  }

  searchList(searchData, pageNumber): Observable<any> {
    const body = searchData;
    let company = '';
    let agent = '';
    if (body.type == 'FACILITY') {
      company = '';
      agent = '';
    }
    else if (body.type == 'INSURANCE') {
      company = searchData.company;
      agent =  searchData.lastname
    }
    else {
      company = searchData.companyName;
      agent = searchData.agentName
    }
   
    if(body.type == 'INSURANCE'){
   
     
      return this.http
      .post(environment.baseUrl + `/api/search/insurance/list/db/${pageNumber}?company=${company}&agent=${agent}`, body, this.httpOptions)
      .pipe(catchError(this.handlError));
    }else{
      
      return this.http
      .post(environment.baseUrl + `/api/search/list/db/${pageNumber}?company=${company}&agent=${agent}`, body, this.httpOptions)
      .pipe(catchError(this.handlError));
    }
    
  }

  customerService(searchData, pageNumber): Observable<any> {
    const body = searchData;
    let company = '';
    company = searchData.companyName;

    return this.http
    .post(environment.baseUrl + `/api/search/customer/service/list/db/${pageNumber}?company=${company}`, body, this.httpOptions)
    .pipe(catchError(this.handlError));
  }


  // getZip(zipcode): Observable<any> {
  //     return this.http.get(environment.baseUrl + `/api/zip-area/get/${zipcode}`, this.httpOptions).pipe(catchError(this.handlError));
  // }

  private handlError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      return throwError(error.error);
    } else {
      if (error.hasOwnProperty('error')) {
        return throwError(error.error);
      } else {
        return throwError({
          message: 'Internal Error'
        });
      }
    }
  }
}
