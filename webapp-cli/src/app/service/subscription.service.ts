import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, forkJoin, of } from 'rxjs';
import { environment } from 'environments/environment';
import { catchError, map } from 'rxjs/operators';

// import { map} from 'rxjs/observable';
// import { forkJoin } from 'rxjs/observable/forkJoin';
// import { of } from 'rxjs/observable/of';
// import {_throw} from 'rxjs/observable/throw';

@Injectable({
    providedIn: 'root'
})
export class SubscriptionService {
    public httpOptions;
    constructor(private http: HttpClient) {
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }

    addToCart(cartData): Observable<any> {
        const body = cartData;
        return this.http.post(environment.baseUrl + '/api/cart/add', body, this.httpOptions).pipe(catchError(this.handlError));
    }
    addToINCCart(cartData): Observable<any> {
        const body = cartData;
        return this.http.post(environment.baseUrl + '/api/cart/add', body, this.httpOptions).pipe(catchError(this.handlError));
    }
    updateToCart(cartData) {
        const body = cartData;
        return this.http.post(environment.baseUrl + '/api/cart/add', body, this.httpOptions).pipe(catchError(this.handlError));
    }
    removeCart(cartData): Observable<any> {
        const body = cartData;
        return this.http.post(environment.baseUrl + '/api/cart/remove', body, this.httpOptions).pipe(catchError(this.handlError));
    }

    payment(paymentInfo): Observable<any> {
        const body = paymentInfo;
        return this.http.post(environment.baseUrl + '/api/payment', body, this.httpOptions).pipe(catchError(this.handlError));
    }

    getAvailableSlot(profileId, zipCode): Observable<any> {
        return this.http
            .get(environment.baseUrl + `/api/slots/available/${profileId}/${zipCode}`, this.httpOptions)
            .pipe(catchError(this.handlError));
    }
    getCartInfo(profileId): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/cart/${profileId}`, this.httpOptions).pipe(catchError(this.handlError));
    }
    getPackage(): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/price-packages`, this.httpOptions).pipe(catchError(this.handlError));
    }
    getZip(zipcode): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/zip-area/get/${zipcode}`, this.httpOptions).pipe(catchError(this.handlError));
    }

    getZipbyId(zipId): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/zip-areas/${zipId}`, this.httpOptions).pipe(catchError(this.handlError));
    }

    getRankByPackage(pId): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/price-packages/${pId}`, this.httpOptions).pipe(catchError(this.handlError));
    }

    private handlError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            return throwError(error.error);
        } else {
            if (error.hasOwnProperty('error')) {
                return throwError(error.error);
            } else {
                return throwError({
                    message: 'Internal Error'
                });
            }
        }
    }

    getContentDataPar1(profileId, zipCode): Observable<any> {
        return forkJoin([
            this.http.get(environment.baseUrl + `/api/price-packages`, this.httpOptions).pipe(
                catchError(e =>
                    of({
                        status: 400,
                        data: e
                    })
                )
            ),
            this.http.get(environment.baseUrl + `/api/slots/available/${profileId}/${zipCode}`, this.httpOptions).pipe(
                catchError(e =>
                    of({
                        status: 400,
                        data: e
                    })
                )
            )
        ]).pipe(
            map((dataArray: any[]) => {
                let pricePackage = dataArray[0];
                let avaialbleSlot = dataArray[1];
                let slotObj = {};
                for (let slot of avaialbleSlot) {
                    slotObj[slot['rank']] = slot['slot'];
                }

                pricePackage.map(packageP => {
                    packageP['slot'] = slotObj[packageP['rank']];
                });

               

                return pricePackage;
            })
        );
    }

    getSlotsAvilable(profileId, zipCode): Observable<any> {
      return             this.http.get(environment.baseUrl + `/api/slots/available/${profileId}/${zipCode}`, this.httpOptions)
      .pipe(catchError(this.handlError));
    }
    getPackSubscription(profileId): Observable<any> {
        return this.http
            .get(environment.baseUrl + `/api/pack-subscriptions/by-profile/${profileId}`, this.httpOptions)
            .pipe(catchError(this.handlError));
    }

    callingService(ebpax, mobile): Observable<any> {
        return this.http
            .get(environment.baseUrl + `/api/clickToCall/${ebpax}/${mobile}`, this.httpOptions)
            .pipe(catchError(this.handlError));
    }

    saveAgentCallLog(data): Observable<any> {
        const body = data;
        return this.http.post(environment.baseUrl + '/api/agent/call/log', body, this.httpOptions).pipe(catchError(this.handlError));
    }
}
