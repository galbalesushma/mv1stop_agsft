import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'environments/environment';
import { catchError } from 'rxjs/operators';
import { LoginUserModel } from 'app/shared/model/login.model';
@Injectable({
    providedIn: 'root'
})
export class AuthUserService {
    public httpOptions;
    constructor(private http: HttpClient) {
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }
    getProfile(userName):Observable<any> {
        return this.http.get(environment.baseUrl + `/api/profile/${userName}`, this.httpOptions).pipe(catchError(this.handlError));
    }
    loginUser(userInput: LoginUserModel): Observable<any> {
        const body = userInput;
        return this.http.post(environment.baseUrl + '/api/authenticate', body, this.httpOptions).pipe(catchError(this.handlError));
    }

    signUp(data) {
        const body = data;
        return this.http.post(environment.baseUrl + '/api/open/agent/signup', body, this.httpOptions).pipe(catchError(this.handlError));
    }
    verifySignUp(data): Observable<any> {
        const body = data;
        return this.http.post(environment.baseUrl + '/api/open/agent/details', body, this.httpOptions).pipe(catchError(this.handlError));
    }
    signUpFacility(data) {
        const body = data;
        return this.http.post(environment.baseUrl + '/api/open/facility/signup', body, this.httpOptions).pipe(catchError(this.handlError));
    }
    activateUser(key) {
        return this.http
            .get(environment.baseUrl + '/api/open/agent/activate?key=' + key, this.httpOptions)
            .pipe(catchError(this.handlError));
    }
    resetUserPassword(data): Observable<any> {
        const body = data;
        return this.http
            .post(environment.baseUrl + '/api/account/reset-password/finish', body, this.httpOptions)
            .pipe(catchError(this.handlError));
    }

    private handlError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            return throwError(error.error);
        } else {
            if (error.hasOwnProperty('error')) {
                return throwError(error.error);
            } else {
                return throwError({
                    message: 'Internal Error'
                });
            }
        }
    }
    verifynpn(npn): Observable<any> {
        return this.http.get(environment.baseUrl + '/api/open/isExist/npn?npn=' + npn, this.httpOptions).pipe(catchError(this.handlError));
    }
    verifyisn(isn): Observable<any> {
        return this.http.get(environment.baseUrl + '/api/open/isExist/isn?isn=' + isn, this.httpOptions).pipe(catchError(this.handlError));
    }
    resetPassword(email): Observable<any> {
        const body = [];
        return this.http.post(environment.baseUrl + '/api/account/reset-password/init/' + email, body, this.httpOptions).pipe(catchError(this.handlError));
    }
    getCompanyList(zip): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/insurance/companyList/${zip}`, this.httpOptions).pipe(catchError(this.handlError));
    }

    getAgentList(zip): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/agent/companyList/${zip}`, this.httpOptions).pipe(catchError(this.handlError));
    }

    getComList(zip,cname): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/insurance/companyList?zip=${zip}&text=${cname}`, this.httpOptions).pipe(catchError(this.handlError));
    }
    getAgeList(zip,aname): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/agent/companyList?zip=${zip}&text=${aname}`, this.httpOptions).pipe(catchError(this.handlError));
    }
    getLastName(name): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/agent/companyListByLastName/${name}`, this.httpOptions).pipe(catchError(this.handlError));
    }
    getComp(cname): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/insurance/companyListByName/${cname}`, this.httpOptions).pipe(catchError(this.handlError));
    }
    getLpA(address): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/insurance/LpaList/${address}`, this.httpOptions).pipe(catchError(this.handlError));
    }
    getDMV(address): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/insurance/DmvList/${address}`, this.httpOptions).pipe(catchError(this.handlError));
    }
    saveInsurance(data): Observable<any> {
        const body = data;
        return this.http.post(environment.baseUrl + `/api/search-insurance-logs`,body,this.httpOptions).pipe(catchError(this.handlError));
    }
    getCarrierList(): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/open/get/available/carriers`, this.httpOptions).pipe(catchError(this.handlError));
    }
}
