import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { environment } from 'environments/environment';
import 'rxjs/add/observable/forkJoin';
import { Response, Headers, Http, ResponseContentType } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class UploadAgentService {
  public httpOptions;
  public requestOption;
  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      return throwError(error.error);
    } else {
      if (error.hasOwnProperty('error')) {
        return throwError(error.error);
      } else {
        return throwError({
          message: 'Internal Error'
        });
      }
    }
  }

  uploadCSV(fileToUpload): Observable<any> {
    return this.http
      .request(new HttpRequest('POST', environment.baseUrl + '/api/csv/upload', fileToUpload, { reportProgress: true }))
      .pipe(catchError(this.handleError));
  }

  uploadBulkCSV(fileToUpload): Observable<any> {
    return this.http
      .request(new HttpRequest('POST', environment.baseUrl + '/api/csv/bulk-edit', fileToUpload, { reportProgress: true }))
      .pipe(catchError(this.handleError));
  }

  getAgentList(): Observable<any> {
    return this.http.get(environment.baseUrl + '/api/agent/all/1', this.httpOptions).pipe(catchError(this.handleError));
  }
  getAgentLists(page): Observable<any> {
    return this.http.get(environment.baseUrl + `/api/agent/all/${page}`, this.httpOptions).pipe(catchError(this.handleError));
  }
  getAgentById(id): Observable<any> {
    return this.http.get(environment.baseUrl + `/api/agent/get/${id}`, this.httpOptions).pipe(catchError(this.handleError));
  }
  getAgentByProfile(id): Observable<any> {
    return this.http.get(environment.baseUrl + `/api/agentProfile/get/${id}`, this.httpOptions).pipe(catchError(this.handleError));
  }

  deleteAgentById(id): Observable<any> {
    const body = {
      agentIds: [id]
    };
    return this.http.post(environment.baseUrl + `/api/agent/delete`, body, this.httpOptions).pipe(catchError(this.handleError));
  }

  addAgent(agentData: any): Observable<any> {
   
    return this.http.post<any>(environment.baseUrl + '/api/agent/enroll', agentData, this.httpOptions);
  }

  /* Get State List*/

  getState(): Observable<any> {
    return this.http.get(environment.baseUrl + `/api/all/states`, this.httpOptions).pipe(catchError(this.handleError));
  }

  /* Get City List*/

  getCity(stateId, searchText): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/state/cities?search=${searchText}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  /* Get Counties */

  getCounties(stateId, searchText): Observable<any> {
    return this.http
      .get(environment.baseUrl + '/api/state/counties?search=' + searchText, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  /* Get Zip List*/

  getZip(zipcode): Observable<any> {
    return this.http
      .get(environment.baseUrl + '/api/agent/zipcode?search=' + zipcode, this.httpOptions)
      .pipe(catchError(this.handleError));
  }
  /* Get Company List*/
  getCompany(comapny): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/search/company?search=${comapny}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }
  getFname(firstName): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/agent/firstName?search=${firstName}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }
  getLname(lastName): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/agent/lastName?search=${lastName}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }
  getNpn(npn): Observable<any> {
    return this.http.get(environment.baseUrl + `/api/agent/by/npn?search=${npn}`, this.httpOptions).pipe(catchError(this.handleError));
  }
  getIsn(isn): Observable<any> {
    return this.http.get(environment.baseUrl + `/api/facility/by/isn?search=${isn}`, this.httpOptions).pipe(catchError(this.handleError));
  }
  getBuisnessList(bname) {
    return this.http
    .get(environment.baseUrl + `/api/search/company?search=${bname}`, this.httpOptions)
    .pipe(catchError(this.handleError));
  }
  
  getFilterAgentList(data, page, state,county, city, zip,npn, licStatus, incAB, company, firstName, lastName, searchText): Observable<any> {
    let pageSize = 50;
    let isCustom = false;
    const body = data;
    return this.http
      .post(
        environment.baseUrl +
        `/api/agent/all/filter?page=${page}&state=${state}&county=${county}&city=${city}&zip=${zip}&npn=${npn}&licStatus=${licStatus}&incAB=${incAB}&company=${company}&fName=${firstName}&lName=${lastName}&pageSize=${pageSize}&isCustom=${isCustom}&generalSearch=${searchText}`,
        body,
        this.httpOptions
      )
      .pipe(catchError(this.handleError));
  }

  getAgentAllAgentList(page, state,county, city, zip,npn, licStatus, incAB, company, firstName, lastName): Observable<any> {
    let pageSize = 50;
    let isCustom = false;
    const body = [];
    return this.http
      .post(
        environment.baseUrl +
        `/api/agent/all/filter?page=${page}&state=${state}&county=${county}&city=${city}&zip=${zip}&npn=${npn}&licStatus=${licStatus}&incAB=${incAB}&company=${company}&fName=${firstName}&lName=${lastName}&pageSize=${pageSize}&isCustom=${isCustom}`,
        body,
        this.httpOptions
      )
      .pipe(catchError(this.handleError));
  }

  // api/agent/all/filter?page=1&state=North Carolina&city=Cary&zip=27509&licStatus=yes&incAB=yes&cerfitied=yes&preCerfitied=yes&company=abc

  uploadData(file: Array<any>): Observable<any> {
    const uploadUrl = '/api/csv/upload';
    const requestFileArray = [];

    for (const fileList of file) {
      requestFileArray.push(
        this.http
          .request(new HttpRequest('POST', environment.baseUrl + '/api/csv/upload', fileList, { reportProgress: true }))
          .pipe(catchError(this.handleError))
      );
    }

    return Observable.forkJoin(requestFileArray).map((data: any[]) => {
      const dataObj = {};
      for (let i = 0; i < requestFileArray.length; i++) {
        dataObj['file' + i] = requestFileArray[i];
      }

      return dataObj;
    });
  }

  downloadAgentCSV(fileType,data, state, city, zip,npn, licStatus, incAB, company, firstName, lastName,generalSearch): Observable<any> {
    const body = data;
    if (fileType == 'XLS') {
      fileType = 'xlsx'
    }
    else if (fileType == 'CSV') {
      fileType = 'CSV'
    }
    const headers = new HttpHeaders();
    let requestOptions = {
      headers: headers
    };
    requestOptions['responseType'] = 'blob';
    return this.http
      .post(
        environment.baseUrl +
        `/api/agent/download/csv?state=${state}&city=${city}&zip=${zip}&npn=${npn}&licStatus=${licStatus}&incAB=${incAB}&company=${company}&fName=${firstName}&lName=${lastName}&fileType=${fileType}&generalSearch=${generalSearch}`,
        body,
        requestOptions
      )
      .pipe(catchError(this.handleError));
  }

  //   this
  //   .http
  //   .get(environment.baseUrl + uploadUrl)
  //   .catch(error => Observable.of({response_body: ''})),
  // this
  //   .http
  //   .get(environment.baseUrl + uploadUrl)
  //   .catch(error => Observable.of({response_body: ''})),
  // this
  //   .http
  //   .get(environment.baseUrl + uploadUrl)
  //   .catch(error => Observable.of({response_body: ''})),
  // this
  //   .http
  //   .get(environment.baseUrl + '/api/GetReceivableFrequencies')
  //   .catch(error => Observable.of({response_body: ''})),
  // this
  //   .http
  //   .get(environment.baseUrl + '/api/GetReceivableFileFormats')
  //   .catch(error => Observable.of({response_body: ''})),
  // this
  //   .http
  //   .get(environment.baseUrl + '/api/GetReceivableDataTypes')
  //   .catch(error => Observable.of({response_body: ''}))

  // saveAgent(data,profileId):Observable<any>{
  //   const body = data;
  //   const headers = new HttpHeaders();
  //   let requestOptions = {
  //     headers: headers
  //   };
  //   return this.http
  //     .post(
  //       environment.baseUrl +
  //       `api/agent/agentProfile/save/${profileId}`,
  //       body,
  //       requestOptions
  //     )
  //     .pipe(catchError(this.handleError));
  // }
  saveAgent(data,profileId):Observable<any>{
    const body = data;
    return this.http
      .post(
        environment.baseUrl +
        `/api/agentProfile/save/${profileId}`,
        body,
        this.httpOptions
      )
      .pipe(catchError(this.handleError));
  }
}
