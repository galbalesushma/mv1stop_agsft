import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { environment } from 'environments/environment';
import 'rxjs/add/observable/forkJoin';

@Injectable({
    providedIn: 'root'
})
export class UploadFacilityService {
    public httpOptions;

    constructor(private http: HttpClient) {
        // let token = localStorage.getItem('token');
        // const token; //your token
        // if (token) {
        this.httpOptions = {
            // reportProgress: true
            // ,
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
                // 'Authorization': token
            })
        };
        // }
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            return throwError(error.error);
        } else {
            if (error.hasOwnProperty('error')) {
                return throwError(error.error);
            } else {
                return throwError({
                    message: 'Internal Error'
                });
            }
        }
    }

    uploadCSV(fileToUpload): Observable<any> {
        return this.http
            .request(new HttpRequest('POST', environment.baseUrl + '/api/csv/upload/facility', fileToUpload, { reportProgress: true }))
            .pipe(catchError(this.handleError));
    }

    uploadBulkCSV(fileToUpload): Observable<any> {
        return this.http
            .request(new HttpRequest('POST', environment.baseUrl + '/api/csv/upload/facility', fileToUpload, { reportProgress: true }))
            .pipe(catchError(this.handleError));
    }

    getFacilityList(data,page,isn,city,zip,incStatus,incAB,fName,county,generalSearch): Observable<any> {
        let pageSize = 50;
        let isCustom = false;
        const body =data
        return this.http
            .post(environment.baseUrl + '/api/facility/all/filter?page=' + page + '&isn='+isn+ '&city=' + city + '&zip=' + zip +'&incStatus=' +incStatus + '&incAB=' + incAB + '&bName=' +fName + '&county=' + county + '&pageSize=' +pageSize + '&isCustom=' + isCustom +'&generalSearch='+generalSearch, body,)
            .pipe(catchError(this.handleError));
    }
    getFacilityById(id): Observable<any> {
        return this.http.get(environment.baseUrl + `/api/facility/get/${id}`, { reportProgress: true }).pipe(catchError(this.handleError));
    }

    deleteFacilityById(id): Observable<any> {
        const body = {
            ids: [id]
        };
        return this.http
            .post(environment.baseUrl + `/api/facility/delete`, body, { reportProgress: true })
            .pipe(catchError(this.handleError));
    }

    addFacility(agentData: any): Observable<any> {
       
        return this.http.post<any>(environment.baseUrl + '/api/facility/save', agentData, this.httpOptions);
    }

    /* Get State List*/

    // getState(): Observable<any> {
    //     return this.http.get(`assets/json/state.json`, this.httpOptions).pipe(catchError(this.handleError));
    // }
    getState(): Observable<any> {
      return this.http.get(environment.baseUrl + `/api/facility/state`, this.httpOptions).pipe(catchError(this.handleError));
    }

    /* Get City List*/

    // getCity(): Observable<any> {
    //     return this.http.get(`assets/json/city.json`, this.httpOptions).pipe(catchError(this.handleError));
    // }
    getCity(searchText): Observable<any> {
      return this.http
        .get(environment.baseUrl + `/api/facility/cities?search=${searchText}`, this.httpOptions)
        .pipe(catchError(this.handleError));
    }
    /* Get Zip List*/

    getZip(searchText): Observable<any> {
      return this.http
      .get(environment.baseUrl + `/api/facility/zip?search=${searchText}`, this.httpOptions)
      .pipe(catchError(this.handleError));
    }

    getfName(searchText): Observable<any> {
      return this.http
      .get(environment.baseUrl + `/api/facility/business?search=${searchText}`, this.httpOptions)
      .pipe(catchError(this.handleError));
    }
    getCounties(searchText): Observable<any> {
      return this.http
        .get(environment.baseUrl + '/api/facility/county?search=' + searchText, this.httpOptions)
        .pipe(catchError(this.handleError));
    }
    getIsn(searchText): Observable<any> {
      return this.http
      .get(environment.baseUrl + `/api/facility/isn?search=${searchText}`, this.httpOptions)
      .pipe(catchError(this.handleError));
    }

    uploadFacilityData(file: Array<any>): Observable<any> {
        const uploadUrl = '/api/csv/upload/facility';
        const requestFileArray = [];

        for (const fileList of file) {
            requestFileArray.push(
                this.http
                    .request(new HttpRequest('POST', environment.baseUrl + '/api/csv/upload', fileList, { reportProgress: true }))
                    .pipe(catchError(this.handleError))
            );
        }

        return Observable.forkJoin(requestFileArray).map((data: any[]) => {
            const dataObj = {};
            for (let i = 0; i < requestFileArray.length; i++) {
                dataObj['file' + i] = requestFileArray[i];
            }

            return dataObj;
        });
    }
    downloadfacility(data,city,isn, zip, incStatus, incAB,firstName, lastName,filetype,generalSearch): Observable<any> {
      const body = data;
      if (filetype == 'XLS') {
        filetype = 'xlsx'
      }
      else if (filetype == 'CSV') {
        filetype = 'CSV'
      }
      const headers = new HttpHeaders();
      let requestOptions = {
        headers: headers
      };
      requestOptions['responseType'] = 'blob';
      return this.http
        .post(
          environment.baseUrl +
          `/api/facility/download?city=${city}&isn=${isn}&zip=${zip}&incStatus=${incStatus}&incAB=${incAB}&bName=${firstName}&county=${lastName}&fileType=${filetype}&generalSearch=${generalSearch}`,
          body,
          requestOptions
        )
        .pipe(catchError(this.handleError));
    }
    saveFacility(data,profileId):Observable<any>{
      const body = data;
      return this.http
        .post(
          environment.baseUrl +
          `/api/facilityProfile/save/${profileId}`,
          body,
          this.httpOptions
        )
        .pipe(catchError(this.handleError));
    }
}
