import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable, throwError, forkJoin, of } from 'rxjs';
import { environment } from 'environments/environment';
import { catchError, map } from 'rxjs/operators';

// import { map} from 'rxjs/observable';
// import { forkJoin } from 'rxjs/observable/forkJoin';
// import { of } from 'rxjs/observable/of';
// import {_throw} from 'rxjs/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  public httpOptions;
  public requestOption;

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      return throwError(error.error);
    } else {
      if (error.hasOwnProperty('error')) {
        return throwError(error.error);
      } else {
        return throwError({
          message: 'Internal Error'
        });
      }
    }
  }


  getReports(data,pageSize, page, state, county, zip, city, incStatus, licStatus, utype, npn, isn, fname, lname, bname, sdate, edate, paymentDate, sref,generalSearch): Observable<any> {
    let isCustom = false;
    const body = data;
    return this.http
      .post(
        environment.baseUrl +
        `/api/admin/payment/report?page=${page}&state=${state}&county=${county}&zip=${zip}&city=${city}&incAB=${incStatus}&incStatus=${licStatus}&utype=${utype}&npn=${npn}&isn=${isn}&fname=${fname}&lname=${lname}&company=${bname}&fromDate=${sdate}&toDate=${edate}&payment_date=${paymentDate}&srep=${sref}&pageSize=${pageSize}&isCustom=${isCustom}&generalSearch=${generalSearch}`,
        body,
        this.httpOptions
      )
      .pipe(catchError(this.handleError));
  }


  getUserLogs(data,page, zip, phone, searcType,
    email, fullName, vehicleType, vehicleYear, county, company, searchedDate,startDate,endDate,generalSearch): Observable<any> {
    let pageSize = 50;
    let isCustom = false;
    const body = data;
    return this.http
      .post(
        environment.baseUrl +
        `/api/admin/log/report?page=${page}&zip=${zip}&phone=${phone}&searchType=${searcType}&email=${email}&fullName=${fullName}&vehicleType=${vehicleType}&vehicleYear=${vehicleYear}&county=${county}&company=${company}&searchedDate=${searchedDate}&fromDate=${startDate}&toDate=${endDate}&pageSize=${pageSize}&isCustom=${isCustom}&generalSearch=${generalSearch}`,
        body,
        this.httpOptions
      )
      .pipe(catchError(this.handleError));
  }


  download(data,fromDate, toDate, zip, incStatus, state, county, city, incAB, fname, lname, npn, isn, company, payment_date, utype, srep, fileType,generalSearch) {
    const body = data;
    if (fileType == 'XLS') {
      fileType = 'xlsx'
    }
    else if (fileType == 'CSV') {
      fileType = 'CSV'
    }
    const headers = new HttpHeaders();
    let requestOptions = {
      headers: headers
    };
    requestOptions['responseType'] = 'blob';
    return this.http
      .post(
        environment.baseUrl +
        `/api/download/payment/report?state=${state}&county=${county}&zip=${zip}&city=${city}&incAB=${incAB}&incStatus=${incStatus}&utype=${utype}&npn=${npn}&isn=${isn}&fname=${fname}&lname=${lname}&company=${company}&fromDate=${fromDate}&toDate=${toDate}&payment_date=${payment_date}&srep=${srep}&fileType=${fileType}&generalSearch=${generalSearch}`,
        body,
        requestOptions
      )
      .pipe(catchError(this.handleError));
  }

  downloadUser(data,zip,phone,searcType,email,fullName,vehicleType,vehicleYear,county,company,searchedDate,toDate,fromDate,fileType,generalSearch){
    const body = data;
    if (fileType == 'XLS') {
      fileType = 'xlsx'
    }
    else if (fileType == 'CSV') {
      fileType = 'CSV'
    }
    const headers = new HttpHeaders();
    let requestOptions = {
      headers: headers
    };
    requestOptions['responseType'] = 'blob';
    return this.http
      .post(
        environment.baseUrl +
        `/api/download/log/report?&zip=${zip}&phone=${phone}&searchType=${searcType}&email=${email}&fullName=${fullName}&vehicleType=${vehicleType}&vehicleYear=${vehicleYear}&county=${county}&company=${company}&searchedDate=${searchedDate}&fromDate=${fromDate}&toDate=${toDate}&fileType=${fileType}&generalSearch=${generalSearch}`,
        body,
        requestOptions
      )
      .pipe(catchError(this.handleError));
  }

  getFname(firstName): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/search/log/name?search=${firstName}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }


  getEmail(email): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/search/log/email?search=${email}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }


  getPhone(phone): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/search/log/phone?search=${phone}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  getZip(zip): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/search/log/zip?search=${zip}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  getvYear(year): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/search/log/vYear?search=${year}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  getCounty(county): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/search/log/county?search=${county}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  getCompany(company): Observable<any> {
    return this.http
      .get(environment.baseUrl + `/api/search/log/company?search=${company}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }
  getCdrList(page,type,generalSearch): Observable<any> {
    let pageSize = 50;
    const body = [];
    return this.http
      .post(
        environment.baseUrl +
        `/api/cdr/list?page=${page}&pageSize=${pageSize}&type=${type}&generalSearch=${generalSearch}`,
        body,
        this.httpOptions
      )
      .pipe(catchError(this.handleError));
  }
  getCdrListDownload(page,type,fileType,generalSearch): Observable<any> {
    let pageSize = 50;
    const body = [];
    const headers = new HttpHeaders();
    let requestOptions = {
      headers: headers
    };
    requestOptions['responseType'] = 'blob';
    return this.http
      .post(
        environment.baseUrl +
        `/api/cdr/list/download?type=${type}&fileType=${fileType}&generalSearch=${generalSearch}`,
        body,
        requestOptions
      )
      .pipe(catchError(this.handleError));
  }
  getCdrLogList(page,generalSearch): Observable<any> {
    let pageSize = 20;
    const body = [];
    return this.http
      .post(
        environment.baseUrl +
        `/api/cdr/log/list?page=${page}&pageSize=${pageSize}&generalSearch=${generalSearch}`,
        body,
        this.httpOptions
      )
      .pipe(catchError(this.handleError));
  }
downloadLog(id,generalSearch){
  return this.http
  .get(environment.baseUrl + `/api/cdr/download/file?id=${id}&generalSearch=${generalSearch}`, this.httpOptions)
  .pipe(catchError(this.handleError));
}


getAdminInvoiceList(page,pageSize,searchText): Observable<any> {
  return this.http
    .get(environment.baseUrl + `/api/admin/invoice/report?page=${page}&pageSize=${pageSize}&generalSearch=${searchText}`, this.httpOptions)
    .pipe(catchError(this.handleError));
}



}
