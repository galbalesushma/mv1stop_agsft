import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';
import { Authguard } from './shared/auth/auth-gurad.service';

export const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: 'user/agent',
        loadChildren: './module/agent/agent.module#AgentModule'
      },
      {
        path: 'user/payment-reports',
        loadChildren: './module/payment-reports/payment-reports.module#PaymentReportsModule'
      },
      {
        path: 'user/facility',
        loadChildren: './module/facility/facility.module#FacilityModule'
      },
      {
        path: 'facility-profile',
        loadChildren: './module/facility-profile/facility-profile.module#FacilityProfileModule'
      },
      {
        path: 'agent-profile',
        loadChildren: './module/agent-profile/agent-core-profile.module#AgentCoreProfileModule'
      },
      {
        path: ':type/subscription',
        loadChildren: './module/subscription/subscription.module#SubscriptionModule'
      },
      {
        path:'user/cdr',
        loadChildren:'./module/cdr/cdr.module#CdrModule'
      },
      {
        path: 'paper-cheque',
        loadChildren: './module/paper-cheque/paper-cheque.module#PaperChequeModule'
      }
    ],
    canActivate: [Authguard]
  },
  {
    path: ':type/login',
    loadChildren: './module/login/login.module#LoginModule'
  },
  {
    path: ':type/signup',
    loadChildren: './module/sign-up/sign-up.module#SignUpModule'
  },
  {
    path: 'activate',
    loadChildren: './module/activate/activate.module#ActivateModule'
  },
  {
    path: 'reset/finish',
    loadChildren: './module/reset-password/reset-password.module#ResetPasswordModule'
  },
  // {
  //     path: ':type/subscription',
  //     loadChildren: './module/subscription/subscription.module#SubscriptionModule'
  // },
  {
    path: 'home',
    loadChildren: './module/landing/landing.module#LandingModule'
  },
  {
    path: 'privacy-policy',
    loadChildren: './module/privacy/privacy.module#PrivacyModule'
  },
  {
    path: 'terms-conditions',
    loadChildren: './module/terms/terms.module#TermsModule'
  },
  {
    path: 'searchlist',
    loadChildren: './module/search-list/search-list.module#SearchListModule'
  },
  {
    path: 'aboutus',
    loadChildren: './module/about-us/about-us.module#AboutUsModule'
  },
 
  // {
  //     path: '**',
  //     redirectTo: '/home'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), NgbModule.forRoot()],
  exports: [RouterModule]
})
export class AppRoutingModule { }
