import { IBusinessMv } from 'app/shared/model//business-mv.model';

export interface ICategoryMv {
    id?: number;
    name?: string;
    businesses?: IBusinessMv[];
}

export class CategoryMv implements ICategoryMv {
    constructor(public id?: number, public name?: string, public businesses?: IBusinessMv[]) {}
}
