export interface IContactMv {
    id?: number;
    phone?: string;
    email?: string;
}

export class ContactMv implements IContactMv {
    constructor(public id?: number, public phone?: string, public email?: string) {}
}
