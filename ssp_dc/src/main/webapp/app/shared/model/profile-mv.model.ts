import { Moment } from 'moment';
import { ICardMv } from 'app/shared/model//card-mv.model';
import { IDonationMv } from 'app/shared/model//donation-mv.model';

export const enum ProfileType {
    USER = 'USER',
    BUSINESS = 'BUSINESS'
}

export interface IProfileMv {
    id?: number;
    firstName?: string;
    lastName?: string;
    email?: string;
    type?: ProfileType;
    userId?: number;
    createdAt?: Moment;
    updatedAt?: Moment;
    isActive?: boolean;
    isDeleted?: boolean;
    addressId?: number;
    contactsId?: number;
    cards?: ICardMv[];
    donations?: IDonationMv[];
}

export class ProfileMv implements IProfileMv {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public type?: ProfileType,
        public userId?: number,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public addressId?: number,
        public contactsId?: number,
        public cards?: ICardMv[],
        public donations?: IDonationMv[]
    ) {
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
