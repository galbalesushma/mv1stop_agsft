import { Moment } from 'moment';
import { IDonationMv } from 'app/shared/model//donation-mv.model';

export interface IPaymentGatewayMv {
    id?: number;
    name?: string;
    desc?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    isActive?: boolean;
    isDeleted?: boolean;
    donations?: IDonationMv[];
}

export class PaymentGatewayMv implements IPaymentGatewayMv {
    constructor(
        public id?: number,
        public name?: string,
        public desc?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public donations?: IDonationMv[]
    ) {
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
