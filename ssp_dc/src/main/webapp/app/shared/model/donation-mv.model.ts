import { Moment } from 'moment';

export const enum PayStatus {
    INITIATED = 'INITIATED',
    SUCCESS = 'SUCCESS',
    FAILURE = 'FAILURE',
    AWAITED = 'AWAITED'
}

export interface IDonationMv {
    id?: number;
    amount?: number;
    txnId?: string;
    orderId?: string;
    status?: PayStatus;
    gatewayResp?: string;
    donee?: string;
    schoolId?: number;
    createdAt?: Moment;
    updatedAt?: Moment;
    isDeleted?: boolean;
    profileId?: number;
}

export class DonationMv implements IDonationMv {
    constructor(
        public id?: number,
        public amount?: number,
        public txnId?: string,
        public orderId?: string,
        public status?: PayStatus,
        public gatewayResp?: string,
        public donee?: string,
        public schoolId?: number,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public isDeleted?: boolean,
        public profileId?: number
    ) {
        this.isDeleted = this.isDeleted || false;
    }
}
