import { Moment } from 'moment';
import { ICategoryMv } from 'app/shared/model//category-mv.model';

export interface IBusinessMv {
    id?: number;
    name?: string;
    discount?: string;
    website?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    verifiedAt?: Moment;
    isVerified?: boolean;
    isActive?: boolean;
    isDeleted?: boolean;
    profileId?: number;
    contactId?: number;
    addressId?: number;
    categories?: ICategoryMv[];
}

export class BusinessMv implements IBusinessMv {
    constructor(
        public id?: number,
        public name?: string,
        public discount?: string,
        public website?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public verifiedAt?: Moment,
        public isVerified?: boolean,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public profileId?: number,
        public contactId?: number,
        public addressId?: number,
        public categories?: ICategoryMv[]
    ) {
        this.isVerified = this.isVerified || false;
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
