import { Moment } from 'moment';

export interface IAddressMv {
    id?: number;
    addr?: string;
    street?: string;
    city?: string;
    zip?: string;
    region?: string;
    state?: string;
    country?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
}

export class AddressMv implements IAddressMv {
    constructor(
        public id?: number,
        public addr?: string,
        public street?: string,
        public city?: string,
        public zip?: string,
        public region?: string,
        public state?: string,
        public country?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment
    ) {}
}
