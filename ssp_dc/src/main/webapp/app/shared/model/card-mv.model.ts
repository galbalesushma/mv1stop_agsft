import { Moment } from 'moment';

export interface ICardMv {
    id?: number;
    firstName?: string;
    lastName?: string;
    number?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    validFrom?: Moment;
    validTill?: Moment;
    isActive?: boolean;
    isDeleted?: boolean;
    profileId?: number;
}

export class CardMv implements ICardMv {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public number?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public validFrom?: Moment,
        public validTill?: Moment,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public profileId?: number
    ) {
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
