import { Moment } from 'moment';

export interface ISchoolMv {
    id?: number;
    stateCode?: string;
    districtCode?: string;
    orgType?: string;
    orgId?: string;
    schoolName?: string;
    address?: string;
    city?: string;
    state?: string;
    zip?: string;
    county?: string;
    phone?: string;
    principal?: string;
    schoolType?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    isActive?: boolean;
    isDeleted?: boolean;
}

export class SchoolMv implements ISchoolMv {
    constructor(
        public id?: number,
        public stateCode?: string,
        public districtCode?: string,
        public orgType?: string,
        public orgId?: string,
        public schoolName?: string,
        public address?: string,
        public city?: string,
        public state?: string,
        public zip?: string,
        public county?: string,
        public phone?: string,
        public principal?: string,
        public schoolType?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public isActive?: boolean,
        public isDeleted?: boolean
    ) {
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
