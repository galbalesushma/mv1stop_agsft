import { LocationsAndSchoolsService } from './../common/services/locations-and-schools.service';
import { DonateRoutingModule } from './donate-routing.module';
import { DonateEffects } from './donate.effects';
import { DonateService } from './donate.service';
import { DonateComponent } from './donate.component';
import { RouterModule } from '@angular/router';
//import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HttpModule } from '@angular/http';
import { MultiSelectModule } from '../common/modules/multi-select/multi-select.module';
//import { MatAutocompleteModule, MatInputModule } from '@angular/material';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    imports: [
        //BrowserAnimationsModule,
        //MatAutocompleteModule,
        //MatInputModule,
        MultiSelectModule,
        DonateRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        //CommonModule,
        HttpModule,
        EffectsModule.forFeature([DonateEffects])
    ],
    declarations: [DonateComponent],
    exports: [DonateComponent],
    providers: [DonateService, LocationsAndSchoolsService]
})
export class DonateModule {}
