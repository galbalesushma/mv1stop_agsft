import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions, RequestOptionsArgs, Headers, Response } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable()
export class DonateService {
    baseUrl = environment.apiUrl;

    private _updateUserUrl = 'search/states/north';

    constructor(private _http: Http) {}

    getStatesList(criteria) {
        let param = {
            criteria: criteria
        };
        return this._http.post(this._updateUserUrl, param).map((res: Response) => res.json());
    }

    getDistrictsList(criteria) {
        let param = {
            criteria: criteria
        };
        return this._http.post(this._updateUserUrl, param).map((res: Response) => res.json());
    }

    getCitiesList(criteria) {
        let param = {
            criteria: criteria
        };
        return this._http.post(this._updateUserUrl, param).map((res: Response) => res.json());
    }

    getSchoolsList(criteria) {
        let param = {
            criteria: criteria
        };
        return this._http.post(this._updateUserUrl, param).map((res: Response) => res.json());
    }
}
