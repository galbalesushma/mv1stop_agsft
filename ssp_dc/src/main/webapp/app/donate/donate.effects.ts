import { LocationsAndSchoolsService } from './../common/services/locations-and-schools.service';
import { DonateService } from './donate.service';
import { map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import 'rxjs/Rx';

import * as SearchActions from '../common/actions/search.actions';
import * as ListStatesActions from '../common/actions/list-states.actions';
import * as ListDistrictsActions from '../common/actions/list-districts.actions';
import * as ListCitiesActions from '../common/actions/list-cities.actions';

@Injectable()
export class DonateEffects {
    constructor(
        private actions$: Actions,
        private donateService: DonateService,
        private locationsAndSchoolsService: LocationsAndSchoolsService
    ) {}

    @Effect()
    searchSchool$: Observable<Action> = this.actions$
        .ofType<SearchActions.Search>(SearchActions.SEARCH)
        .debounceTime(500)
        .map(action => action)
        .switchMap(payload =>
            this.locationsAndSchoolsService
                .searchSchools(payload.criteria)
                .map(results => new SearchActions.SearchSuccess(results))
                .catch(err => Observable.of({ type: SearchActions.SEARCH_FAILURE, err: err }))
        );

    @Effect()
    getStates$: Observable<Action> = this.actions$
        .ofType<ListStatesActions.ListStates>(ListStatesActions.LIST_STATES)
        .debounceTime(500)
        .map(action => action)
        .switchMap(payload =>
            this.locationsAndSchoolsService
                .getStatesList(payload.criteria)
                .map(results => new ListStatesActions.ListStatesSuccess(results))
                .catch(err => Observable.of({ type: ListStatesActions.LIST_STATES_FAILURE, err: err }))
        );

    @Effect()
    getDistricts$: Observable<Action> = this.actions$
        .ofType<ListDistrictsActions.ListDistricts>(ListDistrictsActions.LIST_DISTRICTS)
        .debounceTime(500)
        .map(action => action)
        .switchMap(payload =>
            this.locationsAndSchoolsService
                .getDistrictsList(payload.criteria)
                .map(results => new ListDistrictsActions.ListDistrictsSuccess(results))
                .catch(err => Observable.of({ type: ListDistrictsActions.LIST_DISTRICTS_FAILURE, err: err }))
        );

    @Effect()
    getCities$: Observable<Action> = this.actions$
        .ofType<ListCitiesActions.ListCities>(ListCitiesActions.LIST_CITIES)
        .debounceTime(500)
        .map(action => action)
        .switchMap(payload =>
            this.locationsAndSchoolsService
                .getCitiesList(payload.criteria)
                .map(results => new ListCitiesActions.ListCitiesSuccess(results))
                .catch(err => Observable.of({ type: ListCitiesActions.LIST_CITIES_FAILURE, err: err }))
        );
}
