import { School } from './../common/model/school';
import { LocationsAndSchoolsService } from './../common/services/locations-and-schools.service';
import { City } from './../common/model/city';
import { State } from './../common/model/state';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from './../reducers';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';
import { District } from '../common/model/district';

import * as ListStatesActions from '../common/actions/list-states.actions';
import * as ListDistrictsActions from '../common/actions/list-districts.actions';
import * as ListCitiesActions from '../common/actions/list-cities.actions';
import * as SearchSchoolsActions from '../common/actions/search.actions';
import { FormControl } from '@angular/forms';
import { ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'donate',
    templateUrl: './donate.component.html',
    styleUrls: ['./donate.component.css']
})
export class DonateComponent implements OnInit, OnDestroy {
    //dropdown related variables
    statesList = [];
    districtsList = [];
    citiesList = [];
    selectedItems = [];

    schoolSearchResult = [];

    statesSettings = { singleSelection: true, text: 'States', enableSearchFilter: true };
    districtsSettings = { singleSelection: true, text: 'County/District', enableSearchFilter: true };
    citiesSettings = { singleSelection: true, text: 'City', enableSearchFilter: true };
    schoolsSettings = { singleSelection: true, text: 'Search School By Name', enableSearchFilter: true, noDataLabel: 'Search schools...' };

    selectedState: State = new State();
    selectedDistrict: District = new District();
    selectedCity: City = new City();
    selectedSchool: School = new School();

    //observables
    obsStates: Observable<State[]>;
    subStates: Subscription;
    obsStatesErr: Observable<any>;
    subStatesErr: Subscription;

    obsDistricts: Observable<District[]>;
    subDistricts: Subscription;
    obsDistrictsErr: Observable<any>;
    subDistrictsErr: Subscription;

    obsCities: Observable<City[]>;
    subCities: Subscription;
    obsCitiesErr: Observable<any>;
    subCitiesErr: Subscription;

    obsSchools: Observable<School[]>;
    subSchools: Subscription;
    obsSchoolsErr: Observable<any>;
    subSchoolsErr: Subscription;

    constructor(private _store: Store<fromRoot.State>, private service: LocationsAndSchoolsService) {
        this.obsStates = this._store.select(fromRoot.selectListStatesSuccess);
        this.obsStatesErr = this._store.select(fromRoot.selectListStatesFailure);
        this.obsDistricts = this._store.select(fromRoot.selectListDistrictsSuccess);
        this.obsDistrictsErr = this._store.select(fromRoot.selectListDistrictsFailure);
        this.obsCities = this._store.select(fromRoot.selectListCitiesSuccess);
        this.obsCitiesErr = this._store.select(fromRoot.selectListCitiesFailure);
        this.obsSchools = this._store.select(fromRoot.selectSearchSuccess);
        this.obsSchoolsErr = this._store.select(fromRoot.selectSearchFailure);
    }

    ngOnInit() {
        let isOnInit = true;
        if (!this.statesList || this.statesList.length == 0) {
            this._store.dispatch(new ListStatesActions.ListStates('north'));
        }

        if (!this.districtsList || this.districtsList.length == 0) {
            this._store.dispatch(new ListDistrictsActions.ListDistricts('wake'));
        }

        if (!this.citiesList || this.citiesList.length == 0) {
            this._store.dispatch(new ListCitiesActions.ListCities('apex'));
        }

        this.subSchools = this.obsSchools.subscribe(res => {
            if (res) {
                this.schoolSearchResult = res;
                this.schoolSearchResult.forEach(school => {
                    school.itemName = school.school_name;
                });
            }
        });

        this.subSchoolsErr = this.obsSchoolsErr.subscribe(err => {
            if (err) {
            }
        });

        this.subStates = this.obsStates.subscribe(res => {
            if (res) {
                this.statesList = res;
                this.statesList.forEach(state => {
                    state.itemName = state.state;
                });
            }
        });

        this.subStatesErr = this.obsStatesErr.subscribe(err => {
            if (err) {
            }
        });

        this.subDistricts = this.obsDistricts.subscribe(res => {
            if (res) {
                this.districtsList = res;
                this.districtsList.forEach(district => {
                    district.itemName = district.district;
                });
            }
        });

        this.subDistrictsErr = this.obsDistrictsErr.subscribe(err => {
            if (err) {
            }
        });

        this.subCities = this.obsCities.subscribe(res => {
            if (res) {
                this.citiesList = res;
                this.citiesList.forEach(city => {
                    city.itemName = city.city;
                });
            }
        });

        this.subCitiesErr = this.obsCitiesErr.subscribe(err => {
            if (err) {
            }
        });
    }

    redirectToSSP() {
        window.location.href = 'https://web.goigi.me/schoolsafetyprogram/';
    }

    redirectToMV1() {
        window.location.href = 'https://www.mv1stop.com/';
    }

    changeState(state) {
        if (state) {
            state = state[0];
            if (state) {
                this.selectedState.id = state.id;
                this.selectedState.state = state.state;
            }
        }
    }

    changeDistrict(district) {
        if (district) {
            district = district[0];
            if (district) {
                this.selectedDistrict.id = district.id;
                this.selectedDistrict.district = district.district;
                this.selectedDistrict.state_code = district.state_code;
            }
        }
    }

    changeCity(city) {
        if (city) {
            city = city[0];
            if (city) {
                this.selectedCity.id = city.id;
                this.selectedCity.city = city.city;
                this.selectedCity.district = city.district;
                this.selectedCity.state_code = city.state_code;
            }
        }
    }

    changeSchool(school) {
        if (school) {
            let selSchool = school[0];
            this.selectedSchool = school;
            if (this.selectedSchool) delete this.selectedSchool.itemName;
        }
    }

    searchStates(term) {
        this._store.dispatch(new ListStatesActions.ListStates(term));
    }

    searchDistricts(term) {
        this._store.dispatch(new ListDistrictsActions.ListDistricts(term));
    }

    searchCities(term) {
        this._store.dispatch(new ListCitiesActions.ListCities(term));
    }

    searchSchool(criteria) {
        this._store.dispatch(new SearchSchoolsActions.Search(criteria));
    }

    myContent: any[] = [];

    ngOnDestroy() {
        if (this.subCities) this.subCities.unsubscribe();
        if (this.subCitiesErr) this.subCitiesErr.unsubscribe();
        if (this.subDistricts) this.subDistricts.unsubscribe();
        if (this.subDistrictsErr) this.subDistrictsErr.unsubscribe();
        if (this.subStates) this.subStates.unsubscribe();
        if (this.subStatesErr) this.subStatesErr.unsubscribe();
    }
}
