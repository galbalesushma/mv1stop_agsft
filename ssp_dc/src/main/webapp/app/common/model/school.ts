export class School {
    id: number;
    school_name: string;
    state: string;
    district: string;
    city: string;
    street: string;
    principal: string;
    phone: string;
    zip: number;
    itemName: string;
}
