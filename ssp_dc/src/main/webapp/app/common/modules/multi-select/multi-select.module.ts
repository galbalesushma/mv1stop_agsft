import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { MultiSelectComponent } from './multi-select.component';
import { RouterModule } from '@angular/router';
//import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        //CommonModule,
        HttpModule,
        RouterModule,
        AngularMultiSelectModule
    ],
    declarations: [MultiSelectComponent],
    exports: [MultiSelectComponent],
    providers: []
})
export class MultiSelectModule {}
