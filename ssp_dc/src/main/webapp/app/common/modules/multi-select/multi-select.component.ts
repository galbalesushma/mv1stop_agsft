import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'multi-select',
    templateUrl: './multi-select.component.html',
    styleUrls: ['./multi-select.component.css']
})
export class MultiSelectComponent implements OnInit {
    @Input('dropdownList') dropdownList;
    @Input('selectedItems') selectedItems;
    @Output() selectedItemsChanged: EventEmitter<string[]> = new EventEmitter();
    @Output() searchEvent: EventEmitter<string> = new EventEmitter();

    @Input() dropdownSettings = {};

    constructor() {}

    ngOnInit() {
        if (!this.dropdownList) {
            this.dropdownList = [];
        }

        if (!this.selectedItems) {
            this.selectedItems = [];
        }
    }

    onItemSelect(item: any) {
        this.onSelectedItemsChange();
    }

    OnItemDeSelect(item: any) {
        this.onSelectedItemsChange();
    }

    onSelectAll(items: any) {
        this.onSelectedItemsChange();
    }

    onDeSelectAll(items: any) {
        this.onSelectedItemsChange();
    }

    onSelectedItemsChange() {
        this.selectedItemsChanged.emit(this.selectedItems);
    }

    onSearch(evt: any) {
        console.log(evt.target.value);
        this.searchEvent.emit(evt.target.value);
    }
}
