export const LIST_DISTRICTS = 'LIST_DISTRICTS';
export const LIST_DISTRICTS_SUCCESS = 'LIST_DISTRICTS_SUCCESS';
export const LIST_DISTRICTS_FAILURE = 'LIST_DISTRICTS_FAILURE';

export class ListDistricts {
    readonly type = LIST_DISTRICTS;
    constructor(public criteria: any) {}
}

export class ListDistrictsSuccess {
    readonly type = LIST_DISTRICTS_SUCCESS;
    constructor(public res: any) {}
}

export class ListDistrictsFailure {
    readonly type = LIST_DISTRICTS_FAILURE;
    constructor(public err: any) {}
}

export type All = ListDistricts | ListDistrictsSuccess | ListDistrictsFailure;
