export const LIST_STATES = 'LIST_STATES';
export const LIST_STATES_SUCCESS = 'LIST_STATES_SUCCESS';
export const LIST_STATES_FAILURE = 'LIST_STATES_FAILURE';

export class ListStates {
    readonly type = LIST_STATES;
    constructor(public criteria: any) {}
}

export class ListStatesSuccess {
    readonly type = LIST_STATES_SUCCESS;
    constructor(public res: any) {}
}

export class ListStatesFailure {
    readonly type = LIST_STATES_FAILURE;
    constructor(public err: any) {}
}

export type All = ListStates | ListStatesSuccess | ListStatesFailure;
