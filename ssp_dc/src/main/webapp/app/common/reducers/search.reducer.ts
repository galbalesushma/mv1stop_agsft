import * as SearchActions from '../actions/search.actions';

export interface State {
    result: any[];
    err: any;
}

const initialState: State = {
    result: [],
    err: {}
};

export function reducer(state = initialState, action: SearchActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        case SearchActions.SEARCH_SUCCESS: {
            return {
                ...state,
                result: action.res
            };
        }

        case SearchActions.SEARCH_FAILURE: {
            return {
                ...state,
                err: action.err
            };
        }

        case SearchActions.CLEAR_SEARCH: {
            return {
                ...state,
                result: []
            };
        }

        default: {
            return state;
        }
    }
}
