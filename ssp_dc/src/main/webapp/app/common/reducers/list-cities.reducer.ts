import * as ListCitiesActions from '../actions/list-cities.actions';

export interface State {
    result: any[];
    err: any;
}

const initialState: State = {
    result: [],
    err: {}
};

export function reducer(state = initialState, action: ListCitiesActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        case ListCitiesActions.LIST_CITIES_SUCCESS: {
            return {
                ...state,
                result: action.res
            };
        }

        case ListCitiesActions.LIST_CITIES_FAILURE: {
            return {
                ...state,
                err: action.err
            };
        }

        default: {
            return state;
        }
    }
}
