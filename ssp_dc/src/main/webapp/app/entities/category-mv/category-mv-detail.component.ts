import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICategoryMv } from 'app/shared/model/category-mv.model';

@Component({
    selector: 'jhi-category-mv-detail',
    templateUrl: './category-mv-detail.component.html'
})
export class CategoryMvDetailComponent implements OnInit {
    category: ICategoryMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ category }) => {
            this.category = category;
        });
    }

    previousState() {
        window.history.back();
    }
}
