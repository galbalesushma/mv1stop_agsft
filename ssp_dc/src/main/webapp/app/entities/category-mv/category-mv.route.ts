import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CategoryMv } from 'app/shared/model/category-mv.model';
import { CategoryMvService } from './category-mv.service';
import { CategoryMvComponent } from './category-mv.component';
import { CategoryMvDetailComponent } from './category-mv-detail.component';
import { CategoryMvUpdateComponent } from './category-mv-update.component';
import { CategoryMvDeletePopupComponent } from './category-mv-delete-dialog.component';
import { ICategoryMv } from 'app/shared/model/category-mv.model';

@Injectable({ providedIn: 'root' })
export class CategoryMvResolve implements Resolve<ICategoryMv> {
    constructor(private service: CategoryMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((category: HttpResponse<CategoryMv>) => category.body));
        }
        return of(new CategoryMv());
    }
}

export const categoryRoute: Routes = [
    {
        path: 'category-mv',
        component: CategoryMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.category.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'category-mv/:id/view',
        component: CategoryMvDetailComponent,
        resolve: {
            category: CategoryMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.category.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'category-mv/new',
        component: CategoryMvUpdateComponent,
        resolve: {
            category: CategoryMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.category.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'category-mv/:id/edit',
        component: CategoryMvUpdateComponent,
        resolve: {
            category: CategoryMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.category.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const categoryPopupRoute: Routes = [
    {
        path: 'category-mv/:id/delete',
        component: CategoryMvDeletePopupComponent,
        resolve: {
            category: CategoryMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.category.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
