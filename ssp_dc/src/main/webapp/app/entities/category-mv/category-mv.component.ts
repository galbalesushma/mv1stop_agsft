import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICategoryMv } from 'app/shared/model/category-mv.model';
import { Principal } from 'app/core';
import { CategoryMvService } from './category-mv.service';

@Component({
    selector: 'jhi-category-mv',
    templateUrl: './category-mv.component.html'
})
export class CategoryMvComponent implements OnInit, OnDestroy {
    categories: ICategoryMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private categoryService: CategoryMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.categoryService.query().subscribe(
            (res: HttpResponse<ICategoryMv[]>) => {
                this.categories = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCategories();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICategoryMv) {
        return item.id;
    }

    registerChangeInCategories() {
        this.eventSubscriber = this.eventManager.subscribe('categoryListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
