import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICategoryMv } from 'app/shared/model/category-mv.model';

type EntityResponseType = HttpResponse<ICategoryMv>;
type EntityArrayResponseType = HttpResponse<ICategoryMv[]>;

@Injectable({ providedIn: 'root' })
export class CategoryMvService {
    private resourceUrl = SERVER_API_URL + 'api/categories';

    constructor(private http: HttpClient) {}

    create(category: ICategoryMv): Observable<EntityResponseType> {
        return this.http.post<ICategoryMv>(this.resourceUrl, category, { observe: 'response' });
    }

    update(category: ICategoryMv): Observable<EntityResponseType> {
        return this.http.put<ICategoryMv>(this.resourceUrl, category, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICategoryMv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICategoryMv[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
