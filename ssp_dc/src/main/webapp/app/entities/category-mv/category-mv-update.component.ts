import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ICategoryMv } from 'app/shared/model/category-mv.model';
import { CategoryMvService } from './category-mv.service';
import { IBusinessMv } from 'app/shared/model/business-mv.model';
import { BusinessMvService } from 'app/entities/business-mv';

@Component({
    selector: 'jhi-category-mv-update',
    templateUrl: './category-mv-update.component.html'
})
export class CategoryMvUpdateComponent implements OnInit {
    private _category: ICategoryMv;
    isSaving: boolean;

    businesses: IBusinessMv[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private categoryService: CategoryMvService,
        private businessService: BusinessMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ category }) => {
            this.category = category;
        });
        this.businessService.query().subscribe(
            (res: HttpResponse<IBusinessMv[]>) => {
                this.businesses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.category.id !== undefined) {
            this.subscribeToSaveResponse(this.categoryService.update(this.category));
        } else {
            this.subscribeToSaveResponse(this.categoryService.create(this.category));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICategoryMv>>) {
        result.subscribe((res: HttpResponse<ICategoryMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackBusinessById(index: number, item: IBusinessMv) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
    get category() {
        return this._category;
    }

    set category(category: ICategoryMv) {
        this._category = category;
    }
}
