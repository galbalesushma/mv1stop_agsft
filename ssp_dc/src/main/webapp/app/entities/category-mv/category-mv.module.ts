import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SspDcSharedModule } from 'app/shared';
import {
    CategoryMvComponent,
    CategoryMvDetailComponent,
    CategoryMvUpdateComponent,
    CategoryMvDeletePopupComponent,
    CategoryMvDeleteDialogComponent,
    categoryRoute,
    categoryPopupRoute
} from './';

const ENTITY_STATES = [...categoryRoute, ...categoryPopupRoute];

@NgModule({
    imports: [SspDcSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CategoryMvComponent,
        CategoryMvDetailComponent,
        CategoryMvUpdateComponent,
        CategoryMvDeleteDialogComponent,
        CategoryMvDeletePopupComponent
    ],
    entryComponents: [CategoryMvComponent, CategoryMvUpdateComponent, CategoryMvDeleteDialogComponent, CategoryMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SspDcCategoryMvModule {}
