import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CardMv } from 'app/shared/model/card-mv.model';
import { CardMvService } from './card-mv.service';
import { CardMvComponent } from './card-mv.component';
import { CardMvDetailComponent } from './card-mv-detail.component';
import { CardMvUpdateComponent } from './card-mv-update.component';
import { CardMvDeletePopupComponent } from './card-mv-delete-dialog.component';
import { ICardMv } from 'app/shared/model/card-mv.model';

@Injectable({ providedIn: 'root' })
export class CardMvResolve implements Resolve<ICardMv> {
    constructor(private service: CardMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((card: HttpResponse<CardMv>) => card.body));
        }
        return of(new CardMv());
    }
}

export const cardRoute: Routes = [
    {
        path: 'card-mv',
        component: CardMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.card.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'card-mv/:id/view',
        component: CardMvDetailComponent,
        resolve: {
            card: CardMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.card.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'card-mv/new',
        component: CardMvUpdateComponent,
        resolve: {
            card: CardMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.card.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'card-mv/:id/edit',
        component: CardMvUpdateComponent,
        resolve: {
            card: CardMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.card.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cardPopupRoute: Routes = [
    {
        path: 'card-mv/:id/delete',
        component: CardMvDeletePopupComponent,
        resolve: {
            card: CardMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.card.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
