import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICardMv } from 'app/shared/model/card-mv.model';

@Component({
    selector: 'jhi-card-mv-detail',
    templateUrl: './card-mv-detail.component.html'
})
export class CardMvDetailComponent implements OnInit {
    card: ICardMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ card }) => {
            this.card = card;
        });
    }

    previousState() {
        window.history.back();
    }
}
