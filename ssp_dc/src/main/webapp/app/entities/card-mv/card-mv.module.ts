import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SspDcSharedModule } from 'app/shared';
import {
    CardMvComponent,
    CardMvDetailComponent,
    CardMvUpdateComponent,
    CardMvDeletePopupComponent,
    CardMvDeleteDialogComponent,
    cardRoute,
    cardPopupRoute
} from './';

const ENTITY_STATES = [...cardRoute, ...cardPopupRoute];

@NgModule({
    imports: [SspDcSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [CardMvComponent, CardMvDetailComponent, CardMvUpdateComponent, CardMvDeleteDialogComponent, CardMvDeletePopupComponent],
    entryComponents: [CardMvComponent, CardMvUpdateComponent, CardMvDeleteDialogComponent, CardMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SspDcCardMvModule {}
