import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICardMv } from 'app/shared/model/card-mv.model';

type EntityResponseType = HttpResponse<ICardMv>;
type EntityArrayResponseType = HttpResponse<ICardMv[]>;

@Injectable({ providedIn: 'root' })
export class CardMvService {
    private resourceUrl = SERVER_API_URL + 'api/cards';

    constructor(private http: HttpClient) {}

    create(card: ICardMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(card);
        return this.http
            .post<ICardMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(card: ICardMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(card);
        return this.http
            .put<ICardMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ICardMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ICardMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(card: ICardMv): ICardMv {
        const copy: ICardMv = Object.assign({}, card, {
            createdAt: card.createdAt != null && card.createdAt.isValid() ? card.createdAt.toJSON() : null,
            updatedAt: card.updatedAt != null && card.updatedAt.isValid() ? card.updatedAt.toJSON() : null,
            validFrom: card.validFrom != null && card.validFrom.isValid() ? card.validFrom.toJSON() : null,
            validTill: card.validTill != null && card.validTill.isValid() ? card.validTill.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        res.body.validFrom = res.body.validFrom != null ? moment(res.body.validFrom) : null;
        res.body.validTill = res.body.validTill != null ? moment(res.body.validTill) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((card: ICardMv) => {
            card.createdAt = card.createdAt != null ? moment(card.createdAt) : null;
            card.updatedAt = card.updatedAt != null ? moment(card.updatedAt) : null;
            card.validFrom = card.validFrom != null ? moment(card.validFrom) : null;
            card.validTill = card.validTill != null ? moment(card.validTill) : null;
        });
        return res;
    }
}
