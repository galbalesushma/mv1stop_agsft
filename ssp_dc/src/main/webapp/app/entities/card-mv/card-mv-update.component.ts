import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { ICardMv } from 'app/shared/model/card-mv.model';
import { CardMvService } from './card-mv.service';
import { IProfileMv } from 'app/shared/model/profile-mv.model';
import { ProfileMvService } from 'app/entities/profile-mv';

@Component({
    selector: 'jhi-card-mv-update',
    templateUrl: './card-mv-update.component.html'
})
export class CardMvUpdateComponent implements OnInit {
    private _card: ICardMv;
    isSaving: boolean;

    profiles: IProfileMv[];
    createdAt: string;
    updatedAt: string;
    validFrom: string;
    validTill: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private cardService: CardMvService,
        private profileService: ProfileMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ card }) => {
            this.card = card;
        });
        this.profileService.query().subscribe(
            (res: HttpResponse<IProfileMv[]>) => {
                this.profiles = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.card.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.card.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        this.card.validFrom = moment(this.validFrom, DATE_TIME_FORMAT);
        this.card.validTill = moment(this.validTill, DATE_TIME_FORMAT);
        if (this.card.id !== undefined) {
            this.subscribeToSaveResponse(this.cardService.update(this.card));
        } else {
            this.subscribeToSaveResponse(this.cardService.create(this.card));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICardMv>>) {
        result.subscribe((res: HttpResponse<ICardMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProfileById(index: number, item: IProfileMv) {
        return item.id;
    }
    get card() {
        return this._card;
    }

    set card(card: ICardMv) {
        this._card = card;
        this.createdAt = moment(card.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(card.updatedAt).format(DATE_TIME_FORMAT);
        this.validFrom = moment(card.validFrom).format(DATE_TIME_FORMAT);
        this.validTill = moment(card.validTill).format(DATE_TIME_FORMAT);
    }
}
