import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContactMv } from 'app/shared/model/contact-mv.model';

@Component({
    selector: 'jhi-contact-mv-detail',
    templateUrl: './contact-mv-detail.component.html'
})
export class ContactMvDetailComponent implements OnInit {
    contact: IContactMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ contact }) => {
            this.contact = contact;
        });
    }

    previousState() {
        window.history.back();
    }
}
