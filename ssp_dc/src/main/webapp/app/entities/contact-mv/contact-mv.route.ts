import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ContactMv } from 'app/shared/model/contact-mv.model';
import { ContactMvService } from './contact-mv.service';
import { ContactMvComponent } from './contact-mv.component';
import { ContactMvDetailComponent } from './contact-mv-detail.component';
import { ContactMvUpdateComponent } from './contact-mv-update.component';
import { ContactMvDeletePopupComponent } from './contact-mv-delete-dialog.component';
import { IContactMv } from 'app/shared/model/contact-mv.model';

@Injectable({ providedIn: 'root' })
export class ContactMvResolve implements Resolve<IContactMv> {
    constructor(private service: ContactMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((contact: HttpResponse<ContactMv>) => contact.body));
        }
        return of(new ContactMv());
    }
}

export const contactRoute: Routes = [
    {
        path: 'contact-mv',
        component: ContactMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.contact.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'contact-mv/:id/view',
        component: ContactMvDetailComponent,
        resolve: {
            contact: ContactMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.contact.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'contact-mv/new',
        component: ContactMvUpdateComponent,
        resolve: {
            contact: ContactMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.contact.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'contact-mv/:id/edit',
        component: ContactMvUpdateComponent,
        resolve: {
            contact: ContactMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.contact.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const contactPopupRoute: Routes = [
    {
        path: 'contact-mv/:id/delete',
        component: ContactMvDeletePopupComponent,
        resolve: {
            contact: ContactMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.contact.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
