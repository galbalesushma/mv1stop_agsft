import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBusinessMv } from 'app/shared/model/business-mv.model';

type EntityResponseType = HttpResponse<IBusinessMv>;
type EntityArrayResponseType = HttpResponse<IBusinessMv[]>;

@Injectable({ providedIn: 'root' })
export class BusinessMvService {
    private resourceUrl = SERVER_API_URL + 'api/businesses';

    constructor(private http: HttpClient) {}

    create(business: IBusinessMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(business);
        return this.http
            .post<IBusinessMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(business: IBusinessMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(business);
        return this.http
            .put<IBusinessMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IBusinessMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IBusinessMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(business: IBusinessMv): IBusinessMv {
        const copy: IBusinessMv = Object.assign({}, business, {
            createdAt: business.createdAt != null && business.createdAt.isValid() ? business.createdAt.toJSON() : null,
            updatedAt: business.updatedAt != null && business.updatedAt.isValid() ? business.updatedAt.toJSON() : null,
            verifiedAt: business.verifiedAt != null && business.verifiedAt.isValid() ? business.verifiedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        res.body.verifiedAt = res.body.verifiedAt != null ? moment(res.body.verifiedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((business: IBusinessMv) => {
            business.createdAt = business.createdAt != null ? moment(business.createdAt) : null;
            business.updatedAt = business.updatedAt != null ? moment(business.updatedAt) : null;
            business.verifiedAt = business.verifiedAt != null ? moment(business.verifiedAt) : null;
        });
        return res;
    }
}
