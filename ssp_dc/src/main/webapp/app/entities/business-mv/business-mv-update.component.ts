import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IBusinessMv } from 'app/shared/model/business-mv.model';
import { BusinessMvService } from './business-mv.service';
import { IProfileMv } from 'app/shared/model/profile-mv.model';
import { ProfileMvService } from 'app/entities/profile-mv';
import { IContactMv } from 'app/shared/model/contact-mv.model';
import { ContactMvService } from 'app/entities/contact-mv';
import { IAddressMv } from 'app/shared/model/address-mv.model';
import { AddressMvService } from 'app/entities/address-mv';
import { ICategoryMv } from 'app/shared/model/category-mv.model';
import { CategoryMvService } from 'app/entities/category-mv';

@Component({
    selector: 'jhi-business-mv-update',
    templateUrl: './business-mv-update.component.html'
})
export class BusinessMvUpdateComponent implements OnInit {
    private _business: IBusinessMv;
    isSaving: boolean;

    profiles: IProfileMv[];

    contacts: IContactMv[];

    addresses: IAddressMv[];

    categories: ICategoryMv[];
    createdAt: string;
    updatedAt: string;
    verifiedAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private businessService: BusinessMvService,
        private profileService: ProfileMvService,
        private contactService: ContactMvService,
        private addressService: AddressMvService,
        private categoryService: CategoryMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ business }) => {
            this.business = business;
        });
        this.profileService.query({ filter: 'business-is-null' }).subscribe(
            (res: HttpResponse<IProfileMv[]>) => {
                if (!this.business.profileId) {
                    this.profiles = res.body;
                } else {
                    this.profileService.find(this.business.profileId).subscribe(
                        (subRes: HttpResponse<IProfileMv>) => {
                            this.profiles = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.contactService.query({ filter: 'business-is-null' }).subscribe(
            (res: HttpResponse<IContactMv[]>) => {
                if (!this.business.contactId) {
                    this.contacts = res.body;
                } else {
                    this.contactService.find(this.business.contactId).subscribe(
                        (subRes: HttpResponse<IContactMv>) => {
                            this.contacts = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.addressService.query({ filter: 'business-is-null' }).subscribe(
            (res: HttpResponse<IAddressMv[]>) => {
                if (!this.business.addressId) {
                    this.addresses = res.body;
                } else {
                    this.addressService.find(this.business.addressId).subscribe(
                        (subRes: HttpResponse<IAddressMv>) => {
                            this.addresses = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.categoryService.query().subscribe(
            (res: HttpResponse<ICategoryMv[]>) => {
                this.categories = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.business.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.business.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        this.business.verifiedAt = moment(this.verifiedAt, DATE_TIME_FORMAT);
        if (this.business.id !== undefined) {
            this.subscribeToSaveResponse(this.businessService.update(this.business));
        } else {
            this.subscribeToSaveResponse(this.businessService.create(this.business));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IBusinessMv>>) {
        result.subscribe((res: HttpResponse<IBusinessMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProfileById(index: number, item: IProfileMv) {
        return item.id;
    }

    trackContactById(index: number, item: IContactMv) {
        return item.id;
    }

    trackAddressById(index: number, item: IAddressMv) {
        return item.id;
    }

    trackCategoryById(index: number, item: ICategoryMv) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
    get business() {
        return this._business;
    }

    set business(business: IBusinessMv) {
        this._business = business;
        this.createdAt = moment(business.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(business.updatedAt).format(DATE_TIME_FORMAT);
        this.verifiedAt = moment(business.verifiedAt).format(DATE_TIME_FORMAT);
    }
}
