import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SspDcSharedModule } from 'app/shared';
import {
    DonationMvComponent,
    DonationMvDetailComponent,
    DonationMvUpdateComponent,
    DonationMvDeletePopupComponent,
    DonationMvDeleteDialogComponent,
    donationRoute,
    donationPopupRoute
} from './';

const ENTITY_STATES = [...donationRoute, ...donationPopupRoute];

@NgModule({
    imports: [SspDcSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DonationMvComponent,
        DonationMvDetailComponent,
        DonationMvUpdateComponent,
        DonationMvDeleteDialogComponent,
        DonationMvDeletePopupComponent
    ],
    entryComponents: [DonationMvComponent, DonationMvUpdateComponent, DonationMvDeleteDialogComponent, DonationMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SspDcDonationMvModule {}
