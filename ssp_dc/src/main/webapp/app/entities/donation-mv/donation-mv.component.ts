import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDonationMv } from 'app/shared/model/donation-mv.model';
import { Principal } from 'app/core';
import { DonationMvService } from './donation-mv.service';

@Component({
    selector: 'jhi-donation-mv',
    templateUrl: './donation-mv.component.html'
})
export class DonationMvComponent implements OnInit, OnDestroy {
    donations: IDonationMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private donationService: DonationMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.donationService.query().subscribe(
            (res: HttpResponse<IDonationMv[]>) => {
                this.donations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInDonations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IDonationMv) {
        return item.id;
    }

    registerChangeInDonations() {
        this.eventSubscriber = this.eventManager.subscribe('donationListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
