import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IDonationMv } from 'app/shared/model/donation-mv.model';
import { DonationMvService } from './donation-mv.service';
import { IProfileMv } from 'app/shared/model/profile-mv.model';
import { ProfileMvService } from 'app/entities/profile-mv';

@Component({
    selector: 'jhi-donation-mv-update',
    templateUrl: './donation-mv-update.component.html'
})
export class DonationMvUpdateComponent implements OnInit {
    private _donation: IDonationMv;
    isSaving: boolean;

    profiles: IProfileMv[];
    createdAt: string;
    updatedAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private donationService: DonationMvService,
        private profileService: ProfileMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ donation }) => {
            this.donation = donation;
        });
        this.profileService.query().subscribe(
            (res: HttpResponse<IProfileMv[]>) => {
                this.profiles = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.donation.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.donation.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        if (this.donation.id !== undefined) {
            this.subscribeToSaveResponse(this.donationService.update(this.donation));
        } else {
            this.subscribeToSaveResponse(this.donationService.create(this.donation));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDonationMv>>) {
        result.subscribe((res: HttpResponse<IDonationMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProfileById(index: number, item: IProfileMv) {
        return item.id;
    }
    get donation() {
        return this._donation;
    }

    set donation(donation: IDonationMv) {
        this._donation = donation;
        this.createdAt = moment(donation.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(donation.updatedAt).format(DATE_TIME_FORMAT);
    }
}
