import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DonationMv } from 'app/shared/model/donation-mv.model';
import { DonationMvService } from './donation-mv.service';
import { DonationMvComponent } from './donation-mv.component';
import { DonationMvDetailComponent } from './donation-mv-detail.component';
import { DonationMvUpdateComponent } from './donation-mv-update.component';
import { DonationMvDeletePopupComponent } from './donation-mv-delete-dialog.component';
import { IDonationMv } from 'app/shared/model/donation-mv.model';

@Injectable({ providedIn: 'root' })
export class DonationMvResolve implements Resolve<IDonationMv> {
    constructor(private service: DonationMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((donation: HttpResponse<DonationMv>) => donation.body));
        }
        return of(new DonationMv());
    }
}

export const donationRoute: Routes = [
    {
        path: 'donation-mv',
        component: DonationMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.donation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'donation-mv/:id/view',
        component: DonationMvDetailComponent,
        resolve: {
            donation: DonationMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.donation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'donation-mv/new',
        component: DonationMvUpdateComponent,
        resolve: {
            donation: DonationMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.donation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'donation-mv/:id/edit',
        component: DonationMvUpdateComponent,
        resolve: {
            donation: DonationMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.donation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const donationPopupRoute: Routes = [
    {
        path: 'donation-mv/:id/delete',
        component: DonationMvDeletePopupComponent,
        resolve: {
            donation: DonationMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.donation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
