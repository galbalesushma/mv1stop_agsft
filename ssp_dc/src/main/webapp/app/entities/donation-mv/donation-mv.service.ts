import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDonationMv } from 'app/shared/model/donation-mv.model';

type EntityResponseType = HttpResponse<IDonationMv>;
type EntityArrayResponseType = HttpResponse<IDonationMv[]>;

@Injectable({ providedIn: 'root' })
export class DonationMvService {
    private resourceUrl = SERVER_API_URL + 'api/donations';

    constructor(private http: HttpClient) {}

    create(donation: IDonationMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(donation);
        return this.http
            .post<IDonationMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(donation: IDonationMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(donation);
        return this.http
            .put<IDonationMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IDonationMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IDonationMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(donation: IDonationMv): IDonationMv {
        const copy: IDonationMv = Object.assign({}, donation, {
            createdAt: donation.createdAt != null && donation.createdAt.isValid() ? donation.createdAt.toJSON() : null,
            updatedAt: donation.updatedAt != null && donation.updatedAt.isValid() ? donation.updatedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((donation: IDonationMv) => {
            donation.createdAt = donation.createdAt != null ? moment(donation.createdAt) : null;
            donation.updatedAt = donation.updatedAt != null ? moment(donation.updatedAt) : null;
        });
        return res;
    }
}
