import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDonationMv } from 'app/shared/model/donation-mv.model';

@Component({
    selector: 'jhi-donation-mv-detail',
    templateUrl: './donation-mv-detail.component.html'
})
export class DonationMvDetailComponent implements OnInit {
    donation: IDonationMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ donation }) => {
            this.donation = donation;
        });
    }

    previousState() {
        window.history.back();
    }
}
