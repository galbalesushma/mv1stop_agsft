import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SspDcProfileMvModule } from './profile-mv/profile-mv.module';
import { SspDcAddressMvModule } from './address-mv/address-mv.module';
import { SspDcContactMvModule } from './contact-mv/contact-mv.module';
import { SspDcCategoryMvModule } from './category-mv/category-mv.module';
import { SspDcBusinessMvModule } from './business-mv/business-mv.module';
import { SspDcPaymentGatewayMvModule } from './payment-gateway-mv/payment-gateway-mv.module';
import { SspDcSchoolMvModule } from './school-mv/school-mv.module';
import { SspDcDonationMvModule } from './donation-mv/donation-mv.module';
import { SspDcCardMvModule } from './card-mv/card-mv.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        SspDcProfileMvModule,
        SspDcAddressMvModule,
        SspDcContactMvModule,
        SspDcCategoryMvModule,
        SspDcBusinessMvModule,
        SspDcPaymentGatewayMvModule,
        SspDcSchoolMvModule,
        SspDcDonationMvModule,
        SspDcCardMvModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SspDcEntityModule {}
