import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IPaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';
import { PaymentGatewayMvService } from './payment-gateway-mv.service';

@Component({
    selector: 'jhi-payment-gateway-mv-update',
    templateUrl: './payment-gateway-mv-update.component.html'
})
export class PaymentGatewayMvUpdateComponent implements OnInit {
    private _paymentGateway: IPaymentGatewayMv;
    isSaving: boolean;
    createdAt: string;
    updatedAt: string;

    constructor(private paymentGatewayService: PaymentGatewayMvService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ paymentGateway }) => {
            this.paymentGateway = paymentGateway;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.paymentGateway.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.paymentGateway.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        if (this.paymentGateway.id !== undefined) {
            this.subscribeToSaveResponse(this.paymentGatewayService.update(this.paymentGateway));
        } else {
            this.subscribeToSaveResponse(this.paymentGatewayService.create(this.paymentGateway));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPaymentGatewayMv>>) {
        result.subscribe((res: HttpResponse<IPaymentGatewayMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get paymentGateway() {
        return this._paymentGateway;
    }

    set paymentGateway(paymentGateway: IPaymentGatewayMv) {
        this._paymentGateway = paymentGateway;
        this.createdAt = moment(paymentGateway.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(paymentGateway.updatedAt).format(DATE_TIME_FORMAT);
    }
}
