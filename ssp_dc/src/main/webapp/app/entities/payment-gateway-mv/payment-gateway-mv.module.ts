import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SspDcSharedModule } from 'app/shared';
import {
    PaymentGatewayMvComponent,
    PaymentGatewayMvDetailComponent,
    PaymentGatewayMvUpdateComponent,
    PaymentGatewayMvDeletePopupComponent,
    PaymentGatewayMvDeleteDialogComponent,
    paymentGatewayRoute,
    paymentGatewayPopupRoute
} from './';

const ENTITY_STATES = [...paymentGatewayRoute, ...paymentGatewayPopupRoute];

@NgModule({
    imports: [SspDcSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PaymentGatewayMvComponent,
        PaymentGatewayMvDetailComponent,
        PaymentGatewayMvUpdateComponent,
        PaymentGatewayMvDeleteDialogComponent,
        PaymentGatewayMvDeletePopupComponent
    ],
    entryComponents: [
        PaymentGatewayMvComponent,
        PaymentGatewayMvUpdateComponent,
        PaymentGatewayMvDeleteDialogComponent,
        PaymentGatewayMvDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SspDcPaymentGatewayMvModule {}
