import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';
import { PaymentGatewayMvService } from './payment-gateway-mv.service';

@Component({
    selector: 'jhi-payment-gateway-mv-delete-dialog',
    templateUrl: './payment-gateway-mv-delete-dialog.component.html'
})
export class PaymentGatewayMvDeleteDialogComponent {
    paymentGateway: IPaymentGatewayMv;

    constructor(
        private paymentGatewayService: PaymentGatewayMvService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.paymentGatewayService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'paymentGatewayListModification',
                content: 'Deleted an paymentGateway'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-payment-gateway-mv-delete-popup',
    template: ''
})
export class PaymentGatewayMvDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ paymentGateway }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PaymentGatewayMvDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.paymentGateway = paymentGateway;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
