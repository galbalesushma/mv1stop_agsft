import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';

@Component({
    selector: 'jhi-payment-gateway-mv-detail',
    templateUrl: './payment-gateway-mv-detail.component.html'
})
export class PaymentGatewayMvDetailComponent implements OnInit {
    paymentGateway: IPaymentGatewayMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ paymentGateway }) => {
            this.paymentGateway = paymentGateway;
        });
    }

    previousState() {
        window.history.back();
    }
}
