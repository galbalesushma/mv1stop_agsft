import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IPaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';
import { Principal } from 'app/core';
import { PaymentGatewayMvService } from './payment-gateway-mv.service';

@Component({
    selector: 'jhi-payment-gateway-mv',
    templateUrl: './payment-gateway-mv.component.html'
})
export class PaymentGatewayMvComponent implements OnInit, OnDestroy {
    paymentGateways: IPaymentGatewayMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private paymentGatewayService: PaymentGatewayMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.paymentGatewayService.query().subscribe(
            (res: HttpResponse<IPaymentGatewayMv[]>) => {
                this.paymentGateways = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInPaymentGateways();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPaymentGatewayMv) {
        return item.id;
    }

    registerChangeInPaymentGateways() {
        this.eventSubscriber = this.eventManager.subscribe('paymentGatewayListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
