import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';

type EntityResponseType = HttpResponse<IPaymentGatewayMv>;
type EntityArrayResponseType = HttpResponse<IPaymentGatewayMv[]>;

@Injectable({ providedIn: 'root' })
export class PaymentGatewayMvService {
    private resourceUrl = SERVER_API_URL + 'api/payment-gateways';

    constructor(private http: HttpClient) {}

    create(paymentGateway: IPaymentGatewayMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(paymentGateway);
        return this.http
            .post<IPaymentGatewayMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(paymentGateway: IPaymentGatewayMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(paymentGateway);
        return this.http
            .put<IPaymentGatewayMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IPaymentGatewayMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IPaymentGatewayMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(paymentGateway: IPaymentGatewayMv): IPaymentGatewayMv {
        const copy: IPaymentGatewayMv = Object.assign({}, paymentGateway, {
            createdAt: paymentGateway.createdAt != null && paymentGateway.createdAt.isValid() ? paymentGateway.createdAt.toJSON() : null,
            updatedAt: paymentGateway.updatedAt != null && paymentGateway.updatedAt.isValid() ? paymentGateway.updatedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((paymentGateway: IPaymentGatewayMv) => {
            paymentGateway.createdAt = paymentGateway.createdAt != null ? moment(paymentGateway.createdAt) : null;
            paymentGateway.updatedAt = paymentGateway.updatedAt != null ? moment(paymentGateway.updatedAt) : null;
        });
        return res;
    }
}
