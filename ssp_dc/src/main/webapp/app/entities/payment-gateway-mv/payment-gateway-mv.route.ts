import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';
import { PaymentGatewayMvService } from './payment-gateway-mv.service';
import { PaymentGatewayMvComponent } from './payment-gateway-mv.component';
import { PaymentGatewayMvDetailComponent } from './payment-gateway-mv-detail.component';
import { PaymentGatewayMvUpdateComponent } from './payment-gateway-mv-update.component';
import { PaymentGatewayMvDeletePopupComponent } from './payment-gateway-mv-delete-dialog.component';
import { IPaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';

@Injectable({ providedIn: 'root' })
export class PaymentGatewayMvResolve implements Resolve<IPaymentGatewayMv> {
    constructor(private service: PaymentGatewayMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((paymentGateway: HttpResponse<PaymentGatewayMv>) => paymentGateway.body));
        }
        return of(new PaymentGatewayMv());
    }
}

export const paymentGatewayRoute: Routes = [
    {
        path: 'payment-gateway-mv',
        component: PaymentGatewayMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.paymentGateway.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-gateway-mv/:id/view',
        component: PaymentGatewayMvDetailComponent,
        resolve: {
            paymentGateway: PaymentGatewayMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.paymentGateway.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-gateway-mv/new',
        component: PaymentGatewayMvUpdateComponent,
        resolve: {
            paymentGateway: PaymentGatewayMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.paymentGateway.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-gateway-mv/:id/edit',
        component: PaymentGatewayMvUpdateComponent,
        resolve: {
            paymentGateway: PaymentGatewayMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.paymentGateway.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const paymentGatewayPopupRoute: Routes = [
    {
        path: 'payment-gateway-mv/:id/delete',
        component: PaymentGatewayMvDeletePopupComponent,
        resolve: {
            paymentGateway: PaymentGatewayMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.paymentGateway.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
