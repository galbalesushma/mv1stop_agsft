import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SspDcSharedModule } from 'app/shared';
import {
    AddressMvComponent,
    AddressMvDetailComponent,
    AddressMvUpdateComponent,
    AddressMvDeletePopupComponent,
    AddressMvDeleteDialogComponent,
    addressRoute,
    addressPopupRoute
} from './';

const ENTITY_STATES = [...addressRoute, ...addressPopupRoute];

@NgModule({
    imports: [SspDcSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AddressMvComponent,
        AddressMvDetailComponent,
        AddressMvUpdateComponent,
        AddressMvDeleteDialogComponent,
        AddressMvDeletePopupComponent
    ],
    entryComponents: [AddressMvComponent, AddressMvUpdateComponent, AddressMvDeleteDialogComponent, AddressMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SspDcAddressMvModule {}
