import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AddressMv } from 'app/shared/model/address-mv.model';
import { AddressMvService } from './address-mv.service';
import { AddressMvComponent } from './address-mv.component';
import { AddressMvDetailComponent } from './address-mv-detail.component';
import { AddressMvUpdateComponent } from './address-mv-update.component';
import { AddressMvDeletePopupComponent } from './address-mv-delete-dialog.component';
import { IAddressMv } from 'app/shared/model/address-mv.model';

@Injectable({ providedIn: 'root' })
export class AddressMvResolve implements Resolve<IAddressMv> {
    constructor(private service: AddressMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((address: HttpResponse<AddressMv>) => address.body));
        }
        return of(new AddressMv());
    }
}

export const addressRoute: Routes = [
    {
        path: 'address-mv',
        component: AddressMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.address.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'address-mv/:id/view',
        component: AddressMvDetailComponent,
        resolve: {
            address: AddressMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.address.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'address-mv/new',
        component: AddressMvUpdateComponent,
        resolve: {
            address: AddressMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.address.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'address-mv/:id/edit',
        component: AddressMvUpdateComponent,
        resolve: {
            address: AddressMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.address.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const addressPopupRoute: Routes = [
    {
        path: 'address-mv/:id/delete',
        component: AddressMvDeletePopupComponent,
        resolve: {
            address: AddressMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.address.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
