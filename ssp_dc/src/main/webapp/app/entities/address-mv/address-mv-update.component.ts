import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAddressMv } from 'app/shared/model/address-mv.model';
import { AddressMvService } from './address-mv.service';

@Component({
    selector: 'jhi-address-mv-update',
    templateUrl: './address-mv-update.component.html'
})
export class AddressMvUpdateComponent implements OnInit {
    private _address: IAddressMv;
    isSaving: boolean;
    createdAt: string;
    updatedAt: string;

    constructor(private addressService: AddressMvService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ address }) => {
            this.address = address;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.address.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.address.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        if (this.address.id !== undefined) {
            this.subscribeToSaveResponse(this.addressService.update(this.address));
        } else {
            this.subscribeToSaveResponse(this.addressService.create(this.address));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAddressMv>>) {
        result.subscribe((res: HttpResponse<IAddressMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get address() {
        return this._address;
    }

    set address(address: IAddressMv) {
        this._address = address;
        this.createdAt = moment(address.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(address.updatedAt).format(DATE_TIME_FORMAT);
    }
}
