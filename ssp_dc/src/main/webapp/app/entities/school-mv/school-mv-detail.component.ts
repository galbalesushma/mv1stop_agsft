import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISchoolMv } from 'app/shared/model/school-mv.model';

@Component({
    selector: 'jhi-school-mv-detail',
    templateUrl: './school-mv-detail.component.html'
})
export class SchoolMvDetailComponent implements OnInit {
    school: ISchoolMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ school }) => {
            this.school = school;
        });
    }

    previousState() {
        window.history.back();
    }
}
