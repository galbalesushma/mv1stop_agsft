import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISchoolMv } from 'app/shared/model/school-mv.model';

type EntityResponseType = HttpResponse<ISchoolMv>;
type EntityArrayResponseType = HttpResponse<ISchoolMv[]>;

@Injectable({ providedIn: 'root' })
export class SchoolMvService {
    private resourceUrl = SERVER_API_URL + 'api/schools';

    constructor(private http: HttpClient) {}

    create(school: ISchoolMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(school);
        return this.http
            .post<ISchoolMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(school: ISchoolMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(school);
        return this.http
            .put<ISchoolMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ISchoolMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ISchoolMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(school: ISchoolMv): ISchoolMv {
        const copy: ISchoolMv = Object.assign({}, school, {
            createdAt: school.createdAt != null && school.createdAt.isValid() ? school.createdAt.toJSON() : null,
            updatedAt: school.updatedAt != null && school.updatedAt.isValid() ? school.updatedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((school: ISchoolMv) => {
            school.createdAt = school.createdAt != null ? moment(school.createdAt) : null;
            school.updatedAt = school.updatedAt != null ? moment(school.updatedAt) : null;
        });
        return res;
    }
}
