import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISchoolMv } from 'app/shared/model/school-mv.model';
import { Principal } from 'app/core';
import { SchoolMvService } from './school-mv.service';

@Component({
    selector: 'jhi-school-mv',
    templateUrl: './school-mv.component.html'
})
export class SchoolMvComponent implements OnInit, OnDestroy {
    schools: ISchoolMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private schoolService: SchoolMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.schoolService.query().subscribe(
            (res: HttpResponse<ISchoolMv[]>) => {
                this.schools = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSchools();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISchoolMv) {
        return item.id;
    }

    registerChangeInSchools() {
        this.eventSubscriber = this.eventManager.subscribe('schoolListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
