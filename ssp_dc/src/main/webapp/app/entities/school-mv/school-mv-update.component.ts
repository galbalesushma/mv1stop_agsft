import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISchoolMv } from 'app/shared/model/school-mv.model';
import { SchoolMvService } from './school-mv.service';

@Component({
    selector: 'jhi-school-mv-update',
    templateUrl: './school-mv-update.component.html'
})
export class SchoolMvUpdateComponent implements OnInit {
    private _school: ISchoolMv;
    isSaving: boolean;
    createdAt: string;
    updatedAt: string;

    constructor(private schoolService: SchoolMvService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ school }) => {
            this.school = school;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.school.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.school.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        if (this.school.id !== undefined) {
            this.subscribeToSaveResponse(this.schoolService.update(this.school));
        } else {
            this.subscribeToSaveResponse(this.schoolService.create(this.school));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ISchoolMv>>) {
        result.subscribe((res: HttpResponse<ISchoolMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get school() {
        return this._school;
    }

    set school(school: ISchoolMv) {
        this._school = school;
        this.createdAt = moment(school.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(school.updatedAt).format(DATE_TIME_FORMAT);
    }
}
