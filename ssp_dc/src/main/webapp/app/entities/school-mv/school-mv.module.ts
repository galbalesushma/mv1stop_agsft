import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SspDcSharedModule } from 'app/shared';
import {
    SchoolMvComponent,
    SchoolMvDetailComponent,
    SchoolMvUpdateComponent,
    SchoolMvDeletePopupComponent,
    SchoolMvDeleteDialogComponent,
    schoolRoute,
    schoolPopupRoute
} from './';

const ENTITY_STATES = [...schoolRoute, ...schoolPopupRoute];

@NgModule({
    imports: [SspDcSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SchoolMvComponent,
        SchoolMvDetailComponent,
        SchoolMvUpdateComponent,
        SchoolMvDeleteDialogComponent,
        SchoolMvDeletePopupComponent
    ],
    entryComponents: [SchoolMvComponent, SchoolMvUpdateComponent, SchoolMvDeleteDialogComponent, SchoolMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SspDcSchoolMvModule {}
