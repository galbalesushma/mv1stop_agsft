import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { SchoolMv } from 'app/shared/model/school-mv.model';
import { SchoolMvService } from './school-mv.service';
import { SchoolMvComponent } from './school-mv.component';
import { SchoolMvDetailComponent } from './school-mv-detail.component';
import { SchoolMvUpdateComponent } from './school-mv-update.component';
import { SchoolMvDeletePopupComponent } from './school-mv-delete-dialog.component';
import { ISchoolMv } from 'app/shared/model/school-mv.model';

@Injectable({ providedIn: 'root' })
export class SchoolMvResolve implements Resolve<ISchoolMv> {
    constructor(private service: SchoolMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((school: HttpResponse<SchoolMv>) => school.body));
        }
        return of(new SchoolMv());
    }
}

export const schoolRoute: Routes = [
    {
        path: 'school-mv',
        component: SchoolMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.school.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'school-mv/:id/view',
        component: SchoolMvDetailComponent,
        resolve: {
            school: SchoolMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.school.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'school-mv/new',
        component: SchoolMvUpdateComponent,
        resolve: {
            school: SchoolMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.school.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'school-mv/:id/edit',
        component: SchoolMvUpdateComponent,
        resolve: {
            school: SchoolMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.school.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const schoolPopupRoute: Routes = [
    {
        path: 'school-mv/:id/delete',
        component: SchoolMvDeletePopupComponent,
        resolve: {
            school: SchoolMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.school.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
