export * from './school-mv.service';
export * from './school-mv-update.component';
export * from './school-mv-delete-dialog.component';
export * from './school-mv-detail.component';
export * from './school-mv.component';
export * from './school-mv.route';
