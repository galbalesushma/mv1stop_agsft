import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IProfileMv } from 'app/shared/model/profile-mv.model';
import { Principal } from 'app/core';
import { ProfileMvService } from './profile-mv.service';

@Component({
    selector: 'jhi-profile-mv',
    templateUrl: './profile-mv.component.html'
})
export class ProfileMvComponent implements OnInit, OnDestroy {
    profiles: IProfileMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private profileService: ProfileMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.profileService.query().subscribe(
            (res: HttpResponse<IProfileMv[]>) => {
                this.profiles = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInProfiles();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IProfileMv) {
        return item.id;
    }

    registerChangeInProfiles() {
        this.eventSubscriber = this.eventManager.subscribe('profileListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
