import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProfileMv } from 'app/shared/model/profile-mv.model';
import { ProfileMvService } from './profile-mv.service';
import { ProfileMvComponent } from './profile-mv.component';
import { ProfileMvDetailComponent } from './profile-mv-detail.component';
import { ProfileMvUpdateComponent } from './profile-mv-update.component';
import { ProfileMvDeletePopupComponent } from './profile-mv-delete-dialog.component';
import { IProfileMv } from 'app/shared/model/profile-mv.model';

@Injectable({ providedIn: 'root' })
export class ProfileMvResolve implements Resolve<IProfileMv> {
    constructor(private service: ProfileMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((profile: HttpResponse<ProfileMv>) => profile.body));
        }
        return of(new ProfileMv());
    }
}

export const profileRoute: Routes = [
    {
        path: 'profile-mv',
        component: ProfileMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.profile.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'profile-mv/:id/view',
        component: ProfileMvDetailComponent,
        resolve: {
            profile: ProfileMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.profile.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'profile-mv/new',
        component: ProfileMvUpdateComponent,
        resolve: {
            profile: ProfileMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.profile.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'profile-mv/:id/edit',
        component: ProfileMvUpdateComponent,
        resolve: {
            profile: ProfileMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.profile.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const profilePopupRoute: Routes = [
    {
        path: 'profile-mv/:id/delete',
        component: ProfileMvDeletePopupComponent,
        resolve: {
            profile: ProfileMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'sspDcApp.profile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
