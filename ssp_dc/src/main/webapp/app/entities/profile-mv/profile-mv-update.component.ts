import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IProfileMv } from 'app/shared/model/profile-mv.model';
import { ProfileMvService } from './profile-mv.service';
import { IAddressMv } from 'app/shared/model/address-mv.model';
import { AddressMvService } from 'app/entities/address-mv';
import { IContactMv } from 'app/shared/model/contact-mv.model';
import { ContactMvService } from 'app/entities/contact-mv';

@Component({
    selector: 'jhi-profile-mv-update',
    templateUrl: './profile-mv-update.component.html'
})
export class ProfileMvUpdateComponent implements OnInit {
    private _profile: IProfileMv;
    isSaving: boolean;

    addresses: IAddressMv[];

    contacts: IContactMv[];
    createdAt: string;
    updatedAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private profileService: ProfileMvService,
        private addressService: AddressMvService,
        private contactService: ContactMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ profile }) => {
            this.profile = profile;
        });
        this.addressService.query({ filter: 'profile-is-null' }).subscribe(
            (res: HttpResponse<IAddressMv[]>) => {
                if (!this.profile.addressId) {
                    this.addresses = res.body;
                } else {
                    this.addressService.find(this.profile.addressId).subscribe(
                        (subRes: HttpResponse<IAddressMv>) => {
                            this.addresses = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.contactService.query({ filter: 'profile-is-null' }).subscribe(
            (res: HttpResponse<IContactMv[]>) => {
                if (!this.profile.contactsId) {
                    this.contacts = res.body;
                } else {
                    this.contactService.find(this.profile.contactsId).subscribe(
                        (subRes: HttpResponse<IContactMv>) => {
                            this.contacts = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.profile.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.profile.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        if (this.profile.id !== undefined) {
            this.subscribeToSaveResponse(this.profileService.update(this.profile));
        } else {
            this.subscribeToSaveResponse(this.profileService.create(this.profile));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IProfileMv>>) {
        result.subscribe((res: HttpResponse<IProfileMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackAddressById(index: number, item: IAddressMv) {
        return item.id;
    }

    trackContactById(index: number, item: IContactMv) {
        return item.id;
    }
    get profile() {
        return this._profile;
    }

    set profile(profile: IProfileMv) {
        this._profile = profile;
        this.createdAt = moment(profile.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(profile.updatedAt).format(DATE_TIME_FORMAT);
    }
}
