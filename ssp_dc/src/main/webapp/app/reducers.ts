import * as fromSearchSchools from './common/reducers/search.reducer';
import * as fromStates from './common/reducers/list-states.reducer';
import * as fromDistricts from './common/reducers/list-districts.reducer';
import * as fromCities from './common/reducers/list-cities.reducer';

export interface State {
    searchResult: fromSearchSchools.State;
    statesResult: fromStates.State;
    districtsResult: fromDistricts.State;
    citiesResult: fromCities.State;
}

export const reducers = {
    searchResult: fromSearchSchools.reducer,
    statesResult: fromStates.reducer,
    districtsResult: fromDistricts.reducer,
    citiesResult: fromCities.reducer
};

export function selectSearchSuccess(state: State) {
    return state.searchResult.result;
}

export function selectSearchFailure(state: State) {
    return state.searchResult.err;
}

export function selectListStatesSuccess(state: State) {
    return state.statesResult.result;
}

export function selectListStatesFailure(state: State) {
    return state.statesResult.err;
}

export function selectListDistrictsSuccess(state: State) {
    return state.districtsResult.result;
}

export function selectListDistrictsFailure(state: State) {
    return state.districtsResult.err;
}

export function selectListCitiesSuccess(state: State) {
    return state.citiesResult.result;
}

export function selectListCitiesFailure(state: State) {
    return state.citiesResult.err;
}
