package com.mv1.dc.repository;

import com.mv1.dc.domain.Card;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Card entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomCardRepository extends CardRepository {

    Card findOneByProfileId(Long profileId);
}
