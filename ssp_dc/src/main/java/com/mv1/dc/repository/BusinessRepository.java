package com.mv1.dc.repository;

import com.mv1.dc.domain.Business;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Business entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BusinessRepository extends JpaRepository<Business, Long> {

    @Query(value = "select distinct business from Business business left join fetch business.categories",
        countQuery = "select count(distinct business) from Business business")
    Page<Business> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct business from Business business left join fetch business.categories")
    List<Business> findAllWithEagerRelationships();

    @Query("select business from Business business left join fetch business.categories where business.id =:id")
    Optional<Business> findOneWithEagerRelationships(@Param("id") Long id);

}
