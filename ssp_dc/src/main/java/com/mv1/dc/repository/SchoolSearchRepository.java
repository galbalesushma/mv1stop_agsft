package com.mv1.dc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mv1.dc.domain.School;

public interface SchoolSearchRepository extends JpaRepository<School, Long> {

    @Query(value = 
	    "select * from school "
		+ " where "
			+ " school_name like ?1% "
			+ " and state = ?2 "
			+ " and district =?3 "
			+ " and city =?4 "
			+ " order by school_name ",
		nativeQuery =  true)
    List<School> searchShoolsWithSDC(String name , String state, String district, String city );
    
    @Query(value = 
	    "select * from school "
		+ " where "
			+ " school_name like ?1% "
			+ " and state =?2 "
			+ " order by school_name ",
		nativeQuery =  true)
    List<School> searchShoolsWithState(String name, String state);
    
    @Query(value = 
	    "select * from school "
		+ " where "
			+ " school_name like ?1% "
			+ " and city =?2 "
			+ " order by school_name ",
		nativeQuery =  true)
    List<School> searchShoolsWithCity(String name, String city);
    
    @Query(value = 
	    "select * from school "
		+ " where "
			+ " school_name like ?1% "
			+ " and district =?2 "
			+ " order by school_name ",
		nativeQuery =  true)
    List<School> searchShoolsWithDistrict(String name, String district);

    
    @Query(value = 
	    "select * from school "
		+ " where "
			+ " school_name like ?1% "
			+ " and state =?2 "
			+ " and district =?3 "
			+ " order by school_name ",
		nativeQuery =  true)
    List<School> searchShoolsWithSD(String name, String state, String district);
    
    
    @Query(value = 
	    "select * from school "
		+ " where "
			+ " school_name like ?1% "
			+ " order by school_name",
		nativeQuery =  true)
    List<School> searchShoolsByName(String name);
}
