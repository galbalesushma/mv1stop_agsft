package com.mv1.dc.repository;

import com.mv1.dc.domain.Profile;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Profile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomProfileRepository extends ProfileRepository {

    Profile findOneByEmail(String email);
}
