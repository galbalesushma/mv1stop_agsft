package com.mv1.dc.repository;

import com.mv1.dc.domain.Donation;
import com.mv1.dc.domain.School;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Donation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DashboardDonationRepository extends DonationRepository {

    @Query(value = 
	    "select  " + 
	    "    d.id,  " + 
	    "    d.created_at,  " + 
	    "    d.amount,  " + 
	    "    p.first_name,  " + 
	    "    p.last_name,  " + 
	    "    p.email,  " + 
	    "    a.addr,  " + 
	    "    a.city,  " + 
	    "    a.state, " + 
	    "    s.school_name,  " + 
	    "    s.id  school_id " + 
	    "from  " + 
	    "    donation d " + 
	    "join profile p " + 
	    "    on d.profile_id = p.id " + 
	    "join address a " + 
	    "    on p.address_id = a.id " + 
	    "join school s " + 
	    "    on d.school_id = s.id " + 
	    "where  " + 
	    "    d.created_at >= :from " + 
	    "    and d.created_at < :to ",
		nativeQuery =  true)
    Object[] getAllDonations(@Param("from") ZonedDateTime from ,@Param("to") ZonedDateTime to );
}
