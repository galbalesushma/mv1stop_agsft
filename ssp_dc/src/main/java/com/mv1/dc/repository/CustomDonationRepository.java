package com.mv1.dc.repository;

import com.mv1.dc.domain.Donation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Donation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomDonationRepository extends DonationRepository {
    
    Donation findOneByOrderId(String orderId);
    
    Donation findOneByProfileId(Long profileId);
}
