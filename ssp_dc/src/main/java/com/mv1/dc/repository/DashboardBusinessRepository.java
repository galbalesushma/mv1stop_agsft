package com.mv1.dc.repository;

import com.mv1.dc.domain.Donation;
import com.mv1.dc.domain.School;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Donation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DashboardBusinessRepository extends BusinessRepository {

    @Query(value = 
	    "select   " + 
	    "    b.id,  " + 
	    "    b.discount,  " + 
	    "    c.name type,  " + 
	    "    b.name,  " + 
	    "    cn.phone,  " + 
	    "    cn.email,  " +
	    "    a.street,   " + 
	    "    a.city,   " + 
	    "    a.state,  " + 
	    "    b.website,  " +
	    "    case   " + 
	    "    when b.is_active = 1 and b.is_verified = 1 " + 
	    "    then 'Active' " + 
	    "    when b.is_active = 0 and b.is_verified = 0   " + 
	    "    then 'Pending' " + 
	    "    when b.is_active = 0 and b.is_verified = 1   " + 
	    "    then 'InActive' " + 
	    "    else 'Pending'   " +
	    "	 end as status,  " +
	    "    b.created_at date  " +
	    "from  " + 
	    "    business b  " + 
	    "join business_category bc  " + 
	    "    on b.id = bc.businesses_id  " + 
	    "join category c  " + 
	    "    on c.id = bc.categories_id  " + 
	    "join address a  " + 
	    "    on b.address_id = a.id   " + 
	    "join contact cn  " + 
	    "    on cn.id = b.contact_id  " + 
	    "where   " + 
	    "    b.created_at >= :from  " + 
	    "    and b.created_at < :to  " ,
		nativeQuery =  true)
    Object[] getAllBusinesses(@Param("from") ZonedDateTime from, @Param("to") ZonedDateTime to);
}
