package com.mv1.dc.business.view.web;

import com.opencsv.bean.CsvBindByPosition;

public class DashboardDonationView {

    private Long id;
    @CsvBindByPosition(position = 0)
    private String date;
    @CsvBindByPosition(position = 1)
    private String amount;
    @CsvBindByPosition(position = 2)
    private String name;
    @CsvBindByPosition(position = 3)
    private String phone;
    @CsvBindByPosition(position = 4)
    private String email;
    @CsvBindByPosition(position = 5)
    private String street;
    @CsvBindByPosition(position = 6)
    private String city;
    @CsvBindByPosition(position = 7)
    private String state;
    @CsvBindByPosition(position = 8)
    private String school;
    
    private Long schoolId;

    public DashboardDonationView() {
    }

    public DashboardDonationView(Long id, String date, String amount, String name, String phone, String email, String street,
	    String city, String state, String school, Long schoolId) {
	super();
	this.id = id;
	this.date = date;
	this.amount = amount;
	this.name = name;
	this.phone = phone;
	this.email = email;
	this.street = street;
	this.city = city;
	this.state = state;
	this.school = school;
	this.schoolId = schoolId;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getDate() {
	return date;
    }

    public void setDate(String date) {
	this.date = date;
    }

    public String getAmount() {
	return amount;
    }

    public void setAmount(String amount) {
	this.amount = amount;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getStreet() {
	return street;
    }

    public void setStreet(String street) {
	this.street = street;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    public String getSchool() {
	return school;
    }

    public void setSchool(String school) {
	this.school = school;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	DashboardDonationView other = (DashboardDonationView) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "DonationView [id=" + id + ", date=" + date + ", amount=" + amount + ", name=" + name + ", phone="
		+ phone + ", email=" + email + ", street=" + street + ", city=" + city + ", state=" + state
		+ ", school=" + school + ", schoolId=" + schoolId + "]";
    }

}
