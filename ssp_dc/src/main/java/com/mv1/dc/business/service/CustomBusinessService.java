package com.mv1.dc.business.service;

import com.mv1.dc.service.dto.BusinessDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Business.
 */
public interface CustomBusinessService {

    /**
     * Get all the businesses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    List<BusinessDTO> findAllByCategory(Long id);

    /**
     * Get all the Business with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    Page<BusinessDTO> findAllWithEagerRelationships(Pageable pageable);
    
}
