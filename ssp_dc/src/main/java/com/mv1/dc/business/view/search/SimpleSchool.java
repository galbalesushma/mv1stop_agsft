package com.mv1.dc.business.view.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleSchool {
    private Long id;
    private String schoolName;
    private String stateCode;
    private String districtCode;
    private String city;
    private String street;
    private String principal;
    private String phone;
    private Integer zip;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getSchoolName() {
	return schoolName;
    }

    public void setSchoolName(String schoolName) {
	this.schoolName = schoolName;
    }

    public String getStateCode() {
	return stateCode;
    }

    public void setStateCode(String stateCode) {
	this.stateCode = stateCode;
    }

    public String getDistrictCode() {
	return districtCode;
    }

    public void setDistrictCode(String districtCode) {
	this.districtCode = districtCode;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getStreet() {
	return street;
    }

    public void setStreet(String street) {
	this.street = street;
    }

    public String getPrincipal() {
	return principal;
    }

    public void setPrincipal(String principal) {
	this.principal = principal;
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public Integer getZip() {
	return zip;
    }

    public void setZip(Integer zip) {
	this.zip = zip;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	SimpleSchool other = (SimpleSchool) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "SimpleSchool [id=" + id + ", schoolName=" + schoolName + ", stateCode=" + stateCode + ", districtCode="
		+ districtCode + ", city=" + city + ", street=" + street + ", principal=" + principal + ", phone="
		+ phone + ", zip=" + zip + "]";
    }

}
