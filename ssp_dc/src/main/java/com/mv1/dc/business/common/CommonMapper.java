package com.mv1.dc.business.common;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv1.dc.business.view.search.SimpleSchool;
import com.mv1.dc.business.view.web.DashboardBusinessView;
import com.mv1.dc.business.view.web.DashboardDonationView;
import com.mv1.dc.business.view.web.Item;
import com.mv1.dc.business.view.web.Payment;
import com.mv1.dc.business.view.web.req.BusinessEnrollView;
import com.mv1.dc.domain.enumeration.PayStatus;
import com.mv1.dc.domain.enumeration.ProfileType;
import com.mv1.dc.service.dto.AddressDTO;
import com.mv1.dc.service.dto.BusinessDTO;
import com.mv1.dc.service.dto.CategoryDTO;
import com.mv1.dc.service.dto.ContactDTO;
import com.mv1.dc.service.dto.DonationDTO;
import com.mv1.dc.service.dto.ProfileDTO;
import com.mv1.dc.service.dto.UserDTO;

public class CommonMapper {
    
    private static ObjectMapper mapper = new ObjectMapper();
    
    private static SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
    
    public static SimpleSchool fromSchoolDomain(com.mv1.dc.domain.School schoolDomain) {
	return mapper.convertValue(schoolDomain, SimpleSchool.class);
    }
    
    public static DashboardDonationView donationViewfromObjectArray(Object[] row) {
	return new DashboardDonationView( 
        		((BigInteger)row[0]).longValue(), 
        		formatter.format( (Timestamp) row[1] ),
        		((BigInteger) row[2]).toString(), 
        		(String) row[3], 
        		(String) row[4], 
        		(String) row[5], 
        		(String) row[6], 
        		(String) row[7], 
        		(String) row[8], 
        		(String) row[9],
        		((BigInteger) row[10]).longValue()
		);
    }
    
    public static DashboardBusinessView businessViewfromObjectArray(Object[] row) {
	return new DashboardBusinessView( 
        		((BigInteger)row[0]).longValue(), 	// id
        		(String) row[1],			// discount
        		(String) row[2], 			// type
        		(String) row[3], 			// name
        		(String) row[4], 			// phone
        		(String) row[5], 			// email
        		(String) row[6], 			// street
        		(String) row[7], 			// city
        		(String) row[8], 			// state
        		(String) row[9],			// website
        		(String) row[10],			// status
        		formatter.format( (Timestamp) row[11] )	// date
		);
    }
    
    public static UserDTO editUserDTO(UserDTO src, UserDTO dest) {
	
	if(src.getCreatedDate() == null ) {
	    dest.setCreatedDate(Instant.now());
	}
	
	dest.setActivated(src.isActivated());
	dest.setAuthorities(src.getAuthorities());
	dest.setCreatedBy(src.getCreatedBy());
	dest.setEmail(src.getEmail());
	dest.setFirstName(src.getFirstName());
	dest.setId(src.getId());
	dest.setImageUrl(src.getImageUrl());
	dest.setLangKey(src.getLangKey());
	dest.setLastModifiedDate(Instant.now());
	dest.setLastName(src.getLastName());
	
	if(src.getLogin() == null) {
	    dest.setLogin(src.getEmail());
	} else {
	    dest.setLogin(src.getLogin());
	}
	
	return dest;
    }
    
    public static UserDTO userDTOfrom(Payment payment) {
	UserDTO dto = new UserDTO();
	
	dto.setActivated(true);
	dto.setCreatedBy("System");
	dto.setCreatedDate(Instant.now());
	dto.setEmail(payment.getEmail());
	dto.setFirstName(payment.getFirstName());
	dto.setLastModifiedBy("System");
	dto.setLastModifiedDate(Instant.now());
	dto.setLastName(payment.getLastName());
	dto.setLogin(payment.getEmail());
	
	return dto;
    }
    
    public static AddressDTO addressDTOfromPayment(Payment payment) {
	
	AddressDTO dto = new AddressDTO();
	
	dto.setAddr(payment.getAddr());
	dto.setCity(payment.getCity());
	dto.setCountry(payment.getCountry());
	dto.setCreatedAt(ZonedDateTime.now());
	dto.setState(payment.getState());
	dto.setStreet(payment.getStreet());
	dto.setUpdatedAt(ZonedDateTime.now());
	dto.setZip(payment.getZip());
	
	return dto;
    }
    
    public static AddressDTO addressDTOfromBusinessView(BusinessEnrollView beView) {
	
	AddressDTO dto = new AddressDTO();
	
	dto.setAddr(beView.getStreet());
	dto.setCity(beView.getCity());
	dto.setCountry("USA");
	dto.setCreatedAt(ZonedDateTime.now());
	dto.setState(beView.getState());
	dto.setStreet(beView.getStreet());
	dto.setUpdatedAt(ZonedDateTime.now());
	dto.setZip(beView.getZip());
	
	return dto;
    }
    
    public static ContactDTO contactDTOfromPayment(Payment payment) {
	
	ContactDTO dto = new ContactDTO();
	
	dto.setEmail(payment.getEmail());
	dto.setPhone(payment.getPhone());
	
	return dto;
    }
    
    public static ContactDTO contactDTOfromBusinessView(BusinessEnrollView beView) {
	
	ContactDTO dto = new ContactDTO();
	
	dto.setEmail(beView.getEmail());
	dto.setPhone(beView.getPhone());
	
	return dto;
    }
    
    public static ProfileDTO profileDTOfrom(Payment p, Long addressId, Long contactId, Long userId) {
	
	ProfileDTO d = new ProfileDTO();

	d.setAddressId(addressId);
	d.setContactsId(contactId);
	d.setEmail(p.getEmail());
	d.setFirstName(p.getFirstName());
	d.setIsActive(true);
	d.setIsDeleted(false);
	d.setLastName(p.getLastName());
	d.setType(ProfileType.USER);
	d.setUserId(userId);
	d.setCreatedAt(ZonedDateTime.now());
	d.setUpdatedAt(ZonedDateTime.now());
	
	return d;
    }
    
    public static BusinessDTO BusinessDTOfrom(BusinessEnrollView beView, 
	    Long profileId, Long contactId, Long addressId,CategoryDTO categoryDTO) {
	
	BusinessDTO d = new BusinessDTO();
	Set<CategoryDTO> s = new HashSet<>();
	s.add(categoryDTO);
	
	d.setAddressId(addressId);
	d.setCategories(s);
	d.setDiscount(beView.getDiscount());
	d.setIsVerified(false);
	d.setContactId(contactId);
	d.setName(beView.getName());
	d.setIsActive(true);
	d.setIsDeleted(false);
	d.setProfileId(profileId);
	d.setWebsite(beView.getSite());
	d.setCreatedAt(ZonedDateTime.now());
	d.setUpdatedAt(ZonedDateTime.now());
	
	return d;
    }
    
    public static DonationDTO donationDTOfrom(Payment p, Long profileId, String donee, String orderId) {
	
	DonationDTO d = new DonationDTO();
	
	d.setAmount(30l);
	d.setCreatedAt(ZonedDateTime.now());
	d.setIsDeleted(false);
	d.setOrderId(orderId);
	d.setProfileId(profileId);
	d.setDonee(donee);;
	d.setStatus(PayStatus.INITIATED);
	d.setTxnId("0");
	d.setUpdatedAt(ZonedDateTime.now());
	
	return d;
    }
    
    public static Item fromBusinessDTO(BusinessDTO dto, AddressDTO addr, ContactDTO cont) {
	
	String address = addr.getAddr() + " , " + 
			addr.getStreet() + " , " + addr.getCity() + " , " + addr.getZip();
	
	Item i = new Item();
	
	i.setId(dto.getId());
	i.setName(dto.getName());
	i.setDiscount(Integer.parseInt(dto.getDiscount()));
	i.setUrl(dto.getWebsite());
	i.setPhone(cont.getPhone());
	i.setAddress(address);
	
	return i;
    }
}
