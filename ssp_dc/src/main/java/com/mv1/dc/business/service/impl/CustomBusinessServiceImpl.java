package com.mv1.dc.business.service.impl;

import com.mv1.dc.business.service.CustomBusinessService;
import com.mv1.dc.repository.BusinessRepository;
import com.mv1.dc.service.dto.BusinessDTO;
import com.mv1.dc.service.dto.CategoryDTO;
import com.mv1.dc.service.mapper.BusinessMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Business.
 */
@Service
@Transactional
public class CustomBusinessServiceImpl implements CustomBusinessService{

    private final Logger log = LoggerFactory.getLogger(CustomBusinessServiceImpl.class);
    
    private final BusinessRepository businessRepository;

    private final BusinessMapper businessMapper;

    public CustomBusinessServiceImpl(BusinessRepository businessRepository, BusinessMapper businessMapper) {
        this.businessRepository = businessRepository;
        this.businessMapper = businessMapper;
    }
    
    /**
     * Get all the businesses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    public List<BusinessDTO> findAllByCategory(Long id) {
	log.debug("getting business by category id :"+id);
	CategoryDTO c = new CategoryDTO();
	c.setId(id);
	
	return 
		businessRepository.findAll()
                	.stream()
                	.map(businessMapper::toDto)
                	.filter(b -> b.getCategories().contains(c))
                	.sorted(new Comparator<BusinessDTO>() {
                	        @Override
                	        public int compare(BusinessDTO b1, BusinessDTO b2) {
                	            return b2.getDiscount().compareTo(b1.getDiscount());
                	        }
                	})
                	.collect(Collectors.toList());
    }

    /**
     * Get all the Business with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<BusinessDTO> findAllWithEagerRelationships(Pageable pageable) {
        return businessRepository.findAllWithEagerRelationships(pageable).map(businessMapper::toDto);
    }
    
}
