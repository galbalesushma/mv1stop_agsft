package com.mv1.dc.business.view.web;

public class BusinessRespView {

    private String message;
    private Integer status;

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public Integer getStatus() {
	return status;
    }

    public void setStatus(Integer status) {
	this.status = status;
    }

    @Override
    public String toString() {
	return "BusinessRespView [message=" + message + ", status=" + status + "]";
    }

}
