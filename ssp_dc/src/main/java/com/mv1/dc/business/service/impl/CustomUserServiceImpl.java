package com.mv1.dc.business.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mv1.dc.business.common.CommonMapper;
import com.mv1.dc.business.service.CustomUserService;
import com.mv1.dc.domain.User;
import com.mv1.dc.repository.UserRepository;
import com.mv1.dc.service.UserService;
import com.mv1.dc.service.dto.UserDTO;
import com.mv1.dc.service.mapper.UserMapper;

@Service
public class CustomUserServiceImpl implements CustomUserService {

    private final Logger log = LoggerFactory.getLogger(CustomUserServiceImpl.class);
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private UserMapper userMapper;
    
    /**
     * Create/Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to create/update
     * @return updated user
     */
    public Optional<UserDTO> upsertUser(UserDTO userDTO) {
	
	log.debug("User DTO to upsert : "+userDTO);
	
	Optional<User> user = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
	UserDTO dto = null;
	
	if(user.isPresent()) {
	    dto = userMapper.userToUserDTO(user.get());
	    dto = CommonMapper.editUserDTO(userDTO, dto);
	    log.debug("user DTO before update : "+dto);
	    dto = userService.updateUser(dto).get();
	} else {
	    dto = userMapper.userToUserDTO(userService.createUser(userDTO));
	}
        return Optional.of(dto);
    }
    
}
