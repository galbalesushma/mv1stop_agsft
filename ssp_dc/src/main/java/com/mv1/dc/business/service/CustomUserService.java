package com.mv1.dc.business.service;

import java.util.Optional;

import com.mv1.dc.service.dto.UserDTO;

public interface CustomUserService {

    Optional<UserDTO> upsertUser(UserDTO userDTO);
}
