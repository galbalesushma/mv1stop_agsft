package com.mv1.dc.business.common;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.mv1.dc.business.view.web.CsvBean;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

public class Util implements Constants {
    
    private static CSVParser parser = new CSVParserBuilder()
	    .withSeparator(',')
	    .withIgnoreQuotations(true)
	    .build();
	 
    public static <T> List<T> paginate(List<T> list, Integer page) {
	if (page <= 0) {
	    return list;
	}

	int size = list.size();
	int end = page * PAGE_LIST_SIZE;
	int start = end - PAGE_LIST_SIZE;

	if (end > size) {
	    end = size;
	}
	
	return list.subList(start, end);
    }

    public <T> List<T> beanBuilderExample(Path path, Class clazz) throws Exception {
//	CsvTransfer csvTransfer = new CsvTransfer();
	ColumnPositionMappingStrategy ms = new ColumnPositionMappingStrategy();
	ms.setType(clazz);

	Reader reader = Files.newBufferedReader(path);
	CsvToBean cb = new CsvToBeanBuilder(reader).withType(clazz).withMappingStrategy(ms).build();

//	csvTransfer.setCsvList(cb.parse());
	reader.close();
//	return csvTransfer.getCsvList();
	return null;
    }
    
    public static <T> List<T> readCSV(String path, Class clazz) {
	try {
	    List<T> beans = new CsvToBeanBuilder(new FileReader(path))
		    .withSkipLines(1)
		    .withType(clazz)
		    .build()
		    .parse();
	    return beans;
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return null;
    }
    
    public static void main(String ... args ) {
	//Date,Amount,Name,Phone,Email,Street,City,State,School
	//"09-29-2018","30","Mahen","K","mk@t.com","sfaf","2344","Apex","North Carolina"
	
	List<CsvBean> v = readCSV("csv_reports/donations_report_0c05b73b-e8fd-4711-9efe-11a573a888ad.csv", CsvBean.class);
	v.stream().forEach(System.out::println);
    }
    
    public List<String[]> readAll(String filePath) throws Exception {
	
	URI uri = ClassLoader.getSystemResource(filePath).toURI();
	Path path = Paths.get(uri);
	
	Reader reader = Files.newBufferedReader(path, Charset.defaultCharset());
	CSVReader csvReader = new CSVReaderBuilder(reader)
		    .withSkipLines(1)
		    .withCSVParser(parser)
		    .build();
	List<String[]> list = new ArrayList<>();
	list = csvReader.readAll();
	reader.close();
	csvReader.close();
	
	return list;
    }
    
    public static String generateCard(String name, String cardNumber, 
	    String type, File source, File destination, boolean isImageOnly) throws Exception {
	BufferedImage image = ImageIO.read(source);

	// determine image type and handle correct transparency
	int imageType = "png".equalsIgnoreCase(type) ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
	BufferedImage buffImage = new BufferedImage(image.getWidth(), image.getHeight(), imageType);
	String path = destination.getAbsolutePath();
	String fileName = path.substring(0, path.lastIndexOf("."));

	// initializes necessary graphic properties
	Graphics2D w = (Graphics2D) buffImage.getGraphics();
	w.drawImage(image, 0, 0, null);
	AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
	w.setComposite(alphaChannel);
	w.setColor(Color.black);
	w.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));

	// calculate center of the image
	int centerX = 15;
	int centerY = 120;

	// add text overlay to the image
	w.drawString(name, centerX, centerY);

	// calculate & write card number
	centerX = 170;
	centerY = 120;
	w.drawString(cardNumber, centerX, centerY);

	ImageIO.write(buffImage, type, destination);
	w.dispose();
	
	if (isImageOnly) {
	    return appendImages(destination, fileName);
	}
	return generatePDFFromImage(fileName);
    }

    private static String appendImages(File destination, String fileName) throws IOException {
	BufferedImage result = new BufferedImage(780, 450, // work these out
	    BufferedImage.TYPE_INT_RGB);
	Graphics g = result.getGraphics();

	String card2 = "src/main/resources/images/card2.png";

	String[] images = new String[] { destination.getAbsolutePath(), card2 };
	String dest = fileName + "-dest.png";

	int x = 10, y = 0;
	for (String img : images) {
	BufferedImage bi = ImageIO.read(new File(img));
	g.drawImage(bi, x, y, null);
	y += 222;
	if (x > result.getWidth()) {
	    x = 0;
	    y += bi.getHeight();
	}
	}
	ImageIO.write(result, "png", new File(dest));
	
	return dest;
    }
    
    public static void sendEmailWithAttachments(String host, String port, final String userName, final String password,
	    String toAddress, String subject, String message, String[] attachFiles)
	    throws AddressException, MessagingException {
	// sets SMTP server properties
	Properties properties = new Properties();
	properties.put("mail.smtp.host", host);
	properties.put("mail.smtp.port", port);
	properties.put("mail.smtp.auth", "true");
	properties.put("mail.smtp.starttls.enable", "true");
	properties.put("mail.user", userName);
	properties.put("mail.password", password);

	// creates a new session with an authenticator
	Authenticator auth = new Authenticator() {
	    public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(userName, password);
	    }
	};
	Session session = Session.getInstance(properties, auth);

	// creates a new e-mail message
	Message msg = new MimeMessage(session);

	msg.setFrom(new InternetAddress(userName));
	InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
	msg.setRecipients(Message.RecipientType.TO, toAddresses);
	msg.setSubject(subject);
	msg.setSentDate(new Date());

	// creates message part
	MimeBodyPart messageBodyPart = new MimeBodyPart();
	messageBodyPart.setContent(message, "text/html");

	// creates multi-part
	Multipart multipart = new MimeMultipart();
	multipart.addBodyPart(messageBodyPart);

	// adds attachments
	if (attachFiles != null && attachFiles.length > 0) {
	    for (String filePath : attachFiles) {
		MimeBodyPart attachPart = new MimeBodyPart();
		try {
		    attachPart.attachFile(filePath);
		} catch (IOException ex) {
		    ex.printStackTrace();
		}

		multipart.addBodyPart(attachPart);
	    }
	}

	// sets the multi-part as e-mail's content
	msg.setContent(multipart);

	// sends the e-mail
	Transport.send(msg);

    }

    private static String generatePDFFromImage(String filename) throws Exception {
	Document document = new Document();
	String input1 = filename + ".png";
	String input2 = "src/main/resources/images/card2.png";
	String output = filename + ".pdf";
	FileOutputStream fos = new FileOutputStream(output);

	PdfWriter writer = PdfWriter.getInstance(document, fos);
	writer.open();
	document.open();
	document.add(com.itextpdf.text.Image.getInstance(input1));
	document.add(com.itextpdf.text.Image.getInstance(input2));
	document.close();
	writer.close();
	
	return output;
    }
}
