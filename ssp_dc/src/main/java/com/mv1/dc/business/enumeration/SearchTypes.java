package com.mv1.dc.business.enumeration;

import com.mv1.dc.business.view.search.*;

public enum SearchTypes {

	STATE(State.class, "states"),
	CITY(City.class, "cities"),
	DISTRICT(District.class, "districts"),
	SCHOOL(School.class, "schools"),
	;
	
	private Class clazz;
	private String name;
	
	private SearchTypes(Class clazz, String name) {
		this.clazz = clazz;
		this.name = name;
	}

	public Class getClazz() {
		return clazz;
	}

	public String getName() {
		return name;
	}
	
	public static Class classFrom(String name) {
		for(SearchTypes t : SearchTypes.values()) {
			if(t.name.equals(name)) {
				return t.clazz;
			}
		}
		return null;
	}
}
