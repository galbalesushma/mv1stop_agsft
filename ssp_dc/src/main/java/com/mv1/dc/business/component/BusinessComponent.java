package com.mv1.dc.business.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.dc.business.common.CommonMapper;
import com.mv1.dc.business.common.Util;
import com.mv1.dc.business.service.CustomBusinessService;
import com.mv1.dc.business.view.web.BusinessListView;
import com.mv1.dc.business.view.web.Category;
import com.mv1.dc.business.view.web.req.BusinessEnrollView;
import com.mv1.dc.repository.CustomCategoryRepository;
import com.mv1.dc.service.AddressService;
import com.mv1.dc.service.BusinessService;
import com.mv1.dc.service.CategoryService;
import com.mv1.dc.service.ContactService;
import com.mv1.dc.service.dto.AddressDTO;
import com.mv1.dc.service.dto.BusinessDTO;
import com.mv1.dc.service.dto.CategoryDTO;
import com.mv1.dc.service.dto.ContactDTO;
import com.mv1.dc.service.mapper.CategoryMapper;

@Component
public class BusinessComponent {

    private final Logger log = LoggerFactory.getLogger(BusinessComponent.class);

    @Autowired
    private AddressService addressService;

    @Autowired
    private CustomBusinessService customBusinessService;

    @Autowired
    private ContactService contactService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private CustomCategoryRepository customCategoryRepository;

    @Autowired
    private BusinessService businessService;

    @Transactional
    public BusinessDTO enrollBusiness(BusinessEnrollView beView) {

	log.debug("enrollBusiness : " + beView);

	if (beView.getId() == null) {
	    // Save address
	    AddressDTO addrDTO = CommonMapper.addressDTOfromBusinessView(beView);
	    addrDTO = addressService.save(addrDTO);

	    // Save contact
	    ContactDTO contDTO = CommonMapper.contactDTOfromBusinessView(beView);
	    contDTO = contactService.save(contDTO);

	    // Save Business
	    CategoryDTO categoryDTO = categoryMapper.toDto(customCategoryRepository.findOneByName(beView.getType()));
	    BusinessDTO businessDTO = CommonMapper.BusinessDTOfrom(beView, beView.getProfileId(), contDTO.getId(),
		    addrDTO.getId(), categoryDTO);

	    return businessService.save(businessDTO);
	} else {
	    // Edit business details
	    Optional<BusinessDTO> optBusinessDto = businessService.findOne(beView.getId());

	    if (optBusinessDto.isPresent()) {
		BusinessDTO dto = optBusinessDto.get();
		dto.setName(beView.getName());
		dto.setDiscount(beView.getDiscount());
		dto.setWebsite(beView.getSite());

		// Get address
		Optional<AddressDTO> optAddrDTO = addressService.findOne(dto.getAddressId());
		if (optAddrDTO.isPresent()) {
		    AddressDTO addrDTO = optAddrDTO.get();
		    addrDTO.setStreet(beView.getStreet());
		    addrDTO.setCity(beView.getCity());
		    addrDTO.setState(beView.getState());

		    // Update address
		    addressService.save(addrDTO);
		}

		// Get contact
		Optional<ContactDTO> optContDTO = contactService.findOne(dto.getContactId());
		if (optContDTO.isPresent()) {
		    ContactDTO contDto = optContDTO.get();
		    contDto.setEmail(beView.getEmail());
		    contDto.setPhone(beView.getPhone());

		    // update contact
		    contactService.save(contDto);
		}
		// update business
		return businessService.save(dto);

	    }
	    return null;
	}
    }

    public BusinessListView getBusinessShortList(String category, int page) {

	List<CategoryDTO> categories = categoryService.findAll();
	Map<CategoryDTO, List<BusinessDTO>> map = new HashMap<>();

	categories.forEach(c -> {
	    if (c.getName().equals(category)) {
		map.put(c, customBusinessService.findAllByCategory(c.getId()));
	    } else if ("ALL".equals(category)) {
		map.put(c, customBusinessService.findAllByCategory(c.getId()));
	    }
	});

	BusinessListView business = new BusinessListView();
	map.keySet().stream().forEach(c -> {
	    business.getCategories().add(prepareList(c, map.get(c), page));
	});

	return business;
    }

    private Category prepareList(CategoryDTO dto, List<BusinessDTO> businessList, int page) {
	Category category = new Category();
	category.setName(dto.getName());
	category.setTotalCount(businessList.size());

	category.setItems(Util
		.paginate(businessList, page).stream().map(b -> CommonMapper.fromBusinessDTO(b,
			addressService.findOne(b.getAddressId()).get(), contactService.findOne(b.getContactId()).get()))
		.collect(Collectors.toList()));
	return category;
    }
}
