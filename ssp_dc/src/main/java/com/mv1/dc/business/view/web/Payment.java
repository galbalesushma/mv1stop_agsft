package com.mv1.dc.business.view.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Payment {

    private Long id;
    private String cardNo;
    private String cvv;
    private String validTill;
    private String firstName;
    private String lastName;
    private String addr;
    private String street;
    private String city;
    private String zip;
    private String state;
    private String country;
    private String phone;
    private String email;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getCardNo() {
	return cardNo;
    }

    public void setCardNo(String cardNo) {
	this.cardNo = cardNo;
    }

    public String getCvv() {
	return cvv;
    }

    public void setCvv(String cvv) {
	this.cvv = cvv;
    }

    public String getValidTill() {
	return validTill;
    }

    public void setValidTill(String validTill) {
	this.validTill = validTill;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getAddr() {
	return addr;
    }

    public void setAddr(String addr) {
	this.addr = addr;
    }

    public String getStreet() {
	return street;
    }

    public void setStreet(String street) {
	this.street = street;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getZip() {
	return zip;
    }

    public void setZip(String zip) {
	this.zip = zip;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Payment other = (Payment) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Payment [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", addr=" + addr
		+ ", street=" + street + ", city=" + city + ", zip=" + zip + ", state=" + state + ", country=" + country
		+ ", phone=" + phone + ", email=" + email + "]";
    }

}
