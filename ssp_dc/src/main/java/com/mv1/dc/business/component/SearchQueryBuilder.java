package com.mv1.dc.business.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicHeader;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class SearchQueryBuilder <T> {
	
	private static final Logger log = LoggerFactory.getLogger(SearchQueryBuilder.class);
    
    private static final String elasticServer = "elastic-search";
    
    private RestClient restClient = RestClient.builder(
		       new HttpHost(elasticServer, 9200, "http"))
			.build();
    
	public List<T> search(Class<T> type, String index, String query) {
    	
    	List<T> list = new ArrayList<>();
    	Map<String, String> params = new HashMap<String, String>();
		                               
		try {
			Response response = null;
			
			if(index != null && index.equals("schools")) {
				log.debug("query for school .. " + query);
				String jsonQuery = "";
				jsonQuery = new JSONObject()
	                  .put("query", new JSONObject()
	                       .put("multi_match", new JSONObject()
	                	       .put("fuzziness", "AUTO")
	                	       .put("query", query)
		                       .put("fields", new JSONArray()
	                    		   .put("street^2")
	                    		   .put("city^4")
	                    		   .put("school_name^5")
	                    		   .put("district^3")
	                    		   .put("state")
		                       )
	                       )
                       ).toString();
				log.debug("json query formed for school .. " + jsonQuery);
				Header[] headers = {new BasicHeader("Content-type", "application/json")};
				HttpEntity entity = new NStringEntity(jsonQuery, ContentType.APPLICATION_JSON);
				response = restClient.performRequest("GET", index + "/_search", params, entity, headers);
			} else {
				params.put("q", query);
				response = restClient.performRequest("GET", "/" + index + "/_search", params);
			}
			/*GET cities/_search
			{ "query": {
			    "prefix" : { "city" : "ap" }
			  }
			}*/
			
			/*String key = "";
			if(index.equals("states") ) {
			    key = "state";
			} else if(index.equals("cities") ) {
			    key = "city";
			} else if(index.equals("districts") ) {
			    key = "district";
			} else {
			    key = "school_name";
			}
			String jsonQuery = "";
			jsonQuery = new JSONObject()
                          .put("query", new JSONObject()
                               .put("prefix", new JSONObject()
                        	   .put(key, new JSONObject()
                        		.put("value",query)
                        		.put("boost", 5.0)
                        	   )
                               )
                           ).toString();
			
			log.debug("json query formed for "+ index +" .. " + jsonQuery);
			Header[] headers = {new BasicHeader("Content-type", "application/json")};
			HttpEntity entity = new NStringEntity(jsonQuery, ContentType.APPLICATION_JSON);
			response = restClient.performRequest("GET", index + "/_search", params, entity, headers);*/
			
			String respStr = EntityUtils.toString(response.getEntity());
			log.debug("elasticsearch top level response :" + respStr);
			JSONObject respObj = new JSONObject(respStr);
			JSONObject hits = respObj.getJSONObject("hits");
			if(hits == null) {
				// return empty list
				return list;
			}
			JSONArray arr = hits.getJSONArray("hits");
			if(arr == null || arr.length() == 0) {
				// return empty list
				return list;
			}
			// create the list from hits
			for(int i =0 ; i < arr.length() ; i++) {
				JSONObject o = arr.getJSONObject(i);
				ObjectMapper mapper = new ObjectMapper();
				JavaType objType = mapper.getTypeFactory().constructType(type);
				T item =  mapper.readValue(o.get("_source").toString(), objType) ;
				list.add(item);
			}
		} catch (IOException | JSONException e) {
			log.error("Error while getting elastic search reponse ", e);
		}
		
    	return list;
    }
}
