package com.mv1.dc.business.component;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mv1.dc.business.common.CommonMapper;
import com.mv1.dc.business.common.Constants;
import com.mv1.dc.business.view.web.DashboardBusinessView;
import com.mv1.dc.business.view.web.DashboardDonationView;
import com.mv1.dc.business.view.web.req.DashboardBusinessReq;
import com.mv1.dc.business.view.web.req.DashboardDonationReq;
import com.mv1.dc.repository.DashboardBusinessRepository;
import com.mv1.dc.repository.DashboardDonationRepository;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

@Component
public class DashboardComponent implements Constants {

    private final Logger log = LoggerFactory.getLogger(DashboardComponent.class);

    @Autowired
    private DashboardDonationRepository donationRepo;
    
    @Autowired
    private DashboardBusinessRepository businessRepo;

    /**
     * Method to get All Donations 
     * 
     * @param req
     * @return
     */
    public List<DashboardDonationView> getDonations(DashboardDonationReq req) {

	log.debug("Fetching Donations report for req {} ", req);

	Object[] repoResponse = donationRepo.getAllDonations(ZonedDateTime.parse(req.getFrom()),
		ZonedDateTime.parse(req.getTo()));
	List<DashboardDonationView> responseList = new LinkedList<>();

	for (Object row : repoResponse) {
	    responseList.add(CommonMapper.donationViewfromObjectArray((Object[]) row));
	}
	return donationFilter(req, responseList);
    }
    
    private List<DashboardDonationView> donationFilter(DashboardDonationReq req, List<DashboardDonationView> list) {
	if (req.getCity() != null) {
	    list.removeIf(d -> !d.getCity().equals(req.getCity()));
	}
	if (req.getState() != null) {
	    list.removeIf(d -> !d.getState().equals(req.getState()));
	}
	if (req.getSchoolId() != null) {
	    list.removeIf(d -> !d.getSchoolId().equals(req.getSchoolId()));
	}
	return list;
    }
    
    /**
     * Method to get All Donations 
     * 
     * @param req
     * @return
     */
    public List<DashboardBusinessView> getBusinesses(DashboardBusinessReq req) {

	log.debug("Fetching Business report for req {} ", req);

	Object[] repoResponse = businessRepo.getAllBusinesses(
                                    		ZonedDateTime.parse(req.getFrom()),
                                    		ZonedDateTime.parse(req.getTo()));
	
	List<DashboardBusinessView> responseList = new LinkedList<>();

	for (Object row : repoResponse) {
	    responseList.add(CommonMapper.businessViewfromObjectArray((Object[]) row));
	}
	
	return businessFilter(req, responseList);
    }
    
    private List<DashboardBusinessView> businessFilter(DashboardBusinessReq req, List<DashboardBusinessView> list) {

	if (req.getCity() != null) {
	    list.removeIf(d -> !d.getCity().equals(req.getCity()));
	}
	if (req.getState() != null) {
	    list.removeIf(d -> !d.getState().equals(req.getState()));
	}
	if (req.getType() != null) {
	    list.removeIf(d -> !d.getType().equals(req.getType()));
	}
	if (req.getName() != null) {
	    list.removeIf(d -> !d.getName().equals(req.getName()));
	}
	if (req.getActive() != null) {
	    list.removeIf(d -> !d.getStatus().equals(req.getActive()));
	}
	return list;
    }

    public File downloadDonationsReport(DashboardDonationReq req) {

	log.debug("Creating CSV of Donations report for req {} ", req);

	String[] col = new String[] { "Date", "Amount", "Name", "Phone", 
		"Email", "Street", "City", "State", "School" };

	return writeCSV(col, "donations_report_", getDonations(req), DashboardDonationView.class);
    }
    
    public File downloadBusinessReport(DashboardBusinessReq req) {

	log.debug("Creating CSV of Donations report for req {} ", req);

	String[] col = new String[] { "Name", "Type", "Phone", "Website", 
		"Email", "Address", "City", "State" };

	return writeCSV(col, "discounts_report_", getBusinesses(req), DashboardBusinessView.class);
    }

    private <T> File writeCSV(String[] col, String reportName, List<T> responseList, Class<T> type) {

	try {
	    File dir = new File("csv_reports");
	    if (!dir.exists()) {
		dir.mkdirs();
	    }
	    String fileName = dir.getAbsolutePath() + "/" + reportName + UUID.randomUUID() + ".csv";
	    Writer writer = new FileWriter(fileName);
	    // Write column heads
	    writer.append(StringUtils.join(col, ",")).append("\n");
	    ColumnPositionMappingStrategy<T> mappingStrategy = new ColumnPositionMappingStrategy<>();
	    mappingStrategy.setType(type);

	    StatefulBeanToCsv<T> sbc = new StatefulBeanToCsvBuilder<T>(writer)
		    .withMappingStrategy(mappingStrategy)
		    .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
		    .build();

	    sbc.write(responseList);
	    writer.close();
	    
	    return new File(fileName);
	} catch (Exception e) {
	    log.debug("Exception while generating csv ", e);
	}
	return null;
    }
}
