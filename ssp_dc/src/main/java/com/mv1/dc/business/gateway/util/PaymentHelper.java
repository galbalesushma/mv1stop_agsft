package com.mv1.dc.business.gateway.util;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mv1.dc.business.gateway.bluepay.BluePay;
import com.mv1.dc.business.gateway.model.PayRequest;
import com.mv1.dc.business.gateway.model.PayResponse;
import com.mv1.dc.business.view.web.Payment;

public class PaymentHelper implements PGConst {

    private static final Logger log = LoggerFactory.getLogger(PaymentHelper.class);
    
    private static final String ACCOUNT_ID = "100618791210";
    private static final String SECRET_KEY = "CSKOCZNVUICPMGFP5FESUFAZC0R7SJCB";
    private static final String MODE = "TEST";

    public static PayRequest getPayRequest(Payment p, String orderId, Long profileId) {

	log.debug("Helper getPayRequest : payment {}, order id {}, profile id {}", p , orderId, profileId);
	
	PayRequest payRequest = new PayRequest();

	// Set Customer Information
	HashMap<String, String> customerParams = new HashMap<>();

	BluePay payment = new BluePay(ACCOUNT_ID, SECRET_KEY, MODE);

	customerParams.put(FIRST_NAME, p.getFirstName());
	customerParams.put(LAST_NAME, p.getLastName());
	customerParams.put(ADDR1, p.getAddr());
	customerParams.put(ADDR2, p.getStreet());
	customerParams.put(CITY, p.getCity());
	customerParams.put(STATE, p.getState());
	customerParams.put(ZIP, p.getZip());
	customerParams.put(COUNTRY, "USA");
	customerParams.put(PHONE, p.getPhone());
	customerParams.put(EMAIL, p.getEmail() != null ? p.getEmail() : "test@bluepay.com");

	payment.setCustomID1(profileId.toString());
	payment.setCustomerInformation(customerParams);
	payment.setOrderID(orderId);

	// Set Credit Card Information
	HashMap<String, String> ccParams = new HashMap<>();
	ccParams.put(CARD_NUMBER, p.getCardNo());
	ccParams.put(EXP_DATE, p.getValidTill());
	ccParams.put(CVV2, p.getCvv());
	payment.setCCInformation(ccParams);

	// Set sale amount: $30.00
	HashMap<String, String> saleParams = new HashMap<>();
	saleParams.put(AMOUNT, "31.00"); // Fixed 30$
	payment.sale(saleParams);
	
	payRequest.setOrderId(orderId);
	payRequest.setPayment(payment);

	return payRequest;
    }

    public static PayResponse getPayResponse(BluePay payment) {
	
	log.debug("Helper getPayResponse : bluepay payment {}", payment);
	
	PayResponse payResponse = new PayResponse();
	
	payResponse.setSuccessful(payment.isSuccessful());
	payResponse.setAuthCode(payment.getAuthCode());
	payResponse.setAvsResp(payment.getAVS());
	payResponse.setCardType(payment.getCardType());
	payResponse.setCvv2Resp(payment.getCVV2());
	payResponse.setMaskedAc(payment.getMaskedPaymentAccount());
	payResponse.setMessage(payment.getMessage());
	payResponse.setStatus(payment.getStatus());
	payResponse.setTxnId(payment.getTransID());
	
	return payResponse;
    }
    
    public static PayResponse getErrorPayResponse(BluePay payment) {
	
	log.debug("Helper getErrorPayResponse : bluepay payment {}", payment);
	
	PayResponse payResponse = new PayResponse();
	
	payResponse.setSuccessful(false);
	payResponse.setAuthCode(payment.getAuthCode());
	payResponse.setAvsResp(payment.getAVS());
	payResponse.setCardType(payment.getCardType());
	payResponse.setCvv2Resp(payment.getCVV2());
	payResponse.setMaskedAc(payment.getMaskedPaymentAccount());
	payResponse.setMessage(payment.getMessage());
	payResponse.setStatus(payment.getStatus());
	payResponse.setTxnId(payment.getTransID());
	
	return payResponse;
    }
}
