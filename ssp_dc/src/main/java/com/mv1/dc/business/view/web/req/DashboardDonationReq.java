package com.mv1.dc.business.view.web.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardDonationReq {

    private Integer page;
    private String from;
    private String to;
    private Long schoolId;
    private String city;
    private String state;

    public Integer getPage() {
	return page;
    }

    public void setPage(Integer page) {
	this.page = page;
    }

    public String getFrom() {
	return from;
    }

    public void setFrom(String from) {
	this.from = from;
    }

    public String getTo() {
	return to;
    }

    public void setTo(String to) {
	this.to = to;
    }

    public Long getSchoolId() {
	return schoolId;
    }

    public void setSchoolId(Long schoolId) {
	this.schoolId = schoolId;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    @Override
    public String toString() {
	return "DonationDashboardReq [page=" + page + ", from=" + from + ", to=" + to + ", schoolId=" + schoolId
		+ ", city=" + city + ", state=" + state + "]";
    }

}
