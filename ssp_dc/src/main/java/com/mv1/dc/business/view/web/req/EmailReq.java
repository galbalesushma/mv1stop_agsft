package com.mv1.dc.business.view.web.req;

public class EmailReq {

    private Long cardId;
    private Long profileId;
    private String email;

    public Long getCardId() {
	return cardId;
    }

    public void setCardId(Long cardId) {
	this.cardId = cardId;
    }

    public Long getProfileId() {
	return profileId;
    }

    public void setProfileId(Long profileId) {
	this.profileId = profileId;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cardId == null) ? 0 : cardId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	EmailReq other = (EmailReq) obj;
	if (cardId == null) {
	    if (other.cardId != null)
		return false;
	} else if (!cardId.equals(other.cardId))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "EmailReq [cardId=" + cardId + ", profileId=" + profileId + ", email=" + email + "]";
    }

}
