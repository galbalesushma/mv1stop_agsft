package com.mv1.dc.business.view.web;

import com.opencsv.bean.CsvBindByPosition;

public class CsvBean {

    @CsvBindByPosition(position = 0)
    private String date;
    @CsvBindByPosition(position = 2)
    private String amount;
    @CsvBindByPosition(position = 3)
    private String name;
    @CsvBindByPosition(position = 4)
    private String phone;
    @CsvBindByPosition(position = 5)
    private String email;
    @CsvBindByPosition(position = 6)
    private String street;
    @CsvBindByPosition(position = 7)
    private String city;
    @CsvBindByPosition(position = 8)
    private String state;
    @CsvBindByPosition(position = 9)
    private String school;

    public String getDate() {
	return date;
    }

    public void setDate(String date) {
	this.date = date;
    }

    public String getAmount() {
	return amount;
    }

    public void setAmount(String amount) {
	this.amount = amount;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getStreet() {
	return street;
    }

    public void setStreet(String street) {
	this.street = street;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    public String getSchool() {
	return school;
    }

    public void setSchool(String school) {
	this.school = school;
    }

    @Override
    public String toString() {
	return "CsvBean [date=" + date + ", amount=" + amount + ", name=" + name + ", phone=" + phone + ", email="
		+ email + ", street=" + street + ", city=" + city + ", state=" + state + ", school=" + school + "]";
    }

}
