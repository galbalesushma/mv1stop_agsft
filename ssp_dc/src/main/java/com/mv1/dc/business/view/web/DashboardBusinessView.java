package com.mv1.dc.business.view.web;

import com.opencsv.bean.CsvBindByPosition;

public class DashboardBusinessView {

    private Long id;
    @CsvBindByPosition(position = 0)
    private String name;
    @CsvBindByPosition(position = 1)
    private String type;
    @CsvBindByPosition(position = 2)
    private String phone;
    @CsvBindByPosition(position = 3)
    private String website;
    @CsvBindByPosition(position = 4)
    private String email;
    @CsvBindByPosition(position = 5)
    private String street;
    @CsvBindByPosition(position = 6)
    private String city;
    @CsvBindByPosition(position = 7)
    private String state;

    private String status;
    private String discount;
    private String date;

    public DashboardBusinessView() {
    }

    public DashboardBusinessView(Long id, String discount, String type, String name, String phone, String email,
	    String street, String city, String state, String website, String status, String date) {
	super();
	this.id = id;
	this.discount = discount;
	this.type = type;
	this.name = name;
	this.phone = phone;
	this.email = email;
	this.street = street;
	this.city = city;
	this.state = state;
	this.website = website;
	this.status = status;
	this.date = date;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getDiscount() {
	return discount;
    }

    public void setDiscount(String discount) {
	this.discount = discount;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getStreet() {
	return street;
    }

    public void setStreet(String street) {
	this.street = street;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    public String getWebsite() {
	return website;
    }

    public void setWebsite(String website) {
	this.website = website;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getDate() {
	return date;
    }

    public void setDate(String date) {
	this.date = date;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	DashboardBusinessView other = (DashboardBusinessView) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "DashboardBusinessView [id=" + id + ", name=" + name + ", type=" + type + ", phone=" + phone
		+ ", website=" + website + ", email=" + email + ", street=" + street + ", city=" + city + ", state="
		+ state + ", status=" + status + ", discount=" + discount + ", date=" + date + "]";
    }

}
