package com.mv1.dc.business.component;

import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.dc.business.common.Util;
import com.mv1.dc.domain.Donation;
import com.mv1.dc.domain.User;
import com.mv1.dc.repository.CustomDonationRepository;
import com.mv1.dc.service.CardService;
import com.mv1.dc.service.ProfileService;
import com.mv1.dc.service.SchoolService;
import com.mv1.dc.service.dto.CardDTO;
import com.mv1.dc.service.dto.ProfileDTO;
import com.mv1.dc.service.dto.SchoolDTO;

@Component
public class CardComponent {
    
    private final Logger log = LoggerFactory.getLogger(CardComponent.class);

    @Autowired
    private CardService cardService;

    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private SchoolService schoolService;
    
    @Autowired
    private CustomDonationRepository customDonationRepository;
    
//    @Autowired
//    private MailService mailService;

    @Transactional
    public CardDTO createCard(Long profileId) {

	Optional<ProfileDTO> profileOptional = profileService.findOne(profileId);
	CardDTO card = null;
	
	if (profileOptional.isPresent()) {
	    
	    ProfileDTO dto = profileOptional.get();
	    card = new CardDTO();
	    card.setCreatedAt(ZonedDateTime.now());
	    card.setIsActive(true);
	    card.setIsDeleted(false);
	    card.setFirstName(dto.getFirstName());
	    card.setLastName(dto.getLastName());
	    card.setNumber(genCardNumber(dto));
	    card.setProfileId(dto.getId());
	    card.setUpdatedAt(ZonedDateTime.now());
	    card.setValidFrom(ZonedDateTime.now());
	    card.setValidTill(ZonedDateTime.now().plusDays(365));
	    
	    card = cardService.save(card);
	}
	
	return card;
    }
    
    @Transactional
    public CardDTO saveCard(Long cardId, String firstName, String lastName) {
	Optional<CardDTO> dto = cardService.findOne(cardId);
	
	if(dto.isPresent()) {
	    CardDTO card = dto.get();
	    card.setFirstName(firstName);
	    card.setLastName(lastName);
	    
	    card = cardService.save(card);
	    
	    File file = generatePDF(cardId, firstName, lastName, card.getNumber(), true);
	    log.debug("Saved the card and generated PDF : {} ", file.getAbsolutePath());
	    return card;
	}
	return null;
    }

    public File generatePDF(Long cardId, String firstName, String lastName, String cardNumber, boolean isForce) {
	/*CacheManager cm = CacheManager.getInstance();
	if(cm.getCache("CARD_PDF") == null) {
	    cm.addCache("CARD_PDF");
	}
	
	Cache cache = cm.getCache("CARD_PDF");
	String fileName = (String) ((Element) cache.get(cardId)).getObjectValue();
	if (fileName != null && !isForce ) {
	    return new File(fileName);
	}*/
	
	// overlay settings
	String uid = UUID.randomUUID().toString();
	String text = firstName + " " + lastName;
	String fileName = "/tmp/card-text-" + uid;
	File input = new File("src/main/resources/images/card1.png");
	File output = new File(fileName + ".png");

	// adding text as overlay to an image
	try {
	    String file = Util.generateCard(text, cardNumber, "png", input, output, false);
//	    cache.put(new Element(cardId, file));
	    return new File(file);
	} catch (Exception e) {
	    log.debug("Error while generating pdf ",e);
	}

	return null;
    }
    
    public File generateImage(Long cardId, String firstName, String lastName, String cardNumber, boolean isForce) {
   	
	// overlay settings
   	String uid = UUID.randomUUID().toString();
   	String text = firstName + " " + lastName;
   	String fileName = "/tmp/card-text-" + uid;
   	File input = new File("src/main/resources/images/card1.png");
   	File output = new File(fileName + ".png");

   	// adding text as overlay to an image
   	try {
   	    String file = Util.generateCard(text, cardNumber, "png", input, output, true);
   	    return new File(file);
   	} catch (Exception e) {
   	    log.debug("Error while generating pdf ",e);
   	}

   	return null;
       }
    
    public Optional<CardDTO> getCard(Long cardId) {
	return cardService.findOne(cardId);
    }
    
    public static void main(String ... args) {
	// overlay settings
   	String uid = UUID.randomUUID().toString();
   	String fileName = "/tmp/card-text-" + uid;
   	File input = new File("src/main/resources/images/card1.png");
   	File output = new File(fileName + ".png");

	try {
   	    String file = Util.generateCard("Mahendra Kale", "NC 12323232323", "png", input, output, true);
   	    System.out.println("card file : "+file);
//   	    return new File(file);
   	} catch (Exception e) {
   	    e.printStackTrace();
//   	    log.debug("Error while generating pdf ",e);
   	}
    }
    
    private String genCardNumber(ProfileDTO dto) {
	
	StringBuilder s = new StringBuilder();
	Optional<Donation> donation = customDonationRepository
		.findAll()
		.stream()
		.filter(d -> d.getProfile().getId().equals(dto.getId()))
		.sorted( (d1,d2) -> d2.getCreatedAt().compareTo(d1.getCreatedAt()))
		.findFirst();

	String dnStr = donation.get().getDonee();
	Optional<SchoolDTO> op = Optional.ofNullable(null);
	Long schoolId = 0l;
	String expDate = DateTimeFormatter.ofPattern("MMyy").format(ZonedDateTime.now().plusDays(365));
	
	if(dnStr != null && dnStr.contains("#")) {
	    schoolId = Long.parseLong(dnStr.split("#")[1]);
	    op = schoolService.findOne(schoolId);
	}
	
	if(op.isPresent()) {
	    
	    SchoolDTO d = op.get();
	    
	    s.append(d.getStateCode());
	    s.append(" - ");
	    s.append(d.getDistrictCode());
	    s.append(" - ");
	    s.append(d.getOrgType());
	    s.append(" ");
	    s.append(d.getOrgId());
	    s.append(" - ");
	    s.append(expDate);
	    
	    return s.toString();
	}
	
	//NC-999-4-998-MMYY
	return "NC - 999 - 4 998 - " + expDate;
    }
    
    public boolean sendMail(String email, Long profileId, Long cardId) {

	User user = new User();
	user.setEmail(email);
	user.setLangKey("EN");
	Optional<CardDTO> optCard = cardService.findOne(cardId);
	if (optCard.isPresent()) {
	    CardDTO card = optCard.get();
	    File file = generatePDF(cardId, card.getFirstName(), card.getLastName(), card.getNumber(), true);
	    try {
		sendCardCreationEmail(user, file);
		return true;
	    } catch (Exception e) {
		log.debug("Error while sending email ",e);
	    }
	}
	return false;
	
	/*CacheManager cm = CacheManager.getInstance();
	if (cm.getCache("CARD_PDF") != null) {
	    Cache cache = cm.getCache("CARD_PDF");
	    String fileName = (String) ((Element) cache.get(cardId)).getObjectValue();
	    if (fileName == null) {
		Optional<CardDTO> optCard = cardService.findOne(cardId);
		if (optCard.isPresent()) {
		    CardDTO card = optCard.get();
		    File file = generatePDF(cardId, card.getFirstName(), card.getLastName(), card.getNumber(), true);
		    sendCardCreationEmail(user, file);
		}
	    } else {
		sendCardCreationEmail(user, new File(fileName));
	    }
	}*/
    }
    
    @Async
    private void sendCardCreationEmail(User user, File file) throws Exception {
	
        log.debug("Sending creation email to '{}'", user.getEmail());
        
//        boolean isHtml = false;
//        boolean isMultipart = true;
        String to = user.getEmail();
        String subject = "Your SSP Discount-Card";
        String content = "Dear user,\n Please find your SSP discount card as an attachment to this mail."
        	+ " \n\n Thanks, \n -- SSP Admin";
//        mailService.sendEmailWithAttachment(to, subject, content, isMultipart, isHtml, file);

        // SMTP info
        String host = "smtp.gmail.com";
        String port = "587";
        String mailFrom = "mahen.agiletest@gmail.com";
        String password = "AgileTest@#";
 
        // attachments
        String[] attachFiles = new String[1];
        attachFiles[0] = file.getAbsolutePath();
	
        Util.sendEmailWithAttachments(host, port, mailFrom, password, to, subject, content, attachFiles);
    }
}
