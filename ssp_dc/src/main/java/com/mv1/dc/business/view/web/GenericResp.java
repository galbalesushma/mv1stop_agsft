package com.mv1.dc.business.view.web;

public class GenericResp {

    private Integer statusCode;
    private String message;
    private boolean isSuccess;

    public Integer getStatusCode() {
	return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
	this.statusCode = statusCode;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public boolean isSuccess() {
	return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
	this.isSuccess = isSuccess;
    }

    @Override
    public String toString() {
	return "GenericResp [statusCode=" + statusCode + ", message=" + message + ", isSuccess=" + isSuccess + "]";
    }

}
