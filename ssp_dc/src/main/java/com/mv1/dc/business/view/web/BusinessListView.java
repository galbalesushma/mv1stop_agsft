package com.mv1.dc.business.view.web;

import java.util.LinkedList;
import java.util.List;

public class BusinessListView {

    private List<Category> categories;

    public BusinessListView() {
	categories = new LinkedList<>();
    }

    public List<Category> getCategories() {
	return categories;
    }

    public void setCategories(List<Category> categories) {
	this.categories = categories;
    }

    @Override
    public String toString() {
	return "Business [categories=" + categories + "]";
    }

}
