package com.mv1.dc.business.gateway.txn;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv1.dc.business.common.CommonMapper;
import com.mv1.dc.business.component.CardComponent;
import com.mv1.dc.business.gateway.bluepay.BluePay;
import com.mv1.dc.business.gateway.model.PayRequest;
import com.mv1.dc.business.gateway.model.PayResponse;
import com.mv1.dc.business.gateway.util.PaymentHelper;
import com.mv1.dc.business.view.web.PayResponseView;
import com.mv1.dc.business.view.web.Payment;
import com.mv1.dc.domain.Profile;
import com.mv1.dc.domain.enumeration.PayStatus;
import com.mv1.dc.repository.CustomDonationRepository;
import com.mv1.dc.repository.CustomProfileRepository;
import com.mv1.dc.service.AddressService;
import com.mv1.dc.service.ContactService;
import com.mv1.dc.service.DonationService;
import com.mv1.dc.service.ProfileService;
import com.mv1.dc.service.dto.AddressDTO;
import com.mv1.dc.service.dto.CardDTO;
import com.mv1.dc.service.dto.ContactDTO;
import com.mv1.dc.service.dto.DonationDTO;
import com.mv1.dc.service.dto.ProfileDTO;
import com.mv1.dc.service.mapper.DonationMapper;
import com.mv1.dc.service.mapper.ProfileMapper;

@Component
public class PaymentProcessor {
    
    private final Logger log = LoggerFactory.getLogger(PaymentProcessor.class);

    @Autowired
    private AddressService addressService;
    
    @Autowired
    private CustomProfileRepository customProfileRepository;
    
    @Autowired
    private ProfileMapper profileMapper;
    
    @Autowired
    private ContactService contactService;
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private DonationService donationService;
    
    @Autowired
    private CustomDonationRepository customDonationRepository;
    
    @Autowired
    private DonationMapper donationMapper;
    
    @Autowired
    private CardComponent cardComponent;
    
    public PayResponseView process(Payment payment, String type, Long id) {
	
	String donee = type + (id != null ? "#"+id : "");
	Long schoolId = type.equalsIgnoreCase("school") ? id : null;
	
	String orderId = UUID.randomUUID().toString();
	// Log the payment in the DB
	Long profileId = save(payment, donee, orderId, schoolId);
	
	// Create payment gateway request
	PayRequest req = PaymentHelper.getPayRequest(payment, orderId, profileId);
	
	// Process the payment with payment gateway
	PayResponse resp = processPayment(req);
	
	// Update the payment status
	updatePayment(orderId, resp);
	
	// Prepare & return the payment response view
	PayResponseView view = new PayResponseView();
	
	view.setMessage(resp.getMessage());
	view.setProfileId(profileId);
	view.setStatus(resp.getStatus());
	
	// Create a card on success
	CardDTO card = new CardDTO();
	if(resp.getStatus().equalsIgnoreCase("APPROVED")) {
	    card = cardComponent.createCard(profileId);
	    view.setCardId(card.getId());
	    view.setMessage("Thank you for your donation !");
	} else {
	    view.setMessage("The payment was unsuccessful, Please try again.");
	}
	
	return view;
    }
    
    private PayResponse processPayment(PayRequest payRequest) {
	
	log.debug("payment request : " + payRequest);

	PayResponse payResponse = null;
	// Makes the API Request with BluePay
	BluePay payment = payRequest.getPayment();
	try {
	    payment.process();
	} catch (Exception ex) {
	    log.debug("Exception: " + ex.toString());
	    payResponse = PaymentHelper.getErrorPayResponse(payment);
	}
	
	payResponse = PaymentHelper.getPayResponse(payment);
	log.debug("Recieved payment response : " + payResponse);

	return payResponse;
    }
    
    @Transactional
    private Long save(Payment payment, String donee, String orderId, Long schoolId) {
		
	ProfileDTO profileDTO = null;
	if(payment.getEmail() != null) {
	    Profile profile = customProfileRepository.findOneByEmail(payment.getEmail());
	    if(profile != null) {
		profileDTO = profileMapper.toDto(profile);
	    } else {
		profileDTO = createProfile(payment);
	    }
	} else {
	    profileDTO = createProfile(payment);
	}
	
	// Create donation
	DonationDTO donationDTO = CommonMapper.donationDTOfrom(payment, profileDTO.getId(), donee, orderId);
	donationDTO.setSchoolId(schoolId);
	
	donationDTO = donationService.save(donationDTO);
	
	return profileDTO.getId();
    }
    
    private ProfileDTO createProfile(Payment payment) {
	// Save address
	AddressDTO addrDTO = CommonMapper.addressDTOfromPayment(payment);
	addrDTO = addressService.save(addrDTO);

	// Save contact
	ContactDTO contDTO = CommonMapper.contactDTOfromPayment(payment);
	contDTO = contactService.save(contDTO);

	// Create profile
	ProfileDTO profileDTO = CommonMapper.profileDTOfrom(payment, addrDTO.getId(), contDTO.getId(), 0l);
	return profileService.save(profileDTO);
	    
    }
    
    @Transactional
    private boolean updatePayment(String orderId, PayResponse resp) {
	
	PayStatus payStatus;
	
	if(resp.getStatus().equalsIgnoreCase("APPROVED")) {
	    payStatus = PayStatus.SUCCESS;
	} else if(resp.getStatus().equalsIgnoreCase("DECLINED")) {
	    payStatus = PayStatus.FAILURE;
	} else {
	    payStatus = PayStatus.AWAITED;
	}
	
	ObjectMapper mapper = new ObjectMapper();
	DonationDTO donationDTO =  donationMapper.toDto(customDonationRepository.findOneByOrderId(orderId));
	donationDTO.setStatus(payStatus);
	donationDTO.setTxnId(resp.getTxnId());
	try {
	    donationDTO.setGatewayResp(mapper.writeValueAsString(resp));
	} catch (Exception e) {
	    // do nothing
	}
	
	donationService.save(donationDTO);
	
	return payStatus.equals(PayStatus.SUCCESS);
    }

    public Payment getPayment(Long profileId) {
	
	Payment p = new Payment();
	if(profileId == null || profileId == 0l) {
	    return p;
	}
	
	ProfileDTO profile = profileService.findOne(profileId).get();
	AddressDTO addr = addressService.findOne(profile.getAddressId()).get();
	ContactDTO cont = contactService.findOne(profile.getContactsId()).get();
	
	p.setId(profileId);
	p.setAddr(addr.getAddr());
	p.setCity(addr.getCity());
	p.setCountry(addr.getCountry());
	p.setEmail(cont.getEmail());
	p.setFirstName(profile.getFirstName());
	p.setLastName(profile.getLastName());
	p.setPhone(cont.getPhone());
	p.setState(addr.getState());
	p.setStreet(addr.getStreet());
	p.setZip(addr.getZip());
	
	return p;
    }
}
