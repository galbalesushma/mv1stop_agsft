package com.mv1.dc.business.view.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class State {

    private Long id;

    private String state;

    private String state_upper;

    private String state_code;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    public String getState_upper() {
	return state_upper;
    }

    public void setState_upper(String state_upper) {
	this.state_upper = state_upper;
    }

    public String getState_code() {
	return state_code;
    }

    public void setState_code(String state_code) {
	this.state_code = state_code;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	State other = (State) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "State [id=" + id + ", state=" + state + ", state_upper=" + state_upper + ", state_code=" + state_code
		+ "]";
    }

}
