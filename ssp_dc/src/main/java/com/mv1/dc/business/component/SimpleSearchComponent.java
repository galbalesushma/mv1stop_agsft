package com.mv1.dc.business.component;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mv1.dc.business.common.CommonMapper;
import com.mv1.dc.business.view.search.SimpleSchool;
import com.mv1.dc.repository.SchoolSearchRepository;

@Component
public class SimpleSearchComponent {
    
    @Autowired
    private SchoolSearchRepository repository;
    
    public List<SimpleSchool> searchSchools(String state, String district, String city, String name) {
	
	List<SimpleSchool> list = new LinkedList<>();
	if(name == null || name.isEmpty() || name.length() < 3) {
	    return list;
	}
	
	List<com.mv1.dc.domain.School> dlist = new LinkedList<>();
	
	if(state != null && district != null && city != null) {
	    dlist = repository.searchShoolsWithSDC(name, state, district, city);
	}
	
	if(state != null && district != null ) {
	    dlist = repository.searchShoolsWithSD(name, state, district);
	}
	
	if( dlist.isEmpty() && district != null ) {
	    dlist = repository.searchShoolsWithDistrict(name, city);
	}
	
	if( dlist.isEmpty() && city != null ) {
	    dlist = repository.searchShoolsWithCity(name, city);
	}
	
	if( dlist.isEmpty() && state != null ) {
	    dlist = repository.searchShoolsWithState(name, state);
	}
	
	if(dlist.isEmpty()) {
	    dlist = repository.searchShoolsByName(name);
	}
	
	dlist.stream().
		forEach(s -> list.add(CommonMapper.fromSchoolDomain(s)));
	
	return list;
    }
}
