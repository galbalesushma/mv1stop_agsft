package com.mv1.dc.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.mv1.dc.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.mv1.dc.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Profile.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Profile.class.getName() + ".cards", jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Profile.class.getName() + ".donations", jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Address.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Contact.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Business.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Business.class.getName() + ".categories", jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.School.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.School.class.getName() + ".donations", jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Donation.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Card.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.dc.domain.Category.class.getName() + ".businesses", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
