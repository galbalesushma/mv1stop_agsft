package com.mv1.dc.domain.enumeration;

/**
 * The ProfileType enumeration.
 */
public enum ProfileType {
    USER, BUSINESS
}
