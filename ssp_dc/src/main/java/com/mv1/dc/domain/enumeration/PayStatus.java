package com.mv1.dc.domain.enumeration;

/**
 * The PayStatus enumeration.
 */
public enum PayStatus {
    INITIATED, SUCCESS, FAILURE, AWAITED
}
