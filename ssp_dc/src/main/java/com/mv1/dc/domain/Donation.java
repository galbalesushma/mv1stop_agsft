package com.mv1.dc.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.mv1.dc.domain.enumeration.PayStatus;

/**
 * A Donation.
 */
@Entity
@Table(name = "donation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Donation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Long amount;

    @Column(name = "txn_id")
    private String txnId;

    @NotNull
    @Column(name = "order_id", nullable = false)
    private String orderId;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private PayStatus status;

    @Column(name = "gateway_resp")
    private String gatewayResp;

    @Column(name = "donee")
    private String donee;

    @Column(name = "school_id")
    private Long schoolId;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @ManyToOne
    @JsonIgnoreProperties("donations")
    private Profile profile;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public Donation amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getTxnId() {
        return txnId;
    }

    public Donation txnId(String txnId) {
        this.txnId = txnId;
        return this;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getOrderId() {
        return orderId;
    }

    public Donation orderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public PayStatus getStatus() {
        return status;
    }

    public Donation status(PayStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(PayStatus status) {
        this.status = status;
    }

    public String getGatewayResp() {
        return gatewayResp;
    }

    public Donation gatewayResp(String gatewayResp) {
        this.gatewayResp = gatewayResp;
        return this;
    }

    public void setGatewayResp(String gatewayResp) {
        this.gatewayResp = gatewayResp;
    }

    public String getDonee() {
        return donee;
    }

    public Donation donee(String donee) {
        this.donee = donee;
        return this;
    }

    public void setDonee(String donee) {
        this.donee = donee;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public Donation schoolId(Long schoolId) {
        this.schoolId = schoolId;
        return this;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Donation createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Donation updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public Donation isDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Profile getProfile() {
        return profile;
    }

    public Donation profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Donation donation = (Donation) o;
        if (donation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), donation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Donation{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", txnId='" + getTxnId() + "'" +
            ", orderId='" + getOrderId() + "'" +
            ", status='" + getStatus() + "'" +
            ", gatewayResp='" + getGatewayResp() + "'" +
            ", donee='" + getDonee() + "'" +
            ", schoolId=" + getSchoolId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            "}";
    }
}
