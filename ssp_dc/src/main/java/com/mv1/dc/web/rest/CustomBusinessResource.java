package com.mv1.dc.web.rest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.dc.business.component.BusinessComponent;
import com.mv1.dc.business.view.web.BusinessListView;
import com.mv1.dc.business.view.web.BusinessRespView;
import com.mv1.dc.business.view.web.req.BusinessEnrollView;
import com.mv1.dc.service.dto.BusinessDTO;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Business.
 */
@RestController
@RequestMapping("/api")
public class CustomBusinessResource {

    private final Logger log = LoggerFactory.getLogger(CustomBusinessResource.class);

    @Autowired
    private BusinessComponent businessComponent;

    /**
     * GET /businesses : get all the businesses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of businesses in
     *         body
     */
    @GetMapping("/business/short-list/{category}/{page}")
    @Timed
    public ResponseEntity<BusinessListView> getBusinessesList(@PathVariable String category,
	    @PathVariable Integer page) {
	log.debug("REST request to get a list of Businesses");
	Optional<BusinessListView> business = Optional.of(businessComponent.getBusinessShortList(category, page));
	return ResponseUtil.wrapOrNotFound(business);
    }

    

    /**
     * POST /business/enroll : Create a new business.
     *
     * @param BusinessEnrollView the BusinessEnrollView to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         businessDTO, or with status 400 (Bad Request) if the business has
     *         already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/business/enroll")
    @Timed
    public ResponseEntity<BusinessRespView> createBusiness(@RequestBody BusinessEnrollView beView)
	    throws URISyntaxException {

	log.debug("REST request to save Business : {}", beView);

	BusinessRespView respView = new BusinessRespView();
	if (beView.getId() != null) {
	    respView.setMessage("A new business cannot be created already have an ID");
	    respView.setStatus(400);
	}

	BusinessDTO result = businessComponent.enrollBusiness(beView);
	String message = "";

	if (result.getId() != null) {
	    message = "Enrolled your business successfully, We'll get back to you with details";
	    respView.setMessage(message);
	    respView.setStatus(200);
	} else {
	    message = "Sorry the business enrollment failed, Please try again";
	    respView.setMessage(message);
	    respView.setStatus(400);
	}

	Optional<BusinessRespView> resp = Optional.of(respView);
	return ResponseUtil.wrapOrNotFound(resp);
    }
}
