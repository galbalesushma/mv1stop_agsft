package com.mv1.dc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mv1.dc.service.DonationService;
import com.mv1.dc.web.rest.errors.BadRequestAlertException;
import com.mv1.dc.web.rest.util.HeaderUtil;
import com.mv1.dc.service.dto.DonationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Donation.
 */
@RestController
@RequestMapping("/api")
public class DonationResource {

    private final Logger log = LoggerFactory.getLogger(DonationResource.class);

    private static final String ENTITY_NAME = "donation";

    private final DonationService donationService;

    public DonationResource(DonationService donationService) {
        this.donationService = donationService;
    }

    /**
     * POST  /donations : Create a new donation.
     *
     * @param donationDTO the donationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new donationDTO, or with status 400 (Bad Request) if the donation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/donations")
    @Timed
    public ResponseEntity<DonationDTO> createDonation(@Valid @RequestBody DonationDTO donationDTO) throws URISyntaxException {
        log.debug("REST request to save Donation : {}", donationDTO);
        if (donationDTO.getId() != null) {
            throw new BadRequestAlertException("A new donation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DonationDTO result = donationService.save(donationDTO);
        return ResponseEntity.created(new URI("/api/donations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /donations : Updates an existing donation.
     *
     * @param donationDTO the donationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated donationDTO,
     * or with status 400 (Bad Request) if the donationDTO is not valid,
     * or with status 500 (Internal Server Error) if the donationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/donations")
    @Timed
    public ResponseEntity<DonationDTO> updateDonation(@Valid @RequestBody DonationDTO donationDTO) throws URISyntaxException {
        log.debug("REST request to update Donation : {}", donationDTO);
        if (donationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DonationDTO result = donationService.save(donationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, donationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /donations : get all the donations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of donations in body
     */
    @GetMapping("/donations")
    @Timed
    public List<DonationDTO> getAllDonations() {
        log.debug("REST request to get all Donations");
        return donationService.findAll();
    }

    /**
     * GET  /donations/:id : get the "id" donation.
     *
     * @param id the id of the donationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the donationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/donations/{id}")
    @Timed
    public ResponseEntity<DonationDTO> getDonation(@PathVariable Long id) {
        log.debug("REST request to get Donation : {}", id);
        Optional<DonationDTO> donationDTO = donationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(donationDTO);
    }

    /**
     * DELETE  /donations/:id : delete the "id" donation.
     *
     * @param id the id of the donationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/donations/{id}")
    @Timed
    public ResponseEntity<Void> deleteDonation(@PathVariable Long id) {
        log.debug("REST request to delete Donation : {}", id);
        donationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
