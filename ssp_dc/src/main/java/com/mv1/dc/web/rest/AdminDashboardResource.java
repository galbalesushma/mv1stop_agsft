package com.mv1.dc.web.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.dc.business.component.DashboardComponent;
import com.mv1.dc.business.view.web.DashboardBusinessView;
import com.mv1.dc.business.view.web.DashboardDonationView;
import com.mv1.dc.business.view.web.Page;
import com.mv1.dc.business.view.web.req.DashboardBusinessReq;
import com.mv1.dc.business.view.web.req.DashboardDonationReq;

/**
 * REST controller for managing Donation.
 */
@RestController
@RequestMapping("/api/dashboard")
public class AdminDashboardResource<T> {

    private final Logger log = LoggerFactory.getLogger(AdminDashboardResource.class);

    @Autowired
    private DashboardComponent component;

    /**
     * GET /donations : get all the donations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of donations in
     *         body
     */
    @SuppressWarnings("unchecked")
    @PostMapping("/donations")
    @Timed
    public Page<T> getAllDonations(@RequestBody DashboardDonationReq req) {

	log.debug("REST request to get a dashboard list of Donations ");
	List<DashboardDonationView> list = component.getDonations(req);

	Page<T> pageView = new Page<>();
	pageView.setResult((List<T>) list);
	pageView.setTotal(list.size());

	return pageView;
    }

    @PostMapping("/donations-dl")
    @Produces("text/csv")
    public void downloadDonationsCSV(HttpServletResponse response, @RequestBody DashboardDonationReq req) {

	try {
	    File file = component.downloadDonationsReport(req);

	    String mimeType = "text/csv";
	    response.setContentType(mimeType);
	    response.setHeader("Content-Disposition", 
		    String.format("attachment; filename=\"" + file.getName() + "\""));
	    response.setContentLength((int) file.length());
	    InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
	    FileCopyUtils.copy(inputStream, response.getOutputStream());
	} catch (IOException ex) {
	    log.debug("Error while downloading csv file",ex);
	}
    }
    
    /**
     * GET /donations : get all the donations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of donations in
     *         body
     */
    @SuppressWarnings("unchecked")
    @PostMapping("/discounts")
    @Timed
    public Page<T> getAllDiscounts(@RequestBody DashboardBusinessReq req) {

	log.debug("REST request to get a dashboard list of business Discounts ");
	List<DashboardBusinessView> list = component.getBusinesses(req);

	Page<T> pageView = new Page<>();
	pageView.setResult((List<T>) list);
	pageView.setTotal(list.size());

	return pageView;
    }

    @PostMapping("/discounts-dl")
    @Produces("text/csv")
    public void downloadDiscountsCSV(HttpServletResponse response, @RequestBody DashboardBusinessReq req) {

	try {
	    File file = component.downloadBusinessReport(req);

	    String mimeType = "text/csv";
	    response.setContentType(mimeType);
	    response.setHeader("Content-Disposition", 
		    String.format("attachment; filename=\"" + file.getName() + "\""));
	    response.setContentLength((int) file.length());
	    InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
	    FileCopyUtils.copy(inputStream, response.getOutputStream());
	} catch (IOException ex) {
	    log.debug("Error while downloading discounts csv file",ex);
	}
    }
}
