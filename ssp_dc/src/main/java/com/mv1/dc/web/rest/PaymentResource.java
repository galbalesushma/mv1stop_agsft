package com.mv1.dc.web.rest;

import java.net.URISyntaxException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.dc.business.gateway.txn.PaymentProcessor;
import com.mv1.dc.business.view.web.PayResponseView;
import com.mv1.dc.business.view.web.Payment;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PaymentGateway.
 */
@RestController
@RequestMapping("/api")
public class PaymentResource {

    private final Logger log = LoggerFactory.getLogger(PaymentResource.class);

    @Autowired
    private PaymentProcessor paymentProcessor;

    /**
     * POST  /payment : Create a new paymentGateway.
     *
     * @param payment the payment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new payment, or with status 400 (Bad Request) if the paymentGateway has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment")
    @Timed
    public ResponseEntity<PayResponseView> processDefaultPayment(@RequestBody Payment payment) throws URISyntaxException {
        log.debug("REST request to process default payment : {}", payment);
        String type = "ssp";
        Long id = 0l;
	
        PayResponseView view = paymentProcessor.process(payment, type, id);
        return ResponseUtil.wrapOrNotFound(Optional.of(view));
    }
    
    /**
     * POST  /payment : Create a new paymentGateway.
     *
     * @param payment the payment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new payment, or with status 400 (Bad Request) if the paymentGateway has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payment/{type}/{id}")
    @Timed
    public ResponseEntity<PayResponseView> processPayment(@PathVariable String type, @PathVariable Long id,
	    @RequestBody Payment payment) throws URISyntaxException {
        log.debug("REST request to process payment : {}", payment);
        PayResponseView view = paymentProcessor.process(payment, type, id);
        return ResponseUtil.wrapOrNotFound(Optional.of(view));
    }

    /**
     * GET  /payment/:profileId : get the payment by profileId.
     *
     * @param profileId the id of the payment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the payment, or with status 404 (Not Found)
     */
    @GetMapping("/payment/{profileId}")
    @Timed
    public ResponseEntity<Payment> getPaymentGateway(@PathVariable Long profileId) {
        log.debug("REST request to get Payment info by profile id: {}", profileId);
        Payment payment = paymentProcessor.getPayment(profileId);
        return ResponseUtil.wrapOrNotFound(Optional.of(payment));
    }
}
