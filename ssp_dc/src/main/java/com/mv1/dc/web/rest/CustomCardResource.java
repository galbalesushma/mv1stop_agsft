package com.mv1.dc.web.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.dc.business.component.CardComponent;
import com.mv1.dc.business.view.web.GenericResp;
import com.mv1.dc.business.view.web.req.CardView;
import com.mv1.dc.business.view.web.req.EmailReq;
import com.mv1.dc.service.CardService;
import com.mv1.dc.service.ContactService;
import com.mv1.dc.service.ProfileService;
import com.mv1.dc.service.dto.CardDTO;
import com.mv1.dc.service.dto.ContactDTO;
import com.mv1.dc.service.dto.ProfileDTO;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Card.
 */
@RestController
@RequestMapping("/api/card")
public class CustomCardResource {

    private final Logger log = LoggerFactory.getLogger(CustomCardResource.class);

    @Autowired
    private CardService cardService;
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private ContactService contactService;
    
    @Autowired
    private CardComponent component;

    /**
     * GET  /cards/:id : get the "id" card.
     *
     * @param profileId the id of the cardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cardDTO, or with status 404 (Not Found)
     */
    @PostMapping("/email")
    @Timed
    public ResponseEntity<GenericResp> emailCard(@RequestBody EmailReq req) {
        log.debug("REST request to get Card : {}", req);
        boolean status = false;
        Optional<CardDTO> optCard = cardService.findOne(req.getCardId());
        if(optCard.isPresent()) {
            Optional<ProfileDTO> optProfile = profileService.findOne(req.getProfileId());
            if(optProfile.isPresent()) {
        	String email = "";
        	if(req.getEmail() == null) {
        	    Optional<ContactDTO> optCont = contactService.findOne(optProfile.get().getContactsId());
        	    email = optCont.get().getEmail();
        	} else {
        	    email = req.getEmail();
        	}
        	status = component.sendMail(email, req.getProfileId(), req.getCardId());
            }
        }
        GenericResp resp = new GenericResp();
        resp.setSuccess(status);
        if(status) {
            resp.setMessage("Email sent successfully");
            resp.setStatusCode(200);
        } else {
            resp.setMessage("Error while sending email, Please try again.");
            resp.setStatusCode(400);
        }
        Optional<GenericResp> respOpt = Optional.of(resp);
        return ResponseUtil.wrapOrNotFound(respOpt);
    }

    /**
     * POST /save/ : save the name on the card.
     *
     * @param id the id of the cardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cardDTO, or with status 404 (Not Found)
     */
    @PostMapping("/save")
    @Timed
    public ResponseEntity<CardDTO> saveCard(@RequestBody CardView card) {
        log.debug("REST request to save Card names  : {}", card);
        Optional<CardDTO> cardDTO = Optional.of(component.saveCard(card.getId(), card.getFirstName(), card.getLastName()));
        return ResponseUtil.wrapOrNotFound(cardDTO);
    }
    
    @GetMapping("/download/{id}")
    @Produces("application/pdf;application/x-pdf")
    public void downloadCard(HttpServletResponse response, @PathVariable Long id) {

	try {
	    Optional<CardDTO> optCard = cardService.findOne(id);
	    if (optCard.isPresent()) {
		CardDTO card = optCard.get();
		File file = component.generatePDF(id, card.getFirstName(), card.getLastName(), card.getNumber(), false);

		String mimeType = "application/pdf;application/x-pdf";
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition",
			String.format("attachment; filename=\"" + file.getName() + "\""));
		response.setContentLength((int) file.length());
		InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
		FileCopyUtils.copy(inputStream, response.getOutputStream());
	    }
	} catch (IOException ex) {
	    log.debug("Error while downloading csv file",ex);
	}
    }
    
    @GetMapping("/image/{id}")
    @Produces("image/png")
    public void downloadImage(HttpServletResponse response, @PathVariable Long id) {

	try {
	    Optional<CardDTO> optCard = cardService.findOne(id);
	    if (optCard.isPresent()) {
		CardDTO card = optCard.get();
		File file = component.generateImage(id, card.getFirstName(), card.getLastName(), card.getNumber(), false);

		String mimeType = "image/png";
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition",
			String.format("attachment; filename=\"" + file.getName() + "\""));
		response.setContentLength((int) file.length());
		InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
		FileCopyUtils.copy(inputStream, response.getOutputStream());
	    }
	} catch (IOException ex) {
	    log.debug("Error while downloading csv file",ex);
	}
    }
}
