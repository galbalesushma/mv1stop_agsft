/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mv1.dc.web.rest.vm;
