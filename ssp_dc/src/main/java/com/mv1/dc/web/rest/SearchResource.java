package com.mv1.dc.web.rest;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mv1.dc.business.component.SearchQueryBuilder;
import com.mv1.dc.business.component.SimpleSearchComponent;
import com.mv1.dc.business.enumeration.SearchTypes;
import com.mv1.dc.business.view.search.SimpleSchool;

@RestController
@RequestMapping("/api/search")
public class SearchResource<T> {

    @Autowired
    private SearchQueryBuilder<T> searchQueryBuilder;
    
    @Autowired
    private SimpleSearchComponent simpleSearch;

    @SuppressWarnings("unchecked")
    @GetMapping(value = "/{index}/{text}")
    public List<T> search(@PathVariable String index, @PathVariable String text) {
	return searchQueryBuilder.search(SearchTypes.classFrom(index), index, text);
    }

    @GetMapping(value = "/query/{text}")
    public List<SimpleSchool> searchSchools(@PathVariable String text) {
	
	if(!text.contains("name")) {
	    // return empty results if no school name is typed
	    return new LinkedList<>();
	}
	
	String state = null, district = null, city = null, name = null ;
	if(text != null && !text.isEmpty() && !text.contains("&")) {
	    if(text.startsWith("name")) {
		name = text.split("=")[1];
	    } else if (text.startsWith("state")) {
		state = text.split("=")[1];
	    } else if (text.startsWith("district")) {
		district = text.split("=")[1];
	    } else if (text.startsWith("city")) {
		city = text.split("=")[1];
	    }
	} else {
	    String[] arr = text.split("&");
	    for (String s : arr) {
		if (s.startsWith("name")) {
		    name = s.split("=")[1];
		} else if (s.startsWith("state")) {
		    state = s.split("=")[1];
		} else if (s.startsWith("district")) {
		    district = s.split("=")[1];
		} else if (s.startsWith("city")) {
		    city = s.split("=")[1];
		}
	    }
	}
	
	return simpleSearch.searchSchools(state, district, city, name);
    }
}