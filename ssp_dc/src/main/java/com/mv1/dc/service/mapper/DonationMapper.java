package com.mv1.dc.service.mapper;

import com.mv1.dc.domain.*;
import com.mv1.dc.service.dto.DonationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Donation and its DTO DonationDTO.
 */
@Mapper(componentModel = "spring", uses = {ProfileMapper.class})
public interface DonationMapper extends EntityMapper<DonationDTO, Donation> {

    @Mapping(source = "profile.id", target = "profileId")
    DonationDTO toDto(Donation donation);

    @Mapping(source = "profileId", target = "profile")
    Donation toEntity(DonationDTO donationDTO);

    default Donation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Donation donation = new Donation();
        donation.setId(id);
        return donation;
    }
}
