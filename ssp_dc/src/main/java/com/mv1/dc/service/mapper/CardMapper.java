package com.mv1.dc.service.mapper;

import com.mv1.dc.domain.*;
import com.mv1.dc.service.dto.CardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Card and its DTO CardDTO.
 */
@Mapper(componentModel = "spring", uses = {ProfileMapper.class})
public interface CardMapper extends EntityMapper<CardDTO, Card> {

    @Mapping(source = "profile.id", target = "profileId")
    CardDTO toDto(Card card);

    @Mapping(source = "profileId", target = "profile")
    Card toEntity(CardDTO cardDTO);

    default Card fromId(Long id) {
        if (id == null) {
            return null;
        }
        Card card = new Card();
        card.setId(id);
        return card;
    }
}
