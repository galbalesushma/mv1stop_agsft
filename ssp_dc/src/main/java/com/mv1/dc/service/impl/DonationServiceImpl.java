package com.mv1.dc.service.impl;

import com.mv1.dc.service.DonationService;
import com.mv1.dc.domain.Donation;
import com.mv1.dc.repository.DonationRepository;
import com.mv1.dc.service.dto.DonationDTO;
import com.mv1.dc.service.mapper.DonationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 * Service Implementation for managing Donation.
 */
@Service
@Transactional
public class DonationServiceImpl implements DonationService {

    private final Logger log = LoggerFactory.getLogger(DonationServiceImpl.class);

    private final DonationRepository donationRepository;

    private final DonationMapper donationMapper;

    public DonationServiceImpl(DonationRepository donationRepository, DonationMapper donationMapper) {
        this.donationRepository = donationRepository;
        this.donationMapper = donationMapper;
    }

    /**
     * Save a donation.
     *
     * @param donationDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DonationDTO save(DonationDTO donationDTO) {
        log.debug("Request to save Donation : {}", donationDTO);
        Donation donation = donationMapper.toEntity(donationDTO);
        donation = donationRepository.save(donation);
        return donationMapper.toDto(donation);
    }

    /**
     * Get all the donations.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DonationDTO> findAll() {
        log.debug("Request to get all Donations");
        return donationRepository.findAll().stream()
            .map(donationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one donation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DonationDTO> findOne(Long id) {
        log.debug("Request to get Donation : {}", id);
        return donationRepository.findById(id)
            .map(donationMapper::toDto);
    }

    /**
     * Delete the donation by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Donation : {}", id);
        donationRepository.deleteById(id);
    }
}
