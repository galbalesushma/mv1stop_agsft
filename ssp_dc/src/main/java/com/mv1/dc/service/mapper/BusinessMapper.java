package com.mv1.dc.service.mapper;

import com.mv1.dc.domain.*;
import com.mv1.dc.service.dto.BusinessDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Business and its DTO BusinessDTO.
 */
@Mapper(componentModel = "spring", uses = {ProfileMapper.class, ContactMapper.class, AddressMapper.class, CategoryMapper.class})
public interface BusinessMapper extends EntityMapper<BusinessDTO, Business> {

    @Mapping(source = "profile.id", target = "profileId")
    @Mapping(source = "contact.id", target = "contactId")
    @Mapping(source = "address.id", target = "addressId")
    BusinessDTO toDto(Business business);

    @Mapping(source = "profileId", target = "profile")
    @Mapping(source = "contactId", target = "contact")
    @Mapping(source = "addressId", target = "address")
    Business toEntity(BusinessDTO businessDTO);

    default Business fromId(Long id) {
        if (id == null) {
            return null;
        }
        Business business = new Business();
        business.setId(id);
        return business;
    }
}
