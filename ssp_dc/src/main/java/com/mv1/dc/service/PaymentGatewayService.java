package com.mv1.dc.service;

import com.mv1.dc.service.dto.PaymentGatewayDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing PaymentGateway.
 */
public interface PaymentGatewayService {

    /**
     * Save a paymentGateway.
     *
     * @param paymentGatewayDTO the entity to save
     * @return the persisted entity
     */
    PaymentGatewayDTO save(PaymentGatewayDTO paymentGatewayDTO);

    /**
     * Get all the paymentGateways.
     *
     * @return the list of entities
     */
    List<PaymentGatewayDTO> findAll();


    /**
     * Get the "id" paymentGateway.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PaymentGatewayDTO> findOne(Long id);

    /**
     * Delete the "id" paymentGateway.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
