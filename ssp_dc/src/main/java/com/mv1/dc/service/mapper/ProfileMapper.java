package com.mv1.dc.service.mapper;

import com.mv1.dc.domain.*;
import com.mv1.dc.service.dto.ProfileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Profile and its DTO ProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {AddressMapper.class, ContactMapper.class})
public interface ProfileMapper extends EntityMapper<ProfileDTO, Profile> {

    @Mapping(source = "address.id", target = "addressId")
    @Mapping(source = "contacts.id", target = "contactsId")
    ProfileDTO toDto(Profile profile);

    @Mapping(source = "addressId", target = "address")
    @Mapping(source = "contactsId", target = "contacts")
    @Mapping(target = "cards", ignore = true)
    @Mapping(target = "donations", ignore = true)
    Profile toEntity(ProfileDTO profileDTO);

    default Profile fromId(Long id) {
        if (id == null) {
            return null;
        }
        Profile profile = new Profile();
        profile.setId(id);
        return profile;
    }
}
