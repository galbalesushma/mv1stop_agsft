package com.mv1.dc.service;

import com.mv1.dc.service.dto.DonationDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Donation.
 */
public interface DonationService {

    /**
     * Save a donation.
     *
     * @param donationDTO the entity to save
     * @return the persisted entity
     */
    DonationDTO save(DonationDTO donationDTO);

    /**
     * Get all the donations.
     *
     * @return the list of entities
     */
    List<DonationDTO> findAll();


    /**
     * Get the "id" donation.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DonationDTO> findOne(Long id);

    /**
     * Delete the "id" donation.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
