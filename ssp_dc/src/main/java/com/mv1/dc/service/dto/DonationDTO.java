package com.mv1.dc.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.mv1.dc.domain.enumeration.PayStatus;

/**
 * A DTO for the Donation entity.
 */
public class DonationDTO implements Serializable {

    private Long id;

    @NotNull
    private Long amount;

    private String txnId;

    @NotNull
    private String orderId;

    @NotNull
    private PayStatus status;

    private String gatewayResp;

    private String donee;

    private Long schoolId;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean isDeleted;

    private Long profileId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public PayStatus getStatus() {
        return status;
    }

    public void setStatus(PayStatus status) {
        this.status = status;
    }

    public String getGatewayResp() {
        return gatewayResp;
    }

    public void setGatewayResp(String gatewayResp) {
        this.gatewayResp = gatewayResp;
    }

    public String getDonee() {
        return donee;
    }

    public void setDonee(String donee) {
        this.donee = donee;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DonationDTO donationDTO = (DonationDTO) o;
        if (donationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), donationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DonationDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", txnId='" + getTxnId() + "'" +
            ", orderId='" + getOrderId() + "'" +
            ", status='" + getStatus() + "'" +
            ", gatewayResp='" + getGatewayResp() + "'" +
            ", donee='" + getDonee() + "'" +
            ", schoolId=" + getSchoolId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            ", profile=" + getProfileId() +
            "}";
    }
}
