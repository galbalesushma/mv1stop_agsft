/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { DonationMvDetailComponent } from 'app/entities/donation-mv/donation-mv-detail.component';
import { DonationMv } from 'app/shared/model/donation-mv.model';

describe('Component Tests', () => {
    describe('DonationMv Management Detail Component', () => {
        let comp: DonationMvDetailComponent;
        let fixture: ComponentFixture<DonationMvDetailComponent>;
        const route = ({ data: of({ donation: new DonationMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [DonationMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(DonationMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DonationMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.donation).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
