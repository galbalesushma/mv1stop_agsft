/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SspDcTestModule } from '../../../test.module';
import { DonationMvDeleteDialogComponent } from 'app/entities/donation-mv/donation-mv-delete-dialog.component';
import { DonationMvService } from 'app/entities/donation-mv/donation-mv.service';

describe('Component Tests', () => {
    describe('DonationMv Management Delete Component', () => {
        let comp: DonationMvDeleteDialogComponent;
        let fixture: ComponentFixture<DonationMvDeleteDialogComponent>;
        let service: DonationMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [DonationMvDeleteDialogComponent]
            })
                .overrideTemplate(DonationMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DonationMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DonationMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
