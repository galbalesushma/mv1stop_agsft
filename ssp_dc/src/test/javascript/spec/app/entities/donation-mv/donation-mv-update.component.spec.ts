/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { DonationMvUpdateComponent } from 'app/entities/donation-mv/donation-mv-update.component';
import { DonationMvService } from 'app/entities/donation-mv/donation-mv.service';
import { DonationMv } from 'app/shared/model/donation-mv.model';

describe('Component Tests', () => {
    describe('DonationMv Management Update Component', () => {
        let comp: DonationMvUpdateComponent;
        let fixture: ComponentFixture<DonationMvUpdateComponent>;
        let service: DonationMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [DonationMvUpdateComponent]
            })
                .overrideTemplate(DonationMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DonationMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DonationMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DonationMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.donation = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DonationMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.donation = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
