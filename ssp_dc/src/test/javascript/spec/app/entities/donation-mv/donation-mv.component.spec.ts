/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SspDcTestModule } from '../../../test.module';
import { DonationMvComponent } from 'app/entities/donation-mv/donation-mv.component';
import { DonationMvService } from 'app/entities/donation-mv/donation-mv.service';
import { DonationMv } from 'app/shared/model/donation-mv.model';

describe('Component Tests', () => {
    describe('DonationMv Management Component', () => {
        let comp: DonationMvComponent;
        let fixture: ComponentFixture<DonationMvComponent>;
        let service: DonationMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [DonationMvComponent],
                providers: []
            })
                .overrideTemplate(DonationMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DonationMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DonationMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new DonationMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.donations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
