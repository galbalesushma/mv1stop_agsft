/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { CategoryMvUpdateComponent } from 'app/entities/category-mv/category-mv-update.component';
import { CategoryMvService } from 'app/entities/category-mv/category-mv.service';
import { CategoryMv } from 'app/shared/model/category-mv.model';

describe('Component Tests', () => {
    describe('CategoryMv Management Update Component', () => {
        let comp: CategoryMvUpdateComponent;
        let fixture: ComponentFixture<CategoryMvUpdateComponent>;
        let service: CategoryMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [CategoryMvUpdateComponent]
            })
                .overrideTemplate(CategoryMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CategoryMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CategoryMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CategoryMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.category = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CategoryMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.category = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
