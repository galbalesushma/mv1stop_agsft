/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { CategoryMvDetailComponent } from 'app/entities/category-mv/category-mv-detail.component';
import { CategoryMv } from 'app/shared/model/category-mv.model';

describe('Component Tests', () => {
    describe('CategoryMv Management Detail Component', () => {
        let comp: CategoryMvDetailComponent;
        let fixture: ComponentFixture<CategoryMvDetailComponent>;
        const route = ({ data: of({ category: new CategoryMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [CategoryMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CategoryMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CategoryMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.category).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
