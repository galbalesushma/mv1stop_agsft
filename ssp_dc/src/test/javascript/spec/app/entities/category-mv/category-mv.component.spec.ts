/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SspDcTestModule } from '../../../test.module';
import { CategoryMvComponent } from 'app/entities/category-mv/category-mv.component';
import { CategoryMvService } from 'app/entities/category-mv/category-mv.service';
import { CategoryMv } from 'app/shared/model/category-mv.model';

describe('Component Tests', () => {
    describe('CategoryMv Management Component', () => {
        let comp: CategoryMvComponent;
        let fixture: ComponentFixture<CategoryMvComponent>;
        let service: CategoryMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [CategoryMvComponent],
                providers: []
            })
                .overrideTemplate(CategoryMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CategoryMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CategoryMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CategoryMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.categories[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
