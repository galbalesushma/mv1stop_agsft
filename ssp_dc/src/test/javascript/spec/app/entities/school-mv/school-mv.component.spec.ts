/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SspDcTestModule } from '../../../test.module';
import { SchoolMvComponent } from 'app/entities/school-mv/school-mv.component';
import { SchoolMvService } from 'app/entities/school-mv/school-mv.service';
import { SchoolMv } from 'app/shared/model/school-mv.model';

describe('Component Tests', () => {
    describe('SchoolMv Management Component', () => {
        let comp: SchoolMvComponent;
        let fixture: ComponentFixture<SchoolMvComponent>;
        let service: SchoolMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [SchoolMvComponent],
                providers: []
            })
                .overrideTemplate(SchoolMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SchoolMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SchoolMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new SchoolMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.schools[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
