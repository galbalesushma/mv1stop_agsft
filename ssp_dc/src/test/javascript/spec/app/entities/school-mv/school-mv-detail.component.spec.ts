/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { SchoolMvDetailComponent } from 'app/entities/school-mv/school-mv-detail.component';
import { SchoolMv } from 'app/shared/model/school-mv.model';

describe('Component Tests', () => {
    describe('SchoolMv Management Detail Component', () => {
        let comp: SchoolMvDetailComponent;
        let fixture: ComponentFixture<SchoolMvDetailComponent>;
        const route = ({ data: of({ school: new SchoolMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [SchoolMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SchoolMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SchoolMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.school).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
