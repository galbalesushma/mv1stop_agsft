/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { SchoolMvUpdateComponent } from 'app/entities/school-mv/school-mv-update.component';
import { SchoolMvService } from 'app/entities/school-mv/school-mv.service';
import { SchoolMv } from 'app/shared/model/school-mv.model';

describe('Component Tests', () => {
    describe('SchoolMv Management Update Component', () => {
        let comp: SchoolMvUpdateComponent;
        let fixture: ComponentFixture<SchoolMvUpdateComponent>;
        let service: SchoolMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [SchoolMvUpdateComponent]
            })
                .overrideTemplate(SchoolMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SchoolMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SchoolMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SchoolMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.school = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SchoolMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.school = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
