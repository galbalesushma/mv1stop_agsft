/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SspDcTestModule } from '../../../test.module';
import { SchoolMvDeleteDialogComponent } from 'app/entities/school-mv/school-mv-delete-dialog.component';
import { SchoolMvService } from 'app/entities/school-mv/school-mv.service';

describe('Component Tests', () => {
    describe('SchoolMv Management Delete Component', () => {
        let comp: SchoolMvDeleteDialogComponent;
        let fixture: ComponentFixture<SchoolMvDeleteDialogComponent>;
        let service: SchoolMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [SchoolMvDeleteDialogComponent]
            })
                .overrideTemplate(SchoolMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SchoolMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SchoolMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
