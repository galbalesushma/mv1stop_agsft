/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { ContactMvDetailComponent } from 'app/entities/contact-mv/contact-mv-detail.component';
import { ContactMv } from 'app/shared/model/contact-mv.model';

describe('Component Tests', () => {
    describe('ContactMv Management Detail Component', () => {
        let comp: ContactMvDetailComponent;
        let fixture: ComponentFixture<ContactMvDetailComponent>;
        const route = ({ data: of({ contact: new ContactMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [ContactMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ContactMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ContactMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.contact).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
