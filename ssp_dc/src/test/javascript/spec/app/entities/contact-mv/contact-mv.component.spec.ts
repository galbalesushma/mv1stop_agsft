/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SspDcTestModule } from '../../../test.module';
import { ContactMvComponent } from 'app/entities/contact-mv/contact-mv.component';
import { ContactMvService } from 'app/entities/contact-mv/contact-mv.service';
import { ContactMv } from 'app/shared/model/contact-mv.model';

describe('Component Tests', () => {
    describe('ContactMv Management Component', () => {
        let comp: ContactMvComponent;
        let fixture: ComponentFixture<ContactMvComponent>;
        let service: ContactMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [ContactMvComponent],
                providers: []
            })
                .overrideTemplate(ContactMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ContactMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ContactMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new ContactMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.contacts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
