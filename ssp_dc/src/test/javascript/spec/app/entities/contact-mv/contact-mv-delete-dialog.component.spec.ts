/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SspDcTestModule } from '../../../test.module';
import { ContactMvDeleteDialogComponent } from 'app/entities/contact-mv/contact-mv-delete-dialog.component';
import { ContactMvService } from 'app/entities/contact-mv/contact-mv.service';

describe('Component Tests', () => {
    describe('ContactMv Management Delete Component', () => {
        let comp: ContactMvDeleteDialogComponent;
        let fixture: ComponentFixture<ContactMvDeleteDialogComponent>;
        let service: ContactMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [ContactMvDeleteDialogComponent]
            })
                .overrideTemplate(ContactMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ContactMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ContactMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
