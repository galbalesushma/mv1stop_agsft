/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { ContactMvUpdateComponent } from 'app/entities/contact-mv/contact-mv-update.component';
import { ContactMvService } from 'app/entities/contact-mv/contact-mv.service';
import { ContactMv } from 'app/shared/model/contact-mv.model';

describe('Component Tests', () => {
    describe('ContactMv Management Update Component', () => {
        let comp: ContactMvUpdateComponent;
        let fixture: ComponentFixture<ContactMvUpdateComponent>;
        let service: ContactMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [ContactMvUpdateComponent]
            })
                .overrideTemplate(ContactMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ContactMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ContactMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ContactMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.contact = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ContactMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.contact = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
