/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { CardMvDetailComponent } from 'app/entities/card-mv/card-mv-detail.component';
import { CardMv } from 'app/shared/model/card-mv.model';

describe('Component Tests', () => {
    describe('CardMv Management Detail Component', () => {
        let comp: CardMvDetailComponent;
        let fixture: ComponentFixture<CardMvDetailComponent>;
        const route = ({ data: of({ card: new CardMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [CardMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CardMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CardMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.card).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
