/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { CardMvUpdateComponent } from 'app/entities/card-mv/card-mv-update.component';
import { CardMvService } from 'app/entities/card-mv/card-mv.service';
import { CardMv } from 'app/shared/model/card-mv.model';

describe('Component Tests', () => {
    describe('CardMv Management Update Component', () => {
        let comp: CardMvUpdateComponent;
        let fixture: ComponentFixture<CardMvUpdateComponent>;
        let service: CardMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [CardMvUpdateComponent]
            })
                .overrideTemplate(CardMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CardMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CardMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CardMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.card = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CardMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.card = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
