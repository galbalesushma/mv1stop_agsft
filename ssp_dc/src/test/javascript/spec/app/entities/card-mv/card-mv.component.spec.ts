/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SspDcTestModule } from '../../../test.module';
import { CardMvComponent } from 'app/entities/card-mv/card-mv.component';
import { CardMvService } from 'app/entities/card-mv/card-mv.service';
import { CardMv } from 'app/shared/model/card-mv.model';

describe('Component Tests', () => {
    describe('CardMv Management Component', () => {
        let comp: CardMvComponent;
        let fixture: ComponentFixture<CardMvComponent>;
        let service: CardMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [CardMvComponent],
                providers: []
            })
                .overrideTemplate(CardMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CardMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CardMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CardMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.cards[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
