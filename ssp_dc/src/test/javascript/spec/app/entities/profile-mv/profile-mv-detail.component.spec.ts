/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { ProfileMvDetailComponent } from 'app/entities/profile-mv/profile-mv-detail.component';
import { ProfileMv } from 'app/shared/model/profile-mv.model';

describe('Component Tests', () => {
    describe('ProfileMv Management Detail Component', () => {
        let comp: ProfileMvDetailComponent;
        let fixture: ComponentFixture<ProfileMvDetailComponent>;
        const route = ({ data: of({ profile: new ProfileMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [ProfileMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ProfileMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProfileMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.profile).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
