/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SspDcTestModule } from '../../../test.module';
import { PaymentGatewayMvDeleteDialogComponent } from 'app/entities/payment-gateway-mv/payment-gateway-mv-delete-dialog.component';
import { PaymentGatewayMvService } from 'app/entities/payment-gateway-mv/payment-gateway-mv.service';

describe('Component Tests', () => {
    describe('PaymentGatewayMv Management Delete Component', () => {
        let comp: PaymentGatewayMvDeleteDialogComponent;
        let fixture: ComponentFixture<PaymentGatewayMvDeleteDialogComponent>;
        let service: PaymentGatewayMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [PaymentGatewayMvDeleteDialogComponent]
            })
                .overrideTemplate(PaymentGatewayMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PaymentGatewayMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentGatewayMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
