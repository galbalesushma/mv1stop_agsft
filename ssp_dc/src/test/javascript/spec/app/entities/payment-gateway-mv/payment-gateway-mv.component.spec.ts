/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SspDcTestModule } from '../../../test.module';
import { PaymentGatewayMvComponent } from 'app/entities/payment-gateway-mv/payment-gateway-mv.component';
import { PaymentGatewayMvService } from 'app/entities/payment-gateway-mv/payment-gateway-mv.service';
import { PaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';

describe('Component Tests', () => {
    describe('PaymentGatewayMv Management Component', () => {
        let comp: PaymentGatewayMvComponent;
        let fixture: ComponentFixture<PaymentGatewayMvComponent>;
        let service: PaymentGatewayMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [PaymentGatewayMvComponent],
                providers: []
            })
                .overrideTemplate(PaymentGatewayMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PaymentGatewayMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentGatewayMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new PaymentGatewayMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.paymentGateways[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
