/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { PaymentGatewayMvDetailComponent } from 'app/entities/payment-gateway-mv/payment-gateway-mv-detail.component';
import { PaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';

describe('Component Tests', () => {
    describe('PaymentGatewayMv Management Detail Component', () => {
        let comp: PaymentGatewayMvDetailComponent;
        let fixture: ComponentFixture<PaymentGatewayMvDetailComponent>;
        const route = ({ data: of({ paymentGateway: new PaymentGatewayMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [PaymentGatewayMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PaymentGatewayMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PaymentGatewayMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.paymentGateway).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
