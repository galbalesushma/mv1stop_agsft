/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SspDcTestModule } from '../../../test.module';
import { PaymentGatewayMvUpdateComponent } from 'app/entities/payment-gateway-mv/payment-gateway-mv-update.component';
import { PaymentGatewayMvService } from 'app/entities/payment-gateway-mv/payment-gateway-mv.service';
import { PaymentGatewayMv } from 'app/shared/model/payment-gateway-mv.model';

describe('Component Tests', () => {
    describe('PaymentGatewayMv Management Update Component', () => {
        let comp: PaymentGatewayMvUpdateComponent;
        let fixture: ComponentFixture<PaymentGatewayMvUpdateComponent>;
        let service: PaymentGatewayMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SspDcTestModule],
                declarations: [PaymentGatewayMvUpdateComponent]
            })
                .overrideTemplate(PaymentGatewayMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PaymentGatewayMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentGatewayMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PaymentGatewayMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.paymentGateway = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PaymentGatewayMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.paymentGateway = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
