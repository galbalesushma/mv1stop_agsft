package com.mv1.dc.web.rest;

import com.mv1.dc.SspDcApp;

import com.mv1.dc.domain.Donation;
import com.mv1.dc.repository.DonationRepository;
import com.mv1.dc.service.DonationService;
import com.mv1.dc.service.dto.DonationDTO;
import com.mv1.dc.service.mapper.DonationMapper;
import com.mv1.dc.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mv1.dc.web.rest.TestUtil.sameInstant;
import static com.mv1.dc.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mv1.dc.domain.enumeration.PayStatus;
/**
 * Test class for the DonationResource REST controller.
 *
 * @see DonationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SspDcApp.class)
public class DonationResourceIntTest {

    private static final Long DEFAULT_AMOUNT = 1L;
    private static final Long UPDATED_AMOUNT = 2L;

    private static final String DEFAULT_TXN_ID = "AAAAAAAAAA";
    private static final String UPDATED_TXN_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ORDER_ID = "AAAAAAAAAA";
    private static final String UPDATED_ORDER_ID = "BBBBBBBBBB";

    private static final PayStatus DEFAULT_STATUS = PayStatus.INITIATED;
    private static final PayStatus UPDATED_STATUS = PayStatus.SUCCESS;

    private static final String DEFAULT_GATEWAY_RESP = "AAAAAAAAAA";
    private static final String UPDATED_GATEWAY_RESP = "BBBBBBBBBB";

    private static final String DEFAULT_DONEE = "AAAAAAAAAA";
    private static final String UPDATED_DONEE = "BBBBBBBBBB";

    private static final Long DEFAULT_SCHOOL_ID = 1L;
    private static final Long UPDATED_SCHOOL_ID = 2L;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    @Autowired
    private DonationRepository donationRepository;

    @Autowired
    private DonationMapper donationMapper;
    
    @Autowired
    private DonationService donationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDonationMockMvc;

    private Donation donation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DonationResource donationResource = new DonationResource(donationService);
        this.restDonationMockMvc = MockMvcBuilders.standaloneSetup(donationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Donation createEntity(EntityManager em) {
        Donation donation = new Donation()
            .amount(DEFAULT_AMOUNT)
            .txnId(DEFAULT_TXN_ID)
            .orderId(DEFAULT_ORDER_ID)
            .status(DEFAULT_STATUS)
            .gatewayResp(DEFAULT_GATEWAY_RESP)
            .donee(DEFAULT_DONEE)
            .schoolId(DEFAULT_SCHOOL_ID)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .isDeleted(DEFAULT_IS_DELETED);
        return donation;
    }

    @Before
    public void initTest() {
        donation = createEntity(em);
    }

    @Test
    @Transactional
    public void createDonation() throws Exception {
        int databaseSizeBeforeCreate = donationRepository.findAll().size();

        // Create the Donation
        DonationDTO donationDTO = donationMapper.toDto(donation);
        restDonationMockMvc.perform(post("/api/donations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donationDTO)))
            .andExpect(status().isCreated());

        // Validate the Donation in the database
        List<Donation> donationList = donationRepository.findAll();
        assertThat(donationList).hasSize(databaseSizeBeforeCreate + 1);
        Donation testDonation = donationList.get(donationList.size() - 1);
        assertThat(testDonation.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testDonation.getTxnId()).isEqualTo(DEFAULT_TXN_ID);
        assertThat(testDonation.getOrderId()).isEqualTo(DEFAULT_ORDER_ID);
        assertThat(testDonation.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDonation.getGatewayResp()).isEqualTo(DEFAULT_GATEWAY_RESP);
        assertThat(testDonation.getDonee()).isEqualTo(DEFAULT_DONEE);
        assertThat(testDonation.getSchoolId()).isEqualTo(DEFAULT_SCHOOL_ID);
        assertThat(testDonation.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testDonation.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testDonation.isIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createDonationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = donationRepository.findAll().size();

        // Create the Donation with an existing ID
        donation.setId(1L);
        DonationDTO donationDTO = donationMapper.toDto(donation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDonationMockMvc.perform(post("/api/donations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Donation in the database
        List<Donation> donationList = donationRepository.findAll();
        assertThat(donationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = donationRepository.findAll().size();
        // set the field null
        donation.setAmount(null);

        // Create the Donation, which fails.
        DonationDTO donationDTO = donationMapper.toDto(donation);

        restDonationMockMvc.perform(post("/api/donations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donationDTO)))
            .andExpect(status().isBadRequest());

        List<Donation> donationList = donationRepository.findAll();
        assertThat(donationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrderIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = donationRepository.findAll().size();
        // set the field null
        donation.setOrderId(null);

        // Create the Donation, which fails.
        DonationDTO donationDTO = donationMapper.toDto(donation);

        restDonationMockMvc.perform(post("/api/donations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donationDTO)))
            .andExpect(status().isBadRequest());

        List<Donation> donationList = donationRepository.findAll();
        assertThat(donationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = donationRepository.findAll().size();
        // set the field null
        donation.setStatus(null);

        // Create the Donation, which fails.
        DonationDTO donationDTO = donationMapper.toDto(donation);

        restDonationMockMvc.perform(post("/api/donations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donationDTO)))
            .andExpect(status().isBadRequest());

        List<Donation> donationList = donationRepository.findAll();
        assertThat(donationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDonations() throws Exception {
        // Initialize the database
        donationRepository.saveAndFlush(donation);

        // Get all the donationList
        restDonationMockMvc.perform(get("/api/donations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(donation.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].txnId").value(hasItem(DEFAULT_TXN_ID.toString())))
            .andExpect(jsonPath("$.[*].orderId").value(hasItem(DEFAULT_ORDER_ID.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].gatewayResp").value(hasItem(DEFAULT_GATEWAY_RESP.toString())))
            .andExpect(jsonPath("$.[*].donee").value(hasItem(DEFAULT_DONEE.toString())))
            .andExpect(jsonPath("$.[*].schoolId").value(hasItem(DEFAULT_SCHOOL_ID.intValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getDonation() throws Exception {
        // Initialize the database
        donationRepository.saveAndFlush(donation);

        // Get the donation
        restDonationMockMvc.perform(get("/api/donations/{id}", donation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(donation.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.intValue()))
            .andExpect(jsonPath("$.txnId").value(DEFAULT_TXN_ID.toString()))
            .andExpect(jsonPath("$.orderId").value(DEFAULT_ORDER_ID.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.gatewayResp").value(DEFAULT_GATEWAY_RESP.toString()))
            .andExpect(jsonPath("$.donee").value(DEFAULT_DONEE.toString()))
            .andExpect(jsonPath("$.schoolId").value(DEFAULT_SCHOOL_ID.intValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingDonation() throws Exception {
        // Get the donation
        restDonationMockMvc.perform(get("/api/donations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDonation() throws Exception {
        // Initialize the database
        donationRepository.saveAndFlush(donation);

        int databaseSizeBeforeUpdate = donationRepository.findAll().size();

        // Update the donation
        Donation updatedDonation = donationRepository.findById(donation.getId()).get();
        // Disconnect from session so that the updates on updatedDonation are not directly saved in db
        em.detach(updatedDonation);
        updatedDonation
            .amount(UPDATED_AMOUNT)
            .txnId(UPDATED_TXN_ID)
            .orderId(UPDATED_ORDER_ID)
            .status(UPDATED_STATUS)
            .gatewayResp(UPDATED_GATEWAY_RESP)
            .donee(UPDATED_DONEE)
            .schoolId(UPDATED_SCHOOL_ID)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .isDeleted(UPDATED_IS_DELETED);
        DonationDTO donationDTO = donationMapper.toDto(updatedDonation);

        restDonationMockMvc.perform(put("/api/donations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donationDTO)))
            .andExpect(status().isOk());

        // Validate the Donation in the database
        List<Donation> donationList = donationRepository.findAll();
        assertThat(donationList).hasSize(databaseSizeBeforeUpdate);
        Donation testDonation = donationList.get(donationList.size() - 1);
        assertThat(testDonation.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testDonation.getTxnId()).isEqualTo(UPDATED_TXN_ID);
        assertThat(testDonation.getOrderId()).isEqualTo(UPDATED_ORDER_ID);
        assertThat(testDonation.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDonation.getGatewayResp()).isEqualTo(UPDATED_GATEWAY_RESP);
        assertThat(testDonation.getDonee()).isEqualTo(UPDATED_DONEE);
        assertThat(testDonation.getSchoolId()).isEqualTo(UPDATED_SCHOOL_ID);
        assertThat(testDonation.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDonation.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testDonation.isIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingDonation() throws Exception {
        int databaseSizeBeforeUpdate = donationRepository.findAll().size();

        // Create the Donation
        DonationDTO donationDTO = donationMapper.toDto(donation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDonationMockMvc.perform(put("/api/donations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(donationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Donation in the database
        List<Donation> donationList = donationRepository.findAll();
        assertThat(donationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDonation() throws Exception {
        // Initialize the database
        donationRepository.saveAndFlush(donation);

        int databaseSizeBeforeDelete = donationRepository.findAll().size();

        // Get the donation
        restDonationMockMvc.perform(delete("/api/donations/{id}", donation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Donation> donationList = donationRepository.findAll();
        assertThat(donationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Donation.class);
        Donation donation1 = new Donation();
        donation1.setId(1L);
        Donation donation2 = new Donation();
        donation2.setId(donation1.getId());
        assertThat(donation1).isEqualTo(donation2);
        donation2.setId(2L);
        assertThat(donation1).isNotEqualTo(donation2);
        donation1.setId(null);
        assertThat(donation1).isNotEqualTo(donation2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DonationDTO.class);
        DonationDTO donationDTO1 = new DonationDTO();
        donationDTO1.setId(1L);
        DonationDTO donationDTO2 = new DonationDTO();
        assertThat(donationDTO1).isNotEqualTo(donationDTO2);
        donationDTO2.setId(donationDTO1.getId());
        assertThat(donationDTO1).isEqualTo(donationDTO2);
        donationDTO2.setId(2L);
        assertThat(donationDTO1).isNotEqualTo(donationDTO2);
        donationDTO1.setId(null);
        assertThat(donationDTO1).isNotEqualTo(donationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(donationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(donationMapper.fromId(null)).isNull();
    }
}
