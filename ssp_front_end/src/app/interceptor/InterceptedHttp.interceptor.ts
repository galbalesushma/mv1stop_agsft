import { Injectable } from "@angular/core";
import { ConnectionBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers } from "@angular/http";
import { Observable } from "rxjs/Rx";
import { environment } from "../../environments/environment";
import { Router } from '@angular/router';

@Injectable()
export class InterceptedHttp extends Http {

    noHttpResource = "No HTTP resource was found";
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, private _router: Router) {
        super(backend, defaultOptions);
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return super.request(url, options);
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        // if(url.startsWith("/")){
        url = this.updateUrl(url);
        return this.intercept(super.get(url, this.getRequestOptionArgs(options)));
        // }
        // else{
        //  return super.get(url);
        //  }
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        return this.intercept(super.post(url, body, this.getRequestOptionArgs(options)));
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        return this.intercept(super.put(url, body, this.getRequestOptionArgs(options)));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        return this.intercept(super.delete(url, this.getRequestOptionArgs(options)));
    }

    private updateUrl(req: string) {
        if (req.startsWith('h'))
            return req;
        if (req === "login")
            return environment.apiUrl;
        return environment.apiUrl + req;
        // return req;
    }

    private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        //options.headers.append('Content-Type', 'application/json');
  //      options.headers.append('Cache-Control', 'no-cache');
//        options.headers.append('Authorization', Cookie.get("lgnTkn"));

        return options;
    }

    intercept(observable: Observable<Response>): Observable<Response> {
        return observable.catch((err, source) => {

            switch (err.status) {
              //  case 401:
               // case 403:
                   // Cookie.deleteAll('/');
                    //localStorage.clear();
                 //   this._router.navigate(['login']);
                  //  return Observable.throw(err);
                case 0:
                case 500:
                case 501:
                case 503:
                    //console.log(err.status);
                    //console.log(err);
                   // this._router.navigate(['/error']);
                    return Observable.throw(err);
            /*    case 404:
                    let body;
                    if (err._body)
                        body = JSON.parse(err._body);
                    if (body != null) {
                        if (body.Message.indexOf(this.noHttpResource) !== -1) {
                            this._router.navigate(['error/' + err.status]);
                            return

                        }
                        else {
                            return Observable.throw(err);

                        }
                    }
                    else {
                        return Observable.throw(err);

                    }*/


                default:
                    return Observable.throw(err);
            }
        });

    }

}
