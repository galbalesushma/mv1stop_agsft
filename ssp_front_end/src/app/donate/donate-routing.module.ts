import { DonateComponent } from './donate.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo:'give',
        pathMatch:'full'
    },
    {
        path: 'give',
        component: DonateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DonateRoutingModule { }
