import { environment } from './../../environments/environment';
import { School } from './../common/model/school';
import { City } from './../common/model/city';
import { State } from './../common/model/state';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from './../reducers';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { District } from '../common/model/district';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.css'],

})
export class DonateComponent implements OnInit, OnDestroy {
  baseUrl = environment.apiUrl;
  stateUrl: string = this.baseUrl + 'search/states/state:';
  stateSearchField: string = "state";
  stateTitle: string = "States";

  countyUrl: string = this.baseUrl + 'search/districts/district:';
  countySearchField: string = "district";
  countyTitle: string = "County/District";

  cityUrl: string = this.baseUrl + 'search/cities/city:';
  citySearchField: string = "city";
  cityTitle: string = "City";

  schoolUrl: string = this.baseUrl + 'search/query/name=';
  schoolSearchField: string = "schoolName";
  schoolTitle: string = "Search school";

  message = 'Local area schools are selected for your support by default. You may optionally change it by selecting the filters below';

  showBusiness: boolean = false;

  selectedState: State = new State;
  selectedDistrict: District = new District;
  selectedCity: City = new City;
  selectedSchool: School = new School;


  constructor(
    private router: Router,
    private _store: Store<fromRoot.State>,
    private titlecasePipe: TitleCasePipe) {
  }

  ngOnInit() {
    let isOnInit = true;
  }


  redirectToSSP() {
    window.location.href = 'https://web.goigi.me/schoolsafetyprogram/';
  }

  redirectToMV1() {
    window.location.href = 'https://www.mv1stop.com/';
  }

  changeState(state) {
    this.selectedState = state;
    this.changeMessage();
    if (state) {
     this.countyUrl = this.baseUrl + 'search/districts/state_code:' + state.state_code + ' AND district:'
     this.cityUrl = this.baseUrl + 'search/cities/state_code:' + state.state_code + ' AND city:'
     this.schoolUrl = this.baseUrl + 'search/query/state=' + state.state_code + '&name='
    }
  }


  changeDistrict(district) {
    this.selectedDistrict = district;
    this.changeMessage();
    if (district) {
       if (this.selectedState) {
         this.cityUrl = this.baseUrl + 'search/cities/state_code:'+this.selectedState.state_code +' AND district:' +district.district + ' AND city:'
       }
       else {
         this.cityUrl = this.baseUrl + 'search/cities/district:' + district.district + ' AND city:'
       }
      this.schoolUrl = this.baseUrl + 'search/query/county=' + district.district + '&name='
    }
  }

  changeCity(city) {
    this.selectedCity = city;
    this.changeMessage();
    if (city){
      let ct = city.city ? city.city.trim(): city.city;
      this.schoolUrl = this.baseUrl + 'search/query/city=' + ct + '&name='
    }
  }

  changeSchool(school) {
    this.selectedSchool = school;
    this.changeMessage();
  }

  changeMessage() {
    if (this.selectedSchool && this.selectedSchool.id) {
      if (this.selectedCity && this.selectedCity.id) {
        this.message = this.transformTitleCase(this.selectedSchool.schoolName) + ' from ' + this.transformTitleCase(this.selectedCity.city) + ' city has been selected for your support';
      }
      else if (this.selectedDistrict && this.selectedDistrict.id) {
        this.message = this.transformTitleCase(this.selectedSchool.schoolName) + ' from ' + this.transformTitleCase(this.selectedDistrict.district) + ' county has been selected for your support';
      }
      else if (this.selectedState && this.selectedState.id) {
        this.message = this.transformTitleCase(this.selectedSchool.schoolName) + ' from ' + this.transformTitleCase(this.selectedState.state) + ' state has been selected for your support';
      }
      else {
        this.message = this.transformTitleCase(this.selectedSchool.schoolName) + ' has been selected for your support';
      }
    }
    else if (this.selectedCity && this.selectedCity.id) {
      this.message = this.transformTitleCase(this.selectedCity.city) + ' city schools are selected for your support';
    }
    else if (this.selectedDistrict && this.selectedDistrict.id) {
      this.message = this.transformTitleCase(this.selectedDistrict.district) + ' county schools are selected for your support';
    }
    else if (this.selectedState && this.selectedState.id) {
      this.message = this.transformTitleCase(this.selectedState.state) + ' state schools are selected for your support';
    }
  }

  transformTitleCase(name) {
    if (name)
      return this.titlecasePipe.transform(name);
    else
      return name;
  }

  donate() {
    if (this.selectedSchool && this.selectedSchool.id) {
      this.router.navigate(['/payment', { type: 'school', name: this.transformTitleCase(this.selectedSchool.schoolName), id: this.selectedSchool.id }]);
    }
    else if (this.selectedCity && this.selectedCity.id) {
      this.router.navigate(['/payment', { type: 'city', name: this.transformTitleCase(this.selectedCity.city), id: this.selectedCity.id }]);
    }
    else if (this.selectedDistrict && this.selectedDistrict.id) {
      this.router.navigate(['/payment', { type: 'district', name: this.transformTitleCase(this.selectedDistrict.district), id: this.selectedDistrict.id }]);
    }
    else if (this.selectedState && this.selectedState.id) {
      this.router.navigate(['/payment', { type: 'state', name: this.transformTitleCase(this.selectedState.state), id: this.selectedState.id }]);
    }
    else {
      this.router.navigate(['/payment']);
    }
  }

  ngOnDestroy() {
  }
}
