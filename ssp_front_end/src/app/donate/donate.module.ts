import { AutoCompleteModule } from './../common/modules/autocomplete/autocomplete.module';
import { LocationsAndSchoolsService } from './../common/services/locations-and-schools.service';
import { DonateRoutingModule } from './donate-routing.module';
import { DonateEffects } from './donate.effects';
import { DonateService } from './donate.service';
import { DonateComponent } from './donate.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HttpModule } from '@angular/http';

@NgModule({
    imports: [
        AutoCompleteModule,
        DonateRoutingModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
        EffectsModule.forFeature([DonateEffects]),
    ],
    declarations: [DonateComponent],
    exports: [DonateComponent],
    providers: [DonateService, LocationsAndSchoolsService, TitleCasePipe]
})
export class DonateModule {

}
