import * as fromSearchSchools from './common/reducers/search.reducer';
import * as fromStates from './common/reducers/list-states.reducer';
import * as fromDistricts from './common/reducers/list-districts.reducer';
import * as fromCities from './common/reducers/list-cities.reducer';
import * as fromBusinessList from './businesses/reducers/list-business.reducer';
import * as fromCompleteDonation from './payment/reducers/complete-donation.reducer';
import * as fromChangeName from './payment-success/reducers/change-name.reducer';
import * as fromListUserDetails from './payment/reducers/list-user-details.reducer';
import * as fromCardDetails from './payment-success/reducers/card-details.reducer';
import * as fromAddBusiness from './add-business/reducers/add-business.reducer';
import * as fromListCategories from './common/reducers/list-business-categories.reducer';
import * as fromListDonations from './admin/dashboard/reducers/list-donations.reducer';

export interface State {
    searchResult: fromSearchSchools.State;
    statesResult: fromStates.State;
    districtsResult: fromDistricts.State;
    citiesResult: fromCities.State;
    businessList: fromBusinessList.State;
    completeDonation: fromCompleteDonation.State;
    changeName: fromChangeName.State;
    userDetails: fromListUserDetails.State;
    cardDetails: fromCardDetails.State;
    addBusiness: fromAddBusiness.State;
    businessCategories:fromListCategories.State;
    donationsList:fromListDonations.State;
}

export const reducers = {
    searchResult: fromSearchSchools.reducer,
    statesResult: fromStates.reducer,
    districtsResult: fromDistricts.reducer,
    citiesResult: fromCities.reducer,
    businessList: fromBusinessList.reducer,
    completeDonation: fromCompleteDonation.reducer,
    changeName: fromChangeName.reducer,
    userDetails: fromListUserDetails.reducer,
    cardDetails: fromCardDetails.reducer,
    addBusiness: fromAddBusiness.reducer,
    businessCategories:fromListCategories.reducer,
    donationsList:fromListDonations.reducer,
};

export function selectSearchSuccess(state: State) {
    return state.searchResult.result;
}

export function selectSearchFailure(state: State) {
    return state.searchResult.err;
}

export function selectListStatesSuccess(state: State) {
    return state.statesResult.result;
}

export function selectListStatesFailure(state: State) {
    return state.statesResult.err;
}


export function selectListDistrictsSuccess(state: State) {
    return state.districtsResult.result;
}

export function selectListDistrictsFailure(state: State) {
    return state.districtsResult.err;
}

export function selectListCitiesSuccess(state: State) {
    return state.citiesResult.result;
}

export function selectListCitiesFailure(state: State) {
    return state.citiesResult.err;
}

export function selectListBusinessSuccess(state: State) {
    return state.businessList.result;
}

export function selectListBusinessFailure(state: State) {
    return state.businessList.err;
}

export function selectCompleteDonationSuccess(state: State) {
    return state.completeDonation.result;
}

export function selectCompleteDonationFailure(state: State) {
    return state.completeDonation.err;
}

export function selectChangeNameSuccess(state: State) {
    return state.changeName.result;
}

export function selectChangeNameFailure(state: State) {
    return state.changeName.err;
}

export function selectListUserDetailsSuccess(state: State) {
    return state.userDetails.result;
}

export function selectListUserDetailsFailure(state: State) {
    return state.userDetails.err;
}

export function selectListCardDetailsSuccess(state: State) {
    return state.cardDetails.result;
}

export function selectListCardDetailsFailure(state: State) {
    return state.cardDetails.err;
}

export function selectAddBusinessSuccess(state: State) {
    return state.addBusiness.result;
}

export function selectAddBusinessFailure(state: State) {
    return state.addBusiness.err;
}

export function selectListBusinessCategoriesSuccess(state: State) {
    return state.businessCategories.result;
}

export function selectListBusinessCategoriesFailure(state: State) {
    return state.businessCategories.err;
}

export function selectListDonationsSuccess(state: State) {
    return state.donationsList.result;
}

export function selectListDonationsFailure(state: State) {
    return state.donationsList.err;
}