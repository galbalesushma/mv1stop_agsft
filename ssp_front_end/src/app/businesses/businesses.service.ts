import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions, RequestOptionsArgs, Headers, Response } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable()
export class BusinessesService {
    baseUrl = environment.apiUrl;

    private _getBusinessesUrl = "business/short-list/{category}/{page}";

    constructor(private _http: Http) {
    }

    getBusinesses(category,pageId:number) {
        return this._http.get(this._getBusinessesUrl.replace('{category}',category).replace('{page}',pageId+"")).map((res: Response) => res.json());
    }
}



