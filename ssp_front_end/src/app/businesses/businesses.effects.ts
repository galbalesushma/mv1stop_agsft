import { BusinessesService } from './businesses.service';
import { map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import * as ListBusinessActions from './actions/list-business.actions';
import 'rxjs/add/operator/map'

@Injectable()
export class BusinessesEffects {

    constructor(
        private actions$: Actions,
        private businessesService: BusinessesService,
    ) { }


    @Effect()
    listBusinesses$: Observable<Action> = this.actions$.ofType<ListBusinessActions.ListBusinesses>(ListBusinessActions.LIST_BUSINESSES)
        .map(action => action)
        .switchMap(
        payload => this.businessesService.getBusinesses(payload.category,payload.page)
            .map(results => new ListBusinessActions.ListBusinessesSuccess(results))
            .catch(err =>
                Observable.of({ type: ListBusinessActions.LIST_BUSINESSES_FAILURE, err: err })
            ));



}
