import { Category } from './../common/model/category';
import { Business } from './../add-business/model/business';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as fromRoot from './../reducers';
import * as ListBusinessActions from './actions/list-business.actions';
import * as Constants from './../common/constants/constants';
import * as ListCategoriesActions from './../common/actions/list-business-categories.actions';

@Component({
  selector: 'app-businesses',
  templateUrl: './businesses.component.html',
  styleUrls: ['./businesses.component.css']
})
export class BusinessesComponent implements OnInit, OnDestroy {

  categoriesList: Category[] = [];
  businessList: Business[] = [];
  pageCount: number;

  defaultCategory:any = { id: 0, name: 'ALL' }
  selectedCategory: Category = new Category();

  obsBusinesses: Observable<any>;
  subBusinesses: Subscription;
  obsBusinessesErr: Observable<any>;
  subBusinessesErr: Subscription;

  obsCategories: Observable<any>;
  subCategories: Subscription;
  obsCategoriesErr: Observable<any>;
  subCategoriesErr: Subscription;

  constructor(private _store: Store<fromRoot.State>) {
    this.obsBusinesses = this._store.select(fromRoot.selectListBusinessSuccess);
    this.obsBusinessesErr = this._store.select(fromRoot.selectListBusinessFailure);
    this.obsCategories = this._store.select(fromRoot.selectListBusinessCategoriesSuccess);
    this.obsCategoriesErr = this._store.select(fromRoot.selectListBusinessCategoriesFailure);

  }

  ngOnInit() {
    let isOnInit = true;
    if (!this.categoriesList || this.categoriesList.length == 0) {
      this._store.dispatch(new ListCategoriesActions.ListCategories);
    }
    this.selectedCategory = this.defaultCategory;
    this.getBusinesses();

    this.subBusinesses = this.obsBusinesses.subscribe(res => {
      if (res && !isOnInit) {
        if (res.categories) {
          let category = res.categories[0];
          if (category) {
            this.businessList = category.items;
            this.pageCount = category.totalCount / 10;
            this.pageCount = Math.ceil(this.pageCount);
            if (this.pageCount == 0) {
              this.pageCount = 1;
            }
          }
          else {
            this.businessList = [];
            this.pageCount = 0;
          }
        }
      }
    })

    this.subBusinessesErr = this.obsBusinessesErr.subscribe(err => {
      if (err && !isOnInit) {
        this.businessList = [];
      }
    })

    this.subCategories = this.obsCategories.subscribe(res => {
      if (res) {
        this.categoriesList = res;
      }
    })

    this.subCategoriesErr = this.obsCategoriesErr.subscribe(err => {
      if (err) {
        this.categoriesList = [];
      }
    })
    isOnInit = false;
  }

  getBusinesses() {
    if (this.selectedCategory) {
      this.selectedCategory.pageId = 1;
      this._store.dispatch(new ListBusinessActions.ListBusinesses(this.selectedCategory.name, this.selectedCategory.pageId));
    }
  }

  showNext() {
    if (this.selectedCategory) {
      if (this.pageCount > this.selectedCategory.pageId) {
        this.selectedCategory.pageId += 1;
        this._store.dispatch(new ListBusinessActions.ListBusinesses(this.selectedCategory.name, this.selectedCategory.pageId));
      }
    }
  }

  showPrev() {
    if (this.selectedCategory) {
      if (1 != this.selectedCategory.pageId) {
        this.selectedCategory.pageId -= 1;
        this._store.dispatch(new ListBusinessActions.ListBusinesses(this.selectedCategory.name, this.selectedCategory.pageId));
      }
    }
  }

  getSelectedPage(page) {
    this.selectedCategory.pageId = +page;
    this._store.dispatch(new ListBusinessActions.ListBusinesses(this.selectedCategory.name, this.selectedCategory.pageId));
  }

  createPageRange() {
    let pages = [];
    let start = 1;
    let end = 9 < this.pageCount ? 12 : this.pageCount;
    if (this.selectedCategory && this.selectedCategory.pageId) {
      let currentPage = this.selectedCategory.pageId;
      currentPage = currentPage - 4;
      if (currentPage > 1) {
        start = currentPage;
        end = start + 9;
        if (end < this.pageCount) {
          for (let i = start; i < end; i++)
            pages.push(i);
          return pages;
        }
        else {
          end = this.pageCount;
          start = this.pageCount - 9;
        }
      }
      for (let i = start; i <= end; i++)
        pages.push(i);
    }
    return pages;
  }

  getIcon(category: string) {
    let icon = Constants[category]
    if (icon) {
      return icon;
    }
    else {
      return 'tag';
    }
  }
  ngOnDestroy() {
    if (this.subBusinesses)
      this.subBusinesses.unsubscribe();
    if (this.subBusinessesErr)
      this.subBusinessesErr.unsubscribe();
    if (this.subCategories)
      this.subCategories.unsubscribe();
    if (this.subCategoriesErr)
      this.subCategoriesErr.unsubscribe();
  }

}
