import { BusinessesService } from './businesses.service';
import { BusinessesComponent } from './businesses.component';
import { BusinessesEffects } from './businesses.effects';
import { BusinessesRoutingModule } from './businesses-routing.module';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
    imports: [
        BusinessesRoutingModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
        EffectsModule.forFeature([BusinessesEffects]),
    ],
    declarations: [BusinessesComponent],
    exports: [BusinessesComponent],
    providers: [BusinessesService]
})
export class BusinessesModule {

}
