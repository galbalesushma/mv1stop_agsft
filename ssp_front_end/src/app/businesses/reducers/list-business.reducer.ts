import * as ListBusinessesActions from '../actions/list-business.actions';

export interface State {    
   
    result: any[],
    err: any
}

const initialState: State = {    
    result:[],
    err: {}
};

export function reducer(state = initialState, action: ListBusinessesActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListBusinessesActions.LIST_BUSINESSES_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case ListBusinessesActions.LIST_BUSINESSES_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
      
        default: {
            return state;
        }

    }

}
