
export const LIST_BUSINESSES = 'LIST_BUSINESSES';
export const LIST_BUSINESSES_SUCCESS = 'LIST_BUSINESSES_SUCCESS';
export const LIST_BUSINESSES_FAILURE = 'LIST_BUSINESSES_FAILURE';

export class ListBusinesses {
    readonly type = LIST_BUSINESSES;
    constructor(public category:string,public page:number) { }
}

export class ListBusinessesSuccess {
    readonly type = LIST_BUSINESSES_SUCCESS;
    constructor(public res: any) { }
}

export class ListBusinessesFailure {
    readonly type = LIST_BUSINESSES_FAILURE;
    constructor(public err: any) { }
}


export type All = ListBusinesses | ListBusinessesSuccess | ListBusinessesFailure 