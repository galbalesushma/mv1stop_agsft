import { AlphabetsSpaceDot } from './alphabet-space-dot.directive';
import { OnlyNumber } from './only-numbers.directive';

import { NgModule } from '@angular/core';


@NgModule({
    imports: [

    ],
    declarations: [OnlyNumber, AlphabetsSpaceDot],
    exports: [OnlyNumber, AlphabetsSpaceDot],
    providers: []
})
export class DirectivesModule {

}
