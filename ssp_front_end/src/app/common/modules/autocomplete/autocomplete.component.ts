import { Component, OnInit, Input, OnChanges, EventEmitter, Output, ViewChild } from '@angular/core';
import { CompleterService, CompleterData, CompleterCmp } from 'ng2-completer';
import { SimpleChanges } from '@angular/core';

@Component({
  selector: 'autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})

export class AutocompleteComponent implements OnInit, OnChanges {
  @Input('url') url: string;
  @Input('searchField') searchField: string;
  @Input('title') title: string;
  @Input('dropdown') dropdownCss: string;
  @Output('selected') selected: EventEmitter<any> = new EventEmitter();
  @ViewChild('autocomplete') public autocomplete: CompleterCmp;

  public dataService: CompleterData;
  isOpen: boolean;
  //searchStr: string;

  constructor(private completerService: CompleterService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['url']) {
      this.url = changes['url'].currentValue;
    }
    if (changes['searchField']) {
      this.searchField = changes['searchField'].currentValue;
    }
    if (changes['title']) {
      this.title = changes['title'].currentValue;
    }
    this.dataService = this.completerService.remote(this.url, this.searchField, this.searchField);
  }

  onToggle(value) {
    this.isOpen = value;
  }

  /*toLowerCase() {
    if (this.searchStr)
      this.searchStr = this.searchStr.toLowerCase();
  }*/

  showDropdown() {
    this.autocomplete.open();
    this.autocomplete.focus();
  }

  itemSelected(item) {
    if (item) {
      this.selected.emit(item.originalObject);
    }
  }
}
