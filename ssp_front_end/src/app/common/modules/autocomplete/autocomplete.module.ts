import { AutocompleteComponent } from './autocomplete.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HttpModule } from '@angular/http';
import { Ng2CompleterModule } from "ng2-completer";


@NgModule({
    imports: [
        Ng2CompleterModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
    ],
    declarations: [AutocompleteComponent],
    exports: [AutocompleteComponent],
    providers: []
})
export class AutoCompleteModule {

}
