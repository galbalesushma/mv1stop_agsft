import { Router, NavigationEnd } from '@angular/router';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import * as fromRoot from './../../reducers';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  showBackButton: boolean = true;
  showDiscountsButton: boolean = true;
  imageUrl = 'assets/img/mv1-logo.png';
  logoWidth = 60;
  logoHeight = 60;
  showSponseredTitle:boolean = true;
  constructor(private _store: Store<fromRoot.State>, private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.router.url.includes('/payment')) {
          this.showBackButton = false;
          this.showDiscountsButton = false;
        } else {
          this.showBackButton = true;
          this.showDiscountsButton = true;
        }

        if (this.router.url.includes('/payment-success') || this.router.url.includes('/businesses')) {
          this.showBackButton = false;
          this.showDiscountsButton = true;
          this.imageUrl = "assets/img/ssp-logo.png";
          this.logoHeight = 50;
          this.logoWidth = 150;
          this.showSponseredTitle = false;
        }
        else {
          this.logoWidth = 60;
          this.logoHeight = 60;
          this.showSponseredTitle = true;
          this.imageUrl = "assets/img/mv1-logo.png";
        }
      }
    })
  }

  getActiveButton(button: string) {
    if (this.router.url.includes(button)) {
      return 'btn-selected';
    }
  }
}
