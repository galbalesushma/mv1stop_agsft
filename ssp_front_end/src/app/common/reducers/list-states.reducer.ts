import * as ListStatesActions from '../actions/list-states.actions';

export interface State {    
   
    result: any[],
    err: any
}

const initialState: State = {    
    result:[],
    err: {}
};

export function reducer(state = initialState, action: ListStatesActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListStatesActions.LIST_STATES_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case ListStatesActions.LIST_STATES_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
          
        default: {
            return state;
        }

    }

}
