import * as ShowHideBusinessesActions from '../actions/show-business.actions';

export interface State {

    status: boolean,
}

const initialState: State = {
    status: false,
};

export function reducer(state = initialState, action: ShowHideBusinessesActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {

        case ShowHideBusinessesActions.SHOW_BUSINESSES: {
            return {
                ...state,
                status: true
            }
        }

        case ShowHideBusinessesActions.HIDE_BUSINESSES: {
            return {
                ...state,
                status: false
            }
        }

        default: {
            return state;
        }

    }

}
