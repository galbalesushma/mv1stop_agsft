import * as ListDistrictsActions from '../actions/list-districts.actions';

export interface State {    
   
    result: any[],
    err: any
}

const initialState: State = {    
    result:[],
    err: {}
};

export function reducer(state = initialState, action: ListDistrictsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListDistrictsActions.LIST_DISTRICTS_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case ListDistrictsActions.LIST_DISTRICTS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
          
        default: {
            return state;
        }

    }

}
