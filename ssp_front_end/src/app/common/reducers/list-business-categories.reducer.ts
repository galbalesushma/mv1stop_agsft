import * as ListBusinessCategoriesActions from '../actions/list-business-categories.actions';

export interface State {    
   
    result: any[],
    err: any
}

const initialState: State = {    
    result:[],
    err: {}
};

export function reducer(state = initialState, action: ListBusinessCategoriesActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListBusinessCategoriesActions.LIST_CATEGORIES_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case ListBusinessCategoriesActions.LIST_CATEGORIES_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
      
        default: {
            return state;
        }

    }

}
