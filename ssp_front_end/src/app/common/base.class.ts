import { OnInit } from "@angular/core";

import * as validationMessages from './validation-messages';
import * as validationLength from './metadata';

export abstract class BaseClass {
    // messages for payment page
    public firstNameRequiredMessage: string;
    public lastNameRequiredMessage: string;
    public cardNumberRequiredMessage:string;
    public invalidCardNumberMessage:string;
    public validTillMonthRequiredMessage:string;
    public validTillYearRequiredMessage:string;
    public cvvRequiredMessage: string;
    public invalidCVVMessage:string;
    public addressRequiredMessage: string;
    public zipRequiredMessage: string;
    public cityRequiredMessage: string;
    public stateRequiredMessage: string;
    public countryRequiredMessage: string;
    public phoneRequiredMessage: string;
    public invalidMobileNumberMessage:string;
    public invalidEmailMessage:string;
    public invalidZipMessage:string;
    public businessNameRequiredMsg:string;
    public businessTypeRequiredMsg:string;
    public discountRequiredMsg:string;

    protected constructor() {
        this.assignMessages();
    }


    assignMessages() {
        // messages for payment page
        this.firstNameRequiredMessage = validationMessages.FIRST_NAME_REQUIRED_MESSAGE;
        this.lastNameRequiredMessage = validationMessages.LAST_NAME_REQUIRED_MESSAGE;
        this.cardNumberRequiredMessage = validationMessages.CARD_NUMBER_REQUIRED_MESSAGE;
        this.invalidCardNumberMessage = validationMessages.INVALID_CARD_NUMBER_MESSAGE;
        this.validTillMonthRequiredMessage = validationMessages.VALID_TILL_MONTH_REQUIRED_MESSAGE;
        this.validTillYearRequiredMessage = validationMessages.VALID_TILL_YEAR_REQUIRED_MESSAGE;
        this.cvvRequiredMessage = validationMessages.CVV_REQUIRED_MESSAGE;
        this.invalidCVVMessage = validationMessages.INVALID_CVV_MESSAGE;
        this.addressRequiredMessage = validationMessages.ADDRESS_REQUIRED_MESSAGE;
        this.zipRequiredMessage = validationMessages.ZIP_REQUIRED_MESSAGE;
        this.invalidZipMessage = validationMessages.INVALID_ZIP_MESSAGE;
        this.cityRequiredMessage = validationMessages.CITY_REQUIRED_MESSAGE;
        this.stateRequiredMessage = validationMessages.STATE_REQUIRED_MESSAGE;
        this.countryRequiredMessage = validationMessages.COUNTRY_REQUIRED_MESSAGE;
        this.phoneRequiredMessage = validationMessages.PHONE_REQUIRED_MESSAGE;
        this.invalidEmailMessage = validationMessages.INVALID_EMAIL_MESSAGE;
        this.invalidMobileNumberMessage = validationMessages.INVALID_MOBILE_MESSAGE;
        this.businessNameRequiredMsg = validationMessages.BUSINESS_NAME_REQUIRED_MESSAGE;
        this.businessTypeRequiredMsg = validationMessages.BUSINESS_TYPE_REQUIRED_MESSAGE;
        this.discountRequiredMsg = validationMessages.DISCOUNT_REQUIRED_MESSAGE;
    }

}
