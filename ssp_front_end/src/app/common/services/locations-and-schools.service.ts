import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions, RequestOptionsArgs, Headers, Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { HttpHeaders } from '@angular/common/http/src/headers';

@Injectable()
export class LocationsAndSchoolsService {
    baseUrl = environment.apiUrl;

    private _statesUrl = "search/states/{term}";    //"http://www.mocky.io/v2/5ba0bb6635000071005bb998";//baseUrl+"search/states/north";
    private _districtsUrl = "search/districts/{term}";   //"http://www.mocky.io/v2/5ba0bb883500006e005bb99a"//baseUrl+"search/states/north";
    private _citiesUrl = "search/cities/{term}";    //"http://www.mocky.io/v2/5ba0bbab3500005c005bb99e"//baseUrl+"search/states/north";
    private _searchSchoolUrl = "search/schools/{term}";

    constructor(private _http: Http) {
    }

    getStatesList(criteria) {
        let param = {
            "criteria": criteria
        }
        return this._http.get(this._statesUrl.replace('{term}', criteria)).map((res: Response) => res.json());
    }

    getDistrictsList(criteria) {
        let param = {
            "criteria": criteria
        }
        return this._http.get(this._districtsUrl.replace('{term}', criteria)).map((res: Response) => res.json());
    }

    getCitiesList(criteria) {
        let param = {
            "criteria": criteria
        }
        return this._http.get(this._citiesUrl.replace('{term}', criteria)).map((res: Response) => res.json());
    }

    searchSchools(criteria) {
        return this._http.get(this._searchSchoolUrl.replace('{term}', criteria)).map((res: Response) => res.json());
    }

}



