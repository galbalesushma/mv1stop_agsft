import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions, RequestOptionsArgs, Headers, Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { HttpHeaders } from '@angular/common/http/src/headers';

@Injectable()
export class CommonService {

    private _getCategoriesUrl = "categories";

    constructor(private _http: Http) {
    }

    getCategories() {
        return this._http.get(this._getCategoriesUrl).map((res: Response) => res.json());
    }

}



