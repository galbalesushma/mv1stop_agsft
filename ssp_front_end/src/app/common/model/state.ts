export class State{
    id:number;
    state:string;
    state_code:string;
    state_upper:string;
}