export class School {
        id : number;
        schoolName : string;
        state : string;
        district : string;
        city : string;
        street : string;
        principal : string;     
        phone : string;
        zip : number;
        itemName:string;
}