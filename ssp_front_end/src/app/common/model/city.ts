export class City {
    id:number;
    state_code:string;
    district:string;
    city:string;
}