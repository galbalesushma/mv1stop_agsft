import { CommonService } from './../services/common.service';
import { map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import 'rxjs/Rx';
import * as ListBusinessCategoriesActions from '../actions/list-business-categories.actions';

@Injectable()
export class CommonEffects {

    constructor(
        private actions$: Actions,
        private commonService: CommonService,
    ) { }


    @Effect()
    listBusinessesCategories$: Observable<Action> = this.actions$.ofType<ListBusinessCategoriesActions.ListCategories>(ListBusinessCategoriesActions.LIST_CATEGORIES)
        .map(action => action)
        .switchMap(
        payload => this.commonService.getCategories()
            .map(results => new ListBusinessCategoriesActions.ListCategoriesSuccess(results))
            .catch(err =>
                Observable.of({ type: ListBusinessCategoriesActions.LIST_CATEGORIES_FAILURE, err: err })
            ));
}
