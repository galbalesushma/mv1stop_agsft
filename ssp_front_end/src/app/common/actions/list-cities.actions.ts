
export const LIST_CITIES = 'LIST_CITIES';
export const LIST_CITIES_SUCCESS = 'LIST_CITIES_SUCCESS';
export const LIST_CITIES_FAILURE = 'LIST_CITIES_FAILURE';

export class ListCities {
    readonly type = LIST_CITIES;
    constructor(public criteria:any) { }
}

export class ListCitiesSuccess {
    readonly type = LIST_CITIES_SUCCESS;
    constructor(public res: any) { }
}

export class ListCitiesFailure {
    readonly type = LIST_CITIES_FAILURE;
    constructor(public err: any) { }
}


export type All = ListCities | ListCitiesSuccess | ListCitiesFailure 