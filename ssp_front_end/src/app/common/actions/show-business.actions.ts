
export const SHOW_BUSINESSES = 'SHOW_BUSINESSES';
export const HIDE_BUSINESSES = 'HIDE_BUSINESSES';

export class ShowBusinesses {
    readonly type = SHOW_BUSINESSES;
    constructor() { }
}

export class HideBusinesses {
    readonly type = HIDE_BUSINESSES;
    constructor() { }
}


export type All = ShowBusinesses | HideBusinesses 