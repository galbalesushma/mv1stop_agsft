
export const SEARCH = 'SEARCH';
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';
export const SEARCH_FAILURE = 'SEARCH_FAILURE';
export const CLEAR_SEARCH = 'CLEAR_SEARCH';

export class Search {
    readonly type = SEARCH;
    constructor(public criteria:any) { }
}

export class SearchSuccess {
    readonly type = SEARCH_SUCCESS;
    constructor(public res: any) { }
}

export class SearchFailure {
    readonly type = SEARCH_FAILURE;
    constructor(public err: any) { }
}


export class ClearSearch {
    readonly type = CLEAR_SEARCH;
    constructor() { }
}


export type All = Search | SearchSuccess | SearchFailure | ClearSearch