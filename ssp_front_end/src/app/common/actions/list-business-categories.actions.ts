
export const LIST_CATEGORIES = 'LIST_CATEGORIES';
export const LIST_CATEGORIES_SUCCESS = 'LIST_CATEGORIES_SUCCESS';
export const LIST_CATEGORIES_FAILURE = 'LIST_CATEGORIES_FAILURE';

export class ListCategories {
    readonly type = LIST_CATEGORIES;
    constructor() { }
}

export class ListCategoriesSuccess {
    readonly type = LIST_CATEGORIES_SUCCESS;
    constructor(public res: any) { }
}

export class ListCategoriesFailure {
    readonly type = LIST_CATEGORIES_FAILURE;
    constructor(public err: any) { }
}


export type All = ListCategories | ListCategoriesSuccess | ListCategoriesFailure 