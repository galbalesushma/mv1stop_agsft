import * as ListDonationsActions from '../actions/list-donations.actions';

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
    result:{},
    err: {}
};

export function reducer(state = initialState, action: ListDonationsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListDonationsActions.LIST_DONATIONS_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case ListDonationsActions.LIST_DONATIONS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
      
        default: {
            return state;
        }

    }

}
