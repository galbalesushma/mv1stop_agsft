import { DashboardService } from './dashboard.service';
import { DashboardEffects } from './dashboard.effects';
import { AutoCompleteModule } from './../../common/modules/autocomplete/autocomplete.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
        EffectsModule.forFeature([DashboardEffects]),
        DashboardRoutingModule,
        NgbModule,
        AutoCompleteModule,
    ],
    declarations: [DashboardComponent],
    exports: [DashboardComponent],
    providers: [DashboardService]
})
export class DashboardModule {

}
