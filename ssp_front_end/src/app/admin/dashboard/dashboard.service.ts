import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions, RequestOptionsArgs, Headers, Response } from '@angular/http';

@Injectable()
export class DashboardService {

    private _listDonationsUrl = "dashboard/donations";

    private _downloadDonationsListUrl = "dashboard/donations-dl"


    constructor(private _http: Http) {
    }

    getDonationsList(payload) {
        return this._http.post(this._listDonationsUrl, payload).map((res: Response) => res.json());
    }

    downloadDonationsList(payload) {
        return this._http.post(this._downloadDonationsListUrl, payload).map((res: Response) => res.json());
    }

}



