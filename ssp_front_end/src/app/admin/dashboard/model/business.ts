export class Business{
    profileId:number;
    name:string;
    type:string;
    street:string;
    city:string;
    state:string;
    zip:string;
    phone:string;
    site:string;
    discount:string;
    email:string;
    pageId:number;
}