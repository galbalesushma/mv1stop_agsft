export class Business {
    id: number;
    date: Date;
    amount: number;
    name: string;
    phone: string;
    email: string;
    street: string;
    city: string;
    state: string;
    school: string;
}