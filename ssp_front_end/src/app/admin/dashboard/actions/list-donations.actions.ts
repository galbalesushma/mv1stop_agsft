
export const LIST_DONATIONS = 'LIST_DONATIONS';
export const LIST_DONATIONS_SUCCESS = 'LIST_DONATIONS_SUCCESS';
export const LIST_DONATIONS_FAILURE = 'LIST_DONATIONS_FAILURE';

export class ListDonations {
    readonly type = LIST_DONATIONS;
    constructor(public payload:any) { }
}

export class ListDonationsSuccess {
    readonly type = LIST_DONATIONS_SUCCESS;
    constructor(public res: any) { }
}

export class ListDonationsFailure {
    readonly type = LIST_DONATIONS_FAILURE;
    constructor(public err: any) { }
}


export type All = ListDonations | ListDonationsSuccess | ListDonationsFailure 