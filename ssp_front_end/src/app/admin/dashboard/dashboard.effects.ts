import { DashboardService } from './dashboard.service';
import { map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import 'rxjs/Rx';
import * as ListDonationsActions from './actions/list-donations.actions';

@Injectable()
export class DashboardEffects {

    constructor(
        private actions$: Actions,
        private dashboardService: DashboardService,
    ) { }


    @Effect()
    listDonations$: Observable<Action> = this.actions$.ofType<ListDonationsActions.ListDonations>(ListDonationsActions.LIST_DONATIONS)
        .map(action => action)
        .switchMap(
        payload => this.dashboardService.getDonationsList(payload.payload)
            .map(results => new ListDonationsActions.ListDonationsSuccess(results))
            .catch(err =>
                Observable.of({ type: ListDonationsActions.LIST_DONATIONS_FAILURE, err: err })
            ));

}
