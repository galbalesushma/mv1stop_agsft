import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { School } from './../../common/model/school';
import { City } from './../../common/model/city';
import { State } from './../../common/model/state';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from './../../../environments/environment';
import * as fromRoot from './../../reducers';
import * as ListDonationsActions from './actions/list-donations.actions';
import * as ListBusinessCategories from '../../common/actions/list-business-categories.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit, OnDestroy {
  fromDate;
  toDate;
  baseUrl = environment.apiUrl;
  count: number;
  today: Date = new Date();
  invalidDates: boolean;
  validDates: boolean;
  businessStatus: boolean;
  businessType:string;
  businessName:string;
  
  mode = 'donations';
  categoriesList = [];
  businessList = [];
  
  maxDate = { year: this.today.getFullYear(), month: this.today.getMonth(), day: this.today.getDay() };
  stateUrl: string = this.baseUrl + 'search/states/state:';
  stateSearchField: string = "state";
  stateTitle: string = "State";

  countyUrl: string = this.baseUrl + 'search/districts/district:';
  countySearchField: string = "district";
  countyTitle: string = "County/District";

  cityUrl: string = this.baseUrl + 'search/cities/city:';
  citySearchField: string = "city";
  cityTitle: string = "City";

  schoolUrl: string = this.baseUrl + 'search/query/name=';
  schoolSearchField: string = "schoolName";
  schoolTitle: string = "School";

  selectedState: State = new State;
  selectedCity: City = new City;
  selectedSchool: School = new School;

  donationsList = [];

  obsDonations: Observable<any>;
  subDonations: Subscription;
  obsDonationsErr: Observable<any>;
  subDonationsErr: Subscription;

  obsCategories: Observable<any>;
  subCategories: Subscription;
  obsCategoriesErr: Observable<any>;
  subCategoriesErr: Subscription;

  constructor(private _store: Store<fromRoot.State>) {
    this.obsDonations = this._store.select(fromRoot.selectListDonationsSuccess);
    this.obsDonationsErr = this._store.select(fromRoot.selectListDonationsFailure);
    this.obsCategories = this._store.select(fromRoot.selectListBusinessCategoriesSuccess);
    this.obsCategoriesErr = this._store.select(fromRoot.selectListBusinessCategoriesFailure);
  }

  ngOnInit() {
    let isOnInit = true;
    if (!this.categoriesList || this.categoriesList.length == 0)
      this._store.dispatch(new ListBusinessCategories.ListCategories);
    this.subDonations = this.obsDonations.subscribe(res => {
      if (res && !isOnInit) {
        this.donationsList = res.result;
        this.count = res.total;
      }
    })

    this.subCategories = this.obsCategories.subscribe(res => {
      if (res) {
        this.categoriesList = res;
      }
    })

    this.subCategoriesErr = this.obsCategoriesErr.subscribe(err => {
      if (err) {

      }
    })

    isOnInit = false;
  }

  changeState(state) {
    this.selectedState = state;
    if (state) {
      this.countyUrl = this.baseUrl + 'search/districts/state_code:' + state.state_code + ' AND district:'
      this.cityUrl = this.baseUrl + 'search/cities/state_code:' + state.state_code + ' AND city:'
      this.schoolUrl = this.baseUrl + 'search/query/state=' + state.state_code + '&name='
    }
  }

  changeMode(mode) {
    this.mode = mode;
  }

  checkDates() {
    if (this.toDate && this.fromDate) {
      let startDate = new Date(this.fromDate.year, this.fromDate.month, this.fromDate.day);
      let endDate = new Date(this.toDate.year, this.toDate.month, this.toDate.day);
      if (startDate > endDate) {
        this.invalidDates = true;
      }
      else {
        this.validDates = true;
        this.invalidDates = false;
      }
    }
    else {
      this.invalidDates = false;
    }
  }

  changeCity(city) {
    this.selectedCity = city;
    if (city) {
      let ct = city.city ? city.city.trim() : city.city;
      this.schoolUrl = this.baseUrl + 'search/query/city=' + ct + '&name='
    }
  }

  changeSchool(school) {
    this.selectedSchool = school;
  }

  getDonationsList() {
    let fromDate;
    let toDate;
    if (this.fromDate)
      fromDate = new Date(this.fromDate.year, this.fromDate.month, this.fromDate.day);
    if (this.toDate)
      toDate = new Date(this.toDate.year, this.toDate.month, this.toDate.day);

    let payload = {
      "page": 1,
      "from": fromDate,
      "to": toDate,
      "schoolId": this.selectedSchool.id,
      "city": this.selectedCity.city,
      "state": this.selectedState.state
    };

    this._store.dispatch(new ListDonationsActions.ListDonations(payload));
  }


  downloadDonationsList() {
    let fromDate;
    let toDate;
    if (this.fromDate)
      fromDate = new Date(this.fromDate.year, this.fromDate.month, this.fromDate.day);
    if (this.toDate)
      toDate = new Date(this.toDate.year, this.toDate.month, this.toDate.day);

    let payload = {
      "page": 1,
      "from": fromDate,
      "to": toDate,
      "schoolId": this.selectedSchool.id,
      "city": this.selectedCity.city,
      "state": this.selectedState.state
    };

  }

  ngOnDestroy() {
    if (this.subDonations)
      this.subDonations.unsubscribe();
    if (this.subDonationsErr)
      this.subDonationsErr.unsubscribe();
    if (this.subCategories)
      this.subCategories.unsubscribe();
    if (this.subCategoriesErr)
      this.subCategoriesErr.unsubscribe();
  }
}
