import { AddBusinessService } from './add-business.service';
import { map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import 'rxjs/Rx';
import * as AddBusinessActions from './actions/list-donations.actions';

@Injectable()
export class DashboardEffects {

    constructor(
        private actions$: Actions,
        private addBusinessService: AddBusinessService,
    ) { }


    @Effect()
    addBusinesses$: Observable<Action> = this.actions$.ofType<AddBusinessActions.ListDonations>(AddBusinessActions.LIST_DONATIONS)
        .map(action => action)
        .switchMap(
        payload => this.addBusinessService.addBusiness(payload)
            .map(results => new AddBusinessActions.ListDonationsSuccess(results))
            .catch(err =>
                Observable.of({ type: AddBusinessActions.LIST_DONATIONS_FAILURE, err: err })
            ));

}
