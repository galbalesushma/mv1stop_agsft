import { UserDetails } from './../model/user-details';

export const COMPLETE_DONATION = 'COMPLETE_DONATION';
export const COMPLETE_DONATION_SUCCESS = 'COMPLETE_DONATION_SUCCESS';
export const COMPLETE_DONATION_FAILURE = 'COMPLETE_DONATION_FAILURE';

export class CompleteDonation {
    readonly type = COMPLETE_DONATION;
    constructor(public userDetails:UserDetails, public selectionType:string, public selectionId:number) { }
}

export class CompleteDonationSuccess {
    readonly type = COMPLETE_DONATION_SUCCESS;
    constructor(public res: any) { }
}

export class CompleteDonationFailure {
    readonly type = COMPLETE_DONATION_FAILURE;
    constructor(public err: any) { }
}


export type All = CompleteDonation | CompleteDonationSuccess | CompleteDonationFailure 