import { UserDetails } from './../model/user-details';

export const LIST_USER__DETAILS = 'LIST_USER__DETAILS';
export const LIST_USER__DETAILS_SUCCESS = 'LIST_USER__DETAILS_SUCCESS';
export const LIST_USER__DETAILS_FAILURE = 'LIST_USER__DETAILS_FAILURE';

export class ListUserDetails {
    readonly type = LIST_USER__DETAILS;
    constructor(public profileId:string) { }
}

export class ListUserDetailsSuccess {
    readonly type = LIST_USER__DETAILS_SUCCESS;
    constructor(public res: any) { }
}

export class ListUserDetailsFailure {
    readonly type = LIST_USER__DETAILS_FAILURE;
    constructor(public err: any) { }
}


export type All = ListUserDetails | ListUserDetailsSuccess | ListUserDetailsFailure 