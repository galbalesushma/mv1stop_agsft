import { DirectivesModule } from './../common/directives/directives.module';
import { PaymentService } from './payment.service';
import { PaymentComponent } from './payment.component';
import { PaymentEffects } from './payment.effects';
import { PaymentRoutingModule } from './payment-routing.module';
import { LocationsAndSchoolsService } from './../common/services/locations-and-schools.service';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HttpModule } from '@angular/http';

@NgModule({
    imports: [
        PaymentRoutingModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
        EffectsModule.forFeature([PaymentEffects]),
        DirectivesModule
    ],
    declarations: [PaymentComponent],
    exports: [PaymentComponent],
    providers: [PaymentService]
})
export class PaymentModule {

}
