export class UserDetails {
    id:number;
    cardNo:string;
    cvv:string;
    validTill:string;
    firstName:string;
    lastName:string;
    addr:string;
    street:string;
    city:string;
    zip:string;
    state:string;
    country:string;
    phone:string;
    email:string;
}