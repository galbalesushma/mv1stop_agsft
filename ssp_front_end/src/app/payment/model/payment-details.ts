export class PaymentDetails {
    id:number;
    cardNo:number;
    cvv:string;
    validTill:string;
    expiryMonth:number;
    expiryYear:number;
    firstName:string;
    lastName:string;
    addr:string;
    street:string;
    city:string;
    zip:string;
    state:string;
    country:string;
    phone:string;
    email:string;
}