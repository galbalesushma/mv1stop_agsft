import * as ListUserDetailsActions from '../actions/list-user-details.actions';

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
    result:{},
    err: {}
};

export function reducer(state = initialState, action: ListUserDetailsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ListUserDetailsActions.LIST_USER__DETAILS_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case ListUserDetailsActions.LIST_USER__DETAILS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
      
        default: {
            return state;
        }

    }

}
