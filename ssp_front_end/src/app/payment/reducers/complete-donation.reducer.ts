import * as CompleteDonationActions from '../actions/complete-donation.actions';

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
    result:{},
    err: {}
};

export function reducer(state = initialState, action: CompleteDonationActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case CompleteDonationActions.COMPLETE_DONATION_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case CompleteDonationActions.COMPLETE_DONATION_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
      
        default: {
            return state;
        }

    }

}
