import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx';
import { UserDetails } from './model/user-details';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Validators, FormBuilder, FormControl } from '@angular/forms';
import { CharactersOnlyValidator, EmailValidator, CardNumberValidator, CreditCardCodeValidator, MobileNumberValidator, ZipCodeValidator, PhoneNumberValidator } from '../common/customValidators';
import { BaseClass } from '../common/base.class';
import * as fromRoot from './../reducers';
import * as CompeteDonationActions from './actions/complete-donation.actions';
import * as Constants from './../common/constants/constants';
import * as ListUserDetailsAcitons from './actions/list-user-details.actions';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent extends BaseClass implements OnInit, OnDestroy {
  paymentDetailsForm: FormGroup;
  userDetails: UserDetails = new UserDetails();
  months = [];
  years = [];
  selectionType: string;
  selectionId: number;
  name: string;
  stateList = [];
  paymentSuccess: boolean;
  paymentFailure: boolean;
  expiryMonth: number;
  expiryYear: number;
  schoolsMessage: string;
  monthError: boolean = true;
  yearError: boolean = true;
  validCardNumber: boolean = true;
  validZip: boolean = true;
  validPhone: boolean = true;
  invalidAreaCode: boolean;
  validCVV: boolean = true;
  validEmail: boolean = true;
  errorMsg: string;
  hideSuccessMessage: any;
  showCardError: boolean;

  obsCompleteDonation: Observable<any>;
  subCompleteDonation: Subscription;
  obsCompleteDonationErr: Observable<any>;
  subCompleteDonationErr: Subscription;

  obsUserDetails: Observable<any>;
  subUserDetails: Subscription;
  obsUserDetailsErr: Observable<any>;
  subUserDetailsErr: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private _store: Store<fromRoot.State>,
    private _route: ActivatedRoute,
    private router: Router) {
    super();
    this.userDetails.state = 'North Carolina';
    this.userDetails.country = 'USA';
    this.obsCompleteDonation = this._store.select(fromRoot.selectCompleteDonationSuccess);
    this.obsCompleteDonationErr = this._store.select(fromRoot.selectCompleteDonationFailure);
    this.obsUserDetails = this._store.select(fromRoot.selectListUserDetailsSuccess);
    this.obsUserDetailsErr = this._store.select(fromRoot.selectListUserDetailsFailure);
    this.stateList = Constants.statesList;


    this.paymentDetailsForm = formBuilder.group({
      'cardNumber': ['', Validators.compose([Validators.required])],
      'cvv': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(4)])],
      'expiryMonth': ['', Validators.compose([Validators.required])],
      'expiryYear': ['', Validators.compose([Validators.required])],
      'firstName': ['', Validators.compose([Validators.required])],
      'lastName': ['', Validators.compose([Validators.required])],
      'address': ['', Validators.compose([Validators.required])],
      'street': [''],
      'city': ['', Validators.compose([Validators.required])],
      'zipcode': ['', Validators.compose([Validators.required])],
      'state': ['', Validators.compose([Validators.required])],
      'country': ['', Validators.compose([Validators.required])],
      'email': [''],
      'phone': ['', Validators.compose([Validators.required])],
    })

  }

  ngOnInit() {
    this.paymentDetailsForm.controls
    let isOnInit = true;
    let profileId = localStorage.getItem('profileId');
    if (profileId) {
      this._store.dispatch(new ListUserDetailsAcitons.ListUserDetails(profileId));
    }

    this._route.params.subscribe(params => {
      this.selectionType = params['type'];
      this.selectionId = params['id'];
      this.name = params['name'];
      this.changeMessage();
    })

    this.subCompleteDonationErr = this.obsCompleteDonationErr.subscribe(err => {
      if (err && !isOnInit) {

      }
    })

    this.subCompleteDonation = this.obsCompleteDonation.subscribe(res => {
      if (res && !isOnInit) {
        localStorage.setItem('profileId', res.profileId);
        if (res.status == "ERROR") {
          this.errorMsg = res.message;
          this.paymentFailure = true;
          this.hideSuccessMessage = setInterval(() => {
            this.hideMessage();
          }, 5000);
        }
        else if (res.status == "APPROVED") {
          localStorage.setItem('cardId', res.cardId)
          this.paymentSuccess = true;
          this.hideSuccessMessage = setInterval(() => {
            this.hideMessage();
          }, 5000);
          this.router.navigate(['/payment-success']);
        }
      }
    })

    this.subUserDetails = this.obsUserDetails.subscribe(res => {
      if (res && !isOnInit) {
        this.userDetails = res;
      }
    })

    this.subUserDetailsErr = this.obsUserDetailsErr.subscribe(err => {
      if (err && !isOnInit) {

      }
    })
    isOnInit = false;
  }

  onFocus() {
    this.showCardError = false;
  }

  onBlur() {
    this.showCardError = true;
  }

  changeMessage() {
    if (!this.name) {
      this.schoolsMessage = 'Local area schools are selected for your support by default';
    }
    else if (this.selectionType == 'school' && this.name) {
      this.schoolsMessage = this.name + ' has been selected for your support';
    }
    else if (this.selectionType == 'city' && this.name) {
      this.schoolsMessage = this.name + ' city schools are selected for your support';
    }
    else if (this.selectionType == 'district' && this.name) {
      this.schoolsMessage = this.name + ' county schools are selected for your support';
    }
    else if (this.selectionType == 'state' && this.name) {
      this.schoolsMessage = this.name + ' state schools are selected for your support';
    }
  }

  createMonthRange() {
    this.months = [];
    for (var i = 1; i <= 12; i++) {
      if (i < 10) {
        this.months.push("0" + i);
      }
      else {
        this.months.push(i);
      }
    }
    return this.months;
  }

  createYearRange(value) {
    this.years = [];
    let year = new Date().getFullYear();
    for (var i = year; i <= year + value; i++) {
      this.years.push(i);
    }
    return this.years;
  }

  setNumberAlignment(positions: number[], type, event) {
    if (event.key !== "Backspace" && event.key !== "Delete") {
      if (type == 'phone') {
        if (this.userDetails && this.userDetails.phone) {
          let index = positions.findIndex(po => po == this.userDetails.phone.length);
          if (index != -1) {
            this.userDetails.phone += " ";
          }
        }
      }
      else if (type == 'card') {
        if (this.userDetails && this.userDetails.cardNo) {
          let index = positions.findIndex(po => po == this.userDetails.cardNo.length);
          if (index != -1) {
            this.userDetails.cardNo += " ";
          }
        }
      }
    }
  }

  alignPhone() {
    if (this.userDetails && this.userDetails.phone) {
      let trimmed = this.userDetails.phone.replace(/\s+/g, '');
      let numbers = [];
      for (let i = 0; i < trimmed.length; i += 3) {
        if (i < 6) {
          numbers.push(trimmed.substr(i, 3));
        }
        else {
          numbers.push(trimmed.substr(i));
          i = trimmed.length;
        }
      }
      this.userDetails.phone = numbers.join(' ');

      let numberReg = /^\d{3}\s\d{3}\s\d{4}$/g;
      if (this.userDetails.phone) {
        this.validPhone = numberReg.test(this.userDetails.phone);
        let code = this.userDetails.phone.substring(0, 3);
        let areacode = +code;
        if (areacode < 201) {
          this.invalidAreaCode = true;
        }
        else {
          this.invalidAreaCode = false;
        }
      }
    }
  }

  alignCard() {
    if (this.userDetails && this.userDetails.cardNo) {
      let trimmed = this.userDetails.cardNo.replace(/\s+/g, '');
      let numbers = [];
      for (let i = 0; i < trimmed.length; i += 4) {
        numbers.push(trimmed.substr(i, 4));
      }
      this.userDetails.cardNo = numbers.join(' ');

      let numberReg = /^\d{4}\s\d{4}\s\d{4}\s\d{4}$/g;
      if (this.userDetails.cardNo) {
        this.validCardNumber = numberReg.test(this.userDetails.cardNo);
      }
    }

  }

  validateZip() {
    let numberReg: RegExp = new RegExp('^\\d{5}$');
    if (this.userDetails && this.userDetails.zip) {
      this.validZip = numberReg.test(this.userDetails.zip);
    }
  }

  validateCVV() {
    let numberReg: RegExp = new RegExp('^[0-9]{3,4}$');
    if (this.userDetails && this.userDetails.cvv) {
      this.validCVV = numberReg.test(this.userDetails.cvv);
    }
  }

  validateEmail() {
    let emailReg: RegExp = new RegExp('^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$');
    if (this.userDetails && this.userDetails.email) {
      this.validEmail = emailReg.test(this.userDetails.email);
    }
    else {
      this.validEmail = true;
    }
  }

  completeDonation() {
    if (this.userDetails) {
      this.userDetails.validTill = this.expiryMonth + "" + this.expiryYear;
      this._store.dispatch(new CompeteDonationActions.CompleteDonation(this.userDetails, this.selectionType, this.selectionId));
    }
  }

  pasteCheck(data) {
    let regexStr = '^[0-9 ]+$';
    let regEx = new RegExp(regexStr);
    if (regEx.test(data.clipboardData.getData('text/plain')))
      return;
    else
      data.preventDefault();
  }

  checkLength(data) {
    let text = data.clipboardData.getData('text/plain');
    if (text && text.length < 22)
      return;
    else
      data.preventDefault();
  }

  hideMessage() {
    this.paymentFailure = false;
    this.paymentSuccess = false;
    clearInterval(this.hideSuccessMessage);
    this.hideSuccessMessage = null;
  }

  ngOnDestroy() {
    if (this.subCompleteDonation)
      this.subCompleteDonation.unsubscribe();
    if (this.subCompleteDonationErr)
      this.subCompleteDonationErr.unsubscribe();
    if (this.subUserDetails)
      this.subUserDetails.unsubscribe();
    if (this.subUserDetailsErr)
      this.subUserDetailsErr.unsubscribe();
  }
}
