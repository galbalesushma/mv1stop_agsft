import { UserDetails } from './model/user-details';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions, RequestOptionsArgs, Headers, Response } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable()
export class PaymentService {
    baseUrl = environment.apiUrl;
    
    private _paymentUrl = "payment";
    private _getUserDetailsUrl = "payment/{profileId}";

    constructor(private _http: Http) {
    }

    completeDonation(userDetails:UserDetails, type, id) {
        if(type && id){
            return this._http.post(this._paymentUrl+"/"+type+"/"+id, userDetails).map((res: Response) => res.json());
        }
        return this._http.post(this._paymentUrl, userDetails).map((res: Response) => res.json());
    }

    getUserDetails(profileId:string) {
        return this._http.get(this._getUserDetailsUrl.replace('{profileId}',profileId)).map((res: Response) => res.json());
    }
}




