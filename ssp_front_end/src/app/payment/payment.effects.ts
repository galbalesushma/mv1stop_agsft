import { PaymentService } from './payment.service';
import { map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import 'rxjs/Rx';
import * as CompleteDonationActions from './actions/complete-donation.actions';
import * as ListUserDetailsActions from './actions/list-user-details.actions';

@Injectable()
export class PaymentEffects {

    constructor(
        private actions$: Actions,
        private paymentService: PaymentService,
    ) { }

    @Effect()
    completeDonation$: Observable<Action> = this.actions$.ofType<CompleteDonationActions.CompleteDonation>(CompleteDonationActions.COMPLETE_DONATION)
        .map(action => action)
        .switchMap(
        payload => this.paymentService.completeDonation(payload.userDetails, payload.selectionType, payload.selectionId)
            .map(results => new CompleteDonationActions.CompleteDonationSuccess(results))
            .catch(err =>
                Observable.of({ type: CompleteDonationActions.COMPLETE_DONATION_FAILURE, err: err })
            ));

    @Effect()
    userDetails$: Observable<Action> = this.actions$.ofType<ListUserDetailsActions.ListUserDetails>(ListUserDetailsActions.LIST_USER__DETAILS)
        .map(action => action)
        .switchMap(
        payload => this.paymentService.getUserDetails(payload.profileId)
            .map(results => new ListUserDetailsActions.ListUserDetailsSuccess(results))
            .catch(err =>
                Observable.of({ type: ListUserDetailsActions.LIST_USER__DETAILS_FAILURE, err: err })
            ));

}
