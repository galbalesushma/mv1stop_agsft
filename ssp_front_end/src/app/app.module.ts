import { DashboardModule } from './admin/dashboard/dashboard.module';
import { CommonService } from './common/services/common.service';
import { CommonEffects } from './common/effects/common.effects';
import { BusinessesModule } from './businesses/businesses.module';
import { RequestOptions } from '@angular/http';
import { XHRBackend } from '@angular/http';
import { Http } from '@angular/http';
import { FooterComponent } from './common/footer/footer.component';
import { AddBusinessModule } from './add-business/add-business.module';
import { PaymentSuccessModule } from './payment-success/payment-success.module';
import { PaymentModule } from './payment/payment.module';
import { HeaderComponent } from './common/header/header.component';
import { Router, RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { DonateModule } from './donate/donate.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { reducers } from './reducers';
import { AppComponent } from './app.component';
import { httpFactory } from './factory/http.factory';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    RouterModule,
    BrowserModule,
    EffectsModule.forRoot([]),
    EffectsModule.forFeature([CommonEffects]),
    StoreModule.forRoot(reducers),
    DonateModule,
    PaymentModule,
    PaymentSuccessModule,
    AddBusinessModule,
    BusinessesModule,
    DashboardModule,
    RouterModule.forRoot([])
  ],
  providers: [
    {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions, Router]
    },
    CommonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
