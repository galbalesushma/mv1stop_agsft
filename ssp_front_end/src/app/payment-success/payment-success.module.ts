import { DirectivesModule } from './../common/directives/directives.module';
import { PaymentSuccessService } from './payment-success.service';
import { PaymentSuccessComponent } from './payment-success.component';
import { PaymentSuccessRoutingModule } from './payment-success-routing.module';
import { LocationsAndSchoolsService } from './../common/services/locations-and-schools.service';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HttpModule } from '@angular/http';
import { PaymentSuccessEffects } from './payment-success.effects';

@NgModule({
    imports: [
        PaymentSuccessRoutingModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
        EffectsModule.forFeature([PaymentSuccessEffects]),
        DirectivesModule
    ],
    declarations: [PaymentSuccessComponent],
    exports: [PaymentSuccessComponent],
    providers: [PaymentSuccessService]
})
export class PaymentSuccessModule {

}
