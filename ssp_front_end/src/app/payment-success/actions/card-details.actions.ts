
export const CARD_DETAILS = 'CARD_DETAILS';
export const CARD_DETAILS_SUCCESS = 'CARD_DETAILS_SUCCESS';
export const CARD_DETAILS_FAILURE = 'CARD_DETAILS_FAILURE';

export class GetCardDetails {
    readonly type = CARD_DETAILS;
    constructor(public cardId:string) { }
}

export class GetCardDetailsSuccess {
    readonly type = CARD_DETAILS_SUCCESS;
    constructor(public res: any) { }
}

export class GetCardDetailsFailure {
    readonly type = CARD_DETAILS_FAILURE;
    constructor(public err: any) { }
}


export type All = GetCardDetails | GetCardDetailsSuccess | GetCardDetailsFailure 