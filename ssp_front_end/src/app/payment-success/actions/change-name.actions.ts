
export const CHANGE_NAME = 'CHANGE_NAME';
export const CHANGE_NAME_SUCCESS = 'CHANGE_NAME_SUCCESS';
export const CHANGE_NAME_FAILURE = 'CHANGE_NAME_FAILURE';

export class ChangeName {
    readonly type = CHANGE_NAME;
    constructor() { }
}

export class ChangeNameSuccess {
    readonly type = CHANGE_NAME_SUCCESS;
    constructor(public res: any) { }
}

export class ChangeNameFailure {
    readonly type = CHANGE_NAME_FAILURE;
    constructor(public err: any) { }
}


export type All = ChangeName | ChangeNameSuccess | ChangeNameFailure 