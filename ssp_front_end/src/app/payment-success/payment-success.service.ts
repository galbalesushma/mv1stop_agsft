import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions, RequestOptionsArgs, Headers, Response } from '@angular/http';

@Injectable()
export class PaymentSuccessService {

    private _getCardDetailsUrl = "cards/{cardID}";
    private _getCardToPrintUrl = "card/download/{cardId}";
    private _saveCardUrl = "card/save";
    private _emailCardUrl = "card/email";


    constructor(private _http: Http) {
    }

    getCardDetails(cardId) {
        return this._http.get(this._getCardDetailsUrl.replace('{cardID}', cardId)).map((res: Response) => res.json());
    }

    getCardToPrint(cardId) {
        return this._http.get(this._getCardToPrintUrl.replace('{cardId}',cardId)).map((res: Response) => res.json());
    }

    saveCardDetails(card) {
        return this._http.post(this._saveCardUrl, card).map((res: Response) => res.json());
    }

    emailCard(card) {
        return this._http.post(this._emailCardUrl, card).map((res: Response) => res.json());
    }
}



