import { Router } from '@angular/router';
import { PaymentSuccessService } from './payment-success.service';
import { UserDetails } from './../payment/model/user-details';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { CardDetails } from './model/card-details';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as fromRoot from './../reducers';
import * as ChangeNameActions from './actions/change-name.actions';
import * as CardDetailsActions from './actions/card-details.actions';
import * as ListUserDetailsAcitons from '../payment/actions/list-user-details.actions';
import { BaseClass } from '../common/base.class';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.css']
})
export class PaymentSuccessComponent extends BaseClass implements OnInit, OnDestroy {

  cardId: string;
  profileId: string;

  firstName: string;
  lastName: string;
  email: string;
  disableEmail: boolean;

  invalidDetails: boolean;
  message: string;

  card: CardDetails = new CardDetails();
  userDetails: UserDetails = new UserDetails();
  imageBase64;
  showAllBtns: boolean;
  validEmail = true;
  emailRequired: boolean;
  hideSuccessMessage: any;
  showSuccessMsg: boolean = false;
  showFailureMsg: boolean = false;

  obsChangeName: Observable<any>;
  subChangeName: Subscription;
  obsChangeNameError: Observable<any>;
  subChangeNameError: Subscription;

  obsCardDetails: Observable<any>;
  subCardDetails: Subscription;
  obsCardDetailsErr: Observable<any>;
  subCardDetailsErr: Subscription;

  obsUserDetails: Observable<any>;
  subUserDetails: Subscription;
  obsUserDetailsErr: Observable<any>;
  subUserDetailsErr: Subscription;

  constructor(
    private _store: Store<fromRoot.State>,
    private ref: ChangeDetectorRef,
    private _router: Router,
    private _service: PaymentSuccessService) {
    super();
    this.obsChangeName = this._store.select(fromRoot.selectChangeNameSuccess);
    this.obsChangeNameError = this._store.select(fromRoot.selectChangeNameFailure);
    this.obsCardDetails = this._store.select(fromRoot.selectListCardDetailsSuccess);
    this.obsCardDetailsErr = this._store.select(fromRoot.selectListCardDetailsFailure);
    this.obsUserDetails = this._store.select(fromRoot.selectListUserDetailsSuccess);
    this.obsUserDetailsErr = this._store.select(fromRoot.selectListUserDetailsFailure);
  }

  ngOnInit() {
    let isOnInit = true;
    this.cardId = localStorage.getItem('cardId');
    this.profileId = localStorage.getItem('profileId');
    this._store.dispatch(new CardDetailsActions.GetCardDetails(this.cardId));

    if (!this.userDetails || !this.userDetails.email) {
      this._store.dispatch(new ListUserDetailsAcitons.ListUserDetails(this.profileId));
    }
    this.subChangeName = this.obsChangeName.subscribe(res => {
      if (res && !isOnInit) {

      }
    })

    this.subChangeNameError = this.obsChangeNameError.subscribe(err => {
      if (err && !isOnInit) {

      }
    })

    this.subCardDetails = this.obsCardDetails.subscribe(res => {
      if (res && !isOnInit) {
        this.card = res;
        if (this.card) {
          this.firstName = this.card.firstName;
          this.lastName = this.card.lastName;
        }
      }
    })

    this.subCardDetailsErr = this.obsCardDetailsErr.subscribe(err => {
      if (err && !isOnInit) {

      }
    })

    this.subUserDetails = this.obsUserDetails.subscribe(res => {
      if (res && !isOnInit) {
        this.userDetails = res;
        if (this.userDetails) {
          this.email = this.userDetails.email;
          let emailReg: RegExp = new RegExp('^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$');
          this.disableEmail = emailReg.test(this.email);
        }
      }
    })

    this.subUserDetailsErr = this.obsUserDetailsErr.subscribe(err => {
      if (err && !isOnInit) {

      }
    })

    isOnInit = false;
  }

  changeNameOnCard() {
    this._store.dispatch(new ChangeNameActions.ChangeName());
  }


  printImage() {
    this.showAllBtns = true;
    this._service.getCardToPrint(this.cardId).subscribe(res => {
      this.imageBase64 = res;
      this.ref.markForCheck();
      window.print();
    },
      err => {

      })
  }

  saveCard() {
    let card = {
      "id": +this.cardId,
      "firstName": this.firstName,
      "lastName": this.lastName
    }
    this.showAllBtns = true;

    this._service.saveCardDetails(card).subscribe(res => {
      if (res) {
        this.showSuccessMsg = true;
        this.message = 'Your card details are saved successfully';
        this.hideSuccessMessage = setInterval(() => {
          this.hideMessage();
        }, 5000);
      }
    },
      err => {
        if (err) {
          this.message = 'Failed to update your card details';
          this.showFailureMsg = true;
        }
      })
  }

  hideMessage() {
    this.showSuccessMsg = false;
    this.showFailureMsg = false;
    clearInterval(this.hideSuccessMessage);
    this.hideSuccessMessage = null;
  }

  buyMore() {
    this._router.navigate(['/give']);
  }

  validateEmail() {
    let emailReg: RegExp = new RegExp('^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$');
    if (this.email) {
      this.validEmail = emailReg.test(this.email);
      this.disableEmail = this.validEmail;
      this.emailRequired = false;
    }
    else {
      this.emailRequired = true;
    }
  }

  emailCard() {
    let card = {
      "cardId": +this.cardId,
      "profileId": this.profileId,
      "email": this.email
    }

    this.validateEmail();
    if (this.email) {
      this.showAllBtns = true;
      this._service.emailCard(card).subscribe(res => {
        if (res) {
          this.showSuccessMsg = true;
          this.message = 'Your card details are emailed successfully';
          this.hideSuccessMessage = setInterval(() => {
            this.hideMessage();
          }, 5000);
        }
      },
        err => {
          if (err) {
            this.showFailureMsg = true;
            this.message = 'Failed to email your card details';
            this.hideSuccessMessage = setInterval(() => {
              this.hideMessage();
            }, 5000);
          }
        })
    }
  }

  addBusiness() {
    this._router.navigate(['/add-business']);
  }

  checkNameLength(e) {
    let length = 0;
    if (this.firstName) {
      length = this.firstName.length;
    }
    if (this.lastName) {
      length += this.lastName.length;
    }

    if (length < 22) {
      return;
    }
    else if ([46, 8, 9, 27, 13, 110].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode == 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+V
      (e.keyCode == 86 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode == 88 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    else {
      e.preventDefault();
    }
  }

  checkName() {
    let length = 0;
    if (this.firstName) {
      length = this.firstName.length;
    }
    if (this.lastName) {
      length += this.lastName.length;
    }

    if (length == 0) {
      this.invalidDetails = true;
    }
    else {
      this.invalidDetails = false;
    }
  }

  pasteCheck(data) {
    let length = 0;
    if (this.firstName) {
      length = this.firstName.length;
    }
    if (this.lastName) {
      length += this.lastName.length;
    }

    length += data.clipboardData.getData('text/plain').length;
    if (length < 22)
      return;
    else
      data.preventDefault();
  }

  ngOnDestroy() {
    if (this.subCardDetails)
      this.subCardDetails.unsubscribe();
    if (this.subCardDetailsErr)
      this.subCardDetailsErr.unsubscribe();
    if (this.subChangeName)
      this.subChangeName.unsubscribe();
    if (this.subChangeNameError)
      this.subChangeNameError.unsubscribe();
    if (this.subUserDetails)
      this.subUserDetails.unsubscribe();
    if (this.subUserDetailsErr)
      this.subUserDetailsErr.unsubscribe();
  }
}
