import * as ChangeNameActions from '../actions/change-name.actions';

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
    result:{},
    err: {}
};

export function reducer(state = initialState, action: ChangeNameActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case ChangeNameActions.CHANGE_NAME_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case ChangeNameActions.CHANGE_NAME_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
      
        default: {
            return state;
        }

    }

}
