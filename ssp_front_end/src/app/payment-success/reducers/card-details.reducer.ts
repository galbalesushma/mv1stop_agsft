import * as CardDetailsActions from '../actions/card-details.actions';

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
    result:{},
    err: {}
};

export function reducer(state = initialState, action: CardDetailsActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case CardDetailsActions.CARD_DETAILS_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case CardDetailsActions.CARD_DETAILS_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
      
        default: {
            return state;
        }

    }

}
