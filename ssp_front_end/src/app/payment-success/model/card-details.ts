export class CardDetails {
    id:number;
    firstName:string;
    lastName:string;
    number:string;
    createdAt:Date;
    updatedAt:Date;
    validFrom:Date;
    validTill:string;
    isActive:boolean;
    isDeleted:boolean;
    profileId:number;
}