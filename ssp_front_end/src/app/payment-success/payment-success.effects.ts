import { PaymentSuccessService } from './payment-success.service';
import { map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import 'rxjs/Rx';

import * as ListCardDetailsActions from './actions/card-details.actions';

@Injectable()
export class PaymentSuccessEffects {

    constructor(
        private actions$: Actions,
        private paymentSuccessService: PaymentSuccessService,
    ) { }

    @Effect()
    cardDetails$: Observable<Action> = this.actions$.ofType<ListCardDetailsActions.GetCardDetails>(ListCardDetailsActions.CARD_DETAILS)
        .map(action => action)
        .switchMap(
        payload => this.paymentSuccessService.getCardDetails(payload.cardId)
            .map(results => new ListCardDetailsActions.GetCardDetailsSuccess(results))
            .catch(err =>
                Observable.of({ type: ListCardDetailsActions.CARD_DETAILS_FAILURE, err: err })
            ));
}
