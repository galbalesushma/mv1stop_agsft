import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions, RequestOptionsArgs, Headers, Response } from '@angular/http';

@Injectable()
export class AddBusinessService {

    private _addBusinessUrl = "business/enroll";

    constructor(private _http: Http) {
    }

    addBusiness(business) {
        return this._http.post(this._addBusinessUrl, business).map((res: Response) => res.json());
    }

}



