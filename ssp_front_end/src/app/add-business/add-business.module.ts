import { DirectivesModule } from './../common/directives/directives.module';
import { AddBusinessService } from './add-business.service';
import { AddBusinessComponent } from './add-business.component';
import { AddBusinessEffects } from './add-business.effects';
import { AddBusinessRoutingModule } from './add-business-routing.module';
import { LocationsAndSchoolsService } from './../common/services/locations-and-schools.service';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HttpModule } from '@angular/http';

@NgModule({
    imports: [
        AddBusinessRoutingModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
        EffectsModule.forFeature([AddBusinessEffects]),
        DirectivesModule
    ],
    declarations: [AddBusinessComponent],
    exports: [AddBusinessComponent],
    providers: [AddBusinessService]
})
export class AddBusinessModule {

}
