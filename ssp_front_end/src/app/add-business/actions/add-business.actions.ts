import { Business } from './../model/business';

export const ADD_BUSINESSES = 'ADD_BUSINESSES';
export const ADD_BUSINESSES_SUCCESS = 'ADD_BUSINESSES_SUCCESS';
export const ADD_BUSINESSES_FAILURE = 'ADD_BUSINESSES_FAILURE';

export class AddBusinesses {
    readonly type = ADD_BUSINESSES;
    constructor(public business:Business) { }
}

export class AddBusinessesSuccess {
    readonly type = ADD_BUSINESSES_SUCCESS;
    constructor(public res: any) { }
}

export class AddBusinessesFailure {
    readonly type = ADD_BUSINESSES_FAILURE;
    constructor(public err: any) { }
}


export type All = AddBusinesses | AddBusinessesSuccess | AddBusinessesFailure 