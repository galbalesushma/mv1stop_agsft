import { Router } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx';
import { FormGroup } from '@angular/forms/src/model';
import { Component, OnInit } from '@angular/core';
import { Business } from './model/business';
import { FormBuilder, Validators } from '@angular/forms';
import { ZipCodeValidator, PhoneNumberValidator, EmailValidator } from '../common/customValidators';
import { BaseClass } from '../common/base.class';
import * as fromRoot from './../reducers';
import * as Constants from './../common/constants/constants';
import * as AddBusinessActions from './actions/add-business.actions';
import * as ListBusinessCategories from '../common/actions/list-business-categories.actions';

@Component({
  selector: 'add-business',
  templateUrl: './add-business.component.html',
  styleUrls: ['./add-business.component.css']
})

export class AddBusinessComponent extends BaseClass implements OnInit, OnDestroy {
  addBusinessForm: FormGroup;
  business: Business = new Business();
  stateList = [];
  categoriesList = [];
  showSuccessMsg: boolean = false;
  showFailureMsg: boolean = false;
  hideSuccessMessage: any;
  addClicked: boolean;
  validEmail: boolean = true;
  validZip: boolean = true;
  validPhone: boolean = true;
  invalidAreaCode: boolean;

  message: string;

  obsAddBusiness: Observable<any>;
  subAddBusiness: Subscription;
  obsAddBusinessErr: Observable<any>;
  subAddBusinessErr: Subscription;

  obsCategories: Observable<any>;
  subCategories: Subscription;
  obsCategoriesErr: Observable<any>;
  subCategoriesErr: Subscription;

  constructor(private formBuilder: FormBuilder,
    private _store: Store<fromRoot.State>,
    private _router: Router
  ) {
    super();
    this.business.state = 'North Carolina';
    this.stateList = Constants.statesList;
    this.obsAddBusiness = this._store.select(fromRoot.selectAddBusinessSuccess);
    this.obsAddBusinessErr = this._store.select(fromRoot.selectAddBusinessFailure);
    this.obsCategories = this._store.select(fromRoot.selectListBusinessCategoriesSuccess);
    this.obsCategoriesErr = this._store.select(fromRoot.selectListBusinessCategoriesFailure);

    this.addBusinessForm = formBuilder.group({
      'name': ['', Validators.compose([Validators.required])],
      'type': ['', Validators.compose([Validators.required])],
      'street': ['', Validators.compose([Validators.required])],
      'city': ['', Validators.compose([Validators.required])],
      'zipcode': ['', Validators.compose([Validators.required])],
      'state': ['', Validators.compose([Validators.required])],
      'discount': ['', Validators.compose([Validators.required])],
      'website': [''],
      'email': [''],
      'phone': ['', Validators.compose([Validators.required])],
    })
  }

  ngOnInit() {
    let isOnInit = true;
    this._store.dispatch(new ListBusinessCategories.ListCategories);
    this.subAddBusiness = this.obsAddBusiness.subscribe(res => {
      if (res && !isOnInit) {
        this.message = res.message;
        this.showSuccessMsg = true;
        localStorage.removeItem('profileId');
        localStorage.removeItem('cardId');
        this.hideSuccessMessage = setInterval(() => {
          this.hideMessage();
        }, 5000);
      }
    });

    this.subAddBusinessErr = this.obsAddBusinessErr.subscribe(err => {
      if (err && !isOnInit) {
        localStorage.removeItem('profileId');
        localStorage.removeItem('cardId');
        this.message = 'Sorry the business enrollment failed, Please try again';
        this.showFailureMsg = true;
        this.hideSuccessMessage = setInterval(() => {
          this.hideMessage();
        }, 5000);
      }
    })

    this.subCategories = this.obsCategories.subscribe(res => {
      if (res) {
        this.categoriesList = res;
      }
    })

    this.subCategoriesErr = this.obsCategoriesErr.subscribe(err => {
      if (err) {

      }
    })
    isOnInit = false;
  }

  hideMessage() {
    this.showSuccessMsg = false;
    this.showFailureMsg = false;
    this._router.navigate(['/give']);
    clearInterval(this.hideSuccessMessage);
    this.hideSuccessMessage = null;
  }

  setNumberAlignment(positions: number[], event) {
    if (event.key !== "Backspace" && event.key !== "Delete") {
      if (this.business && this.business.phone) {
        let index = positions.findIndex(po => po == this.business.phone.toString().length);
        if (index != -1) {
          this.business.phone += " ";
        }
      }
    }
  }

  alignPhone() {
    if (this.business && this.business.phone) {
      let trimmed = this.business.phone.replace(/\s+/g, '');
      let numbers = [];
      for (let i = 0; i < trimmed.length; i += 3) {
        if (i < 6) {
          numbers.push(trimmed.substr(i, 3));
        }
        else {
          numbers.push(trimmed.substr(i));
          i = trimmed.length;
        }
      }
      this.business.phone = numbers.join(' ');

      let numberReg = /^\d{3}\s\d{3}\s\d{4}$/g;
      if (this.business.phone) {
        this.validPhone = numberReg.test(this.business.phone);
        let code = this.business.phone.substring(0, 3);
        let areacode = +code;
        if (areacode < 201) {
          this.invalidAreaCode = true;
        }
        else {
          this.invalidAreaCode = false;
        }
      }
    }
  }

  validateZip() {
    let numberReg: RegExp = new RegExp('^\\d{5}$');
    if (this.business && this.business.zip) {
      this.validZip = numberReg.test(this.business.zip);
    }
  }

  validateEmail() {
    let emailReg: RegExp = new RegExp('^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$');
    if (this.business && this.business.email) {
      this.validEmail = emailReg.test(this.business.email);
    }
    else {
      this.validEmail = true;
    }
  }

  pasteCheck(data) {
    let regexStr = '^[0-9 ]+$';
    let regEx = new RegExp(regexStr);
    if (regEx.test(data.clipboardData.getData('text/plain')))
      return;
    else
      data.preventDefault();
  }
  
  addBusiness() {
    if (this.business) {
      this.business.profileId = +localStorage.getItem('profileId');
      this._store.dispatch(new AddBusinessActions.AddBusinesses(this.business));
      this.addClicked = true;
    }
  }

  ngOnDestroy() {
    if (this.subAddBusiness)
      this.subAddBusiness.unsubscribe();
    if (this.subAddBusinessErr)
      this.subAddBusinessErr.unsubscribe();
    if (this.subCategories)
      this.subCategories.unsubscribe();
    if (this.subCategoriesErr)
      this.subCategoriesErr.unsubscribe();
  }
}
