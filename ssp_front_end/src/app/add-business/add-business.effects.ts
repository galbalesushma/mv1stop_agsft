import { AddBusinessService } from './add-business.service';
import { map } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import 'rxjs/Rx';
import * as AddBusinessActions from './actions/add-business.actions';

@Injectable()
export class AddBusinessEffects {

    constructor(
        private actions$: Actions,
        private addBusinessService: AddBusinessService,
    ) { }


    @Effect()
    addBusinesses$: Observable<Action> = this.actions$.ofType<AddBusinessActions.AddBusinesses>(AddBusinessActions.ADD_BUSINESSES)
        .map(action => action)
        .switchMap(
        payload => this.addBusinessService.addBusiness(payload.business)
            .map(results => new AddBusinessActions.AddBusinessesSuccess(results))
            .catch(err =>
                Observable.of({ type: AddBusinessActions.ADD_BUSINESSES_FAILURE, err: err })
            ));

}
