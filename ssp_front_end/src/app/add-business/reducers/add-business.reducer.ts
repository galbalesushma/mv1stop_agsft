import * as AddBusinessesActions from '../actions/add-business.actions';

export interface State {    
   
    result: any,
    err: any
}

const initialState: State = {    
    result:{},
    err: {}
};

export function reducer(state = initialState, action: AddBusinessesActions.All): State {
    //console.log("Action Type" + action.type);
    switch (action.type) {
        
        case AddBusinessesActions.ADD_BUSINESSES_SUCCESS: {
            return {
                ...state,
                result: action.res
            }
        }

        case AddBusinessesActions.ADD_BUSINESSES_FAILURE: {
            return {
                ...state,
                err: action.err
            }
        }
      
        default: {
            return state;
        }

    }

}
