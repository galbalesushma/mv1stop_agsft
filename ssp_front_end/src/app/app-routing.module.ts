import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './donate/donate.module#DonateModule'
  },
  {
    path: 'donate',
    loadChildren: './donate/donate.module#DonateModule'
  },
  {
    path: 'payment/:type/:name/:id',
    loadChildren: './payment/payment.module#PaymentModule'
  },
  {
    path: 'payment-success',
    loadChildren: './payment-success/payment-success.module#PaymentSuccessModule'
  },
  {
    path: 'add-business',
    loadChildren: './add-business/add-business.module#AddBusinessModule'
  },
  {
    path: 'dashboard',
    loadChildren: './admin/dashboard/dashboard.module#DashboardModule'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
