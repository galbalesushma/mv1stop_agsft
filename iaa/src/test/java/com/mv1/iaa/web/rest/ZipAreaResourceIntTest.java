package com.mv1.iaa.web.rest;

import static com.mv1.iaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.IaaApp;
import com.mv1.iaa.domain.ZipArea;
import com.mv1.iaa.repository.ZipAreaRepository;
import com.mv1.iaa.service.ZipAreaService;
import com.mv1.iaa.service.dto.ZipAreaDTO;
import com.mv1.iaa.service.mapper.ZipAreaMapper;
import com.mv1.iaa.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the ZipAreaResource REST controller.
 *
 * @see ZipAreaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IaaApp.class)
public class ZipAreaResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ZIP = "AAAAAAAAAA";
    private static final String UPDATED_ZIP = "BBBBBBBBBB";

    @Autowired
    private ZipAreaRepository zipAreaRepository;

    @Autowired
    private ZipAreaMapper zipAreaMapper;
    
    @Autowired
    private ZipAreaService zipAreaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restZipAreaMockMvc;

    private ZipArea zipArea;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ZipAreaResource zipAreaResource = new ZipAreaResource(zipAreaService);
        this.restZipAreaMockMvc = MockMvcBuilders.standaloneSetup(zipAreaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ZipArea createEntity(EntityManager em) {
        ZipArea zipArea = new ZipArea()
            .name(DEFAULT_NAME)
            .zip(DEFAULT_ZIP);
        return zipArea;
    }

    @Before
    public void initTest() {
        zipArea = createEntity(em);
    }

    @Test
    @Transactional
    public void createZipArea() throws Exception {
        int databaseSizeBeforeCreate = zipAreaRepository.findAll().size();

        // Create the ZipArea
        ZipAreaDTO zipAreaDTO = zipAreaMapper.toDto(zipArea);
        restZipAreaMockMvc.perform(post("/api/zip-areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zipAreaDTO)))
            .andExpect(status().isCreated());

        // Validate the ZipArea in the database
        List<ZipArea> zipAreaList = zipAreaRepository.findAll();
        assertThat(zipAreaList).hasSize(databaseSizeBeforeCreate + 1);
        ZipArea testZipArea = zipAreaList.get(zipAreaList.size() - 1);
        assertThat(testZipArea.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testZipArea.getZip()).isEqualTo(DEFAULT_ZIP);
    }

    @Test
    @Transactional
    public void createZipAreaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = zipAreaRepository.findAll().size();

        // Create the ZipArea with an existing ID
        zipArea.setId(1L);
        ZipAreaDTO zipAreaDTO = zipAreaMapper.toDto(zipArea);

        // An entity with an existing ID cannot be created, so this API call must fail
        restZipAreaMockMvc.perform(post("/api/zip-areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zipAreaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ZipArea in the database
        List<ZipArea> zipAreaList = zipAreaRepository.findAll();
        assertThat(zipAreaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllZipAreas() throws Exception {
        // Initialize the database
        zipAreaRepository.saveAndFlush(zipArea);

        // Get all the zipAreaList
        restZipAreaMockMvc.perform(get("/api/zip-areas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zipArea.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].zip").value(hasItem(DEFAULT_ZIP.toString())));
    }
    
    @Test
    @Transactional
    public void getZipArea() throws Exception {
        // Initialize the database
        zipAreaRepository.saveAndFlush(zipArea);

        // Get the zipArea
        restZipAreaMockMvc.perform(get("/api/zip-areas/{id}", zipArea.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(zipArea.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.zip").value(DEFAULT_ZIP.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingZipArea() throws Exception {
        // Get the zipArea
        restZipAreaMockMvc.perform(get("/api/zip-areas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateZipArea() throws Exception {
        // Initialize the database
        zipAreaRepository.saveAndFlush(zipArea);

        int databaseSizeBeforeUpdate = zipAreaRepository.findAll().size();

        // Update the zipArea
        ZipArea updatedZipArea = zipAreaRepository.findById(zipArea.getId()).get();
        // Disconnect from session so that the updates on updatedZipArea are not directly saved in db
        em.detach(updatedZipArea);
        updatedZipArea
            .name(UPDATED_NAME)
            .zip(UPDATED_ZIP);
        ZipAreaDTO zipAreaDTO = zipAreaMapper.toDto(updatedZipArea);

        restZipAreaMockMvc.perform(put("/api/zip-areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zipAreaDTO)))
            .andExpect(status().isOk());

        // Validate the ZipArea in the database
        List<ZipArea> zipAreaList = zipAreaRepository.findAll();
        assertThat(zipAreaList).hasSize(databaseSizeBeforeUpdate);
        ZipArea testZipArea = zipAreaList.get(zipAreaList.size() - 1);
        assertThat(testZipArea.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testZipArea.getZip()).isEqualTo(UPDATED_ZIP);
    }

    @Test
    @Transactional
    public void updateNonExistingZipArea() throws Exception {
        int databaseSizeBeforeUpdate = zipAreaRepository.findAll().size();

        // Create the ZipArea
        ZipAreaDTO zipAreaDTO = zipAreaMapper.toDto(zipArea);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restZipAreaMockMvc.perform(put("/api/zip-areas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(zipAreaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ZipArea in the database
        List<ZipArea> zipAreaList = zipAreaRepository.findAll();
        assertThat(zipAreaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteZipArea() throws Exception {
        // Initialize the database
        zipAreaRepository.saveAndFlush(zipArea);

        int databaseSizeBeforeDelete = zipAreaRepository.findAll().size();

        // Get the zipArea
        restZipAreaMockMvc.perform(delete("/api/zip-areas/{id}", zipArea.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ZipArea> zipAreaList = zipAreaRepository.findAll();
        assertThat(zipAreaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ZipArea.class);
        ZipArea zipArea1 = new ZipArea();
        zipArea1.setId(1L);
        ZipArea zipArea2 = new ZipArea();
        zipArea2.setId(zipArea1.getId());
        assertThat(zipArea1).isEqualTo(zipArea2);
        zipArea2.setId(2L);
        assertThat(zipArea1).isNotEqualTo(zipArea2);
        zipArea1.setId(null);
        assertThat(zipArea1).isNotEqualTo(zipArea2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ZipAreaDTO.class);
        ZipAreaDTO zipAreaDTO1 = new ZipAreaDTO();
        zipAreaDTO1.setId(1L);
        ZipAreaDTO zipAreaDTO2 = new ZipAreaDTO();
        assertThat(zipAreaDTO1).isNotEqualTo(zipAreaDTO2);
        zipAreaDTO2.setId(zipAreaDTO1.getId());
        assertThat(zipAreaDTO1).isEqualTo(zipAreaDTO2);
        zipAreaDTO2.setId(2L);
        assertThat(zipAreaDTO1).isNotEqualTo(zipAreaDTO2);
        zipAreaDTO1.setId(null);
        assertThat(zipAreaDTO1).isNotEqualTo(zipAreaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(zipAreaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(zipAreaMapper.fromId(null)).isNull();
    }
}
