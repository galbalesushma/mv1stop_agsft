package com.mv1.iaa.web.rest;

import static com.mv1.iaa.web.rest.TestUtil.createFormattingConversionService;
import static com.mv1.iaa.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.IaaApp;
import com.mv1.iaa.domain.SearchLogs;
import com.mv1.iaa.repository.SearchLogsRepository;
import com.mv1.iaa.service.SearchLogsService;
import com.mv1.iaa.service.dto.SearchLogsDTO;
import com.mv1.iaa.service.mapper.SearchLogsMapper;
import com.mv1.iaa.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the SearchLogsResource REST controller.
 *
 * @see SearchLogsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IaaApp.class)
public class SearchLogsResourceIntTest {

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_ZIP = "AAAAAAAAAA";
    private static final String UPDATED_ZIP = "BBBBBBBBBB";

    private static final String DEFAULT_SEARCH_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_SEARCH_TYPE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_SEARCHED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SEARCHED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SearchLogsRepository searchLogsRepository;

    @Autowired
    private SearchLogsMapper searchLogsMapper;
    
    @Autowired
    private SearchLogsService searchLogsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSearchLogsMockMvc;

    private SearchLogs searchLogs;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SearchLogsResource searchLogsResource = new SearchLogsResource(searchLogsService);
        this.restSearchLogsMockMvc = MockMvcBuilders.standaloneSetup(searchLogsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SearchLogs createEntity(EntityManager em) {
        SearchLogs searchLogs = new SearchLogs()
            .fullName(DEFAULT_FULL_NAME)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .zip(DEFAULT_ZIP)
            .searchType(DEFAULT_SEARCH_TYPE)
            .searchedDate(DEFAULT_SEARCHED_DATE);
        return searchLogs;
    }

    @Before
    public void initTest() {
        searchLogs = createEntity(em);
    }

    @Test
    @Transactional
    public void createSearchLogs() throws Exception {
        int databaseSizeBeforeCreate = searchLogsRepository.findAll().size();

        // Create the SearchLogs
        SearchLogsDTO searchLogsDTO = searchLogsMapper.toDto(searchLogs);
        restSearchLogsMockMvc.perform(post("/api/search-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(searchLogsDTO)))
            .andExpect(status().isCreated());

        // Validate the SearchLogs in the database
        List<SearchLogs> searchLogsList = searchLogsRepository.findAll();
        assertThat(searchLogsList).hasSize(databaseSizeBeforeCreate + 1);
        SearchLogs testSearchLogs = searchLogsList.get(searchLogsList.size() - 1);
        assertThat(testSearchLogs.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
        assertThat(testSearchLogs.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testSearchLogs.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testSearchLogs.getZip()).isEqualTo(DEFAULT_ZIP);
        assertThat(testSearchLogs.getSearchType()).isEqualTo(DEFAULT_SEARCH_TYPE);
        assertThat(testSearchLogs.getSearchedDate()).isEqualTo(DEFAULT_SEARCHED_DATE);
    }

    @Test
    @Transactional
    public void createSearchLogsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = searchLogsRepository.findAll().size();

        // Create the SearchLogs with an existing ID
        searchLogs.setId(1L);
        SearchLogsDTO searchLogsDTO = searchLogsMapper.toDto(searchLogs);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSearchLogsMockMvc.perform(post("/api/search-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(searchLogsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SearchLogs in the database
        List<SearchLogs> searchLogsList = searchLogsRepository.findAll();
        assertThat(searchLogsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFullNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = searchLogsRepository.findAll().size();
        // set the field null
        searchLogs.setFullName(null);

        // Create the SearchLogs, which fails.
        SearchLogsDTO searchLogsDTO = searchLogsMapper.toDto(searchLogs);

        restSearchLogsMockMvc.perform(post("/api/search-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(searchLogsDTO)))
            .andExpect(status().isBadRequest());

        List<SearchLogs> searchLogsList = searchLogsRepository.findAll();
        assertThat(searchLogsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = searchLogsRepository.findAll().size();
        // set the field null
        searchLogs.setEmail(null);

        // Create the SearchLogs, which fails.
        SearchLogsDTO searchLogsDTO = searchLogsMapper.toDto(searchLogs);

        restSearchLogsMockMvc.perform(post("/api/search-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(searchLogsDTO)))
            .andExpect(status().isBadRequest());

        List<SearchLogs> searchLogsList = searchLogsRepository.findAll();
        assertThat(searchLogsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = searchLogsRepository.findAll().size();
        // set the field null
        searchLogs.setPhone(null);

        // Create the SearchLogs, which fails.
        SearchLogsDTO searchLogsDTO = searchLogsMapper.toDto(searchLogs);

        restSearchLogsMockMvc.perform(post("/api/search-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(searchLogsDTO)))
            .andExpect(status().isBadRequest());

        List<SearchLogs> searchLogsList = searchLogsRepository.findAll();
        assertThat(searchLogsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkZipIsRequired() throws Exception {
        int databaseSizeBeforeTest = searchLogsRepository.findAll().size();
        // set the field null
        searchLogs.setZip(null);

        // Create the SearchLogs, which fails.
        SearchLogsDTO searchLogsDTO = searchLogsMapper.toDto(searchLogs);

        restSearchLogsMockMvc.perform(post("/api/search-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(searchLogsDTO)))
            .andExpect(status().isBadRequest());

        List<SearchLogs> searchLogsList = searchLogsRepository.findAll();
        assertThat(searchLogsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSearchLogs() throws Exception {
        // Initialize the database
        searchLogsRepository.saveAndFlush(searchLogs);

        // Get all the searchLogsList
        restSearchLogsMockMvc.perform(get("/api/search-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(searchLogs.getId().intValue())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].zip").value(hasItem(DEFAULT_ZIP.toString())))
            .andExpect(jsonPath("$.[*].searchType").value(hasItem(DEFAULT_SEARCH_TYPE.toString())))
            .andExpect(jsonPath("$.[*].searchedDate").value(hasItem(sameInstant(DEFAULT_SEARCHED_DATE))));
    }
    
    @Test
    @Transactional
    public void getSearchLogs() throws Exception {
        // Initialize the database
        searchLogsRepository.saveAndFlush(searchLogs);

        // Get the searchLogs
        restSearchLogsMockMvc.perform(get("/api/search-logs/{id}", searchLogs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(searchLogs.getId().intValue()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.zip").value(DEFAULT_ZIP.toString()))
            .andExpect(jsonPath("$.searchType").value(DEFAULT_SEARCH_TYPE.toString()))
            .andExpect(jsonPath("$.searchedDate").value(sameInstant(DEFAULT_SEARCHED_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingSearchLogs() throws Exception {
        // Get the searchLogs
        restSearchLogsMockMvc.perform(get("/api/search-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSearchLogs() throws Exception {
        // Initialize the database
        searchLogsRepository.saveAndFlush(searchLogs);

        int databaseSizeBeforeUpdate = searchLogsRepository.findAll().size();

        // Update the searchLogs
        SearchLogs updatedSearchLogs = searchLogsRepository.findById(searchLogs.getId()).get();
        // Disconnect from session so that the updates on updatedSearchLogs are not directly saved in db
        em.detach(updatedSearchLogs);
        updatedSearchLogs
            .fullName(UPDATED_FULL_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .zip(UPDATED_ZIP)
            .searchType(UPDATED_SEARCH_TYPE)
            .searchedDate(UPDATED_SEARCHED_DATE);
        SearchLogsDTO searchLogsDTO = searchLogsMapper.toDto(updatedSearchLogs);

        restSearchLogsMockMvc.perform(put("/api/search-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(searchLogsDTO)))
            .andExpect(status().isOk());

        // Validate the SearchLogs in the database
        List<SearchLogs> searchLogsList = searchLogsRepository.findAll();
        assertThat(searchLogsList).hasSize(databaseSizeBeforeUpdate);
        SearchLogs testSearchLogs = searchLogsList.get(searchLogsList.size() - 1);
        assertThat(testSearchLogs.getFullName()).isEqualTo(UPDATED_FULL_NAME);
        assertThat(testSearchLogs.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testSearchLogs.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testSearchLogs.getZip()).isEqualTo(UPDATED_ZIP);
        assertThat(testSearchLogs.getSearchType()).isEqualTo(UPDATED_SEARCH_TYPE);
        assertThat(testSearchLogs.getSearchedDate()).isEqualTo(UPDATED_SEARCHED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingSearchLogs() throws Exception {
        int databaseSizeBeforeUpdate = searchLogsRepository.findAll().size();

        // Create the SearchLogs
        SearchLogsDTO searchLogsDTO = searchLogsMapper.toDto(searchLogs);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSearchLogsMockMvc.perform(put("/api/search-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(searchLogsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SearchLogs in the database
        List<SearchLogs> searchLogsList = searchLogsRepository.findAll();
        assertThat(searchLogsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSearchLogs() throws Exception {
        // Initialize the database
        searchLogsRepository.saveAndFlush(searchLogs);

        int databaseSizeBeforeDelete = searchLogsRepository.findAll().size();

        // Get the searchLogs
        restSearchLogsMockMvc.perform(delete("/api/search-logs/{id}", searchLogs.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SearchLogs> searchLogsList = searchLogsRepository.findAll();
        assertThat(searchLogsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SearchLogs.class);
        SearchLogs searchLogs1 = new SearchLogs();
        searchLogs1.setId(1L);
        SearchLogs searchLogs2 = new SearchLogs();
        searchLogs2.setId(searchLogs1.getId());
        assertThat(searchLogs1).isEqualTo(searchLogs2);
        searchLogs2.setId(2L);
        assertThat(searchLogs1).isNotEqualTo(searchLogs2);
        searchLogs1.setId(null);
        assertThat(searchLogs1).isNotEqualTo(searchLogs2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SearchLogsDTO.class);
        SearchLogsDTO searchLogsDTO1 = new SearchLogsDTO();
        searchLogsDTO1.setId(1L);
        SearchLogsDTO searchLogsDTO2 = new SearchLogsDTO();
        assertThat(searchLogsDTO1).isNotEqualTo(searchLogsDTO2);
        searchLogsDTO2.setId(searchLogsDTO1.getId());
        assertThat(searchLogsDTO1).isEqualTo(searchLogsDTO2);
        searchLogsDTO2.setId(2L);
        assertThat(searchLogsDTO1).isNotEqualTo(searchLogsDTO2);
        searchLogsDTO1.setId(null);
        assertThat(searchLogsDTO1).isNotEqualTo(searchLogsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(searchLogsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(searchLogsMapper.fromId(null)).isNull();
    }
}
