package com.mv1.iaa.web.rest;

import static com.mv1.iaa.web.rest.TestUtil.createFormattingConversionService;
import static com.mv1.iaa.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.IaaApp;
import com.mv1.iaa.domain.PackSubscription;
import com.mv1.iaa.repository.PackSubscriptionRepository;
import com.mv1.iaa.service.PackSubscriptionService;
import com.mv1.iaa.service.dto.PackSubscriptionDTO;
import com.mv1.iaa.service.mapper.PackSubscriptionMapper;
import com.mv1.iaa.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the PackSubscriptionResource REST controller.
 *
 * @see PackSubscriptionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IaaApp.class)
public class PackSubscriptionResourceIntTest {

    private static final Long DEFAULT_PACKAGE_ID = 1L;
    private static final Long UPDATED_PACKAGE_ID = 2L;

    private static final Long DEFAULT_ZIP_AREA_ID = 1L;
    private static final Long UPDATED_ZIP_AREA_ID = 2L;

    private static final Integer DEFAULT_SLOTS = 1;
    private static final Integer UPDATED_SLOTS = 2;

    private static final String DEFAULT_DURATION = "AAAAAAAAAA";
    private static final String UPDATED_DURATION = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_VALID_TILL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALID_TILL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    @Autowired
    private PackSubscriptionRepository packSubscriptionRepository;

    @Autowired
    private PackSubscriptionMapper packSubscriptionMapper;
    
    @Autowired
    private PackSubscriptionService packSubscriptionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPackSubscriptionMockMvc;

    private PackSubscription packSubscription;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PackSubscriptionResource packSubscriptionResource = new PackSubscriptionResource(packSubscriptionService);
        this.restPackSubscriptionMockMvc = MockMvcBuilders.standaloneSetup(packSubscriptionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PackSubscription createEntity(EntityManager em) {
        PackSubscription packSubscription = new PackSubscription()
            .packageId(DEFAULT_PACKAGE_ID)
            .zipAreaId(DEFAULT_ZIP_AREA_ID)
            .slots(DEFAULT_SLOTS)
            .duration(DEFAULT_DURATION)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .validTill(DEFAULT_VALID_TILL)
            .isActive(DEFAULT_IS_ACTIVE)
            .isDeleted(DEFAULT_IS_DELETED);
        return packSubscription;
    }

    @Before
    public void initTest() {
        packSubscription = createEntity(em);
    }

    @Test
    @Transactional
    public void createPackSubscription() throws Exception {
        int databaseSizeBeforeCreate = packSubscriptionRepository.findAll().size();

        // Create the PackSubscription
        PackSubscriptionDTO packSubscriptionDTO = packSubscriptionMapper.toDto(packSubscription);
        restPackSubscriptionMockMvc.perform(post("/api/pack-subscriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(packSubscriptionDTO)))
            .andExpect(status().isCreated());

        // Validate the PackSubscription in the database
        List<PackSubscription> packSubscriptionList = packSubscriptionRepository.findAll();
        assertThat(packSubscriptionList).hasSize(databaseSizeBeforeCreate + 1);
        PackSubscription testPackSubscription = packSubscriptionList.get(packSubscriptionList.size() - 1);
        assertThat(testPackSubscription.getPackageId()).isEqualTo(DEFAULT_PACKAGE_ID);
        assertThat(testPackSubscription.getZipAreaId()).isEqualTo(DEFAULT_ZIP_AREA_ID);
        assertThat(testPackSubscription.getSlots()).isEqualTo(DEFAULT_SLOTS);
        assertThat(testPackSubscription.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testPackSubscription.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPackSubscription.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testPackSubscription.getValidTill()).isEqualTo(DEFAULT_VALID_TILL);
        assertThat(testPackSubscription.isIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testPackSubscription.isIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createPackSubscriptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = packSubscriptionRepository.findAll().size();

        // Create the PackSubscription with an existing ID
        packSubscription.setId(1L);
        PackSubscriptionDTO packSubscriptionDTO = packSubscriptionMapper.toDto(packSubscription);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPackSubscriptionMockMvc.perform(post("/api/pack-subscriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(packSubscriptionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PackSubscription in the database
        List<PackSubscription> packSubscriptionList = packSubscriptionRepository.findAll();
        assertThat(packSubscriptionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPackSubscriptions() throws Exception {
        // Initialize the database
        packSubscriptionRepository.saveAndFlush(packSubscription);

        // Get all the packSubscriptionList
        restPackSubscriptionMockMvc.perform(get("/api/pack-subscriptions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(packSubscription.getId().intValue())))
            .andExpect(jsonPath("$.[*].packageId").value(hasItem(DEFAULT_PACKAGE_ID.intValue())))
            .andExpect(jsonPath("$.[*].zipAreaId").value(hasItem(DEFAULT_ZIP_AREA_ID.intValue())))
            .andExpect(jsonPath("$.[*].slots").value(hasItem(DEFAULT_SLOTS)))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].validTill").value(hasItem(sameInstant(DEFAULT_VALID_TILL))))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getPackSubscription() throws Exception {
        // Initialize the database
        packSubscriptionRepository.saveAndFlush(packSubscription);

        // Get the packSubscription
        restPackSubscriptionMockMvc.perform(get("/api/pack-subscriptions/{id}", packSubscription.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(packSubscription.getId().intValue()))
            .andExpect(jsonPath("$.packageId").value(DEFAULT_PACKAGE_ID.intValue()))
            .andExpect(jsonPath("$.zipAreaId").value(DEFAULT_ZIP_AREA_ID.intValue()))
            .andExpect(jsonPath("$.slots").value(DEFAULT_SLOTS))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION.toString()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.validTill").value(sameInstant(DEFAULT_VALID_TILL)))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPackSubscription() throws Exception {
        // Get the packSubscription
        restPackSubscriptionMockMvc.perform(get("/api/pack-subscriptions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePackSubscription() throws Exception {
        // Initialize the database
        packSubscriptionRepository.saveAndFlush(packSubscription);

        int databaseSizeBeforeUpdate = packSubscriptionRepository.findAll().size();

        // Update the packSubscription
        PackSubscription updatedPackSubscription = packSubscriptionRepository.findById(packSubscription.getId()).get();
        // Disconnect from session so that the updates on updatedPackSubscription are not directly saved in db
        em.detach(updatedPackSubscription);
        updatedPackSubscription
            .packageId(UPDATED_PACKAGE_ID)
            .zipAreaId(UPDATED_ZIP_AREA_ID)
            .slots(UPDATED_SLOTS)
            .duration(UPDATED_DURATION)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .validTill(UPDATED_VALID_TILL)
            .isActive(UPDATED_IS_ACTIVE)
            .isDeleted(UPDATED_IS_DELETED);
        PackSubscriptionDTO packSubscriptionDTO = packSubscriptionMapper.toDto(updatedPackSubscription);

        restPackSubscriptionMockMvc.perform(put("/api/pack-subscriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(packSubscriptionDTO)))
            .andExpect(status().isOk());

        // Validate the PackSubscription in the database
        List<PackSubscription> packSubscriptionList = packSubscriptionRepository.findAll();
        assertThat(packSubscriptionList).hasSize(databaseSizeBeforeUpdate);
        PackSubscription testPackSubscription = packSubscriptionList.get(packSubscriptionList.size() - 1);
        assertThat(testPackSubscription.getPackageId()).isEqualTo(UPDATED_PACKAGE_ID);
        assertThat(testPackSubscription.getZipAreaId()).isEqualTo(UPDATED_ZIP_AREA_ID);
        assertThat(testPackSubscription.getSlots()).isEqualTo(UPDATED_SLOTS);
        assertThat(testPackSubscription.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testPackSubscription.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPackSubscription.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testPackSubscription.getValidTill()).isEqualTo(UPDATED_VALID_TILL);
        assertThat(testPackSubscription.isIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testPackSubscription.isIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingPackSubscription() throws Exception {
        int databaseSizeBeforeUpdate = packSubscriptionRepository.findAll().size();

        // Create the PackSubscription
        PackSubscriptionDTO packSubscriptionDTO = packSubscriptionMapper.toDto(packSubscription);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPackSubscriptionMockMvc.perform(put("/api/pack-subscriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(packSubscriptionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PackSubscription in the database
        List<PackSubscription> packSubscriptionList = packSubscriptionRepository.findAll();
        assertThat(packSubscriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePackSubscription() throws Exception {
        // Initialize the database
        packSubscriptionRepository.saveAndFlush(packSubscription);

        int databaseSizeBeforeDelete = packSubscriptionRepository.findAll().size();

        // Get the packSubscription
        restPackSubscriptionMockMvc.perform(delete("/api/pack-subscriptions/{id}", packSubscription.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PackSubscription> packSubscriptionList = packSubscriptionRepository.findAll();
        assertThat(packSubscriptionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PackSubscription.class);
        PackSubscription packSubscription1 = new PackSubscription();
        packSubscription1.setId(1L);
        PackSubscription packSubscription2 = new PackSubscription();
        packSubscription2.setId(packSubscription1.getId());
        assertThat(packSubscription1).isEqualTo(packSubscription2);
        packSubscription2.setId(2L);
        assertThat(packSubscription1).isNotEqualTo(packSubscription2);
        packSubscription1.setId(null);
        assertThat(packSubscription1).isNotEqualTo(packSubscription2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PackSubscriptionDTO.class);
        PackSubscriptionDTO packSubscriptionDTO1 = new PackSubscriptionDTO();
        packSubscriptionDTO1.setId(1L);
        PackSubscriptionDTO packSubscriptionDTO2 = new PackSubscriptionDTO();
        assertThat(packSubscriptionDTO1).isNotEqualTo(packSubscriptionDTO2);
        packSubscriptionDTO2.setId(packSubscriptionDTO1.getId());
        assertThat(packSubscriptionDTO1).isEqualTo(packSubscriptionDTO2);
        packSubscriptionDTO2.setId(2L);
        assertThat(packSubscriptionDTO1).isNotEqualTo(packSubscriptionDTO2);
        packSubscriptionDTO1.setId(null);
        assertThat(packSubscriptionDTO1).isNotEqualTo(packSubscriptionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(packSubscriptionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(packSubscriptionMapper.fromId(null)).isNull();
    }
}
