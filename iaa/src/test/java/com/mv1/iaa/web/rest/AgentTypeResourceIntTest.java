package com.mv1.iaa.web.rest;

import static com.mv1.iaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.IaaApp;
import com.mv1.iaa.domain.AgentType;
import com.mv1.iaa.repository.AgentTypeRepository;
import com.mv1.iaa.service.AgentTypeService;
import com.mv1.iaa.service.dto.AgentTypeDTO;
import com.mv1.iaa.service.mapper.AgentTypeMapper;
import com.mv1.iaa.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AgentTypeResource REST controller.
 *
 * @see AgentTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IaaApp.class)
public class AgentTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_ZIP_AREA_ID = 1L;
    private static final Long UPDATED_ZIP_AREA_ID = 2L;

    @Autowired
    private AgentTypeRepository agentTypeRepository;

    @Autowired
    private AgentTypeMapper agentTypeMapper;
    
    @Autowired
    private AgentTypeService agentTypeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAgentTypeMockMvc;

    private AgentType agentType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AgentTypeResource agentTypeResource = new AgentTypeResource(agentTypeService);
        this.restAgentTypeMockMvc = MockMvcBuilders.standaloneSetup(agentTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentType createEntity(EntityManager em) {
        AgentType agentType = new AgentType()
            .name(DEFAULT_NAME)
            .zipAreaId(DEFAULT_ZIP_AREA_ID);
        return agentType;
    }

    @Before
    public void initTest() {
        agentType = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgentType() throws Exception {
        int databaseSizeBeforeCreate = agentTypeRepository.findAll().size();

        // Create the AgentType
        AgentTypeDTO agentTypeDTO = agentTypeMapper.toDto(agentType);
        restAgentTypeMockMvc.perform(post("/api/agent-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the AgentType in the database
        List<AgentType> agentTypeList = agentTypeRepository.findAll();
        assertThat(agentTypeList).hasSize(databaseSizeBeforeCreate + 1);
        AgentType testAgentType = agentTypeList.get(agentTypeList.size() - 1);
        assertThat(testAgentType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAgentType.getZipAreaId()).isEqualTo(DEFAULT_ZIP_AREA_ID);
    }

    @Test
    @Transactional
    public void createAgentTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agentTypeRepository.findAll().size();

        // Create the AgentType with an existing ID
        agentType.setId(1L);
        AgentTypeDTO agentTypeDTO = agentTypeMapper.toDto(agentType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgentTypeMockMvc.perform(post("/api/agent-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AgentType in the database
        List<AgentType> agentTypeList = agentTypeRepository.findAll();
        assertThat(agentTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAgentTypes() throws Exception {
        // Initialize the database
        agentTypeRepository.saveAndFlush(agentType);

        // Get all the agentTypeList
        restAgentTypeMockMvc.perform(get("/api/agent-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agentType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].zipAreaId").value(hasItem(DEFAULT_ZIP_AREA_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getAgentType() throws Exception {
        // Initialize the database
        agentTypeRepository.saveAndFlush(agentType);

        // Get the agentType
        restAgentTypeMockMvc.perform(get("/api/agent-types/{id}", agentType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(agentType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.zipAreaId").value(DEFAULT_ZIP_AREA_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingAgentType() throws Exception {
        // Get the agentType
        restAgentTypeMockMvc.perform(get("/api/agent-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgentType() throws Exception {
        // Initialize the database
        agentTypeRepository.saveAndFlush(agentType);

        int databaseSizeBeforeUpdate = agentTypeRepository.findAll().size();

        // Update the agentType
        AgentType updatedAgentType = agentTypeRepository.findById(agentType.getId()).get();
        // Disconnect from session so that the updates on updatedAgentType are not directly saved in db
        em.detach(updatedAgentType);
        updatedAgentType
            .name(UPDATED_NAME)
            .zipAreaId(UPDATED_ZIP_AREA_ID);
        AgentTypeDTO agentTypeDTO = agentTypeMapper.toDto(updatedAgentType);

        restAgentTypeMockMvc.perform(put("/api/agent-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentTypeDTO)))
            .andExpect(status().isOk());

        // Validate the AgentType in the database
        List<AgentType> agentTypeList = agentTypeRepository.findAll();
        assertThat(agentTypeList).hasSize(databaseSizeBeforeUpdate);
        AgentType testAgentType = agentTypeList.get(agentTypeList.size() - 1);
        assertThat(testAgentType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAgentType.getZipAreaId()).isEqualTo(UPDATED_ZIP_AREA_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingAgentType() throws Exception {
        int databaseSizeBeforeUpdate = agentTypeRepository.findAll().size();

        // Create the AgentType
        AgentTypeDTO agentTypeDTO = agentTypeMapper.toDto(agentType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgentTypeMockMvc.perform(put("/api/agent-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AgentType in the database
        List<AgentType> agentTypeList = agentTypeRepository.findAll();
        assertThat(agentTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAgentType() throws Exception {
        // Initialize the database
        agentTypeRepository.saveAndFlush(agentType);

        int databaseSizeBeforeDelete = agentTypeRepository.findAll().size();

        // Get the agentType
        restAgentTypeMockMvc.perform(delete("/api/agent-types/{id}", agentType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AgentType> agentTypeList = agentTypeRepository.findAll();
        assertThat(agentTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentType.class);
        AgentType agentType1 = new AgentType();
        agentType1.setId(1L);
        AgentType agentType2 = new AgentType();
        agentType2.setId(agentType1.getId());
        assertThat(agentType1).isEqualTo(agentType2);
        agentType2.setId(2L);
        assertThat(agentType1).isNotEqualTo(agentType2);
        agentType1.setId(null);
        assertThat(agentType1).isNotEqualTo(agentType2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentTypeDTO.class);
        AgentTypeDTO agentTypeDTO1 = new AgentTypeDTO();
        agentTypeDTO1.setId(1L);
        AgentTypeDTO agentTypeDTO2 = new AgentTypeDTO();
        assertThat(agentTypeDTO1).isNotEqualTo(agentTypeDTO2);
        agentTypeDTO2.setId(agentTypeDTO1.getId());
        assertThat(agentTypeDTO1).isEqualTo(agentTypeDTO2);
        agentTypeDTO2.setId(2L);
        assertThat(agentTypeDTO1).isNotEqualTo(agentTypeDTO2);
        agentTypeDTO1.setId(null);
        assertThat(agentTypeDTO1).isNotEqualTo(agentTypeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(agentTypeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(agentTypeMapper.fromId(null)).isNull();
    }
}
