package com.mv1.iaa.web.rest;

import static com.mv1.iaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.IaaApp;
import com.mv1.iaa.domain.PricePackage;
import com.mv1.iaa.domain.enumeration.PageRank;
import com.mv1.iaa.repository.PricePackageRepository;
import com.mv1.iaa.service.PricePackageService;
import com.mv1.iaa.service.dto.PricePackageDTO;
import com.mv1.iaa.service.mapper.PricePackageMapper;
import com.mv1.iaa.web.rest.errors.ExceptionTranslator;
/**
 * Test class for the PricePackageResource REST controller.
 *
 * @see PricePackageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IaaApp.class)
public class PricePackageResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final PageRank DEFAULT_RANK = PageRank.ONE;
    private static final PageRank UPDATED_RANK = PageRank.TWO;

    private static final Long DEFAULT_PRICE = 1L;
    private static final Long UPDATED_PRICE = 2L;

    private static final String DEFAULT_DESC = "AAAAAAAAAA";
    private static final String UPDATED_DESC = "BBBBBBBBBB";

    @Autowired
    private PricePackageRepository pricePackageRepository;

    @Autowired
    private PricePackageMapper pricePackageMapper;
    
    @Autowired
    private PricePackageService pricePackageService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPricePackageMockMvc;

    private PricePackage pricePackage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PricePackageResource pricePackageResource = new PricePackageResource(pricePackageService);
        this.restPricePackageMockMvc = MockMvcBuilders.standaloneSetup(pricePackageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PricePackage createEntity(EntityManager em) {
        PricePackage pricePackage = new PricePackage()
            .name(DEFAULT_NAME)
            .rank(DEFAULT_RANK)
            .price(DEFAULT_PRICE)
            .desc(DEFAULT_DESC);
        return pricePackage;
    }

    @Before
    public void initTest() {
        pricePackage = createEntity(em);
    }

    @Test
    @Transactional
    public void createPricePackage() throws Exception {
        int databaseSizeBeforeCreate = pricePackageRepository.findAll().size();

        // Create the PricePackage
        PricePackageDTO pricePackageDTO = pricePackageMapper.toDto(pricePackage);
        restPricePackageMockMvc.perform(post("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackageDTO)))
            .andExpect(status().isCreated());

        // Validate the PricePackage in the database
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeCreate + 1);
        PricePackage testPricePackage = pricePackageList.get(pricePackageList.size() - 1);
        assertThat(testPricePackage.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPricePackage.getRank()).isEqualTo(DEFAULT_RANK);
        assertThat(testPricePackage.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testPricePackage.getDesc()).isEqualTo(DEFAULT_DESC);
    }

    @Test
    @Transactional
    public void createPricePackageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pricePackageRepository.findAll().size();

        // Create the PricePackage with an existing ID
        pricePackage.setId(1L);
        PricePackageDTO pricePackageDTO = pricePackageMapper.toDto(pricePackage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPricePackageMockMvc.perform(post("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PricePackage in the database
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPricePackages() throws Exception {
        // Initialize the database
        pricePackageRepository.saveAndFlush(pricePackage);

        // Get all the pricePackageList
        restPricePackageMockMvc.perform(get("/api/price-packages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pricePackage.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].rank").value(hasItem(DEFAULT_RANK.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].desc").value(hasItem(DEFAULT_DESC.toString())));
    }
    
    @Test
    @Transactional
    public void getPricePackage() throws Exception {
        // Initialize the database
        pricePackageRepository.saveAndFlush(pricePackage);

        // Get the pricePackage
        restPricePackageMockMvc.perform(get("/api/price-packages/{id}", pricePackage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pricePackage.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.rank").value(DEFAULT_RANK.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()))
            .andExpect(jsonPath("$.desc").value(DEFAULT_DESC.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPricePackage() throws Exception {
        // Get the pricePackage
        restPricePackageMockMvc.perform(get("/api/price-packages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePricePackage() throws Exception {
        // Initialize the database
        pricePackageRepository.saveAndFlush(pricePackage);

        int databaseSizeBeforeUpdate = pricePackageRepository.findAll().size();

        // Update the pricePackage
        PricePackage updatedPricePackage = pricePackageRepository.findById(pricePackage.getId()).get();
        // Disconnect from session so that the updates on updatedPricePackage are not directly saved in db
        em.detach(updatedPricePackage);
        updatedPricePackage
            .name(UPDATED_NAME)
            .rank(UPDATED_RANK)
            .price(UPDATED_PRICE)
            .desc(UPDATED_DESC);
        PricePackageDTO pricePackageDTO = pricePackageMapper.toDto(updatedPricePackage);

        restPricePackageMockMvc.perform(put("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackageDTO)))
            .andExpect(status().isOk());

        // Validate the PricePackage in the database
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeUpdate);
        PricePackage testPricePackage = pricePackageList.get(pricePackageList.size() - 1);
        assertThat(testPricePackage.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPricePackage.getRank()).isEqualTo(UPDATED_RANK);
        assertThat(testPricePackage.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testPricePackage.getDesc()).isEqualTo(UPDATED_DESC);
    }

    @Test
    @Transactional
    public void updateNonExistingPricePackage() throws Exception {
        int databaseSizeBeforeUpdate = pricePackageRepository.findAll().size();

        // Create the PricePackage
        PricePackageDTO pricePackageDTO = pricePackageMapper.toDto(pricePackage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPricePackageMockMvc.perform(put("/api/price-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pricePackageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PricePackage in the database
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePricePackage() throws Exception {
        // Initialize the database
        pricePackageRepository.saveAndFlush(pricePackage);

        int databaseSizeBeforeDelete = pricePackageRepository.findAll().size();

        // Get the pricePackage
        restPricePackageMockMvc.perform(delete("/api/price-packages/{id}", pricePackage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PricePackage> pricePackageList = pricePackageRepository.findAll();
        assertThat(pricePackageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PricePackage.class);
        PricePackage pricePackage1 = new PricePackage();
        pricePackage1.setId(1L);
        PricePackage pricePackage2 = new PricePackage();
        pricePackage2.setId(pricePackage1.getId());
        assertThat(pricePackage1).isEqualTo(pricePackage2);
        pricePackage2.setId(2L);
        assertThat(pricePackage1).isNotEqualTo(pricePackage2);
        pricePackage1.setId(null);
        assertThat(pricePackage1).isNotEqualTo(pricePackage2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PricePackageDTO.class);
        PricePackageDTO pricePackageDTO1 = new PricePackageDTO();
        pricePackageDTO1.setId(1L);
        PricePackageDTO pricePackageDTO2 = new PricePackageDTO();
        assertThat(pricePackageDTO1).isNotEqualTo(pricePackageDTO2);
        pricePackageDTO2.setId(pricePackageDTO1.getId());
        assertThat(pricePackageDTO1).isEqualTo(pricePackageDTO2);
        pricePackageDTO2.setId(2L);
        assertThat(pricePackageDTO1).isNotEqualTo(pricePackageDTO2);
        pricePackageDTO1.setId(null);
        assertThat(pricePackageDTO1).isNotEqualTo(pricePackageDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pricePackageMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pricePackageMapper.fromId(null)).isNull();
    }
}
