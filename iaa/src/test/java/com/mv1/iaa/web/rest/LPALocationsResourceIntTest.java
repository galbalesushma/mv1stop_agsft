package com.mv1.iaa.web.rest;

import com.mv1.iaa.IaaApp;

import com.mv1.iaa.domain.LPALocations;
import com.mv1.iaa.repository.LPALocationsRepository;
import com.mv1.iaa.service.LPALocationsService;
import com.mv1.iaa.service.dto.LPALocationsDTO;
import com.mv1.iaa.service.mapper.LPALocationsMapper;
import com.mv1.iaa.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mv1.iaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LPALocationsResource REST controller.
 *
 * @see LPALocationsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IaaApp.class)
public class LPALocationsResourceIntTest {

    private static final String DEFAULT_BRANCH = "AAAAAAAAAA";
    private static final String UPDATED_BRANCH = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_MAIN_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_MAIN_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_FAX_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_FAX_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_WEEKDAYS_HRS = "AAAAAAAAAA";
    private static final String UPDATED_WEEKDAYS_HRS = "BBBBBBBBBB";

    private static final String DEFAULT_SATURDAY_HRS = "AAAAAAAAAA";
    private static final String UPDATED_SATURDAY_HRS = "BBBBBBBBBB";

    private static final String DEFAULT_DIRECTIONS = "AAAAAAAAAA";
    private static final String UPDATED_DIRECTIONS = "BBBBBBBBBB";

    @Autowired
    private LPALocationsRepository lPALocationsRepository;

    @Autowired
    private LPALocationsMapper lPALocationsMapper;

    @Autowired
    private LPALocationsService lPALocationsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLPALocationsMockMvc;

    private LPALocations lPALocations;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LPALocationsResource lPALocationsResource = new LPALocationsResource(lPALocationsService);
        this.restLPALocationsMockMvc = MockMvcBuilders.standaloneSetup(lPALocationsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LPALocations createEntity(EntityManager em) {
        LPALocations lPALocations = new LPALocations()
            .branch(DEFAULT_BRANCH)
            .address(DEFAULT_ADDRESS)
            .city(DEFAULT_CITY)
            .state(DEFAULT_STATE)
            .postalCode(DEFAULT_POSTAL_CODE)
            .country(DEFAULT_COUNTRY)
            .mainPhone(DEFAULT_MAIN_PHONE)
            .faxNumber(DEFAULT_FAX_NUMBER)
            .weekdaysHrs(DEFAULT_WEEKDAYS_HRS)
            .saturdayHrs(DEFAULT_SATURDAY_HRS)
            .directions(DEFAULT_DIRECTIONS);
        return lPALocations;
    }

    @Before
    public void initTest() {
        lPALocations = createEntity(em);
    }

    @Test
    @Transactional
    public void createLPALocations() throws Exception {
        int databaseSizeBeforeCreate = lPALocationsRepository.findAll().size();

        // Create the LPALocations
        LPALocationsDTO lPALocationsDTO = lPALocationsMapper.toDto(lPALocations);
        restLPALocationsMockMvc.perform(post("/api/lpa-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lPALocationsDTO)))
            .andExpect(status().isCreated());

        // Validate the LPALocations in the database
        List<LPALocations> lPALocationsList = lPALocationsRepository.findAll();
        assertThat(lPALocationsList).hasSize(databaseSizeBeforeCreate + 1);
        LPALocations testLPALocations = lPALocationsList.get(lPALocationsList.size() - 1);
        assertThat(testLPALocations.getBranch()).isEqualTo(DEFAULT_BRANCH);
        assertThat(testLPALocations.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testLPALocations.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testLPALocations.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testLPALocations.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testLPALocations.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testLPALocations.getMainPhone()).isEqualTo(DEFAULT_MAIN_PHONE);
        assertThat(testLPALocations.getFaxNumber()).isEqualTo(DEFAULT_FAX_NUMBER);
        assertThat(testLPALocations.getWeekdaysHrs()).isEqualTo(DEFAULT_WEEKDAYS_HRS);
        assertThat(testLPALocations.getSaturdayHrs()).isEqualTo(DEFAULT_SATURDAY_HRS);
        assertThat(testLPALocations.getDirections()).isEqualTo(DEFAULT_DIRECTIONS);
    }

    @Test
    @Transactional
    public void createLPALocationsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lPALocationsRepository.findAll().size();

        // Create the LPALocations with an existing ID
        lPALocations.setId(1L);
        LPALocationsDTO lPALocationsDTO = lPALocationsMapper.toDto(lPALocations);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLPALocationsMockMvc.perform(post("/api/lpa-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lPALocationsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LPALocations in the database
        List<LPALocations> lPALocationsList = lPALocationsRepository.findAll();
        assertThat(lPALocationsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLPALocations() throws Exception {
        // Initialize the database
        lPALocationsRepository.saveAndFlush(lPALocations);

        // Get all the lPALocationsList
        restLPALocationsMockMvc.perform(get("/api/lpa-locations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lPALocations.getId().intValue())))
            .andExpect(jsonPath("$.[*].branch").value(hasItem(DEFAULT_BRANCH.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].mainPhone").value(hasItem(DEFAULT_MAIN_PHONE.toString())))
            .andExpect(jsonPath("$.[*].faxNumber").value(hasItem(DEFAULT_FAX_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].weekdaysHrs").value(hasItem(DEFAULT_WEEKDAYS_HRS.toString())))
            .andExpect(jsonPath("$.[*].saturdayHrs").value(hasItem(DEFAULT_SATURDAY_HRS.toString())))
            .andExpect(jsonPath("$.[*].directions").value(hasItem(DEFAULT_DIRECTIONS.toString())));
    }
    
    @Test
    @Transactional
    public void getLPALocations() throws Exception {
        // Initialize the database
        lPALocationsRepository.saveAndFlush(lPALocations);

        // Get the lPALocations
        restLPALocationsMockMvc.perform(get("/api/lpa-locations/{id}", lPALocations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(lPALocations.getId().intValue()))
            .andExpect(jsonPath("$.branch").value(DEFAULT_BRANCH.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.mainPhone").value(DEFAULT_MAIN_PHONE.toString()))
            .andExpect(jsonPath("$.faxNumber").value(DEFAULT_FAX_NUMBER.toString()))
            .andExpect(jsonPath("$.weekdaysHrs").value(DEFAULT_WEEKDAYS_HRS.toString()))
            .andExpect(jsonPath("$.saturdayHrs").value(DEFAULT_SATURDAY_HRS.toString()))
            .andExpect(jsonPath("$.directions").value(DEFAULT_DIRECTIONS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingLPALocations() throws Exception {
        // Get the lPALocations
        restLPALocationsMockMvc.perform(get("/api/lpa-locations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLPALocations() throws Exception {
        // Initialize the database
        lPALocationsRepository.saveAndFlush(lPALocations);

        int databaseSizeBeforeUpdate = lPALocationsRepository.findAll().size();

        // Update the lPALocations
        LPALocations updatedLPALocations = lPALocationsRepository.findById(lPALocations.getId()).get();
        // Disconnect from session so that the updates on updatedLPALocations are not directly saved in db
        em.detach(updatedLPALocations);
        updatedLPALocations
            .branch(UPDATED_BRANCH)
            .address(UPDATED_ADDRESS)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .postalCode(UPDATED_POSTAL_CODE)
            .country(UPDATED_COUNTRY)
            .mainPhone(UPDATED_MAIN_PHONE)
            .faxNumber(UPDATED_FAX_NUMBER)
            .weekdaysHrs(UPDATED_WEEKDAYS_HRS)
            .saturdayHrs(UPDATED_SATURDAY_HRS)
            .directions(UPDATED_DIRECTIONS);
        LPALocationsDTO lPALocationsDTO = lPALocationsMapper.toDto(updatedLPALocations);

        restLPALocationsMockMvc.perform(put("/api/lpa-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lPALocationsDTO)))
            .andExpect(status().isOk());

        // Validate the LPALocations in the database
        List<LPALocations> lPALocationsList = lPALocationsRepository.findAll();
        assertThat(lPALocationsList).hasSize(databaseSizeBeforeUpdate);
        LPALocations testLPALocations = lPALocationsList.get(lPALocationsList.size() - 1);
        assertThat(testLPALocations.getBranch()).isEqualTo(UPDATED_BRANCH);
        assertThat(testLPALocations.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testLPALocations.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testLPALocations.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testLPALocations.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testLPALocations.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testLPALocations.getMainPhone()).isEqualTo(UPDATED_MAIN_PHONE);
        assertThat(testLPALocations.getFaxNumber()).isEqualTo(UPDATED_FAX_NUMBER);
        assertThat(testLPALocations.getWeekdaysHrs()).isEqualTo(UPDATED_WEEKDAYS_HRS);
        assertThat(testLPALocations.getSaturdayHrs()).isEqualTo(UPDATED_SATURDAY_HRS);
        assertThat(testLPALocations.getDirections()).isEqualTo(UPDATED_DIRECTIONS);
    }

    @Test
    @Transactional
    public void updateNonExistingLPALocations() throws Exception {
        int databaseSizeBeforeUpdate = lPALocationsRepository.findAll().size();

        // Create the LPALocations
        LPALocationsDTO lPALocationsDTO = lPALocationsMapper.toDto(lPALocations);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLPALocationsMockMvc.perform(put("/api/lpa-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lPALocationsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the LPALocations in the database
        List<LPALocations> lPALocationsList = lPALocationsRepository.findAll();
        assertThat(lPALocationsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLPALocations() throws Exception {
        // Initialize the database
        lPALocationsRepository.saveAndFlush(lPALocations);

        int databaseSizeBeforeDelete = lPALocationsRepository.findAll().size();

        // Get the lPALocations
        restLPALocationsMockMvc.perform(delete("/api/lpa-locations/{id}", lPALocations.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LPALocations> lPALocationsList = lPALocationsRepository.findAll();
        assertThat(lPALocationsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LPALocations.class);
        LPALocations lPALocations1 = new LPALocations();
        lPALocations1.setId(1L);
        LPALocations lPALocations2 = new LPALocations();
        lPALocations2.setId(lPALocations1.getId());
        assertThat(lPALocations1).isEqualTo(lPALocations2);
        lPALocations2.setId(2L);
        assertThat(lPALocations1).isNotEqualTo(lPALocations2);
        lPALocations1.setId(null);
        assertThat(lPALocations1).isNotEqualTo(lPALocations2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LPALocationsDTO.class);
        LPALocationsDTO lPALocationsDTO1 = new LPALocationsDTO();
        lPALocationsDTO1.setId(1L);
        LPALocationsDTO lPALocationsDTO2 = new LPALocationsDTO();
        assertThat(lPALocationsDTO1).isNotEqualTo(lPALocationsDTO2);
        lPALocationsDTO2.setId(lPALocationsDTO1.getId());
        assertThat(lPALocationsDTO1).isEqualTo(lPALocationsDTO2);
        lPALocationsDTO2.setId(2L);
        assertThat(lPALocationsDTO1).isNotEqualTo(lPALocationsDTO2);
        lPALocationsDTO1.setId(null);
        assertThat(lPALocationsDTO1).isNotEqualTo(lPALocationsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(lPALocationsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(lPALocationsMapper.fromId(null)).isNull();
    }
}
