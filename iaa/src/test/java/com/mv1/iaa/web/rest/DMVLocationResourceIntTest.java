package com.mv1.iaa.web.rest;

import com.mv1.iaa.IaaApp;

import com.mv1.iaa.domain.DMVLocation;
import com.mv1.iaa.repository.DMVLocationRepository;
import com.mv1.iaa.service.DMVLocationService;
import com.mv1.iaa.service.dto.DMVLocationDTO;
import com.mv1.iaa.service.mapper.DMVLocationMapper;
import com.mv1.iaa.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mv1.iaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DMVLocationResource REST controller.
 *
 * @see DMVLocationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IaaApp.class)
public class DMVLocationResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_MAIN_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_MAIN_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_FAX_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_FAX_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_WEEKDAYS_HRS = "AAAAAAAAAA";
    private static final String UPDATED_WEEKDAYS_HRS = "BBBBBBBBBB";

    private static final String DEFAULT_SATURDAY_HRS = "AAAAAAAAAA";
    private static final String UPDATED_SATURDAY_HRS = "BBBBBBBBBB";

    private static final String DEFAULT_DIRECTIONS = "AAAAAAAAAA";
    private static final String UPDATED_DIRECTIONS = "BBBBBBBBBB";

    @Autowired
    private DMVLocationRepository dMVLocationRepository;

    @Autowired
    private DMVLocationMapper dMVLocationMapper;

    @Autowired
    private DMVLocationService dMVLocationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDMVLocationMockMvc;

    private DMVLocation dMVLocation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DMVLocationResource dMVLocationResource = new DMVLocationResource(dMVLocationService);
        this.restDMVLocationMockMvc = MockMvcBuilders.standaloneSetup(dMVLocationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DMVLocation createEntity(EntityManager em) {
        DMVLocation dMVLocation = new DMVLocation()
            .name(DEFAULT_NAME)
            .address(DEFAULT_ADDRESS)
            .city(DEFAULT_CITY)
            .state(DEFAULT_STATE)
            .postalCode(DEFAULT_POSTAL_CODE)
            .country(DEFAULT_COUNTRY)
            .mainPhone(DEFAULT_MAIN_PHONE)
            .faxNumber(DEFAULT_FAX_NUMBER)
            .weekdaysHrs(DEFAULT_WEEKDAYS_HRS)
            .saturdayHrs(DEFAULT_SATURDAY_HRS)
            .directions(DEFAULT_DIRECTIONS);
        return dMVLocation;
    }

    @Before
    public void initTest() {
        dMVLocation = createEntity(em);
    }

    @Test
    @Transactional
    public void createDMVLocation() throws Exception {
        int databaseSizeBeforeCreate = dMVLocationRepository.findAll().size();

        // Create the DMVLocation
        DMVLocationDTO dMVLocationDTO = dMVLocationMapper.toDto(dMVLocation);
        restDMVLocationMockMvc.perform(post("/api/dmv-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dMVLocationDTO)))
            .andExpect(status().isCreated());

        // Validate the DMVLocation in the database
        List<DMVLocation> dMVLocationList = dMVLocationRepository.findAll();
        assertThat(dMVLocationList).hasSize(databaseSizeBeforeCreate + 1);
        DMVLocation testDMVLocation = dMVLocationList.get(dMVLocationList.size() - 1);
        assertThat(testDMVLocation.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDMVLocation.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testDMVLocation.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testDMVLocation.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testDMVLocation.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testDMVLocation.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testDMVLocation.getMainPhone()).isEqualTo(DEFAULT_MAIN_PHONE);
        assertThat(testDMVLocation.getFaxNumber()).isEqualTo(DEFAULT_FAX_NUMBER);
        assertThat(testDMVLocation.getWeekdaysHrs()).isEqualTo(DEFAULT_WEEKDAYS_HRS);
        assertThat(testDMVLocation.getSaturdayHrs()).isEqualTo(DEFAULT_SATURDAY_HRS);
        assertThat(testDMVLocation.getDirections()).isEqualTo(DEFAULT_DIRECTIONS);
    }

    @Test
    @Transactional
    public void createDMVLocationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dMVLocationRepository.findAll().size();

        // Create the DMVLocation with an existing ID
        dMVLocation.setId(1L);
        DMVLocationDTO dMVLocationDTO = dMVLocationMapper.toDto(dMVLocation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDMVLocationMockMvc.perform(post("/api/dmv-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dMVLocationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DMVLocation in the database
        List<DMVLocation> dMVLocationList = dMVLocationRepository.findAll();
        assertThat(dMVLocationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDMVLocations() throws Exception {
        // Initialize the database
        dMVLocationRepository.saveAndFlush(dMVLocation);

        // Get all the dMVLocationList
        restDMVLocationMockMvc.perform(get("/api/dmv-locations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dMVLocation.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].mainPhone").value(hasItem(DEFAULT_MAIN_PHONE.toString())))
            .andExpect(jsonPath("$.[*].faxNumber").value(hasItem(DEFAULT_FAX_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].weekdaysHrs").value(hasItem(DEFAULT_WEEKDAYS_HRS.toString())))
            .andExpect(jsonPath("$.[*].saturdayHrs").value(hasItem(DEFAULT_SATURDAY_HRS.toString())))
            .andExpect(jsonPath("$.[*].directions").value(hasItem(DEFAULT_DIRECTIONS.toString())));
    }
    
    @Test
    @Transactional
    public void getDMVLocation() throws Exception {
        // Initialize the database
        dMVLocationRepository.saveAndFlush(dMVLocation);

        // Get the dMVLocation
        restDMVLocationMockMvc.perform(get("/api/dmv-locations/{id}", dMVLocation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dMVLocation.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.mainPhone").value(DEFAULT_MAIN_PHONE.toString()))
            .andExpect(jsonPath("$.faxNumber").value(DEFAULT_FAX_NUMBER.toString()))
            .andExpect(jsonPath("$.weekdaysHrs").value(DEFAULT_WEEKDAYS_HRS.toString()))
            .andExpect(jsonPath("$.saturdayHrs").value(DEFAULT_SATURDAY_HRS.toString()))
            .andExpect(jsonPath("$.directions").value(DEFAULT_DIRECTIONS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDMVLocation() throws Exception {
        // Get the dMVLocation
        restDMVLocationMockMvc.perform(get("/api/dmv-locations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDMVLocation() throws Exception {
        // Initialize the database
        dMVLocationRepository.saveAndFlush(dMVLocation);

        int databaseSizeBeforeUpdate = dMVLocationRepository.findAll().size();

        // Update the dMVLocation
        DMVLocation updatedDMVLocation = dMVLocationRepository.findById(dMVLocation.getId()).get();
        // Disconnect from session so that the updates on updatedDMVLocation are not directly saved in db
        em.detach(updatedDMVLocation);
        updatedDMVLocation
            .name(UPDATED_NAME)
            .address(UPDATED_ADDRESS)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .postalCode(UPDATED_POSTAL_CODE)
            .country(UPDATED_COUNTRY)
            .mainPhone(UPDATED_MAIN_PHONE)
            .faxNumber(UPDATED_FAX_NUMBER)
            .weekdaysHrs(UPDATED_WEEKDAYS_HRS)
            .saturdayHrs(UPDATED_SATURDAY_HRS)
            .directions(UPDATED_DIRECTIONS);
        DMVLocationDTO dMVLocationDTO = dMVLocationMapper.toDto(updatedDMVLocation);

        restDMVLocationMockMvc.perform(put("/api/dmv-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dMVLocationDTO)))
            .andExpect(status().isOk());

        // Validate the DMVLocation in the database
        List<DMVLocation> dMVLocationList = dMVLocationRepository.findAll();
        assertThat(dMVLocationList).hasSize(databaseSizeBeforeUpdate);
        DMVLocation testDMVLocation = dMVLocationList.get(dMVLocationList.size() - 1);
        assertThat(testDMVLocation.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDMVLocation.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testDMVLocation.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testDMVLocation.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testDMVLocation.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testDMVLocation.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testDMVLocation.getMainPhone()).isEqualTo(UPDATED_MAIN_PHONE);
        assertThat(testDMVLocation.getFaxNumber()).isEqualTo(UPDATED_FAX_NUMBER);
        assertThat(testDMVLocation.getWeekdaysHrs()).isEqualTo(UPDATED_WEEKDAYS_HRS);
        assertThat(testDMVLocation.getSaturdayHrs()).isEqualTo(UPDATED_SATURDAY_HRS);
        assertThat(testDMVLocation.getDirections()).isEqualTo(UPDATED_DIRECTIONS);
    }

    @Test
    @Transactional
    public void updateNonExistingDMVLocation() throws Exception {
        int databaseSizeBeforeUpdate = dMVLocationRepository.findAll().size();

        // Create the DMVLocation
        DMVLocationDTO dMVLocationDTO = dMVLocationMapper.toDto(dMVLocation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDMVLocationMockMvc.perform(put("/api/dmv-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dMVLocationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DMVLocation in the database
        List<DMVLocation> dMVLocationList = dMVLocationRepository.findAll();
        assertThat(dMVLocationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDMVLocation() throws Exception {
        // Initialize the database
        dMVLocationRepository.saveAndFlush(dMVLocation);

        int databaseSizeBeforeDelete = dMVLocationRepository.findAll().size();

        // Get the dMVLocation
        restDMVLocationMockMvc.perform(delete("/api/dmv-locations/{id}", dMVLocation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DMVLocation> dMVLocationList = dMVLocationRepository.findAll();
        assertThat(dMVLocationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DMVLocation.class);
        DMVLocation dMVLocation1 = new DMVLocation();
        dMVLocation1.setId(1L);
        DMVLocation dMVLocation2 = new DMVLocation();
        dMVLocation2.setId(dMVLocation1.getId());
        assertThat(dMVLocation1).isEqualTo(dMVLocation2);
        dMVLocation2.setId(2L);
        assertThat(dMVLocation1).isNotEqualTo(dMVLocation2);
        dMVLocation1.setId(null);
        assertThat(dMVLocation1).isNotEqualTo(dMVLocation2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DMVLocationDTO.class);
        DMVLocationDTO dMVLocationDTO1 = new DMVLocationDTO();
        dMVLocationDTO1.setId(1L);
        DMVLocationDTO dMVLocationDTO2 = new DMVLocationDTO();
        assertThat(dMVLocationDTO1).isNotEqualTo(dMVLocationDTO2);
        dMVLocationDTO2.setId(dMVLocationDTO1.getId());
        assertThat(dMVLocationDTO1).isEqualTo(dMVLocationDTO2);
        dMVLocationDTO2.setId(2L);
        assertThat(dMVLocationDTO1).isNotEqualTo(dMVLocationDTO2);
        dMVLocationDTO1.setId(null);
        assertThat(dMVLocationDTO1).isNotEqualTo(dMVLocationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(dMVLocationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(dMVLocationMapper.fromId(null)).isNull();
    }
}
