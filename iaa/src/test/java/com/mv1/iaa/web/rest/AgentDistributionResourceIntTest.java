package com.mv1.iaa.web.rest;

import static com.mv1.iaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.IaaApp;
import com.mv1.iaa.domain.AgentDistribution;
import com.mv1.iaa.repository.AgentDistributionRepository;
import com.mv1.iaa.service.AgentDistributionService;
import com.mv1.iaa.service.dto.AgentDistributionDTO;
import com.mv1.iaa.service.mapper.AgentDistributionMapper;
import com.mv1.iaa.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AgentDistributionResource REST controller.
 *
 * @see AgentDistributionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IaaApp.class)
public class AgentDistributionResourceIntTest {

    private static final Integer DEFAULT_SLOT = 1;
    private static final Integer UPDATED_SLOT = 2;

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private AgentDistributionRepository agentDistributionRepository;

    @Autowired
    private AgentDistributionMapper agentDistributionMapper;
    
    @Autowired
    private AgentDistributionService agentDistributionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAgentDistributionMockMvc;

    private AgentDistribution agentDistribution;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AgentDistributionResource agentDistributionResource = new AgentDistributionResource(agentDistributionService);
        this.restAgentDistributionMockMvc = MockMvcBuilders.standaloneSetup(agentDistributionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentDistribution createEntity(EntityManager em) {
        AgentDistribution agentDistribution = new AgentDistribution()
            .slot(DEFAULT_SLOT)
            .type(DEFAULT_TYPE);
        return agentDistribution;
    }

    @Before
    public void initTest() {
        agentDistribution = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgentDistribution() throws Exception {
        int databaseSizeBeforeCreate = agentDistributionRepository.findAll().size();

        // Create the AgentDistribution
        AgentDistributionDTO agentDistributionDTO = agentDistributionMapper.toDto(agentDistribution);
        restAgentDistributionMockMvc.perform(post("/api/agent-distributions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentDistributionDTO)))
            .andExpect(status().isCreated());

        // Validate the AgentDistribution in the database
        List<AgentDistribution> agentDistributionList = agentDistributionRepository.findAll();
        assertThat(agentDistributionList).hasSize(databaseSizeBeforeCreate + 1);
        AgentDistribution testAgentDistribution = agentDistributionList.get(agentDistributionList.size() - 1);
        assertThat(testAgentDistribution.getSlot()).isEqualTo(DEFAULT_SLOT);
        assertThat(testAgentDistribution.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createAgentDistributionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agentDistributionRepository.findAll().size();

        // Create the AgentDistribution with an existing ID
        agentDistribution.setId(1L);
        AgentDistributionDTO agentDistributionDTO = agentDistributionMapper.toDto(agentDistribution);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgentDistributionMockMvc.perform(post("/api/agent-distributions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentDistributionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AgentDistribution in the database
        List<AgentDistribution> agentDistributionList = agentDistributionRepository.findAll();
        assertThat(agentDistributionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAgentDistributions() throws Exception {
        // Initialize the database
        agentDistributionRepository.saveAndFlush(agentDistribution);

        // Get all the agentDistributionList
        restAgentDistributionMockMvc.perform(get("/api/agent-distributions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agentDistribution.getId().intValue())))
            .andExpect(jsonPath("$.[*].slot").value(hasItem(DEFAULT_SLOT)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getAgentDistribution() throws Exception {
        // Initialize the database
        agentDistributionRepository.saveAndFlush(agentDistribution);

        // Get the agentDistribution
        restAgentDistributionMockMvc.perform(get("/api/agent-distributions/{id}", agentDistribution.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(agentDistribution.getId().intValue()))
            .andExpect(jsonPath("$.slot").value(DEFAULT_SLOT))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAgentDistribution() throws Exception {
        // Get the agentDistribution
        restAgentDistributionMockMvc.perform(get("/api/agent-distributions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgentDistribution() throws Exception {
        // Initialize the database
        agentDistributionRepository.saveAndFlush(agentDistribution);

        int databaseSizeBeforeUpdate = agentDistributionRepository.findAll().size();

        // Update the agentDistribution
        AgentDistribution updatedAgentDistribution = agentDistributionRepository.findById(agentDistribution.getId()).get();
        // Disconnect from session so that the updates on updatedAgentDistribution are not directly saved in db
        em.detach(updatedAgentDistribution);
        updatedAgentDistribution
            .slot(UPDATED_SLOT)
            .type(UPDATED_TYPE);
        AgentDistributionDTO agentDistributionDTO = agentDistributionMapper.toDto(updatedAgentDistribution);

        restAgentDistributionMockMvc.perform(put("/api/agent-distributions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentDistributionDTO)))
            .andExpect(status().isOk());

        // Validate the AgentDistribution in the database
        List<AgentDistribution> agentDistributionList = agentDistributionRepository.findAll();
        assertThat(agentDistributionList).hasSize(databaseSizeBeforeUpdate);
        AgentDistribution testAgentDistribution = agentDistributionList.get(agentDistributionList.size() - 1);
        assertThat(testAgentDistribution.getSlot()).isEqualTo(UPDATED_SLOT);
        assertThat(testAgentDistribution.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingAgentDistribution() throws Exception {
        int databaseSizeBeforeUpdate = agentDistributionRepository.findAll().size();

        // Create the AgentDistribution
        AgentDistributionDTO agentDistributionDTO = agentDistributionMapper.toDto(agentDistribution);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgentDistributionMockMvc.perform(put("/api/agent-distributions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentDistributionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AgentDistribution in the database
        List<AgentDistribution> agentDistributionList = agentDistributionRepository.findAll();
        assertThat(agentDistributionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAgentDistribution() throws Exception {
        // Initialize the database
        agentDistributionRepository.saveAndFlush(agentDistribution);

        int databaseSizeBeforeDelete = agentDistributionRepository.findAll().size();

        // Get the agentDistribution
        restAgentDistributionMockMvc.perform(delete("/api/agent-distributions/{id}", agentDistribution.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AgentDistribution> agentDistributionList = agentDistributionRepository.findAll();
        assertThat(agentDistributionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentDistribution.class);
        AgentDistribution agentDistribution1 = new AgentDistribution();
        agentDistribution1.setId(1L);
        AgentDistribution agentDistribution2 = new AgentDistribution();
        agentDistribution2.setId(agentDistribution1.getId());
        assertThat(agentDistribution1).isEqualTo(agentDistribution2);
        agentDistribution2.setId(2L);
        assertThat(agentDistribution1).isNotEqualTo(agentDistribution2);
        agentDistribution1.setId(null);
        assertThat(agentDistribution1).isNotEqualTo(agentDistribution2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentDistributionDTO.class);
        AgentDistributionDTO agentDistributionDTO1 = new AgentDistributionDTO();
        agentDistributionDTO1.setId(1L);
        AgentDistributionDTO agentDistributionDTO2 = new AgentDistributionDTO();
        assertThat(agentDistributionDTO1).isNotEqualTo(agentDistributionDTO2);
        agentDistributionDTO2.setId(agentDistributionDTO1.getId());
        assertThat(agentDistributionDTO1).isEqualTo(agentDistributionDTO2);
        agentDistributionDTO2.setId(2L);
        assertThat(agentDistributionDTO1).isNotEqualTo(agentDistributionDTO2);
        agentDistributionDTO1.setId(null);
        assertThat(agentDistributionDTO1).isNotEqualTo(agentDistributionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(agentDistributionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(agentDistributionMapper.fromId(null)).isNull();
    }
}
