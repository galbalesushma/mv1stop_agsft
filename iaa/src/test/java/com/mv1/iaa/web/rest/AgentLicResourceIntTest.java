package com.mv1.iaa.web.rest;

import static com.mv1.iaa.web.rest.TestUtil.createFormattingConversionService;
import static com.mv1.iaa.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.IaaApp;
import com.mv1.iaa.domain.AgentLic;
import com.mv1.iaa.repository.AgentLicRepository;
import com.mv1.iaa.service.AgentLicService;
import com.mv1.iaa.service.dto.AgentLicDTO;
import com.mv1.iaa.service.mapper.AgentLicMapper;
import com.mv1.iaa.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AgentLicResource REST controller.
 *
 * @see AgentLicResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IaaApp.class)
public class AgentLicResourceIntTest {

    private static final String DEFAULT_LIC_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_LIC_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_AUTHORITY = "AAAAAAAAAA";
    private static final String UPDATED_AUTHORITY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_FIRST_ACTIVE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FIRST_ACTIVE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_EFFECTIVE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EFFECTIVE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_EXPIRATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EXPIRATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final Boolean DEFAULT_IS_DELETED = false;
    private static final Boolean UPDATED_IS_DELETED = true;

    @Autowired
    private AgentLicRepository agentLicRepository;

    @Autowired
    private AgentLicMapper agentLicMapper;
    
    @Autowired
    private AgentLicService agentLicService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAgentLicMockMvc;

    private AgentLic agentLic;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AgentLicResource agentLicResource = new AgentLicResource(agentLicService);
        this.restAgentLicMockMvc = MockMvcBuilders.standaloneSetup(agentLicResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentLic createEntity(EntityManager em) {
        AgentLic agentLic = new AgentLic()
            .licNumber(DEFAULT_LIC_NUMBER)
            .type(DEFAULT_TYPE)
            .authority(DEFAULT_AUTHORITY)
            .firstActiveDate(DEFAULT_FIRST_ACTIVE_DATE)
            .effectiveDate(DEFAULT_EFFECTIVE_DATE)
            .expirationDate(DEFAULT_EXPIRATION_DATE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .isActive(DEFAULT_IS_ACTIVE)
            .isDeleted(DEFAULT_IS_DELETED);
        return agentLic;
    }

    @Before
    public void initTest() {
        agentLic = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgentLic() throws Exception {
        int databaseSizeBeforeCreate = agentLicRepository.findAll().size();

        // Create the AgentLic
        AgentLicDTO agentLicDTO = agentLicMapper.toDto(agentLic);
        restAgentLicMockMvc.perform(post("/api/agent-lics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentLicDTO)))
            .andExpect(status().isCreated());

        // Validate the AgentLic in the database
        List<AgentLic> agentLicList = agentLicRepository.findAll();
        assertThat(agentLicList).hasSize(databaseSizeBeforeCreate + 1);
        AgentLic testAgentLic = agentLicList.get(agentLicList.size() - 1);
        assertThat(testAgentLic.getLicNumber()).isEqualTo(DEFAULT_LIC_NUMBER);
        assertThat(testAgentLic.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testAgentLic.getAuthority()).isEqualTo(DEFAULT_AUTHORITY);
        assertThat(testAgentLic.getFirstActiveDate()).isEqualTo(DEFAULT_FIRST_ACTIVE_DATE);
        assertThat(testAgentLic.getEffectiveDate()).isEqualTo(DEFAULT_EFFECTIVE_DATE);
        assertThat(testAgentLic.getExpirationDate()).isEqualTo(DEFAULT_EXPIRATION_DATE);
        assertThat(testAgentLic.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testAgentLic.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testAgentLic.isIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testAgentLic.isIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createAgentLicWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agentLicRepository.findAll().size();

        // Create the AgentLic with an existing ID
        agentLic.setId(1L);
        AgentLicDTO agentLicDTO = agentLicMapper.toDto(agentLic);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgentLicMockMvc.perform(post("/api/agent-lics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentLicDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AgentLic in the database
        List<AgentLic> agentLicList = agentLicRepository.findAll();
        assertThat(agentLicList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAgentLics() throws Exception {
        // Initialize the database
        agentLicRepository.saveAndFlush(agentLic);

        // Get all the agentLicList
        restAgentLicMockMvc.perform(get("/api/agent-lics?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agentLic.getId().intValue())))
            .andExpect(jsonPath("$.[*].licNumber").value(hasItem(DEFAULT_LIC_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].authority").value(hasItem(DEFAULT_AUTHORITY.toString())))
            .andExpect(jsonPath("$.[*].firstActiveDate").value(hasItem(sameInstant(DEFAULT_FIRST_ACTIVE_DATE))))
            .andExpect(jsonPath("$.[*].effectiveDate").value(hasItem(sameInstant(DEFAULT_EFFECTIVE_DATE))))
            .andExpect(jsonPath("$.[*].expirationDate").value(hasItem(sameInstant(DEFAULT_EXPIRATION_DATE))))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getAgentLic() throws Exception {
        // Initialize the database
        agentLicRepository.saveAndFlush(agentLic);

        // Get the agentLic
        restAgentLicMockMvc.perform(get("/api/agent-lics/{id}", agentLic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(agentLic.getId().intValue()))
            .andExpect(jsonPath("$.licNumber").value(DEFAULT_LIC_NUMBER.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.authority").value(DEFAULT_AUTHORITY.toString()))
            .andExpect(jsonPath("$.firstActiveDate").value(sameInstant(DEFAULT_FIRST_ACTIVE_DATE)))
            .andExpect(jsonPath("$.effectiveDate").value(sameInstant(DEFAULT_EFFECTIVE_DATE)))
            .andExpect(jsonPath("$.expirationDate").value(sameInstant(DEFAULT_EXPIRATION_DATE)))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingAgentLic() throws Exception {
        // Get the agentLic
        restAgentLicMockMvc.perform(get("/api/agent-lics/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgentLic() throws Exception {
        // Initialize the database
        agentLicRepository.saveAndFlush(agentLic);

        int databaseSizeBeforeUpdate = agentLicRepository.findAll().size();

        // Update the agentLic
        AgentLic updatedAgentLic = agentLicRepository.findById(agentLic.getId()).get();
        // Disconnect from session so that the updates on updatedAgentLic are not directly saved in db
        em.detach(updatedAgentLic);
        updatedAgentLic
            .licNumber(UPDATED_LIC_NUMBER)
            .type(UPDATED_TYPE)
            .authority(UPDATED_AUTHORITY)
            .firstActiveDate(UPDATED_FIRST_ACTIVE_DATE)
            .effectiveDate(UPDATED_EFFECTIVE_DATE)
            .expirationDate(UPDATED_EXPIRATION_DATE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .isActive(UPDATED_IS_ACTIVE)
            .isDeleted(UPDATED_IS_DELETED);
        AgentLicDTO agentLicDTO = agentLicMapper.toDto(updatedAgentLic);

        restAgentLicMockMvc.perform(put("/api/agent-lics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentLicDTO)))
            .andExpect(status().isOk());

        // Validate the AgentLic in the database
        List<AgentLic> agentLicList = agentLicRepository.findAll();
        assertThat(agentLicList).hasSize(databaseSizeBeforeUpdate);
        AgentLic testAgentLic = agentLicList.get(agentLicList.size() - 1);
        assertThat(testAgentLic.getLicNumber()).isEqualTo(UPDATED_LIC_NUMBER);
        assertThat(testAgentLic.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testAgentLic.getAuthority()).isEqualTo(UPDATED_AUTHORITY);
        assertThat(testAgentLic.getFirstActiveDate()).isEqualTo(UPDATED_FIRST_ACTIVE_DATE);
        assertThat(testAgentLic.getEffectiveDate()).isEqualTo(UPDATED_EFFECTIVE_DATE);
        assertThat(testAgentLic.getExpirationDate()).isEqualTo(UPDATED_EXPIRATION_DATE);
        assertThat(testAgentLic.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testAgentLic.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testAgentLic.isIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testAgentLic.isIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingAgentLic() throws Exception {
        int databaseSizeBeforeUpdate = agentLicRepository.findAll().size();

        // Create the AgentLic
        AgentLicDTO agentLicDTO = agentLicMapper.toDto(agentLic);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgentLicMockMvc.perform(put("/api/agent-lics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agentLicDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AgentLic in the database
        List<AgentLic> agentLicList = agentLicRepository.findAll();
        assertThat(agentLicList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAgentLic() throws Exception {
        // Initialize the database
        agentLicRepository.saveAndFlush(agentLic);

        int databaseSizeBeforeDelete = agentLicRepository.findAll().size();

        // Get the agentLic
        restAgentLicMockMvc.perform(delete("/api/agent-lics/{id}", agentLic.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AgentLic> agentLicList = agentLicRepository.findAll();
        assertThat(agentLicList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentLic.class);
        AgentLic agentLic1 = new AgentLic();
        agentLic1.setId(1L);
        AgentLic agentLic2 = new AgentLic();
        agentLic2.setId(agentLic1.getId());
        assertThat(agentLic1).isEqualTo(agentLic2);
        agentLic2.setId(2L);
        assertThat(agentLic1).isNotEqualTo(agentLic2);
        agentLic1.setId(null);
        assertThat(agentLic1).isNotEqualTo(agentLic2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentLicDTO.class);
        AgentLicDTO agentLicDTO1 = new AgentLicDTO();
        agentLicDTO1.setId(1L);
        AgentLicDTO agentLicDTO2 = new AgentLicDTO();
        assertThat(agentLicDTO1).isNotEqualTo(agentLicDTO2);
        agentLicDTO2.setId(agentLicDTO1.getId());
        assertThat(agentLicDTO1).isEqualTo(agentLicDTO2);
        agentLicDTO2.setId(2L);
        assertThat(agentLicDTO1).isNotEqualTo(agentLicDTO2);
        agentLicDTO1.setId(null);
        assertThat(agentLicDTO1).isNotEqualTo(agentLicDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(agentLicMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(agentLicMapper.fromId(null)).isNull();
    }
}
