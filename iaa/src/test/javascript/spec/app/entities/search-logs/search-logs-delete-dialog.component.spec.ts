/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { SearchLogsDeleteDialogComponent } from 'app/entities/search-logs/search-logs-delete-dialog.component';
import { SearchLogsService } from 'app/entities/search-logs/search-logs.service';

describe('Component Tests', () => {
    describe('SearchLogs Management Delete Component', () => {
        let comp: SearchLogsDeleteDialogComponent;
        let fixture: ComponentFixture<SearchLogsDeleteDialogComponent>;
        let service: SearchLogsService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [SearchLogsDeleteDialogComponent]
            })
                .overrideTemplate(SearchLogsDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SearchLogsDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SearchLogsService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
