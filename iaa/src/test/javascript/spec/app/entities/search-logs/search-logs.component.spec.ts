/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { SearchLogsComponent } from 'app/entities/search-logs/search-logs.component';
import { SearchLogsService } from 'app/entities/search-logs/search-logs.service';
import { SearchLogs } from 'app/shared/model/search-logs.model';

describe('Component Tests', () => {
    describe('SearchLogs Management Component', () => {
        let comp: SearchLogsComponent;
        let fixture: ComponentFixture<SearchLogsComponent>;
        let service: SearchLogsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [SearchLogsComponent],
                providers: []
            })
                .overrideTemplate(SearchLogsComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SearchLogsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SearchLogsService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new SearchLogs(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.searchLogs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
