/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { AgentDistributionMvComponent } from 'app/entities/agent-distribution-mv/agent-distribution-mv.component';
import { AgentDistributionMvService } from 'app/entities/agent-distribution-mv/agent-distribution-mv.service';
import { AgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';

describe('Component Tests', () => {
    describe('AgentDistributionMv Management Component', () => {
        let comp: AgentDistributionMvComponent;
        let fixture: ComponentFixture<AgentDistributionMvComponent>;
        let service: AgentDistributionMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentDistributionMvComponent],
                providers: []
            })
                .overrideTemplate(AgentDistributionMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AgentDistributionMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentDistributionMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new AgentDistributionMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.agentDistributions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
