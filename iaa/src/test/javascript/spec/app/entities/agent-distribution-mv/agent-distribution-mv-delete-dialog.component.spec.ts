/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { AgentDistributionMvDeleteDialogComponent } from 'app/entities/agent-distribution-mv/agent-distribution-mv-delete-dialog.component';
import { AgentDistributionMvService } from 'app/entities/agent-distribution-mv/agent-distribution-mv.service';

describe('Component Tests', () => {
    describe('AgentDistributionMv Management Delete Component', () => {
        let comp: AgentDistributionMvDeleteDialogComponent;
        let fixture: ComponentFixture<AgentDistributionMvDeleteDialogComponent>;
        let service: AgentDistributionMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentDistributionMvDeleteDialogComponent]
            })
                .overrideTemplate(AgentDistributionMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AgentDistributionMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentDistributionMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
