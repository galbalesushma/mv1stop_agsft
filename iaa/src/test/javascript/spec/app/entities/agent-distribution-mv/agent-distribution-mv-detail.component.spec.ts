/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AgentDistributionMvDetailComponent } from 'app/entities/agent-distribution-mv/agent-distribution-mv-detail.component';
import { AgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';

describe('Component Tests', () => {
    describe('AgentDistributionMv Management Detail Component', () => {
        let comp: AgentDistributionMvDetailComponent;
        let fixture: ComponentFixture<AgentDistributionMvDetailComponent>;
        const route = ({ data: of({ agentDistribution: new AgentDistributionMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentDistributionMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AgentDistributionMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AgentDistributionMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.agentDistribution).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
