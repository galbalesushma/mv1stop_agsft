/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AgentDistributionMvUpdateComponent } from 'app/entities/agent-distribution-mv/agent-distribution-mv-update.component';
import { AgentDistributionMvService } from 'app/entities/agent-distribution-mv/agent-distribution-mv.service';
import { AgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';

describe('Component Tests', () => {
    describe('AgentDistributionMv Management Update Component', () => {
        let comp: AgentDistributionMvUpdateComponent;
        let fixture: ComponentFixture<AgentDistributionMvUpdateComponent>;
        let service: AgentDistributionMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentDistributionMvUpdateComponent]
            })
                .overrideTemplate(AgentDistributionMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AgentDistributionMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentDistributionMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AgentDistributionMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.agentDistribution = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AgentDistributionMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.agentDistribution = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
