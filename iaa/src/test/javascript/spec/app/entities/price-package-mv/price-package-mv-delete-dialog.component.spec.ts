/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { PricePackageMvDeleteDialogComponent } from 'app/entities/price-package-mv/price-package-mv-delete-dialog.component';
import { PricePackageMvService } from 'app/entities/price-package-mv/price-package-mv.service';

describe('Component Tests', () => {
    describe('PricePackageMv Management Delete Component', () => {
        let comp: PricePackageMvDeleteDialogComponent;
        let fixture: ComponentFixture<PricePackageMvDeleteDialogComponent>;
        let service: PricePackageMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PricePackageMvDeleteDialogComponent]
            })
                .overrideTemplate(PricePackageMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PricePackageMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PricePackageMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
