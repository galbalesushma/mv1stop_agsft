/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { PricePackageMvDetailComponent } from 'app/entities/price-package-mv/price-package-mv-detail.component';
import { PricePackageMv } from 'app/shared/model/price-package-mv.model';

describe('Component Tests', () => {
    describe('PricePackageMv Management Detail Component', () => {
        let comp: PricePackageMvDetailComponent;
        let fixture: ComponentFixture<PricePackageMvDetailComponent>;
        const route = ({ data: of({ pricePackage: new PricePackageMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PricePackageMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PricePackageMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PricePackageMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.pricePackage).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
