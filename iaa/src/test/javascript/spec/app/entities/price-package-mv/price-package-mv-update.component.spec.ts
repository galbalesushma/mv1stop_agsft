/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { PricePackageMvUpdateComponent } from 'app/entities/price-package-mv/price-package-mv-update.component';
import { PricePackageMvService } from 'app/entities/price-package-mv/price-package-mv.service';
import { PricePackageMv } from 'app/shared/model/price-package-mv.model';

describe('Component Tests', () => {
    describe('PricePackageMv Management Update Component', () => {
        let comp: PricePackageMvUpdateComponent;
        let fixture: ComponentFixture<PricePackageMvUpdateComponent>;
        let service: PricePackageMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PricePackageMvUpdateComponent]
            })
                .overrideTemplate(PricePackageMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PricePackageMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PricePackageMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PricePackageMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.pricePackage = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PricePackageMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.pricePackage = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
