/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { PricePackageMvComponent } from 'app/entities/price-package-mv/price-package-mv.component';
import { PricePackageMvService } from 'app/entities/price-package-mv/price-package-mv.service';
import { PricePackageMv } from 'app/shared/model/price-package-mv.model';

describe('Component Tests', () => {
    describe('PricePackageMv Management Component', () => {
        let comp: PricePackageMvComponent;
        let fixture: ComponentFixture<PricePackageMvComponent>;
        let service: PricePackageMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PricePackageMvComponent],
                providers: []
            })
                .overrideTemplate(PricePackageMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PricePackageMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PricePackageMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new PricePackageMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.pricePackages[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
