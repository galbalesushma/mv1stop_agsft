/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { ProducerMvDetailComponent } from 'app/entities/producer-mv/producer-mv-detail.component';
import { ProducerMv } from 'app/shared/model/producer-mv.model';

describe('Component Tests', () => {
    describe('ProducerMv Management Detail Component', () => {
        let comp: ProducerMvDetailComponent;
        let fixture: ComponentFixture<ProducerMvDetailComponent>;
        const route = ({ data: of({ producer: new ProducerMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [ProducerMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ProducerMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProducerMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.producer).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
