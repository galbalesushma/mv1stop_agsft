/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { ProducerMvDeleteDialogComponent } from 'app/entities/producer-mv/producer-mv-delete-dialog.component';
import { ProducerMvService } from 'app/entities/producer-mv/producer-mv.service';

describe('Component Tests', () => {
    describe('ProducerMv Management Delete Component', () => {
        let comp: ProducerMvDeleteDialogComponent;
        let fixture: ComponentFixture<ProducerMvDeleteDialogComponent>;
        let service: ProducerMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [ProducerMvDeleteDialogComponent]
            })
                .overrideTemplate(ProducerMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProducerMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProducerMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
