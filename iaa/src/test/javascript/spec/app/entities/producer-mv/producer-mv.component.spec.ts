/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { ProducerMvComponent } from 'app/entities/producer-mv/producer-mv.component';
import { ProducerMvService } from 'app/entities/producer-mv/producer-mv.service';
import { ProducerMv } from 'app/shared/model/producer-mv.model';

describe('Component Tests', () => {
    describe('ProducerMv Management Component', () => {
        let comp: ProducerMvComponent;
        let fixture: ComponentFixture<ProducerMvComponent>;
        let service: ProducerMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [ProducerMvComponent],
                providers: []
            })
                .overrideTemplate(ProducerMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ProducerMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProducerMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new ProducerMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.producers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
