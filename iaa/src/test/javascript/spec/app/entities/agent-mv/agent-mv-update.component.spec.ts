/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AgentMvUpdateComponent } from 'app/entities/agent-mv/agent-mv-update.component';
import { AgentMvService } from 'app/entities/agent-mv/agent-mv.service';
import { AgentMv } from 'app/shared/model/agent-mv.model';

describe('Component Tests', () => {
    describe('AgentMv Management Update Component', () => {
        let comp: AgentMvUpdateComponent;
        let fixture: ComponentFixture<AgentMvUpdateComponent>;
        let service: AgentMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentMvUpdateComponent]
            })
                .overrideTemplate(AgentMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AgentMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AgentMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.agent = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AgentMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.agent = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
