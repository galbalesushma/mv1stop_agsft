/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { AgentMvComponent } from 'app/entities/agent-mv/agent-mv.component';
import { AgentMvService } from 'app/entities/agent-mv/agent-mv.service';
import { AgentMv } from 'app/shared/model/agent-mv.model';

describe('Component Tests', () => {
    describe('AgentMv Management Component', () => {
        let comp: AgentMvComponent;
        let fixture: ComponentFixture<AgentMvComponent>;
        let service: AgentMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentMvComponent],
                providers: []
            })
                .overrideTemplate(AgentMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AgentMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new AgentMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.agents[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
