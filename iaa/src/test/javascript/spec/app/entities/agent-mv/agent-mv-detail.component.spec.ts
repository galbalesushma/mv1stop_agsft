/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AgentMvDetailComponent } from 'app/entities/agent-mv/agent-mv-detail.component';
import { AgentMv } from 'app/shared/model/agent-mv.model';

describe('Component Tests', () => {
    describe('AgentMv Management Detail Component', () => {
        let comp: AgentMvDetailComponent;
        let fixture: ComponentFixture<AgentMvDetailComponent>;
        const route = ({ data: of({ agent: new AgentMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AgentMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AgentMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.agent).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
