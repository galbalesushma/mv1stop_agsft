/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { PackSubscriptionMvDetailComponent } from 'app/entities/pack-subscription-mv/pack-subscription-mv-detail.component';
import { PackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';

describe('Component Tests', () => {
    describe('PackSubscriptionMv Management Detail Component', () => {
        let comp: PackSubscriptionMvDetailComponent;
        let fixture: ComponentFixture<PackSubscriptionMvDetailComponent>;
        const route = ({ data: of({ packSubscription: new PackSubscriptionMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PackSubscriptionMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PackSubscriptionMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PackSubscriptionMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.packSubscription).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
