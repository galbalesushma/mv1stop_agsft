/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { PackSubscriptionMvComponent } from 'app/entities/pack-subscription-mv/pack-subscription-mv.component';
import { PackSubscriptionMvService } from 'app/entities/pack-subscription-mv/pack-subscription-mv.service';
import { PackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';

describe('Component Tests', () => {
    describe('PackSubscriptionMv Management Component', () => {
        let comp: PackSubscriptionMvComponent;
        let fixture: ComponentFixture<PackSubscriptionMvComponent>;
        let service: PackSubscriptionMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PackSubscriptionMvComponent],
                providers: []
            })
                .overrideTemplate(PackSubscriptionMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PackSubscriptionMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PackSubscriptionMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new PackSubscriptionMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.packSubscriptions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
