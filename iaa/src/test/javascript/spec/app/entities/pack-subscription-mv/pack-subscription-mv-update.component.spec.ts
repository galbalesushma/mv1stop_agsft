/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { PackSubscriptionMvUpdateComponent } from 'app/entities/pack-subscription-mv/pack-subscription-mv-update.component';
import { PackSubscriptionMvService } from 'app/entities/pack-subscription-mv/pack-subscription-mv.service';
import { PackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';

describe('Component Tests', () => {
    describe('PackSubscriptionMv Management Update Component', () => {
        let comp: PackSubscriptionMvUpdateComponent;
        let fixture: ComponentFixture<PackSubscriptionMvUpdateComponent>;
        let service: PackSubscriptionMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PackSubscriptionMvUpdateComponent]
            })
                .overrideTemplate(PackSubscriptionMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PackSubscriptionMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PackSubscriptionMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PackSubscriptionMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.packSubscription = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PackSubscriptionMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.packSubscription = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
