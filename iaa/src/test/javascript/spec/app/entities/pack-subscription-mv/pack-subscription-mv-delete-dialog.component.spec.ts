/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { PackSubscriptionMvDeleteDialogComponent } from 'app/entities/pack-subscription-mv/pack-subscription-mv-delete-dialog.component';
import { PackSubscriptionMvService } from 'app/entities/pack-subscription-mv/pack-subscription-mv.service';

describe('Component Tests', () => {
    describe('PackSubscriptionMv Management Delete Component', () => {
        let comp: PackSubscriptionMvDeleteDialogComponent;
        let fixture: ComponentFixture<PackSubscriptionMvDeleteDialogComponent>;
        let service: PackSubscriptionMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PackSubscriptionMvDeleteDialogComponent]
            })
                .overrideTemplate(PackSubscriptionMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PackSubscriptionMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PackSubscriptionMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
