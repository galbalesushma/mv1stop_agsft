/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { BusinessMvDetailComponent } from 'app/entities/business-mv/business-mv-detail.component';
import { BusinessMv } from 'app/shared/model/business-mv.model';

describe('Component Tests', () => {
    describe('BusinessMv Management Detail Component', () => {
        let comp: BusinessMvDetailComponent;
        let fixture: ComponentFixture<BusinessMvDetailComponent>;
        const route = ({ data: of({ business: new BusinessMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [BusinessMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(BusinessMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BusinessMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.business).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
