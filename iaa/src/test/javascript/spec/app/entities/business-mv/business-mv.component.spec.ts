/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { BusinessMvComponent } from 'app/entities/business-mv/business-mv.component';
import { BusinessMvService } from 'app/entities/business-mv/business-mv.service';
import { BusinessMv } from 'app/shared/model/business-mv.model';

describe('Component Tests', () => {
    describe('BusinessMv Management Component', () => {
        let comp: BusinessMvComponent;
        let fixture: ComponentFixture<BusinessMvComponent>;
        let service: BusinessMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [BusinessMvComponent],
                providers: []
            })
                .overrideTemplate(BusinessMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(BusinessMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BusinessMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new BusinessMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.businesses[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
