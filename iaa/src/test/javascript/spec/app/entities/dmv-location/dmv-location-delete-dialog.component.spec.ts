/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { DMVLocationDeleteDialogComponent } from 'app/entities/dmv-location/dmv-location-delete-dialog.component';
import { DMVLocationService } from 'app/entities/dmv-location/dmv-location.service';

describe('Component Tests', () => {
    describe('DMVLocation Management Delete Component', () => {
        let comp: DMVLocationDeleteDialogComponent;
        let fixture: ComponentFixture<DMVLocationDeleteDialogComponent>;
        let service: DMVLocationService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [DMVLocationDeleteDialogComponent]
            })
                .overrideTemplate(DMVLocationDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DMVLocationDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DMVLocationService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
