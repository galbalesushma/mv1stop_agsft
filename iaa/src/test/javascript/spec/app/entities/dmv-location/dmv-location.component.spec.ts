/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { DMVLocationComponent } from 'app/entities/dmv-location/dmv-location.component';
import { DMVLocationService } from 'app/entities/dmv-location/dmv-location.service';
import { DMVLocation } from 'app/shared/model/dmv-location.model';

describe('Component Tests', () => {
    describe('DMVLocation Management Component', () => {
        let comp: DMVLocationComponent;
        let fixture: ComponentFixture<DMVLocationComponent>;
        let service: DMVLocationService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [DMVLocationComponent],
                providers: []
            })
                .overrideTemplate(DMVLocationComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DMVLocationComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DMVLocationService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new DMVLocation(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.dMVLocations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
