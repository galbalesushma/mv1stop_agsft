/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { DMVLocationDetailComponent } from 'app/entities/dmv-location/dmv-location-detail.component';
import { DMVLocation } from 'app/shared/model/dmv-location.model';

describe('Component Tests', () => {
    describe('DMVLocation Management Detail Component', () => {
        let comp: DMVLocationDetailComponent;
        let fixture: ComponentFixture<DMVLocationDetailComponent>;
        const route = ({ data: of({ dMVLocation: new DMVLocation(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [DMVLocationDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(DMVLocationDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DMVLocationDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.dMVLocation).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
