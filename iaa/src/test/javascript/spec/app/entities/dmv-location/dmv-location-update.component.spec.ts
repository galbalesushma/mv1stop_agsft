/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { DMVLocationUpdateComponent } from 'app/entities/dmv-location/dmv-location-update.component';
import { DMVLocationService } from 'app/entities/dmv-location/dmv-location.service';
import { DMVLocation } from 'app/shared/model/dmv-location.model';

describe('Component Tests', () => {
    describe('DMVLocation Management Update Component', () => {
        let comp: DMVLocationUpdateComponent;
        let fixture: ComponentFixture<DMVLocationUpdateComponent>;
        let service: DMVLocationService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [DMVLocationUpdateComponent]
            })
                .overrideTemplate(DMVLocationUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DMVLocationUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DMVLocationService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DMVLocation(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.dMVLocation = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DMVLocation();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.dMVLocation = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
