/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { AgentTypeMvDeleteDialogComponent } from 'app/entities/agent-type-mv/agent-type-mv-delete-dialog.component';
import { AgentTypeMvService } from 'app/entities/agent-type-mv/agent-type-mv.service';

describe('Component Tests', () => {
    describe('AgentTypeMv Management Delete Component', () => {
        let comp: AgentTypeMvDeleteDialogComponent;
        let fixture: ComponentFixture<AgentTypeMvDeleteDialogComponent>;
        let service: AgentTypeMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentTypeMvDeleteDialogComponent]
            })
                .overrideTemplate(AgentTypeMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AgentTypeMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentTypeMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
