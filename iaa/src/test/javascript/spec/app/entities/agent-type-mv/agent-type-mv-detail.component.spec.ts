/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AgentTypeMvDetailComponent } from 'app/entities/agent-type-mv/agent-type-mv-detail.component';
import { AgentTypeMv } from 'app/shared/model/agent-type-mv.model';

describe('Component Tests', () => {
    describe('AgentTypeMv Management Detail Component', () => {
        let comp: AgentTypeMvDetailComponent;
        let fixture: ComponentFixture<AgentTypeMvDetailComponent>;
        const route = ({ data: of({ agentType: new AgentTypeMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentTypeMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AgentTypeMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AgentTypeMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.agentType).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
