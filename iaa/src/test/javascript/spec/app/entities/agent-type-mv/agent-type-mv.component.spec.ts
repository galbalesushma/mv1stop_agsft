/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { AgentTypeMvComponent } from 'app/entities/agent-type-mv/agent-type-mv.component';
import { AgentTypeMvService } from 'app/entities/agent-type-mv/agent-type-mv.service';
import { AgentTypeMv } from 'app/shared/model/agent-type-mv.model';

describe('Component Tests', () => {
    describe('AgentTypeMv Management Component', () => {
        let comp: AgentTypeMvComponent;
        let fixture: ComponentFixture<AgentTypeMvComponent>;
        let service: AgentTypeMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentTypeMvComponent],
                providers: []
            })
                .overrideTemplate(AgentTypeMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AgentTypeMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentTypeMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new AgentTypeMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.agentTypes[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
