/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AgentTypeMvUpdateComponent } from 'app/entities/agent-type-mv/agent-type-mv-update.component';
import { AgentTypeMvService } from 'app/entities/agent-type-mv/agent-type-mv.service';
import { AgentTypeMv } from 'app/shared/model/agent-type-mv.model';

describe('Component Tests', () => {
    describe('AgentTypeMv Management Update Component', () => {
        let comp: AgentTypeMvUpdateComponent;
        let fixture: ComponentFixture<AgentTypeMvUpdateComponent>;
        let service: AgentTypeMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentTypeMvUpdateComponent]
            })
                .overrideTemplate(AgentTypeMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AgentTypeMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentTypeMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AgentTypeMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.agentType = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AgentTypeMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.agentType = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
