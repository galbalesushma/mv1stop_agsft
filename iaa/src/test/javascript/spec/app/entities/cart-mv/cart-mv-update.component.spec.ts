/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { CartMvUpdateComponent } from 'app/entities/cart-mv/cart-mv-update.component';
import { CartMvService } from 'app/entities/cart-mv/cart-mv.service';
import { CartMv } from 'app/shared/model/cart-mv.model';

describe('Component Tests', () => {
    describe('CartMv Management Update Component', () => {
        let comp: CartMvUpdateComponent;
        let fixture: ComponentFixture<CartMvUpdateComponent>;
        let service: CartMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CartMvUpdateComponent]
            })
                .overrideTemplate(CartMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CartMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CartMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CartMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.cart = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CartMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.cart = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
