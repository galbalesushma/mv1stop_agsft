/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { CartMvDetailComponent } from 'app/entities/cart-mv/cart-mv-detail.component';
import { CartMv } from 'app/shared/model/cart-mv.model';

describe('Component Tests', () => {
    describe('CartMv Management Detail Component', () => {
        let comp: CartMvDetailComponent;
        let fixture: ComponentFixture<CartMvDetailComponent>;
        const route = ({ data: of({ cart: new CartMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CartMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CartMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CartMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.cart).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
