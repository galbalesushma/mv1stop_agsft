/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { CartMvComponent } from 'app/entities/cart-mv/cart-mv.component';
import { CartMvService } from 'app/entities/cart-mv/cart-mv.service';
import { CartMv } from 'app/shared/model/cart-mv.model';

describe('Component Tests', () => {
    describe('CartMv Management Component', () => {
        let comp: CartMvComponent;
        let fixture: ComponentFixture<CartMvComponent>;
        let service: CartMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CartMvComponent],
                providers: []
            })
                .overrideTemplate(CartMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CartMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CartMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CartMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.carts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
