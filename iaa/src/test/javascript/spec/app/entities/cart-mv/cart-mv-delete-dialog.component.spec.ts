/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { CartMvDeleteDialogComponent } from 'app/entities/cart-mv/cart-mv-delete-dialog.component';
import { CartMvService } from 'app/entities/cart-mv/cart-mv.service';

describe('Component Tests', () => {
    describe('CartMv Management Delete Component', () => {
        let comp: CartMvDeleteDialogComponent;
        let fixture: ComponentFixture<CartMvDeleteDialogComponent>;
        let service: CartMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CartMvDeleteDialogComponent]
            })
                .overrideTemplate(CartMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CartMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CartMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
