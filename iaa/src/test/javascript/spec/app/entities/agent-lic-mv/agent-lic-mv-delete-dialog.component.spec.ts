/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { AgentLicMvDeleteDialogComponent } from 'app/entities/agent-lic-mv/agent-lic-mv-delete-dialog.component';
import { AgentLicMvService } from 'app/entities/agent-lic-mv/agent-lic-mv.service';

describe('Component Tests', () => {
    describe('AgentLicMv Management Delete Component', () => {
        let comp: AgentLicMvDeleteDialogComponent;
        let fixture: ComponentFixture<AgentLicMvDeleteDialogComponent>;
        let service: AgentLicMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentLicMvDeleteDialogComponent]
            })
                .overrideTemplate(AgentLicMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AgentLicMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentLicMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
