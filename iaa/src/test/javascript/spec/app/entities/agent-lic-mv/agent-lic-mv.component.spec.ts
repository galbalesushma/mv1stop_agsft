/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { AgentLicMvComponent } from 'app/entities/agent-lic-mv/agent-lic-mv.component';
import { AgentLicMvService } from 'app/entities/agent-lic-mv/agent-lic-mv.service';
import { AgentLicMv } from 'app/shared/model/agent-lic-mv.model';

describe('Component Tests', () => {
    describe('AgentLicMv Management Component', () => {
        let comp: AgentLicMvComponent;
        let fixture: ComponentFixture<AgentLicMvComponent>;
        let service: AgentLicMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentLicMvComponent],
                providers: []
            })
                .overrideTemplate(AgentLicMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AgentLicMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentLicMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new AgentLicMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.agentLics[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
