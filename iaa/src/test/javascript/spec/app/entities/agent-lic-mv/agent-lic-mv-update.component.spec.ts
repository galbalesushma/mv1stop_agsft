/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AgentLicMvUpdateComponent } from 'app/entities/agent-lic-mv/agent-lic-mv-update.component';
import { AgentLicMvService } from 'app/entities/agent-lic-mv/agent-lic-mv.service';
import { AgentLicMv } from 'app/shared/model/agent-lic-mv.model';

describe('Component Tests', () => {
    describe('AgentLicMv Management Update Component', () => {
        let comp: AgentLicMvUpdateComponent;
        let fixture: ComponentFixture<AgentLicMvUpdateComponent>;
        let service: AgentLicMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentLicMvUpdateComponent]
            })
                .overrideTemplate(AgentLicMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AgentLicMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgentLicMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AgentLicMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.agentLic = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AgentLicMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.agentLic = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
