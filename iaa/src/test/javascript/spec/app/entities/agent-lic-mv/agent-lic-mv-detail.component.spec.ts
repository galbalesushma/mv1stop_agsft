/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AgentLicMvDetailComponent } from 'app/entities/agent-lic-mv/agent-lic-mv-detail.component';
import { AgentLicMv } from 'app/shared/model/agent-lic-mv.model';

describe('Component Tests', () => {
    describe('AgentLicMv Management Detail Component', () => {
        let comp: AgentLicMvDetailComponent;
        let fixture: ComponentFixture<AgentLicMvDetailComponent>;
        const route = ({ data: of({ agentLic: new AgentLicMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AgentLicMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AgentLicMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AgentLicMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.agentLic).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
