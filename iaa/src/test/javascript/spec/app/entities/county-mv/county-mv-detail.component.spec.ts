/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { CountyMvDetailComponent } from 'app/entities/county-mv/county-mv-detail.component';
import { CountyMv } from 'app/shared/model/county-mv.model';

describe('Component Tests', () => {
    describe('CountyMv Management Detail Component', () => {
        let comp: CountyMvDetailComponent;
        let fixture: ComponentFixture<CountyMvDetailComponent>;
        const route = ({ data: of({ county: new CountyMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CountyMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CountyMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CountyMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.county).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
