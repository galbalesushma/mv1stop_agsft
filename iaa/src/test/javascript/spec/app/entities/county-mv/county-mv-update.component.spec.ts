/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { CountyMvUpdateComponent } from 'app/entities/county-mv/county-mv-update.component';
import { CountyMvService } from 'app/entities/county-mv/county-mv.service';
import { CountyMv } from 'app/shared/model/county-mv.model';

describe('Component Tests', () => {
    describe('CountyMv Management Update Component', () => {
        let comp: CountyMvUpdateComponent;
        let fixture: ComponentFixture<CountyMvUpdateComponent>;
        let service: CountyMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CountyMvUpdateComponent]
            })
                .overrideTemplate(CountyMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CountyMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CountyMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CountyMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.county = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CountyMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.county = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
