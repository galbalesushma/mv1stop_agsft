/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { CountyMvComponent } from 'app/entities/county-mv/county-mv.component';
import { CountyMvService } from 'app/entities/county-mv/county-mv.service';
import { CountyMv } from 'app/shared/model/county-mv.model';

describe('Component Tests', () => {
    describe('CountyMv Management Component', () => {
        let comp: CountyMvComponent;
        let fixture: ComponentFixture<CountyMvComponent>;
        let service: CountyMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CountyMvComponent],
                providers: []
            })
                .overrideTemplate(CountyMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CountyMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CountyMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CountyMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.counties[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
