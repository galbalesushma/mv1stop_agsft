/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { StateMvUpdateComponent } from 'app/entities/state-mv/state-mv-update.component';
import { StateMvService } from 'app/entities/state-mv/state-mv.service';
import { StateMv } from 'app/shared/model/state-mv.model';

describe('Component Tests', () => {
    describe('StateMv Management Update Component', () => {
        let comp: StateMvUpdateComponent;
        let fixture: ComponentFixture<StateMvUpdateComponent>;
        let service: StateMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [StateMvUpdateComponent]
            })
                .overrideTemplate(StateMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(StateMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StateMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new StateMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.state = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new StateMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.state = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
