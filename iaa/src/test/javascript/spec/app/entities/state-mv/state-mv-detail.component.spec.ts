/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { StateMvDetailComponent } from 'app/entities/state-mv/state-mv-detail.component';
import { StateMv } from 'app/shared/model/state-mv.model';

describe('Component Tests', () => {
    describe('StateMv Management Detail Component', () => {
        let comp: StateMvDetailComponent;
        let fixture: ComponentFixture<StateMvDetailComponent>;
        const route = ({ data: of({ state: new StateMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [StateMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(StateMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(StateMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.state).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
