/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { StateMvDeleteDialogComponent } from 'app/entities/state-mv/state-mv-delete-dialog.component';
import { StateMvService } from 'app/entities/state-mv/state-mv.service';

describe('Component Tests', () => {
    describe('StateMv Management Delete Component', () => {
        let comp: StateMvDeleteDialogComponent;
        let fixture: ComponentFixture<StateMvDeleteDialogComponent>;
        let service: StateMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [StateMvDeleteDialogComponent]
            })
                .overrideTemplate(StateMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(StateMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StateMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
