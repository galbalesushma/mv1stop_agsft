/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { StateMvComponent } from 'app/entities/state-mv/state-mv.component';
import { StateMvService } from 'app/entities/state-mv/state-mv.service';
import { StateMv } from 'app/shared/model/state-mv.model';

describe('Component Tests', () => {
    describe('StateMv Management Component', () => {
        let comp: StateMvComponent;
        let fixture: ComponentFixture<StateMvComponent>;
        let service: StateMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [StateMvComponent],
                providers: []
            })
                .overrideTemplate(StateMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(StateMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StateMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new StateMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.states[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
