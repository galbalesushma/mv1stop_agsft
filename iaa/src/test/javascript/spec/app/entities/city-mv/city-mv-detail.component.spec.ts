/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { CityMvDetailComponent } from 'app/entities/city-mv/city-mv-detail.component';
import { CityMv } from 'app/shared/model/city-mv.model';

describe('Component Tests', () => {
    describe('CityMv Management Detail Component', () => {
        let comp: CityMvDetailComponent;
        let fixture: ComponentFixture<CityMvDetailComponent>;
        const route = ({ data: of({ city: new CityMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CityMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CityMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CityMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.city).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
