/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { CityMvUpdateComponent } from 'app/entities/city-mv/city-mv-update.component';
import { CityMvService } from 'app/entities/city-mv/city-mv.service';
import { CityMv } from 'app/shared/model/city-mv.model';

describe('Component Tests', () => {
    describe('CityMv Management Update Component', () => {
        let comp: CityMvUpdateComponent;
        let fixture: ComponentFixture<CityMvUpdateComponent>;
        let service: CityMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CityMvUpdateComponent]
            })
                .overrideTemplate(CityMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CityMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CityMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CityMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.city = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CityMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.city = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
