/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { CityMvComponent } from 'app/entities/city-mv/city-mv.component';
import { CityMvService } from 'app/entities/city-mv/city-mv.service';
import { CityMv } from 'app/shared/model/city-mv.model';

describe('Component Tests', () => {
    describe('CityMv Management Component', () => {
        let comp: CityMvComponent;
        let fixture: ComponentFixture<CityMvComponent>;
        let service: CityMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [CityMvComponent],
                providers: []
            })
                .overrideTemplate(CityMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CityMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CityMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CityMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.cities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
