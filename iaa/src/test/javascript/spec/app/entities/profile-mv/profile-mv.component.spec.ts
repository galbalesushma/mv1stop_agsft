/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { ProfileMvComponent } from 'app/entities/profile-mv/profile-mv.component';
import { ProfileMvService } from 'app/entities/profile-mv/profile-mv.service';
import { ProfileMv } from 'app/shared/model/profile-mv.model';

describe('Component Tests', () => {
    describe('ProfileMv Management Component', () => {
        let comp: ProfileMvComponent;
        let fixture: ComponentFixture<ProfileMvComponent>;
        let service: ProfileMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [ProfileMvComponent],
                providers: []
            })
                .overrideTemplate(ProfileMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ProfileMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProfileMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new ProfileMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.profiles[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
