/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { ZipAreaMvDetailComponent } from 'app/entities/zip-area-mv/zip-area-mv-detail.component';
import { ZipAreaMv } from 'app/shared/model/zip-area-mv.model';

describe('Component Tests', () => {
    describe('ZipAreaMv Management Detail Component', () => {
        let comp: ZipAreaMvDetailComponent;
        let fixture: ComponentFixture<ZipAreaMvDetailComponent>;
        const route = ({ data: of({ zipArea: new ZipAreaMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [ZipAreaMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ZipAreaMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ZipAreaMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.zipArea).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
