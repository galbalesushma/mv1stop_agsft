/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { ZipAreaMvDeleteDialogComponent } from 'app/entities/zip-area-mv/zip-area-mv-delete-dialog.component';
import { ZipAreaMvService } from 'app/entities/zip-area-mv/zip-area-mv.service';

describe('Component Tests', () => {
    describe('ZipAreaMv Management Delete Component', () => {
        let comp: ZipAreaMvDeleteDialogComponent;
        let fixture: ComponentFixture<ZipAreaMvDeleteDialogComponent>;
        let service: ZipAreaMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [ZipAreaMvDeleteDialogComponent]
            })
                .overrideTemplate(ZipAreaMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ZipAreaMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZipAreaMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
