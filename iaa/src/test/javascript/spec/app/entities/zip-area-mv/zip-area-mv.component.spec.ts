/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { ZipAreaMvComponent } from 'app/entities/zip-area-mv/zip-area-mv.component';
import { ZipAreaMvService } from 'app/entities/zip-area-mv/zip-area-mv.service';
import { ZipAreaMv } from 'app/shared/model/zip-area-mv.model';

describe('Component Tests', () => {
    describe('ZipAreaMv Management Component', () => {
        let comp: ZipAreaMvComponent;
        let fixture: ComponentFixture<ZipAreaMvComponent>;
        let service: ZipAreaMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [ZipAreaMvComponent],
                providers: []
            })
                .overrideTemplate(ZipAreaMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ZipAreaMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZipAreaMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new ZipAreaMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.zipAreas[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
