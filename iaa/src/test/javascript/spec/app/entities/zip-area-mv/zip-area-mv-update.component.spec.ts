/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { ZipAreaMvUpdateComponent } from 'app/entities/zip-area-mv/zip-area-mv-update.component';
import { ZipAreaMvService } from 'app/entities/zip-area-mv/zip-area-mv.service';
import { ZipAreaMv } from 'app/shared/model/zip-area-mv.model';

describe('Component Tests', () => {
    describe('ZipAreaMv Management Update Component', () => {
        let comp: ZipAreaMvUpdateComponent;
        let fixture: ComponentFixture<ZipAreaMvUpdateComponent>;
        let service: ZipAreaMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [ZipAreaMvUpdateComponent]
            })
                .overrideTemplate(ZipAreaMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ZipAreaMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ZipAreaMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ZipAreaMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.zipArea = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ZipAreaMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.zipArea = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
