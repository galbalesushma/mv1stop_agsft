/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { PaymentMvComponent } from 'app/entities/payment-mv/payment-mv.component';
import { PaymentMvService } from 'app/entities/payment-mv/payment-mv.service';
import { PaymentMv } from 'app/shared/model/payment-mv.model';

describe('Component Tests', () => {
    describe('PaymentMv Management Component', () => {
        let comp: PaymentMvComponent;
        let fixture: ComponentFixture<PaymentMvComponent>;
        let service: PaymentMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PaymentMvComponent],
                providers: []
            })
                .overrideTemplate(PaymentMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PaymentMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new PaymentMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.payments[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
