/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { PaymentMvDeleteDialogComponent } from 'app/entities/payment-mv/payment-mv-delete-dialog.component';
import { PaymentMvService } from 'app/entities/payment-mv/payment-mv.service';

describe('Component Tests', () => {
    describe('PaymentMv Management Delete Component', () => {
        let comp: PaymentMvDeleteDialogComponent;
        let fixture: ComponentFixture<PaymentMvDeleteDialogComponent>;
        let service: PaymentMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PaymentMvDeleteDialogComponent]
            })
                .overrideTemplate(PaymentMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PaymentMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
