/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { PaymentMvDetailComponent } from 'app/entities/payment-mv/payment-mv-detail.component';
import { PaymentMv } from 'app/shared/model/payment-mv.model';

describe('Component Tests', () => {
    describe('PaymentMv Management Detail Component', () => {
        let comp: PaymentMvDetailComponent;
        let fixture: ComponentFixture<PaymentMvDetailComponent>;
        const route = ({ data: of({ payment: new PaymentMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PaymentMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PaymentMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PaymentMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.payment).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
