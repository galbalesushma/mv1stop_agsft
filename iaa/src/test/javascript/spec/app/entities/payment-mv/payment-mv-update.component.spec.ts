/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { PaymentMvUpdateComponent } from 'app/entities/payment-mv/payment-mv-update.component';
import { PaymentMvService } from 'app/entities/payment-mv/payment-mv.service';
import { PaymentMv } from 'app/shared/model/payment-mv.model';

describe('Component Tests', () => {
    describe('PaymentMv Management Update Component', () => {
        let comp: PaymentMvUpdateComponent;
        let fixture: ComponentFixture<PaymentMvUpdateComponent>;
        let service: PaymentMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [PaymentMvUpdateComponent]
            })
                .overrideTemplate(PaymentMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PaymentMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PaymentMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.payment = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PaymentMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.payment = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
