/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { FacilityMvDeleteDialogComponent } from 'app/entities/facility-mv/facility-mv-delete-dialog.component';
import { FacilityMvService } from 'app/entities/facility-mv/facility-mv.service';

describe('Component Tests', () => {
    describe('FacilityMv Management Delete Component', () => {
        let comp: FacilityMvDeleteDialogComponent;
        let fixture: ComponentFixture<FacilityMvDeleteDialogComponent>;
        let service: FacilityMvService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [FacilityMvDeleteDialogComponent]
            })
                .overrideTemplate(FacilityMvDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(FacilityMvDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FacilityMvService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
