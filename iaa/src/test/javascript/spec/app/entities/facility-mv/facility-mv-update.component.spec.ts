/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { FacilityMvUpdateComponent } from 'app/entities/facility-mv/facility-mv-update.component';
import { FacilityMvService } from 'app/entities/facility-mv/facility-mv.service';
import { FacilityMv } from 'app/shared/model/facility-mv.model';

describe('Component Tests', () => {
    describe('FacilityMv Management Update Component', () => {
        let comp: FacilityMvUpdateComponent;
        let fixture: ComponentFixture<FacilityMvUpdateComponent>;
        let service: FacilityMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [FacilityMvUpdateComponent]
            })
                .overrideTemplate(FacilityMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(FacilityMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FacilityMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new FacilityMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.facility = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new FacilityMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.facility = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
