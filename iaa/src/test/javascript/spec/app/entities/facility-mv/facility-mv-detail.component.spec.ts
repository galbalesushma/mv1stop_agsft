/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { FacilityMvDetailComponent } from 'app/entities/facility-mv/facility-mv-detail.component';
import { FacilityMv } from 'app/shared/model/facility-mv.model';

describe('Component Tests', () => {
    describe('FacilityMv Management Detail Component', () => {
        let comp: FacilityMvDetailComponent;
        let fixture: ComponentFixture<FacilityMvDetailComponent>;
        const route = ({ data: of({ facility: new FacilityMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [FacilityMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(FacilityMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(FacilityMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.facility).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
