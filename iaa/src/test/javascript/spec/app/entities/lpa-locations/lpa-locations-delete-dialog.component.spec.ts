/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IaaTestModule } from '../../../test.module';
import { LPALocationsDeleteDialogComponent } from 'app/entities/lpa-locations/lpa-locations-delete-dialog.component';
import { LPALocationsService } from 'app/entities/lpa-locations/lpa-locations.service';

describe('Component Tests', () => {
    describe('LPALocations Management Delete Component', () => {
        let comp: LPALocationsDeleteDialogComponent;
        let fixture: ComponentFixture<LPALocationsDeleteDialogComponent>;
        let service: LPALocationsService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [LPALocationsDeleteDialogComponent]
            })
                .overrideTemplate(LPALocationsDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LPALocationsDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LPALocationsService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
