/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { LPALocationsDetailComponent } from 'app/entities/lpa-locations/lpa-locations-detail.component';
import { LPALocations } from 'app/shared/model/lpa-locations.model';

describe('Component Tests', () => {
    describe('LPALocations Management Detail Component', () => {
        let comp: LPALocationsDetailComponent;
        let fixture: ComponentFixture<LPALocationsDetailComponent>;
        const route = ({ data: of({ lPALocations: new LPALocations(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [LPALocationsDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(LPALocationsDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LPALocationsDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.lPALocations).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
