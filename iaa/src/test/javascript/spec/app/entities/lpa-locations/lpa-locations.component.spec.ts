/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { LPALocationsComponent } from 'app/entities/lpa-locations/lpa-locations.component';
import { LPALocationsService } from 'app/entities/lpa-locations/lpa-locations.service';
import { LPALocations } from 'app/shared/model/lpa-locations.model';

describe('Component Tests', () => {
    describe('LPALocations Management Component', () => {
        let comp: LPALocationsComponent;
        let fixture: ComponentFixture<LPALocationsComponent>;
        let service: LPALocationsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [LPALocationsComponent],
                providers: []
            })
                .overrideTemplate(LPALocationsComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(LPALocationsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LPALocationsService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new LPALocations(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.lPALocations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
