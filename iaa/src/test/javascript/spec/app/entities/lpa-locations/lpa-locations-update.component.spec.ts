/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { LPALocationsUpdateComponent } from 'app/entities/lpa-locations/lpa-locations-update.component';
import { LPALocationsService } from 'app/entities/lpa-locations/lpa-locations.service';
import { LPALocations } from 'app/shared/model/lpa-locations.model';

describe('Component Tests', () => {
    describe('LPALocations Management Update Component', () => {
        let comp: LPALocationsUpdateComponent;
        let fixture: ComponentFixture<LPALocationsUpdateComponent>;
        let service: LPALocationsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [LPALocationsUpdateComponent]
            })
                .overrideTemplate(LPALocationsUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(LPALocationsUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LPALocationsService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new LPALocations(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.lPALocations = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new LPALocations();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.lPALocations = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
