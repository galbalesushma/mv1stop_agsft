/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { IaaTestModule } from '../../../test.module';
import { AddressMvComponent } from 'app/entities/address-mv/address-mv.component';
import { AddressMvService } from 'app/entities/address-mv/address-mv.service';
import { AddressMv } from 'app/shared/model/address-mv.model';

describe('Component Tests', () => {
    describe('AddressMv Management Component', () => {
        let comp: AddressMvComponent;
        let fixture: ComponentFixture<AddressMvComponent>;
        let service: AddressMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AddressMvComponent],
                providers: []
            })
                .overrideTemplate(AddressMvComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AddressMvComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AddressMvService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new AddressMv(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.addresses[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
