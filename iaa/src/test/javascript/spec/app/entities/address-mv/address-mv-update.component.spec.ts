/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AddressMvUpdateComponent } from 'app/entities/address-mv/address-mv-update.component';
import { AddressMvService } from 'app/entities/address-mv/address-mv.service';
import { AddressMv } from 'app/shared/model/address-mv.model';

describe('Component Tests', () => {
    describe('AddressMv Management Update Component', () => {
        let comp: AddressMvUpdateComponent;
        let fixture: ComponentFixture<AddressMvUpdateComponent>;
        let service: AddressMvService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AddressMvUpdateComponent]
            })
                .overrideTemplate(AddressMvUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AddressMvUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AddressMvService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AddressMv(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.address = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new AddressMv();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.address = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
