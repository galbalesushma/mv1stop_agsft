/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IaaTestModule } from '../../../test.module';
import { AddressMvDetailComponent } from 'app/entities/address-mv/address-mv-detail.component';
import { AddressMv } from 'app/shared/model/address-mv.model';

describe('Component Tests', () => {
    describe('AddressMv Management Detail Component', () => {
        let comp: AddressMvDetailComponent;
        let fixture: ComponentFixture<AddressMvDetailComponent>;
        const route = ({ data: of({ address: new AddressMv(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [IaaTestModule],
                declarations: [AddressMvDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AddressMvDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AddressMvDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.address).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
