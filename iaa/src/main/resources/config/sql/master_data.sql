insert into zip_area values 
(1, 'INC A/B','00000');

insert into price_package values 
(1, 'First Page', 'ONE', 100, 'NA'), 
(2, 'Second Page', 'TWO', 50, 'NA'), 
(3, 'Third Page', 'THREE', 25, 'NA');

/************************     5th December 2018 ******************/     

ALTER TABLE `iaa`.`search_logs` 
ADD COLUMN `vehicle_type` VARCHAR(255) NULL DEFAULT NULL AFTER `searched_date`,
ADD COLUMN `vehicle_year` VARCHAR(45) NULL DEFAULT NULL AFTER `vehicle_type`,
ADD COLUMN `county` VARCHAR(45) NULL DEFAULT NULL AFTER `vehicle_year`,
ADD COLUMN `company` VARCHAR(45) NULL DEFAULT NULL AFTER `county`;


/************************     5th December 2018 *************/


/****************** View for search records  14th December 2018 ****************/


	create view search_records as 
		select a.id , a.npn , p.first_name , p.last_name, p.phone ,
		addr.zip , s.name as state, c.name as city, pc.package_id,
		z.zip as slot_zip , pc.slots , p.jhi_type, b.business_name
	from agent a 
		left join profile p
			on p.id = a.profile_id 
		left join business b
			on b.id = a.business_id
		left join address addr
			on addr.id = b.address_id 
		left join state s
			on s.id = addr.state_id
		left join city c
			on c.id = addr.city_id
		left join agent_lic lic
			on lic.id = a.lic_id
		left join producer pd
			on pd.id = lic.producer_id
		left join pack_subscription pc
			on pc.profile_id = p.id
		left join zip_area z
			on z.id = pc.zip_area_id
	where lic.is_active = 1 and pd.ce_compliance = 1;



/****************   View for search records End ****************/


/****************** View for search records facility 18th December 2018 ****************/

create view search_facility_records as 
		select f.id , f.isn , p.first_name , p.last_name, p.phone ,
		addr.zip , s.name as state, c.name as city, pc.package_id,
		z.zip as slot_zip , pc.slots , p.jhi_type, f.name
	from facility f 
		left join profile p
			on p.id = f.profile_id 
		left join business b
			on b.id = f.business_id
		left join address addr
			on addr.id = b.address_id 
		left join state s
			on s.id = addr.state_id
		left join city c
			on c.id = addr.city_id
		left join pack_subscription pc
			on pc.profile_id = p.id
		left join zip_area z
			on z.id = pc.zip_area_id;


/****************   View for search records facility End ****************/



/****************** add columns in facility 20th December 2018 ****************/

ALTER TABLE `iaa`.`facility` 
ADD COLUMN `insp_type` VARCHAR(45) NULL DEFAULT NULL AFTER `business_id`,
ADD COLUMN `mech_duty` VARCHAR(45) NULL DEFAULT NULL AFTER `insp_type`;


/****************    add columns in facility End ****************/


/****************** add columns in profile 26th December 2018 ****************/

ALTER TABLE `iaa`.`profile` 
ADD COLUMN `pref_company` VARCHAR(255) NULL DEFAULT NULL AFTER `addr_id`,
ADD COLUMN `comments` VARCHAR(250) NULL DEFAULT NULL AFTER `pref_company`;

/****************    add columns in profile End ****************/

/***************************  Alter view Added pref_company and comments *************************/

USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `search_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`
    FROM
        (((((((((`agent` `a`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
    WHERE
        ((`lic`.`is_active` = 1)
            AND (`pd`.`ce_compliance` = 1));



 
`root`@`localhost`

USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `search_facility_records` AS
    SELECT 
        `f`.`id` AS `id`,
        `f`.`isn` AS `isn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `f`.`name` AS `name`,
		`p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`
    FROM
        (((((((`facility` `f`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `f`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `f`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)));


/***************************  Alter view  End *************************/


/****************** add columns in facility/agent 31dec 2018 ****************/

ALTER TABLE `iaa`.`agent` 
ADD COLUMN `extension` VARCHAR(255) NULL AFTER `business_id`;

ALTER TABLE `iaa`.`facility` 
ADD COLUMN `extension` VARCHAR(255) NULL AFTER `mech_duty`;

/****************   add columns in facility/agent End ****************/

/****************** add cdr logs table 2nd jan 2018 ****************/


CREATE TABLE `iaa`.`cdr_logs` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `file_path` VARCHAR(255) NULL DEFAULT NULL,
  `parent_dir_filter_path` VARCHAR(255) NULL DEFAULT NULL,
  `file_created_at` BIGINT(11) NULL DEFAULT NULL,
  `file_last_modified_at` BIGINT(11) NULL DEFAULT NULL,
  `file_log_path` VARCHAR(255) NULL DEFAULT NULL,
  `process_status` VARCHAR(45) NULL DEFAULT NULL,
  `error_message` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `iaa`.`cdr_logs` 
ADD COLUMN `record_count` INT NULL DEFAULT 0 AFTER `error_message`;


ALTER TABLE `iaa`.`cdr_logs` 
ADD COLUMN `execution_time` BIGINT(11) NULL DEFAULT 0 AFTER `record_count`;


/****************   add cdr logs table  End ****************/


/****************** add cdr table 3rd jan 2018 ****************/

CREATE TABLE `iaa`.`cdr` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `calldate` DATETIME NULL DEFAULT NULL,
  `clid` VARCHAR(255) NULL DEFAULT NULL,
  `src` VARCHAR(45) NULL DEFAULT NULL,
  `dst` VARCHAR(45) NULL DEFAULT NULL,
  `dcontext` VARCHAR(100) NULL DEFAULT NULL,
  `channel` VARCHAR(255) NULL DEFAULT NULL,
  `dstchannel` VARCHAR(255) NULL DEFAULT NULL,
  `lastapp` VARCHAR(45) NULL DEFAULT NULL,
  `lastdata` VARCHAR(255) NULL DEFAULT NULL,
  `duration` VARCHAR(45) NULL DEFAULT NULL,
  `billsec` VARCHAR(45) NULL DEFAULT NULL,
  `disposition` VARCHAR(55) NULL DEFAULT NULL,
  `amaflags` VARCHAR(45) NULL DEFAULT NULL,
  `accountcode` VARCHAR(100) NULL DEFAULT NULL,
  `uniqueid` VARCHAR(45) NULL DEFAULT NULL,
  `userfield` VARCHAR(45) NULL DEFAULT NULL,
  `did` VARCHAR(45) NULL DEFAULT NULL,
  `recordingfile` VARCHAR(255) NULL DEFAULT NULL,
  `cnum` VARCHAR(45) NULL DEFAULT NULL,
  `cnam` VARCHAR(45) NULL DEFAULT NULL,
  `outbound_cnum` VARCHAR(45) NULL DEFAULT NULL,
  `outbound_cnam` VARCHAR(45) NULL DEFAULT NULL,
  `dst_cnam` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));



ALTER TABLE `iaa`.`cdr` 
ADD COLUMN `linkedid` VARCHAR(45) NULL DEFAULT NULL AFTER `dst_cnam`,
ADD COLUMN `peeraccount` VARCHAR(45) NULL DEFAULT NULL AFTER `linkedid`,
ADD COLUMN `sequence` VARCHAR(45) NULL DEFAULT NULL AFTER `peeraccount`;


/****************** add cdr table end ****************/


/******************  cdr view 10th jan 2018  ****************/


CREATE TABLE `iaa`.`cdr_bridge` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bridge` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));


insert into cdr_bridge values(1,9842372100);



create view cdr_records as 
		select c.id ,c.calldate,c.src,c.dst,c.linkedid,c.duration,
       a.id as aId, a.npn , a.extension , pr.first_name, pr.last_name , 
       ( select cd.dst from cdr cd   where  cd.linkedid = c.linkedid
		and cd.src in (select bridge from cdr_bridge) )aNumber
	from cdr c 
		left join agent a
			on a.extension = c.src 
		left join profile pr
			on pr.id = a.profile_id 
	where c.src IS NOT NULL  and c.src <> ''
    and (  a.npn IS NOT NULL  or 
    (a.npn IS NULL and  (select count(*) from cdr cdd where cdd.linkedid = c.linkedid)=1 ) )
		group by c.linkedid ,c.id ,c.calldate,c.src,c.dst,c.linkedid,
        aId, a.npn , a.extension , pr.first_name, pr.last_name
        
        ;









/****************** add  cdr view 10th jan 2018 ****************/




/********************* Add insurance_company *************************/

ALTER TABLE `iaa`.`profile` 
ADD COLUMN `insurance_company` VARCHAR(255) NULL AFTER `comments`;


/********************* Add insurance_company end *************************/


/********************* Add insurance_company  in view  start *************************/


USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `search_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `p`.`insurance_company` AS `insurance_company`
    FROM
        (((((((((`agent` `a`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
    WHERE
        ((`lic`.`is_active` = 1)
            AND (`pd`.`ce_compliance` = 1));

/********************* Add insurance_company in view end *************************/






/********************* Create DMV Loaction master  *************************/


CREATE TABLE `dmv_location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `directions` varchar(255) DEFAULT NULL,
  `fax_number` varchar(255) DEFAULT NULL,
  `main_phone` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `saturday_hrs` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `weekdays_hrs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `dmv_location` VALUES (1,'111 East Crescent Square','Graham','Alamance','https://www.bing.com/maps/default.aspx?where1=111%20EAST%20CRESCENT%20SQUARE%20GRAHAM,%20NC%2027253&amp;rtop=0~1~0','','(336) 570-6811','Graham','27253','','NC','8:00 am - 5:00 pm'),(2,'2390 NC Highway 90 E','Taylorsville','Alexander','https://www.bing.com/maps/default.aspx?where1=2390%20NC%20HIGHWAY%2090%20E.%20TAYLORSVILLE,%20NC%2028681&amp;rtop=0~1~0','','(828) 632-1159','Taylorsville','28681','','NC','8:00 am - 5:00 pm'),(3,'115 Atwood St, Suite 508','Sparta','Alleghany','https://www.bing.com/maps/default.aspx?where1=115%20ATWOOD%20ST.%20SUITE%20508%20SPARTA,%20NC%2028675&amp;rtop=0~1~0','','(336) 372-6442','Blue Ridge Business Development Center','28675','','NC','8:30 am - 4:30 pm'),(4,'5920 Highway 74 W','Polkton','Anson','https://www.bing.com/maps/default.aspx?where1=5920%20HIGHWAY%2074%20W.%20POLKTON,%20NC%2028135&amp;rtop=0~1~0','(704) 694-6348','(704) 694-5534','Polkton','28135','','NC','8:00 am - 5:00 pm'),(5,'140 Government Circle','Jefferson','Ashe','https://www.bing.com/maps/default.aspx?where1=140%20GOVERNMENT%20CIRCLE%20JEFFERSON,%20NC%2028640&amp;rtop=0~1~0','(336) 246-4568','(336) 246-5001','Ashe County Law Enforcement Center','28640','','NC','8:00 am - 5:00 pm'),(6,'301 Cranberry Street','Newland','Avery','https://www.bing.com/maps/default.aspx?where1=301%20CRANBERRY%20STREET%20NEWLAND,%20NC%2028657&amp;rtop=0~1~0','(828) 733-2069','(828) 733-8284','Newland Town Hall','28567','','NC','8:30 am - 4:30 pm'),(7,'1821 Carolina Avenue U.S Highway 17 N','Washington','Beaufort','https://www.bing.com/maps/default.aspx?where1=1821%20CAROLINA%20AVENUE%20(U.S.%20HIGHWAY%2017%20N.)%20WASHINGTON,%20NC%2027889&amp;rtop=0~1~0','','(252) 946-3995','Washington','27889','','NC','8:00 am - 5:00 pm'),(8,'197 S. Pine St','Elizabethtown','Bladen','https://www.bing.com/maps/default.aspx?where1=197%20S%20PINE%20ST%20ELIZABETHTOWN,%20NC%2028337-9162&amp;rtop=0~1~0','(910) 862-0918','(910) 862-3169','Elizabthtown','28337','','NC','8:00 am - 5:00 pm'),(9,'5298 Main Street','Shallotte','Brunswick','https://www.bing.com/maps/default.aspx?where1=5298%20MAIN%20ST%20SHALLOTTE,%20NC%2028470-3438&amp;rtop=0~1~0','(910) 754-9141','(910) 754-5114','Shallotte','28470','','NC','8:00 am - 5:00 pm'),(10,'111 East Nash St','Southport','Brunswick','https://www.bing.com/maps/default.aspx?where1=111%20E%20NASH%20ST%20SOUTHPORT,%20NC%2028461-3933&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at Fire Department Downtown','28461','','NC',''),(11,'1624 Oatton Drive','Asheville','Buncombe','https://www.bing.com/maps/default.aspx?where1=1624%20PATTON%20AVENUE%20ASHEVILLE,%20NC%2028806&amp;rtop=0~1~0','(828) 782-9600','(828) 232-2436','Asheville','28806','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(12,'115 Government Drive','Morganton','Burke','https://www.bing.com/maps/default.aspx?where1=115%20GOVERNMENT%20DRIVE%20MORGANTON,%20NC%2028655&amp;rtop=0~1~0','(828) 433-4042','(828) 438-6294','Morganton','28655','','NC','8:00 am - 5:00 pm'),(13,'2192 Kannapolis Highway','Concord','Cabarrus','https://www.bing.com/maps/default.aspx?where1=2192%20KANNAPOLIS%20HIGHWAY%20CONCORD,%20NC%2028027&amp;rtop=0~1~0','(704) 792-0222','(704) 782-7214','Concord','28027','','NC','8:00 am - 5:00 pm'),(14,'309 Pine Mountain Road','Hudson','Caldwell','https://www.bing.com/maps/default.aspx?where1=309%20PINE%20MOUNTAIN%20ROAD%20HUDSON,%20NC%2028638&amp;rtop=0~1~0','(828) 726-2519','(828) 726-2504','Hudson','28638','','NC','8:00 am - 5:00 pm'),(15,'5347 US 70 W','Morehead City','Carteret','https://www.bing.com/maps/default.aspx?where1=5347%20US%2070%20W.%20MOREHEAD%20CITY,%20NC%2028557&amp;rtop=0~1~0','(252) 808-2094','(252) 726-5586','Morehead City','28557','','NC','8:00 am - 5:00 pm'),(16,'958 Fire Tower Road','Yanceyville','Caswell','https://www.bing.com/maps/default.aspx?where1=958%20FIRE%20TOWER%20ROAD%20YANCEYVILLE,%20NC%2027379&amp;rtop=0~1~0','','(336) 694-9498','Yanceyville','27379','','NC',''),(17,'1158 Lenoir-Rhyne Blvd SE','Hickory','Catawba','https://www.bing.com/maps/default.aspx?where1=1158%20LENOIR-RHYNE%20BOULEVARD%20SE%20HICKORY,%20NC%2028602&amp;rtop=0~1~0','(828) 326-7096','(828) 326-9126','Lenoir-Rhyne Plaza','28602','','NC','8:00 am - 5:00 pm'),(18,'1033 Smyre Farm Road','Newton','Catawba','https://www.bing.com/maps/default.aspx?where1=1033%20SMYRE%20FARM%20ROAD%20NEWTON,%20NC%2028658&amp;rtop=0~1~0','(828) 466-5603','(828) 466-5516','Newton','28658','','NC','8:00 am - 5:00 pm'),(19,'1103 North 2nd Ave. Exit U.S. 421 /64W','Siler City','Chatham','https://www.bing.com/maps/default.aspx?where1=1103%20NORTH%202ND%20AVENUE%20EXIT%20(U.S.%20421%20S/64%20W)%20SILER%20CITY,%20NC%2027344&amp;rtop=0~1~0','(919) 663-2581','(919) 663-2601','Siler City','27344','','NC','8:00 am - 5:00 pm'),(20,'1440 Main Street','Andrews','Cherokee','https://www.bing.com/maps/default.aspx?where1=1440%20MAIN%20ST.%20ANDREWS,%20NC%2028901&amp;rtop=0~1~0','(828) 321-1443','(828) 321-1442','Andrews','28901','','NC','8:00 am - 5:00 pm'),(21,'307 West Freemason Street','Edenton','Chowan','https://www.bing.com/maps/default.aspx?where1=307%20W%20FREEMASON%20ST%20EDENTON,%20NC%2027932-1878&amp;rtop=0~1~0','(252) 482-2285','(252) 482-8941','Edenton ','27932','','NC','8:00 am - 5:00 pm'),(22,'1 Riverside Circle','Hayesville','Clay','https://www.bing.com/maps/default.aspx?where1=1%20RIVERSIDE%20CIRCLE%20HAYESVILLE,%20NC%2028904&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Clay County Social Services','28904','','NC',''),(23,'US 74 Bypass 1914 East Dixon Blvd','Shelby','Cleveland','https://www.bing.com/maps/default.aspx?where1=US%2074%20BYPASS%201914%20EAST%20DIXON%20BOULEVARD%20SHELBY,%20NC%2028152&amp;rtop=0~1~0','(704) 480-5498','(704) 480-5408','Shelby','28152','','NC','8:00 am - 5:00 pm'),(24,'917 Washington Street','Whiteville','Columbus','https://www.bing.com/maps/default.aspx?where1=917%20WASHINGTON%20STREET%20WHITEVILLE,%20NC%2028472&amp;rtop=0~1~0','(910) 640-2551','(910) 642-2017','Whiteville','28472','','NC','8:00 am - 5:00 pm'),(25,'300 Miller Blvd','Havelock','Craven','https://www.bing.com/maps/default.aspx?where1=300%20MILLER%20BOULEVARD%20HAVELOCK,%20NC%2028532&amp;rtop=0~1~0','(252) 444-4175','(252) 444-6425','Craven County Office Building','28532','','NC','8:00 am - 5:00 pm'),(26,'2106 Neuse Blvd','New Bern','Craven','https://www.bing.com/maps/default.aspx?where1=2106%20NEUSE%20BOULEVARD%20NEW%20BERN,%20NC%2028560&amp;rtop=0~1~0','(252) 514-4791','(252) 514-4734','New Bern','28560','','NC','8:00 am - 5:00 pm'),(27,'2439 Gillespie Street','Fayetteville','Cumberland','https://www.bing.com/maps/default.aspx?where1=2439%20GILLESPIE%20ST.%20FAYETTEVILLE,%20NC%2028303&amp;rtop=0~1~0','(910) 437-0252','(910) 486-1353','South Fayetteville','28303','','NC','7:00 am - 5:00 pm'),(28,'831 Elm St','Fayetteville','Cumberland','https://www.bing.com/maps/default.aspx?where1=831%20ELM%20ST%20FAYETTEVILLE,%20NC%2028303-4151&amp;rtop=0~1~0','(910) 484-6517','(910) 484-6249','Eutaw Village Shopping Center','28303','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(29,'4705 Clinton Rd','Stedman','Cumberland','https://www.bing.com/maps/default.aspx?where1=4705%20CLINTON%20ROAD%20STEDMAN,%20NC%2028312-8524&amp;rtop=0~1~0','(910) 483-3096','(910) 483-7670','Stedman','28312','','NC','8:00 am - 5:00 pm'),(30,'2843 Normandy Drive','Fort Bragg','Cumberland','https://www.bing.com/maps/default.aspx?where1=2843%20NORMANDY%20DRIVE%20FORT%20BRAGG,%20NC%2028307&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Fort Bragg Soldier Center','28307','','NC',''),(31,'120 Community Way','Barco','Currituck','https://www.bing.com/maps/default.aspx?where1=120%20COMMUNITY%20WAY%20BARCO,%20NC%2027917-9559&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the NC Cooperative Extension-Currituck County Center','27917','','NC',''),(32,'57709 NC Highway 12','Hatteras','Dare','https://www.bing.com/maps/default.aspx?where1=57709%20NC%20HIGHWAY%2012%20HATTERAS,%20NC%2027943&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Hatteras Civic Center','27943','','NC',''),(33,'2808 S Croatan Hwy','Nags Head','Dare','https://www.bing.com/maps/default.aspx?where1=2808%20S%20CROATAN%20HWY%20NAGS%20HEAD,%20NC%2027959-9024&amp;rtop=0~1~0','(252) 480-6467','(252) 480-6465','Nags Head','27595','','NC','8:00 am - 5:00 pm'),(34,'2314 South Main Street','Lexington','Davidson','https://www.bing.com/maps/default.aspx?where1=2314%20SOUTH%20MAIN%20STREET%20LEXINGTON,%20NC%2027292&amp;rtop=0~1~0','(336) 248-8905','(336) 248-5179','Lexington','27292','','NC','8:00 am - 5:00 pm'),(35,'1033 Randolph Street, Suite 16','Thomasville','Davidson','https://www.bing.com/maps/default.aspx?where1=1033%20RANDOLPH%20STREET%20SUITE%2016%20THOMASVILLE,%20NC%2027360&amp;rtop=0~1~0','(336) 472-0144','(336) 472-7334','Thomasville','27360','','NC','8:00 am - 5:00 pm'),(36,'101 W. Newsome Avenue','Denton','Davidson','https://www.bing.com/maps/default.aspx?where1=101%20W.%20NEWSOME%20AVENUE%20DENTON,%20NC%2027239&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Denton Fire Department','27239','','NC',''),(37,'161 Poplar Street, Suite 101','Mocksville','Davie','https://www.bing.com/maps/default.aspx?where1=161%20POPLAR%20STREET%20SUITE%20101%20MOCKSVILLE,%20NC%2027028&amp;rtop=0~1~0','(336) 751-2643','(336) 751-5016','Mocksville','27028','','NC','8:00 am - 5:00 pm'),(38,'133 Routledge Street','Kenansville','Duplin','https://www.bing.com/maps/default.aspx?where1=133%20ROUTLEDGE%20ST%20KENANSVILLE,%20NC%2028349-8002&amp;rtop=0~1~0','(910) 296-1546','(910) 296-0234','Driver License Office Adjacent to the Kenansville Town Hall','28349','','NC','8:00 am - 5:00 pm'),(39,'101 South Miami Blvd','Durham','Durham','https://www.bing.com/maps/default.aspx?where1=101%20SOUTH%20MIAMI%20BOULEVARD%20DURHAM,%20NC%2027703&amp;rtop=0~1~0','(919) 560-6896','(919) 560-5431','East Durham','27703','','NC','8:00 am - 5:00 pm'),(40,'3825 South Roxboro St, Sye 119','Durham','Durham','https://www.bing.com/maps/default.aspx?where1=3825%20SOUTH%20ROXBORO%20STREET%20SUITE%20119%20DURHAM,%20NC%2027713&amp;rtop=0~1~0','(919) 560-5432','(919) 560-3378','Shops of Hope Valley Shopping Center','27713','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(41,'125 East Granville Street','Tarboro','Edgecombe','https://www.bing.com/maps/default.aspx?where1=125%20E%20GRANVILLE%20STREET%20TARBORO,%20NC%2027886&amp;rtop=0~1~0','','(252) 823-0242','Tarboro','27886','','NC','8:00 am - 5:00 pm'),(42,'3637 North Patterson Avenue','Winston Salem','Forsyth','https://www.bing.com/maps/default.aspx?where1=3637%20NORTH%20PATTERSON%20AVENUE%20WINSTON-SALEM,%20NC%2027105&amp;rtop=0~1~0','(336) 761-2036','(336) 761-2259','Winston Salem North','27105','','NC','7:00 am - 5:00 pm'),(43,'2001 Silas Creek Parkway','Winston Salem','Forsyth','https://www.bing.com/maps/default.aspx?where1=2001%20SILAS%20CREEK%20PARKWAY%20WINSTON-SALEM,%20NC%2027103&amp;rtop=0~1~0','(336) 761-2244','(336) 761-2258','Winston Salem South','27103','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(44,'810 A North Main Street','Kernersville','Forsyth','https://www.bing.com/maps/default.aspx?where1=810A%20NORTH%20MAIN%20STREET%20KERNERSVILLE,%20NC%2027284&amp;rtop=0~1~0','','(336) 993-5651','North Main Crossing','27284','','NC','8:00 am - 5:00 pm'),(45,'90 Tanglewood Drive','Louisburg','Franklin','https://www.bing.com/maps/default.aspx?where1=90%20TANGLEWOOD%20DRIVE%20LOUISBURG,%20NC%2027549-2698&amp;rtop=0~1~0','(919) 496-1415','(919) 496-2590','Louisburg','27549','','NC','8:00 am - 5:00 pm'),(46,'2560 West Franklin Blvd','Gastonia','Gaston','https://www.bing.com/maps/default.aspx?where1=2560%20W%20FRANKLIN%20BLVD%20GASTONIA,%20NC%2028052-1250&amp;rtop=0~1~0','(704) 853-5393','(704) 853-5372','Gastonia','28052','','NC','8:00 am - 5:00 pm'),(47,'785 W Charlotte Ave','Mount Holly','Gaston','https://www.bing.com/maps/default.aspx?where1=785%20W%20CHARLOTTE%20AVENUE%20MOUNT%20HOLLY,%20NC%2028120&amp;rtop=0~1~0','(704) 827-8403','(704) 827-9486','Mount Holly Municipal Buildiing ','28120','','NC','8:00 am - 5:00 pm'),(48,'130 US Highway 158 W','Gatesville ','Gates','https://www.bing.com/maps/default.aspx?where1=130%20US%20HIGHWAY%20158%20W%20GATESVILLE,%20NC%2027938-9437&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Gates County Communty Center','27938','','NC',''),(49,'196 Knight Street','Robbinsville','Graham','https://www.bing.com/maps/default.aspx?where1=196%20KNIGHT%20STREET%20ROBBINSVILLE,%20NC%2028771&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Graham County Social Services','28771','','NC',''),(50,'100 providence Rd','Oxford','Granville','https://www.bing.com/maps/default.aspx?where1=100%20PROVIDENCE%20RD%20OXFORD,%20NC%2027565-3162&amp;rtop=0~1~0','(919) 690-0107','(919) 693-6128','Oxford','27565','','NC','8:00 am - 5:00 pm'),(51,'108 Wilton Ave','Creedmoor','Granville','https://www.bing.com/maps/default.aspx?where1=108%20WILTON%20AVENUE%20CREEDMOOR,%20NC%2027522&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the South Granville Senior Center','27522','','NC',''),(52,'229 Kingold Blvd','Snow Hill','Greene','https://www.bing.com/maps/default.aspx?where1=229%20KINGOLD%20BOULEVARD%20SNOW%20HILL,%20NC%2028580&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the County Office Complex Library','28580','','NC',''),(53,'2527 East Market Street','Greensboro','Guilford','https://www.bing.com/maps/default.aspx?where1=2527%20E%20MARKET%20STREET%20GREENSBORO,%20NC%2027401&amp;rtop=0~1~0','(336) 334-5131','(336) 334-5745','East Greensboro','27401','','NC','7:00 am - 5:00 pm'),(54,'2391 Coliseum Blvd','Greensboro','Guilford','https://www.bing.com/maps/default.aspx?where1=2391%20COLISEUM%20BOULEVARD%20GREENSBORO,%20NC%2027403&amp;rtop=0~1~0','(336) 256-0549','(336) 334-5438','West Greensboro','27403','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(55,'650 Francis Street','High Point','Guilford','https://www.bing.com/maps/default.aspx?where1=650%20FRANCIS%20STREET%20HIGH%20POINT,%20NC%2027263&amp;rtop=0~1~0','(336) 884-7399','(336) 884-1003','High Point','27623','','NC','7:00 am - 5:00 pm'),(56,'26 Three Bridges Road','Roanoke Rapids','Halifax','https://www.bing.com/maps/default.aspx?where1=26%20THREE%20BRIDGES%20ROAD%20ROANOKE%20RAPIDS,%20NC%2027870&amp;rtop=0~1~0','(252) 536-0155','(252) 536-4046','Roanoke Rapids','27870','','NC','8:00 am - 5:00 pm'),(57,'1403 Church St','Scotland','Halifax','https://www.bing.com/maps/default.aspx?where1=1403%20CHURCH%20ST%20SCOTLAND%20NECK,%20NC%2027874-1336&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Senior Center','27874','','NC',''),(58,'1005 Edwards Brothers Drive','Lillington','Harnett','https://www.bing.com/maps/default.aspx?where1=1005%20EDWARDS%20BROTHERS%20DRIVE%20LILLINGTON,%20NC%2027546&amp;rtop=0~1~0','(910) 893-9054','(910) 893-8939','Harnett County Emergency Services','27546','','NC','8:00 am - 5:00 pm'),(59,'125 West Jackson Blvd','Erwin','Harnett','https://www.bing.com/maps/default.aspx?where1=125%20WEST%20JACKSON%20BOULEVARD%20ERWIN,%20NC%2028339&amp;rtop=0~1~0','(910) 891-5685','(910) 892-1456','Erwin','28339','','NC','8:00 am - 5:00 pm'),(60,'290 Lee Road','Clyde','Haywood','https://www.bing.com/maps/default.aspx?where1=290%20LEE%20ROAD%20CLYDE,%20NC%2028721&amp;rtop=0~1~0','(828) 627-3659','(828) 627-6969','Clyde','28721','','NC','8:00 am - 5:00 pm'),(61,'125 Baystone Drive','Hendersonville','Henderson','https://www.bing.com/maps/default.aspx?where1=125%20BAYSTONE%20DRIVE%20%20HENDERSONVILLE,%20NC%2028791&amp;rtop=0~1~0','(828) 692-5996','(828) 692-6915','Hendersonville','28791','','NC','8:00 am - 5:00 pm'),(62,'242 NC 42 West','Ahoskie','Hertford','https://www.bing.com/maps/default.aspx?where1=242%20NC%2042%20WEST%20AHOSKIE,%20NC%2027910&amp;rtop=0~1~0','(252) 332-1635','(252) 332-5525','Ahoski','27910','','NC','8:00 am - 5:00 pm'),(63,'3144 Highway 401 Business','Raeford','Hoke','https://www.bing.com/maps/default.aspx?where1=3144%20HIGHWAY%20401%20BUSINESS%20RAEFORD,%20NC%2028376&amp;rtop=0~1~0','(910) 904-0574','(910) 875-2442','Civil Preparedness Building','28376','','NC','8:00 am - 5:00 pm'),(64,'822 Irvin Garrish Highway','Ocracoke','Hyde','https://www.bing.com/maps/default.aspx?where1=822%20IRVIN%20GARRISH%20HIGHWAY%20OCRACOKE,%20NC%2027960&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Volunteer Fire Department','27960','','NC',''),(65,'13 Main St','Swan Quarter','Hyde','https://www.bing.com/maps/default.aspx?where1=13%20MAIN%20ST%20SWAN%20QUARTER,%20NC%2027885-9382&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Hyde County Government Bldg','27885','','NC',''),(66,'533 Patterson Ave, Suite 100','Mooresville','Iredell','https://www.bing.com/maps/default.aspx?where1=533%20PATTERSON%20AVE%20SUITE%20100%20MOORESVILLE,%20NC%2028115&amp;rtop=0~1~0','(704) 664-7464','(704) 664-3344','Design Center','28115','','NC','8:00 am - 5:00 pm'),(67,'905 Carolina Avenue North','Statesville','Iredell','https://www.bing.com/maps/default.aspx?where1=905%20CAROLINA%20AVENUE%20NORTH%20STATESVILLE,%20NC%2028677&amp;rtop=0~1~0','','(704) 878-4220','Statesville','28677','','NC','8:00 am - 5:00 pm'),(68,'1028 Turnersburg Hwy','Statesville','Iredell','https://www.bing.com/maps/default.aspx?where1=1028%20TURNERSBURG%20HWY%20STATESVILLE,%20NC%2028625&amp;rtop=0~1~0','(704) 876-1089','(704) 876-1052','Commercial Driver License Skills Testing Site','28625','','NC','8:00 am - 5:00 pm'),(69,'876 Skyland Drive, Ste 2','Sylva','Jackson','https://www.bing.com/maps/default.aspx?where1=876%20SKYLAND%20DRIVE,%20SUITE%202%20SYLVA,%20NC%2028779&amp;rtop=0~1~0','','(828) 586-5413','Skyland Service Center','28779','','NC','8:00 am - 5:00 pm'),(70,'1665 Old US 70 Hwy W','Clayton','Johnston','https://www.bing.com/maps/default.aspx?where1=1665%20OLD%20US%2070%20HWY%20W%20CLAYTON,%20NC%2027520-6566&amp;rtop=0~1~0','(919) 550-2351','(919) 550-2425','Shotwell Station','27520','','NC','8:00 am - 5:00 pm'),(71,'3783 US 301 South','Smithfield','Johnston','https://www.bing.com/maps/default.aspx?where1=3783%20US%20301%20SOUTH%20SMITHFIELD,%20NC%2027577&amp;rtop=0~1~0','(919) 934-2344','(919) 934-3187','Smithfield','27577','','NC','8:00 am - 5:00 pm'),(72,'NC Highway 58','Trenton','Jones','https://www.bing.com/maps/default.aspx?where1=N.C%20HIGHWAY%2058%20TRENTON,%20NC%2028585&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Civic Center','28585','','NC',''),(73,'2210 arthage Street','Sanford','Lee','https://www.bing.com/maps/default.aspx?where1=2210%20CARTHAGE%20STREET%20SANFORD,%20NC%2027330&amp;rtop=0~1~0','(919) 774-4803','(919) 776-1113','Sanford','27330','','NC','8:00 am - 5:00 pm'),(74,'2214 West Vernon Ave','Kinston','Lenoir','https://www.bing.com/maps/default.aspx?where1=2214%20WEST%20VERNON%20AVENUE%20KINSTON,%20NC%2028501&amp;rtop=0~1~0','(252) 526-6763','(252) 526-4432','Kinston','28501','','NC','7:00 am - 5:00 pm'),(75,'1450 North Aspen Street','Lincolnton','Lincoln','https://www.bing.com/maps/default.aspx?where1=1450%20NORTH%20ASPEN%20STREET%20LINCOLNTON,%20NC%2028092&amp;rtop=0~1~0','(704) 735-3956','(704) 735-6923','Lincolnton','28092','','NC','8:00 am - 5:00 pm'),(76,'16 Patton Ave','Franklin','Macon','https://www.bing.com/maps/default.aspx?where1=16%20PATTON%20AVE%20FRANKLIN,%20NC%2028734-3043&amp;rtop=0~1~0','(828) 524-3486','(828) 524-3592','Franklin','28734','','NC',''),(77,'164 North Main Street','Marshall','Madison','https://www.bing.com/maps/default.aspx?where1=164%20NORTH%20MAIN%20STREET%20MARSHALL,%20NC%2028753&amp;rtop=0~1~0','(828) 649-3394','(828) 649-2248','Jackie Ball Building','28753','','NC','8:00 am - 5:00 pm'),(78,'305 E Main St, Ste 116','Williamston','Martin','https://www.bing.com/maps/default.aspx?where1=305%20E%20MAIN%20ST.%20%20%20%20%20%20%20%20%20%20STE%20116%20WILLIAMSTON,%20NC%2027892-2566&amp;rtop=0~1~0','','(252) 789-4395','Martin County Government Center','27892','','NC','8:00 am - 5:00 pm'),(79,'3975 226 South','Marion','McDowell','https://www.bing.com/maps/default.aspx?where1=3975%20NC%20226%20SOUTH%20MARION,%20NC%2028752&amp;rtop=0~1~0','(828) 652-3557','(828) 652-5828','Marion','28752','','NC','8:00 am - 5:00 pm'),(80,'6635 Executive Circle, Ste 130','Charlotte','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=6635%20EXECUTIVE%20CIRCLE,%20SUITE%20130%20CHARLOTTE,%20NC%2028212&amp;rtop=0~1~0','(704) 531-5628','(704) 531-5563','East Charlotte','28212','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(81,'201-H West Arrowwood Road','Charlotte','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=201-H%20WEST%20ARROWOOD%20ROAD%20CHARLOTTE,%20NC%2028217&amp;rtop=0~1~0','(704) 527-6518','(704) 527-2562','South Charlotte','28217','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(82,'6016 Brookshire Blvd','Charlotte','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=6016%20BROOKSHIRE%20BOULEVARD%20CHARLOTTE,%20NC%2028216&amp;rtop=0~1~0','(704) 392-7889','(704) 392-3266','West Charlotte','28216','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(83,'12101 Mt Holly-Huntersville Rd','Huntersville','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=12101%20MT.%20HOLLY-HUNTERSVILLE%20ROAD%20HUNTERSVILLE,%20NC%2028078&amp;rtop=0~1~0','(704) 331-4585','(704) 547-5786','Huntersville','28078','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(84,'118 College Street','Pineville','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=118%20COLLEGE%20ST%20PINEVILLE,%20NC%2028134-9706&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at Pineville Communications Systems','28134','','NC',''),(85,'1032 Oak Ave','Spruce Pine','Mitchell','https://www.bing.com/maps/default.aspx?where1=1032%20OAK%20AVE%20SPRUCE%20PINE,%20NC%2028777-2834&amp;rtop=0~1~0','(828) 766-7636','(828) 766-7649','Spruce Pine','28777','','NC','8:00 am - 4:30 pm'),(86,'168 Glenn Road','Tryo','Montgomery','https://www.bing.com/maps/default.aspx?where1=168%20GLENN%20ROAD%20TROY,%20NC%2027371&amp;rtop=0~1~0','','(910) 572-2001','Troy','27371','','NC','8:00 am - 5:00 pm'),(87,'521 South Sandhills Blvd','Aberdeen','Moore','https://www.bing.com/maps/default.aspx?where1=521%20SOUTH%20SANDHILLS%20BOULEVARD%20ABERDEEN,%20NC%2028315&amp;rtop=0~1~0','(910) 944-3087','(910) 944-7555','Aberdeen','28315','','NC','8:00 am - 5:00 pm'),(88,'161 East Magnolia Drive','Robbins ','Moore','https://www.bing.com/maps/default.aspx?where1=161%20EAST%20MAGNOLIA%20DRIVE%20ROBBINS,%20NC%2027325&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Robbins Area Library','27325','','NC',''),(89,'2617 North Wsesleyan Blvd','Rocky Mount','Nash','https://www.bing.com/maps/default.aspx?where1=2617%20NORTH%20WESLEYAN%20BOULEVARD%20ROCKY%20MOUNT,%20NC%2027804&amp;rtop=0~1~0','(252) 985-5769','(252) 442-8905','Rocky Mount','27804','','NC','8:00 am - 5:00 pm'),(90,'One Station Rd','Wilmington','New Hanover','https://www.bing.com/maps/default.aspx?where1=ONE%20STATION%20ROAD%20%20WILMINGTON,%20NC%2028405&amp;rtop=0~1~0','','(910) 350-2005','Wilmington North','28405','','NC','7:00 am - 5:00 pm'),(91,'2390 Carolina Beach Rd, Ste 104','Wilmington','New Hanover','https://www.bing.com/maps/default.aspx?where1=2390%20CAROLINA%20BEACH%20RD;%20%20%20%20%20%20%20%20STE%20104%20WILMINGTON,%20NC%2028401-7629&amp;rtop=0~1~0','','(910) 251-5747','South Square Plaza','28401','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(92,'299 Wilmington Highway','Jacksonville','Onslow','https://www.bing.com/maps/default.aspx?where1=299%20WILMINGTON%20HIGHWAY%20JACKSONVILLE,%20NC%2028540&amp;rtop=0~1~0','(910) 938-5804','(910) 347-3613','Jacksonville','28540','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(93,'84 Holcomb Blvd','Camp Lejeune','Onslow','https://www.bing.com/maps/default.aspx?where1=84%20HOLCOMB%20BLVD.%20CAMP%20LEJEUNE,%20NC%2028540&amp;rtop=0~1~0','','','Mobile Unit at Camp Lejeune Hadnot Point Plaza','28540','','NC',''),(94,'104 NC Highway 54, Ste GG','Carrboro','Orange','https://www.bing.com/maps/default.aspx?where1=104%20NC%20HIGHWAY%2054,%20SUITE%20GG%20CARRBORO,%20NC%2027510&amp;rtop=0~1~0','(919) 929-8821','(919) 929-4161','Carrboro Plaza Shopping Center','27510','','NC','7:00 am - 5:00 pm'),(95,'1201 US Highway 70 W','Hillsborough','Orange','https://www.bing.com/maps/default.aspx?where1=1201%20US%20HIGHWAY%2070%20W%20HILLSBOROUGH,%20NC%2027278-7004&amp;rtop=0~1~0','(919) 732-1799','(919) 732-2427','Hillsborough','27278','','NC','8:00 am - 5:00 pm'),(96,'828 Alliance St','Bayboro','Pamlico','https://www.bing.com/maps/default.aspx?where1=828%20ALLIANCE%20ST.%20BAYBORO,%20NC%2028515&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Pamlico County Human Servicese Center','28515','','NC',''),(97,'1164 US 17 South','Pasquotank','Pasquotank','https://www.bing.com/maps/default.aspx?where1=1164%20US%2017%20SOUTH%20ELIZABETH%20CITY,%20NC%2027909&amp;rtop=0~1~0','(252) 331-4843','(252) 331-4776','Elizabeth City','27909','','NC','7:00 am - 5:00 pm'),(98,'781 U.S. 117 South','Burgaw','Pender','https://www.bing.com/maps/default.aspx?where1=781%20U.S.%20117%20SOUTH%20BURGAW,%20NC%2028425&amp;rtop=0~1~0','(910) 259-5417','(910) 259-2799','Burgaw','28425','','NC','8:00 am - 5:00 pm'),(99,'114 West Grubb St','Hertford','Perquimans','https://www.bing.com/maps/default.aspx?where1=114%20W.%20GRUBB%20ST.%20HERTFORD,%20NC%2027944&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Perquimans County Municipal Building','27944','','NC',''),(100,'3434 Burlington Road','Roxboro','Person','https://www.bing.com/maps/default.aspx?where1=3434%20BURLINGTON%20ROAD%20ROXBORO,%20NC%2027573&amp;rtop=0~1~0','(336) 597-8951','(336) 597-5636','Roxboro','27573','','NC','8:00 am - 5:00 pm'),(101,'703 SE Greenville Blvd, Unit #124','Greenville','Pitt','https://www.bing.com/maps/default.aspx?where1=703%20SE%20GREENVILLE%20BLVD.,%20UNIT%20#124GREENVILLE,NC27858&amp;rtop=0~1~0','(252) 830-3457','(252) 830-3456','Greenville Square','27858','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(102,'51 Walker Street','Columbus','Polk','https://www.bing.com/maps/default.aspx?where1=51%20WALKER%20ST%20COLUMBUS,%20NC%2028722-7497&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Polk County License Plate Agency','28722','','NC',''),(103,'2754 US Highway 220 Business South','Ashevboro','Randolph','https://www.bing.com/maps/default.aspx?where1=2754%20US%20HIGHWAY%20220%20BUSINESS%20SOUTH%20ASHEBORO,%20NC%2027205&amp;rtop=0~1~0','(336) 626-0635','(336) 629-1949','Asheboro','27205','','NC','8:00 am - 5:00 pm'),(104,'200 College Drive','Hamlet','Richmond','https://www.bing.com/maps/default.aspx?where1=200%20COLLEGE%20DR%20HAMLET,%20NC%2028345-4637&amp;rtop=0~1~0','(910) 582-7218','(910) 582-7032','Hamlet','28345','','NC','8:00 am - 5:00 pm'),(105,'4650 Kahn Drive','Lumberton','Robeson','https://www.bing.com/maps/default.aspx?where1=4650%20KAHN%20DRIVE%20LUMBERTON,%20NC%2028358-2308&amp;rtop=0~1~0','(910) 618-5552','(910) 618-5551','Lumberton','28358','','NC','8:00 am - 5:00 pm'),(106,'2479 East Fifth St','Lumberton','Robeson','https://www.bing.com/maps/default.aspx?where1=2479%20E%20FIFTH%20STREET%20LUMBERTON,%20NC%2028358-6809&amp;rtop=0~1~0','','(910) 618-5527','Commercial Driver License Skills Testing Site','28358','','NC','8:00 am - 5:00 pm'),(107,'100 South Union Chapel Rd','Pembroke','Robeson','https://www.bing.com/maps/default.aspx?where1=100%20SOUTH%20UNION%20CHAPEL%20ROAD%20PEMBROKE,%20NC%2028372&amp;rtop=0~1~0','','(910) 668-1492','Pembroke','28372','','NC','8:00 am - 5:00 pm'),(108,'210 West Blue Street','St. Pauls','Robeson','https://www.bing.com/maps/default.aspx?where1=210%20WEST%20BLUE%20STREET%20ST.%20PAULS,%20NC%2028384&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the St Pauls Police Station','28384','','NC',''),(109,'103 Cottage St','Fairmont','Robeson','https://www.bing.com/maps/default.aspx?where1=103%20COTTAGE%20STREET%20FAIRMONT,%20NC%2028340&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Fairmont Fire Department','28340','','NC',''),(110,'111 Wilson-Way Road','Reidsville','Rockingham','https://www.bing.com/maps/default.aspx?where1=111%20WILSON-WAY%20ROAD%20REIDSVILLE,%20NC%2027320&amp;rtop=0~1~0','(336) 634-5678','(336) 634-5608','Wentworth','27320','','NC','8:00 am - 5:00 pm'),(111,'5780 South Main Street','Salisbury','Rowan','https://www.bing.com/maps/default.aspx?where1=5780%20SOUTH%20MAIN%20STREET%20SALISBURY,%20NC%2028144&amp;rtop=0~1~0','(704) 857-9700','(704) 633-5873','Salisbury','28144','','NC','8:00 am - 5:00 pm'),(112,'596 Withrow Road','Forest City','Rutherford','https://www.bing.com/maps/default.aspx?where1=596%20WITHROW%20ROAD%20FOREST%20CITY,%20NC%2028043&amp;rtop=0~1~0','(828) 286-8615','(828) 286-2973','Forest City','28043','','NC','8:00 am - 5:00 pm'),(113,'305 North Blvd','Clinton','Sampson','https://www.bing.com/maps/default.aspx?where1=305%20NORTH%20BOULEVARD%20CLINTON,%20NC%2028328&amp;rtop=0~1~0','(910) 596-0428','(910) 592-5820','Clinton','28328','','NC','8:00 am - 5:00 pm'),(114,'1421 West Blvd','Laurinburg','Scotland','https://www.bing.com/maps/default.aspx?where1=1421%20WEST%20BOULEVARD%20LAURINBURG,%20NC%2028352&amp;rtop=0~1~0','(910) 227-3140','(910) 277-2430','Highway Patrol Building ','28352','','NC','8:00 am - 5:00 pm'),(115,'611 Concord Road','Albemarle','Stanly','https://www.bing.com/maps/default.aspx?where1=611%20CONCORD%20ROAD%20ALBEMARLE,%20NC%2028001&amp;rtop=0~1~0','(704) 982-4014','(704) 982-4077','Albemarle','28001','','NC','8:00 am - 5:00 pm'),(116,'111 West Sixth Street','Walnut Cove','Stokes','https://www.bing.com/maps/default.aspx?where1=111%20W.%20SIXTH%20STREET%20WALNUT%20COVE,%20NC%2027052&amp;rtop=0~1~0','','(336) 591-1136','Walnut Cove','27052','','NC','9:00 am - 4:00 pm'),(117,'1687 North Bridge Street','Elkin','Surry','https://www.bing.com/maps/default.aspx?where1=1687%20NORTH%20BRIDGE%20STREET%20ELKIN,%20NC%2028621&amp;rtop=0~1~0','(336) 526-1979','(336) 835-5247','Elkin','28621','','NC','8:00 am - 5:00 pm'),(118,'155 Patrol Station Road','Mount Airy','Surry','https://www.bing.com/maps/default.aspx?where1=155%20PATROL%20STATION%20ROAD%20MOUNT%20AIRY,%20NC%2027030&amp;rtop=0~1~0','(336) 786-7308','(336) 786-7015','Mount Airy','27030','','NC','8:00 am - 5:00 pm'),(119,'2650 Governors Island Rd','Bryson City','Swain','https://www.bing.com/maps/default.aspx?where1=2650%20GOVERNORS%20ISLAND%20RD%20BRYSON%20CITY,%20NC%2028713-7983&amp;rtop=0~1~0','','(828) 488-3684','Bryson City','28713','','NC','8:00 am - 5:00 pm'),(120,'50 Commerce St, Ste 4','Brevard','Transylvania','https://www.bing.com/maps/default.aspx?where1=50%20COMMERCE%20ST;%20%20%20%20%20%20%20%20STE%204%20BREVARD,%20NC%2028712-4692&amp;rtop=0~1~0','(828) 877-5377','(828) 883-2070','Brevard','28712','','NC','8:00 am - 5:00 pm'),(121,'403 Main Street','Columbia','Tyrrell','https://www.bing.com/maps/default.aspx?where1=403%20MAIN%20STREET%20COLUMBIA,%20NC%2027925&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Medical Center','27925','','NC',''),(122,'3122 US 74 West','Monroe','Union','https://www.bing.com/maps/default.aspx?where1=3122%20US%2074%20WEST%20MONROE,%20NC%2028110&amp;rtop=0~1~0','(704) 291-7353','(704) 283-4264','Monroe','28110','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(123,'414 Hasty Street','Marshville','Union','https://www.bing.com/maps/default.aspx?where1=414%20HASTY%20ST%20MARSHVILLE,%20NC%2028103-1242&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at Edwards Memorial Library','28103','','NC',''),(124,'1080 Eastern Blvd','Henderson','Vance','https://www.bing.com/maps/default.aspx?where1=1080%20EASTERN%20BOULEVARD%20HENDERSON,%20NC%2027536&amp;rtop=0~1~0','(252) 438-8536','(252) 438-8930','Henderson','27536','','NC','8:00 am - 5:00 pm'),(125,'2431 Spring Forest Rd, Unit 101','Raleigh','Wake','https://www.bing.com/maps/default.aspx?where1=2431%20SPRING%20FOREST%20ROAD,%20UNIT%20101%20RALEIGH,%20NC%2027615&amp;rtop=0~1~0','(919) 855-6891','(919) 855-6877','Spring Forest Pavilion','27615','8:00 am - 12:00 pm','NC','7:00 am - 5:00 pm'),(126,'3231 Avent Ferry Road','Raleigh','Wake','https://www.bing.com/maps/default.aspx?where1=3231%20AVENT%20FERRY%20ROAD%20RALEIGH,%20NC%2027606&amp;rtop=0~1~0','(919) 816-9276','(919) 816-9128','Avent Ferry Shopping Center','27606','','NC','7:00 am - 5:00 pm'),(127,'222 Forest Hills Drive','Garner','Wake','https://www.bing.com/maps/default.aspx?where1=222%20FOREST%20HILLS%20DRIVE%20GARNER,%20NC%2027529&amp;rtop=0~1~0','(919) 773-2811','(919) 662-4366','Forest Hills Shopping Center','27529','','NC','8:00 am - 5:00 pm'),(128,'131 South Fuquay Avenue','Fuquay Varina','Wake','https://www.bing.com/maps/default.aspx?where1=131%20SOUTH%20FUQUAY%20AVENUE%20FUQUAY-VARINA,%20NC%2027526&amp;rtop=0~1~0','(919) 552-0660','(919) 552-1895','Fuquay-Varina Municipal Building','27526','','NC','8:00 am - 5:00 pm'),(129,'1387 SE Maynard Road','Cary','Wake','https://www.bing.com/maps/default.aspx?where1=1387%20SE%20MAYNARD%20ROAD%20CARY,%20NC%2027511&amp;rtop=0~1~0','(919) 468-9612','(919) 468-0319','Freeman Center','27511','','NC','7:00 am - 5:00 pm'),(130,'2851 Wendell Blvd','Wendell','Wake','https://www.bing.com/maps/default.aspx?where1=2851%20WENDELL%20BLVD%20WENDELL,%20NC%2027591-6904&amp;rtop=0~1~0','(919) 365-0787','(919) 365-9516','Wendell','27591','','NC','7:00 am - 5:00 pm'),(131,'405 Brooks Street','Wake Forest','Wake','https://www.bing.com/maps/default.aspx?where1=405%20BROOKS%20ST%20WAKE%20FOREST,%20NC%2027587-2930&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Renaissance Center','27587','','NC',''),(132,'501 US 158 Business East','Warrenton','Warren','https://www.bing.com/maps/default.aspx?where1=501%20US%20158%20BUSINESS%20EAST%20WARRENTON,%20NC%2027589&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Old NC National Guard Armory','27589','','NC',''),(133,'201 East Third St','Plymouth','Washington','https://www.bing.com/maps/default.aspx?where1=201%20E.%20THIRD%20ST.%20PLYMOUTH,%20NC%2027962&amp;rtop=0~1~0','','(919) 715-7000','Mobile Unit at the Pettigrew Library','27926','','NC',''),(134,'4469 Bamboo Road, Ste 103','Boone','Watauga','https://www.bing.com/maps/default.aspx?where1=4469%20BAMBOO%20RD;%20%20%20%20%20%20%20%20%20STE%20103%20BOONE,%20NC%2028607-6555&amp;rtop=0~1~0','(828) 265-5425','(828) 265-5384','Boone','28607','','NC','8:00 am - 5:00 pm'),(135,'701 West Grantham St','Goldsboro','Wayne','https://www.bing.com/maps/default.aspx?where1=701%20WEST%20GRANTHAM%20STREET%20GOLDSBORO,%20NC%2027530&amp;rtop=0~1~0','(919) 731-7937','(919) 731-7963','Goldsboro','27530','','NC','7:00 am - 5:00 pm'),(136,'110 North Chestnet Street','Mount Olive','Wayne','https://www.bing.com/maps/default.aspx?where1=110%20NORTH%20CHESTNUT%20STREET%20MOUNT%20OLIVE,%20NC%2028365&amp;rtop=0~1~0','(919) 658-2586','(919) 658-3942','Old Steele Library Building','28365','','NC','8:00 am - 5:00 pm'),(137,'1230 School Safety','Wilkesboro','Wilkes','https://www.bing.com/maps/default.aspx?where1=1230%20SCHOOL%20STREET%20WILKESBORO,%20NC%2028697&amp;rtop=0~1~0','(336) 838-3976','(336) 838-4725','Wilkes','28697','','NC','8:00 am - 5:00 pm'),(138,'1822 Goldsboro Street SW','Wilson','Wilson','https://www.bing.com/maps/default.aspx?where1=1822%20GOLDSBORO%20STREET%20SW%20WILSON,%20NC%2027893&amp;rtop=0~1~0','(252) 234-1127','(252) 243-4072','Wilson','27893','','NC','8:00 am - 5:00 pm'),(139,'225 Ash Street','Yadkinville','Yadkin','https://www.bing.com/maps/default.aspx?where1=225%20ASH%20STREET%20YADKINVILLE,%20NC%2027055&amp;rtop=0~1~0','(336) 679-7228','(336) 679-3232','Yadkinville','27055','','NC','8:00 am - 5:00 pm'),(140,'116 North Main Street','Burnsville','Yancey','https://www.bing.com/maps/default.aspx?where1=116%20NORTH%20MAIN%20STREET%20BURNSVILLE,%20NC%2028714&amp;rtop=0~1~0','(828) 682-7372','(828) 682-9619','Burnsville','28714','','NC','8:00 am - 5:00 pm');

/********************* Create DMV Loaction master end  *************************/

/********************* Create LPA Loaction master  *************************/

CREATE TABLE `lpa_location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `directions` varchar(255) DEFAULT NULL,
  `fax_number` varchar(255) DEFAULT NULL,
  `main_phone` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `saturday_hrs` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `weekdays_hrs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;

INSERT INTO `lpa_location` VALUES (1,'1313 N. Sandhills Blvd','Aberdeen 75','Aberdeen','Moore','https://www.bing.com/maps/default.aspx?where1=1313%20NORTH%20SANDHILLS%20BOULEVARD%20ABERDEEN,%20NC%2028315&amp;rtop=0~1~0','(910) 944-3184','(910) 944-1303','28315','','NC','9:00 am - 5:00 pm'),(2,'907 Memorial Drive East','Ahoskie 5','Ahoskie','Hertford','https://www.bing.com/maps/default.aspx?where1=907%20EAST%20MEMORIAL%20DRIVE%20AHOSKIE,%20NC%2027910&amp;rtop=0~1~0','(252) 332-2801','(252) 332-2801','27910','','NC','9:00 am - 5:00 pm'),(3,'1542 Hey. 24 27 Byp. W.','Albemarle 1','Albemarle','Stanly','https://www.bing.com/maps/default.aspx?where1=1542%20NC%2024%2027%20BYP%20W%20ALBEMARLE,%20NC%2028001&amp;rtop=0~1~0','(704) 982-4700','(704) 982-3854','28001','','NC','9:00 am - 5:00 pm'),(4,'85 East Main Avenue','Alexander Cty 176','Taylorsville','Alexander','https://www.bing.com/maps/default.aspx?where1=85%20EAST%20MAIN%20AVENUE%20TAYLORSVILLE,%20NC%2028681&amp;rtop=0~1~0','(828) 632-4097','(828) 632-4463','28681','','NC','9:00 am - 5:00 pm'),(5,'13580 Hwy 55 E','Alliance 51','Alliance','Pamlico','https://www.bing.com/maps/default.aspx?where1=13580%20N.C.%20HIGHWAY%2055%20EAST%20ALLIANCE,%20NC%2028509&amp;rtop=0~1~0','(252) 745-0696','(252) 745-0695','28509','','NC','9:00 am - 5:00 pm'),(6,'18 E. Depot St, Hwy 210','Angier (Chamber) 155','Angier','Harnett','https://www.bing.com/maps/default.aspx?where1=18%20E%20DEPOT%20ST%20ANGIER,%20NC%2027501-6017&amp;rtop=0~1~0','(919) 639-9919','(919) 639-9900','27501','','NC','8:30 am - 5:00 pm'),(7,'150 Government Cr, Ste 2200','Ashe Cty 151','Jefferson','Ashe','https://www.bing.com/maps/default.aspx?where1=150%20GOVERNMENT%20CIR;%20%20%20%20STE%202200%20JEFFERSON,%20NC%2028640-8960&amp;rtop=0~1~0','(336) 846-5564','(336) 846-5530','28640','','NC','8:00 am - 5:00 pm'),(8,'338 S. Fayetteville St','Asheboro 2','Asheboro','Randolph','https://www.bing.com/maps/default.aspx?where1=338%20SOUTH%20FAYETTEVILLE%20STREET%20ASHEBORO,%20NC%2027203&amp;rtop=0~1~0','(336) 629-0520','(336) 629-9623','27203','','NC','9:00 am - 5:00 pm'),(9,'780 Hendersonville Rd/US 25 Ste. 8','Asheville 42','Asheville','Buncombe','https://www.bing.com/maps/default.aspx?where1=780%20HENDERSONVILLE%20RD;%20%20%20%20%20%20STE%208%20ASHEVILLE,%20NC%2028803-2926&amp;rtop=0~1~0','(828) 277-7760','(828) 277-7767','28803','','NC','9:00 am - 5:00 pm'),(10,'153 Smokey Park Hwy, Ste 160A','Asheville 56','Asheville','Buncombe','https://www.bing.com/maps/default.aspx?where1=153%20SMOKEY%20PARK%20HIGHWAY,%20SUITE%20160-A%20ASHEVILLE,%20NC%2028806-1166&amp;rtop=0~1~0','(828) 252-7438','(828) 252-8526','28806','','NC','9:00 am - 5:00 pm'),(11,'30 Government Dr NE','Brunswick Cty 58','Bolivia','Brunswick','https://www.bing.com/maps/default.aspx?where1=30%20GOVERNMENT%20CENTER%20DR%20BOLIVIA,%20NC%2028422-7987&amp;rtop=0~1~0','(910) 253-2432','(910) 253-2971','28422','','NC','8:00 am - 5:00 pm'),(12,'2668 Ramada Rd','Burlington 8','Burlington','Alamance','https://www.bing.com/maps/default.aspx?where1=2668%20RAMADA%20ROAD%20BURLINGTON,%20NC%2027215&amp;rtop=0~1~0','(336) 228-0061','(336) 228-7152','27215','','NC','9:00 am - 5:00 pm'),(13,'11 Peach Bloom Dr','Canton 45','Canton','Haywood','https://www.bing.com/maps/default.aspx?where1=11%20PEACH%20BLOOM%20DR%20CANTON,%20NC%2028716-3211&amp;rtop=0~1~0','(828) 646-0094','(828) 646-0091','28716','','NC','9:00 am - 1:00 pm\n2:00 pm - 5:00 pm'),(14,'1251 Buck Jones Rd','Cary 107','Cary','Wake','https://www.bing.com/maps/default.aspx?where1=1251%20BUCK%20JONES%20ROAD%20RALEIGH,%20NC%2027606&amp;rtop=0~1~0','(919) 388-0919','(919) 469-1444','27606','','NC','8:30 am - 5:00 pm'),(15,'104 W. Hwy 54, Ste D','Chapel Hill 87','Carrboro','Orange','https://www.bing.com/maps/default.aspx?where1=104%20W.%20N.C.%20HIGHWAY%2054,%20SUITE%20D%20CARRBORO,%20NC%2027510&amp;rtop=0~1~0','(866) 217-4704','(919) 521-8600','27510','','NC','9:00 am - 5:00 pm'),(16,'3250 G Wilkinson Blvd','Charlotte 78','Charlotte','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=3250-G%20WILKINSON%20BLVD.%20CHARLOTTE,%20NC%2028208&amp;rtop=0~1~0','(980) 237-9810','(980) 237-9598','28208','','NC','9:00 am - 5:00 pm'),(17,'6016 Brookshire Blvd','Charlotte 9','Charlotte','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=6016%20BROOKSHIRE%20BOULEVARD%20CHARLOTTE,%20NC%2028216&amp;rtop=0~1~0','(704) 697-0063','(980) 260-2651','28216','','NC','8:00 am - 5:00 pm'),(18,'5622 E. Independence Blvd. Ste #124','Charlotte 158','Charlotte','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=5622%20EAST%20INDEPENDENCE%20BOULEVARD%20SUITE%20124%20CHARLOTTE,%20NC%2028212&amp;rtop=0~1~0','(704) 535-2559','(704) 535-2525','28212','','NC','9:00 am - 5:00 pm'),(19,'809 E. Arrowwood Rd. Ste #800','Charlotte 122','Charlotte','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=809%20EAST%20ARROWOOD%20RD.%20SUITE%20800%20CHARLOTTE,%20NC%2028217&amp;rtop=0~1~0','(704) 525-1647','(704) 525-3832','28217','','NC','9:00 am - 5:00 pm'),(20,'1121 Sunset Ave','Clinton 11','Clinton','Sampson','https://www.bing.com/maps/default.aspx?where1=1121%20SUNSET%20AVENUE%20CLINTON,%20NC%2028328&amp;rtop=0~1~0','(910) 592-5915','(910) 592-5265','28328','','NC','9:00 am - 5:00 pm'),(21,'929-D Concord Parkway South','Concord 163','Concord','Cabarrus','https://www.bing.com/maps/default.aspx?where1=929-D%20CONCORD%20PARKWAY%20SOUTH%20CONCORD,%20NC%2028027&amp;rtop=0~1~0','(704) 721-2638','(704) 723-4991','28027','','NC','9:00 am - 5:00 pm'),(22,'128 N Clinton Ave','Dunn 6','Dunn','Harnett','https://www.bing.com/maps/default.aspx?where1=128%20N%20CLINTON%20AVE%20DUNN,%20NC%2028334-4235&amp;rtop=0~1~0','(910) 892-1421','(910) 892-6324','28334','','NC','9:00 am - 5:00 pm'),(23,'1058 West Club Blvd., Ste 626','Durham 12','Durham','Durham','https://www.bing.com/maps/default.aspx?where1=1058%20W%20CLUB%20BLVD,%20%20STE%20626%20DURHAM,%20NC%2027701-1100&amp;rtop=0~1~0','(919) 416-3383','(919) 286-4908','27701','','NC','9:00 am - 5:00 pm'),(24,'5410 Hwy 55 Ste AB','Durham 179','Durham','Durham','https://www.bing.com/maps/default.aspx?where1=5410%20NC%20HIGHWAY%2055%20SUITE%20AB%20DURHAM,%20NC%2027713&amp;rtop=0~1~0','(919) 544-3993','(919) 544-3662','27713','','NC','9:00 am - 5:00 pm'),(25,'712 Washington St','Eden 184','Eden','Rockingham','https://www.bing.com/maps/default.aspx?where1=712%20WASHINGTON%20STREET%20EDEN,%20NC%2027288&amp;rtop=0~1~0','(336) 627-1369','(336) 627-1368','27288','','NC','9:00 am - 5:00 pm'),(26,'810 N. Broad Street','Edenton 13','Edenton','Chowan','https://www.bing.com/maps/default.aspx?where1=810%20NORTH%20BROAD%20STREET%20EDENTON,%20NC%2027932&amp;rtop=0~1~0','(252) 482-2922','(252) 482-2424','27932','','NC','CLOSED'),(27,'201 Saint Andrew Street','Edgecombe Cty 166','Tarboro','Edgecombe','https://www.bing.com/maps/default.aspx?where1=201%20SAINT%20ANDREW%20STREET%20TARBORO,%20NC%2027886&amp;rtop=0~1~0','(252) 641-6686','(252) 641-6684','27886','','NC','8:00 am - 5:00 pm'),(28,'1545 N. Road Street, Ste E','Elizabeth City 14','Elizabeth City','Pasquotank','https://www.bing.com/maps/default.aspx?where1=1545%20N%20ROAD%20STREET%20SUITE%20E%20ELIZABETH%20CITY,%20NC%2027909&amp;rtop=0~1~0','(252) 331-7037','(252) 338-6965','27909','','NC','9:00 am - 5:00 pm'),(29,'307 E. Broad St','Elizabethtown 62','Elizabethtown','Bladen','https://www.bing.com/maps/default.aspx?where1=307%20EAST%20BROAD%20STREET%20ELIZABETHTOWN,%20NC%2028337&amp;rtop=0~1~0','(910) 862-2574','(910) 862-3527','28337','','NC','9:00 am - 12:00 pm\n1:00 pm - 5:00 pm'),(30,'220 W Main St','Elkin 90','Elkin','Surry','https://www.bing.com/maps/default.aspx?where1=220%20WEST%20MAIN%20STREET%20ELKIN,%20NC%2028621&amp;rtop=0~1~0','(336) 835-8616','(336) 835-2757','28621','','NC','CLOSED'),(31,'34910 US Hwy 264','Engelhard 149','Engelhard','Hyde','https://www.bing.com/maps/default.aspx?where1=34910%20U.S.%20HIGHWAY%20264%20ENGELHARD,%20NC%2027824&amp;rtop=0~1~0','(252) 925-1092','(252) 925-3752','27824','','NC','9:00 am - 12:00 pm\n1:00 pm - 5:00 pm'),(32,'3672 North Main Street','Farmville (Town) 157','Farmville','Pitt','https://www.bing.com/maps/default.aspx?where1=3672%20NORTH%20MAIN%20STREET%20FARMVILLE,%20NC%2027828&amp;rtop=0~1~0','(252) 753-6061','(252) 753-6726','27828','','NC','Mon-Thurs\n8:00 am - 1:00 pm\n2:00 pm - 5:00 pm\nFriday\n8:00 am - 12:00 pm'),(33,'815 Elm Street','Fayetteville 15','Fayetteville','Cumberland','https://www.bing.com/maps/default.aspx?where1=815%20ELM%20STREET%20FAYETTEVILLE,%20NC%2028303&amp;rtop=0~1~0','(910) 485-6102','(910) 485-1590','28303','','NC','9:00 am - 5:00 pm'),(34,'353 Westgate Plz','Franklin 71','Franklin','Macon','https://www.bing.com/maps/default.aspx?where1=353%20WESTGATE%20PLZ%20FRANKLIN,%20NC%2028734-9012&amp;rtop=0~1~0','(828) 369-8166','(828) 369-8165','28734','','NC','9:00 am - 5:00 pm'),(35,'246 N New Hope Rd, Ste #239','Gastonia 131','Gastonia','Gaston','https://www.bing.com/maps/default.aspx?where1=246%20NORTH%20NEW%20HOPE%20ROAD,%20SUITE%20#239GASTONIA,NC28054&amp;rtop=0~1~0','(704) 864-0912','(704) 864-4856','28054','','NC','9:00 am - 5:00 pm'),(36,'1801 E Ash St','Goldsboro 18','Goldsboro','Wayne','https://www.bing.com/maps/default.aspx?where1=1801%20EAST%20ASH%20STREET%20GOLDSBORO,%20NC%2027530&amp;rtop=0~1~0','(919) 734-0838','(919) 734-0881','27530','','NC','9:00 am - 5:00 pm'),(37,'5533 West Market St','Greensboro 134','Greensboro ','Guilford','https://www.bing.com/maps/default.aspx?where1=5533%20W%20MARKET%20ST%20GREENSBORO,%20NC%2027409-2525&amp;rtop=0~1~0','(336) 856-0909','(336) 856-1510','27409','','NC','9:00 am - 5:00 pm'),(38,'2218 Golden Gate Dr','Greensboro 185','Greensboro ','Guilford','https://www.bing.com/maps/default.aspx?where1=2218%20GOLDEN%20GATE%20DR%20GREENSBORO,%20NC%2027405-4302&amp;rtop=0~1~0','(336) 275-7716','(336) 275-7715','27405','','NC','9:00 am - 5:00 pm'),(39,'2462 Stantonsburg Rd.','Greenville 147','Greenville','Pitt','https://www.bing.com/maps/default.aspx?where1=2462%20STANTONSBURG%20RD%20GREENVILLE,%20NC%2027834-7210&amp;rtop=0~1~0','(252) 756-5460','(252) 756-5099','27858','','NC','8:30 am - 5:00 pm'),(40,'14687 US Highway 17','Hampstead 145','Hampstead','Pender','https://www.bing.com/maps/default.aspx?where1=14687%20US%20HIGHWAY%2017%20N%20HAMPSTEAD,%20NC%2028443-3505&amp;rtop=0~1~0','(910) 270-2127','(910) 270-9010','28443','','NC','9:00 am - 5:00 pm'),(41,'4612 Highway 49 S.','Harrisburg 182','Harrisburg','Cabarrus','https://www.bing.com/maps/default.aspx?where1=4612%20HIGHWAY%2049%20S%20HARRISBURG,%20NC%2028075-8461&amp;rtop=0~1~0','(980) 258-8079','(980) 258-8078','28075','','NC','9:00 am - 5:00 pm'),(42,'319 W. Main Street','Havelock 104','Havelock','Craven','https://www.bing.com/maps/default.aspx?where1=319%20WEST%20MAIN%20STREET%20HAVELOCK,%20NC%2028532&amp;rtop=0~1~0','(252) 447-5178','(252) 447-3097','28532','','NC','9:00 am - 5:00 pm'),(43,'1440 Hwy 64W','Hayesville 117','Hayesville','Clay','https://www.bing.com/maps/default.aspx?where1=1440%20HIGHWAY%2064%20W%20HAYESVILLE,%20NC%2028904-4528&amp;rtop=0~1~0','(828) 389-2577','(828) 389-8133','28904','','NC','9:00 am - 12:30 pm\n1:30 pm - 5:00 pm'),(44,'946-D West Andrews Avenue','Henderson 21','Henderson','Vance','https://www.bing.com/maps/default.aspx?where1=946-D%20WEST%20ANDREWS%20AVENUE%20HENDERSON,%20NC%2027536&amp;rtop=0~1~0','(252) 436-2489','(252) 438-3528','27536','','NC','9:00 am - 5:00 pm'),(45,'145 Four Seasons Mall','Hendersonville 22','Hendersonville','Henderson','https://www.bing.com/maps/default.aspx?where1=145%20FOUR%20SEASONS%20MALL%20HENDERSONVILLE,%20NC%2028792-2878&amp;rtop=0~1~0','(828) 692-2979','(828) 692-0648','28792','','NC','9:00 am - 5:00 pm'),(46,'114 West Grubb St','Hertford (Town) 93','Hertford','Perquimans','https://www.bing.com/maps/default.aspx?where1=114%20WEST%20GRUBB%20STREET%20HERTFORD,%20NC%2027944&amp;rtop=0~1~0','(252) 426-7060','(252) 426-1087','27944','','NC','8:00 am - 5:00 pm'),(47,'901 Hwy. 321 North West, Ste. 100','Hickory 23','Hickory','Catawba','https://www.bing.com/maps/default.aspx?where1=901%20US%20HIGHWAY%20321%20NW;%20%20STE%20100%20HICKORY,%20NC%2028601-4770&amp;rtop=0~1~0','(828) 328-6311','(828) 328-3783','28601','','NC','9:00 am - 5:00 pm'),(48,'1701 Westchester Dr. Ste. 220','High Point 165','High Point','Guilford','https://www.bing.com/maps/default.aspx?where1=1701-220B%20WESTCHESTER%20DR%20HIGH%20POINT,%20NC%2027262-7248&amp;rtop=0~1~0','(336) 889-8288','(336) 889-8247','27262','','NC','9:00 am - 5:00 pm'),(49,'520 West Donaldson Ave','Hoke Cty 152','Raeford','Hoke','https://www.bing.com/maps/default.aspx?where1=520%20W%20DONALDSON%20AVE%20RAEFORD,%20NC%2028376-2620&amp;rtop=0~1~0','(910) 875-3466','(910) 875-2179','28376','','NC','8:00 am - 5:00 pm'),(50,'408 Village Walk Dr','Holly Springs 53','Holly Springs','Wake','https://www.bing.com/maps/default.aspx?where1=408%20VILLAGE%20WALK%20DR%20HOLLY%20SPRINGS,%20NC%2027540-7699&amp;rtop=0~1~0','(919) 552-6284','(919) 552-5449','27540','','NC','8:00 am - 5:00 pm'),(51,'3333 N. Main St. Ste 140','Hope Mills 106','Hope Mills','Wake','https://www.bing.com/maps/default.aspx?where1=3333%20NORTH%20MAIN%20STREET,%20SUITE%20140%20HOPE%20MILLS,%20NC%2028348&amp;rtop=0~1~0','(910) 424-2104','(910) 424-2500-6','28348','','NC','9:00 am - 5:00 pm'),(52,'12101 Mount Holly-Huntersville Rd','Huntersville 52','Huntersville','Mecklenburg','https://www.bing.com/maps/default.aspx?where1=12101%20MT.%20HOLLY-HUNTERSVILLE%20ROAD%20HUNTERSVILLE,%20NC%2028078&amp;rtop=0~1~0','(704) 875-0886','(704) 875-2991','28078','','NC','9:00 am - 5:00 pm'),(53,'14015-J E Independence Blvd','Indian Trail 65','Indian Trail','Union','https://www.bing.com/maps/default.aspx?where1=14015-J%20EAST%20INDEPENDENCE%20BOULEVARD%20INDIAN%20TRAIL,%20NC%2028079&amp;rtop=0~1~0','(704) 684-5109','(704) 234-8704','28079','','NC','9:00 am - 5:00 pm'),(54,'201 Wilmington Hwy.','Jacksonville 16','Jacksonville','Onslow','https://www.bing.com/maps/default.aspx?where1=201%20WILMINGTON%20HWY%20JACKSONVILLE,%20NC%2028540-3505&amp;rtop=0~1~0','(910) 347-1134','(910) 347-1000','28540','','NC','9:00 am - 5:00 pm'),(55,'1509 Dale Earnhardt Blvd','Kannapolis 25','Kannapolis','Cabarrus','https://www.bing.com/maps/default.aspx?where1=1509%20DALE%20EARNHARDT%20BOULEVARD%20KANNAPOLIS,%20NC%2028083&amp;rtop=0~1~0','(704) 932-0383','(704) 932-3146','28083','','NC','9:00 am - 5:00 pm'),(56,'810-J N. Main St','Kernersville 10','Kernersville','Forsyth','https://www.bing.com/maps/default.aspx?where1=810%20J%20NORTH%20MAIN%20ST%20KERNERSVILLE,%20NC%2027284&amp;rtop=0~1~0','(336) 904-0745','(336) 904-0743','27284','','NC','9:00 am - 5:00 pm'),(57,'2431-B  N. Heritage Rd','Kinston 26','Kinston','Lenoir','https://www.bing.com/maps/default.aspx?where1=2431%20N%20HERRITAGE%20ST%20KINSTON,%20NC%2028501-1613&amp;rtop=0~1~0','(252) 523-4252','(252) 523-5292','28504','','NC','9:00 am - 5:00 pm'),(58,'508 E. Church St','Laurinburg 17','Laurinburg','Scotland','https://www.bing.com/maps/default.aspx?where1=508%20E%20CHURCH%20ST%20LAURINBURG,%20NC%2028352-4027&amp;rtop=0~1~0','(910) 361-4260','(910) 361-4177','28532','','NC','9:00 am - 5:00 pm'),(59,'1408 Blowing Rock Blvd. ','Lenoir 28','Lenoir','Caldwell','https://www.bing.com/maps/default.aspx?where1=1402%20BLOWING%20ROCK%20BOULEVARD%20LENOIR,%20NC%2028645&amp;rtop=0~1~0','(828) 757-2828','828-758-1528','28645','','NC','9:00 am - 5:00 pm'),(60,'27 Plaza Parkway','Lexington 177','Lexington','Davidson','https://www.bing.com/maps/default.aspx?where1=27%20PLAZA%20PARKWAY%20LEXINGTON,%20NC%2027292&amp;rtop=0~1~0','(336) 248-5822','(336) 248-2720','27292','','NC','9:00 am - 5:00 pm'),(61,'646 Center Drive','Lincolnton 30','Lincolnton','Lincoln','https://www.bing.com/maps/default.aspx?where1=646%20CENTER%20DRIVE%20LINCOLNTON,%20NC%2028092&amp;rtop=0~1~0','(704) 736-0706','(704) 735-0018','28092','','NC','9:00 am - 5:00 pm'),(62,'808 S. Bickett Blvd.','Louisburg 81','Louisburg','Franklin','https://www.bing.com/maps/default.aspx?where1=808%20SOUTH%20BICKETT%20BOULEVARD%20LOUISBURG,%20NC%2027549&amp;rtop=0~1~0','(919) 496-7805','(919) 496-4655','27549','','NC','8:00 am - 5:00 pm'),(63,'3467 Lackey St','Lumberton 187','Lumberton','Sampson','https://www.bing.com/maps/default.aspx?where1=3467%20LACKEY%20STREET%20LUMBERTON,%20NC%2028360&amp;rtop=0~1~0','(910) 737-6549','(910) 737-6550','28360','','NC','9:00 am - 5:00 pm'),(64,'101 E. Murphy St','Madison 183','Madison','Rockingham','https://www.bing.com/maps/default.aspx?where1=101%20EAST%20MURPHY%20STREET%20MADISON,%20NC%2027025&amp;rtop=0~1~0','(336) 949-4771','(336) 949-4770','27025','','NC','9:00 am - 12:30 pm\n1:30 pm - 5:00 pm'),(65,'Island Pharmacy, 210 Hwy 64 S.','Manteo 31','Manteo','Dare','https://www.bing.com/maps/default.aspx?where1=210%20US%20HIGHWAY%2064%20AND%20264%20MANTEO,%20NC%2027954-9718&amp;rtop=0~1~0','(252) 473-9370','(252) 473-3926','27954','','NC','8:30 am - 12:30 pm\n1:30 pm - 5:00 pm'),(66,'133 S. Main Street, Suite 102','Marshall 167','Marshall','Madison','https://www.bing.com/maps/default.aspx?where1=133%20SOUTH%20MAIN%20ST.,%20SUITE%20102%20MARSHALL,%20NC%2028753&amp;rtop=0~1~0','(828) 649-3583','(828) 649-3528','28753','','NC','8:30 am - 5:00 pm'),(67,'305 E. Main Street','Martin Cty 171','Williamston','Martin','https://www.bing.com/maps/default.aspx?where1=305%20EAST%20MAIN%20STREET%20WILLIAMSTON,%20NC%2027892&amp;rtop=0~1~0','(252) 789-4569','(252) 789-4560','27892','','NC','8:00 am - 5:00 pm'),(68,'404 Main St','Maysville (Town) 82','Maysville','Jones','https://www.bing.com/maps/default.aspx?where1=404%20MAIN%20ST%20MAYSVILLE,%20NC%2028555-8046&amp;rtop=0~1~0','(910) 743-0895','(910) 400-4077','28555','','NC','8:30 am - 5:00 pm'),(69,'60 East Court Street','McDowell Cty 33','Marion','McDowell','https://www.bing.com/maps/default.aspx?where1=60%20EAST%20COURT%20STREET%20MARION,%20NC%2028752&amp;rtop=0~1~0','(828) 659-2864','(828) 652-0179','28752','','NC','8:30 am - 5:00 pm'),(70,'1047 Yadkinville Rd','Mocksville 69','Mocksville','Davie','https://www.bing.com/maps/default.aspx?where1=1047%20YADKINVILLE%20RD%20MOCKSVILLE,%20NC%2027028-2075&amp;rtop=0~1~0','(336) 753-6678','(336) 753-6677','27028','','NC','9:00 am - 1:00 pm\n2:00 pm - 5:00 pm'),(71,'622 West Roosevelt Blvd., Ste. E','Monroe 34','Monroe','Union','https://www.bing.com/maps/default.aspx?where1=622%20WEST%20ROOSEVELT%20BOULEVARD%20SUITE%20E%20MONROE,%20NC%2028110&amp;rtop=0~1~0','(704) 291-2396','(704) 283-4113','28110','','NC','9:00 am - 5:00 pm'),(72,'125-3 North Main St','Mooresville 77','Mooresville','Iredell','https://www.bing.com/maps/default.aspx?where1=125-3%20NORTH%20MAIN%20STREET%20MOORESVILLE,%20NC%2028115&amp;rtop=0~1~0','(704) 663-7428','(704) 633-5472','28115','','NC','9:00 am - 5:00 pm'),(73,'3025 H Bridges St.','Morehead City 40','Morehead City','Carteret','https://www.bing.com/maps/default.aspx?where1=3025H%20BRIDGES%20ST%20MOREHEAD%20CITY,%20NC%2028557-3329&amp;rtop=0~1~0','(252) 726-0352','(252) 726-7695','28557','','NC','9:00 am - 5:00 pm'),(74,'603 S. College St.','Morganton 35','Morganton','Burke','https://www.bing.com/maps/default.aspx?where1=603%20SOUTH%20COLLEGE%20STREET%20MORGANTON,%20NC%2028655&amp;rtop=0~1~0','(828) 433-1032','(828) 437-4505','28655','','NC','9:00 am - 5:00 pm'),(75,'137 Riverside Drive','Mount Airy 168','Mount Airy','Surry','https://www.bing.com/maps/default.aspx?where1=137%20RIVERSIDE%20DRIVE%20MOUNT%20AIRY,%20NC%2027030&amp;rtop=0~1~0','(336) 786-6912','(336) 786-5201','27030','','NC','9:00 am - 5:00 pm'),(76,'225 N Center St','Mount Olive 130','Mount Olive','Wayne','https://www.bing.com/maps/default.aspx?where1=225%20N%20CENTER%20ST%20MOUNT%20OLIVE,%20NC%2028365-1719&amp;rtop=0~1~0','(919) 658-9921','(919) 658-9921','28365','','NC','9:00 am - 12:30 pm\n1:30 pm - 5:00 pm'),(77,'1176 Andrews Road','Murphy 39','Murphy','Cherokee','https://www.bing.com/maps/default.aspx?where1=1176%20ANDREWS%20ROAD%20MURPHY,%20NC%2028906&amp;rtop=0~1~0','(828) 837-2023','(828) 837-2023','28906','','NC','9:00 am - 12:00 pm\n1:00 pm - 5:00 pm'),(78,'2505 Neuse Blvd','New Bern 37','New Bern','Craven','https://www.bing.com/maps/default.aspx?where1=2505%20NEUSE%20BOULEVARD%20NEW%20BERN,%20NC%2028562&amp;rtop=0~1~0','(252) 514-6562','(252) 637-4524','28562','','NC','9:00 am - 5:00 pm'),(79,'301 Cranberry St','Newland (Town) 114','Newland','Avery','https://www.bing.com/maps/default.aspx?where1=301%20CRANBERRY%20STREET%20NEWLAND,%20NC%2028657&amp;rtop=0~1~0','(828) 733-2069','(828) 733-6803','28657','','NC','8:00 am - 12:00 pm\n12:30 pm - 4:30 pm'),(80,'803 Conover Blvd W.','Newton 101','Conover','Catawba','https://www.bing.com/maps/default.aspx?where1=803%20WEST%20CONOVER%20BOULEVARD%20NEWTON,%20NC%2028613&amp;rtop=0~1~0','(828) 464-4388','(828) 464-6878','28613','','NC','9:00 am - 5:00 pm'),(81,'2533 Atlantic Ave','North Raleigh 115','Raleigh','Wake','https://www.bing.com/maps/default.aspx?where1=2533%20ATLANTIC%20AVE,%20SUITE%20102%20RALEIGH,%20NC%2027604&amp;rtop=0~1~0','(919) 831-9997','(919) 831-9996','27609','','NC','9:00 am - 5:00 pm'),(82,'85 B Boone Trail','North Wilkesboro 38','North Wilkesboro','Wilkes','https://www.bing.com/maps/default.aspx?where1=85%20BOONE%20TRL;%20%20%20%20%20%20%20%20%20STE%20B%20NORTH%20WILKESBORO,%20NC%2028659-3576&amp;rtop=0~1~0','(336) 903-1285','(336) 903-1283','28569','','NC','9:00 am - 5:00 pm'),(83,'120 Roxboro Road','Oxford 170','Oxford','Granville','https://www.bing.com/maps/default.aspx?where1=120%20ROXBORO%20ROAD%20OXFORD,%20NC%2027565&amp;rtop=0~1~0','(919) 693-9818','(919) 693-7821','27565','','NC','9:00 am - 5:00 pm'),(84,'69 New Hendersonville Hwy, Suite 8','Pisgah Forest 143','Pisgah Forest','Transylvania','https://www.bing.com/maps/default.aspx?where1=69%20NEW%20HENDERSONVILLE%20HIGHWAY%20SUITE%208%20PISGAH%20FOREST,%20NC%2028768&amp;rtop=0~1~0','(828) 883-3251','(828) 883-3251','28768','','NC','9:00 am - 1:00 pm\n2:00 pm - 5:00 pm'),(85,'383 Hwy 64 West, Suite 2','Plymouth 72','Plymouth','Washington','https://www.bing.com/maps/default.aspx?where1=383%20US%20HIGHWAY%2064%20WEST%20SUITE%202%20PLYMOUTH,%20NC%2027962&amp;rtop=0~1~0','(252) 793-5183','(252) 793-2063','27962','','NC','9:00 am - 12:30 pm\n1:30 pm - 5:00 pm'),(86,'51 Walker St','Polk Cty 29','Columbus','Polk','https://www.bing.com/maps/default.aspx?where1=51%20WALKER%20ST%20COLUMBUS,%20NC%2028722-7497&amp;rtop=0~1~0','(828) 894-6430','(828)894-8500-4','28722','','NC','8:00 am - 5:00 pm'),(87,'1100 New Bern Ave','Raleigh Window 907','Raleigh','Wake','https://www.bing.com/maps/default.aspx?where1=1100%20NEW%20BERN%20AVENUE%20RALEIGH,%20NC%2027697&amp;rtop=0~1~0','(919) 715-6757','(919) 715-7000','27697','','NC','8:00 am - 5:00 pm'),(88,'1720 Julian R. Allsbrook Hwy','Roanoke Rapids 70','Roanoke Rapids','Halifax','https://www.bing.com/maps/default.aspx?where1=1720%20JULIAN%20R.%20ALLSBROOK%20HIGHWAY%20ROANOKE%20RAPIDS,%20NC%2027870&amp;rtop=0~1~0','(252) 537-2051','(252) 537-6357','27870','','NC','9:00 am - 5:00 pm'),(89,'101 North Middleton Street','Robbins (Town) 154','Robbins','Moore','https://www.bing.com/maps/default.aspx?where1=101%20NORTH%20MIDDLETON%20STREET%20ROBBINS,%20NC%2027325&amp;rtop=0~1~0','(910) 948-3981','(910) 948-2431','27325','','NC','CLOSED'),(90,'74 S. Main St','Robbinsville 84','Robbinsville','Graham','https://www.bing.com/maps/default.aspx?where1=74%20SOUTH%20MAIN%20STREET%20ROBBINSVILLE,%20NC%2028771&amp;rtop=0~1~0','(828) 479-4896','(828) 479-4896','28771','','NC','9:00 am - 1:00 pm\n2:00 pm - 5:00 pm'),(91,'601 S. Long Dr.','Rockingham 64','Rockingham','Richmond','https://www.bing.com/maps/default.aspx?where1=601%20S%20LONG%20DR%20ROCKINGHAM,%20NC%2028379-4386&amp;rtop=0~1~0','(910) 997-6681','(910) 997-4014','28379','','NC','9:00 am - 5:00 pm'),(92,'1850 Stone Rose Drive','Rocky Mount 44','Rocky Mount','Nash','https://www.bing.com/maps/default.aspx?where1=1850%20STONE%20ROSE%20DR%20ROCKY%20MOUNT,%20NC%2027804-2512&amp;rtop=0~1~0','(252) 443-6113','(252) 443-7900','27803','','NC','9:00 am - 5:00 pm'),(93,'811 N. Madison Blvd.','Roxboro 178','Roxboro','Person','https://www.bing.com/maps/default.aspx?where1=811%20NORTH%20MADISON%20BOULEVARD%20ROXBORO,%20NC%2027573&amp;rtop=0~1~0','(336) 597-9369','(336) 597-4809','27573','','NC','9:00 am - 5:00 pm'),(94,'1014 Bethania Rural Hall Rd.','Rural Hall 123','Rural Hall','Forsyth','https://www.bing.com/maps/default.aspx?where1=1014%20BETHANIA%20RURAL%20HALL%20ROAD%20RURAL%20HALL,%20NC%2027045&amp;rtop=0~1~0','(336) 969-2337','(336) 969-2814','27045','','NC','9:00 am - 5:00 pm'),(95,'130 E. Kerr St.','Salisbury 46','Salisbury','Rowan','https://www.bing.com/maps/default.aspx?where1=130%20E%20KERR%20ST%20SALISBURY,%20NC%2028144-4454&amp;rtop=0~1~0','(704) 636-3262','(704) 633-5312','28144','','NC','9:00 am - 5:00 pm'),(96,'331 Wilson Rd.','Sanford 47','Sanford','Lee','https://www.bing.com/maps/default.aspx?where1=331%20WILSON%20ROAD%20SANFORD,%20NC%2027332&amp;rtop=0~1~0','(919) 775-5488','(919) 774-6027','27332','','NC','9:00 am - 1:00 pm\n2:00 pm - 5:00 pm'),(97,'5300 South Main St','Shallotte 97','Shallotte','Brunswick','https://www.bing.com/maps/default.aspx?where1=5300%20SOUTH%20MAIN%20STREET,%20SUITE%20A%20SHALLOTTE,%20NC%2028470&amp;rtop=0~1~0','(910) 754-4674','(910) 754-4591','28470','','NC','9:00 am - 5:00 pm'),(98,'118 N. Morgan St. Ste. A','Shelby 48','Shelby','Cleveland','https://www.bing.com/maps/default.aspx?where1=118%20N%20MORGAN%20ST%20SHELBY,%20NC%2028150-4457&amp;rtop=0~1~0','(704) 487-5275','(704) 487-4551','28150','','NC','9:00 am - 5:00 pm'),(99,'311 N. Second Ave','Siler City (Town) 181','Siler City','Chatham','https://www.bing.com/maps/default.aspx?where1=311%20NORTH%20SECOND%20AVENUE%20SILER%20CITY,%20NC%2027344&amp;rtop=0~1~0','(919) 742-1306','(919) 742-1307','27344','','NC','9:00 am - 1:00 pm\n2:00 pm - 5:00 pm'),(100,'3175A Highway, 301 South','Smithfield 83','Smithfield','Johnston','https://www.bing.com/maps/default.aspx?where1=3175A%20U.S.%20HIGHWAY%20301%20SOUTH%20SMITHFIELD,%20NC%2027577&amp;rtop=0~1~0','(919) 989-3478','(919) 934-8707','27577','','NC','9:00 am - 5:00 pm'),(101,'4831 Port Loop Rd, Unit #105','Southport 160','Southport','Brunswick','https://www.bing.com/maps/default.aspx?where1=4831%20PORT%20LOOP%20RD%20SE%20SOUTHPORT,%20NC%2028461-7486&amp;rtop=0~1~0','(910) 457-6076','(910) 457-7020','28461','','NC','9:00 am - 1:00 pm\n2:00 pm - 5:00 pm'),(102,'1433 US Hwy 21 S.','Sparta 162','Sparta','Alleghany','https://www.bing.com/maps/default.aspx?where1=348%20SOUTH%20MAIN%20STREET%20SPARTA,%20NC%2028675&amp;rtop=0~1~0','(336) 372-1627','(336) 372-8247','28675','','NC','8:00 am - 5:00 pm'),(103,'1639 US Hwy 74A Bypass, Ste 140','Spindale 180','Spindale','Rutherford','https://www.bing.com/maps/default.aspx?where1=1639%20US%20HWY%2074A%20BYPASS,%20SUITE%20140%20SPINDALE,%20NC%2028160&amp;rtop=0~1~0','(828) 287-3608','(828) 287-3600','28160','','NC','9:00 am - 5:00 pm'),(104,'316 NC Hwy. 210 N., Suite C','Spring Lake 112','Spring Lake','Wake','https://www.bing.com/maps/default.aspx?where1=316C%20NC%20HIGHWAY%20210%20N%20SPRING%20LAKE,%20NC%2028390&amp;rtop=0~1~0','(910) 497-0024','(910) 497-3707','28390','','NC','9:00 am - 5:00 pm'),(105,'11889 South 226 Highway','Spruce Pine 57','Spruce Pine','Mitchell','https://www.bing.com/maps/default.aspx?where1=11889%20NC%20226%20SOUTH%20SPRUCE%20PINE,%20NC%2028777&amp;rtop=0~1~0','(828) 765-2926','(828) 765-2926','28777','','NC','9:00 am - 12:30 pm\n1:30 pm - 5:00 pm'),(106,'121 W. Water St','Statesville 50','Statesville','Iredell','https://www.bing.com/maps/default.aspx?where1=121%20WEST%20WATER%20STREET%20STATESVILLE,%20NC%2028677&amp;rtop=0~1~0','(704) 873-0840','(704) 878-3185','28677','','NC','9:00 am - 5:00 pm'),(107,'101 Mitchell Street','Swain Cty - Bryson City 153','Bryson City','Swain','https://www.bing.com/maps/default.aspx?where1=101%20MITCHELL%20STREET%20BRYSON%20CITY,%20NC%2028713&amp;rtop=0~1~0','(828) 488-7855','(828) 488-8247','28713','','NC','8:00 am - 4:30 pm'),(108,'454 E. Main Street, Suite #3','Sylva 54','Sylva','Jackson','https://www.bing.com/maps/default.aspx?where1=454%20E%20MAIN%20ST;%20%20STE%203%20SYLVA,%20NC%2028779-3230&amp;rtop=0~1~0','(828) 586-3886','(828) 586-3886','28779','','NC','8:30 am - 1:00 pm\n2:00 pm - 4:30 pm'),(109,'1033 Randolph Street, Ste 13','Thomasville 164','Thomasville','Davidson','https://www.bing.com/maps/default.aspx?where1=1033%20RANDOLPH%20STREET%20SUITE%2013%20THOMASVILLE,%20NC%2027360&amp;rtop=0~1~0','(336) 476-5088','(336) 476-5070','27360','','NC','9:00 am - 5:00 pm'),(110,'448 Albemarle Road, Ste 1','Troy 80','Troy','Montgomery','https://www.bing.com/maps/default.aspx?where1=448%20ALBEMARLE%20ROAD%20SUITE%201%20TROY,%20NC%2027371&amp;rtop=0~1~0','(910) 572-2911','(910) 572-2911','27371','','NC','Mon, Tues, Thurs, Fri\n9:00 am - 12:30 pm\n1:45 pm - 5:00 pm\nWednesday\n9:00 am - 12:00 pm'),(111,'106 S. Water St','Tyrrell Cty 188','Columbia','Tyrrell','https://www.bing.com/maps/default.aspx?where1=106%20S.%20WATERS%20ST.%20COLUMBIA,%20NC%2027925&amp;rtop=0~1~0','(252) 796-4987','(252) 796-2676','27925','','NC','9:00 am - 5:00 pm'),(112,'101 S. Greene Street','Wadesboro Cty 169','Wadesboro','Anson','https://www.bing.com/maps/default.aspx?where1=101%20SOUTH%20GREENE%20STREET%20WADESBORO,%20NC%2028170&amp;rtop=0~1~0','(704) 994-3263','(704) 994-3292','28170','','NC','8:30 am - 5:00 pm'),(113,'2012 S. Main St, Suite 502','Wake Forest 148','Wake Forest','Wake','https://www.bing.com/maps/default.aspx?where1=2012%20SOUTH%20MAIN%20STREET%20SUITE%20502%20WAKE%20FOREST,%20NC%2027587&amp;rtop=0~1~0','(919) 554-4216','(919) 554-0770','27587','','NC','9:00 am - 5:00 pm'),(114,'112 Medical Village Dr. Unit C','Wallace 24','Wallace','Duplin','https://www.bing.com/maps/default.aspx?where1=112%20MEDICAL%20VILLAGE%20DRIVE,%20UNIT%20C%20WALLACE,%20NC%2028466&amp;rtop=0~1~0','(910) 285-1705','(910) 285-1700','28466','','NC','9:00 am - 5:00 pm'),(115,'208 W Third St','Walnut Cove (Town) 49','Walnut Cove','Stokes','https://www.bing.com/maps/default.aspx?where1=208%20W%203RD%20ST%20WALNUT%20COVE,%20NC%2027052-6995&amp;rtop=0~1~0','(336) 591-3257','(336) 591-3252','27052','','NC','8:00 am - 5:00 pm'),(116,'127 N. Market St.','Washington 55','Washington','Beaufort','https://www.bing.com/maps/default.aspx?where1=127%20NORTH%20MARKET%20STREET%20WASHINGTON,%20NC%2027889&amp;rtop=0~1~0','(252) 975-3691','(252) 975-3691','27889','','NC','9:00 am - 5:00 pm'),(117,'274 Winklers Creek Rd, Suite A','Watauga Cty 63','Boone','Watauga','https://www.bing.com/maps/default.aspx?where1=274%20WINKLERS%20CREEK%20RD.%20BOONE,%20NC%2028607&amp;rtop=0~1~0','(828) 268-2890','(828) 268-2315','28607','','NC','8:00 am - 4:30 pm'),(118,'80 Waynesville Plaza','Waynesville 161','Waynesville','Haywood','https://www.bing.com/maps/default.aspx?where1=80%20WAYNESVILLE%20PLAZA%20WAYNESVILLE,%20NC%2028786&amp;rtop=0~1~0','(828) 452-1579','(828) 452-1577','28786','','NC','9:00 am - 5:00 pm'),(119,'201 E Main St','Whiteville 186','Whiteville','Columbus','https://www.bing.com/maps/default.aspx?where1=201%20EAST%20MAIN%20STREET%20WHITEVILLE,%20NC%2028472&amp;rtop=0~1~0','(910) 642-0228','(910) 642-7500','28472','','NC','9:00 am - 12:30 pm\n1:30 pm - 5:00 pm'),(120,'2390 Carolina Beach Rd., Ste. 108','Wilmington 88','Wilmington','New Hanover','https://www.bing.com/maps/default.aspx?where1=2390%20CAROLINA%20BEACH%20ROAD%20SUITE%20108%20WILMINGTON,%20NC%2028401&amp;rtop=0~1~0','(910) 763-6865','(910) 763-6752','28401','','NC','9:00 am - 5:00 pm'),(121,'13 South Kerr Ave','Wilmington 150','Wilmington','New Hanover','https://www.bing.com/maps/default.aspx?where1=13%20SOUTH%20KERR%20AVENUE%20WILMINGTON,%20NC%2028403&amp;rtop=0~1~0','(910) 397-0947','(910) 397-0277','28403','','NC','9:00 am - 5:00 pm'),(122,'4000 Ward Blvd, Ste. D','Wilson 60','Wilson','Wilson','https://www.bing.com/maps/default.aspx?where1=4000%20WARD%20BLVD;%20%20STE%20D%20WILSON,%20NC%2027893-3279&amp;rtop=0~1~0','(252) 237-3746','(252) 291-8955','27893','','NC','9:00 am - 5:00 pm'),(123,'122 E. Granville Street','Windsor (Chamber) 103','Windsor','Bertie','https://www.bing.com/maps/default.aspx?where1=122%20EAST%20GRANVILLE%20STREET%20WINDSOR,%20NC%2027983&amp;rtop=0~1~0','(252) 794-9676','(252) 794-3253','27983','','NC','9:00 am - 5:00 pm'),(124,'1141 Silas Creek Parkway','Winston Salem 61','Winston Salem','Forsyth','https://www.bing.com/maps/default.aspx?where1=1141%20SILAS%20CREEK%20PARKWAY%20WINSTON-SALEM,%20NC%2027127&amp;rtop=0~1~0','(336) 725-1251','(336) 725-2795','27127','','NC','9:00 am - 5:00 pm'),(125,'101 S. State St','Yadkin Cty 20','Yadkinville','Yadkin','https://www.bing.com/maps/default.aspx?where1=101%20S%20STATE%20ST%20YADKINVILLE,%20NC%2027055-8249&amp;rtop=0~1~0','(336) 849-7923','(336) 849-7731','27055','','NC','8:00 am - 4:45 pm'),(126,'14 Town Square','Yancey Cty 189','Burnsville','Yancey','https://www.bing.com/maps/default.aspx?where1=14%20TOWN%20SQUARE%20BURNSVILLE,%20NC%2028714&amp;rtop=0~1~0','(828) 675-2634','(828) 682-2312','28714','','NC','9:00 am - 5:00 pm'),(127,'1430 Main Street','Yanceyville 173','Yanceyville','Caswell','https://www.bing.com/maps/default.aspx?where1=1430%20MAIN%20STREET%20YANCEYVILLE,%20NC%2027379&amp;rtop=0~1~0','(336) 459-3303','(336) 459-3300','27379','','NC','8:00 am - 5:00 pm'),(128,'520 West Gannon Ave','Wedgewood Shopping Ctr','Zebulon','Wake','https://www.bing.com/maps/default.aspx?where1=520%20WEST%20GANNON%20AVENUE%20ZEBULON,%20NC%2027597&amp;rtop=0~1~0','(919) 404-8059','(919) 404-8008','27597','','NC','9:00 am - 5:00 pm'),(129,'219-B Turner Drive','Reidsville','Reidsville','Rockingham','https://www.bing.com/maps/default.aspx?where1=219-B%20TURNER%20DRIVE%20REIDSVILLE,%20NC%2027320&amp;rtop=0~1~0','(336) 347-0593','(336) 347-0580','27320','','NC','9:00 am - 5:00 pm');

/********************* Create LPA Loaction master end  *************************/

/********************* Create Search insurance log master  *************************/

CREATE TABLE `search_insurance_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `agent_id` bigint(20) DEFAULT NULL,
  `profile_id` bigint(20) DEFAULT NULL,
  `lpa_id` bigint(20) DEFAULT NULL,
  `dmv_id` bigint(20) DEFAULT NULL,
  `search_log_id` bigint(20) DEFAULT NULL,
  `home` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/********************* Create Search insurance log master end *************************/


/********************* Change view remove ce compliance flag *************************/

USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER =  `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `iaa`.`search_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `p`.`insurance_company` AS `insurance_company`,
        `pd`.`ce_compliance` AS `ce_compliance`
    FROM
        (((((((((`iaa`.`agent` `a`
        LEFT JOIN `iaa`.`profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `iaa`.`business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `iaa`.`address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `iaa`.`state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `iaa`.`city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `iaa`.`agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `iaa`.`producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `iaa`.`pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `iaa`.`zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
    WHERE
        (`lic`.`is_active` = 1);



/********************* Change view remove ce compliance flag end *************************/


/************* Customer service table ***************/

CREATE TABLE `iaa`.`customer_service` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `company_name` VARCHAR(100) NULL,
  `customer_number` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
insert into iaa.customer_service values(1,'State Farm','800-782-8332'),(2,'Nationwide','800-282-1446'),(3,'Farm Bureau','919-782-1705'),(4,'Allstate','877-810-2920'),(5,'Integon-National General','800-325-1088'),(6,'Progressive','800-776-4737'),(7,'Travelers','800-842-5075'),(8,'Geico','800-207-7847'),(9,'Liberty Mutual','888-398-8924'),(10,'Metlife','800-422-4272'),(11,'Erie','800-458-0811');

/************* Customer service table end ***************/



/************* Agent call log table ***************/

CREATE TABLE `iaa`.`agent_call_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `calldate` DATETIME NULL,
  `user_number` VARCHAR(45) NULL,
  `agent_number` VARCHAR(45) NULL,
  `user_name` VARCHAR(100) NULL,
  `type` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`));


ALTER TABLE `iaa`.`agent_call_log` 
ADD COLUMN `npn` VARCHAR(45) NOT NULL AFTER `type`;


/************* Agent call log table end ***************/


/************* update facility view ***************/
USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `search_facility_records` AS
    SELECT 
        `f`.`id` AS `id`,
        `f`.`isn` AS `isn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `f`.`name` AS `name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `addr`.`addr_1` AS `addr_1`,
        `addr`.`addr_2` AS `addr_2`,
        `addr`.`addr_3` AS `addr_3`
    FROM
        (((((((`facility` `f`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `f`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `f`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)));



/************* update facility view end***************/



/************* Available carriers table for sign up agent 5 companies ***************/

CREATE TABLE `iaa`.`available_carriers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `carrier` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`));



CREATE TABLE `iaa`.`profile_carriers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `profile_id` BIGINT(20) NULL,
  `carriers_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_profile_carriers_1_idx` (`profile_id` ASC),
  INDEX `fk_profile_carriers_2_idx` (`carriers_id` ASC),
  CONSTRAINT `fk_profile_carriers_1`
    FOREIGN KEY (`profile_id`)
    REFERENCES `iaa`.`profile` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_carriers_2`
  
    FOREIGN KEY (`carriers_id`)
    REFERENCES `iaa`.`available_carriers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    
    ALTER TABLE `iaa`.`profile_carriers` 
ADD COLUMN `is_primary` BIT(1) NOT NULL DEFAULT 0 AFTER `carriers_id`;
    
    
    ALTER TABLE `iaa`.`available_carriers` 
ADD COLUMN `show_in_list` BIT(1) NOT NULL DEFAULT 1 AFTER `carrier`;
    

USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `search_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `p`.`insurance_company` AS `insurance_company`,
        `pd`.`ce_compliance` AS `ce_compliance`,
        `carr`.`carrier` AS `carrier_name`,
        `p`.`id` AS `profile_id`
    FROM
        (((((((((((`agent` `a`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
        LEFT JOIN `profile_carriers` `pcar` ON ((`pcar`.`profile_id` = `p`.`id`)))
        LEFT JOIN `available_carriers` `carr` ON ((`pcar`.`carriers_id` = `carr`.`id`)))
    WHERE
        (`lic`.`is_active` = 1);
        
        
  USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `search_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `pd`.`ce_compliance` AS `ce_compliance`,
        `carr`.`carrier` AS `insurance_company`,
        `p`.`id` AS `profile_id`
    FROM
        (((((((((((`agent` `a`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
        LEFT JOIN `profile_carriers` `pcar` ON ((`pcar`.`profile_id` = `p`.`id`)))
        LEFT JOIN `available_carriers` `carr` ON ((`pcar`.`carriers_id` = `carr`.`id`)))
    WHERE
        (`lic`.`is_active` = 1);
  
  
  
  USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `search_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `pd`.`ce_compliance` AS `ce_compliance`,
        `carr`.`carrier` AS `insurance_company`,
        `pcar`.`is_primary` AS `is_primary`,
        `p`.`id` AS `profile_id`
    FROM
        (((((((((((`agent` `a`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
        LEFT JOIN `profile_carriers` `pcar` ON ((`pcar`.`profile_id` = `p`.`id`)))
        LEFT JOIN `available_carriers` `carr` ON ((`pcar`.`carriers_id` = `carr`.`id`)))
    WHERE
        (`lic`.`is_active` = 1);
  



drop view search_records;


CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `search_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `pd`.`ce_compliance` AS `ce_compliance`,
        `carr`.`carrier` AS `insurance_company`,
        `pcar`.`is_primary` AS `is_primary`,
        `p`.`id` AS `profile_id`
    FROM
        (((((((((((`agent` `a`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
        LEFT JOIN `profile_carriers` `pcar` ON ((`pcar`.`profile_id` = `p`.`id`)))
        LEFT JOIN `available_carriers` `carr` ON ((`pcar`.`carriers_id` = `carr`.`id`)))
    WHERE
        (`lic`.`is_active` = 1);
 
 
 
 
 
 CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `search_agent_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `pd`.`ce_compliance` AS `ce_compliance`,
        `p`.`insurance_company` AS `insurance_company`,
        `p`.`id` AS `profile_id`
    FROM
        (((((((((`agent` `a`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
    WHERE
        (`lic`.`is_active` = 1)
 
/************* Available carriers table end ***************/


/************* Agent Extension table ***************/

CREATE TABLE `iaa`.`agent_extensions` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `agent_id` BIGINT(20) NOT NULL,
  `phone` VARCHAR(255) NULL,
  `extension` VARCHAR(255) NULL,
  `is_primary` BIT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_agent_extensions_1_idx` (`agent_id` ASC),
  CONSTRAINT `fk_agent_extensions_1`
    FOREIGN KEY (`agent_id`)
    REFERENCES `iaa`.`agent` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
 // ALTER TABLE `iaa`.`agent_extensions` 
// ADD COLUMN `is_primary` BIT(1) NULL DEFAULT 0 AFTER `extension`;


CREATE TABLE `iaa`.`extension_county` (
  `id` BIGINT(20) NOT NULL,
  `name` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `iaa`.`profile` 
ADD COLUMN `second_phone` VARCHAR(255) NULL DEFAULT NULL AFTER `insurance_company`;


/************* Agent Extension table end ***************/


/********************* Create MV1 admin *************************/


insert into iaa.jhi_user(id,login,password_hash,first_name,last_name,email,activated,lang_key,created_by,
created_date,last_modified_by,last_modified_date)
 values(5,'mv1admin','$2a$10$WPGT.GhMUs1nz9IfVfuRfu6PFYaKfBVM6PPQUxE8tPPsJsGhcddom','admin','admin','mv1stopuser@gmail.com',1,'en','system',
 now(),'system', now());
 
 insert into jhi_user_authority values(5,'ROLE_ADMIN');
 
 
insert into iaa.profile values(1,'admin','','admin','','mv1stopuser@gmail.com','','','ADMIN',
1,5,now(),now(),1,0,'',null,'','','','');


/****************** Create MV1 admin end ****************/

/********************* update table MV1 payment *************************/
ALTER TABLE `iaa`.`payment` 
ADD COLUMN `txn_type` VARCHAR(255) NULL DEFAULT NULL AFTER `is_deleted`;
ADD COLUMN `cheque_no` BIGINT(20) NULL DEFAULT NULL AFTER `txn_type`;


ALTER TABLE `iaa`.`payment` 
ADD COLUMN `routing_no` VARCHAR(45) NULL DEFAULT NULL AFTER `cheque_no`,
ADD COLUMN `account_no` VARCHAR(45) NULL DEFAULT NULL AFTER `routing_no`,
ADD COLUMN `account_type` VARCHAR(45) NULL DEFAULT NULL AFTER `account_no`;


ALTER TABLE `iaa`.`payment` 
CHANGE COLUMN `gateway_resp` `gateway_resp` VARCHAR(700) NULL DEFAULT NULL ;


/********************* update MV1 payment end *************************/


/********************* update MV1 search_agent_records *************************/

USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `search_agent_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `pd`.`ce_compliance` AS `ce_compliance`,
        `p`.`insurance_company` AS `insurance_company`,
        `p`.`id` AS `profile_id`,
        `p`.`user_id` AS `user_id`
    FROM
        (((((((((`agent` `a`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
    WHERE
        (`lic`.`is_active` = 1);
/********************* update MV1 search_agent_records end*************************/


/********************* update MV1 search_records *************************/
USE `iaa`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `search_records` AS
    SELECT 
        `a`.`id` AS `id`,
        `a`.`npn` AS `npn`,
        `p`.`first_name` AS `first_name`,
        `p`.`last_name` AS `last_name`,
        `p`.`phone` AS `phone`,
        `addr`.`zip` AS `zip`,
        `s`.`name` AS `state`,
        `c`.`name` AS `city`,
        `pc`.`package_id` AS `package_id`,
        `z`.`zip` AS `slot_zip`,
        `pc`.`slots` AS `slots`,
        `p`.`jhi_type` AS `jhi_type`,
        `b`.`business_name` AS `business_name`,
        `p`.`pref_company` AS `pref_company`,
        `p`.`comments` AS `comments`,
        `pd`.`ce_compliance` AS `ce_compliance`,
        `carr`.`carrier` AS `insurance_company`,
        `pcar`.`is_primary` AS `is_primary`,
        `p`.`id` AS `profile_id`,
        `p`.`user_id` AS `user_id`
        
    FROM
        (((((((((((`agent` `a`
        LEFT JOIN `profile` `p` ON ((`p`.`id` = `a`.`profile_id`)))
        LEFT JOIN `business` `b` ON ((`b`.`id` = `a`.`business_id`)))
        LEFT JOIN `address` `addr` ON ((`addr`.`id` = `b`.`address_id`)))
        LEFT JOIN `state` `s` ON ((`s`.`id` = `addr`.`state_id`)))
        LEFT JOIN `city` `c` ON ((`c`.`id` = `addr`.`city_id`)))
        LEFT JOIN `agent_lic` `lic` ON ((`lic`.`id` = `a`.`lic_id`)))
        LEFT JOIN `producer` `pd` ON ((`pd`.`id` = `lic`.`producer_id`)))
        LEFT JOIN `pack_subscription` `pc` ON ((`pc`.`profile_id` = `p`.`id`)))
        LEFT JOIN `zip_area` `z` ON ((`z`.`id` = `pc`.`zip_area_id`)))
        LEFT JOIN `profile_carriers` `pcar` ON ((`pcar`.`profile_id` = `p`.`id`)))
        LEFT JOIN `available_carriers` `carr` ON ((`pcar`.`carriers_id` = `carr`.`id`)))
    WHERE
        (`lic`.`is_active` = 1);
/********************* update MV1 search_records end*************************/

/********************* update MV1 search_logs *************************/
ALTER TABLE `iaa`.`search_logs` 
CHANGE COLUMN `company` `company` VARCHAR(255) NULL DEFAULT NULL ;

/********************* update MV1 search_logs end*************************/
