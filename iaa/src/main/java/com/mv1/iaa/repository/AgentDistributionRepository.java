package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.AgentDistribution;


/**
 * Spring Data  repository for the AgentDistribution entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentDistributionRepository extends JpaRepository<AgentDistribution, Long> {

}
