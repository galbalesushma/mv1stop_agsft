package com.mv1.iaa.repository;

import java.time.Instant;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.ProfileCarriers;
import com.mv1.iaa.domain.User;

@SuppressWarnings("unused")
@Repository
public interface ProfileCarriersRepository extends JpaRepository<ProfileCarriers, Long> {

//	
//	List<ProfileCarriers> findAllByProfileId();

	List<ProfileCarriers> findAllByProfileId(Long profileId);

	@Query(value = "SELECT carriers_id FROM profile_carriers pc	"
			+ "	where pc.profile_id =:pId and is_primary = 0 ", nativeQuery = true)
	List<Integer> getProfileCarriers(@Param("pId") Long pId);

	@Query(value = "  SELECT * FROM profile_carriers pc 	"
			+ "	where is_primary = 1 and profile_id = :pId ", nativeQuery = true)
	ProfileCarriers findPrimaryCompanyOfProfile(@Param("pId") Long pId);
}
