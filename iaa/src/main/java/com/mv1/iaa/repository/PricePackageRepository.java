package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.PricePackage;


/**
 * Spring Data  repository for the PricePackage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PricePackageRepository extends JpaRepository<PricePackage, Long> {

}
