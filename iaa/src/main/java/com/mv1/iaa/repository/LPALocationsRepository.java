package com.mv1.iaa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.LPALocations;


/**
 * Spring Data  repository for the LPALocations entity.
 */
@Repository
public interface LPALocationsRepository extends JpaRepository<LPALocations, Long> {

	@Query("FROM LPALocations where city LIKE CONCAT('%',:city, '%')")
	 List<LPALocations> findByCityContaining(@Param("city") String city);
	
}
