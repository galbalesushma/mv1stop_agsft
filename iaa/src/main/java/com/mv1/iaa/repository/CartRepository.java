package com.mv1.iaa.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.Cart;

/**
 * Spring Data  repository for the Cart entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    @Query(value = "select distinct cart from Cart cart left join fetch cart.subscriptions",
        countQuery = "select count(distinct cart) from Cart cart")
    Page<Cart> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct cart from Cart cart left join fetch cart.subscriptions")
    List<Cart> findAllWithEagerRelationships();

    @Query("select cart from Cart cart left join fetch cart.subscriptions where cart.id =:id")
    Optional<Cart> findOneWithEagerRelationships(@Param("id") Long id);

    @Query(value="select zip from zip_area z where z.id =:id",nativeQuery=true)
    String findZipCodeFromId(@Param("id") Long id);
}
