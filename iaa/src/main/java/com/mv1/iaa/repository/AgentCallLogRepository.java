package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.AgentCallLog;

@Repository
public interface AgentCallLogRepository extends JpaRepository<AgentCallLog, Long>{

}
