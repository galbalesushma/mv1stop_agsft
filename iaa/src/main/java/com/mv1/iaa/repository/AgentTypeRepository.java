package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.AgentType;


/**
 * Spring Data  repository for the AgentType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentTypeRepository extends JpaRepository<AgentType, Long> {

}
