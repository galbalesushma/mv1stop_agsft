package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.AgentExtension;

@Repository
public interface AgentExtensionRepository extends JpaRepository<AgentExtension, Long> {

	
	@Query(value="select * from agent_extensions ae where ae.agent_id = :agentId and "
			+ "	ae.is_primary = 1 "
			,nativeQuery= true)
	AgentExtension findByAgentAndPrimary(@Param("agentId") Long agentId);


	@Query(value="select * from agent_extensions ae where ae.agent_id = :agentId and "
			+ "	ae.is_primary = 0 "
			,nativeQuery= true)
	AgentExtension findByAgentAndSecondary(@Param("agentId") Long agentId);
	
}
