package com.mv1.iaa.repository;

import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.ZipArea;


/**
 * Spring Data  repository for the ZipArea entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomZipAreaRepository extends ZipAreaRepository {
    
    ZipArea findOneByZip(String zip);
    
    ZipArea findOneById(Long id);
}
