package com.mv1.iaa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.AvailableCarriers;

@SuppressWarnings("unused")
@Repository
public interface AvailableCarriersRepository extends JpaRepository<AvailableCarriers, Long> {

	
	  @Query(value = 
		   		 "  SELECT * FROM iaa.available_carriers  	" + 
		   		 "	where carrier = :carrier and show_in_list = :flag ",
				  nativeQuery =  true)
	AvailableCarriers findByCarrierAndShow(@Param("carrier") String carrier , @Param("flag")  boolean flag);

	  
	  @Query(value = 
		   		 "  SELECT * FROM iaa.available_carriers pc 	" + 
		   		 "	where show_in_list = :flag order by pc.carrier",
				  nativeQuery =  true)
	List<AvailableCarriers> findAllShowInList(@Param("flag")  boolean flag);

//	Optional<AvailableCarriers> findByCarrierAndShow(String insuranceCompany, int i);


}
