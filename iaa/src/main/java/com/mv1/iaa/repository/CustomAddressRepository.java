package com.mv1.iaa.repository;

import java.util.Set;

import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.Address;
import com.mv1.iaa.domain.Contact;

@Repository
public interface CustomAddressRepository extends AddressRepository {

	Address findByAddr1AndContacts(String trim, Set<Contact> contacts);

	Address findByAddr1(String addr1);

}
