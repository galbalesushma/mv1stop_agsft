package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.SearchLogs;


/**
 * Spring Data  repository for the SearchLogs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SearchLogsRepository extends JpaRepository<SearchLogs, Long> {

}
