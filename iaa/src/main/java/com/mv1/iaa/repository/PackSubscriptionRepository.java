package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.PackSubscription;


/**
 * Spring Data  repository for the PackSubscription entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PackSubscriptionRepository extends JpaRepository<PackSubscription, Long> {

}
