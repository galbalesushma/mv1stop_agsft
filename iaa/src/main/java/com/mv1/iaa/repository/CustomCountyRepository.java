package com.mv1.iaa.repository;

import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.County;

@Repository
public interface CustomCountyRepository extends CountyRepository {

	County findOneByNameIgnoreCase(String stateCode);
}
