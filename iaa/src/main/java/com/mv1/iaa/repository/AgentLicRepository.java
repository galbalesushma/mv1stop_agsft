package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.AgentLic;


/**
 * Spring Data  repository for the AgentLic entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentLicRepository extends JpaRepository<AgentLic, Long> {

}
