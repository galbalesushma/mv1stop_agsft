package com.mv1.iaa.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.Cart;

/**
 * Spring Data  repository for the Cart entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomCartRepository extends CartRepository {

    
    Optional<Cart> findOneByProfileIdAndIsCompleted(@Param("profileId") Long profileId, 
	    @Param("isCompleted") Boolean isCompleted);

    Cart findOneById(@Param("id") Long id);
    
    @Query(value = 
        "select    " +
        "    p.jhi_rank as        rank,    " +
        "    case   " +
        "       when p.jhi_rank = 'ONE'    " + 
        "       then abs( 10 - ifnull (sum(s.slots), 0))    " + 
        "       when  p.jhi_rank = 'THREE'  || a.zip = '00000'    " + 
        "       then abs(20)    " + 
        "    else    abs( 20 - ifnull (sum(s.slots), 0))    " + 
        "   end as                 slot    " + 
        "from     " +
        "    price_package p    " +
        "left outer join pack_subscription s    " +
        "    on p.id = s.package_id     " +
        /*"    and s.is_active = 1    " +*/
        "    and s.is_deleted = 0    " +
        "    and s.valid_till >= now()    " +
        "left outer join profile pr     " +
        "    on pr.id = s.profile_id    " +
        "    and pr.jhi_type = :type    " +
        "left outer join zip_area a     " +
        "    on a.id = s.zip_area_id    " +
        "    and  a.zip = :zip    " +
        "group by    " +
        "    p.jhi_rank,a.zip" ,
        nativeQuery =  true)
    Object[][] findAvailableItemsByZip(@Param("zip") String zip, @Param("type") String type);
    
    @Modifying(clearAutomatically = true)
    @Query(value = 
         "delete                        " +
         "from cart_subscriptions       " +
         "where                         " +
         "  carts_id = :cartId          " +
         "  and subscriptions_id in :ids " 
        , nativeQuery = true) 
    Integer deleteSubscriptionByIds(@Param("cartId") Long cartId, @Param("ids") List<Long> ids);
    
    
    @Query(value = 
    		 "     SELECT sum(slots) FROM iaa.pack_subscription s		"+
    		 "    left outer join zip_area a 		"+	
    		 "    	on a.id = s.zip_area_id		"+	
    		 "    left outer join profile pr 	"+	
    		 "     on pr.id = s.profile_id  	"+
    		 "       where	"+
    		 "     package_id = :packageId and a.zip =	:zip and pr.jhi_type = :type		",
		  nativeQuery =  true)
 String findAvailableItemsByZipyPackageId(@Param("zip") String zip, @Param("type") String type
		 , @Param("packageId") String packageId);
}
