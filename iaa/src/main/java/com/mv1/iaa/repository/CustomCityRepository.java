package com.mv1.iaa.repository;

import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.City;


/**
 * Spring Data  repository for the City entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomCityRepository extends CityRepository {

    City findOneByNameIgnoreCase(String name);
}
