package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.Profile;


/**
 * Spring Data  repository for the Agent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomProfileRepository extends ProfileRepository {
    
    @Query(value = 
	    "select  " + 
	    "    p.id          profileId,  " +
	    "    p.user_id     userId,     " +
	    "    p.first_name  firstName,  " +
	    "    p.last_name   lastName,   " +
	    "    p.jhi_type    type        " +
	    "from   " + 
	    "profile p  " + 
	    "left outer join agent a  " + 
        "    on p.id = a.profile_id  " + 
        "left outer join business b  " + 
	    "    on a.business_id = b.id  " + 
	    "left outer join address ba  " + 
	    "    on b.address_id = ba.id  " + 
	    "left outer join contact c  " + 
	    "    on ba.id = c.address_id  " + 
	    "where   " + 
	    "    p.is_deleted = 0  "+
	    "    and c.email = :email  " ,
		nativeQuery =  true)
    Object[][] findAgentProfileByEmail(@Param("email") String email);
    
    @Query(value = 
        "select  " + 
        "    p.id          profileId,  " +
        "    p.user_id     userId,     " +
        "    p.first_name  firstName,  " +
        "    p.last_name   lastName,   " +
        "    p.jhi_type    type        " +
        "from   " + 
        "profile p  " + 
        "left outer join facility f  " + 
        "    on p.id = f.profile_id  " + 
        "left outer join business b  " + 
        "    on f.business_id = b.id  " + 
        "left outer join address ba  " + 
        "    on b.address_id = ba.id  " + 
        "left outer join contact c  " + 
        "    on ba.id = c.address_id  " + 
        "where   " + 
        "    p.is_deleted = 0  "+
        "    and c.email = :email  " ,
        nativeQuery =  true)
    Object[][] findFacilityProfileByEmail(@Param("email") String email);
        
    @Query(value = 
        "select                         " + 
        "    p.id          profileId,   " +
        "    p.user_id     userId,      " +
        "    p.first_name  firstName,   " +
        "    p.last_name   lastName,    " +
        "    p.jhi_type    type         " +
        "from                           " + 
        "profile p                      " + 
        "left outer join jhi_user u     " + 
        "    on u.id = p.user_id        " + 
        "where                          " + 
        "    p.is_deleted = 0           " +
        "    and (  u.email = :email    " +
        "    or     u.login = :email  ) " ,
        nativeQuery =  true)
    Object[][] findProfileByEmail(@Param("email") String email);
    
    
    Profile findProfileByPrefEmail(@Param("email") String email);
    
    
    @Query(value = 
            "select  " + 
            "    p.id          profileId,  " +
            "    p.user_id     userId,     " +
            "    p.first_name  firstName,  " +
            "    p.last_name   lastName,   " +
            "    p.jhi_type    type        " +
            "from   " + 
            "profile p  " + 
            "left outer join facility f  " + 
            "    on p.id = f.profile_id  " + 
            "left outer join business b  " + 
            "    on f.business_id = b.id  " + 
            "left outer join address ba  " + 
            "    on b.address_id = ba.id  " + 
            "left outer join contact c  " + 
            "    on ba.id = c.address_id  " + 
            "where   " + 
            "    p.is_deleted = 0  "+
            "    and p.id = :pId  " ,
            nativeQuery =  true)
        Object[][] findFacilityProfileById(@Param("pId") String pId);
}
