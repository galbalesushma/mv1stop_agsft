package com.mv1.iaa.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.business.view.web.req.AgentVM;
import com.mv1.iaa.domain.Agent;
import com.mv1.iaa.domain.CustomerService;
import com.mv1.iaa.domain.Profile;


/**
 * Spring Data  repository for the Agent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomAgentRepository extends AgentRepository {
    
    @Query(value = 
	    "select  " + 
	    "    c.id,             " +
	    "    c.email           " + 
	    "from   " + 
	    "    agent a  " + 
	    "left outer join profile p  " + 
	    "    on a.profile_id = p.id  " + 
	    "left outer join business b  " + 
	    "    on a.business_id = b.id  " + 
	    "left outer join agent_lic al   " + 
	    "    on a.lic_id = al.id  " + 
	    "left outer join state ds  " + 
	    "    on a.domicile_state_id = ds.id  " + 
	    "left outer join address ba  " + 
	    "    on b.address_id = ba.id  " + 
	    "left outer join contact c  " + 
	    "    on ba.id = c.address_id  " + 
	    "where   " + 
	    "    a.is_deleted = 0  "+
	    "    and a.npn = :npn  " ,
		nativeQuery =  true)
    Object[][] findEmailByNpn(@Param("npn") String npn);
    
    Agent findOneByNpn(String npn);
    
    Agent findOneByProfileId(Long profileId);
    
    List<Agent> findByNpnIn(Set<String> npn);
//added new key (p.user_id isRegistered) for registered users -paper check task
    @Query(value = 
	    "select distinct  " + 
	    "    a.id                                ,  " + 
	    "    a.npn                               ,  " + 
	    "    a.naicno                            ,  " + 
	    "    a.fein                              ,  " +     
	    
	    "    a.resident_state            resState,  " + 
	    "    p.first_name                firstName,  " + 
	    "    p.middle_name               middleName,  " + 
	    "    p.last_name                 lastName,  " + 
	    "    p.suffix                    suffix,  " + 
	    "    b.business_name             businessName,  " + 
	    "    al.lic_number               licNumber,  " + 
	    "    al.jhi_type                 licType,  " + 
	    "    al.authority                lineAuth,  " + 
	    "    al.first_active_date        firstActiveDate,  " + 
	    "    al.effective_date           effectiveDate,  " + 
	    "    al.expiration_date          expDate,  " + 
	    "    case   when al.is_active = 1    " + 
	    "	    then 'Active'    " + 
	    "	    else 'InActive'    " + 
	    "	end as                       licStatus,    " + 
	    "    ds.id                       domState,    " + 
	    "    ba.addr_1                   bAddr1,  " + 
	    "    ba.addr_2                   bAddr2,  " + 
	    "    ba.addr_3                   bAddr3,  " + 
	    "    ba.zip                      bZip,  " + 
	    "    ba.country                  bCountry,  " + 
//	    "    bc.id                       bCity,  " + 
//	    "    bs.id		             bState,  " + 
	    "    pr.ce_compliance            isCECompliant,  " + 
	    "    pa.addr_1                   pAddr1,  " + 
	    "    pa.addr_2                   pAddr2,  " + 
	    "    pa.addr_3                   pAddr3,  " + 
	    "    pa.zip                      pZip,  " + 
	    "    pa.country                  pCountry,  " + 
//	    "    pc.id                       pCity,  " + 
//	    "    ps.id               	     pState,  " + 
	    "    c.office_phone              bPhone,  " + 
	    "    c.email                     bEmail , " + 
	    "	(select  if ((count(*) >0 ), 'true','false' )	"+
		"	from cart crt " +
		"	join pack_subscription psub on crt.profile_id =  psub.profile_id  " +
		"	where psub.package_id= 3 and  crt.is_completed = 1  " +
		"	and crt.profile_id = a.profile_id  " +
		"	) as incAB,  " +
		"	(select  if ((count(*) >0 ), 'true','false' )  " +
		"	from cart crt  " +
		"	join pack_subscription psub on crt.profile_id =  psub.profile_id  " +
			"where psub.package_id= 1 and  crt.is_completed = 1  " +
		"	and crt.profile_id = a.profile_id  " +
		"	) as certified, "  +
		"	(select  if ((count(*) >0 ), 'true','false' ) " +
		"	from cart crt " +
		"	join pack_subscription psub on crt.profile_id =  psub.profile_id  " +
		"	where psub.package_id= 2 and  crt.is_completed = 1 " +
		"	and crt.profile_id = a.profile_id " +
		"	) as preCertified, 	 " +
		"    p.id                    profileId,  " + 
		" (select valid_till from pack_subscription pm  where  pm.profile_id= p.id and pm.package_id=3 )  as  paymentDate, "+
		"    p.phone                     prefPhone , " + 
		"    p.pref_mobile               	     prefMobile, " + 
		"    p.user_id               	     isRegistered " + 
	    "from   " + 
	    "    agent a  " + 
	    "left outer join profile p  " + 
	    "    on a.profile_id = p.id  " + 
	    "left outer join business b  " + 
	    "    on a.business_id = b.id  " + 
	    "left outer join agent_lic al   " + 
	    "    on a.lic_id = al.id  " + 
	    "left outer join state ds  " + 
	    "    on a.domicile_state_id = ds.id  " + 
	    "left outer join address ba  " + 
	    "    on b.address_id = ba.id  " + 
	    "left outer join contact c  " + 
	    "    on ba.id = c.address_id  " + 
//	    "left outer join city bc  " + 
//	    "    on ba.city_id = bc.id  " + 
//	    "left outer join state bs  " + 
//	    "    on ba.state_id = bs.id  " + 
	    "left outer join producer pr  " + 
	    "    on al.producer_id = pr.id  " + 
	    "left outer join address pa  " + 
	    "    on pr.addr_id = pa.id  " + 
	  
//	    "left outer join city pc  " + 
//	    "    on pa.city_id = pc.id or ba.city_id = pc.id " + 
//	    "left outer join state ps  " + 
//	    "    on pa.state_id = ps.id or  ba.state_id = ps.id    " + 
//	    "left outer join county co  " + 
//	    "    on pa.county_id = co.id  or  ba.county_id = co.id   " + 
	    "where   " + 
	    "    a.is_deleted = 0 " ,
		nativeQuery =  true)
    Object[][] getAllAgents();
    
    
    
    @Query(value = 
	    "select  " + 
	    "    a.id                                ,  " + 
	    "    a.npn                               ,  " + 
	    "    a.naicno                            ,  " + 
	    "    a.fein                              ,  " + 
	    "    a.resident_state            resState,  " + 
	    "    p.first_name                firstName,  " + 
	    "    p.middle_name               middleName,  " + 
	    "    p.last_name                 lastName,  " + 
	    "    p.suffix                    suffix,  " + 
	    "    b.business_name             businessName,  " + 
	    "    al.lic_number               licNumber,  " + 
	    "    al.jhi_type                 licType,  " + 
	    "    al.authority                lineAuth,  " + 
	    "    al.first_active_date        firstActiveDate,  " + 
	    "    al.effective_date           effectiveDate,  " + 
	    "    al.expiration_date          expDate,  " + 
	    "    case   when al.is_active = 1    " + 
	    "	    then 'Active'    " + 
	    "	    else 'InActive'    " + 
	    "	end as                       licStatus,    " + 
	    "    ds.id                       domState,    " + 
	    "    ba.addr_1                   bAddr1,  " + 
	    "    ba.addr_2                   bAddr2,  " + 
	    "    ba.addr_3                   bAddr3,  " + 
	    "    ba.zip                      bZip,  " + 
	    "    ba.country                  bCountry,  " + 
	    "    ba.county_id                  bCounty,  " + 
	    "    bc.id                       bCity,  " + 
	    "    bs.id		             bState,  " + 
	    "    pr.ce_compliance            isCECompliant,  " + 
	    "    pa.addr_1                   pAddr1,  " + 
	    "    pa.addr_2                   pAddr2,  " + 
	    "    pa.addr_3                   pAddr3,  " + 
	    "    pa.zip                      pZip,  " + 
	    "    pa.country                  pCountry,  " + 
	    "    pa.county_id                pCounty,  " + 
	    "    pc.id                       pCity,  " + 
	    "    ps.id               	     pState,  " + 
	    "    c.office_phone              bPhone,  " + 
	    "    c.phone              bMobile,  " + 
	    "    c.email                     bEmail , " + 
	    "		p.is_speaks_spanish    speaksSpanish, "+
	    "    p.pref_company                     prefCompany, " + 
	    "    p.comments          comment, " + 
	    "    p.pref_email          prefEmail " + 
	    "from   " + 
	    "    agent a  " + 
	    "left outer join profile p  " + 
	    "    on a.profile_id = p.id  " + 
	    "left outer join business b  " + 
	    "    on a.business_id = b.id  " + 
	    "left outer join agent_lic al   " + 
	    "    on a.lic_id = al.id  " + 
	    "left outer join state ds  " + 
	    "    on a.domicile_state_id = ds.id  " + 
	    "left outer join address ba  " + 
	    "    on b.address_id = ba.id  " + 
	    "left outer join contact c  " + 
	    "    on ba.id = c.address_id  " + 
	    "left outer join city bc  " + 
	    "    on ba.city_id = bc.id  " + 
	    "left outer join state bs  " + 
	    "    on ba.state_id = bs.id  " + 
	    "left outer join producer pr  " + 
	    "    on al.producer_id = pr.id  " + 
	    "left outer join address pa  " + 
	    "    on pr.addr_id = pa.id  " + 
	    "left outer join city pc  " + 
	    "    on pa.city_id = pc.id  " + 
	    "left outer join state ps  " + 
	    "    on pa.state_id = ps.id      " + 
	    "where   " + 
	    "    a.is_deleted = 0  "
	    + "	 and a.npn = :npn " ,
		nativeQuery =  true)
    Object[][] getAgent(@Param("npn") String npn);
    
    
    @Query(value = 
    	    "select  " + 
    	    "    a.id                                ,  " + 
    	    "    a.npn                               ,  " + 
    	    "    a.naicno                            ,  " + 
    	    "    a.fein                              ,  " + 
    	    "    a.resident_state            resState,  " + 
    	    "    p.first_name                firstName,  " + 
    	    "    p.middle_name               middleName,  " + 
    	    "    p.last_name                 lastName,  " + 
    	    "    p.suffix                    suffix,  " + 
    	    "    b.business_name             businessName,  " + 
    	    "    al.lic_number               licNumber,  " + 
    	    "    al.jhi_type                 licType,  " + 
    	    "    al.authority                lineAuth,  " + 
    	    "    al.first_active_date        firstActiveDate,  " + 
    	    "    al.effective_date           effectiveDate,  " + 
    	    "    al.expiration_date          expDate,  " + 
    	    "    case   when al.is_active = 1    " + 
    	    "	    then 'Active'    " + 
    	    "	    else 'InActive'    " + 
    	    "	end as                       licStatus,    " + 
    	    "    ds.id                       domState,    " + 
    	    "    ba.addr_1                   bAddr1,  " + 
    	    "    ba.addr_2                   bAddr2,  " + 
    	    "    ba.addr_3                   bAddr3,  " + 
    	    "    ba.zip                      bZip,  " + 
    	    "    ba.country                  bCountry,  " + 
    	    "    ba.county_id                  bCounty,  " + 
    	    "    bc.id                       bCity,  " + 
    	    "    bs.id		             bState,  " + 
    	    "    pr.ce_compliance            isCECompliant,  " + 
    	    "    pa.addr_1                   pAddr1,  " + 
    	    "    pa.addr_2                   pAddr2,  " + 
    	    "    pa.addr_3                   pAddr3,  " + 
    	    "    pa.zip                      pZip,  " + 
    	    "    pa.country                  pCountry,  " + 
    	    "    pa.county_id                pCounty,  " + 
    	    "    pc.id                       pCity,  " + 
    	    "    ps.id               	     pState,  " + 
    	    "    c.office_phone              bPhone,  " + 
    	    "    c.phone             		 bMobile,  " + 
    	    "    c.email                     bEmail, " + 
    	    "    p.is_speaks_spanish          speaksSpanish ," + 
    	    "    p.pref_company                     prefCompany, " + 
    	    "    p.comments          comments ," + 
    	    "    p.pref_email          prefEmail " + 
    	    "from   " + 
    	    "    agent a  " + 
    	    "left outer join profile p  " + 
    	    "    on a.profile_id =  p.id  " + 
    	    "left outer join business b  " + 
    	    "    on a.business_id = b.id  " + 
    	    "left outer join agent_lic al   " + 
    	    "    on a.lic_id = al.id  " + 
    	    "left outer join state ds  " + 
    	    "    on a.domicile_state_id = ds.id  " + 
    	    "left outer join address ba  " + 
    	    "    on b.address_id = ba.id  " + 
    	    "left outer join contact c  " + 
    	    "    on ba.id = c.address_id  " + 
    	    "left outer join city bc  " + 
    	    "    on ba.city_id = bc.id  " + 
    	    "left outer join state bs  " + 
    	    "    on ba.state_id = bs.id  " + 
    	    "left outer join producer pr  " + 
    	    "    on al.producer_id = pr.id  " + 
    	    "left outer join address pa  " + 
    	    "    on pr.addr_id = pa.id  " + 
    	    "left outer join city pc  " + 
    	    "    on pa.city_id = pc.id  " + 
    	    "left outer join state ps  " + 
    	    "    on pa.state_id = ps.id      " + 
    	    "where   " + 
    	    "    a.is_deleted = 0  "
    	    + "	 and a.profile_id = :profileId " ,
    		nativeQuery =  true)
        Object[][] getAgentByProfileId(@Param("profileId") Long profileId);

    
    @Query(value = 
            "select  distinct " + 
            "    p.package_id                    packageId,  " + 
            "    p.slots                             , " + 
           "    p.duration                          ,  " + 
           "    p.created_at                       createdAt,  " + 
           "    p.valid_till                         validTill ,  " + 
            "    p.zip_area_id                       zipAreaId,  " + 
             "    p.profile_id                         profileId ,  " + 
             "    zip.zip                         zipcode  " + 
             "from   " + 
             "    cart c  " + 
            "inner join pack_subscription p   " + 
            "   on p.profile_id  =  c.profile_id  " + 
            "inner join zip_area zip  " + 
            "   on zip.id  =  p.zip_area_id  " + 
            "where   " +
             "p.profile_id = :profileId and c.is_completed = 1  and p.is_active= 1 and  p.valid_till >= now()  ",
            nativeQuery=true)
 Object[][] findByProfileId(@Param("profileId") Long profileId);


    

	@Query(value="select id from profile where pref_email=:email and user_id=:id",nativeQuery=true)
	Long findByEmailAndUseId(@Param("email") String email,@Param("id") Long id);

	
	@Query(value="select business_name from business where  business_name like :search",nativeQuery=true)
	List<String> findAllCompanyBySearch(@Param("search") String search);
	
	@Query(value=
			 "      select distinct zip from address a      " + 
					 "     left outer join profile p          " + 
					 "     on p.addr_id = a.id           " + 
					 "     where p.jhi_type like 'AGENT' and a.zip like :search      " 
			,nativeQuery=true)
	List<String> findAgentZipCodeBySearch(@Param("search") String search);
	
	
	@Query(value="select distinct first_name from profile p inner join agent a on p.id = a.profile_id where p.jhi_type like 'AGENT' and p.first_name like :search",nativeQuery=true)
	List<String> findAllByFirstName(@Param("search") String search);
	
	@Query(value="select distinct last_name from profile p inner join agent a on p.id = a.profile_id where p.jhi_type like 'AGENT' and  p.last_name like :search",nativeQuery=true)
	List<String> findAllByLastName(@Param("search") String search);
	
	@Query(value="select npn from agent where npn like :search ",nativeQuery=true)
	List<String> findAllNpnBySearch(@Param("search")String search);
	
	@Query(value="select * from agent where profile_id=:pId ",nativeQuery=true)
	Agent findAgentByProfileId(@Param("pId") Long pId);
	
	//Query to get all registered and unregistered users for paper cheque payment
	
	@Query(value="select * from jhi_user u where u.activated=1;",nativeQuery=true)
	List<AgentVM> findAllRegisteredAgents(); 
	
	@Query(value="select * from jhi_user u where u.activated=0;",nativeQuery=true)
	List<AgentVM> findAllUnRegisteredAgents();
	
//	@Query(value = "select * from city  c where c.state_code=:stateCode and c.name like :search", nativeQuery = true)
//	Object[][]  findCityByStateAndSearch(@Param("stateCode") String stateCode, @Param("search") String search);

	
	
	@Query(value=
			"	select distinct name from city c where c.name  like :search and c.id in	 "+
				"	(select distinct city_id from address a	 "+
				"	left outer join producer p	 "+
				"		on p.addr_id = a.id	 "+
				"	  left outer join business b	 "+
				"		on b.address_id = a.id	 "+
				"	 left outer join profile pr	 "+
				"		on b.address_id = pr.addr_id   and pr.jhi_type like :type1 )	"
			, nativeQuery = true)
	List<String>  findCityByStateAndSearch(@Param("search") String search, @Param("type1") String type1);
	
	//	@Query(value="select * from county where state_code=:stateCode and name like :search",nativeQuery=true)
//	Object[][] findCountyByStateAndSearch(@Param("stateCode") String stateCode, @Param("search") String search);
	
	@Query(value=
	"	select distinct name from county c where c.name like :search and c.id in	 "+
		"	(select distinct county_id from address a	 "+
		"	left outer join producer p	 "+
		"		on p.addr_id = a.id	 "+
		"	  left outer join business b	 "+
		"		on b.address_id = a.id	 "+
		"	 left outer join profile pr	 "+
		"		on b.address_id = pr.addr_id   and pr.jhi_type like :type1 )	"
	, nativeQuery = true)
	List<String> findCountyByStateAndSearch(@Param("search") String search,@Param("type1") String type1);
	
	
	
	
	
	@Query(value= 
		"select  " + 
		"    a.id                                ,  " + 
		"    a.npn                               ,  " + 
		"    p.first_name                firstName,  " + 
		"    p.middle_name               middleName,  " + 
		"    p.last_name                 lastName,  " + 
		"    p.suffix                    suffix,  " + 
		"    p.id                    profileId,  " + 
	    "	(select  if ((count(*) >0 ), 'true','false' )	"+
		"	from cart crt " +
		"	join pack_subscription psub on crt.profile_id =  psub.profile_id  " +
		"	where psub.package_id= 3 and  crt.is_completed = 1  " +
		"	and crt.profile_id = a.profile_id  " +
		"	) as incAB,  " +
		"	(select  if ((count(*) >0 ), 'true','false' )  " +
		"	from cart crt  " +
		"	join pack_subscription psub on crt.profile_id =  psub.profile_id  " +
			"where psub.package_id= 1 and  crt.is_completed = 1  " +
		"	and crt.profile_id = a.profile_id  " +
		"	) as certified, "  +
		"	(select  if ((count(*) >0 ), 'true','false' ) " +
		"	from cart crt " +
		"	join pack_subscription psub on crt.profile_id =  psub.profile_id  " +
		"	where psub.package_id= 2 and  crt.is_completed = 1 " +
		"	and crt.profile_id = a.profile_id " +
		"	) as preCertified, 	 " +
		"  (select valid_till from pack_subscription pm  where  pm.profile_id= p.id and pm.package_id=3 )  as  paymentDate, " +
		"    p.phone                     prefPhone , " + 
		"    p.pref_mobile               	     prefMobile " + 
		"    from   " + 
		"    agent a  " + 
		"   left outer join profile p on a.profile_id = p.id" +
		"    where a.lic_id is NULL and  a.resident_state is NULL" ,nativeQuery=true )
	Object[][] getAllSignUpOnlyAgents();
	
	
	
	
	
    @Query(value = 
    	    "select distinct " + 
    	    "    a.id                                ,  " + 
    	    "    a.npn                               ,  " + 
    	    "    a.naicno                            ,  " + 
    	    "    a.fein                              ,  " +     
    	    
    	    "    a.resident_state            resState,  " + 
    	    "    p.first_name                firstName,  " + 
    	    "    p.middle_name               middleName,  " + 
    	    "    p.last_name                 lastName,  " + 
    	    "    p.suffix                    suffix,  " + 
    	    "    b.business_name             businessName,  " + 
    	    "    al.lic_number               licNumber,  " + 
    	    "    al.jhi_type                 licType,  " + 
    	    "    al.authority                lineAuth,  " + 
    	    "    al.first_active_date        firstActiveDate,  " + 
    	    "    al.effective_date           effectiveDate,  " + 
    	    "    al.expiration_date          expDate,  " + 
    	    "    case   when al.is_active = 1    " + 
    	    "	    then 'Active'    " + 
    	    "	    else 'InActive'    " + 
    	    "	end as                       licStatus,    " + 
    	    "    ds.id                       domState,    " + 
    	    "    ba.addr_1                   bAddr1,  " + 
    	    "    ba.addr_2                   bAddr2,  " + 
    	    "    ba.addr_3                   bAddr3,  " + 
    	    "    ba.zip                      bZip,  " + 
    	    "    ba.country                  bCountry,  " +
//    	    "    bc.name                       bCity,  " + 
//    	    "    bs.name		             bState,  " + 
    	    "    pr.ce_compliance            isCECompliant,  " + 
    	    "    pa.addr_1                   pAddr1,  " + 
    	    "    pa.addr_2                   pAddr2,  " + 
    	    "    pa.addr_3                   pAddr3,  " + 
    	    "    pa.zip                      pZip,  " + 
    	    "    pa.country                  pCountry,  " + 
//    	    "    pc.name                       pCity,  " + 
//    	    "    ps.name               	     pState,  " + 
    	    "    c.office_phone              bPhone,  " + 
    	    "    c.email                     bEmail , " + 
    	    "	(select  if ((count(*) >0 ), 'true','false' )	"+
    		"	from cart crt " +
    		"	join pack_subscription psub on crt.profile_id =  psub.profile_id  " +
    		"	where psub.package_id= 3 and  crt.is_completed = 1  " +
    		"	and crt.profile_id = a.profile_id  and  psub.valid_till > CURDATE()" +
    		"	) as incAB,  " +
    		"	(select  if ((count(*) >0 ), 'true','false' )  " +
    		"	from cart crt  " +
    		"	join pack_subscription psub on crt.profile_id =  psub.profile_id  " +
    			"where psub.package_id= 1 and  crt.is_completed = 1  " +
    		"	and crt.profile_id = a.profile_id and  psub.valid_till > CURDATE() " +
    		"	) as certified, "  +
    		"	(select  if ((count(*) >0 ), 'true','false' ) " +
    		"	from cart crt " +
    		"	join pack_subscription psub on crt.profile_id =  psub.profile_id  " +
    		"	where psub.package_id= 2 and  crt.is_completed = 1 " +
    		"	and crt.profile_id = a.profile_id and  psub.valid_till > CURDATE()" +
    		"	) as preCertified, 	 " +
    		"    p.id                    profileId,  " +
    		 "  (select valid_till from pack_subscription pm  where  pm.profile_id= p.id and pm.package_id=3 )  as  paymentDate  " +
//    		 "    co.name               	     county,  " + 
//    		 "    pc.name               	     city,  " + 
//    		 "    ps.name               	     state  " + 
    	    "from   " + 
    	    "    agent a  " + 
    	    "left outer join profile p  " + 
    	    "    on a.profile_id = p.id  " + 
    	    "left outer join business b  " + 
    	    "    on a.business_id = b.id  " + 
    	    "left outer join agent_lic al   " + 
    	    "    on a.lic_id = al.id  " + 
    	    "left outer join state ds  " + 
    	    "    on a.domicile_state_id = ds.id  " + 
    	    "left outer join address ba  " + 
    	    "    on b.address_id = ba.id  " + 
    	    "left outer join contact c  " + 
    	    "    on ba.id = c.address_id  " + 
//    	    "left outer join city bc  " + 
//    	    "    on ba.city_id = bc.id  or pa.city_id = bc.id " + 
//    	    "left outer join state bs  " + 
//    	    "    on ba.state_id = bs.id  " + 
    	    "left outer join producer pr  " + 
    	    "    on al.producer_id = pr.id  " + 
    	    "left outer join address pa  " + 
    	    "    on pr.addr_id = pa.id  " + 
//    	    "left outer join city pc  " + 
//    	    "    on pa.city_id = pc.id or ba.city_id = pc.id " + 
//    	    "left outer join state ps  " + 
//    	    "    on pa.state_id = ps.id or  ba.state_id = ps.id    " + 
//    	    "left outer join county co  " + 
//    	    "    on pa.county_id = co.id  or  ba.county_id = co.id   " + 
    	    "where   " + 
    	    "    a.is_deleted = 0 	and " + 
//    	    "	pc.name like :city and	" + 
//    	    "	ps.name like :state and	"  + 
    	    "	ba.zip like :zip and	"  + 
			"	b.business_name like :company and " +
			"	p.first_name  like :fName and " +
			"	p.last_name like :lName  	  " 
//			"	co.name like :county  "  
    	    ,
    		nativeQuery =  true)
        Object[][] getAllAgentsByFilter(@Param("zip") String zip,@Param("company") String company,
        		@Param("fName") String fName,@Param("lName") String lName);
    

    @Query(value=
    		"	select	" + 
    		"    z.zip as zip,	" + 
    		"    sum(p.amount) 	as	 amount,	" + 
    		"    sum(ps.slots) 	as	 slots,	" + 
    		"	ps.profile_id 	as 	profileId,	" + 
    		"	p.created_at 	as 	paymentDate,	" + 
    		"    pr.jhi_type 	as 	userType,	" +
    		"    pr.first_name 	as 	agentFirstName,	" + 
    		"    pr.last_name 	as	 agentLastName	,	" + 
    		"    pr.pref_email 	as	 agentEmail ,	" + 
    		"    a.npn as npn,	" + 
    		"	ps.package_id as packageId,	" + 
       		"    pr.pref_email 	as	 sales_resp, 	" +
       		"    f.isn as isn,	" + 
       		"    pr.addr_id as addrId	" + 
    		"	from pack_subscription ps	" + 
    		"	left outer join zip_area z	" + 
    		"		on z.id = ps.zip_area_id	" + 
    		"	left outer join payment p	" + 
    		"		on p.profile_id = ps.profile_id	" + 
    		"	left outer join profile pr	" + 
    		"		on p.profile_id = pr.id	" + 
    		"	left outer join agent a	" + 
    		"		on a.profile_id = pr.id	" + 
    		"	left outer join facility f	" + 
    		"		on f.profile_id = pr.id	" + 
    		
    		"	where	" + 
    		"	p.status = 'SUCCESS'	and 	" + 
    		"	z.zip like :zip	and 	" +   		
			"	   pr.first_name like :fname  and 	" + 
			"	   pr.last_name like :lname  and   " + 
			"	    pr.jhi_type like :utype    " + 
    		"    group by z.zip , ps.package_id, ps.profile_id, a.npn,p.created_at  " +
    	    " ,f.isn " 
    		,nativeQuery=true)
	Object[][] getAgentPaymentRepot(@Param("zip") String zip ,
			@Param("fname") String fname,@Param("lname") String lname,
			@Param("utype") String utype);
    
    
    
    
    
    
    @Query(value=
    		"	   SELECT distinct ag.npn  FROM city c  "+	   
    		"	     left outer join address a	"+
    		"	    on a.city_id = c.id 		"+
    		"	     left outer join producer p		"+
    		"	    on p.addr_id = a.id 	"+
    		"	    left outer join profile pr		"+
    		"	    on pr.addr_id = a.id 		"+
    		"	    left outer join agent ag		"+
    		"	    on ag.profile_id = a.id 		"+
    		"	   where c.name like :search	"
			, nativeQuery = true)
	List<String>  findCityOfAgentSearch(@Param("search") String search);
    
    
    @Query(value=
    		"	   SELECT distinct ag.npn  FROM state c  "+	   
    		"	     left outer join address a	"+
    		"	    on a.state_id = c.id 		"+
    		"	     left outer join producer p		"+
    		"	    on p.addr_id = a.id 	"+
    		"	    left outer join profile pr		"+
    		"	    on pr.addr_id = a.id 		"+
    		"	    left outer join agent ag		"+
    		"	    on ag.profile_id = a.id 		"+
    		"	   where c.name like :search	"
			, nativeQuery = true)
	List<String>  findStateOfAgentSearch(@Param("search") String search);
    
    
    @Query(value=
    		"	   SELECT distinct ag.npn  FROM county c  "+	   
    		"	     left outer join address a	"+
    		"	    on a.county_id = c.id 		"+
    		"	     left outer join producer p		"+
    		"	    on p.addr_id = a.id 	"+
    		"	    left outer join profile pr		"+
    		"	    on pr.addr_id = a.id 		"+
    		"	    left outer join agent ag		"+
    		"	    on ag.profile_id = a.id 		"+
    		"	   where c.name like :search	"
			, nativeQuery = true)
	List<String>  findCountyOfAgentSearch(@Param("search") String search);
    
    
    
    @Query(value=
    	  
    		"	  select distinct ag.npn  FROM county c  "+
    		"	     left outer join address a	"+
    		"	    on a.county_id = c.id 		"+
    		"	left outer join producer p   "+
    		"	on p.addr_id = a.id    "+
    		"	 left outer join agent_lic lic   "+
    		"		on lic.producer_id = p.id    "+
    		"	 left outer join agent ag	   "+
    		"		on ag.lic_id = lic.id   "+
    		"	   where c.name like :search	"
  	, nativeQuery = true)
	List<String>  findMailingCountyOfAgentSearch(@Param("search") String search);
    
    
    @Query(value=
    		"	    select distinct ag.npn  FROM city c  "+	   
    		"	     left outer join address a	"+
    		"	    on a.city_id = c.id 		"+
    		"	left outer join producer p   "+
    		"	on p.addr_id = a.id    "+
    		"	 left outer join agent_lic lic   "+
    		"		on lic.producer_id = p.id    "+
    		"	 left outer join agent ag	   "+
    		"		on ag.lic_id = lic.id   "+
    		"	   where c.name like :search	"
			, nativeQuery = true)
	List<String>  findMailingCityOfAgentSearch(@Param("search") String search);
    
    
    
    @Query(value=
    		"	   select distinct ag.npn  FROM state c  "+	   
    		"	     left outer join address a	"+
    		"	    on a.state_id = c.id 		"+
    		"	left outer join producer p   "+
    		"	on p.addr_id = a.id    "+
    		"	 left outer join agent_lic lic   "+
    		"		on lic.producer_id = p.id    "+
    		"	 left outer join agent ag	   "+
    		"		on ag.lic_id = lic.id   "+
    		"	   where c.name like :search	"
			, nativeQuery = true)
	List<String>  findMailingStateOfAgentSearch(@Param("search") String search);
    
    
	@Query(value="select * from search_records where slot_zip like (:zip) and insurance_company like :cName  ",nativeQuery=true)
	Object[][] findSerachRecordsByZipFromView(@Param("zip") String zip,@Param("cName") String cName);
    
	@Query(value="select * from search_records where slot_zip=:zip  ",nativeQuery=true)
	Object[][] findSerachRecordsByZipFromView(@Param("zip") String zip);
	
	@Query(value="select * from search_facility_records where slot_zip=:zip ",nativeQuery=true)
	Object[][] findSerachFacilityRecordsByZipFromView(@Param("zip") String zip);
	
	@Query(value=
			"	(	select t.package_id 		packgeId,	" +
			"		t.zip				zip,		"+
			"		t.slot_zip 			buyZip ,		"+
			"		first_name				firstName,		"+
			"		t.last_name			lastName,		"+
			"		t.phone				phone,		"+
			"		t.slots				slots,		"+
			"		t.city				city,		"+
			"		t.state				state,		"+
			"		t.business_name				bName,		"+
			"		t.pref_company				pref_company,		"+
			"		t.comments				comments,		"+
			"		t.npn				npn	,	"+
			"		t.insurance_company				insurance_company	,	"+
			"		t.ce_compliance				ce_compliance		"+
			" 		from (		"+
			"		select p1.*, 1 'indx' from search_records p1		"+
			"		where p1.slot_zip  LIKE CONCAT('%', :zip, '%')  and p1.package_id = 1 and  p1.insurance_company like :cName	"+
			"		union		"+
			"		select p2.*, 2 'indx' from search_records p2		"+
			"		where p2.slot_zip  LIKE CONCAT('%', :zip, '%')  and p2.package_id = 2 and  p2.insurance_company like :cName	"+
			"		union		"+
			"		select p3.*, 3 'indx' from search_records p3		"+
			"		where p3.zip  LIKE (:zip) and p3.slot_zip  NOT LIKE CONCAT('%', :zip, '%') and p3.insurance_company like :cName and p3.user_id is not null and p3.npn <> '0000000'"+
			"		union		"+
			"		select p4.*, 4 'indx' from search_records p4		"+
			"		where p4.npn = '0000000'"+
			"		union		"+
			"		select p5.*, 5 'indx' from search_records p5		"+
			"		where p5.npn = '0000001'"+
			"		union		"+
			"		select p6.*, 6 'indx' from search_records p6		"+
			"		where p6.zip  LIKE (:zip) and p6.insurance_company like :cName and p6.user_id is null and p6.npn <> '0000000'"+
			"		) as t		"+ 
			"	where  t.jhi_type = :type order by t.indx asc "+ 
			/*
			 * "	group by profile_id ,package_id,slot_zip,zip,first_name,last_name,phone,slots,city, "
			 * +
			 * "                    state,business_name,pref_company,comments,npn,insurance_company,ce_compliance	"
			 * +
			 */
			"                    );"	
			//")"
			,nativeQuery=true)
	Object[][] findSerachRecordsFromView(@Param("zip") String zip,@Param("type") String type,@Param("cName") String cName);
	
	
	@Query(value=
			"		select t.package_id 		packgeId,	" +
			"		t.zip				zip,		"+
			"		t.slot_zip 			buyZip ,		"+
			"		first_name				firstName,		"+
			"		t.last_name			lastName,		"+
			"		t.phone				phone,		"+
			"		t.slots				slots,		"+
			"		t.city				city,		"+
			"		t.state				state,		"+
			"		t.business_name				bName,		"+
			"		t.pref_company				pref_company,		"+
			"		t.comments				comments,		"+
			"		t.npn				npn	,	"+
			"		t.insurance_company				insurance_company	,	"+
			"		t.ce_compliance				ce_compliance		"+
			" 		from (		"+
			"		select p1.*, 1 'indx' from search_agent_records p1		"+
			"		where p1.slot_zip LIKE CONCAT('%', :zip, '%') and p1.package_id = 1	"+
			"		union		"+
			"		select p2.*, 2 'indx' from search_agent_records p2		"+
			"		where p2.slot_zip LIKE CONCAT('%', :zip, '%') and p2.package_id = 2 	"+
			"		union		"+
			"		select p3.*, 3 'indx' from search_agent_records p3		"+
			"		where p3.zip LIKE CONCAT('%', :zip, '%') and p3.package_id is not null and p3.slot_zip NOT LIKE CONCAT('%', :zip, '%') and p3.npn <> '0000000'  "+
			"		union		"+
			"		select p4.*, 4 'indx' from search_agent_records p4		"+
			"		where p4.zip LIKE CONCAT('%', :zip, '%') and p4.package_id is null and p4.user_id is not null and p4.npn <> '0000000' "+
			"		union		"+
			"		select p5.*, 5 'indx' from search_agent_records p5		"+
			"		where p5.npn = '0000000'"+
			"		union		"+
			"		select p6.*, 6 'indx' from search_agent_records p6		"+
			"		where p6.npn = '0000001'"+
			"		union		"+
			"		select p7.*, 7 'indx' from search_agent_records p7		"+
			"		where p7.zip LIKE CONCAT('%', :zip, '%') and  p7.package_id is null and p7.user_id is null and p7.npn <> '0000000'"+
			"		) as t		"+
			"	where  t.jhi_type = :type order by t.indx asc; "
			,nativeQuery=true)
	Object[][] findSerachRecordsFromView(@Param("zip") String zip,@Param("type") String type);
	
	
	
	@Query(value="select count(*) from search_records where slot_zip LIKE CONCAT('%', :zip, '%') and  package_id =1 and insurance_company like :cName ",nativeQuery=true)
	int findSerachCountForPage1FromViewCompany(@Param("cName") String cName,@Param("zip") String zip);
    
	@Query(value="select count(*) from search_records where slot_zip LIKE CONCAT('%', :zip, '%') and package_id =2  and insurance_company like :cName ",nativeQuery=true)
	int findSerachCountForPage2FromViewCompany(@Param("cName") String cName,@Param("zip") String zip);
	
	@Query(value="select count(*) from search_records where slot_zip=:zip and package_id =1 ",nativeQuery=true)
	int findSerachCountForPage1FromView(@Param("zip") String zip);
    
	@Query(value="select count(*) from search_records where slot_zip=:zip and package_id =2 ",nativeQuery=true)
	int findSerachCountForPage2FromView(@Param("zip") String zip);
	
	@Query(value=
			"		select t.package_id 		packgeId,	" +
			"		t.zip				zip,		"+
			"		t.slot_zip 			buyZip ,		"+
			"		first_name				firstName,		"+
			"		t.last_name			lastName,		"+
			"		t.phone				phone,		"+
			"		t.slots				slots,		"+
			"		t.city				city,		"+
			"		t.state				state,		"+
			"		t.name			bName	,	"+
			"		t.pref_company				pref_company,		"+
			"		t.comments				comments,		"+
			"		t.isn				npn	,	"+
			"		t.addr_1				addr_1,		"+
			"		t.addr_2				addr_2,		"+
			"		t.addr_3				addr_3		"+
			" 		from (		"+
			"		select p1.*, 1 'indx' from search_facility_records p1		"+
			"		where p1.slot_zip LIKE CONCAT('%', :zip, '%') and p1.package_id = 1	"+
			"		union		"+
			"		select p2.*, 2 'indx' from search_facility_records p2		"+
			"		where p2.slot_zip LIKE CONCAT('%', :zip, '%') and p2.package_id = 2 	"+
			"		union		"+
			"		select p3.*, 3 'indx' from search_facility_records p3		"+
			"		where p3.zip LIKE CONCAT('%', :zip, '%') and p3.package_id is not NULL and p3.slot_zip NOT LIKE CONCAT('%', :zip, '%')  "+
			"		union		"+
			"		select p4.*, 4 'indx' from search_facility_records p4		"+
			"		where p4.zip LIKE CONCAT('%', :zip, '%')and p4.package_id is NULL "+
			"		) as t		"+
			"	where  t.jhi_type = :type	"+
			"		order by t.indx asc;		"
			,nativeQuery=true)
	Object[][] findSerachFacilityRecordsFromView(@Param("zip") String zip,@Param("type") String type);
	
	
	
	@Query(value=
			"		( select t.package_id 		packgeId,	" + 
			"					t.zip				zip,		" + 
			"					t.slot_zip 			buyZip ,		" + 
			"					first_name				firstName,		" + 
			"					t.last_name			lastName,		" + 
			"					t.phone				phone,		" + 
			"					t.slots				slots,		" + 
			"					t.city				city,		" + 
			"					t.state				state,		" + 
			"					t.business_name				bName,		" + 
			"					t.pref_company				pref_company,		" + 
			"					t.comments				comments,		" + 
			"					t.npn				npn	,	" + 
			"					t.insurance_company				insurance_company	,	" + 
			"		            t.ce_compliance				ce_compliance		"+
			"			 		from search_records t		" + 
			"				    where (t.jhi_type =:type and t.first_name =:firstName and  t.last_name =:lastName )"+
			"	                group by profile_id ,package_id,slot_zip,zip,first_name,last_name,phone,slots,city, " + 
			"                    state,business_name,pref_company,comments,npn,insurance_company,ce_compliance	" + 
			"                 );		"
			,nativeQuery=true)
	Object[][] findSerachAgentRecordsFromView(@Param("type") String type
			,@Param("firstName") String firstName,@Param("lastName") String lastName);
	
	@Query(value="select count(*) from search_facility_records where slot_zip=:zip and package_id =1 ",nativeQuery=true)
	int findSerachFacilityCountForPage1FromView(@Param("zip") String zip);
    
	@Query(value="select count(*) from search_facility_records where slot_zip=:zip and package_id =2  ",nativeQuery=true)
	int findSerachFacilityCountForPage2FromView(@Param("zip") String zip);
	

	@Query(value="select * from profile where id=:pId ",nativeQuery=true)
	Profile getProfileId(@Param("pId") Long pId); 
 
	@Query(value="select * from agent where extension=:ext ",nativeQuery=true)
	Agent getAgentDataByExtension(@Param("ext") String ext);
	
	@Query(value="select distinct extension from agent",nativeQuery=true)
	List<String> getAgentExtensions();
	
	@Query(value="select distinct extension from agent where npn like :npn",nativeQuery=true)
	String getAgentExtensionFromNPN(@Param("npn") String npn);
	
	@Query(value="select distinct extension from facility where isn like :isn",nativeQuery=true)
	String getFacilityExtensionFromISN(@Param("isn") String isn);

	//	@Query(value="select distinct p.insurance_company from search_records p " + 
	//	"	where p.insurance_company IS NOT NULL and p.zip =:zip "
	//	,nativeQuery=true)
	
	//Query modified by sanjay
	@Query(value="SELECT DISTINCT " + 
		"    p.insurance_company " + 
		"FROM " + 
		"    search_records p " + 
		"WHERE " + 
		"    p.insurance_company LIKE CONCAT('%', :text, '%') " + 
		"        AND p.zip LIKE CONCAT('%', :zip, '%')"
	,nativeQuery=true)
	List<String> getInsuranceCompanyList(@Param("zip") String zip,@Param("text") String text);
	
	
	@Query(value="SELECT DISTINCT " + 
			"    p.carrier " + 
			"FROM " + 
			"    available_carriers p " + 
			"WHERE " + 
			"    p.carrier LIKE CONCAT('%', :text, '%') " 
		,nativeQuery=true)
		List<String> getCarriersCompanyList(@Param("text") String text);
	
	//@Query(value="select distinct CONCAT(p.first_name,' ', p.last_name)  from search_records p " + 
	//	"	where p.zip =:zip  "
	//	,nativeQuery=true)
	
	//Query modified by sanjay	
	@Query(value="SELECT DISTINCT " + 
		"    CONCAT(p.first_name, ' ', p.last_name) " + 
		"FROM " + 
		"    agent a " + 
		"        LEFT OUTER JOIN " + 
		"    profile p ON a.profile_id = p.id " + 
		"        LEFT OUTER JOIN " + 
		"    business b ON a.business_id = b.id " + 
		"        LEFT OUTER JOIN " + 
		"    address ba ON b.address_id = ba.id " + 
		"WHERE " + 
		"    a.is_deleted = 0 AND ba.zip = :zip " + 
		"        AND (p.first_name LIKE CONCAT('%', :text, '%') " + 
		"        OR p.last_name LIKE CONCAT('%', :text, '%') " + 
		"        OR CONCAT(p.last_name, p.first_name) LIKE CONCAT('%', :text, '%') " + 
		"        OR CONCAT(p.first_name, p.last_name) LIKE CONCAT('%', :text, '%'))"
		,nativeQuery=true)
	List<String> getAgentsOfCompany(@Param("zip") String zip,@Param("text") String text);
	
	
	@Query(value="SELECT DISTINCT " + 
			"    CONCAT(p.first_name, ' ', p.last_name) " + 
			"FROM " + 
			"    agent a " + 
			"        LEFT OUTER JOIN " + 
			"    profile p ON a.profile_id = p.id " + 
			"        LEFT OUTER JOIN " + 
			"    business b ON a.business_id = b.id " + 
			"        LEFT OUTER JOIN " + 
			"    address ba ON b.address_id = ba.id " + 
			"WHERE " + 
			"    a.is_deleted = 0" + 
			"        AND (p.first_name LIKE CONCAT('%', :text, '%') " + 
			"        OR p.last_name LIKE CONCAT('%', :text, '%') " + 
			"        OR CONCAT(p.last_name, p.first_name) LIKE CONCAT('%', :text, '%') " + 
			"        OR CONCAT(p.first_name, p.last_name) LIKE CONCAT('%', :text, '%'))"
			,nativeQuery=true)
	List<String> getAgentsOfCompanyByName(@Param("text") String text);
	
	@Query(value="SELECT DISTINCT " + 
			"    CONCAT(p.first_name, ' ', p.last_name) as name, a.id as id " + 
			"FROM " + 
			"    agent a " + 
			"        LEFT OUTER JOIN " + 
			"    profile p ON a.profile_id = p.id " + 
			"        LEFT OUTER JOIN " + 
			"    business b ON a.business_id = b.id " + 
			"        LEFT OUTER JOIN " + 
			"    address ba ON b.address_id = ba.id " + 
			"WHERE " + 
			"    p.last_name LIKE CONCAT('%', :text, '%')"
			,nativeQuery=true)
		Object[][] getAgentsOfCompanyByLastName(@Param("text") String text);
		
		@Query(value="SELECT DISTINCT " + 
				"    p.insurance_company AS name " + 
				"FROM " + 
				"    search_records p " + 
				"WHERE " + 
				"    p.insurance_company LIKE CONCAT('%', :name, '%')"
		,nativeQuery=true)
		Object[][] getInsuranceCompanyListByName(@Param("name") String name);

		
		
		
		
		@Query(value="		select "+
		"		   pc.package_id AS package_id,		"+
		"		   addr.zip AS zip,		"+
		"		   z.zip AS buyZip,		"+
		"		  p.first_name AS firstName,		"+
		"		  p.last_name AS lastName,		"+
		"		  p.phone AS phone,		"+
		"		   pc.slots AS slots,		"+
		"		  s.name AS state,		"+
		"		   c.name AS city,		"+
		"		  b.business_name AS bName,		"+
		"		  p.pref_company AS pref_company,		"+
		"		  p.comments AS comments,		"+
		"		  a.npn AS npn,		"+
		"		  p.insurance_company AS insurance_company	,	"+
		"		  pd.ce_compliance				ce_compliance		"+
		"		from agent a 		"+
		"		 LEFT JOIN iaa.profile p ON ((p.id = a.profile_id))		"+
		"		   LEFT JOIN iaa.business b ON ((b.id = a.business_id))		"+
		"		   LEFT JOIN iaa.address addr ON ((addr.id = b.address_id))		"+
		"		   LEFT JOIN iaa.state s ON ((s.id = addr.state_id))		"+
		"		   LEFT JOIN iaa.city c ON ((c.id = addr.city_id))		"+
		"		  LEFT JOIN iaa.agent_lic lic ON ((lic.id = a.lic_id))		"+
		"		  LEFT JOIN iaa.producer pd ON ((pd.id = lic.producer_id))		"+
		"		   LEFT JOIN iaa.pack_subscription pc ON ((pc.profile_id = p.id))		"+
		"		  LEFT JOIN iaa.zip_area z ON ((z.id = pc.zip_area_id))		"
		,nativeQuery=true)
		Object[][] findSerachInsuranceRecords();


	
		
		@Query(value="select id from jhi_user u where 		" + 
				"	u.id = (select user_id from profile p where 		" + 
				"		p.jhi_type ='AGENT' and p.id = (		" + 
				"		select profile_id from agent a where a.npn = :npn)		" + 
				"        )		"
				,nativeQuery=true)
		Long checkAgentSignUp(@Param("npn") String npn);

		
		@Query(value="SELECT count(*) FROM profile_carriers where profile_id=:profileId and is_primary=0"
				,nativeQuery=true)
		int findCountByProfileId(@Param("profileId") Long long1);
		
		
	//get insurance company name by npn -04 Sept 19
	  @Query(value="SELECT insurance_company from iaa.profile where id=(select profile_id from iaa.agent where npn=:npn)" ,nativeQuery=true) 
	  String getInsuranceCompanyByNpn(@Param("npn") String npn);
	 
		
	/*
	 * @Query(
	 * value="SELECT * from all_agent_list where (LOWER(firstName) like CONCAT('%', :searchText, '%')) OR (LOWER(lastName) like CONCAT('%', :searchText, '%')) OR (npn like CONCAT('%', :searchText, '%')) order by :sortColumn :sortType"
	 * ,nativeQuery=true) Object[][] searchSortAgents(@Param("searchText") String
	 * searchText,@Param("sortColumn") String sortColumn,@Param("sortType") String
	 * sortType);
	 */
	/*
	 * @Query(value="SELECT * from all_agent_list order by :sortColumn :sortType"
	 * ,nativeQuery=true) Object[][] sortAgents(@Param("sortColumn") String
	 * sortColumn,@Param("sortType") String sortType);
	 */
	
}
