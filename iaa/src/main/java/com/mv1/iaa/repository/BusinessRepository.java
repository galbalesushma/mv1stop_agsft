package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.Business;


/**
 * Spring Data  repository for the Business entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BusinessRepository extends JpaRepository<Business, Long> {

	  @Query(value = 
			  "       select * from business b	"+
				"     where b.address_id =:addrId	"
					, nativeQuery = true)
	  Business findBusinessByAddrId(@Param("addrId") Long addrId);
}
