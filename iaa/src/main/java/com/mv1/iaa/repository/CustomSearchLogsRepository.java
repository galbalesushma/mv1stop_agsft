package com.mv1.iaa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomSearchLogsRepository extends SearchLogsRepository{

	@Query(value=
			"select "
			+ "	zip ,		"
			+ "	full_name	as fullName,		"
			+ "	email,		"
			+ "	phone,		"
			+ "	search_type as searchType, 	"
			+ "	searched_date as searchedDate, 	"
			+ "	vehicle_type as vehicleType, 	"
			+ "	vehicle_year as vehicleYear, 	"
			+ "	county	,"
			+ "	company	"
			+ "	from search_logs where	"
			+ "	full_name like :fullName and	"
			+ "	phone like :phone and	"
			+ "	zip like :zip	"
			,nativeQuery=true)
	Object[][] getAdminLogReport(@Param("zip") String zip,@Param("phone") String phone,
			 @Param("fullName") String fullName);

	
	@Query(value="select distinct full_name from search_logs where full_name like :search ",nativeQuery=true)
	List<String> findSearchUsersName(@Param("search")String search);

	@Query(value="select distinct email from search_logs where email like :search ",nativeQuery=true)
	List<String> findSearchEmail(@Param("search")String search);

	@Query(value="select distinct phone from search_logs where phone like :search ",nativeQuery=true)
	List<String> findSearchPhone(@Param("search")String search);

	@Query(value="select distinct zip from search_logs where zip like :search ",nativeQuery=true)
	List<String> findSearchZip(@Param("search")String search);

	@Query(value="select distinct county from search_logs where county like :search ",nativeQuery=true)
	List<String> findSearchCounty(@Param("search")String likeSearch);

	@Query(value="select distinct company from search_logs where company like :search ",nativeQuery=true)
	List<String> findSearchCompany(@Param("search")String likeSearch);

	@Query(value="select distinct vehicle_year from search_logs where vehicle_year like :search ",nativeQuery=true)
	List<String> findSearchVehicleYear(@Param("search")String likeSearch);


}
