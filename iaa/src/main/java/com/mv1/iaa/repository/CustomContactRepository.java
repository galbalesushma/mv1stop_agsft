package com.mv1.iaa.repository;

import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.Contact;

/**
 * Spring Data repository for the Contact entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomContactRepository extends ContactRepository {

	Contact findOneByAddressId(Long addressId);

	Contact findByPhone(String phone);
}
