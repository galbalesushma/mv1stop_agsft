package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mv1.iaa.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
