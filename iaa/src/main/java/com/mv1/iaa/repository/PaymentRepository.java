package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.Payment;





/**
 * Spring Data  repository for the Payment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
	@Query(value = 
	   		 "  SELECT count(*) FROM iaa.payment pay join iaa.profile_carriers pr on(pay.profile_id=pr.profile_id) where status like 'SUCCESS' and pay.profile_id = :pId and pay.updated_at>=pr.updated_date;",
			  nativeQuery =  true)
		Long findPaymentStatusByprofile(@Param("pId") Long pId);
}
