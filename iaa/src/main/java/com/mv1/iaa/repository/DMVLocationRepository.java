package com.mv1.iaa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.DMVLocation;


/**
 * Spring Data  repository for the DMVLocation entity.
 */
@Repository
public interface DMVLocationRepository extends JpaRepository<DMVLocation, Long> {

	@Query("FROM DMVLocation where city LIKE CONCAT('%',:city, '%')")
	 List<DMVLocation> findByCityContaining(@Param("city") String city);
}
