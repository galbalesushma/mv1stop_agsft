package com.mv1.iaa.repository;

import com.mv1.iaa.domain.SearchInsuranceLogs;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SearchInsuranceLogs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SearchInsuranceLogsRepository extends JpaRepository<SearchInsuranceLogs, Long> {

}
