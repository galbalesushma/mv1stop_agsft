package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.ExtensionCounty;

@Repository
public interface ExtensionCountyRepository  extends JpaRepository<ExtensionCounty, Long> {


}
