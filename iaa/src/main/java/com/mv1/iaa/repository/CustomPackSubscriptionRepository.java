package com.mv1.iaa.repository;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.PackSubscription;


/**
 * Spring Data  repository for the PackSubscription entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomPackSubscriptionRepository extends PackSubscriptionRepository {

    List<PackSubscription> findAllByProfileId(Long profileId);
    List<PackSubscription> findAllByProfileIdAndIsActive(
            @Param("profileId") Long profileId, 
            @Param("isActive") Boolean isActive);
}
