package com.mv1.iaa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.Facility;

@Repository
public interface CustomFacilityRepository extends FacilityRepository {

    @Query(value = 
        "select  " + 
        "    c.id,  " +
        "    c.email " + 
        "from   " + 
        "    facility f  " + 
        "left outer join business b  " + 
        "    on f.business_id = b.id  " + 
        "left outer join address ba  " + 
        "    on b.address_id = ba.id  " + 
        "left outer join contact c  " + 
        "    on ba.id = c.address_id  " + 
        "where   " + 
        "    f.is_deleted = 0  "+
        "    and f.isn = :isn  " ,
        nativeQuery =  true)
    Object[][] findEmailByIsn(@Param("isn") String isn);
    
    Facility findOneByProfileId(Long profileId);

	@Query(value = "" +
		"select    " + 
		" *            " + 
		"from          " + 
		"   facility f 	   " + 
		"left outer join business b  " + 
        "    on f.business_id = b.id  " + 
        "left outer join address a    " + 
		"    on a.id = b.address_id    " + 
		"left outer join contact c          " + 
		"    ON a.id = c.address_id    " + 
		"where     " + 
		"    c.phone =:phone    " + 
		"    and a.zip =:zip    " + 
		"    and f.name =:name  "
	    , nativeQuery = true)
    Facility findByZipAndPhone(@Param(value = "zip") String zip, @Param(value = "phone") String phone,
	    @Param(value = "name") String name);

    @Query(value = 
        "select                             " + 
        "    f.id,                          " + 
        "    f.profile_id    profileId,     " + 
        "    f.name,                        " + 
        "    f.weekday_hours weekDayHours,  " + 
        "    f.saturday_hours saturdayHours," + 
        "    f.sunday_hours  sundayHours,   " + 
        "    f.isn           isn,       " + 
        "    a.zip           zipCode,       " + 
        "    a.addr_1        address,       " + 
        "    p.first_name           firstName ,      " + 
        "    p.last_name           lastName ,      " + 
        "    p.pref_mobile           mobile ,      " + 
        "    p.pref_email           email ,      " + 
        "    p.phone           phone,       " + 
        "    ct.name         city         " + 
        "from                               " + 
        "    facility f                     " + 
        "left outer join business b         " + 
        "    on f.business_id = b.id        " + 
        "left outer join address a          " + 
        "    on b.address_id = a.id         " + 
        "left outer join contact c          " + 
        "    on a.id = c.address_id         " + 
        "left outer join state s            " + 
        "    on a.state_id = s.id           " + 
        "left outer join city ct            " + 
        "    on a.city_id = ct.id           " + 
        "left outer join profile p            " + 
        "    on f.profile_id = p.id           " + 
        "where                              " + 
        "    f.is_deleted = 0               "  
        , nativeQuery = true)
    Object[][] getAllFacilities();
    
    @Query(value = 
        "select                             " + 
        "    f.id,                          " + 
        "    f.profile_id    profileId,     " + 
        "    f.name,                        " + 
        "    f.weekday_hours weekDayHours,  " + 
        "    f.saturday_hours saturdayHours," + 
        "    f.sunday_hours  sundayHours,   " + 
        "    a.zip           zipCode,       " + 
        "    a.addr_1        address,       " + 
        "    ct.name         city,          " + 
        "    c.office_phone         bPhone,         " + 
   		"    c.phone        mobile,         " + 
        "    f.isn           isn,       " + 
        "    st.name           state,       " + 
        "    co.name           county,       " + 
        "    c.email           bEmail,       " + 
        "    f.mech_duty           mech_duty,       " + 
        "    f.insp_type           insp_type,       " + 
        "    p.first_name           firstName,       " + 
        "    p.last_name           lastName,       " + 
        "    p.pref_mobile           prefMobile,       " + 
        "    p.phone           prefPhone,       " + 
        "    p.pref_email           prefEmail,       " + 
        "    p.is_speaks_spanish           speaks_spanish ,      " + 
        "    p.pref_company                     prefCompany, " + 
	    "    p.comments          comments " + 
        "from                               " + 
        "    facility f                     " + 
        "left outer join business b         " + 
        "    on f.business_id = b.id        " + 
        "left outer join address a          " + 
        "    on b.address_id = a.id         " + 
        "left outer join contact c          " + 
        "    on a.id = c.address_id         " + 
        "left outer join state s            " + 
        "    on a.state_id = s.id           " + 
        "left outer join city ct            " + 
        "    on a.city_id = ct.id           " + 
        "left outer join county co            " + 
        "    on a.county_id = co.id           " + 
        "left outer join state st            " + 
        "    on a.state_id = st.id           " + 
        "left outer join profile p            " + 
        "    on f.profile_id = p.id           " + 
        "where                              " + 
        "    f.is_deleted = 0               " + 
        "    and f.id = :id                 " 
        , nativeQuery = true)
    Object[][] getFacility(@Param(value = "id") Long id);
    
    @Modifying(clearAutomatically = true)
    @Query(value = 
         "update                        " +
         "  facility f                  " +
         "left outer join profile pr    " +
         "  on pr.id = f.profile_id     " +
         "set                           " +
         "  f.is_deleted = true,        " +
         "  pr.is_deleted = true        " +
         "where                         " +
         "  f.id in ?1                  " 
            , nativeQuery = true) 
    Integer deleteFacilitiesByIds(/*@Param(value = "ids")*/ List<Long> ids);
    
    
    Facility findOneByIsn(String isn);
    
    
	@Query(value="select isn from facility where isn like :search ",nativeQuery=true)
	List<String> findAllIsnBySearch(@Param("search")String search);

	
	@Query(value=
	 " select                             " + 
	 "    f.id           id,       " + 
     "    f.isn           isn,       " + 
     "    p.first_name           firstName ,      " + 
     "    p.last_name           lastName ,      " + 
     "    p.pref_mobile           mobile ,      " + 
     "    p.pref_email           email ,      " + 
     "    p.phone           phone ,      " + 
     "     (select  if ((count(*) >0 ), 'true','false' )	          " + 
     "    	from cart crt            " + 
     "    	join pack_subscription psub on crt.profile_id =  psub.profile_id             " +   
     "    	where psub.package_id= 3 and  crt.is_completed = 1               " + 
     "    	and crt.profile_id = f.profile_id               " + 
     "    	) as incAB,               " + 
     "    	(select  if ((count(*) >0 ), 'true','false' )               " + 
     "    	from cart crt               " + 
     "    	join pack_subscription psub on crt.profile_id =  psub.profile_id              " +  
     "    	where psub.package_id= 1 and  crt.is_completed = 1               " + 
     "    	and crt.profile_id = f.profile_id               " + 
     "    	) as certified,               " + 
     "    	(select  if ((count(*) >0 ), 'true','false' )              " + 
     "    	from cart crt              " + 
     "    	join pack_subscription psub on crt.profile_id =  psub.profile_id              " +  
     "    	where psub.package_id= 2 and  crt.is_completed = 1              " + 
     "    	and crt.profile_id = f.profile_id              " + 
     "    	) as preCertified, 	              " + 
     "     (select valid_till from pack_subscription pm  where  pm.profile_id= p.id and pm.package_id=3 )  as  paymentDate            " +  
     " from                               " + 
     "     profile p                   " + 
     "    left outer join facility f                     " + 
     "    on f.profile_id = p.id                     " + 
     " where                              " + 
     "      p.jhi_type like :type and f.name is NULL	" 
     ,nativeQuery=true)
	Object[][] getAllSignUpOnlyFacility(@Param("type") String type);


	
	 @Query(value = 
        "select                             " + 
        "    f.id,                          " + 
        "    f.profile_id    profileId,     " + 
        "    f.name,                        " + 
        "    f.weekday_hours weekDayHours,  " + 
        "    f.saturday_hours saturdayHours," + 
        "    f.sunday_hours  sundayHours,   " + 
        "    f.isn           isn,       " + 
        "    a.zip           zipCode,       " + 
        "    a.addr_1        address,       " + 
        "    p.first_name           firstName ,      " + 
        "    p.last_name           lastName ,      " + 
        "    p.pref_mobile           mobile ,      " + 
        "    p.pref_email           email ,      " + 
        "    c.phone           phone,       " + 
        "    c.office_phone           bPhone,       " + 
        "    ct.name         city,          " + 
        "     (select  if ((count(*) >0 ), 'true','false' )	          " + 
        "    	from cart crt            " + 
        "    	join pack_subscription psub on crt.profile_id =  psub.profile_id             " +   
        "    	where psub.package_id= 3 and  crt.is_completed = 1               " + 
        "    	and crt.profile_id = f.profile_id               " + 
        "    	) as incAB,               " + 
        "    	(select  if ((count(*) >0 ), 'true','false' )               " + 
        "    	from cart crt               " + 
        "    	join pack_subscription psub on crt.profile_id =  psub.profile_id              " +  
        "    	where psub.package_id= 1 and  crt.is_completed = 1               " + 
        "    	and crt.profile_id = f.profile_id               " + 
        "    	) as certified,               " + 
        "    	(select  if ((count(*) >0 ), 'true','false' )              " + 
        "    	from cart crt              " + 
        "    	join pack_subscription psub on crt.profile_id =  psub.profile_id              " +  
        "    	where psub.package_id= 2 and  crt.is_completed = 1              " + 
        "    	and crt.profile_id = f.profile_id              " + 
        "    	) as preCertified, 	              " + 
        "     (select valid_till from pack_subscription pm  where  pm.profile_id= p.id and pm.package_id=3 )  as  paymentDate  ,          " +
        "    co.name         county,          " + 
        "    s.name         state,          " + 
        "    f.mech_duty         mech_duty,          " + 
        "    f.insp_type         insp_type ,         " + 
        "    b.business_name         businessName,          " + 
        "    c.email         bEmail          " + 
        "from                               " + 
        "    facility f                     " + 
        "left outer join business b         " + 
        "    on f.business_id = b.id        " + 
        "left outer join address a          " + 
        "    on b.address_id = a.id         " + 
        "left outer join contact c          " + 
        "    on a.id = c.address_id         " + 
        "left outer join state s            " + 
        "    on a.state_id = s.id           " + 
        "left outer join city ct            " + 
        "    on a.city_id = ct.id           " + 
        "left outer join county co            " + 
        "    on a.county_id = co.id           " + 
        "left outer join profile p            " + 
        "    on f.profile_id = p.id           " + 
        "where                              " + 
        "    f.is_deleted = 0	"
       , nativeQuery = true)
	Object[][] getAllFacilitiesWithFilter();

	 
	 @Query(value = 
			 "      select distinct name from city c       " + 
			 "      left outer join address a         " + 
			 "      on a.city_id = c.id           " + 
			 "     left outer join profile p          " + 
			 "     on p.addr_id = a.id           " + 
			 "     where p.jhi_type like :type and c.name like :likeSearch      " 
		        		, nativeQuery = true)
	List<String> findFacilityCity(@Param("type") String type,@Param("likeSearch") String likeSearch);

	 
	 @Query(value = 
			 "      select distinct zip from address a      " + 
			 "     left outer join profile p          " + 
			 "     on p.addr_id = a.id           " + 
			 "     where p.jhi_type like :type and a.zip like :likeSearch      " 
		        		, nativeQuery = true)
	List<String> findFacilityZip(@Param("type") String type,@Param("likeSearch") String likeSearch);

	 
	 @Query(value = 
			 "      select distinct first_name from profile p      " + 
			 "     where p.jhi_type like :type and p.first_name like :likeSearch      " 
				, nativeQuery = true)
	List<String> findFacilityFname(@Param("type") String type,@Param("likeSearch") String likeSearch);

	 
	 @Query(value = 
			 "      select distinct last_name from profile p      " + 
			 "     where p.jhi_type like :type and p.last_name like :likeSearch      " 
				, nativeQuery = true)
	List<String> findFacilityLname(@Param("type") String type,@Param("likeSearch") String likeSearch);

	 @Query(value = 
			 "      select isn from facility f      " + 
			 "     where f.isn like :likeSearch      " 
				, nativeQuery = true)
	List<String> findFacilityIsn(@Param("likeSearch") String likeSearch);

	 
	/* @Query(value = 
			 "      select business_name from business b      " + 
			 "      left outer join facility f      " +
			 "     on f.business_id = b.id      " +
			 "     where b.business_name like :likeSearch      " 
				, nativeQuery = true)
	List<String> findFacilityBusiness(@Param("likeSearch") String likeSearch);*/
	 
	 
	 @Query(value = 
			 "      select name from facility f      " + 
			 "     where f.name like :likeSearch      " 
				, nativeQuery = true)
	List<String> findFacilityBusiness(@Param("likeSearch") String likeSearch);
	 
	 @Query(value = 
			 "       select  "+
			 "	p.package_id        as packageId	,"+
			 "	z.zip        as zip,"+
			 "	pr.first_name         as firstName	,"+
			 "	pr.middle_name        as middleName	,"+
			 "	pr.last_name             as lastName	, " +
			 "	pr.phone         as phone,	"+
			 "	p.slots         as slots	"+
			 "        FROM pack_subscription p     " +
			 "      left outer join zip_area z     " +
			 "      	on z.id = p.zip_area_id     " +
			 "       left outer join profile pr     " +
			 "      	on pr.id = p.profile_id     " +
			 "      left join agent a    " +
			 "      	on a.profile_id = pr.id     " +
			 "      left join agent_lic lic     " +
			 "      	on lic.id = a.lic_id     " +
			 "      left join producer pd     " +
			"      	on pd.id = lic.producer_id     " +
			 "      where pr.jhi_type like :type and z.zip like :zip and p.package_id like :packageId     " +
			 "      	and p.valid_till > now() and p.is_active =1 and "+
			 " 	lic.is_active = 1 and pd.ce_compliance  = 1     " 
				, nativeQuery = true)
	Object[][] findAgentsSlots(@Param("type") String type,@Param("zip") String zip, @Param("packageId") String packageId);

	 @Query(value = 
			 "       select   " + 
			 "    	 ad.zip 		zip,     " +
			 "    	 pr.first_name 		firstName,     " +
			 "    	 		pr.middle_name	middleName	,     " +
			 "    	 		pr.last_name		lastName,       " +
			 "    	 		pr.phone      " +
			 "       from  profile pr    " +
			 "       	left outer join agent a    " +
			 "      		on a.profile_id = pr.id    " +
			 "      	left outer join business b    " +
			 "      	on b.id = a.business_id     " +
			 "      left outer join address ad    " +
			 "      	on ad.id = b.id     " +
			 "      where ad.zip like :zip   " 
				, nativeQuery = true)
	Object[][] findZipAgent(@Param("zip") String zip);
	 
	 
	 @Query(value = 
	 "       select   " + 
			 "    	 ad.zip 		zip,     " +
			 "    	 pr.first_name 		firstName,     " +
			 "    	 		pr.middle_name	middleName	,     " +
			 "    	 		pr.last_name		lastName,       " +
			 "    	 		pr.phone      " +
			 "       from  profile pr    " +
			 "       	left outer join facility f    " +
			 "      		on f.profile_id = pr.id    " +
			 "      	left outer join business b    " +
			 "      	on b.id = a.business_id     " +
			 "      left outer join address ad    " +
			 "      	on ad.id = b.id     " +
			 "      where ad.zip like  :zip   " 
				, nativeQuery = true)
	Object[][] findZipFacility(@Param("zip") String zip);
	 
	 
	 
	 
	 
	 @Query(value = 
			 "       select  "+
			 "	z.zip        as zip"+
			 "        FROM pack_subscription p     " +
			 "      left outer join zip_area z     " +
			 "      	on z.id = p.zip_area_id     " +
			 "       left outer join profile pr     " +
			 "      	on pr.id = p.profile_id     " +
			 "      where pr.jhi_type like :type  and p.package_id like :packageId     " +
			 "      	and p.valid_till > now() and p.is_active =1  and z.zip != :zip    " 
				, nativeQuery = true)
	List<String> findZipAgentsSlots(@Param("type") String type, @Param("packageId") String packageId,
			@Param("zip") String zip);
	 
	 
	 
	 
	 @Query(value = 
			 "       select distinct    " +
			 "      	addr.zip        as zip,    " +
			 "      	pr.first_name         as firstName	,    " +
			 "       	pr.middle_name        as middleName	,    " +
			 "       	pr.last_name             as lastName	,     " +
			 "      	pr.phone         as phone    " +
			 "       from profile pr     " +
			 "      left join agent a    " +
			 "      	on a.profile_id = pr.id     " +
			 "      left join agent_lic lic     " +
			 "      	on lic.id = a.lic_id     " +
			 "      left join producer p     " +
			 "      	on p.id = lic.producer_id     " +
			 "      left join business b     " +
			 "      	on a.business_id = b.id     " +
			 "      left join address addr     " +
			 "      	on b.address_id = addr.id     " +
			 "      where lic.is_active = 1 and p.ce_compliance  = 1     " +
			 "      and a.is_active =1  and addr.zip like :zip and pr.jhi_type like :type  " 
						, nativeQuery = true)
			Object[][] findAllAgents(@Param("zip") String zip,@Param("type") String type);
	 
	 
	  @Query(value = 
		        "select  " + 
		        "    f.profile_id  " +
		        "from   " + 
		        "    facility f  " + 
		        "where   " + 
		        "    f.is_deleted = 0  "+
		        "    and f.isn = :isn  " ,
		        nativeQuery =  true)
		   String findProfileIdByIsn(@Param("isn") String isn);


	  @Query(value = 
		        "select  " + 
		        "    f.id  " +
		        "from   " + 
		        "    facility f  " + 
		        "where   " + 
		        "     f.profile_id = :pid  " ,
		        nativeQuery =  true)
		   String findFacilityByProfile(@Param("pid") Long pid);
	  
	  @Query(value="select * from facility where profile_id=:pId ",nativeQuery=true)
	  Facility findFacilityByProfileId(@Param("pId") Long pId);

	  

	  
	  @Query(value = 
			  "       select distinct s.name from state s	"+
			  "     left outer join address a	"+
			  "     	on a.state_id = s.id	"+
			  "       left outer join profile p	"+
			  "     	on p.addr_id = a.id 	"+
							 "     where p.jhi_type like :type	"
					, nativeQuery = true)
		List<String> findFacilityState(@Param("type") String type);

}
