package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.CDR;


@SuppressWarnings("unused")
@Repository
public interface CDRRepository extends JpaRepository<CDR, Long> {

	
	 @Query(value = 
		    "select  " + 
		    "    c.id,             " +
		    "    c.calldate  ,         " + 
		    "    c.clid	,             " +
		    "    c.src  ,         " + 
		    "    c.dst	,             " +
		    "    c.dcontext  ,         " + 
		    "    c.channel	,             " +
		    "    c.dstchannel  ,         " + 
		    "    c.lastapp	,             " +
		    "    c.lastdata  ,         " + 
		    "    c.duration	,             " +
		    "    c.billsec  ,         " + 
		    "    c.disposition	,             " +
		    "    c.amaflags  ,         " + 
		    "    c.accountcode	,             " +
		    "    c.uniqueid  ,         " + 
		    "    c.userfield	,             " +
		    "    c.did  ,         " + 
		    "    c.recordingfile	,             " +
			"    c.cnum  ,         " + 
			"    c.cnam	,             " +
			"    c.outbound_cnum  ,         " + 
			"    c.outbound_cnam	,             " +
			"    c.dst_cnam ,          " + 
			"    c.linkedid  ,         " + 
			"    c.peeraccount	,             " +
			"    c.sequence           " + 
		    "from   " + 
		    "    cdr c  " ,
		    nativeQuery =  true)
	Object[][] getAllCdrRecords();
	 
	 
	 @Query(value = "select " +
			 "    c.id,             " +
			    "    c.calldate  ,         " + 
			    "    c.src  ,         " + 
			    "    c.dst	,             " +
			    "    c.linkedid  ,         " + 
			    "    c.duration	,             " +
			    "    c.aId 	agentId ,         " + 
			    "    c.npn	,             " +
			    "    c.extension	,             " +
			    "    CONCAT(c.first_name, ' ',c.last_name )	agentName ,         " + 
			    "   c.aNumber	agentNumber           " +
			    "	from   " + 
			    "    cdr_records c  " 
	 		,nativeQuery =  true)
	Object[][] getAllCdrViewRecords();
	 
	 
	 @Query(value = "select " +
			 "    count(*)           " +
			    "	from   " + 
			    "    cdr_records c  where linkedid like :linkedId " 
	 		,nativeQuery =  true)
	 int isLinkedIdExist(@Param("linkedId") String linkedId);
	 

   
}
