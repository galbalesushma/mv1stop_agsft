package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.CDRLogs;


/**
 * Spring Data  repository for the ZipArea entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CDR_Logs_Repository extends JpaRepository<CDRLogs, Long> {

	
	 @Query(value = 
		        "select  " + 
			        "  count(*) " +
			        "	from   cdr_logs c  " + 
			        "    where  c.file_path like :fileName and c.process_status like :status  " ,
			        nativeQuery =  true)
	int getFileCompletedCount(@Param("fileName") String fileName, @Param("status") String status);

		  @Query(value = 
	        "select  " + 
		        "   * " +
		        "	from   " + 
		        "    cdr_logs c  " + 
		        "    where c.file_path like :filePath and c.process_status like :status  " ,
		        nativeQuery =  true)
	  CDRLogs findByFilePath(@Param("filePath") String filePath,@Param("status") String status);
	
	
	CDRLogs findOneByFilePath(String filePath);

	
	@Query(value = 
		"	select id , file_created_at ,	"+
		"	file_path 			filePath,	"+
		"	process_status 			status,	"+
		"	record_count 			recordCount,	"+
		"	execution_time 			executionTime	"+
		" from iaa.cdr_logs c		",
		    nativeQuery =  true)
	Object[][] getAllCdrRecords();
}