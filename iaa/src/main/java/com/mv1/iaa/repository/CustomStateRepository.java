package com.mv1.iaa.repository;

import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.State;


/**
 * Spring Data  repository for the State entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomStateRepository extends StateRepository {
    
    State findOneByStateCodeIgnoreCase(String stateCode);
    
    State findOneByNameIgnoreCase(String name);
}
