package com.mv1.iaa.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.Business;
import com.mv1.iaa.domain.CustomerService;

@Repository
public interface CustomerServiceRepository extends JpaRepository<CustomerService, Long> {

	 @Query(value = 
			 "      select *  from customer_service c      " + 
			 "     where c.company_name like CONCAT('%', :company, '%')       " 
				, nativeQuery = true)
	List<CustomerService> findByCompanyName(@Param("company") String company);
}
