package com.mv1.iaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mv1.iaa.domain.ZipArea;


/**
 * Spring Data  repository for the ZipArea entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ZipAreaRepository extends JpaRepository<ZipArea, Long> {

}
