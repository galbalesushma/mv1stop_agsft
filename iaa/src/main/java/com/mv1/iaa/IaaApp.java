
package com.mv1.iaa;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.mv1.iaa.business.common.DirectoryWatcher;
import com.mv1.iaa.config.ApplicationProperties;
import com.mv1.iaa.config.DefaultProfileUtil;

import io.github.jhipster.config.JHipsterConstants;

@SpringBootApplication
@EnableConfigurationProperties({LiquibaseProperties.class, ApplicationProperties.class})
@EnableScheduling
public class IaaApp {

    private static final Logger log = LoggerFactory.getLogger(IaaApp.class);

    private final Environment env;

    public IaaApp(Environment env) {
        this.env = env;
    }
    

    
    /*@Autowired 
    private DirectoryWatcher ctcResource;*/

    /**
     * Initializes iaa.
     * <p>
     * Spring profiles can be configured with a program argument --spring.profiles.active=your-active-profile
     * <p>
     * You can find more information on how profiles work with JHipster on <a href="https://www.jhipster.tech/profiles/">https://www.jhipster.tech/profiles/</a>.
     */
    @PostConstruct
    public void initApplication() {
        Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
        if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_PRODUCTION)) {
            log.error("You have misconfigured your application! It should not run " +
                "with both the 'dev' and 'prod' profiles at the same time.");
        }
        if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_CLOUD)) {
            log.error("You have misconfigured your application! It should not " +
                "run with both the 'dev' and 'cloud' profiles at the same time.");
        }
        
    }
    
    @Bean
    public Validator jsr303Validator() {
        return new LocalValidatorFactoryBean();
    }

    /**
     * Main method, used to run the application.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(IaaApp.class);
        DefaultProfileUtil.addDefaultProfile(app);
        ConfigurableApplicationContext applicationContext = app.run(args);
        Environment env = applicationContext.getEnvironment();
        logApplicationStartup(env);
        
     /*   DirectoryWatcher directoryWatcher = applicationContext.getBean(DirectoryWatcher.class);
        directoryWatcher.fileWatcher();*/

        
    }

    
    
    
    
    private static void logApplicationStartup(Environment env) {
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        String serverPort = env.getProperty("server.port");
        String contextPath = env.getProperty("server.servlet.context-path");
        if (StringUtils.isBlank(contextPath)) {
            contextPath = "/";
        }
        String hostAddress = "localhost";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }
        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}{}\n\t" +
                "External: \t{}://{}:{}{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            protocol,
            serverPort,
            contextPath,
            protocol,
            hostAddress,
            serverPort,
            contextPath,
            env.getActiveProfiles());
    }
    
//    @Bean
//    public VelocityEngine getVelocityEngine() throws VelocityException, IOException{
//        VelocityEngineFactory factory = new VelocityEngineFactory();
//        Properties props = new Properties();
//        props.put("resource.loader", "classpath");        
//        props.put("classpath.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
//        factory.setVelocityProperties(props);
//        
//        VelocityContext context = new VelocityContext();
//        context.put("numberTool", new NumberTool());
//        
//        VelocityEngine ve = factory.createVelocityEngine();
//        ve.init();
//        return ve;      
//    }
    
    @Bean
    public VelocityEngine velocityEngine() throws Exception {
    Properties properties = new Properties();
    properties.setProperty("input.encoding", "UTF-8");
    properties.setProperty("output.encoding", "UTF-8");
    properties.setProperty("resource.loader", "class");
    properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    VelocityEngine velocityEngine = new VelocityEngine(properties);
    return velocityEngine;
    }
    
	/*
	 * @Bean public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
	 * DefaultHttpFirewall firewall = new DefaultHttpFirewall();
	 * firewall.setAllowUrlEncodedSlash(true); return firewall; }
	 */
	 

}
