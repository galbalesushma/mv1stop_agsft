package com.mv1.iaa.web.rest;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mv1.iaa.business.component.SearchQueryBuilder;
import com.mv1.iaa.business.enumeration.SearchTypes;
import com.mv1.iaa.business.view.search.Agent;
import com.mv1.iaa.business.view.search.Holder;
import com.mv1.iaa.business.view.search.SearchReq;
import com.mv1.iaa.business.view.search.Subs;
import com.mv1.iaa.business.view.web.resp.Page;
import com.mv1.iaa.service.SearchLogsService;
import com.mv1.iaa.service.dto.SearchLogsDTO;

@RestController
@RequestMapping("/api/search")
public class SearchResource <T> {

	@Autowired
    private SearchQueryBuilder<T> searchQueryBuilder;

	@Autowired
	private SearchLogsService searchLogsService;
	
    @SuppressWarnings("unchecked")
	@PostMapping(value = "/list/{page}")
    public Page<T> search(@PathVariable("page") Integer page, @RequestBody SearchReq req) {
        int from = 0, size = 0;
        if(page == null) {
            page = 1;
        }
        if(page == 1) {
            from = 0;
            size = 10;
        } else if(page == 2){
            size = 20;
            from = 10;
        } else {
            size = 20;
            from = (size * page) ;
        }
        
        createSerachLog(req);
        
        SearchTypes sType = SearchTypes.fromUserType(req.getType());
        Holder<List<T>> result = searchQueryBuilder.search(sType.getClazz(), 
                sType.getIndex(), req.getZip(), from, size);
        
        if( result.getType()==null  ||(result.getType()!=null && result.getType().isEmpty())) {
        	from = 0;
        	result = searchQueryBuilder.search(sType.getClazz(), 
                    sType.getIndex(), req.getZip(), from, size);
        }
              
    
        
        if(sType.getClazz().equals(Agent.class)){
        	List<Agent> agentList= (List<Agent>)result.getType();
        	List<Agent> agentOutputList = new ArrayList<>();

            if(agentList!=null) {
                for (Agent element : agentList ) {
                    for (Subs sub : element.getSubs()) {
                    	for(int i=0 ;i<sub.getSlots();i++) {
                    		System.out.println(sub.getZip()+"sub.getSlots()-------"+page+"-----------"+sub.getRank());
	                        Agent agentOutput = new Agent();
	                    
	                        agentOutput.setFirst_name(element.getFirst_name());
	                        agentOutput.setLast_name(element.getLast_name());
	                        agentOutput.setId(element.getId());
	
	                        if(page == 1) {
	                            if(sub.getZip().equals(req.getZip()) && sub.getRank().equals("ONE")){
	                                agentOutputList.add(agentOutput);
	                            }
	                        } else if(page == 2) {
	                        	System.out.println("page 22222222222----------------");
	                        	System.out.println("condition----------------"+(sub.getZip().equals(req.getZip()) && sub.getRank().equals("TWO")));
	                            if(sub.getZip().equals(req.getZip()) && sub.getRank().equals("TWO")){
	                                agentOutputList.add(agentOutput);
	                            }
	                        }else {
	                            if(sub.getZip().equals("00000") && sub.getRank().equals("THREE")){
	                                agentOutputList.add(agentOutput);
	                            }
	                        }
                    	}
                    }
                    
                }
            }
            
            
        	Page<T> resp = new Page<>();
            resp.setTotal(agentOutputList.size());
//            resp.setTotal(result.getTotal());
            resp.setResult((List<T>)agentOutputList);
            return resp;
        }
		
		        
        Page<T> resp = new Page<>();
        resp.setTotal(result.getTotal());
        resp.setResult(result.getType());

        return resp;
    }
    
    
    
     
    public void createSerachLog( SearchReq req){
        SearchLogsDTO searchLogs = new SearchLogsDTO();
        searchLogs.setEmail(req.getEmail());
        searchLogs.setFullName(req.getName());
        searchLogs.setPhone(req.getPhone());
        searchLogs.setSearchType(req.getType().toUpperCase());
        searchLogs.setZip(req.getZip());
        searchLogs.setSearchedDate(ZonedDateTime.now())
        ;
        if(req.getType().equalsIgnoreCase("facility")) {
        	searchLogs.setVehicleType(req.getVehicleType());
        	searchLogs.setVehicleYear(req.getVehicleYear());
        	searchLogs.setCounty(req.getCounty());
        }
        
        if(req.getType().equalsIgnoreCase("insurance")) {
        	searchLogs.setCompany(req.getCompany());
        }
        searchLogsService.save(searchLogs);
    }
}
