package com.mv1.iaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.service.SearchInsuranceLogsService;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;
import com.mv1.iaa.service.dto.SearchInsuranceLogsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SearchInsuranceLogs.
 */
@RestController
@RequestMapping("/api")
public class SearchInsuranceLogsResource {

    private final Logger log = LoggerFactory.getLogger(SearchInsuranceLogsResource.class);

    private static final String ENTITY_NAME = "searchInsuranceLogs";

    private final SearchInsuranceLogsService searchInsuranceLogsService;

    public SearchInsuranceLogsResource(SearchInsuranceLogsService searchInsuranceLogsService) {
        this.searchInsuranceLogsService = searchInsuranceLogsService;
    }

    /**
     * POST  /search-insurance-logs : Create a new searchInsuranceLogs.
     *
     * @param searchInsuranceLogsDTO the searchInsuranceLogsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new searchInsuranceLogsDTO, or with status 400 (Bad Request) if the searchInsuranceLogs has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/search-insurance-logs")
    @Timed
    public ResponseEntity<SearchInsuranceLogsDTO> createSearchInsuranceLogs(@RequestBody SearchInsuranceLogsDTO searchInsuranceLogsDTO) throws URISyntaxException {
        log.debug("REST request to save SearchInsuranceLogs : {}", searchInsuranceLogsDTO);
        if (searchInsuranceLogsDTO.getId() != null) {
            throw new BadRequestAlertException("A new searchInsuranceLogs cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SearchInsuranceLogsDTO result = searchInsuranceLogsService.save(searchInsuranceLogsDTO);
        return ResponseEntity.created(new URI("/api/search-insurance-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /search-insurance-logs : Updates an existing searchInsuranceLogs.
     *
     * @param searchInsuranceLogsDTO the searchInsuranceLogsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated searchInsuranceLogsDTO,
     * or with status 400 (Bad Request) if the searchInsuranceLogsDTO is not valid,
     * or with status 500 (Internal Server Error) if the searchInsuranceLogsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/search-insurance-logs")
    @Timed
    public ResponseEntity<SearchInsuranceLogsDTO> updateSearchInsuranceLogs(@RequestBody SearchInsuranceLogsDTO searchInsuranceLogsDTO) throws URISyntaxException {
        log.debug("REST request to update SearchInsuranceLogs : {}", searchInsuranceLogsDTO);
        if (searchInsuranceLogsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SearchInsuranceLogsDTO result = searchInsuranceLogsService.save(searchInsuranceLogsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, searchInsuranceLogsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /search-insurance-logs : get all the searchInsuranceLogs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of searchInsuranceLogs in body
     */
    @GetMapping("/search-insurance-logs")
    @Timed
    public List<SearchInsuranceLogsDTO> getAllSearchInsuranceLogs() {
        log.debug("REST request to get all SearchInsuranceLogs");
        return searchInsuranceLogsService.findAll();
    }

    /**
     * GET  /search-insurance-logs/:id : get the "id" searchInsuranceLogs.
     *
     * @param id the id of the searchInsuranceLogsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the searchInsuranceLogsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/search-insurance-logs/{id}")
    @Timed
    public ResponseEntity<SearchInsuranceLogsDTO> getSearchInsuranceLogs(@PathVariable Long id) {
        log.debug("REST request to get SearchInsuranceLogs : {}", id);
        Optional<SearchInsuranceLogsDTO> searchInsuranceLogsDTO = searchInsuranceLogsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(searchInsuranceLogsDTO);
    }

    /**
     * DELETE  /search-insurance-logs/:id : delete the "id" searchInsuranceLogs.
     *
     * @param id the id of the searchInsuranceLogsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/search-insurance-logs/{id}")
    @Timed
    public ResponseEntity<Void> deleteSearchInsuranceLogs(@PathVariable Long id) {
        log.debug("REST request to delete SearchInsuranceLogs : {}", id);
        searchInsuranceLogsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
