package com.mv1.iaa.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.service.PricePackageService;
import com.mv1.iaa.service.dto.PricePackageDTO;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PricePackage.
 */
@RestController
@RequestMapping("/api")
public class PricePackageResource {

    private final Logger log = LoggerFactory.getLogger(PricePackageResource.class);

    private static final String ENTITY_NAME = "pricePackage";

    private final PricePackageService pricePackageService;

    public PricePackageResource(PricePackageService pricePackageService) {
        this.pricePackageService = pricePackageService;
    }

    /**
     * POST  /price-packages : Create a new pricePackage.
     *
     * @param pricePackageDTO the pricePackageDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pricePackageDTO, or with status 400 (Bad Request) if the pricePackage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/price-packages")
    @Timed
    public ResponseEntity<PricePackageDTO> createPricePackage(@RequestBody PricePackageDTO pricePackageDTO) throws URISyntaxException {
        log.debug("REST request to save PricePackage : {}", pricePackageDTO);
        if (pricePackageDTO.getId() != null) {
            throw new BadRequestAlertException("A new pricePackage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PricePackageDTO result = pricePackageService.save(pricePackageDTO);
        return ResponseEntity.created(new URI("/api/price-packages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /price-packages : Updates an existing pricePackage.
     *
     * @param pricePackageDTO the pricePackageDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pricePackageDTO,
     * or with status 400 (Bad Request) if the pricePackageDTO is not valid,
     * or with status 500 (Internal Server Error) if the pricePackageDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/price-packages")
    @Timed
    public ResponseEntity<PricePackageDTO> updatePricePackage(@RequestBody PricePackageDTO pricePackageDTO) throws URISyntaxException {
        log.debug("REST request to update PricePackage : {}", pricePackageDTO);
        if (pricePackageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PricePackageDTO result = pricePackageService.save(pricePackageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pricePackageDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /price-packages : get all the pricePackages.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pricePackages in body
     */
    @GetMapping("/price-packages")
    @Timed
    public List<PricePackageDTO> getAllPricePackages() {
        log.debug("REST request to get all PricePackages");
        return pricePackageService.findAll();
    }

    /**
     * GET  /price-packages/:id : get the "id" pricePackage.
     *
     * @param id the id of the pricePackageDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pricePackageDTO, or with status 404 (Not Found)
     */
    @GetMapping("/price-packages/{id}")
    @Timed
    public ResponseEntity<PricePackageDTO> getPricePackage(@PathVariable Long id) {
        log.debug("REST request to get PricePackage : {}", id);
        Optional<PricePackageDTO> pricePackageDTO = pricePackageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pricePackageDTO);
    }

    /**
     * DELETE  /price-packages/:id : delete the "id" pricePackage.
     *
     * @param id the id of the pricePackageDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/price-packages/{id}")
    @Timed
    public ResponseEntity<Void> deletePricePackage(@PathVariable Long id) {
        log.debug("REST request to delete PricePackage : {}", id);
        pricePackageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
