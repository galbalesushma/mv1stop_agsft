package com.mv1.iaa.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.repository.LPALocationsRepository;
import com.mv1.iaa.service.LPALocationsService;
import com.mv1.iaa.service.dto.LPALocationsDTO;
import com.mv1.iaa.service.mapper.LPALocationsMapper;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing LPALocations.
 */
@RestController
@RequestMapping("/api")
public class LPALocationsResource {

	private final Logger log = LoggerFactory.getLogger(LPALocationsResource.class);

	private static final String ENTITY_NAME = "lPALocations";

	private final LPALocationsService lPALocationsService;

	@Autowired
	private LPALocationsRepository lPALocationsRepository;

	@Autowired
	private LPALocationsMapper lPALocationsMapper;

	public LPALocationsResource(LPALocationsService lPALocationsService) {
		this.lPALocationsService = lPALocationsService;
	}

	/**
	 * POST /lpa-locations : Create a new lPALocations.
	 *
	 * @param lPALocationsDTO the lPALocationsDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         lPALocationsDTO, or with status 400 (Bad Request) if the lPALocations
	 *         has already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/lpa-locations")
	@Timed
	public ResponseEntity<LPALocationsDTO> createLPALocations(@RequestBody LPALocationsDTO lPALocationsDTO)
			throws URISyntaxException {
		log.debug("REST request to save LPALocations : {}", lPALocationsDTO);
		if (lPALocationsDTO.getId() != null) {
			throw new BadRequestAlertException("A new lPALocations cannot already have an ID", ENTITY_NAME, "idexists");
		}
		LPALocationsDTO result = lPALocationsService.save(lPALocationsDTO);
		return ResponseEntity.created(new URI("/api/lpa-locations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /lpa-locations : Updates an existing lPALocations.
	 *
	 * @param lPALocationsDTO the lPALocationsDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         lPALocationsDTO, or with status 400 (Bad Request) if the
	 *         lPALocationsDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the lPALocationsDTO couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/lpa-locations")
	@Timed
	public ResponseEntity<LPALocationsDTO> updateLPALocations(@RequestBody LPALocationsDTO lPALocationsDTO)
			throws URISyntaxException {
		log.debug("REST request to update LPALocations : {}", lPALocationsDTO);
		if (lPALocationsDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		LPALocationsDTO result = lPALocationsService.save(lPALocationsDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lPALocationsDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /lpa-locations : get all the lPALocations.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of lPALocations
	 *         in body
	 */
	@GetMapping("/lpa-locations")
	@Timed
	public List<LPALocationsDTO> getAllLPALocations() {
		log.debug("REST request to get all LPALocations");
		return lPALocationsService.findAll();
	}

	/**
	 * GET /lpa-locations/:id : get the "id" lPALocations.
	 *
	 * @param id the id of the lPALocationsDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         lPALocationsDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/lpa-locations/{id}")
	@Timed
	public ResponseEntity<LPALocationsDTO> getLPALocations(@PathVariable Long id) {
		log.debug("REST request to get LPALocations : {}", id);
		Optional<LPALocationsDTO> lPALocationsDTO = lPALocationsService.findOne(id);
		return ResponseUtil.wrapOrNotFound(lPALocationsDTO);
	}

	/**
	 * DELETE /lpa-locations/:id : delete the "id" lPALocations.
	 *
	 * @param id the id of the lPALocationsDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/lpa-locations/{id}")
	@Timed
	public ResponseEntity<Void> deleteLPALocations(@PathVariable Long id) {
		log.debug("REST request to delete LPALocations : {}", id);
		lPALocationsService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/insurance/LpaList/{city}")
	@Timed
	public GenericResp getLPALocationsByCity(@PathVariable String city) {

		log.debug("REST request to get LPALocations : {}", city);
		GenericResp resp = new GenericResp();
		city = city.trim();
		List<LPALocationsDTO> lPALocationsDTO = lPALocationsRepository.findByCityContaining(city).stream()
				.map(lPALocationsMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
		resp.setValue(lPALocationsDTO);
		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage("LPA Location List fetched successfully.");
		return resp;
	}
}
