package com.mv1.iaa.web.rest;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.domain.User;
import com.mv1.iaa.repository.UserRepository;
import com.mv1.iaa.security.jwt.JWTFilter;
import com.mv1.iaa.security.jwt.TokenProvider;
import com.mv1.iaa.web.rest.vm.LoginVM;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;
    
    @Autowired
    private UserRepository userRepository;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManager authenticationManager) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/authenticate")
    @Timed
    public  ResponseEntity<GenericResp>  authorize(@Valid @RequestBody LoginVM loginVM) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());
            GenericResp<JWTToken> resp = new GenericResp();
            
    	
    	Optional<User> user =  userRepository.findOneByEmailIgnoreCase(loginVM.getUsername());
    	if(user== null ||  !user.isPresent()) {
    	   resp.setSuccess(false);
    	   resp.setMessage("User does not exists.");
		   resp.setStatusCode(400);
		   Optional<GenericResp> respOpt = Optional.of(resp);
		   return ResponseUtil.wrapOrNotFound(respOpt);
    	}
    	if(user.get()!=null &&  !user.get().getActivated()) {
    	   resp.setSuccess(false);
      	   resp.setMessage("Please activate your Account.");
  		   resp.setStatusCode(400);
  		   Optional<GenericResp> respOpt = Optional.of(resp);
  		   return ResponseUtil.wrapOrNotFound(respOpt);
    	}
    
    	
        try{
        	 Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
        	  SecurityContextHolder.getContext().setAuthentication(authentication);
              boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
              String jwt = tokenProvider.createToken(authentication, rememberMe);
              HttpHeaders httpHeaders = new HttpHeaders();
              httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
          	  resp.setSuccess(true);
          	  resp.setMessage("Log in  successfully");
          	  resp.setStatusCode(200);
//              return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
          	  resp.setValue(new JWTToken(jwt));
          	  Optional<GenericResp> respOpt = Optional.of(resp);
          	  return ResponseUtil.wrapOrNotFound(respOpt);
        }catch(Exception e){
        	   resp.setSuccess(false);
        	   resp.setMessage("Invalid Credentials");
    		   resp.setStatusCode(400);
//        	   return new ResponseEntity<>(null, null, HttpStatus.BAD_REQUEST);
    		   Optional<GenericResp> respOpt = Optional.of(resp);
    		   return ResponseUtil.wrapOrNotFound(respOpt);
        }
      
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
