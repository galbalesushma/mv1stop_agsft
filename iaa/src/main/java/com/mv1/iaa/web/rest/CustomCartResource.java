package com.mv1.iaa.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.component.SubsComponent;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.business.view.web.req.CartVM;
import com.mv1.iaa.business.view.web.resp.AvailableItemVM;

/**
 * REST controller for managing Cart.
 */
@RestController
@RequestMapping("/api")
public class CustomCartResource {

    private final Logger log = LoggerFactory.getLogger(CustomCartResource.class);

    @Autowired
    private SubsComponent subsComponent;

    /**
     * POST /cart/add : Create a new cart and add subscriptions to it.
     *
     * @param cart the cartDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         cartDTO, or with status 400 (Bad Request) if the cart has already an
     *         ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cart/add")
    @Timed
    public GenericResp<CartVM> add2Cart(@RequestBody CartVM cart) throws URISyntaxException {

        log.debug("REST request to save Cart : {}", cart);

        CartVM result = subsComponent.add2Cart(cart);
        GenericResp<CartVM> resp = new GenericResp<CartVM>();
        if (result != null) {
            resp.setStatusCode(200);
            resp.setValue(result);
            resp.setSuccess(true);
            resp.setMessage("Subscription added sucessfully");
        } else {
            resp.setMessage("Failed to add the subscription, Please re-check availability and try again");
            resp.setStatusCode(400);
            resp.setSuccess(false);
            resp.setValue(null);
        }
        return resp;
    }
    
    /**
     * GET /cart/{profileId} : get cart and subscriptions.
     *
     * @param profile Id to get
     * @return get the cartDTO, or with status 400 (Bad Request) if the cart has already an
     *         ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @GetMapping("/cart/{profileId}")
    @Timed
    public GenericResp<CartVM> getCart(@PathVariable("profileId") Long profileId) throws URISyntaxException {

        log.debug("REST request to get Cart for profile id : {}", profileId);

        CartVM result = subsComponent.getCart(profileId);
        GenericResp<CartVM> resp = new GenericResp<CartVM>();
        if (result != null) {
            resp.setStatusCode(200);
            resp.setValue(result);
            resp.setSuccess(true);
            resp.setMessage("Found the cart sucessfully");
        } else {
            resp.setMessage("Failed to find the cart, Please re-check availability and try again");
            resp.setStatusCode(400);
            resp.setSuccess(false);
            resp.setValue(null);
        }
        // TODO set subscription locking period and remove the subscriptions from cart after lock period is over
        return resp;
    }
    
    /**
     * POST /cart/remove : Remove subscription from cart.
     *
     * @param cart items
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         cartDTO, or with status 400 (Bad Request) if the cart has already an
     *         ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cart/remove")
    @Timed
    public GenericResp<CartVM> removeFromCart(@RequestBody CartVM cart) throws URISyntaxException {

        log.debug("REST request to remove subs from Cart : {}", cart);

        CartVM result = subsComponent.removeFromCart(cart);
        GenericResp<CartVM> resp = new GenericResp<CartVM>();
        if (result != null) {
            resp.setStatusCode(200);
            resp.setValue(result);
            resp.setSuccess(true);
            resp.setMessage("Subscription removed sucessfully");
        } else {
            resp.setMessage("Failed to remove the subscription, Please try again");
            resp.setStatusCode(400);
            resp.setSuccess(false);
            resp.setValue(null);
        }
        return resp;
    }
    
    /**
     * GET  /cart/available/{zip} : get all the available ranks & slots for the zip.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ranks and slots in body
     */
    @GetMapping("/slots/available/{profileId}/{zip}")
    @Timed
    public List<AvailableItemVM> getAvailableSlots(@PathVariable("profileId") Long profileId, 
                                                        @PathVariable("zip") String zip) {
        log.debug("REST request to get all available slot & ranks for the zip");
        return subsComponent.getAvailableRankAndSlots(zip, profileId);
    }
}
