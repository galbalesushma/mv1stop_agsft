package com.mv1.iaa.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.service.PackSubscriptionService;
import com.mv1.iaa.service.dto.PackSubscriptionCustomDTO;
import com.mv1.iaa.service.dto.PackSubscriptionDTO;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PackSubscription.
 */
@RestController
@RequestMapping("/api")
public class PackSubscriptionResource {

    private final Logger log = LoggerFactory.getLogger(PackSubscriptionResource.class);

    private static final String ENTITY_NAME = "packSubscription";

    private final PackSubscriptionService packSubscriptionService;

    public PackSubscriptionResource(PackSubscriptionService packSubscriptionService) {
        this.packSubscriptionService = packSubscriptionService;
    }

    /**
     * POST  /pack-subscriptions : Create a new packSubscription.
     *
     * @param packSubscriptionDTO the packSubscriptionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new packSubscriptionDTO, or with status 400 (Bad Request) if the packSubscription has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pack-subscriptions")
    @Timed
    public ResponseEntity<PackSubscriptionDTO> createPackSubscription(@RequestBody PackSubscriptionDTO packSubscriptionDTO) throws URISyntaxException {
        log.debug("REST request to save PackSubscription : {}", packSubscriptionDTO);
        if (packSubscriptionDTO.getId() != null) {
            throw new BadRequestAlertException("A new packSubscription cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PackSubscriptionDTO result = packSubscriptionService.save(packSubscriptionDTO);
        return ResponseEntity.created(new URI("/api/pack-subscriptions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pack-subscriptions : Updates an existing packSubscription.
     *
     * @param packSubscriptionDTO the packSubscriptionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated packSubscriptionDTO,
     * or with status 400 (Bad Request) if the packSubscriptionDTO is not valid,
     * or with status 500 (Internal Server Error) if the packSubscriptionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pack-subscriptions")
    @Timed
    public ResponseEntity<PackSubscriptionDTO> updatePackSubscription(@RequestBody PackSubscriptionDTO packSubscriptionDTO) throws URISyntaxException {
        log.debug("REST request to update PackSubscription : {}", packSubscriptionDTO);
        if (packSubscriptionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PackSubscriptionDTO result = packSubscriptionService.save(packSubscriptionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, packSubscriptionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pack-subscriptions : get all the packSubscriptions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of packSubscriptions in body
     */
    @GetMapping("/pack-subscriptions")
    @Timed
    public List<PackSubscriptionDTO> getAllPackSubscriptions() {
        log.debug("REST request to get all PackSubscriptions");
        return packSubscriptionService.findAll();
    }

    /**
     * GET  /pack-subscriptions/:id : get the "id" packSubscription.
     *
     * @param id the id of the packSubscriptionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the packSubscriptionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pack-subscriptions/{id}")
    @Timed
    public ResponseEntity<PackSubscriptionDTO> getPackSubscription(@PathVariable Long id) {
        log.debug("REST request to get PackSubscription : {}", id);
        Optional<PackSubscriptionDTO> packSubscriptionDTO = packSubscriptionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(packSubscriptionDTO);
    }

    /**
     * DELETE  /pack-subscriptions/:id : delete the "id" packSubscription.
     *
     * @param id the id of the packSubscriptionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pack-subscriptions/{id}")
    @Timed
    public ResponseEntity<Void> deletePackSubscription(@PathVariable Long id) {
        log.debug("REST request to delete PackSubscription : {}", id);
        packSubscriptionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    
    @GetMapping("/pack-subscriptions/by-profile/{profileId}")
    @Timed
     public List<PackSubscriptionCustomDTO> getPackSubscriptionByProfileId(@PathVariable Long profileId) {
	    log.debug("REST request to get PackSubscription : {}", profileId);

	    List<PackSubscriptionCustomDTO> packSubscriptionList= packSubscriptionService.findByProfileId(profileId);
	        return packSubscriptionList;
	   }    
    
    
  
}
