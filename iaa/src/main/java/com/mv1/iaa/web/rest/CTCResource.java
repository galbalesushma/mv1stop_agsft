package com.mv1.iaa.web.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;

import org.asteriskjava.manager.ManagerConnection;
import org.asteriskjava.manager.ManagerConnectionFactory;
import org.asteriskjava.manager.action.OriginateAction;
import org.asteriskjava.manager.response.ManagerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.common.Util;
import com.mv1.iaa.business.exception.Mv1Exception;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.business.view.web.req.AgentCallLogVM;
import com.mv1.iaa.business.view.web.req.CDRLogVM;
import com.mv1.iaa.business.view.web.req.CDRVm;
import com.mv1.iaa.business.view.web.resp.AdminInvoiceVM;
import com.mv1.iaa.business.view.web.resp.Page;
//import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.service.PBXIntegrationService;
import com.mv1.iaa.service.dto.CustomFilterSearchDTO;
import com.mv1.iaa.web.rest.UserJWTController.JWTToken;

import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class CTCResource<T> {

	@Autowired
	private  PBXIntegrationService pBXIntegrationService;

	@Value("${pbx.syncDirectoryPath}")
	private  String cdrDirectory;

	
	private final static Logger log = LoggerFactory.getLogger(CTCResource.class);
	/**
	 * GET /clickToCall/{extension} : get the agents by its NPN .
	 *
	 * @return the GenericResp and the agent in body
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/clickToCall/{extension}/{telNumber}")
	@Timed
	public Object clickToCall(@PathVariable("extension") String extension,
			@PathVariable("telNumber") String telNumber) {

		try {
			testManagerOriginate(extension, telNumber);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return null;
	}

	public static void testManagerOriginate(String extension, String telNumber) throws Exception {
		ManagerConnectionFactory factory = new ManagerConnectionFactory("mv1stop.skytelglobal.com", 5038, "mv1AManager",
				"MV1Stop!23$");
		ManagerConnection managerConnection = factory.createManagerConnection();
		managerConnection.login();
		OriginateAction originateAction = new OriginateAction();

		final String randomUUID = java.util.UUID.randomUUID().toString();

		System.out.println("ID random:_" + randomUUID);

//	        TBD - WORKED from CLI
//	        channel originate Local/1001@from-internal extension 1002@from-internal
//	        String callingNumber = "1001";
//	        String number = "1002";

//	        TBD - WORKED from CLI
//	        channel originate Local/6507777581@from-internal extension 1001@from-internal
//	        String callingNumber = "6507777137";//"Abhijeet-6505857010,Conf-6507777137,Amit-6507777581";
//	        String number = "1002"; //"1001"

		String callingNumber = telNumber;// "Abhijeet-6505857010,Conf-6507777137,Amit-6507777581";
		String number = extension; // "1001"

		originateAction.setChannel(String.format("Local/%s@from-internal", callingNumber));
		originateAction.setContext("from-internal");
		originateAction.setExten(number);
		originateAction.setPriority(1);
		originateAction.setTimeout(30000);
		originateAction.setAsync(true);

		System.out.println("originateAction :: " + originateAction.toString());

		ManagerResponse originateResponse = managerConnection.sendAction(originateAction, 30000);

		System.out.println("originateResponse :: " + originateResponse);

		Thread.sleep(2000);

		managerConnection.logoff();

//	        if(!"Error".equals(originateResponse.getResponse())){
//	            while (true) {
//	                Thread.sleep(100000);
//	                System.out.println(".");
//	            }
//	        }

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/cdrRecords")
	@Timed
	public ResponseEntity<GenericResp> cdrRecords() {

		GenericResp<JWTToken> resp = new GenericResp();
		resp.setSuccess(true);
		resp.setStatusCode(200);
		resp.setMessage("CDR file uploaded successfully.");
		Optional<GenericResp> respOpt = Optional.of(resp);

		try {
			pBXIntegrationService.moveSyncDataToGrab();
		} catch (Mv1Exception e) {
			System.out.println("execption----------" + e.getMessage());

			resp.setSuccess(false);
			resp.setStatusCode(400);
			resp.setMessage("Failed to upload csv file :" + e.getMessage());
			// TODO: handle exception
		}

		return ResponseUtil.wrapOrNotFound(respOpt);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/cdr/list")
	@Timed
	public Page<T>  getCdrRecordsList(@RequestParam("page") Integer page,
			@RequestParam(required = false) String calldate, @RequestParam(required = false) String cnum,
			 @RequestParam(required = false) String cnam,@RequestParam(required = false) String dst, @RequestParam(required = false) String src,
			@RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs, @RequestParam("pageSize") Integer pageSize,
			@RequestParam("type") String type,@RequestParam(required = false) String generalSearch ) {


		Date cdate = null;
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		System.out.println(calldate + "-----------------------parse date ........" );

		if (calldate != null && !calldate.isEmpty()) {
			try {
				cdate = format.parse(calldate);
			} catch (ParseException e) {
				System.out.println("Failed to parse payment_date ........");
				return null;
			}

		}
		
		
		List<CDRVm> list = new ArrayList<>();
		list = pBXIntegrationService.getCdrList(cdate,cnum,cnam,dst,src,customFilterSearchDTOs,type,generalSearch);

		int size = list.size();
		// Util.paginate(agents, page)
		Page<T> pageView = new Page<>();
		pageView.setResult((List<T>) Util.paginate(list, page, pageSize, false));
		pageView.setTotal(size);
		
		return pageView;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/cdr/list/download")
	@Timed
	public void  getCdrRecordsListDownload(HttpServletResponse response,
			@RequestParam(required = false) String calldate, @RequestParam(required = false) String cnum,
			 @RequestParam(required = false) String cnam,@RequestParam(required = false) String dst, @RequestParam(required = false) String src,
			@RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs, 
			@RequestParam("type") String type ,@RequestParam("fileType") String fileType ,@RequestParam(required = false) String generalSearch) {


		Date cdate = null;
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		System.out.println(calldate + "-----------------------parse date ........" );

		if (calldate != null && !calldate.isEmpty()) {
			try {
				cdate = format.parse(calldate);
			} catch (ParseException e) {
				System.out.println("Failed to parse payment_date ........");
//				return null;
			}

		}
		
		
		List<CDRVm> list = new ArrayList<>();
		list = pBXIntegrationService.getCdrList(cdate,cnum,cnam,dst,src,customFilterSearchDTOs,type,generalSearch);

		try {
			File file = pBXIntegrationService.downloadFacilityList(list,fileType);

			String mimeType = "text/csv";
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));
			response.setContentLength((int) file.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException ex) {
			log.debug("Error while downloading csv file", ex);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/cdr/log/list")
	@Timed
	public Page<T>  getCdrLogsList(@RequestParam("page") Integer page,
			@RequestParam("pageSize") Integer pageSize,@RequestParam(required = false) String generalSearch) {

		List<CDRLogVM> list = new ArrayList<>();
		list = pBXIntegrationService.getCdrLogList(generalSearch);

		int size = list.size();
		// Util.paginate(agents, page)
		Page<T> pageView = new Page<>();
		pageView.setResult((List<T>) Util.paginate(list, page, pageSize, false));
		pageView.setTotal(size);
		
		return pageView;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/cdr/download/file")
	@Produces("text/csv")
	public void getCdrDownloadFile(HttpServletRequest request,HttpServletResponse response,@RequestParam("id") Long fileId
			,@RequestParam(required = false) String generalSearch) throws IOException {

//		"/home/snehal/cdr_reports/mv1stopdatapull20190104114451.csv"
		try {
			pBXIntegrationService.downloadCDRFile(fileId,request,response,generalSearch);
		}catch (Mv1Exception e) {
			// TODO: handle exception
			System.out.println("Invalid file id");
		}catch (IOException e) {
			// TODO: handle exception
			System.out.println("File not found");
		}
		

	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/agent/call/log")
	@Timed
	public void  saveCdrLogsList(@RequestBody AgentCallLogVM agentCallLogVM) {

		pBXIntegrationService.saveCdrLogsList(agentCallLogVM);
	
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/admin/invoice/report")
	@Timed
	public Page<T>  getAdminInvoiceList(@RequestParam("page") Integer page,
			@RequestParam("pageSize") Integer pageSize,@RequestParam(required = false) String generalSearch) {

		List<AdminInvoiceVM> list = new ArrayList<>();
		list = pBXIntegrationService.getAdminInvoiceList(generalSearch);

		int size = list.size();
		// Util.paginate(agents, page)
		Page<T> pageView = new Page<>();
		pageView.setResult((List<T>) Util.paginate(list, page, pageSize, false));
		pageView.setTotal(size);
		
		return pageView;
	} 
}
