package com.mv1.iaa.web.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.FormSubmitEvent.MethodType;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.mv1.iaa.business.common.Util;
import com.mv1.iaa.business.component.AgentComponent;
import com.mv1.iaa.business.view.search.SearchReq;
import com.mv1.iaa.business.view.web.req.AgentVM;
import com.mv1.iaa.business.view.web.req.FacilityVM;
import com.mv1.iaa.business.view.web.req.PaymentReportVM;
import com.mv1.iaa.business.view.web.req.SearchLogsVM;
import com.mv1.iaa.business.view.web.resp.Page;
import com.mv1.iaa.business.view.web.resp.SearchPage;
import com.mv1.iaa.service.CustomAgentFilterSevice;
import com.mv1.iaa.service.SearchLogsService;
import com.mv1.iaa.service.dto.CustomFilterSearchDTO;
import com.mv1.iaa.service.dto.SearchLogsDTO;

/**
 * CustomAgentFilterResourse class for agent dashboard filters
 * 
 * @author snehal
 *
 */
@RestController
@RequestMapping("/api")
public class CustomAgentFilterResource<T> {

	private final Logger log = LoggerFactory.getLogger(CustomAgentFilterResource.class);

	@Autowired
	private CustomAgentFilterSevice customAgentFilterSevice;

	@Autowired
	private AgentComponent agentComponent;

	@Autowired
	private SearchLogsService searchLogsService;

	/**
	 * Get Agent States
	 * 
	 * @return List of StateDto
	 */
	@GetMapping("/all/states")
	public List<String> getAgentStates() {
		log.info("inside states");

		List<String> stateList = new ArrayList<>();
		stateList = customAgentFilterSevice.getStates();
		log.info("/all/states api success");
		return stateList;
	}

	/**
	 * Search cities by state
	 * 
	 * @param stateId stateId to search
	 * @param search  search text
	 * @return List of cityDTo
	 */
	@GetMapping("/state/cities")
	public List<String> getAgentCity(@RequestParam("search") String search) {
//		@RequestParam("stateId") String stateId,

		if (search.equals("")) {
			return new ArrayList<>();
		}

		log.info("inside city search");
//		Long sId = new Long(stateId);
		List<String> cityList = customAgentFilterSevice.cityByState(search);
		log.info("/state/cities api success");
		return cityList;
	}

	/**
	 * Search countries by state
	 * 
	 * @param stateId stateId to search
	 * @param search  search text
	 * @return List of CountyDTO
	 */
	@GetMapping("/state/counties")
	public List<String> getAgentCountry(@RequestParam("search") String search) {
//		@RequestParam("stateId") String stateId,
		if (search.equals("")) {
			return new ArrayList<>();
		}
		log.info("inside counties search");
//		Long sId = new Long(stateId);
		List<String> cityList = customAgentFilterSevice.countiesByState(search);
		log.info("counties list --" + cityList);
		return cityList;
	}

	/**
	 * Search zipcode by state
	 * 
	 * @param stateId stateId to search
	 * @param search  search text
	 * @return List of CountyDTO
	 */
	@GetMapping("/agent/zipcode")
	public List<String> getAgentZip(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> zipcodeList = customAgentFilterSevice.agentZipCode(search);
		log.info("zipcode list --" + zipcodeList);
		return zipcodeList;
	}

	/**
	 * Search agent first name
	 * 
	 * @param search search text
	 * @return List of agent Names
	 */
	@GetMapping("/agent/firstName")
	public List<String> searchAgentFirstName(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> agentList = customAgentFilterSevice.findAgentByFirstName(search);
		return agentList;
	}

	/**
	 * Search agent last name
	 * 
	 * @param search search text
	 * @return List of agent Names
	 */
	@GetMapping("/agent/lastName")
	public List<String> searchAgentLastName(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> agentList = customAgentFilterSevice.findAgentByLastName(search);
		return agentList;
	}

	/**
	 * Search agent npn
	 * 
	 * @param search search text
	 * @return List of agent npn
	 */
	@GetMapping("/agent/by/npn")
	public List<String> searchAgentNPNList(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> npnList = customAgentFilterSevice.findNpnList(search);
		return npnList;
	}

	/**
	 * Search agent company
	 * 
	 * @param search search text
	 * @return List of agent company
	 */
	@GetMapping("/search/company")
	public List<String> searchCompanyList(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> companyList = customAgentFilterSevice.findCompanyList(search);
		return companyList;
	}

	/**
	 * GET /agents : get all the agents.
	 * 
	 * @param <T>
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of agents in
	 *         body
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(path = "/agent/all/filter")
	@Timed
	public Page<T> getAllAgentsWithFilter(@RequestParam("page") Integer page,
			@RequestParam(required = false) String state, @RequestParam(required = false) String city,
			@RequestParam(required = false) String zip, @RequestParam(required = false) String licStatus,
			@RequestParam(required = false) String incAB, @RequestParam(required = false) String fName,
			@RequestParam(required = false) String lName, @RequestParam(required = false) String company,
			@RequestParam(required = false) String npn, @RequestParam(required = false) String county,
			@RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs, @RequestParam("pageSize") Integer pageSize,
			@RequestParam("isCustom") boolean isCustom, @RequestParam(required = false) String generalSearch) {

		log.debug("REST request to get all Agents");
		List<AgentVM> list = new ArrayList<>();
		if ((state == null || state == "" || state.isEmpty()) && (city == null || city == "" || city.isEmpty())
				&& (zip == null || zip == "" || zip.isEmpty())
				&& (licStatus == null || licStatus == "" || licStatus.isEmpty())
				&& (incAB == null || incAB == "" || incAB.isEmpty())
				&& (lName == null || lName == "" || lName.isEmpty())
				&& (fName == null || fName == "" || fName.isEmpty()) && (npn == null || npn.isEmpty() || npn == "")
				&& (county == null || county == "" || county.isEmpty())
				&& (company == null || company == "" || company.isEmpty())) {
			list = customAgentFilterSevice.getAllAgentsWithoutFilter(customFilterSearchDTOs, generalSearch);
		} else {
			list = customAgentFilterSevice.getAllAgentsByFilter(state, city, zip, licStatus, incAB, fName, lName,
					company, npn, county, customFilterSearchDTOs);
		}

		int size = list.size();
		// Util.paginate(agents, page)
		Page<T> pageView = new Page<>();
		pageView.setResult((List<T>) Util.paginate(list, page, pageSize, isCustom));
		pageView.setTotal(size);

		return pageView;
	}

	/**
	 * Search agent npn
	 * 
	 * @param search search text
	 * @return List of agent npn
	 */
	@GetMapping("/facility/by/isn")
	public List<String> searchFacilityISNList(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> isnList = customAgentFilterSevice.findIsnList(search);
		return isnList;
	}

	/*
	 * GET /agents : Download csv for the agents.
	 * 
	 * @param <T>
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of agents in
	 * body
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(path = "/agent/download/csv")
	@Produces("text/csv")
	public void downloadAgentCsvWithFilter(HttpServletResponse response, @RequestParam(required = false) String state,
			@RequestParam(required = false) String city, @RequestParam(required = false) String zip,
			@RequestParam(required = false) String licStatus, @RequestParam(required = false) String incAB,
			@RequestParam(required = false) String fName, @RequestParam(required = false) String lName,
			@RequestParam(required = false) String company, @RequestParam(required = false) String county,
			@RequestParam(required = false) String npn, @RequestParam("fileType") String fileType,
			@RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs,
			@RequestParam(required = false) String generalSearch) {

		log.debug("REST request to get all Agents");
		List<AgentVM> list = new ArrayList<>();
		if ((state == null || state == "" || state.isEmpty()) && (city == null || city == "" || city.isEmpty())
				&& (zip == null || zip == "" || zip.isEmpty())
				&& (licStatus == null || licStatus == "" || licStatus.isEmpty())
				&& (incAB == null || incAB == "" || incAB.isEmpty())
				&& (lName == null || lName == "" || lName.isEmpty())
				&& (fName == null || fName == "" || fName.isEmpty()) && (npn == null || npn.isEmpty() || npn == "")
				&& (county == null || county == "" || county.isEmpty())
				&& (company == null || company == "" || company.isEmpty())) {
			list = customAgentFilterSevice.getAllAgentsWithoutFilter(customFilterSearchDTOs, generalSearch);
		} else {
			list = customAgentFilterSevice.getAllAgentsByFilter(state, city, zip, licStatus, incAB, fName, lName,
					company, npn, county, customFilterSearchDTOs);
		}

		try {
			File file = customAgentFilterSevice.downloadAgentList(list, fileType);

			String mimeType = "text/csv";
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));
			response.setContentLength((int) file.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException ex) {
			log.debug("Error while downloading csv file", ex);
		}

	}

	/**
	 * GET /agents : get all the agents.
	 * 
	 * @param <T>
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of agents in
	 *         body
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(path = "/admin/payment/report")
	@Timed
	public Page<T> adminPaymentReport(@RequestParam("page") Integer page, @RequestParam("pageSize") Integer pageSize,
			@RequestParam("isCustom") boolean isCustom, @RequestParam(required = false) String fromDate,
			@RequestParam(required = false) String toDate, @RequestParam(required = false) String zip,
			@RequestParam(required = false) String incStatus,

			@RequestParam(required = false) String state, @RequestParam(required = false) String county,
			@RequestParam(required = false) String city, @RequestParam(required = false) String incAB,
			@RequestParam(required = false) String fname, @RequestParam(required = false) String lname,
			@RequestParam(required = false) String npn, @RequestParam(required = false) String isn,
			@RequestParam(required = false) String company, @RequestParam(required = false) String payment_date,
			@RequestParam(required = false) String utype, @RequestParam(required = false) String srep,
			@RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs,
			@RequestParam(required = false) String generalSearch) {
		System.out.println("payment_date--------------------" + payment_date);
		System.out.println("payment_date--------------------");
		Date toDate1 = null;
		Date fromDate1 = null;
		Date paymentDate = null;
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		System.out.println(fromDate + "-----------------------parse date ........" + toDate);
		if (toDate != null && fromDate != null && !toDate.isEmpty() && !fromDate.isEmpty()) {
			try {
//			   System.out.println("payment_date--------------------"+ format.parse(new Date(payment_date)));
				toDate1 = format.parse(toDate);
				fromDate1 = format.parse(fromDate);
			} catch (ParseException e) {
				System.out.println("Failed to parse date ........");
				return null;
			}

			if (toDate1 != null && fromDate1 != null && toDate1.before(fromDate1)) {
				return null;
			}
		}

		if (payment_date != null && !payment_date.isEmpty()) {
			try {
				paymentDate = format.parse(payment_date);
			} catch (ParseException e) {
				System.out.println("Failed to parse payment_date ........");
				return null;
			}

		}

		List<PaymentReportVM> list = new ArrayList<>();
		list = customAgentFilterSevice.getPaymentReport(fromDate1, toDate1, zip, incStatus, state, county, city, incAB,
				fname, lname, npn, isn, company, paymentDate, utype, srep, customFilterSearchDTOs, generalSearch);
		Page<T> pageView = new Page<>();
		if (list != null) {
			int size = list.size();
			// Util.paginate(agents, page)
			pageView.setResult((List<T>) Util.paginate(list, page, pageSize, isCustom));
			pageView.setTotal(size);
		}

		return pageView;
	}

	@SuppressWarnings("unchecked")
	@PostMapping(path = "/download/payment/report")
	@Timed
	public void adminPaymentReport(HttpServletResponse response, @RequestParam(required = false) String fromDate,
			@RequestParam(required = false) String toDate, @RequestParam(required = false) String zip,
			@RequestParam(required = false) String incStatus,

			@RequestParam(required = false) String state, @RequestParam(required = false) String county,
			@RequestParam(required = false) String city, @RequestParam(required = false) String incAB,
			@RequestParam(required = false) String fname, @RequestParam(required = false) String lname,
			@RequestParam(required = false) String npn, @RequestParam(required = false) String isn,
			@RequestParam(required = false) String company, @RequestParam(required = false) String payment_date,
			@RequestParam(required = false) String utype, @RequestParam(required = false) String srep,
			@RequestParam("fileType") String fileType, @RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs,
			@RequestParam(required = false) String generalSearch) {

		Date toDate1 = null;
		Date fromDate1 = null;
		Date paymentDate = null;
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		System.out.println(fromDate + "-----------------------parse date ........" + toDate);
		if ((toDate != null && !toDate.isEmpty()) || (fromDate != null && !fromDate.isEmpty())) {
			try {
				if ((toDate != null && !toDate.isEmpty())) {
					toDate1 = format.parse(toDate);
				}
				if ((fromDate != null && !fromDate.isEmpty())) {
					fromDate1 = format.parse(fromDate);
				}
			} catch (ParseException e) {
				System.out.println("Failed to parse date ........");
			}

			if (toDate1 != null && fromDate1 != null && toDate1.before(fromDate1)) {
			}
		}

		if (payment_date != null && !payment_date.isEmpty()) {
			try {
				paymentDate = format.parse(payment_date);
			} catch (ParseException e) {
				System.out.println("Failed to parse payment_date ........");
			}

		}

		List<PaymentReportVM> list = new ArrayList<>();
		list = customAgentFilterSevice.getPaymentReport(fromDate1, toDate1, zip, incStatus, state, county, city, incAB,
				fname, lname, npn, isn, company, paymentDate, utype, srep, customFilterSearchDTOs, generalSearch);

		try {
			File file = customAgentFilterSevice.downloadPaymentReport(list, fileType);

			if (fileType.equalsIgnoreCase("csv")) {
				String mimeType = "text/csv";
				response.setContentType(mimeType);
				response.setHeader("Content-Disposition",
						String.format("attachment; filename=\"" + file.getName() + "\""));
			} else {
				String mimeType = "application/vnd.ms-excel";
				response.setContentType(mimeType);
				response.setHeader("Content-Disposition",
						String.format("attachment; filename=\"" + file.getName() + "\""));
			}

			response.setContentLength((int) file.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException ex) {
			log.debug("Error while downloading csv file", ex);
		}
	}

	/**
	 * GET /agents : get all the agents.
	 * 
	 * @param <T>
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of agents in
	 *         body
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(path = "/admin/log/report")
	@Timed
	public Page<T> adminLogReport(@RequestParam("page") Integer page, @RequestParam("pageSize") Integer pageSize,
			@RequestParam("isCustom") boolean isCustom, @RequestParam(required = false) String zip,
			@RequestParam(required = false) String phone, @RequestParam(required = false) String searchType,
			@RequestParam(required = false) String email, @RequestParam(required = false) String fullName,
			@RequestParam(required = false) String vehicleType, @RequestParam(required = false) String vehicleYear,
			@RequestParam(required = false) String county, @RequestParam(required = false) String company,
			@RequestParam(required = false) String searchedDate, @RequestParam(required = false) String toDate,
			@RequestParam(required = false) String fromDate,
			@RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs,
			@RequestParam(required = false) String generalSearch) {

		Date searchedDateConvert = null;
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		if (searchedDate != null && searchedDate != "") {
			try {
				searchedDateConvert = format.parse(searchedDate);
			} catch (ParseException e) {
				System.out.println("Failed to parse payment_date ........");
			}

		}

		Date toDate1 = null;
		Date fromDate1 = null;
		if ((toDate != null && !toDate.isEmpty()) || (fromDate != null && !fromDate.isEmpty())) {
			try {
				if ((toDate != null && !toDate.isEmpty())) {
					toDate1 = format.parse(toDate);
				}
				if ((fromDate != null && !fromDate.isEmpty())) {
					fromDate1 = format.parse(fromDate);
				}

			} catch (ParseException e) {
				System.out.println("Failed to parse date ........");
			}

		}

		List<SearchLogsVM> list = new ArrayList<>();

		list = customAgentFilterSevice.getLogReport(zip, phone, searchType, email, fullName, vehicleType, vehicleYear,
				county, company, searchedDateConvert, toDate1, fromDate1, customFilterSearchDTOs, generalSearch);

		Page<T> pageView = new Page<>();
		if (list != null) {
			int size = list.size();
			// Util.paginate(agents, page)
			pageView.setResult((List<T>) Util.paginate(list, page, pageSize, isCustom));
			pageView.setTotal(size);
		}

		return pageView;

	}

	/**
	 * GET /agents : get all the agents.
	 * 
	 * @param <T>
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of agents in
	 *         body
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(path = "/download/log/report")
	@Timed
	public void downloadAdminLogReport(HttpServletResponse response, @RequestParam(required = false) String zip,
			@RequestParam(required = false) String phone, @RequestParam(required = false) String searchType,
			@RequestParam(required = false) String email, @RequestParam(required = false) String fullName,
			@RequestParam(required = false) String vehicleType, @RequestParam(required = false) String vehicleYear,
			@RequestParam(required = false) String county, @RequestParam(required = false) String company,
			@RequestParam(required = false) String searchedDate, @RequestParam(required = false) String toDate,
			@RequestParam(required = false) String fromDate, @RequestParam(required = false) String fileType,
			@RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs,
			@RequestParam(required = false) String generalSearch) {
		Date searchedDateConvert = null;
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		if (searchedDate != null && searchedDate != "") {
			try {
				searchedDateConvert = format.parse(searchedDate);
			} catch (ParseException e) {
				System.out.println("Failed to parse payment_date ........");
			}

		}

		Date toDate1 = null;
		Date fromDate1 = null;
		if ((toDate != null && !toDate.isEmpty()) || (fromDate != null && !fromDate.isEmpty())) {
			try {
				if ((toDate != null && !toDate.isEmpty())) {
					toDate1 = format.parse(toDate);
				}
				if ((fromDate != null && !fromDate.isEmpty())) {
					fromDate1 = format.parse(fromDate);
				}

			} catch (ParseException e) {
				System.out.println("Failed to parse date ........");
			}

		}

		List<SearchLogsVM> list = new ArrayList<>();

		list = customAgentFilterSevice.getLogReport(zip, phone, searchType, email, fullName, vehicleType, vehicleYear,
				county, company, searchedDateConvert, toDate1, fromDate1, customFilterSearchDTOs, generalSearch);

		try {
			File file = customAgentFilterSevice.downloadLogReport(list, fileType);

			if (fileType.equalsIgnoreCase("csv")) {
				String mimeType = "text/csv";
				response.setContentType(mimeType);
				response.setHeader("Content-Disposition",
						String.format("attachment; filename=\"" + file.getName() + "\""));
			} else {
				String mimeType = "application/vnd.ms-excel";
				response.setContentType(mimeType);
				response.setHeader("Content-Disposition",
						String.format("attachment; filename=\"" + file.getName() + "\""));
			}

			response.setContentLength((int) file.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException ex) {
			log.debug("Error while downloading csv file", ex);
		}
	}

	@GetMapping("/search/log/name")
	public List<String> searchNameForLogs(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> nameList = customAgentFilterSevice.findSearchNameList(search);
		return nameList;
	}

	@GetMapping("/search/log/email")
	public List<String> searchEmailForLogs(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> emailList = customAgentFilterSevice.findSearchEmailList(search);
		return emailList;
	}

	@GetMapping("/search/log/phone")
	public List<String> searchPhoneForLogs(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> phoneList = customAgentFilterSevice.findSearchPhonelList(search);
		return phoneList;
	}

	@GetMapping("/search/log/zip")
	public List<String> searchZipForLogs(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> zipList = customAgentFilterSevice.findSearchZiplList(search);
		return zipList;
	}

	@GetMapping("/search/log/vYear")
	public List<String> searchvehicleYearForLogs(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> vyearList = customAgentFilterSevice.findSearchVehicleYearList(search);
		return vyearList;
	}

	@GetMapping("/search/log/county")
	public List<String> searchCountyForLogs(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> countyList = customAgentFilterSevice.findSearchCountylList(search);
		return countyList;
	}

	@GetMapping("/search/log/company")
	public List<String> searchCompanyForLogs(@RequestParam("search") String search) {
		if (search.equals("")) {
			return new ArrayList<>();
		}
		List<String> companyList = customAgentFilterSevice.findSearchCompanylList(search);
		return companyList;
	}

	@GetMapping("/facility/cities")
	public List<String> getFacilityCity(@RequestParam("search") String search) {
		if (search.isEmpty()) {
			return new ArrayList<>();
		}
		List<String> cityList = customAgentFilterSevice.getFacilityCities(search);
		return cityList;
	}

	@GetMapping("/facility/zip")
	public List<String> getFacilityZip(@RequestParam("search") String search) {
		if (search.isEmpty() || search == null) {
			return new ArrayList<>();
		}
		List<String> zipList = customAgentFilterSevice.getFacilityZip(search);
		return zipList;
	}

	@GetMapping("/facility/fname")
	public List<String> getFacilityFname(@RequestParam("search") String search) {
		if (search.isEmpty()) {
			return new ArrayList<>();
		}
		List<String> fnameList = customAgentFilterSevice.getFacilityFname(search);
		return fnameList;
	}

	@GetMapping("/facility/lname")
	public List<String> getFacilityLName(@RequestParam("search") String search) {
		if (search.isEmpty()) {
			return new ArrayList<>();
		}
		List<String> lNameList = customAgentFilterSevice.getFacilityLname(search);
		return lNameList;
	}

	@GetMapping("/facility/isn")
	public List<String> getFacilityIsn(@RequestParam("search") String search) {
		if (search.isEmpty()) {
			return new ArrayList<>();
		}
		List<String> isnList = customAgentFilterSevice.getFacilityIsn(search);
		return isnList;
	}

	@GetMapping("/facility/business")
	public List<String> getFacilityBusinessName(@RequestParam("search") String search) {
		if (search.isEmpty()) {
			return new ArrayList<>();
		}
		List<String> bList = customAgentFilterSevice.getFacilityBusinessName(search);
		return bList;
	}

	@GetMapping("/facility/county")
	public List<String> getFacilityCountyName(@RequestParam("search") String search) {
		if (search.isEmpty()) {
			return new ArrayList<>();
		}
		List<String> cList = customAgentFilterSevice.getFacilityCountyName(search);
		return cList;
	}

	@GetMapping("/facility/state")
	public List<String> getFacilityStateName() {

		List<String> sList = customAgentFilterSevice.getFacilityStateName();
		return sList;
	}

	/**
	 * GET /agents : get all the agents.
	 * 
	 * @param <T>
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of agents in
	 *         body
	 */
	@SuppressWarnings("unchecked")
	@PostMapping(path = "/facility/all/filter")
	@Timed
	public Page<T> getAllFacilitiesWithFilter(@RequestParam("page") Integer page,
			@RequestParam(required = false) String city, @RequestParam(required = false) String zip,
			@RequestParam(required = false) String incStatus, @RequestParam(required = false) String incAB,
			@RequestParam(required = false) String fName, @RequestParam(required = false) String lName,
			@RequestParam("pageSize") Integer pageSize, @RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs,
			@RequestParam("isCustom") boolean isCustom, @RequestParam(required = false) String isn,
			@RequestParam(required = false) String county, @RequestParam(required = false) String state,
			@RequestParam(required = false) String bName, @RequestParam(required = false) String generalSearch) {
//		@RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs, 

		log.debug("REST request to get all Agents");
		List<FacilityVM> list = new ArrayList<>();

		list = customAgentFilterSevice.getAllFacilitiesByFilter(city, zip, incStatus, incAB, fName, lName, isn, county,
				state, bName, customFilterSearchDTOs, generalSearch);

		int size = list.size();
		// Util.paginate(agents, page)
		Page<T> pageView = new Page<>();
		pageView.setResult((List<T>) Util.paginate(list, page, pageSize, isCustom));
		pageView.setTotal(size);
		return pageView;
	}

	@SuppressWarnings("unchecked")
	@PostMapping(path = "/facility/download")
	@Produces("text/csv")
	public void downloadFacilitiesWithFilter(HttpServletResponse response, @RequestParam(required = false) String city,
			@RequestParam(required = false) String zip, @RequestParam(required = false) String incStatus,
			@RequestParam(required = false) String incAB, @RequestParam(required = false) String fName,
			@RequestParam(required = false) String lName,
			@RequestBody List<CustomFilterSearchDTO> customFilterSearchDTOs, @RequestParam(required = false) String isn,
			@RequestParam(required = false) String county, @RequestParam(required = false) String state,
			@RequestParam(required = false) String bName, @RequestParam("fileType") String fileType,
			@RequestParam(required = false) String generalSearch) {

		log.debug("REST request to get all Agents");
		List<FacilityVM> list = new ArrayList<>();

		list = customAgentFilterSevice.getAllFacilitiesByFilter(city, zip, incStatus, incAB, fName, lName, isn, county,
				state, bName, customFilterSearchDTOs, generalSearch);

		try {
			File file = customAgentFilterSevice.downloadFacilityList(list, fileType);

			String mimeType = "text/csv";
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));
			response.setContentLength((int) file.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException ex) {
			log.debug("Error while downloading csv file", ex);
		}

	}

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/search/list/db/{page}")
	public SearchPage searchAgentsFacility(@PathVariable("page") Integer page, @RequestBody SearchReq req,
			@RequestParam(required = false) String company, @RequestParam(required = false) String agent) {

		SearchLogsDTO savedSearchLogs = createSerachLog(req);

		SearchPage sPage = customAgentFilterSevice.getSearchAgentFacilitySlots(req.getType(), req.getZip(), page,
				company, agent);
		sPage.setSearchId(savedSearchLogs.getId());
		return sPage;
	}

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/search/insurance/list/db/{page}")
	public Page<T> searchAInsurance(@PathVariable("page") Integer page, @RequestBody SearchReq req,
			@RequestParam(required = false) String company, @RequestParam(required = false) String agent) {

		SearchLogsDTO savedSearchLogs = createSerachLog(req);

		System.out.println("company---------------------------------" + company);
		System.out.println("agent---------------------------------" + agent);
		SearchPage sPage = customAgentFilterSevice.getSearchOfInsurance(req.getType(), req.getZip(), page, company,
				agent);
		sPage.setSearchId(savedSearchLogs.getId());
		Page<T> pageView = new Page<>();

		if (sPage != null && sPage.getResult().size() > 0) {
			int size = sPage.getResult().size();
			// Util.paginate(agents, page)
			pageView.setResult((List<T>) Util.paginate(sPage.getResult(), page, 20, false));
			pageView.setTotal(size);
			pageView.setTotalPages(sPage.getTotalPages());
			return pageView;
		}
		else {
			pageView.setResult(new ArrayList<>());
			pageView.setTotal(0);
			pageView.setTotalPages(0);
			return pageView;
		}

	
	}

	/**
	 * Post : get customer service records.
	 * 
	 * @author sushama
	 * 
	 *         Date : - 10 Sept 19
	 * @param <T>
	 *
	 */
	@SuppressWarnings("unchecked")

	@RequestMapping(value = "/search/customer/service/list/db/{page}", method = RequestMethod.POST, consumes = "application/json")

	/* @PostMapping(value = "/search/customer/service/list/db/{page}") */
	public Page<T> searchCustomerServiceByCompany(@PathVariable("page") Integer page, @RequestBody SearchReq req,

			@RequestParam(required = false) String company) {

		SearchLogsDTO savedSearchLogs = createSerachLog(req);

		Page<T> pageView = new Page<>();

		System.out.println("company---------------------------------" + company);

		SearchPage sPage = customAgentFilterSevice.getSearchCustomerServiceByCompany(page, company);
		if (sPage != null && sPage.getResult().size() > 0) {
			sPage.setSearchId(savedSearchLogs.getId());
			int size = sPage.getResult().size();
			pageView.setResult((List<T>) Util.paginate(sPage.getResult(), page, 20, false));
			pageView.setTotal(size);
			pageView.setTotalPages(sPage.getTotalPages());
			return pageView;
		} else {
			pageView.setResult(new ArrayList<>());
			pageView.setTotal(0);
			pageView.setTotalPages(0);
			return pageView;
		}
	}

	public SearchLogsDTO createSerachLog(SearchReq req) {
		SearchLogsDTO searchLogs = new SearchLogsDTO();
		searchLogs.setEmail(req.getEmail());
		searchLogs.setFullName(req.getName());
		searchLogs.setPhone(req.getPhone());
		searchLogs.setSearchType(req.getType().toUpperCase());
		searchLogs.setZip(req.getZip());
		searchLogs.setSearchedDate(ZonedDateTime.now());

		if (req.getType().equalsIgnoreCase("facility")) {
			searchLogs.setVehicleType(req.getVehicleType());
			searchLogs.setVehicleYear(req.getVehicleYear());
			searchLogs.setCounty(req.getCounty());
		}

		if (req.getType().equalsIgnoreCase("insurance")) {
			searchLogs.setCompany(req.getCompany());
		}
		SearchLogsDTO savedSearchLogs = searchLogsService.save(searchLogs);
		return savedSearchLogs;
	}

}
