package com.mv1.iaa.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.service.AgentDistributionService;
import com.mv1.iaa.service.dto.AgentDistributionDTO;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing AgentDistribution.
 */
@RestController
@RequestMapping("/api")
public class AgentDistributionResource {

    private final Logger log = LoggerFactory.getLogger(AgentDistributionResource.class);

    private static final String ENTITY_NAME = "agentDistribution";

    private final AgentDistributionService agentDistributionService;

    public AgentDistributionResource(AgentDistributionService agentDistributionService) {
        this.agentDistributionService = agentDistributionService;
    }

    /**
     * POST  /agent-distributions : Create a new agentDistribution.
     *
     * @param agentDistributionDTO the agentDistributionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new agentDistributionDTO, or with status 400 (Bad Request) if the agentDistribution has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/agent-distributions")
    @Timed
    public ResponseEntity<AgentDistributionDTO> createAgentDistribution(@RequestBody AgentDistributionDTO agentDistributionDTO) throws URISyntaxException {
        log.debug("REST request to save AgentDistribution : {}", agentDistributionDTO);
        if (agentDistributionDTO.getId() != null) {
            throw new BadRequestAlertException("A new agentDistribution cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentDistributionDTO result = agentDistributionService.save(agentDistributionDTO);
        return ResponseEntity.created(new URI("/api/agent-distributions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /agent-distributions : Updates an existing agentDistribution.
     *
     * @param agentDistributionDTO the agentDistributionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated agentDistributionDTO,
     * or with status 400 (Bad Request) if the agentDistributionDTO is not valid,
     * or with status 500 (Internal Server Error) if the agentDistributionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/agent-distributions")
    @Timed
    public ResponseEntity<AgentDistributionDTO> updateAgentDistribution(@RequestBody AgentDistributionDTO agentDistributionDTO) throws URISyntaxException {
        log.debug("REST request to update AgentDistribution : {}", agentDistributionDTO);
        if (agentDistributionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentDistributionDTO result = agentDistributionService.save(agentDistributionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, agentDistributionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /agent-distributions : get all the agentDistributions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of agentDistributions in body
     */
    @GetMapping("/agent-distributions")
    @Timed
    public List<AgentDistributionDTO> getAllAgentDistributions() {
        log.debug("REST request to get all AgentDistributions");
        return agentDistributionService.findAll();
    }

    /**
     * GET  /agent-distributions/:id : get the "id" agentDistribution.
     *
     * @param id the id of the agentDistributionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the agentDistributionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/agent-distributions/{id}")
    @Timed
    public ResponseEntity<AgentDistributionDTO> getAgentDistribution(@PathVariable Long id) {
        log.debug("REST request to get AgentDistribution : {}", id);
        Optional<AgentDistributionDTO> agentDistributionDTO = agentDistributionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(agentDistributionDTO);
    }

    /**
     * DELETE  /agent-distributions/:id : delete the "id" agentDistribution.
     *
     * @param id the id of the agentDistributionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/agent-distributions/{id}")
    @Timed
    public ResponseEntity<Void> deleteAgentDistribution(@PathVariable Long id) {
        log.debug("REST request to delete AgentDistribution : {}", id);
        agentDistributionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
