package com.mv1.iaa.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.service.SearchLogsService;
import com.mv1.iaa.service.dto.SearchLogsDTO;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing SearchLogs.
 */
@RestController
@RequestMapping("/api")
public class SearchLogsResource {

    private final Logger log = LoggerFactory.getLogger(SearchLogsResource.class);

    private static final String ENTITY_NAME = "searchLogs";

    private final SearchLogsService searchLogsService;

    public SearchLogsResource(SearchLogsService searchLogsService) {
        this.searchLogsService = searchLogsService;
    }

    /**
     * POST  /search-logs : Create a new searchLogs.
     *
     * @param searchLogsDTO the searchLogsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new searchLogsDTO, or with status 400 (Bad Request) if the searchLogs has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/search-logs")
    @Timed
    public ResponseEntity<SearchLogsDTO> createSearchLogs(@Valid @RequestBody SearchLogsDTO searchLogsDTO) throws URISyntaxException {
        log.debug("REST request to save SearchLogs : {}", searchLogsDTO);
        if (searchLogsDTO.getId() != null) {
            throw new BadRequestAlertException("A new searchLogs cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SearchLogsDTO result = searchLogsService.save(searchLogsDTO);
        return ResponseEntity.created(new URI("/api/search-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /search-logs : Updates an existing searchLogs.
     *
     * @param searchLogsDTO the searchLogsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated searchLogsDTO,
     * or with status 400 (Bad Request) if the searchLogsDTO is not valid,
     * or with status 500 (Internal Server Error) if the searchLogsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/search-logs")
    @Timed
    public ResponseEntity<SearchLogsDTO> updateSearchLogs(@Valid @RequestBody SearchLogsDTO searchLogsDTO) throws URISyntaxException {
        log.debug("REST request to update SearchLogs : {}", searchLogsDTO);
        if (searchLogsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SearchLogsDTO result = searchLogsService.save(searchLogsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, searchLogsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /search-logs : get all the searchLogs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of searchLogs in body
     */
    @GetMapping("/search-logs")
    @Timed
    public List<SearchLogsDTO> getAllSearchLogs() {
        log.debug("REST request to get all SearchLogs");
        return searchLogsService.findAll();
    }

    /**
     * GET  /search-logs/:id : get the "id" searchLogs.
     *
     * @param id the id of the searchLogsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the searchLogsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/search-logs/{id}")
    @Timed
    public ResponseEntity<SearchLogsDTO> getSearchLogs(@PathVariable Long id) {
        log.debug("REST request to get SearchLogs : {}", id);
        Optional<SearchLogsDTO> searchLogsDTO = searchLogsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(searchLogsDTO);
    }

    /**
     * DELETE  /search-logs/:id : delete the "id" searchLogs.
     *
     * @param id the id of the searchLogsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/search-logs/{id}")
    @Timed
    public ResponseEntity<Void> deleteSearchLogs(@PathVariable Long id) {
        log.debug("REST request to delete SearchLogs : {}", id);
        searchLogsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
