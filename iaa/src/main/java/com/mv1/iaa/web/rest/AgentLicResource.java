package com.mv1.iaa.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.service.AgentLicService;
import com.mv1.iaa.service.dto.AgentLicDTO;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing AgentLic.
 */
@RestController
@RequestMapping("/api")
public class AgentLicResource {

    private final Logger log = LoggerFactory.getLogger(AgentLicResource.class);

    private static final String ENTITY_NAME = "agentLic";

    private final AgentLicService agentLicService;

    public AgentLicResource(AgentLicService agentLicService) {
        this.agentLicService = agentLicService;
    }

    /**
     * POST  /agent-lics : Create a new agentLic.
     *
     * @param agentLicDTO the agentLicDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new agentLicDTO, or with status 400 (Bad Request) if the agentLic has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/agent-lics")
    @Timed
    public ResponseEntity<AgentLicDTO> createAgentLic(@RequestBody AgentLicDTO agentLicDTO) throws URISyntaxException {
        log.debug("REST request to save AgentLic : {}", agentLicDTO);
        if (agentLicDTO.getId() != null) {
            throw new BadRequestAlertException("A new agentLic cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentLicDTO result = agentLicService.save(agentLicDTO);
        return ResponseEntity.created(new URI("/api/agent-lics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /agent-lics : Updates an existing agentLic.
     *
     * @param agentLicDTO the agentLicDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated agentLicDTO,
     * or with status 400 (Bad Request) if the agentLicDTO is not valid,
     * or with status 500 (Internal Server Error) if the agentLicDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/agent-lics")
    @Timed
    public ResponseEntity<AgentLicDTO> updateAgentLic(@RequestBody AgentLicDTO agentLicDTO) throws URISyntaxException {
        log.debug("REST request to update AgentLic : {}", agentLicDTO);
        if (agentLicDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentLicDTO result = agentLicService.save(agentLicDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, agentLicDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /agent-lics : get all the agentLics.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of agentLics in body
     */
    @GetMapping("/agent-lics")
    @Timed
    public List<AgentLicDTO> getAllAgentLics() {
        log.debug("REST request to get all AgentLics");
        return agentLicService.findAll();
    }

    /**
     * GET  /agent-lics/:id : get the "id" agentLic.
     *
     * @param id the id of the agentLicDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the agentLicDTO, or with status 404 (Not Found)
     */
    @GetMapping("/agent-lics/{id}")
    @Timed
    public ResponseEntity<AgentLicDTO> getAgentLic(@PathVariable Long id) {
        log.debug("REST request to get AgentLic : {}", id);
        Optional<AgentLicDTO> agentLicDTO = agentLicService.findOne(id);
        return ResponseUtil.wrapOrNotFound(agentLicDTO);
    }

    /**
     * DELETE  /agent-lics/:id : delete the "id" agentLic.
     *
     * @param id the id of the agentLicDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/agent-lics/{id}")
    @Timed
    public ResponseEntity<Void> deleteAgentLic(@PathVariable Long id) {
        log.debug("REST request to delete AgentLic : {}", id);
        agentLicService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
