package com.mv1.iaa.web.rest;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.gateway.txn.PaymentProcessor;
import com.mv1.iaa.business.view.web.req.PackSubVM;
import com.mv1.iaa.business.view.web.req.PaymentReceiptVM;
import com.mv1.iaa.business.view.web.req.PaymentVM;
import com.mv1.iaa.business.view.web.resp.PayResponseView;
import com.mv1.iaa.domain.Cart;
import com.mv1.iaa.domain.PackSubscription;
import com.mv1.iaa.repository.CartRepository;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.ZipAreaRepository;
import com.mv1.iaa.service.MailService;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.dto.ProfileDTO;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing PaymentGateway.
 */
@RestController
@RequestMapping("/api")
public class CustomPaymentResource {

	private final Logger log = LoggerFactory.getLogger(PaymentResource.class);

	@Autowired
	private PaymentProcessor paymentProcessor;

	@Autowired
	private MailService mailService;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private ZipAreaRepository zipAreaRepository;

	@Autowired
	private ProfileService profileService;

	@Autowired
	CustomAgentRepository customAgentRepository;

	/**
	 * POST /payment : Create a new paymentGateway.
	 *
	 * @param payment the payment to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         payment, or with status 400 (Bad Request) if the paymentGateway has
	 *         already an ID
	 * @throws Exception
	 */
	@PostMapping("/payment")
	@Timed
	public ResponseEntity<PayResponseView> processDefaultPayment(@RequestBody PaymentVM payment) throws Exception {
		log.debug("REST request to process default payment : {}", payment);

		PayResponseView view = paymentProcessor.process(payment);

		if (view.getMessage().contains("Thank you for your payment")) {

			File file = null;
			PaymentReceiptVM receipt = getReceiptResponse(payment);
			try {
				file = paymentProcessor.createTicket(receipt);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mailService.sendPaymentReceiptMail(receipt, file);
		}

		return ResponseUtil.wrapOrNotFound(Optional.of(view));
	}

	public PaymentReceiptVM getReceiptResponse(PaymentVM payment) {

		Optional<Cart> optCart = cartRepository.findOneWithEagerRelationships(payment.getCartId());
		Cart cart = null;
		Optional<ProfileDTO> pr = profileService.findOne(payment.getProfileId());
		PaymentReceiptVM pm = new PaymentReceiptVM();
		pm.setFirstName(payment.getFirstName());
		pm.setLastName(payment.getLastName());
		if (payment.getTxnType().trim().equalsIgnoreCase("paper_cheque")) {
			Long cheque_no = payment.getChequeNo();
			pm.setCheque_no(payment.getChequeNo());

		} else {
			if (payment.getTxnType().trim().equalsIgnoreCase("ACH")) {
				String lastFourDigitsRouting = payment.getRoutingNumber().substring(payment.getRoutingNumber().length() - 4);
				pm.setReceiptDate("**********" + lastFourDigitsRouting);
				String lastFourDigitsAccount = payment.getAccountNumber().substring(payment.getAccountNumber().length() - 4);
				pm.setAccountNumber("**********" + lastFourDigitsAccount);
			} else {
				if (payment.getTxnType().trim().equalsIgnoreCase("card")) {
					String lastFourDigits = payment.getCardNo().substring(payment.getCardNo().length() - 4);
					pm.setCardNo("**********" + lastFourDigits);
				}
			}
		}
		pm.setEmail(payment.getEmail());
		pm.setFirstName(payment.getFirstName());
		pm.setLastName(payment.getLastName());

		pm.setEmail(payment.getEmail());
		pm.setProfileId(payment.getProfileId());
		pm.setUserType(payment.getUserType());
		if (pr.get().getIsSpeaksSpanish() != null) {
			String lk = pr.get().isIsSpeaksSpanish() ? "es" : "en";
			pm.setLangKey(lk);
		} else {
			pm.setLangKey("en");
		}

		Date d = new Date();
		DateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm");
		String transactionTime = df.format(d);
		System.out.println("String in dd/MM/yyyy format is: " + transactionTime);

		pm.setTransactionTime(transactionTime);

		DateFormat df1 = new SimpleDateFormat("yyyy");
		String transactionTime1 = df1.format(d);
		System.out.println("String in yyyy format is: " + transactionTime);
		pm.setCopyRight(transactionTime1);

		DateFormat df2 = new SimpleDateFormat("MM_dd_yyyy");
		String transactionTime2 = df2.format(d);

		pm.setReceiptDate("receipt_" + transactionTime2);

		if (optCart.isPresent()) {
			cart = optCart.get();

			// update subscriptions

			List<PackSubVM> pvmList = new ArrayList<>();

			double totalAmount = 0;

			for (PackSubscription ps : cart.getSubscriptions()) {
				PackSubVM pvm = new PackSubVM();
				pvm.setPackageId(ps.getPackageId() + "");
				pvm.setSlots(ps.getSlots());

				String zp = cartRepository.findZipCodeFromId(ps.getZipAreaId());
				pvm.setZip(zp);
				int price = 0;
				int amount = 0;
				int carrierCount = 0;
				carrierCount = customAgentRepository.findCountByProfileId(ps.getProfile().getId());
				// carrierCount=CustomAgentRepository;
				if (ps.getPackageId() == 1) {
					if (carrierCount == 0) {
						price += 100 * ps.getSlots(); // now price by yearly
						pvm.setAmount("$" + (100 * ps.getSlots()));
					} else if (carrierCount >= 1 && carrierCount <= 3) {
						price += 300 * ps.getSlots(); // now price by yearly
						pvm.setAmount("$" + (300 * ps.getSlots()));
					} else if (carrierCount >= 4) {
						price += 400 * ps.getSlots(); // now price by yearly
						pvm.setAmount("$" + (400 * ps.getSlots()));
					}

				} else if (ps.getPackageId() == 2) {
					if (carrierCount == 0) {
						price += 50 * ps.getSlots(); // now price by yearly
						pvm.setAmount("$" + (50 * ps.getSlots()));
					} else if (carrierCount >= 1 && carrierCount <= 3) {
						price += 150 * ps.getSlots(); // now price by yearly
						pvm.setAmount("$" + (150 * ps.getSlots()));
					} else if (carrierCount >= 4) {
						price += 200 * ps.getSlots(); // now price by yearly
						pvm.setAmount("$" + (200 * ps.getSlots()));
					}
				} else {
					price += 25 * ps.getSlots();
					pvm.setAmount("$" + 25);
				}

				totalAmount += price;

				/*
				 * if(ps.getPackageId() == 1) { price = 100; }else if(ps.getPackageId() == 2) {
				 * price = 50; } totalAmount = totalAmount + (price*ps.getSlots());
				 * pvm.setAmount("$"+price*ps.getSlots());
				 */
				pvm.setAmount("$" + price);

				pvmList.add(pvm);
			}

			pm.setPackSubVM(pvmList);
			pm.setTotalAmount("$" + totalAmount);
		}
		return pm;

	}

}