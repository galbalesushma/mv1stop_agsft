package com.mv1.iaa.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.service.ZipAreaService;
import com.mv1.iaa.service.dto.ZipAreaDTO;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing ZipArea.
 */
@RestController
@RequestMapping("/api")
public class ZipAreaResource {

    private final Logger log = LoggerFactory.getLogger(ZipAreaResource.class);

    private static final String ENTITY_NAME = "zipArea";

    private final ZipAreaService zipAreaService;

    public ZipAreaResource(ZipAreaService zipAreaService) {
        this.zipAreaService = zipAreaService;
    }

    /**
     * POST  /zip-areas : Create a new zipArea.
     *
     * @param zipAreaDTO the zipAreaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new zipAreaDTO, or with status 400 (Bad Request) if the zipArea has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/zip-areas")
    @Timed
    public ResponseEntity<ZipAreaDTO> createZipArea(@RequestBody ZipAreaDTO zipAreaDTO) throws URISyntaxException {
        log.debug("REST request to save ZipArea : {}", zipAreaDTO);
        if (zipAreaDTO.getId() != null) {
            throw new BadRequestAlertException("A new zipArea cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ZipAreaDTO result = zipAreaService.save(zipAreaDTO);
        return ResponseEntity.created(new URI("/api/zip-areas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /zip-areas : Updates an existing zipArea.
     *
     * @param zipAreaDTO the zipAreaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated zipAreaDTO,
     * or with status 400 (Bad Request) if the zipAreaDTO is not valid,
     * or with status 500 (Internal Server Error) if the zipAreaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/zip-areas")
    @Timed
    public ResponseEntity<ZipAreaDTO> updateZipArea(@RequestBody ZipAreaDTO zipAreaDTO) throws URISyntaxException {
        log.debug("REST request to update ZipArea : {}", zipAreaDTO);
        if (zipAreaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ZipAreaDTO result = zipAreaService.save(zipAreaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, zipAreaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /zip-areas : get all the zipAreas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of zipAreas in body
     */
    @GetMapping("/zip-areas")
    @Timed
    public List<ZipAreaDTO> getAllZipAreas() {
        log.debug("REST request to get all ZipAreas");
        return zipAreaService.findAll();
    }

    /**
     * GET  /zip-areas/:id : get the "id" zipArea.
     *
     * @param id the id of the zipAreaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the zipAreaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/zip-areas/{id}")
    @Timed
    public ResponseEntity<ZipAreaDTO> getZipArea(@PathVariable Long id) {
        log.debug("REST request to get ZipArea : {}", id);
        Optional<ZipAreaDTO> zipAreaDTO = zipAreaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(zipAreaDTO);
    }

    /**
     * DELETE  /zip-areas/:id : delete the "id" zipArea.
     *
     * @param id the id of the zipAreaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/zip-areas/{id}")
    @Timed
    public ResponseEntity<Void> deleteZipArea(@PathVariable Long id) {
        log.debug("REST request to delete ZipArea : {}", id);
        zipAreaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
