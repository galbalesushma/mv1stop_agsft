package com.mv1.iaa.web.rest;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.domain.User;
import com.mv1.iaa.repository.UserRepository;
import com.mv1.iaa.security.SecurityUtils;
import com.mv1.iaa.service.MailService;
import com.mv1.iaa.service.UserService;
import com.mv1.iaa.service.dto.PasswordChangeDTO;
import com.mv1.iaa.service.dto.UserDTO;
import com.mv1.iaa.web.rest.UserJWTController.JWTToken;
import com.mv1.iaa.web.rest.errors.EmailAlreadyUsedException;
import com.mv1.iaa.web.rest.errors.EmailNotFoundException;
import com.mv1.iaa.web.rest.errors.InternalServerErrorException;
import com.mv1.iaa.web.rest.errors.InvalidPasswordException;
import com.mv1.iaa.web.rest.errors.LoginAlreadyUsedException;
import com.mv1.iaa.web.rest.vm.KeyAndPasswordVM;
import com.mv1.iaa.web.rest.vm.ManagedUserVM;

import io.github.jhipster.web.util.ResponseUtil;


/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserRepository userRepository;

    private final UserService userService;

    private final MailService mailService;
    
    private final PasswordEncoder passwordEncoder;

    public AccountResource(UserRepository userRepository, UserService userService, MailService mailService,
    		 PasswordEncoder passwordEncoder) {
    	this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
    }

    /**
     * POST  /register : register the user.
     *
     * @param managedUserVM the managed user View Model
     * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
     * @throws LoginAlreadyUsedException 400 (Bad Request) if the login is already used
     */
    @PostMapping("/register")
    @Timed
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            throw new InvalidPasswordException();
        }
        User user = userService.registerUser(managedUserVM, managedUserVM.getPassword());
        mailService.sendActivationEmail(user);
    }

    /**
     * GET  /activate : activate the registered user.
     *
     * @param key the activation key
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be activated
     */
    @GetMapping("/activate")
    @Timed
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent()) {
            throw new InternalServerErrorException("No user was found for this activation key");
        }
    }

    /**
     * GET  /authenticate : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request
     * @return the login if the user is authenticated
     */
    @GetMapping("/authenticate")
    @Timed
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * GET  /account : get the current user.
     *
     * @return the current user
     * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be returned
     */
    @GetMapping("/account")
    @Timed
    public UserDTO getAccount() {
        return userService.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new InternalServerErrorException("User could not be found"));
    }

    /**
     * POST  /account : update the current user information.
     *
     * @param userDTO the current user information
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already used
     * @throws RuntimeException 500 (Internal Server Error) if the user login wasn't found
     */
    @PostMapping("/account")
    @Timed
    public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
        final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) {
            throw new InternalServerErrorException("User could not be found");
        }
        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
            userDTO.getLangKey(), userDTO.getImageUrl());
    }

    /**
     * POST  /account/change-password : changes the current user's password
     *
     * @param passwordChangeDto current and new password
     * @throws InvalidPasswordException 400 (Bad Request) if the new password is incorrect
     */
    @PostMapping(path = "/account/change-password")
    @Timed
    public ResponseEntity<GenericResp> changePassword(@RequestBody PasswordChangeDTO passwordChangeDto) {
 	   GenericResp<JWTToken> resp = new GenericResp();
 	   resp.setSuccess(false);
	   resp.setStatusCode(400);
	   Optional<GenericResp> respOpt = Optional.of(resp);
		   
        if (!checkPasswordLength(passwordChangeDto.getNewPassword())) {
        	 resp.setMessage("Incorrect password length");
     		   return ResponseUtil.wrapOrNotFound(respOpt);
        }
        if (!passwordEncoder.matches(passwordChangeDto.getNewPassword(), passwordChangeDto.getCurrentPassword())) {
        	resp.setMessage("Password and Confirm Password does not match.");
    		return ResponseUtil.wrapOrNotFound(respOpt);
        }
     
        
        userService.changePassword(passwordChangeDto.getCurrentPassword(), passwordChangeDto.getNewPassword());
        resp.setSuccess(true);
        resp.setMessage("Password changed successfully.");
 	    resp.setStatusCode(200);
        return null;
    }

    /**
     * POST   /account/reset-password/init : Send an email to reset the password of the user
     *
     * @param mail the mail of the user
     * @return 
     * @throws EmailNotFoundException 400 (Bad Request) if the email address is not registered
     */
    @PostMapping(path = "/account/reset-password/init/{email:.+}")
    @Timed
    public ResponseEntity<GenericResp> requestPasswordReset( @PathVariable("email")  String email) {
    	   GenericResp<JWTToken> resp = new GenericResp();
    	Optional<User>  user =    userService.requestPasswordReset(email);
//    	 .orElseThrow(EmailNotFoundException::new)
    	 if(user == null || !user.isPresent()) {
    	  	   resp.setSuccess(false);
         	   resp.setMessage("Email Id doest not exists.");
     		   resp.setStatusCode(400);
     		   Optional<GenericResp> respOpt = Optional.of(resp);
     		   return ResponseUtil.wrapOrNotFound(respOpt);
    	 }

    	 mailService.sendPasswordResetMail(user.get());
	   resp.setSuccess(true);
	   resp.setMessage("Reset Password link sent to your email address.");
	   resp.setStatusCode(200);
	   Optional<GenericResp> respOpt = Optional.of(resp);
	   return ResponseUtil.wrapOrNotFound(respOpt);
    }

    /**
     * POST   /account/reset-password/finish : Finish to reset the password of the user
     *
     * @param keyAndPassword the generated key and the new password
     * @return 
     * @throws InvalidPasswordException 400 (Bad Request) if the password is incorrect
     * @throws RuntimeException 500 (Internal Server Error) if the password could not be reset
     */
    @PostMapping(path = "/account/reset-password/finish")
    @Timed
    public ResponseEntity<GenericResp> finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
    	  GenericResp<JWTToken> resp = new GenericResp();
    	   resp.setSuccess(false);
   	   resp.setStatusCode(400);
   	   Optional<GenericResp> respOpt = Optional.of(resp);
   		   
         
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
//            throw new InvalidPasswordException();
        	resp.setMessage("Incorrect password length");
		   return ResponseUtil.wrapOrNotFound(respOpt);
        }
        Optional<User> user =
            userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey());

        if (!user.isPresent()) {
//            throw new InternalServerErrorException("No user was found for this reset key");
        	resp.setMessage("No user was found for this reset key");
 		   return ResponseUtil.wrapOrNotFound(respOpt);
        }
        
		resp.setSuccess(true);
	    resp.setStatusCode(200);
		resp.setMessage("Password reset successfully.");
		return ResponseUtil.wrapOrNotFound(respOpt);
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }
    
    @GetMapping(path = "/open/admin/passsword")
    @Timed
    public String encodePassword() {
	    String password = "Password123#";
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		return hashedPassword;
    }

}
