package com.mv1.iaa.web.rest.vm;

public class SearchInsuranceVM {

    private static final String[] keys = new String[] {
            "name",           
            "id",
       };
	
	private String name;
	
	private Long id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public static String[] getKeys() {
		return keys;
	}
	
	
	
}
