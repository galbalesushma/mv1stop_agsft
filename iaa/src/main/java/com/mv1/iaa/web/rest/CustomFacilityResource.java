package com.mv1.iaa.web.rest;

import java.net.URISyntaxException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.component.FacillityComponent;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.business.view.web.req.FacilityIdVM;
import com.mv1.iaa.business.view.web.req.FacilityVM;
import com.mv1.iaa.business.view.web.req.ProfileVm;
import com.mv1.iaa.domain.Facility;
import com.mv1.iaa.domain.Profile;
import com.mv1.iaa.domain.User;
import com.mv1.iaa.repository.CustomFacilityRepository;
import com.mv1.iaa.repository.CustomProfileRepository;
import com.mv1.iaa.repository.UserRepository;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.UserService;
import com.mv1.iaa.service.dto.ProfileDTO;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Facility.
 */
@RestController
@RequestMapping("/api")
public class CustomFacilityResource {

    private final Logger log = LoggerFactory.getLogger(CustomFacilityResource.class);

    @Autowired
    private FacillityComponent facillityComponent;
    
    @Autowired
    private CustomProfileRepository customProfileRepository;
    
    @Autowired
    private CustomFacilityRepository customFacilityRepository;
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;
    
    
    /**
     * POST  /facility : Create/update a new facility.
     *
     * @param facilityVm the facilityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new facilityDTO, or with status 400 (Bad Request) if the facility has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/facility/save")
    @Timed
    public GenericResp<FacilityVM> upsertFacility(@RequestBody FacilityVM facilityVm) throws URISyntaxException {
        
	log.debug("REST request to save Facility : {}", facilityVm);
        
        FacilityVM result = facillityComponent.upsertFacility(facilityVm);
        GenericResp<FacilityVM> resp = new GenericResp<>();
        
        if(result != null && result.getId() != null) {
            resp.setStatusCode(200);
            resp.setSuccess(true);
            resp.setValue(result);
            resp.setMessage("Facility data is persisted successfully.");
        } else {
            resp.setStatusCode(400);
            resp.setSuccess(false);
            resp.setValue(result);
            resp.setMessage("Failed to persist the Facility data, Please try again.");
        }
        
        return resp;
    }

    /**
     * GET  /facility/:id : get the "id" facility.
     *
     * @param id the id of the facilityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the facilityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/facility/get/{id}")
    @Timed
    public ResponseEntity<FacilityVM> getFacility(@PathVariable Long id) {
        log.debug("REST request to get Facility : {}", id);
        Optional<FacilityVM> vm = Optional.ofNullable(facillityComponent.getFacility(id));
    	if (vm != null) {
    		if(vm.get().getPrefEmail()!=null) {
//    			Profile profile = customProfileRepository.findProfileByPrefEmail(vm.get().getPrefEmail());
    			Optional<Profile> p = customProfileRepository.findById(id);
    			if(p!=null && p.get()!=null) {
    				Profile profile = p.get();
    				vm.get().setPrefPhone(profile.getPhone());
    				vm.get().setPrefMobile(profile.getPrefMobile());
    				vm.get().setPrefEmail(profile.getPrefEmail());
    				vm.get().setProfileId(profile.getId());
    				vm.get().setPrefCompany(profile.getPrefCompany());
    				vm.get().setComments(profile.getComments());
//    				vm.get().setMech_duty(profile.getMechDuty());
    			}
    		}
    		
    	}
        
        return ResponseUtil.wrapOrNotFound(vm);
    }

    /**
     * DELETE  /facilities/:id : delete the "id" facility.
     *
     * @param id the id of the facilityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @SuppressWarnings("rawtypes")
    @PostMapping("/facility/delete")
    @Timed
    public ResponseEntity<GenericResp> deleteFacility(@RequestBody FacilityIdVM facilityIdVm) {
        log.debug("REST request to delete Facility : {}", facilityIdVm);
        Integer count = facillityComponent.deleteFacility(facilityIdVm);
        
        GenericResp resp = new GenericResp();
        boolean status = count != null && count > 0;
        resp.setSuccess(status);
        if (status) {
            resp.setMessage("Facility delete was successful");
            resp.setStatusCode(200);
        } else {
            resp.setMessage("Error while deleting facility, Please try again.");
            resp.setStatusCode(400);
        }

        log.debug("REST response of delete facility : {}", resp);
        Optional<GenericResp> respOpt = Optional.of(resp);

        return ResponseUtil.wrapOrNotFound(respOpt);
    }
    
    /**
	 * GET /agent/{npn} : get the agents by its NPN .
	 *
	 * @return the GenericResp and the agent in body
     * @throws Exception 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/facilityProfile/save/{profileId}")
	@Timed
	public GenericResp saveAgentProfile(@PathVariable("profileId") Long profileId,@RequestBody ProfileVm agentVM) throws Exception {

		log.debug("REST request to get Agent by profileId : " + profileId);
		GenericResp resp = new GenericResp();
		if (profileId == null) {
			resp.setStatusCode(400);
			resp.setSuccess(false);
			resp.setMessage("Failed to retrieve agent data, Please try again.");
			return resp;
		}
		Facility facility = customFacilityRepository.findFacilityByProfileId(profileId);
		if (facility != null) {
			  Optional<ProfileDTO> optDto = profileService.findOne(profileId);
              if (optDto.isPresent()) {
            	    ProfileDTO  dto = optDto.get();
            	    dto.setPrefMobile(agentVM.getPreferredMobile());
	                dto.setPhone(agentVM.getPhone());
	                dto.setPrefCompany(agentVM.getPrefCompany());
	                dto.setComments(agentVM.getComments());
//	                dto.setMechDuty(agentVM.getMechDuty());
	                if(agentVM.getLangKey()!=null && agentVM.getLang()!=null && !agentVM.getLang().isEmpty()
	                		&& !agentVM.getLangKey().isEmpty() ) {
	                    dto.setIsSpeaksSpanish(agentVM.getLang().equalsIgnoreCase("ES"));
	                }
	            
            	    profileService.save(dto);
            	    
            	    Optional<User> newUser = userService.getUserWithAuthorities(optDto.get().getUserId());
            	    if(agentVM.getLangKey()!=null && agentVM.getLang()!=null && !agentVM.getLang().isEmpty()
	                		&& !agentVM.getLangKey().isEmpty() ) {            		   User user =   newUser.get();
            		   user.setLangKey(agentVM.getLangKey());
            		   userRepository.save(user);
            	   }
            	    
            	    facility.setMechDuty(agentVM.getMechDuty());
            	    customFacilityRepository.save(facility);
            	    
            	    resp.setValue(optDto);
        			resp.setStatusCode(200);
        			resp.setSuccess(true);
        			resp.setMessage("Agent profile updated successfully." );
              }else {
      			resp.setStatusCode(400);
      			resp.setSuccess(false);
      			resp.setMessage("Failed to retrieve profile data by profileId : " + profileId); 
              }
			
		}else {
			resp.setStatusCode(400);
			resp.setSuccess(false);
			resp.setMessage("Failed to retrieve agent data by profileId : " + profileId );
			return resp;
		
		}
		return resp;
	}
}
