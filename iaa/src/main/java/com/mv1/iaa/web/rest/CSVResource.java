package com.mv1.iaa.web.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mv1.iaa.business.component.CsvComponent;
import com.mv1.iaa.business.exception.Mv1Exception;
import com.mv1.iaa.business.view.csv.CsvResp;
import com.mv1.iaa.business.view.web.GenericResp;

@RestController
@RequestMapping("/api/csv")
public class CSVResource {

    private final Logger log = LoggerFactory.getLogger(CSVResource.class);

    @Autowired
    private CsvComponent agentCsvComponent;

    @RequestMapping(value = "/upload", 
	    method = RequestMethod.POST, 
	    headers = "content-type=multipart/form-data", 
	    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResp> uploadSessionCSV(@RequestParam("file") MultipartFile file) {

	log.debug("Csv file being uploaded is - {} ", file.getOriginalFilename());
	File convFile = null;
	GenericResp resp = new GenericResp();
	try {

	    convFile = multipartToFile(file);
	    CsvResp rec;
	    try{
	     rec = agentCsvComponent.mapAgentCsv(convFile.getAbsolutePath());
	    }catch (Mv1Exception e) {
		    e.printStackTrace();
		    resp.setStatusCode(400);
			resp.setSuccess(false);
			resp.setErrorList(e.getErrorList());
			resp.setMessage("Failed to upload file " +  file.getOriginalFilename());
		    return new ResponseEntity<GenericResp>(resp, HttpStatus.UNPROCESSABLE_ENTITY);
		} 

	    if (rec.getInsertCount() > 0 || rec.getUpdateCount() > 0) {

		StringBuilder message = new StringBuilder();
		message.append("For " + file.getOriginalFilename());
		if (rec.getInsertCount() > 0) {
		    message.append(" new " + rec.getInsertCount());
		}
		if (rec.getUpdateCount() > 0) {
		    if (rec.getInsertCount() > 0) {
			message.append(" and");
		    }
		    message.append(" update of " + rec.getUpdateCount());
		}
		message.append(" records processed successfully");

		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage(message.toString());

		return new ResponseEntity<GenericResp>(resp, HttpStatus.OK);
	    } else {
		resp.setStatusCode(400);
		resp.setSuccess(false);
		resp.setMessage("Failed to upload file " + file.getOriginalFilename() + ", Please re-verify file data");
		return new ResponseEntity<GenericResp>(resp, HttpStatus.UNPROCESSABLE_ENTITY);
	    }

	} 
	catch (Exception e) {
	    e.printStackTrace();
	    return new ResponseEntity<GenericResp>(resp, HttpStatus.UNPROCESSABLE_ENTITY);
	} finally {
	    if (convFile != null) {
		convFile.delete();
	    }
	}
    }

    @PostMapping(value = "/upload/facility", 
	    headers = "content-type=multipart/form-data", 
	    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResp> uploadFacilityCSV(@RequestParam("file") MultipartFile file) {

	log.debug("Csv file being uploaded is - {} ", file.getOriginalFilename());
	File convFile = null;
	GenericResp resp = new GenericResp();
	try {

	    convFile = multipartToFile(file);
	    CsvResp rec;
	    try{
	    	 rec = agentCsvComponent.mapFacilityCsv(convFile.getAbsolutePath());
	    	
		 }catch (Mv1Exception e) {
		    e.printStackTrace();
		    resp.setStatusCode(400);
			resp.setSuccess(false);
			resp.setErrorList(e.getErrorList());
			resp.setMessage("Failed to upload file " +  file.getOriginalFilename());
		    return new ResponseEntity<GenericResp>(resp, HttpStatus.UNPROCESSABLE_ENTITY);
		} 
	
	    if (rec.getInsertCount() > 0 || rec.getUpdateCount() > 0) {

		StringBuilder message = new StringBuilder();
		message.append("For " + file.getOriginalFilename());
		if (rec.getInsertCount() > 0) {
		    message.append(" new " + rec.getInsertCount());
		}
		if (rec.getUpdateCount() > 0) {
		    if (rec.getInsertCount() > 0) {
			message.append(" and");
		    }
		    message.append(" update of " + rec.getUpdateCount());
		}
		message.append(" records processed successfully");

		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage(message.toString());

		return new ResponseEntity<>(resp, HttpStatus.OK);
	    } else {
		resp.setStatusCode(400);
		resp.setSuccess(false);
		resp.setMessage("Failed to upload file " + file.getOriginalFilename() + ", Please re-verify file data");
		return new ResponseEntity<>(resp, HttpStatus.UNPROCESSABLE_ENTITY);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    return new ResponseEntity<>(resp, HttpStatus.UNPROCESSABLE_ENTITY);
	} finally {
	    if (convFile != null) {
		convFile.delete();
	    }
	}
    }

    @RequestMapping(value = "/agent/delete", method = RequestMethod.POST, headers = "content-type=multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericResp> deleteAgents(@RequestParam("file") MultipartFile file) {

	log.debug("Csv file being uploaded is - {} ", file.getOriginalFilename());
	File convFile = null;
	GenericResp resp = new GenericResp();
	try {

	    convFile = multipartToFile(file);
	    CsvResp rec = agentCsvComponent.deleteAgentCsv(convFile.getAbsolutePath());
	    StringBuilder message = new StringBuilder();

	    if (rec.getDeleteCount() > 0) {

		message.append(rec.getDeleteCount() + " records been deleted successfully ");
		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage(message.toString());

		return new ResponseEntity<GenericResp>(resp, HttpStatus.OK);
	    } else {
		resp.setStatusCode(400);
		resp.setSuccess(false);
		resp.setMessage("Failed to upload file " + file.getOriginalFilename() + ", Please re-verify file data");
		return new ResponseEntity<GenericResp>(resp, HttpStatus.UNPROCESSABLE_ENTITY);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    return new ResponseEntity<GenericResp>(resp, HttpStatus.UNPROCESSABLE_ENTITY);
	} finally {
	    if (convFile != null) {
		convFile.delete();
	    }
	}
    }

    public static File multipartToFile(MultipartFile file) throws IllegalStateException, IOException {
	File convFile = new File(file.getOriginalFilename());
	convFile.createNewFile();
	FileOutputStream fos = new FileOutputStream(convFile);
	fos.write(file.getBytes());
	fos.close();
	return convFile;
    }
    
    
    
    
    
    @SuppressWarnings("unchecked")
	@GetMapping(path = "/open/download/sample")
	@Produces("text/csv")
	public void downloadAgentCsvWithFilter(HttpServletResponse response,@RequestParam("type") String type) {

		try {
			File file = agentCsvComponent.downloadAgentList(type.toLowerCase());

			String mimeType = "text/csv";
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));
			response.setContentLength((int) file.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException ex) {
			log.debug("Error while downloading csv file", ex);
		}

	}

}
