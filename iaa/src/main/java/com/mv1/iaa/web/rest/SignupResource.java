package com.mv1.iaa.web.rest;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.business.view.web.req.AgentSignupVM;
import com.mv1.iaa.business.view.web.req.FacilitySignupVM;
import com.mv1.iaa.business.view.web.resp.ContactVM;
import com.mv1.iaa.business.view.web.resp.ProfileVM;
import com.mv1.iaa.domain.Agent;
import com.mv1.iaa.domain.Authority;
import com.mv1.iaa.domain.AvailableCarriers;
import com.mv1.iaa.domain.Facility;
import com.mv1.iaa.domain.Profile;
import com.mv1.iaa.domain.ProfileCarriers;
import com.mv1.iaa.domain.User;
import com.mv1.iaa.domain.enumeration.ProfileType;
import com.mv1.iaa.repository.AuthorityRepository;
import com.mv1.iaa.repository.AvailableCarriersRepository;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.CustomFacilityRepository;
import com.mv1.iaa.repository.CustomProfileRepository;
import com.mv1.iaa.repository.ProfileCarriersRepository;
import com.mv1.iaa.repository.ProfileRepository;
import com.mv1.iaa.repository.UserRepository;
import com.mv1.iaa.security.AuthoritiesConstants;
import com.mv1.iaa.service.MailService;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.UserService;
import com.mv1.iaa.service.dto.ProfileDTO;
import com.mv1.iaa.service.util.RandomUtil;
import com.mv1.iaa.web.rest.errors.EmailAlreadyUsedException;
import com.mv1.iaa.web.rest.errors.EmailMismatchException;
import com.mv1.iaa.web.rest.errors.InvalidPasswordException;
import com.mv1.iaa.web.rest.errors.LoginAlreadyUsedException;
import com.mv1.iaa.web.rest.vm.ManagedUserVM;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Agent.
 */
@RestController
@RequestMapping("/api")
public class SignupResource {

	private final Logger log = LoggerFactory.getLogger(SignupResource.class);

	@Autowired
	private ProfileService profileService;

	@Autowired
	private CustomAgentRepository customAgentRepository;

	@Autowired
	private CustomProfileRepository customProfileRepository;

	@Autowired
	private CustomFacilityRepository customFacilityRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private MailService mailService;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private CustomAgentRepository agentRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AuthorityRepository authorityRepository;
	

	@Autowired
	private AvailableCarriersRepository availableCarriersRepository;
	
	
	@Autowired
	private ProfileCarriersRepository profileCarriersRepository;
	
	/*
	 * @Autowired private CustomAgentRepository profileRepository;
	 */
	/**
	 * POST /open/agent/signup : Sign-up the user.
	 *
	 * @param agentVM the managed user View Model
	 * @return
	 * @throws Exception
	 * @throws InvalidPasswordException  400 (Bad Request) if the password is
	 *                                   incorrect
	 * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already
	 *                                   used
	 * @throws LoginAlreadyUsedException 400 (Bad Request) if the login is already
	 *                                   used
	 * @throws EmailMismatchException    400 (Bad Request) if the login is not
	 *                                   matched with business email
	 */
	@SuppressWarnings("rawtypes")
	@PostMapping("/open/agent/signup")
	@Timed
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<GenericResp> agentSignup(@Valid @RequestBody AgentSignupVM agentVM) throws Exception {
		if (!checkPasswordLength(agentVM.getPassword())) {
			throw new InvalidPasswordException();
		}
		User user = null;

		// Get Profile details by Npn
		Agent agent = agentRepository.findOneByNpn(agentVM.getNpn());
		// Get existing profile
		if (agent.getProfile() != null) {
			Profile profile = agent.getProfile();
			if (profile.getUserId() == null) {
				// Create User
				user = userService.registerUser(agentVM, agentVM.getPassword());
			} else {
				// update user
				Optional<User> savedUser = userRepository.findById(profile.getUserId());
				if(savedUser!=null && savedUser.isPresent()) {
					user = savedUser.get();
					if (agentVM.getPassword() != null) {
						String encryptedPassword = passwordEncoder.encode(agentVM.getPassword());
						user.setPassword(encryptedPassword);
					}

					user.setLogin(agentVM.getEmail().toLowerCase()==null?user.getEmail():agentVM.getEmail()); // removed username , set email to username
					// new user gets initially a generated password

					user.setFirstName(agentVM.getFirstName()==null?user.getFirstName():agentVM.getFirstName());
					user.setLastName(agentVM.getLastName()==null?user.getLastName():agentVM.getLastName());
					user.setEmail(agentVM.getEmail().toLowerCase()==null?user.getEmail():agentVM.getEmail());
					user.setImageUrl(agentVM.getImageUrl()==null?user.getImageUrl():agentVM.getImageUrl());
					user.setLangKey(agentVM.getLangKey()==null?user.getLangKey():agentVM.getLangKey());
					// new user is not active
					user.setActivated(false);
					// new user gets registration key
					user.setActivationKey(RandomUtil.generateActivationKey());

					Set<Authority> authorities = new HashSet<>();
					authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
					user.setAuthorities(authorities);
					userRepository.save(user);
				}else {
					user = userService.registerUser(agentVM, agentVM.getPassword());
				}
			
			}
		}

		ProfileDTO dto = null;
		Long pId= null;
		// Retrieve & verify if user's business email matches with signup email
//		Object[][] arr = customAgentRepository.findEmailByNpn(agentVM.getNpn());
//		if (arr != null && arr.length > 0) {
//			ContactVM cvm = null;
//			try {
//				cvm = CommonMapper.fromKeyVal(ContactVM.getKeys(), arr[0], ContactVM.class);
//
//				if (cvm != null && !user.getEmail().equalsIgnoreCase(cvm.getEmail())) {
//					throw new EmailMismatchException();
//				}
//			} catch (Exception e) {
//				log.debug("Error while signing up agent", e);
//			}
//		}

		// Get user profile by email Id
//		Object[][] parr = customProfileRepository.findAgentProfileByEmail(user.getEmail());
		boolean agentExist = false;
		if (agent.getProfile()!=null) {
			try {
				agentExist = true;
//				ProfileVM pvm = CommonMapper.fromKeyVal(ProfileVM.getKeys(), parr[0], ProfileVM.class);
				// Link the agent profile with this user
				Optional<ProfileDTO> optDto = profileService.findOne(agent.getProfile().getId());
				if (optDto.isPresent()) {
					dto = optDto.get();
					dto.setFirstName(user.getFirstName());
					dto.setLastName(user.getLastName());
					dto.setUserId(user.getId());
					dto.setPrefMobile(agentVM.getPreferredMobile());
					dto.setIsDeleted(false);
					dto.setPrefEmail(agentVM.getEmail());

					dto.setPhone(agentVM.getPhone());
					dto.setSecondPhone(agentVM.getSecondPhone());
					dto.setIsSpeaksSpanish(agentVM.getLang().equalsIgnoreCase("ES"));
					dto.setRefBy(agentVM.getRefBy());
					dto.setPrefCompany(agentVM.getPrefCompany());
					dto.setComments(agentVM.getComments());
					dto.setInsuranceCompany(agentVM.getInsuranceCompany());
					/*
					 * // Removed ref by and email dto.setRefBy(agentVM.getRefBy());
					 * 
					 * 
					 */
					pId = dto.getId();
					
					// Update profile for the activated agent
					profileService.save(dto);
				}
			} catch (Exception e) {
				log.debug("Error while linking profile with the user", e);
			}
		} else {
			// create a new profile
			dto = new ProfileDTO();
			dto.setFirstName(user.getFirstName());
			dto.setLastName(user.getLastName());
			dto.setType(ProfileType.AGENT);
			dto.setCreatedAt(ZonedDateTime.now());
			dto.setIsSpeaksSpanish(agentVM.getLang().equalsIgnoreCase("ES"));
			dto.setUpdatedAt(ZonedDateTime.now());
			dto.setUserId(user.getId());
			dto.setPrefMobile(agentVM.getPreferredMobile());
			dto.setIsDeleted(false);
			dto.setPrefEmail(agentVM.getEmail());
			dto.setPhone(agentVM.getPhone());
			dto.setSecondPhone(agentVM.getSecondPhone());
			dto.setRefBy(agentVM.getRefBy());
			dto.setPrefCompany(agentVM.getPrefCompany());
			dto.setComments(agentVM.getComments());
			dto.setInsuranceCompany(agentVM.getInsuranceCompany());
			/*
			 * // Removed ref by and email dto.setRefBy(agentVM.getRefBy());
			 * 
			 * 
			 */

			// Update profile for the activated agent
			dto = profileService.save(dto);
			
			pId = dto.getId();
		}

		Long agentProfile = customAgentRepository.findByEmailAndUseId(agentVM.getEmail(), user.getId());
		if (agentProfile != null && !agentExist) {
			try {
				// ProfileVM pvm = CommonMapper.fromKeyVal(ProfileVM.getKeys(),
				// parr[0], ProfileVM.class);
				// Link the agent profile with this user
				// Optional<ProfileDTO> optDto =
				// profileService.findOne(pvm.getProfileId());
				Profile profile = new Profile();
				agent.setNpn(agentVM.getNpn());

				profile.setFirstName(agentVM.getFirstName());
				profile.setLastName(agentVM.getLastName());
				profile.setRefBy(agentVM.getRefBy());
				profile.setId(agentProfile);
				profile.setPrefCompany(agentVM.getPrefCompany());
				profile.setComments(agentVM.getComments());
				profile.setInsuranceCompany(agentVM.getInsuranceCompany());
				agent.setProfile(profile);
				agent.setIsActive(true);
				Agent agentData = agentRepository.save(agent);
				// if(agentData.isIsActive()){
				// throw new Mv1Exception("Failed to save agent details");
				// }

			} catch (Exception e) {
				log.debug("Error while linking profile with the user", e);
			}
		}

		
		List<ProfileCarriers> cars = profileCarriersRepository.findAllByProfileId(pId);
		for (ProfileCarriers c : cars) {
			if(!c.getIsPrimary()) {
				profileCarriersRepository.delete(c);
			}
		
		}
		
		for (Long carrier : agentVM.getCarriers()) {
			if(carrier!=null) {
				Optional<AvailableCarriers> c=  availableCarriersRepository.findById(carrier);
				if(c.isPresent() && c!=null) {
					ProfileCarriers pc = new ProfileCarriers();
					pc.setCarriersId(carrier);
					pc.setProfileId(pId);
					pc.setIsPrimary(false);
					profileCarriersRepository.save(pc);
				}
			}
		}
		

		AvailableCarriers  carr = availableCarriersRepository.findByCarrierAndShow(agentVM.getInsuranceCompany(),false);
		if(carr == null) {
			AvailableCarriers c = new AvailableCarriers();
			c.setCarrier(agentVM.getInsuranceCompany());
			c.setShowInList(false);
			availableCarriersRepository.save(c);
		}
		
		carr = availableCarriersRepository.findByCarrierAndShow(agentVM.getInsuranceCompany(),false);
		if(carr!=null) {
			ProfileCarriers pc = profileCarriersRepository.findPrimaryCompanyOfProfile(pId);
			if(pc == null) {
				 pc = new ProfileCarriers();
			}
//			ProfileCarriers pc = new ProfileCarriers();
			pc.setProfileId(pId);
			pc.setCarriersId(carr.getId());
			pc.setIsPrimary(true);
			profileCarriersRepository.save(pc);
		}

		
		user.setLangKey(agentVM.getLang());
		mailService.sendActivationEmail(user);

		return ResponseUtil.wrapOrNotFound(getResp(user, dto, "signing"));
	}

	/**
	 * GET /open/agent/activate : activate the registered user.
	 *
	 * @param key the activation key
	 * @return
	 * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be
	 *                          activated
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping("/open/agent/activate")
	@Timed
	public ResponseEntity<GenericResp> activateAccount(@RequestParam(value = "key") String key) {
		Optional<User> user = userService.activateRegistration(key);
		User u = null;
		ProfileDTO dto = null;
		if (user.isPresent()) {
			u = user.get();
			// Get user profile by email Id
			Object[][] arr = customProfileRepository.findAgentProfileByEmail(u.getEmail());
			if (arr != null && arr.length > 0) {
				try {
					ProfileVM pvm = CommonMapper.fromKeyVal(ProfileVM.getKeys(), arr[0], ProfileVM.class);
					// Link the agent profile with this user'
					Optional<ProfileDTO> optDto = profileService.findOne(pvm.getProfileId());
					if (optDto.isPresent()) {
						dto = optDto.get();
						dto.setUserId(u.getId());
						dto.setIsActive(true);
						dto.setUpdatedAt(ZonedDateTime.now());
						dto.setType(ProfileType.AGENT);
						// Update profile for the activated agent
						profileService.save(dto);
					}
				} catch (Exception e) {
					log.debug("Error while linking profile with the user", e);
				}
			} else {
				dto = new ProfileDTO();
				dto.setType(ProfileType.AGENT);
			}
		} else {
			dto = new ProfileDTO();
			dto.setType(ProfileType.AGENT);
		}

		return ResponseUtil.wrapOrNotFound(getResp(u, dto, "activating"));
	}

	@SuppressWarnings("rawtypes")
	private Optional<GenericResp> getResp(User u, ProfileDTO dto, String action) {
		String name = "";
		if (dto != null && ProfileType.AGENT.equals(dto.getType())) {
			name = "Agent";
		} else {
			name = "Facility";
		}

		GenericResp resp = new GenericResp();
		boolean status = u != null && u.getId() != null;
		resp.setSuccess(status);
		if (status) {
			resp.setMessage(action + " " + name + " was successful.");
			resp.setStatusCode(200);
		} else {
			resp.setMessage("Error while " + action + " " + name + ", Please try again.");
			resp.setStatusCode(400);
		}

		return Optional.of(resp);
	}

	private static boolean checkPasswordLength(String password) {
		return !StringUtils.isEmpty(password) && password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH
				&& password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
	}

	/**
	 * POST /open/facility/signup : Sign-up the facility.
	 *
	 * @param facilityVM the managed user View Model
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@PostMapping("/open/facility/signup")
	@Timed
	public ResponseEntity<GenericResp> facilitySignup(@Valid @RequestBody FacilitySignupVM facilityVM)
			throws Exception {

		if (!checkPasswordLength(facilityVM.getPassword())) {
			throw new InvalidPasswordException();
		}

		User user = userService.registerUser(facilityVM, facilityVM.getPassword());
		ProfileDTO dto = null;

		// Retrieve & verify if user's business email matches with signup email
		Object[][] arr = customFacilityRepository.findEmailByIsn(facilityVM.getIsn());
		if (arr != null && arr.length > 0) {
			ContactVM cvm = null;
			try {
				cvm = CommonMapper.fromKeyVal(ContactVM.getKeys(), arr[0], ContactVM.class);

				if (cvm != null && cvm.getEmail() != null && !user.getEmail().equalsIgnoreCase(cvm.getEmail())) {
					throw new EmailMismatchException();
				}
			} catch (Exception e) {
				log.debug("Error while signing up agent", e);
			}
		}
		boolean facilityExist = false;
		// Get user profile by email Id
		// Object[][] parr =
		// customProfileRepository.findFacilityProfileByEmail(user.getEmail());

		String pId = customFacilityRepository.findProfileIdByIsn(facilityVM.getIsn());
		Object[][] parr = customProfileRepository.findFacilityProfileById(pId);
		if (parr != null && parr.length > 0) {
			try {
				facilityExist = true;
				ProfileVM pvm = CommonMapper.fromKeyVal(ProfileVM.getKeys(), parr[0], ProfileVM.class);
				// Link the agent profile with this user
				Optional<ProfileDTO> optDto = profileService.findOne(pvm.getProfileId());
				if (optDto.isPresent()) {
					dto = optDto.get();
					dto.setType(ProfileType.FACILITY);
					dto.setUserId(user.getId());
					dto.setFirstName(user.getFirstName());
					dto.setLastName(user.getLastName());
					dto.setType(ProfileType.FACILITY);
					dto.setCreatedAt(ZonedDateTime.now());
					dto.setIsSpeaksSpanish(facilityVM.getLang().equalsIgnoreCase("SPANISH"));
					dto.setUpdatedAt(ZonedDateTime.now());
					// dto.setPrefEmail(facilityVM.getPreferredEmail());
					dto.setPrefMobile(facilityVM.getPreferredMobile());
					dto.setRefBy(facilityVM.getRefBy());
					dto.setIsDeleted(false);
					dto.setPrefEmail(facilityVM.getEmail());
					dto.setPhone(facilityVM.getPhone());
					dto.setPrefCompany(facilityVM.getPrefCompany());
					dto.setComments(facilityVM.getComments());
//					dto.setMechDuty(facilityVM.getMechDuty());
					// Update profile for the activated agent
					profileService.save(dto);
				}
			} catch (Exception e) {
				log.debug("Error while linking profile with the user", e);
			}
		} else {
			// create a new profile
			dto = new ProfileDTO();
			dto.setFirstName(user.getFirstName());
			dto.setLastName(user.getLastName());
			dto.setType(ProfileType.FACILITY);
			dto.setCreatedAt(ZonedDateTime.now());
			dto.setIsSpeaksSpanish(facilityVM.getLang().equalsIgnoreCase("SPANISH"));
			dto.setUpdatedAt(ZonedDateTime.now());
			dto.setUserId(user.getId());
			// dto.setPrefEmail(facilityVM.getPreferredEmail());
			dto.setPrefMobile(facilityVM.getPreferredMobile());
			dto.setRefBy(facilityVM.getRefBy());
			dto.setIsDeleted(false);
			dto.setPrefEmail(facilityVM.getEmail());
			dto.setPhone(facilityVM.getPhone());
			dto.setPrefCompany(facilityVM.getPrefCompany());
			dto.setComments(facilityVM.getComments());
//			dto.setMechDuty(facilityVM.getMechDuty());

			// Update profile for the activated agent
			profileService.save(dto);
		}

		// Long facilityrofile =
		// customFacilityRepository.findByEmailAndUseId(facilityVM.getEmail(),user.getId());

		Long facilityrofile = customAgentRepository.findByEmailAndUseId(facilityVM.getEmail(), user.getId());
		if (facilityrofile != null && !facilityExist) {
			try {
				// ProfileVM pvm = CommonMapper.fromKeyVal(ProfileVM.getKeys(),
				// parr[0], ProfileVM.class);
				// Link the agent profile with this user
				// Optional<ProfileDTO> optDto =
				// profileService.findOne(pvm.getProfileId());
				Facility facility = customFacilityRepository.findOneByProfileId(Long.parseLong(pId));
				Profile profile = profileRepository.getOne(Long.parseLong(pId));
				facility.setIsn(facilityVM.getIsn());
				profile.setFirstName(facilityVM.getFirstName());
				profile.setLastName(facilityVM.getLastName());
				profile.setId(facilityrofile);
				profile.setPrefCompany(facilityVM.getPrefCompany());
				profile.setComments(facilityVM.getComments());
//				profile.setMechDuty(facilityVM.getMechDuty());
				facility.setProfile(profile);
				facility.setMechDuty(facilityVM.getMechDuty());
				facility.setIsActive(true);
				Facility facilityData = customFacilityRepository.save(facility);

			} catch (Exception e) {
				log.debug("Error while linking profile with the user", e);
			}
		}

		user.setLangKey(facilityVM.getLang());
		mailService.sendActivationEmail(user);

		return ResponseUtil.wrapOrNotFound(getResp(user, dto, "signing"));
	}

	/**
	 * GET /open/facility/activate : activate the registered user.
	 *
	 * @param key the activation key
	 * @return
	 * @throws RuntimeException 500 (Internal Server Error) if the user couldn't be
	 *                          activated
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping("/open/facility/activate")
	@Timed
	public ResponseEntity<GenericResp> activateFacility(@RequestParam(value = "key") String key) {
		Optional<User> user = userService.activateRegistration(key);
		User u = null;
		ProfileDTO dto = null;

		if (user.isPresent()) {
			u = user.get();
			// Get user profile by email Id
			Object[][] arr = customProfileRepository.findFacilityProfileByEmail(u.getEmail());
			if (arr != null && arr.length > 0) {
				try {
					ProfileVM pvm = CommonMapper.fromKeyVal(ProfileVM.getKeys(), arr[0], ProfileVM.class);
					// Link the agent profile with this user
					Optional<ProfileDTO> optDto = profileService.findOne(pvm.getProfileId());
					if (optDto.isPresent()) {
						dto = optDto.get();
						dto.setUserId(u.getId());
						dto.setIsActive(true);
						dto.setUpdatedAt(ZonedDateTime.now());

						// Update profile for the activated agent
						profileService.save(dto);
					}
				} catch (Exception e) {
					log.debug("Error while linking profile with the user", e);
				}
			}
		}

		return ResponseUtil.wrapOrNotFound(getResp(u, dto, "activating"));
	}

	@SuppressWarnings("rawtypes")
	@GetMapping("/open/isExist/npn")
	@Timed
	public ResponseEntity<GenericResp> checkNPN(@RequestParam(value = "npn") String npn) {
		GenericResp resp = new GenericResp();
		if (npn.length() < 4) {
			resp.setSuccess(false);
			resp.setMessage("Invalid NPN length.");
			resp.setStatusCode(200);
			return ResponseUtil.wrapOrNotFound(Optional.of(resp));
		}
    if(!npn.isEmpty() && (npn.equals("0000000") || npn.equals("0000001")) )
    {
    	    resp.setSuccess(false);
			resp.setMessage("NPN not verified.");
			resp.setStatusCode(400);
     }
     else
     {
     
		Agent agent = customAgentRepository.findOneByNpn(npn);

		if (agent != null) {
			resp.setSuccess(true);
			resp.setMessage("NPN verified.");
			resp.setStatusCode(200);
		} else {
			resp.setSuccess(false);
			resp.setMessage("NPN not verified.");
			resp.setStatusCode(400);
		}
     }
		return ResponseUtil.wrapOrNotFound(Optional.of(resp));
		// return resp;
	}

	@SuppressWarnings("rawtypes")
	@PostMapping("/open/agent/details")
	@Timed
	public ResponseEntity<GenericResp> verifyAgentDetails(@RequestBody AgentSignupVM agentVM) {
		Agent existingAgent = customAgentRepository.findOneByNpn(agentVM.getNpn());
		GenericResp resp = new GenericResp();
		if (existingAgent != null) {
			if (existingAgent.getProfile().getFirstName() != null && agentVM.getFirstName() != null
					&& agentVM.getFirstName() != ""
					&& !agentVM.getFirstName().equalsIgnoreCase(existingAgent.getProfile().getFirstName())) {
				resp.setSuccess(false);
				resp.setStatusCode(400);
				resp.setMessage("First Name does not matches as per NPN record.");
				return ResponseUtil.wrapOrNotFound(Optional.of(resp));
			}
			if (existingAgent.getProfile().getLastName() != null && agentVM.getLastName() != null
					&& agentVM.getLastName() != ""
					&& !agentVM.getLastName().equalsIgnoreCase(existingAgent.getProfile().getLastName())) {
				resp.setSuccess(false);
				resp.setStatusCode(400);
				resp.setMessage("Last Name does not matches as per NPN record.");
				return ResponseUtil.wrapOrNotFound(Optional.of(resp));
			}
			if (existingAgent.getProfile().getPrefEmail() != null && agentVM.getEmail() != null
					&& agentVM.getEmail() != ""
					&& !agentVM.getEmail().equalsIgnoreCase(existingAgent.getProfile().getPrefEmail())) {
				resp.setSuccess(false);
				resp.setStatusCode(400);
				resp.setMessage("Email Id does not matches as per NPN record.");
				return ResponseUtil.wrapOrNotFound(Optional.of(resp));
			}

		}
		resp.setSuccess(true);
		resp.setStatusCode(200);
		resp.setMessage("Agent details matches as per NPN record.");
		return ResponseUtil.wrapOrNotFound(Optional.of(resp));
	}

	@SuppressWarnings("rawtypes")
	@GetMapping("/open/isExist/isn")
	@Timed
	public ResponseEntity<GenericResp> checkISN(@RequestParam(value = "isn") String isn) {
		GenericResp resp = new GenericResp();
		/*
		 * if(isn.length()<4){ resp.setSuccess(false);
		 * resp.setMessage("Invalid ISN length."); resp.setStatusCode(200); return
		 * ResponseUtil.wrapOrNotFound(Optional.of(resp)); }
		 */
		Facility facility = customFacilityRepository.findOneByIsn(isn);

		if (facility != null) {
			resp.setSuccess(true);
			resp.setMessage("ISN verified.");
			resp.setStatusCode(200);
		} else {
			resp.setSuccess(false);
			resp.setMessage("ISN not verified.");
			resp.setStatusCode(400);
		}
		return ResponseUtil.wrapOrNotFound(Optional.of(resp));

	}

	@SuppressWarnings("rawtypes")
	@PostMapping("/open/facility/details")
	@Timed
	public ResponseEntity<GenericResp> verifyFacilityDetails(@RequestBody FacilitySignupVM facilityVM) {
		Facility existingFaciliy = customFacilityRepository.findOneByIsn(facilityVM.getIsn());
		GenericResp resp = new GenericResp();
		if (existingFaciliy != null) {
			if (existingFaciliy.getProfile().getFirstName() != null && facilityVM.getFirstName() != null
					&& facilityVM.getFirstName() != ""
					&& !facilityVM.getFirstName().equalsIgnoreCase(existingFaciliy.getProfile().getFirstName())) {
				resp.setSuccess(false);
				resp.setStatusCode(400);
				resp.setMessage("First Name does not matches as per ISN record.");
				return ResponseUtil.wrapOrNotFound(Optional.of(resp));
			}
			if (existingFaciliy.getProfile().getLastName() != null && facilityVM.getLastName() != null
					&& facilityVM.getLastName() != ""
					&& !facilityVM.getLastName().equalsIgnoreCase(existingFaciliy.getProfile().getLastName())) {
				resp.setSuccess(false);
				resp.setStatusCode(400);
				resp.setMessage("Last Name does not matches as per ISN record.");
				return ResponseUtil.wrapOrNotFound(Optional.of(resp));
			}
			if (existingFaciliy.getProfile().getPrefEmail() != null && facilityVM.getEmail() != null
					&& facilityVM.getEmail() != ""
					&& !facilityVM.getEmail().equalsIgnoreCase(existingFaciliy.getProfile().getPrefEmail())) {
				resp.setSuccess(false);
				resp.setStatusCode(400);
				resp.setMessage("Email Id does not matches as per ISN record.");
				return ResponseUtil.wrapOrNotFound(Optional.of(resp));
			}

		}
		resp.setSuccess(true);
		resp.setStatusCode(200);
		resp.setMessage("Agent details matches as per NPN record.");
		return ResponseUtil.wrapOrNotFound(Optional.of(resp));
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/open/get/available/carriers")
	@Timed
	public ResponseEntity<GenericResp> getAVailableCarriers() {
		GenericResp resp = new GenericResp();
		/*
		 * if(isn.length()<4){ resp.setSuccess(false);
		 * resp.setMessage("Invalid ISN length."); resp.setStatusCode(200); return
		 * ResponseUtil.wrapOrNotFound(Optional.of(resp)); }
		 */
		List<AvailableCarriers> carriers = availableCarriersRepository.findAllShowInList(true);
		

		resp.setValue(carriers);
		resp.setSuccess(true);
		resp.setMessage("ISN verified.");
		resp.setStatusCode(200);
		
		return ResponseUtil.wrapOrNotFound(Optional.of(resp));

	}
	
}
