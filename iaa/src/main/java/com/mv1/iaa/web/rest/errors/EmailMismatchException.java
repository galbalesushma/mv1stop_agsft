package com.mv1.iaa.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class EmailMismatchException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public EmailMismatchException() {
        super(ErrorConstants.EMAIL_MISMATCH_TYPE, "Business email is different", Status.BAD_REQUEST);
    }
}
