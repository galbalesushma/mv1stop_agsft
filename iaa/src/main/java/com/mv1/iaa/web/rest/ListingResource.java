package com.mv1.iaa.web.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.common.Util;
import com.mv1.iaa.business.component.AgentComponent;
import com.mv1.iaa.business.component.FacillityComponent;
import com.mv1.iaa.business.view.web.req.AgentVM;
import com.mv1.iaa.business.view.web.req.FacilityVM;
import com.mv1.iaa.business.view.web.resp.Page;
import com.mv1.iaa.repository.CustomZipAreaRepository;
import com.mv1.iaa.service.BusinessService;
import com.mv1.iaa.service.CityService;
import com.mv1.iaa.service.StateService;
import com.mv1.iaa.service.ZipAreaService;
import com.mv1.iaa.service.dto.BusinessDTO;
import com.mv1.iaa.service.dto.CityDTO;
import com.mv1.iaa.service.dto.StateDTO;
import com.mv1.iaa.service.dto.ZipAreaDTO;
import com.mv1.iaa.service.mapper.ZipAreaMapper;

/**
 * REST controller for managing Business.
 */
@RestController
@RequestMapping("/api")
public class ListingResource<T> {

    private final Logger log = LoggerFactory.getLogger(ListingResource.class);

    @Autowired
    private BusinessService businessService;
    
    @Autowired
    private ZipAreaService zipAreaService;
    
    @Autowired
    private CustomZipAreaRepository customZipAreaRepository;
    
    @Autowired
    private StateService stateService;
    
    @Autowired
    private CityService cityService;
    
    @Autowired
    private ZipAreaMapper zipAreaMapper;
    
    @Autowired
    private AgentComponent agentComponent;
    
    @Autowired
    private FacillityComponent facillityComponent;

    /**
     * GET  /businesses : get all the businesses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of businesses in body
     */
    @GetMapping("/business/all")
    @Timed
    public List<BusinessDTO> getAllBusinesses() {
        log.debug("REST request to get all Businesses");
        return businessService
                    .findAll()
                    .stream()
                    .filter(b -> !b.getBusinessName().isEmpty() )
                    .collect(Collectors.toList());
    }
    
    /**
     * GET  /zip-areas : get all the zipAreas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of zipAreas in body
     */
    @GetMapping("/zip-area/all")
    @Timed
    public List<ZipAreaDTO> getAllZipAreas() {
        log.debug("REST request to get all ZipAreas");
        return zipAreaService.findAll();
    }
    
    /**
     * GET  /zip-areas : get all the zipAreas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of zipAreas in body
     */
    @GetMapping("/zip-area/get/{zip}")
    @Timed
    public ZipAreaDTO getZipArea(@PathVariable("zip") String zip) {
        log.debug("REST request to get ZipArea");
        return zipAreaMapper.toDto(customZipAreaRepository.findOneByZip(zip));
    }
    
    /**
     * GET  /states : get all the states.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of states in body
     */
    @GetMapping("/state/all")
    @Timed
    public List<StateDTO> getAllStates() {
        log.debug("REST request to get all States");
        return stateService.findAll();
    }
    
    /**
     * GET  /cities : get all the cities.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cities in body
     */
    @GetMapping("/city/all")
    @Timed
    public List<CityDTO> getAllCities() {
        log.debug("REST request to get all Cities");
        return cityService.findAll();
    }
    
    /**
     * GET /agents : get all the agents.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of agents in
     *         body
     */
    @SuppressWarnings("unchecked")
    @GetMapping("/agent/all/{page}")
    @Timed
    public Page<T> getAllAgents(@PathVariable("page") Integer page,@PathVariable("pageSize") Integer pageSize
    		,@PathVariable("isCustom") boolean isCustom) {

        log.debug("REST request to get all Agents");

        List<AgentVM> list = agentComponent.getAllAgents();
        int size = list.size();
        // Util.paginate(agents, page)
        Page<T> pageView = new Page<>();
        pageView.setResult((List<T>) Util.paginate(list, page,pageSize,isCustom));
        pageView.setTotal(size);

        return pageView;
    }

    /**
     * GET  /facility/all : get all the facilities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of facilities in body
     */
    @GetMapping("/facility/all/{page}/{pageSize}/{isCustom}")
    @Timed
    public Page<FacilityVM> getAllFacilities(@PathVariable("page") Integer page,@PathVariable("pageSize") Integer pageSize
    		,@PathVariable("isCustom") boolean isCustom) {
        log.debug("REST request to get a page of Facilities");
        Page<FacilityVM> pageResp = new Page<>();
        List<FacilityVM> list = facillityComponent.getAllFacilities();
        int size = list.size();
        
        if(list.isEmpty()) {
            pageResp.setResult(list);
            pageResp.setTotal(size);
        } else {
            pageResp.setResult(Util.paginate(list, page,pageSize,isCustom));
            pageResp.setTotal(size);
        }
        return pageResp;
    }

}
