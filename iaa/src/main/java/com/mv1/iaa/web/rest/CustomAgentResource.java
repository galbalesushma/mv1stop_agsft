package com.mv1.iaa.web.rest;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.itextpdf.text.log.SysoCounter;
import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.component.AgentComponent;
import com.mv1.iaa.business.exception.DeleteException;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.business.view.web.req.AgentIdVM;
import com.mv1.iaa.business.view.web.req.AgentVM;
import com.mv1.iaa.business.view.web.req.ProfileVm;
import com.mv1.iaa.domain.Agent;
import com.mv1.iaa.domain.AvailableCarriers;
import com.mv1.iaa.domain.City;
import com.mv1.iaa.domain.County;
import com.mv1.iaa.domain.Payment;
import com.mv1.iaa.domain.Profile;
import com.mv1.iaa.domain.ProfileCarriers;
import com.mv1.iaa.domain.State;
import com.mv1.iaa.domain.User;
import com.mv1.iaa.repository.AvailableCarriersRepository;
import com.mv1.iaa.repository.CityRepository;
import com.mv1.iaa.repository.CountyRepository;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.CustomProfileRepository;
import com.mv1.iaa.repository.PaymentRepository;
import com.mv1.iaa.repository.ProfileCarriersRepository;
import com.mv1.iaa.repository.StateRepository;
import com.mv1.iaa.repository.UserRepository;
import com.mv1.iaa.service.PBXIntegrationService;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.UserService;
import com.mv1.iaa.service.dto.ProfileDTO;
import com.mv1.iaa.web.rest.vm.SearchInsuranceVM;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Agent.
 */
@RestController
@RequestMapping("/api")
public class CustomAgentResource<T> {

	private final Logger log = LoggerFactory.getLogger(CustomAgentResource.class);

	@Autowired
	private AgentComponent agentComponent;

	@Autowired
	private CustomAgentRepository customAgentRepository;

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private StateRepository stateRepository;

	@Autowired
	private CustomProfileRepository customProfileRepository;

	@Autowired
	private CountyRepository countyRepository;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PBXIntegrationService pbxIntegrationService;

	@Autowired
	private ProfileCarriersRepository profileCarriersRepository;

	@Autowired
	private AvailableCarriersRepository availableCarriersRepository;

	@Autowired
	private PaymentRepository paymentRepository;

	/**
	 * POST /agent : Create a new agent.
	 *
	 * @param agent the agentDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         agentDTO, or with status 400 (Bad Request) if the agent has already
	 *         an ID
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@PostMapping("/agent/enroll")
	@Timed
	public ResponseEntity<GenericResp> createAgent(@RequestBody AgentVM agent) throws Exception {
		log.debug("REST request to save Agent : {}", agent);

		GenericResp resp = new GenericResp();
		boolean status = agentComponent.enrollAgent(agent);
		resp.setSuccess(status);
		if (status) {
			resp.setMessage("Agent details saved successfully");
			resp.setStatusCode(200);
		} else {
			resp.setMessage("Error while saving agent details, Please try again.");
			resp.setStatusCode(400);
		}
		log.debug("REST response of save Agent : {}", resp);
		Optional<GenericResp> respOpt = Optional.of(resp);
		return ResponseUtil.wrapOrNotFound(respOpt);
	}

	/**
	 * POST /agent : Create a new agent.
	 *
	 * @param agent the agentDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         agentDTO, or with status 400 (Bad Request) if the agent has already
	 *         an ID
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@PostMapping("/agent/edit")
	@Timed
	public ResponseEntity<GenericResp> editAgent(@RequestBody AgentVM agent) throws Exception {
		log.debug("REST request to edit Agent : {}", agent);

		GenericResp resp = new GenericResp();
		boolean status = agentComponent.editAgent(agent);
		resp.setSuccess(status);
		if (status) {
			resp.setMessage("Agent details are edited successfully");
			resp.setStatusCode(200);
		} else {
			resp.setMessage("Error while editing agent details, Please try again.");
			resp.setStatusCode(400);
		}
		log.debug("REST response of edit Agent : {}", resp);
		Optional<GenericResp> respOpt = Optional.of(resp);
		return ResponseUtil.wrapOrNotFound(respOpt);
	}

	/**
	 * GET /agent/{npn} : get the agents by its NPN .
	 *
	 * @return the GenericResp and the agent in body
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/agent/get/{npn}")
	@Timed
	public GenericResp getAgent(@PathVariable("npn") String npn) {

		log.debug("REST request to get Agent by NPN : " + npn);
		GenericResp resp = new GenericResp();
		if (npn == null) {
			resp.setStatusCode(400);
			resp.setSuccess(false);
			resp.setMessage("Failed to retrieve agent data, Please try again.");
			return resp;
		}
		AgentVM vm = agentComponent.getAgent(npn);
		if (vm != null) {
			if (vm.getpCity() != null) {
				Optional<City> pCity = cityRepository.findById(vm.getpCity());
				vm.setpCityName(pCity.get().getName());
			}

			if (vm.getpState() != null) {
				Optional<State> pState = stateRepository.findById(vm.getpState());
				vm.setpStateName(pState.get().getName());
			}
			if (vm.getbState() != null) {
				Optional<State> bState = stateRepository.findById(vm.getbState());
				vm.setbStateName(bState.get().getName());
			}

			if (vm.getbCity() != null) {
				Optional<City> bCity = cityRepository.findById(vm.getbCity());
				vm.setbCityName(bCity.get().getName());
			}

			if (vm.getbCounty() != null) {
				Optional<County> bCounty = countyRepository.findById(vm.getbCounty());
				vm.setbCountyName(bCounty.get().getName());
			}

			if (vm.getpCounty() != null) {
				Optional<County> pCounty = countyRepository.findById(vm.getpCounty());
				vm.setpCountyName(pCounty.get().getName());
			}

			if (vm.getDomState() != null) {
				Optional<State> dState = stateRepository.findById(vm.getDomState());
				vm.setdStateName(dState.get().getName());
			}

			System.out.println("vm--------------------------" + vm.getbMobile());
			if (vm.getSpeaksSpanish() == null || vm.getSpeaksSpanish() == false) {
				vm.setSpeaksSpanish(false);
			} else {
				vm.setSpeaksSpanish(true);
			}

			if (vm.getbMobile() == null || vm.getbMobile() == "") {
				vm.setbMobile("");
			} else {
				vm.setbMobile(vm.getbMobile());
			}

			Agent agent = customAgentRepository.findOneByNpn(npn);

//			Profile profile = customProfileRepository.findProfileByPrefEmail(vm.getPrefEmail());
			Profile profile = customProfileRepository.findById(agent.getProfile().getId()).get();

			if (profile != null) {
				vm.setPrefPhone(profile.getPhone());
				vm.setPrefMobile(profile.getPrefMobile());
				vm.setSecondPhone(profile.getSecondPhone());
				vm.setPrefEmail(profile.getPrefEmail());
				vm.setProfileId(profile.getId());
				vm.setPrefCompany(profile.getPrefCompany());
				vm.setComments(profile.getComments());
				vm.setInsuranceCompany(profile.getInsuranceCompany());

				ProfileCarriers pc = profileCarriersRepository.findPrimaryCompanyOfProfile(profile.getId());
				if (pc != null) {
					Optional<AvailableCarriers> c = availableCarriersRepository.findById(pc.getCarriersId());
					if (c.isPresent()) {
						vm.setInsuranceCompany(c.get().getCarrier());
					}

				}

				List<ProfileCarriers> ProfileCarriers = profileCarriersRepository.findAllByProfileId(profile.getId());
//				List<Long> clist = pr.ofileCarriersRepository.getProfileCarriers(profile.getId());
				List<Long> carriers = new ArrayList<>();

				Long paymentStatus = paymentRepository.findPaymentStatusByprofile(profile.getId());
				int primary = 0;
				int count = 0;

				if (paymentStatus != null && paymentStatus >= 0) {

					for (ProfileCarriers l1 : ProfileCarriers) {
						if (l1.getIsPrimary()) {
							primary = 1;
						} else {
							carriers.add(l1.getCarriersId());
							count++;
						}

					}

					if (ProfileCarriers.size() > 0) {
						if (count > 0) {
							if (primary == 0 && ProfileCarriers.get(0).getUpdatedDate()!=null) {
								int diff = (int) ((new Date().getTime()
										- ProfileCarriers.get(0).getUpdatedDate().getTime()) / (1000 * 60 * 60 * 24));
								if (diff > 90) {
									resp.setValidToUpdateCarrier(true);
								}
							} else if (primary == 1 && ProfileCarriers.get(count).getUpdatedDate()!=null) {
								int diff1 = (int) ((new Date().getTime()
										- ProfileCarriers.get(count).getUpdatedDate().getTime())
										/ (1000 * 60 * 60 * 24));
								if (diff1 > 90) {
									resp.setValidToUpdateCarrier(true);
								}
							}
						} else {
							resp.setValidToUpdateCarrier(true);
						}
					} else {
						resp.setValidToUpdateCarrier(true);
					}

				} else {
					for (ProfileCarriers l1 : ProfileCarriers) {
						if (!l1.getIsPrimary()) {

							carriers.add(l1.getCarriersId());
						}
					}
					resp.setValidToUpdateCarrier(true);
				}

				/*
				 * int primary= 0; int count = 0; for (ProfileCarriers l1 : ProfileCarriers) {
				 * if (l1.getIsPrimary()) { primary=1; } else { count ++; }
				 * carriers.add(l1.getCarriersId()); } if (ProfileCarriers.size() > 0) { if
				 * (count>0 ) { if(primary==0) { int diff = (int) ((new Date().getTime() -
				 * ProfileCarriers.get(0).getUpdatedDate().getTime()) / (1000 * 60 * 60 * 24));
				 * if (diff > 90) { resp.setValidToUpdateCarrier(true); } } else if(primary==1)
				 * { int diff1 = (int) ((new Date().getTime() -
				 * ProfileCarriers.get(count).getUpdatedDate().getTime()) / (1000 * 60 * 60 *
				 * 24)); if (diff1 > 90) { resp.setValidToUpdateCarrier(true); } } } else {
				 * resp.setValidToUpdateCarrier(true); } } else {
				 * resp.setValidToUpdateCarrier(true); }
				 */

				vm.setCarriers(carriers);

			}

			resp.setValue(vm);
			resp.setStatusCode(200);
			resp.setSuccess(true);
			resp.setMessage("Retrieve the Agent by NPN : " + npn);
		} else {
			resp.setStatusCode(400);
			resp.setSuccess(false);
			resp.setValidToUpdateCarrier(false);
			resp.setMessage("Failed to retrieve agent data by NPN : " + npn + ", Please try again.");
			return resp;
		}
		return resp;
	}

	/**
	 * POST /agents : Delete multiple agents.
	 * 
	 * @param agentIds the agent ID's to delete
	 * @return the ResponseEntity with status 200 (OK-Deleted) and message, or with
	 *         status 400 (Bad Request) if the failed to delete one of more
	 *         agent(s).
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@SuppressWarnings("rawtypes")
	@PostMapping(path = { "/agent/delete" }, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@Timed
	public ResponseEntity<GenericResp> removeAgent(@RequestBody AgentIdVM agentIdVM) throws URISyntaxException {
		log.debug("REST request to delete Agents : {}", agentIdVM);

		GenericResp resp = new GenericResp();
		try {
			agentComponent.deleteAgent(new TreeSet<String>(agentIdVM.getAgentNpn()));
			resp.setSuccess(true);
			resp.setMessage("Agents removed successfully");
			resp.setStatusCode(200);
		} catch (DeleteException cause) {
			resp.setMessage("Error while deleting agents, please try again. Error is : " + cause.getMessage());
			resp.setStatusCode(400);
		}
		log.debug("REST response of delete Agent : {}", resp);
		Optional<GenericResp> respOpt = Optional.of(resp);
		return ResponseUtil.wrapOrNotFound(respOpt);
	}

	/**
	 * GET /agent/{npn} : get the agents by its NPN .
	 *
	 * @return the GenericResp and the agent in body
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/agentProfile/get/{profileId}")
	@Timed
	public GenericResp getAgentByProfileId(@PathVariable("profileId") Long profileId) {

		log.debug("REST request to get Agent by profileId : " + profileId);
		GenericResp resp = new GenericResp();
		if (profileId == null) {
			resp.setStatusCode(400);
			resp.setSuccess(false);
			resp.setMessage("Failed to retrieve agent data, Please try again.");
			return resp;
		}
		AgentVM vm = agentComponent.getAgentByProfileId(profileId);
		if (vm != null) {

			if (vm.getpCity() != null) {
				Optional<City> pCity = cityRepository.findById(vm.getpCity());
				vm.setpCityName(pCity.get().getName());
			}

			if (vm.getpState() != null) {
				Optional<State> pState = stateRepository.findById(vm.getpState());
				vm.setpStateName(pState.get().getName());
			}
			if (vm.getbState() != null) {
				Optional<State> bState = stateRepository.findById(vm.getbState());
				vm.setbStateName(bState.get().getName());
			}
			if (vm.getbCity() != null) {
				Optional<City> bCity = cityRepository.findById(vm.getbCity());
				vm.setbCityName(bCity.get().getName());
			}

			if (vm.getSpeaksSpanish() == null || vm.getSpeaksSpanish() == false) {
				vm.setSpeaksSpanish(false);
			} else {
				vm.setSpeaksSpanish(true);
			}

			if (vm.getbMobile() == null || vm.getbMobile() == "") {
				vm.setbMobile("");
			} else {
				vm.setbMobile(vm.getbMobile());
			}

//			Profile profile = customProfileRepository.findProfileByPrefEmail(vm.getPrefEmail());
			Profile profile = customProfileRepository.findById(profileId).get();

			vm.setPrefPhone(profile.getPhone());
			vm.setSecondPhone(profile.getSecondPhone());
			vm.setPrefMobile(profile.getPrefMobile());
			vm.setPrefEmail(profile.getPrefEmail());
			vm.setProfileId(profile.getId());
			vm.setInsuranceCompany(profile.getInsuranceCompany());
			ProfileCarriers pc = profileCarriersRepository.findPrimaryCompanyOfProfile(profile.getId());
			if (pc != null) {
				Optional<AvailableCarriers> c = availableCarriersRepository.findById(pc.getCarriersId());
				if (c.isPresent()) {
					vm.setInsuranceCompany(c.get().getCarrier());
				}

			}
			resp.setValue(vm);
			resp.setStatusCode(200);
			resp.setSuccess(true);
			resp.setMessage("Retrieve the Agent by profileId : " + profileId);
		} else {
			Agent agent = customAgentRepository.findAgentByProfileId(profileId);

			if (agent != null) {
				AgentVM agentData = new AgentVM();
				agentData.setFirstName(agent.getProfile().getFirstName());
				agentData.setLastName(agent.getProfile().getLastName());
				agentData.setNpn(agent.getNpn());
				agentData.setbEmail(agent.getProfile().getPrefEmail());
				agentData.setSpeaksSpanish(agent.getProfile().isIsSpeaksSpanish());
				agentData.setPrefPhone(agent.getProfile().getPhone());
				agentData.setPrefMobile(agent.getProfile().getPrefMobile());
				agentData.setPrefEmail(agent.getProfile().getPrefEmail());

				resp.setValue(agentData);
				resp.setStatusCode(200);
				resp.setSuccess(true);
				resp.setMessage("Retrieve the Agent by profileId : " + profileId);
			} else {
				resp.setStatusCode(400);
				resp.setSuccess(false);
				resp.setMessage("Failed to retrieve agent data by profileId : " + profileId + ", Please try again.");
				return resp;
			}

		}
		return resp;
	}

	/**
	 * GET /agent/{npn} : get the agents by its NPN .
	 *
	 * @return the GenericResp and the agent in body
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/agentProfile/save/{profileId}")
	@Timed
	public GenericResp saveAgentProfile(@PathVariable("profileId") Long profileId, @RequestBody ProfileVm agentVM)
			throws Exception {

		log.debug("REST request to get Agent by profileId : " + profileId);
		GenericResp resp = new GenericResp();
		if (profileId == null) {
			resp.setStatusCode(400);
			resp.setSuccess(false);
			resp.setMessage("Failed to retrieve agent data, Please try again.");
			return resp;
		}
		Agent agent = customAgentRepository.findAgentByProfileId(profileId);
		if (agent != null) {
			Optional<ProfileDTO> optDto = profileService.findOne(profileId);
			if (optDto.isPresent()) {
				ProfileDTO dto = optDto.get();
				dto.setPrefMobile(agentVM.getPreferredMobile());
				dto.setPhone(agentVM.getPhone());
				dto.setSecondPhone(agentVM.getSecondPhone());
				dto.setPrefCompany(agentVM.getPrefCompany());
				dto.setComments(agentVM.getComments());
				dto.setInsuranceCompany(agentVM.getInsuranceCompany());
				dto.setIsSpeaksSpanish(agentVM.getIsSpeaksSpanish());
				/*
				 * List<ProfileCarriers> c =
				 * profileCarriersRepository.findAllByProfileId(profileId); dto.setCarriers(c);
				 */
				System.out.println("lan=" + agentVM.getLangKey());
				if (agentVM.getLangKey() != null && agentVM.getLang() != null && !agentVM.getLang().isEmpty()
						&& !agentVM.getLangKey().isEmpty()) {
					dto.setIsSpeaksSpanish(agentVM.getLang().equalsIgnoreCase("ES"));
				}

				profileService.save(dto);

				Optional<User> newUser = userService.getUserWithAuthorities(optDto.get().getUserId());
				if (agentVM.getLangKey() != null && agentVM.getLang() != null && !agentVM.getLang().isEmpty()
						&& !agentVM.getLangKey().isEmpty()) {

					if (!newUser.isPresent()) {

					} else {
						User user = newUser.get();
						user.setLangKey(agentVM.getLangKey());
						userRepository.save(user);
					}

				}
//if carriers are not equal then update date in profileCarriers table -26 Aug 2019.
				List<Integer> list = profileCarriersRepository.getProfileCarriers(profileId);
				int nInts = list.size();
				List<Long> pcList = new ArrayList<Long>(nInts);
				for (int i = 0; i < nInts; ++i) {
					pcList.add(list.get(i).longValue());
				}
				List<Long> list1 = agentVM.getCarriers();
				
//				if(agentVM.getCarriers().size()> 0) {
				if(!list1.equals(pcList))
				{
				List<ProfileCarriers> cars = profileCarriersRepository.findAllByProfileId(profileId);
				for (ProfileCarriers c : cars) {
					if (!c.getIsPrimary()) {
						
						profileCarriersRepository.delete(c);
					}
				}
				
				for (Long carrier : agentVM.getCarriers()) {
					if (carrier != null) {
						Optional<AvailableCarriers> c = availableCarriersRepository.findById(carrier);
						if (c.isPresent() && c != null) {
							ProfileCarriers pc1 = new ProfileCarriers();
							pc1.setCarriersId(carrier);
							pc1.setProfileId(profileId);
							pc1.setIsPrimary(false);
							pc1.setUpdatedDate(new Date());
							profileCarriersRepository.save(pc1);
						}
					}
				}
				
				}

				AvailableCarriers carr = availableCarriersRepository.findByCarrierAndShow(agentVM.getInsuranceCompany(),
						false);

				if (carr == null && agentVM.getInsuranceCompany() != null) {

					AvailableCarriers c = new AvailableCarriers();
					c.setCarrier(agentVM.getInsuranceCompany());
					c.setShowInList(false);
					availableCarriersRepository.save(c);
				}

				carr = availableCarriersRepository.findByCarrierAndShow(agentVM.getInsuranceCompany(), false);
				if (carr != null) {
					ProfileCarriers pc = profileCarriersRepository.findPrimaryCompanyOfProfile(profileId);
					if (pc == null) {
						pc = new ProfileCarriers();
					}
//						ProfileCarriers pc = new ProfileCarriers();
					pc.setProfileId(profileId);
					pc.setCarriersId(carr.getId());
					pc.setIsPrimary(true);
					pc.setUpdatedDate(new Date());
					profileCarriersRepository.save(pc);

				}

				resp.setValue(optDto);
				resp.setStatusCode(200);
				resp.setSuccess(true);
				resp.setMessage("Agent profile updated successfully.");
			} else {
				resp.setStatusCode(400);
				resp.setSuccess(false);
				resp.setMessage("Failed to retrieve profile data by profileId : " + profileId);
			}

		} else {
			resp.setStatusCode(400);
			resp.setSuccess(false);
			resp.setMessage("Failed to retrieve agent data by profileId : " + profileId);
			return resp;

		}
		return resp;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/insurance/companyList/{zip}")
	@Timed
	public GenericResp getInsuranceCompanyList(@PathVariable("zip") String zip) {

		GenericResp resp = new GenericResp();
		List<String> companyList = customAgentRepository.getInsuranceCompanyList(zip, "");
		resp.setValue(companyList);
		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage("Company List fetched successfully.");

		return resp;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/agent/companyList/{zip}")
	@Timed
	public GenericResp getAgentsOfCompany(@PathVariable("zip") String zip) {

		GenericResp resp = new GenericResp();
		List<String> companyList = customAgentRepository.getAgentsOfCompany(zip, "");
		resp.setValue(companyList);
		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage("Agents List fetched successfully.");

		return resp;
	}

	// Added by Sanjay
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/agent/companyList")
	@Timed
	public GenericResp getAgentsOfCompany(@RequestParam("zip") String zip, @RequestParam("text") String text) {

		GenericResp resp = new GenericResp();
		text = text.replaceAll("\\s+", "");
//		List<String> companyList = customAgentRepository.getAgentsOfCompany(zip,text);
		List<String> companyList = customAgentRepository.getAgentsOfCompanyByName(text);

		resp.setValue(companyList);
		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage("Agents List fetched successfully.");

		return resp;
	}

	// Added by Sanjay
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/insurance/companyList")
	@Timed
	public GenericResp getInsuranceCompanyList(@RequestParam("zip") String zip, @RequestParam("text") String text) {

		GenericResp resp = new GenericResp();
		zip = "";
		List<String> companyList = customAgentRepository.getInsuranceCompanyList(zip, text);

		List<String> carrierList = customAgentRepository.getCarriersCompanyList(text);

		for (String carrier : carrierList) {
			if (!companyList.contains(carrier)) {
				companyList.add(carrier);
			}

		}

		resp.setValue(companyList);
		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage("Company List fetched successfully.");

		return resp;
	}

	// Added by Sanjay
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/agent/companyListByLastName/{text}")
	@Timed
	public GenericResp getAgentsOfCompanyByLastName(@PathVariable("text") String text) {

		GenericResp resp = new GenericResp();
		List<SearchInsuranceVM> searchInsuranceVMList = new ArrayList<>();
		text = text.trim();
		Object[][] agentList = customAgentRepository.getAgentsOfCompanyByLastName(text);
		if (agentList != null && agentList.length > 0) {
			for (Object[] arr : agentList) {
				try {
					searchInsuranceVMList
							.add(CommonMapper.fromKeyVal(SearchInsuranceVM.getKeys(), arr, SearchInsuranceVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}

		}
		resp.setValue(searchInsuranceVMList);
		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage("Agents List fetched successfully.");

		return resp;
	}

	// Added by Sanjay
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping("/insurance/companyListByName/{companyName}")
	@Timed
	public GenericResp getInsuranceCompanyListByName(@PathVariable("companyName") String companyName) {

		GenericResp resp = new GenericResp();
		List<SearchInsuranceVM> searchInsuranceVMList = new ArrayList<>();
		companyName = companyName.trim();
		Object[][] companyList = customAgentRepository.getInsuranceCompanyListByName(companyName);

		if (companyList != null && companyList.length > 0) {
			for (Object[] arr : companyList) {
				try {
					searchInsuranceVMList
							.add(CommonMapper.fromKeyVal(SearchInsuranceVM.getKeys(), arr, SearchInsuranceVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}

		}

		resp.setValue(searchInsuranceVMList);
		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage("Company List fetched successfully.");

		return resp;
	}
}
