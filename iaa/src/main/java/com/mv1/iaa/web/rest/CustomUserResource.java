package com.mv1.iaa.web.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.business.view.web.resp.ProfileVM;
import com.mv1.iaa.domain.Payment;
import com.mv1.iaa.domain.ProfileCarriers;
import com.mv1.iaa.repository.CustomProfileRepository;
import com.mv1.iaa.repository.PaymentRepository;
import com.mv1.iaa.repository.ProfileCarriersRepository;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Agent.
 */
@RestController
@RequestMapping("/api")
public class CustomUserResource {

	private final Logger log = LoggerFactory.getLogger(CustomUserResource.class);

	@Autowired
	private CustomProfileRepository customProfileRepository;

	@Autowired
	private ProfileCarriersRepository profileCarriersRepository;

	@Autowired
	private PaymentRepository paymentRepository;

	/**
	 * POST /profile/{email} : profile of the user.
	 *
	 * @param email
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping("/profile/{email}")
	@Timed
	public ResponseEntity<GenericResp> getProfileDetails(@PathVariable("email") String email) {

		ProfileVM pvm = null;

		// Get user profile by email Id
		Object[][] parr = customProfileRepository.findProfileByEmail(email);
		if (parr != null && parr.length > 0) {
			try {
				pvm = CommonMapper.fromKeyVal(ProfileVM.getKeys(), parr[0], ProfileVM.class);
			} catch (Exception e) {
				log.debug("Error while getting the profile", e);
			}
		}

		GenericResp<ProfileVM> resp = new GenericResp<>();
		if (pvm != null) {
			List<ProfileCarriers> ProfileCarriers = profileCarriersRepository.findAllByProfileId(pvm.getProfileId());
			List<Long> carriers = new ArrayList<>();
			Long paymentStatus = paymentRepository.findPaymentStatusByprofile(pvm.getProfileId());

			int primary = 0;
			int count = 0;

			if (paymentStatus != null && paymentStatus>=0) {
				for (ProfileCarriers l1 : ProfileCarriers) {
					if (l1.getIsPrimary()) {
						primary = 1;
					} else {
						count++;
						carriers.add(l1.getCarriersId());
					}

				}
				if (ProfileCarriers.size() > 0) {
					if (count > 0) {
						if (primary == 0) {
							int diff = (int) ((new Date().getTime() - ProfileCarriers.get(0).getUpdatedDate().getTime())
									/ (1000 * 60 * 60 * 24));
							if (diff > 90) {
								resp.setValidToUpdateCarrier(true);
							}
						} else if (primary == 1) {
							int diff1 = (int) ((new Date().getTime()
									- ProfileCarriers.get(count).getUpdatedDate().getTime()) / (1000 * 60 * 60 * 24));
							if (diff1 > 90) {
								resp.setValidToUpdateCarrier(true);
							}
						}
					} else {
						resp.setValidToUpdateCarrier(true);
					}
				} else {
					resp.setValidToUpdateCarrier(true);
				}

			} else {
				for (ProfileCarriers l1 : ProfileCarriers) {
					if (!l1.getIsPrimary()) {
						carriers.add(l1.getCarriersId());
					}
				}
				resp.setValidToUpdateCarrier(true);
			}

			pvm.setCarriers(carriers);
			resp.setMessage("Retrieved profile info");
			resp.setStatusCode(200);
			resp.setValue(pvm);
			resp.setSuccess(true);
		} else {
			resp.setMessage("Failed to retrie the profile info");
			resp.setStatusCode(400);
			resp.setValue(pvm);
			resp.setSuccess(false);
			resp.setValidToUpdateCarrier(false);
		}
		return ResponseUtil.wrapOrNotFound(Optional.of(resp));
	}
}
