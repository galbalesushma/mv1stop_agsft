package com.mv1.iaa.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.service.AgentTypeService;
import com.mv1.iaa.service.dto.AgentTypeDTO;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing AgentType.
 */
@RestController
@RequestMapping("/api")
public class AgentTypeResource {

    private final Logger log = LoggerFactory.getLogger(AgentTypeResource.class);

    private static final String ENTITY_NAME = "agentType";

    private final AgentTypeService agentTypeService;

    public AgentTypeResource(AgentTypeService agentTypeService) {
        this.agentTypeService = agentTypeService;
    }

    /**
     * POST  /agent-types : Create a new agentType.
     *
     * @param agentTypeDTO the agentTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new agentTypeDTO, or with status 400 (Bad Request) if the agentType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/agent-types")
    @Timed
    public ResponseEntity<AgentTypeDTO> createAgentType(@RequestBody AgentTypeDTO agentTypeDTO) throws URISyntaxException {
        log.debug("REST request to save AgentType : {}", agentTypeDTO);
        if (agentTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new agentType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentTypeDTO result = agentTypeService.save(agentTypeDTO);
        return ResponseEntity.created(new URI("/api/agent-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /agent-types : Updates an existing agentType.
     *
     * @param agentTypeDTO the agentTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated agentTypeDTO,
     * or with status 400 (Bad Request) if the agentTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the agentTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/agent-types")
    @Timed
    public ResponseEntity<AgentTypeDTO> updateAgentType(@RequestBody AgentTypeDTO agentTypeDTO) throws URISyntaxException {
        log.debug("REST request to update AgentType : {}", agentTypeDTO);
        if (agentTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentTypeDTO result = agentTypeService.save(agentTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, agentTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /agent-types : get all the agentTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of agentTypes in body
     */
    @GetMapping("/agent-types")
    @Timed
    public List<AgentTypeDTO> getAllAgentTypes() {
        log.debug("REST request to get all AgentTypes");
        return agentTypeService.findAll();
    }

    /**
     * GET  /agent-types/:id : get the "id" agentType.
     *
     * @param id the id of the agentTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the agentTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/agent-types/{id}")
    @Timed
    public ResponseEntity<AgentTypeDTO> getAgentType(@PathVariable Long id) {
        log.debug("REST request to get AgentType : {}", id);
        Optional<AgentTypeDTO> agentTypeDTO = agentTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(agentTypeDTO);
    }

    /**
     * DELETE  /agent-types/:id : delete the "id" agentType.
     *
     * @param id the id of the agentTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/agent-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteAgentType(@PathVariable Long id) {
        log.debug("REST request to delete AgentType : {}", id);
        agentTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
