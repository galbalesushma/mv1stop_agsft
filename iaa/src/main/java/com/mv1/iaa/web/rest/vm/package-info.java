/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mv1.iaa.web.rest.vm;
