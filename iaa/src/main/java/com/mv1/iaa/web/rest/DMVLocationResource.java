package com.mv1.iaa.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.view.web.GenericResp;
import com.mv1.iaa.repository.DMVLocationRepository;
import com.mv1.iaa.service.DMVLocationService;
import com.mv1.iaa.service.dto.DMVLocationDTO;
import com.mv1.iaa.service.mapper.DMVLocationMapper;
import com.mv1.iaa.web.rest.errors.BadRequestAlertException;
import com.mv1.iaa.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing DMVLocation.
 */
@RestController
@RequestMapping("/api")
public class DMVLocationResource {

    private final Logger log = LoggerFactory.getLogger(DMVLocationResource.class);

    private static final String ENTITY_NAME = "dMVLocation";
    
    @Autowired
    private DMVLocationRepository dmvLocationRepository;
    
    @Autowired
    private DMVLocationMapper dmvLocationMapper;

    private final DMVLocationService dMVLocationService;

    public DMVLocationResource(DMVLocationService dMVLocationService) {
        this.dMVLocationService = dMVLocationService;
    }

    /**
     * POST  /dmv-locations : Create a new dMVLocation.
     *
     * @param dMVLocationDTO the dMVLocationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dMVLocationDTO, or with status 400 (Bad Request) if the dMVLocation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dmv-locations")
    @Timed
    public ResponseEntity<DMVLocationDTO> createDMVLocation(@RequestBody DMVLocationDTO dMVLocationDTO) throws URISyntaxException {
        log.debug("REST request to save DMVLocation : {}", dMVLocationDTO);
        if (dMVLocationDTO.getId() != null) {
            throw new BadRequestAlertException("A new dMVLocation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DMVLocationDTO result = dMVLocationService.save(dMVLocationDTO);
        return ResponseEntity.created(new URI("/api/dmv-locations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dmv-locations : Updates an existing dMVLocation.
     *
     * @param dMVLocationDTO the dMVLocationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dMVLocationDTO,
     * or with status 400 (Bad Request) if the dMVLocationDTO is not valid,
     * or with status 500 (Internal Server Error) if the dMVLocationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dmv-locations")
    @Timed
    public ResponseEntity<DMVLocationDTO> updateDMVLocation(@RequestBody DMVLocationDTO dMVLocationDTO) throws URISyntaxException {
        log.debug("REST request to update DMVLocation : {}", dMVLocationDTO);
        if (dMVLocationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DMVLocationDTO result = dMVLocationService.save(dMVLocationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dMVLocationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dmv-locations : get all the dMVLocations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dMVLocations in body
     */
    @GetMapping("/dmv-locations")
    @Timed
    public List<DMVLocationDTO> getAllDMVLocations() {
        log.debug("REST request to get all DMVLocations");
        return dMVLocationService.findAll();
    }

    /**
     * GET  /dmv-locations/:id : get the "id" dMVLocation.
     *
     * @param id the id of the dMVLocationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dMVLocationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/dmv-locations/{id}")
    @Timed
    public ResponseEntity<DMVLocationDTO> getDMVLocation(@PathVariable Long id) {
        log.debug("REST request to get DMVLocation : {}", id);
        Optional<DMVLocationDTO> dMVLocationDTO = dMVLocationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dMVLocationDTO);
    }

    /**
     * DELETE  /dmv-locations/:id : delete the "id" dMVLocation.
     *
     * @param id the id of the dMVLocationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dmv-locations/{id}")
    @Timed
    public ResponseEntity<Void> deleteDMVLocation(@PathVariable Long id) {
        log.debug("REST request to delete DMVLocation : {}", id);
        dMVLocationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @GetMapping("/insurance/DmvList/{city}")
	@Timed
	public GenericResp getDMVLocationByCity(@PathVariable String city) {
		log.debug("REST request to get DMVLocation : {}", city);
		
		GenericResp resp = new GenericResp();
		city = city.trim();
		List<DMVLocationDTO> dMVLocationDTO = dmvLocationRepository.findByCityContaining(city).stream()
				.map(dmvLocationMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
		resp.setValue(dMVLocationDTO);
		resp.setStatusCode(200);
		resp.setSuccess(true);
		resp.setMessage("DMV Location List fetched successfully.");
		return resp;
	}
}
