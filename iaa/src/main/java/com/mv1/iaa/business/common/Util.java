package com.mv1.iaa.business.common;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.mv1.iaa.business.view.csv.AgentCsvBean;
import com.mv1.iaa.business.view.web.req.PaymentReceiptVM;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;

public class Util implements Constants {

	public static <T> List<T> paginate(List<T> list, Integer page, Integer pageSize, boolean isCustom) {
		if (page == null || page <= 0) {
			// set default page as 1
			page = 1;
		}

		int size = list.size();
//        int end = page * PAGE_LIST_SIZE;
//        int start = end - PAGE_LIST_SIZE;
		int end = page * pageSize;
		int start = end - pageSize;

		if (end >= size) {
			end = size;
		}
		if (isCustom) {
			if (page == 1) {
				return list.subList(0, pageSize > 10 ? 10 : pageSize);
			}
		}

		return list.subList(start, end);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> List<T> readCSV(String path, Class clazz) {
		try {
			HeaderColumnNameMappingStrategy<T> strat = new HeaderColumnNameMappingStrategy<T>();
			strat.setType(clazz);

			List<T> beans = new CsvToBeanBuilder(new FileReader(path))
//		            .withSkipLines(1)
					.withType(clazz).withIgnoreLeadingWhiteSpace(true).withMappingStrategy(strat).build().parse();
			return beans;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String... args) {
		// Date,Amount,Name,Phone,Email,Street,City,State,School
		// "09-29-2018","30","Mahen","K","mk@t.com","sfaf","2344","Apex","North
		// Carolina"

		List<AgentCsvBean> v = readCSV("/home/mahen/Documents/mv1-stop/Agent_BasicData_Sample500_2.csv",
				AgentCsvBean.class);
		v.stream().limit(5).forEach(System.out::println);
	}

	public static String generateCard(PaymentReceiptVM receipt, String type, File source, File destination,
			boolean isImageOnly) throws Exception {
		BufferedImage image = ImageIO.read(source);

		// determine image type and handle correct transparency
		int imageType = "png".equalsIgnoreCase(type) ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
		BufferedImage buffImage = new BufferedImage(image.getWidth(), image.getHeight(), imageType);
		String path = destination.getAbsolutePath();
		String fileName = path.substring(0, path.lastIndexOf("."));

		// initializes necessary graphic properties
		Graphics2D w = (Graphics2D) buffImage.getGraphics();
		w.drawImage(image, 0, 0, null);
		AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
		w.setComposite(alphaChannel);
		w.setColor(Color.black);
		w.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));

		// calculate center of the image
		int centerX = 15;
		int centerY = 120;

		// add text overlay to the image
//		w.drawString(name, centerX, centerY);

		// calculate & write card number
		centerX = 170;
		centerY = 120;
//		w.drawString(cardNumber, centerX, centerY);

		ImageIO.write(buffImage, type, destination);
		w.dispose();

		if (isImageOnly) {
			return appendImages(destination, fileName);
		}
		return generatePDFFromImage(fileName);
	}

	private static String appendImages(File destination, String fileName) throws IOException {
		BufferedImage result = new BufferedImage(780, 450, // work these out
				BufferedImage.TYPE_INT_RGB);
		Graphics g = result.getGraphics();

		String card2 = "src/main/resources/images/card2.png";

		String[] images = new String[] { destination.getAbsolutePath(), card2 };
		String dest = fileName + "-dest.png";

		int x = 10, y = 0;
		for (String img : images) {
			BufferedImage bi = ImageIO.read(new File(img));
			g.drawImage(bi, x, y, null);
			y += 222;
			if (x > result.getWidth()) {
				x = 0;
				y += bi.getHeight();
			}
		}
		ImageIO.write(result, "png", new File(dest));

		return dest;
	}

	private static String generatePDFFromImage(String filename) throws Exception {
		Document document = new Document();
		String input1 = filename + ".png";
		String input2 = "src/main/resources/images/card2.png";
		String output = filename + ".pdf";
		FileOutputStream fos = new FileOutputStream(output);

		PdfWriter writer = PdfWriter.getInstance(document, fos);
		writer.open();
		document.open();
		document.add(com.itextpdf.text.Image.getInstance(input1));
		document.add(com.itextpdf.text.Image.getInstance(input2));
		document.close();
		writer.close();

		return output;
	}

	

}
