package com.mv1.iaa.business.common;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mv1.iaa.service.PBXIntegrationService;

@Configurable
@Component
public class DirectoryWatcher {

	@Value("${pbx.syncDirectoryPath}")
	private String dir;

	@Autowired
	private PBXIntegrationService pBXIntegrationService;

	@Scheduled(cron ="${file.transfer.cronExpression}")
	public  void fileWatcher() {
		System.out.println("cdrDirectory------------"+dir);
		 Path myDir = Paths.get(dir);       

	        try {
	    		System.out.println("cdrDirectory1111------------");
	    		
	    	      
	    	              WatchService watcher = myDir.getFileSystem().newWatchService();
							System.out.println("cdrDirectory watcher------------"+watcher);
			           myDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, 
			           StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
	           
	           
	           
	       		File targetDirctory = new File(dir);
	         	System.out.println("targetDirctory path------------"+targetDirctory.getAbsolutePath());
	         		      //     for (;;) {  
	        	   
	        //	   if(events.size() <= 0) {
		        	      System.out.println("0 events------------------");
		        	   if (targetDirctory.isDirectory()) {
		        		      System.out.println("targetDirctory.isDirectory()-----------------");
			   	   			File[] csvFiles = targetDirctory.listFiles(File::isFile);
			   	   		 System.out.println("csvFiles size-----------------"+csvFiles.length);
			   	   			if(csvFiles.length > 0) {
			   	   			 System.out.println("call pBXIntegrationService----------------");
			   	   				pBXIntegrationService.moveSyncDataToGrab();
			   	   			}

		   	           //}
	          // }
	        	   
		           WatchKey watckKey = watcher.take();
		           System.out.println("watckKey-----------------"+watckKey);
		           
		           
		           List<WatchEvent<?>> events = watckKey.pollEvents();
		           System.out.println("inside watcher------------------"+events.size());
		           
		          
		           System.out.println("outside watcher------------------");
		           for (WatchEvent event : events) {
		                if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
		                    System.out.println("Created: " + event.context().toString());
		                	pBXIntegrationService.moveSyncDataToGrab();
		                }
		                if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE) {
		                    System.out.println("Delete: " + event.context().toString());
		                	pBXIntegrationService.moveSyncDataToGrab();
		                }
		                if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
		                    System.out.println("Modify: " + event.context().toString());
		                	pBXIntegrationService.moveSyncDataToGrab();
		             }
		            }
	           watckKey.reset();

		           System.out.println("for end" );
	           }  
	           
	        }catch(

	Exception e)
	{
		System.out.println("Error: " + e.toString());
	}
}

}
