package com.mv1.iaa.business.view.web.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.opencsv.bean.CsvBindByPosition;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgentCsvSampleVM {

	@CsvBindByPosition(position = 0)
	private String LICENSE_NUM;

	@CsvBindByPosition(position = 1)
	private String NPN;

	@CsvBindByPosition(position = 2)
	private String LAST_NAME;

	@CsvBindByPosition(position = 3)
	private String FIRST_NAME;

	@CsvBindByPosition(position = 4)
	private String MIDDLE_NAME;

	@CsvBindByPosition(position = 5)
	private String SUFFIX;

	@CsvBindByPosition(position = 6)
	private String BUSINESS_ENTITY_NAME;

	@CsvBindByPosition(position = 7)
	private String FEIN;

	@CsvBindByPosition(position = 8)
	private String LICENSE_TYPE;

	@CsvBindByPosition(position = 9)
	private String LINE_OF_AUTHORITY;

	@CsvBindByPosition(position = 10)
	private String LICENSE_STATUS;

	@CsvBindByPosition(position = 11)
	private String FIRST_ACTIVE_DATE;

	@CsvBindByPosition(position = 12)
	private String EFFECTIVE_DATE;

	@CsvBindByPosition(position = 13)
	private String EXPIRATION_DATE;

	@CsvBindByPosition(position = 14)
	private String DOMICILESTATE;

	@CsvBindByPosition(position = 15)
	private String IS_RESIDENT;

	@CsvBindByPosition(position = 16)
	private String MLG_ADDRESS1;

	@CsvBindByPosition(position = 17)
	private String MLG_ADDRESS2;

	@CsvBindByPosition(position = 18)
	private String MLG_ADDRESS3;

	@CsvBindByPosition(position = 19)
	private String MLG_CITY;

	@CsvBindByPosition(position = 20)
	private String MLG_STATE;

	@CsvBindByPosition(position = 21)
	private String MLG_ZIP;

	@CsvBindByPosition(position = 22)
	private String MLG_COUNTRY;

	@CsvBindByPosition(position = 23)
	private String BUS_ADDRESS1;

	@CsvBindByPosition(position = 24)
	private String BUS_ADDRESS2;

	@CsvBindByPosition(position = 25)
	private String BUS_ADDRESS3;

	@CsvBindByPosition(position = 26)
	private String BUS_CITY;

	@CsvBindByPosition(position = 27)
	private String BUS_STATE;

	@CsvBindByPosition(position = 28)
	private String BUS_ZIP;

	@CsvBindByPosition(position = 29)
	private String BUS_COUNTRY;

	@CsvBindByPosition(position = 30)
	private String BUS_PHONE;

	@CsvBindByPosition(position = 31)
	private String BUS_EMAIL;

	@CsvBindByPosition(position = 32)
	private String CE_COMPLIANCE;

	@CsvBindByPosition(position = 33)
	private String NAICNO;

	public String getLICENSE_NUM() {
		return LICENSE_NUM;
	}

	public String getNPN() {
		return NPN;
	}

	public String getLAST_NAME() {
		return LAST_NAME;
	}

	public String getFIRST_NAME() {
		return FIRST_NAME;
	}

	public String getMIDDLE_NAME() {
		return MIDDLE_NAME;
	}

	public String getSUFFIX() {
		return SUFFIX;
	}

	public String getBUSINESS_ENTITY_NAME() {
		return BUSINESS_ENTITY_NAME;
	}

	public String getFEIN() {
		return FEIN;
	}

	public String getLICENSE_TYPE() {
		return LICENSE_TYPE;
	}

	public String getLINE_OF_AUTHORITY() {
		return LINE_OF_AUTHORITY;
	}

	public String getLICENSE_STATUS() {
		return LICENSE_STATUS;
	}

	public String getFIRST_ACTIVE_DATE() {
		return FIRST_ACTIVE_DATE;
	}

	public String getEFFECTIVE_DATE() {
		return EFFECTIVE_DATE;
	}

	public String getEXPIRATION_DATE() {
		return EXPIRATION_DATE;
	}

	public String getDOMICILESTATE() {
		return DOMICILESTATE;
	}

	public String getIS_RESIDENT() {
		return IS_RESIDENT;
	}

	public String getMLG_ADDRESS1() {
		return MLG_ADDRESS1;
	}

	public String getMLG_ADDRESS2() {
		return MLG_ADDRESS2;
	}

	public String getMLG_ADDRESS3() {
		return MLG_ADDRESS3;
	}

	public String getMLG_CITY() {
		return MLG_CITY;
	}

	public String getMLG_STATE() {
		return MLG_STATE;
	}

	public String getMLG_ZIP() {
		return MLG_ZIP;
	}

	public String getMLG_COUNTRY() {
		return MLG_COUNTRY;
	}

	public String getBUS_ADDRESS1() {
		return BUS_ADDRESS1;
	}

	public String getBUS_ADDRESS2() {
		return BUS_ADDRESS2;
	}

	public String getBUS_ADDRESS3() {
		return BUS_ADDRESS3;
	}

	public String getBUS_CITY() {
		return BUS_CITY;
	}

	public String getBUS_STATE() {
		return BUS_STATE;
	}

	public String getBUS_ZIP() {
		return BUS_ZIP;
	}

	public String getBUS_COUNTRY() {
		return BUS_COUNTRY;
	}

	public String getBUS_PHONE() {
		return BUS_PHONE;
	}

	public String getBUS_EMAIL() {
		return BUS_EMAIL;
	}

	public String getCE_COMPLIANCE() {
		return CE_COMPLIANCE;
	}

	public String getNAICNO() {
		return NAICNO;
	}

	public void setLICENSE_NUM(String lICENSE_NUM) {
		LICENSE_NUM = lICENSE_NUM;
	}

	public void setNPN(String nPN) {
		NPN = nPN;
	}

	public void setLAST_NAME(String lAST_NAME) {
		LAST_NAME = lAST_NAME;
	}

	public void setFIRST_NAME(String fIRST_NAME) {
		FIRST_NAME = fIRST_NAME;
	}

	public void setMIDDLE_NAME(String mIDDLE_NAME) {
		MIDDLE_NAME = mIDDLE_NAME;
	}

	public void setSUFFIX(String sUFFIX) {
		SUFFIX = sUFFIX;
	}

	public void setBUSINESS_ENTITY_NAME(String bUSINESS_ENTITY_NAME) {
		BUSINESS_ENTITY_NAME = bUSINESS_ENTITY_NAME;
	}

	public void setFEIN(String fEIN) {
		FEIN = fEIN;
	}

	public void setLICENSE_TYPE(String lICENSE_TYPE) {
		LICENSE_TYPE = lICENSE_TYPE;
	}

	public void setLINE_OF_AUTHORITY(String lINE_OF_AUTHORITY) {
		LINE_OF_AUTHORITY = lINE_OF_AUTHORITY;
	}

	public void setLICENSE_STATUS(String lICENSE_STATUS) {
		LICENSE_STATUS = lICENSE_STATUS;
	}

	public void setFIRST_ACTIVE_DATE(String fIRST_ACTIVE_DATE) {
		FIRST_ACTIVE_DATE = fIRST_ACTIVE_DATE;
	}

	public void setEFFECTIVE_DATE(String eFFECTIVE_DATE) {
		EFFECTIVE_DATE = eFFECTIVE_DATE;
	}

	public void setEXPIRATION_DATE(String eXPIRATION_DATE) {
		EXPIRATION_DATE = eXPIRATION_DATE;
	}

	public void setDOMICILESTATE(String dOMICILESTATE) {
		DOMICILESTATE = dOMICILESTATE;
	}

	public void setIS_RESIDENT(String iS_RESIDENT) {
		IS_RESIDENT = iS_RESIDENT;
	}

	public void setMLG_ADDRESS1(String mLG_ADDRESS1) {
		MLG_ADDRESS1 = mLG_ADDRESS1;
	}

	public void setMLG_ADDRESS2(String mLG_ADDRESS2) {
		MLG_ADDRESS2 = mLG_ADDRESS2;
	}

	public void setMLG_ADDRESS3(String mLG_ADDRESS3) {
		MLG_ADDRESS3 = mLG_ADDRESS3;
	}

	public void setMLG_CITY(String mLG_CITY) {
		MLG_CITY = mLG_CITY;
	}

	public void setMLG_STATE(String mLG_STATE) {
		MLG_STATE = mLG_STATE;
	}

	public void setMLG_ZIP(String mLG_ZIP) {
		MLG_ZIP = mLG_ZIP;
	}

	public void setMLG_COUNTRY(String mLG_COUNTRY) {
		MLG_COUNTRY = mLG_COUNTRY;
	}

	public void setBUS_ADDRESS1(String bUS_ADDRESS1) {
		BUS_ADDRESS1 = bUS_ADDRESS1;
	}

	public void setBUS_ADDRESS2(String bUS_ADDRESS2) {
		BUS_ADDRESS2 = bUS_ADDRESS2;
	}

	public void setBUS_ADDRESS3(String bUS_ADDRESS3) {
		BUS_ADDRESS3 = bUS_ADDRESS3;
	}

	public void setBUS_CITY(String bUS_CITY) {
		BUS_CITY = bUS_CITY;
	}

	public void setBUS_STATE(String bUS_STATE) {
		BUS_STATE = bUS_STATE;
	}

	public void setBUS_ZIP(String bUS_ZIP) {
		BUS_ZIP = bUS_ZIP;
	}

	public void setBUS_COUNTRY(String bUS_COUNTRY) {
		BUS_COUNTRY = bUS_COUNTRY;
	}

	public void setBUS_PHONE(String bUS_PHONE) {
		BUS_PHONE = bUS_PHONE;
	}

	public void setBUS_EMAIL(String bUS_EMAIL) {
		BUS_EMAIL = bUS_EMAIL;
	}

	public void setCE_COMPLIANCE(String cE_COMPLIANCE) {
		CE_COMPLIANCE = cE_COMPLIANCE;
	}

	public void setNAICNO(String nAICNO) {
		NAICNO = nAICNO;
	}

}
