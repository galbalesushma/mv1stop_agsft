package com.mv1.iaa.business.view.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchReq {

    private String zip;
    private String phone;
    private String type;
    private String email;
    private String name;
    private String vehicleType;
    private String vehicleYear;
    private String county;
    private String company;
    
  
	public String getZip() {
		return zip;
	}

	public String getPhone() {
		return phone;
	}

	public String getType() {
		return type;
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public String getVehicleYear() {
		return vehicleYear;
	}

	public String getCounty() {
		return county;
	}

	public String getCompany() {
		return company;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public void setVehicleYear(String vehicleYear) {
		this.vehicleYear = vehicleYear;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((zip == null) ? 0 : zip.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SearchReq other = (SearchReq) obj;
        if (zip == null) {
            if (other.zip != null)
                return false;
        } else if (!zip.equals(other.zip))
            return false;
        return true;
    }

	@Override
	public String toString() {
		return "SearchReq [zip=" + zip + ", phone=" + phone + ", type=" + type + ", email=" + email + ", name=" + name
				+ ", vehicleType=" + vehicleType + ", vehicleyear=" + vehicleYear + ", county=" + county + ", company="
				+ company + "]";
	}



	
   

}
