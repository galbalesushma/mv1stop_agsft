package com.mv1.iaa.business.view.web;

import java.util.List;

import com.mv1.iaa.business.view.Error;

public class GenericResp<T> {

    private Integer statusCode;
    private String message;
    private T value;
    private boolean isSuccess;
    private List<Error> errorList ;
    private boolean isValidToUpdateCarrier;


	public boolean isValidToUpdateCarrier() {
		return isValidToUpdateCarrier;
	}

	public void setValidToUpdateCarrier(boolean isValidToUpdateCarrier) {
		this.isValidToUpdateCarrier = isValidToUpdateCarrier;
	}

    public Integer getStatusCode() {
	return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
	this.statusCode = statusCode;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public boolean isSuccess() {
	return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
	this.isSuccess = isSuccess;
    }

    public T getValue() {
	return value;
    }

    public void setValue(T value) {
	this.value = value;
    }
    


    public List<Error> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<Error> errorList) {
		this.errorList = errorList;
	}

	@Override
    public String toString() {
	return "GenericResp [statusCode=" + statusCode + ", message=" + message + ", value=" + value + ", isSuccess="
		+ isSuccess + ", isValidToUpdateCarrier=" + isValidToUpdateCarrier + "]";
    }
}
