package com.mv1.iaa.business.view;

public class Error {

	private String fieldName;
	private String message;
	private boolean isValid = true;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	@Override
	public String toString() {
		return "Error [fieldName=" + fieldName + ", message=" + message + ", isValid=" + isValid + "]";
	}

	
	
}
