package com.mv1.iaa.business.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicHeader;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv1.iaa.business.view.search.Holder;

@Component
public class SearchQueryBuilder <T> {
	
	private static final Logger log = LoggerFactory.getLogger(SearchQueryBuilder.class);
    
    private static final String elasticServer = "elastic-search";
    
    private RestClient restClient = RestClient
            .builder(
		       new HttpHost(elasticServer, 9200, "http"))
			.build();
    
	public Holder<List<T>> search(Class<T> type, String index, String query, int from, int size) {
        
        Holder<List<T>> result = new Holder<>();
        Holder<JSONArray> holder = new Holder<>();

        List<T> list = new ArrayList<>();
    	try {
			log.debug("query {} for index {} ", query, index);
                /**
                params.put("q", query);
                response = restClient.performRequest("GET", index + "/_search", params);
                 */
            holder = searchNestedSubs(index, query, from, size);
            JSONArray arr = holder.getType();
            
//			if(arr == null || arr.length() == 0) {
//			    holder = searchPrimaryZip(index, query, from, size);
//                arr = holder.getType();
			    if(arr == null || arr.length() == 0) {
    				// return empty list
    				return result;
			    }
//			}
			
			// create the list from hits
			for(int i =0 ; i < arr.length() ; i++) {
				JSONObject o = arr.getJSONObject(i);
				ObjectMapper mapper = new ObjectMapper();
				JavaType objType = mapper.getTypeFactory().constructType(type);
				T item =  mapper.readValue(o.get("_source").toString(), objType) ;
				list.add(item);
            }
            
            // Fill the results 
            result.setTotal(holder.getTotal());
            result.setType(list);
		} catch (IOException | JSONException e) {
			log.error("Error while getting elastic search reponse ", e);
		}
		
    	return result;
    }
    
    private Holder<JSONArray> searchPrimaryZip(String index, String query, int from, int size) {
        Map<String, String> params = new HashMap<String, String>();
        Holder<JSONArray> holder = new Holder<>();
        Response response = null;
        String jsonQuery = new JSONObject()
                .put("from", from)
                .put("size", size)
                .put("query", new JSONObject()
                    .put("multi_match", new JSONObject()
                    .put("query", query)
                    .put("fields", new JSONArray()
                        .put("subs.zip")
                        .put("zip")
                        )
                    )
                ).put("sort", new JSONArray()
                    .put(new JSONObject()
                        .put("subs.score", new JSONObject()
                            .put("order", "desc")
                            .put("nested", new JSONObject()
                                .put("path", "subs")
                                .put("filter", new JSONObject()
                                    .put("term", new JSONObject()
                                        .put("subs", query)
                                    )
                                )
                            )
                        )
                    )
                ).toString();
                
        try {           
            log.debug("json query {} formed for index {} ", jsonQuery, index);
            Header[] headers = {new BasicHeader("Content-type", "application/json")};
            HttpEntity entity = new NStringEntity(jsonQuery, ContentType.APPLICATION_JSON);
            response = restClient.performRequest("GET", "/"+ index + "/_search", params, entity, headers);
            
            String respStr = EntityUtils.toString(response.getEntity());
            log.debug("elasticsearch top level response :" + respStr);
            
            JSONObject respObj = new JSONObject(respStr);
            JSONObject hits = respObj.getJSONObject("hits");
            if(hits == null) {
                // return empty list
                return holder;
            }
            
            holder.setTotal(hits.getInt("total"));
            holder.setType(hits.getJSONArray("hits"));

        } catch (Exception e ) {
            log.error("Error while getting elastic search nested subs query reponse ", e);
        }
            
        return holder;
    }
    
	private Holder<JSONArray> searchNestedSubs(String index, String query, int from, int size) {
        Map<String, String> params = new HashMap<String, String>();
        Holder<JSONArray> holder = new Holder<>();

        Response response = null;
        try {
            String jsonQuery = "";
            jsonQuery = new JSONObject()
                .put("from", from)
                .put("size", size)
                .put("query", new JSONObject()
                    .put("nested", new JSONObject()
                        .put("path", "subs")
                        .put("query", new JSONObject()
                            .put("multi_match", new JSONObject()
                                .put("query", query)
                                .put("fields", new JSONArray()
                                    .put("subs.zip^4")
                                )
                            )
                        )
                    )
               ).put("sort", new JSONArray()
                    .put(new JSONObject()
                        .put("subs.score", new JSONObject()
                            .put("order", "desc")
                            .put("nested", new JSONObject()
                                .put("path", "subs")
                                .put("filter", new JSONObject()
                                    .put("term", new JSONObject()
                                        .put("subs", query)
                                    )
                                )
                            )
                        )
                    )
               ).toString();
            
            log.debug("nested subs json query formed for "+ index + " .. " + jsonQuery);
            Header[] headers = {new BasicHeader("Content-type", "application/json")};
            HttpEntity entity = new NStringEntity(jsonQuery, ContentType.APPLICATION_JSON);
            response = restClient.performRequest("GET","/"+ index + "/_search", params, entity, headers);
            
            String respStr = EntityUtils.toString(response.getEntity());
            log.debug("elasticsearch subs response :" + respStr);
            JSONObject respObj = new JSONObject(respStr);
            JSONObject hits = respObj.getJSONObject("hits");
            
            if(hits == null) {
                // return empty hits
                return holder;
            }
            holder.setTotal(hits.getInt("total"));
            holder.setType(hits.getJSONArray("hits"));
        } catch (Exception e ) {
            log.error("Error while getting elastic search nested subs query reponse ", e);
        }
        return holder;
    }
}