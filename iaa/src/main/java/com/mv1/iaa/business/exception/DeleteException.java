/**
 * 
 */
package com.mv1.iaa.business.exception;

/**
 * @author bpawar
 * @since 11-Oct-2018
 */
public class DeleteException extends Exception {

	private static final long serialVersionUID = 5686464368289535875L;

	public DeleteException() {
	}

	public DeleteException(String message) {
		super(message);
	}

	public DeleteException(String message, Throwable cause) {
		super(message, cause);
	}
}
