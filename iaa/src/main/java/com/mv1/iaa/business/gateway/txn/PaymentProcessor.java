package com.mv1.iaa.business.gateway.txn;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.allcolor.yahp.converter.CYaHPConverter;
import org.allcolor.yahp.converter.IHtmlToPdfTransformer;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.pdf.qrcode.WriterException;
import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.common.CommonService;
import com.mv1.iaa.business.component.SearchObjectWriter;
import com.mv1.iaa.business.gateway.bluepay.BluePay;
import com.mv1.iaa.business.gateway.model.PayRequest;
import com.mv1.iaa.business.gateway.model.PayResponse;
import com.mv1.iaa.business.gateway.util.PaymentHelper;
import com.mv1.iaa.business.view.web.req.PaymentReceiptVM;
import com.mv1.iaa.business.view.web.req.PaymentVM;
import com.mv1.iaa.business.view.web.resp.PayResponseView;
import com.mv1.iaa.domain.Cart;
import com.mv1.iaa.domain.City;
import com.mv1.iaa.domain.State;
import com.mv1.iaa.domain.enumeration.PayStatus;
import com.mv1.iaa.domain.enumeration.ProfileType;
import com.mv1.iaa.repository.CartRepository;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.CustomCityRepository;
import com.mv1.iaa.repository.CustomPackSubscriptionRepository;
import com.mv1.iaa.repository.CustomStateRepository;
import com.mv1.iaa.service.AddressService;
import com.mv1.iaa.service.PaymentService;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.dto.AddressDTO;
import com.mv1.iaa.service.dto.PaymentDTO;
import com.mv1.iaa.service.dto.ProfileDTO;

@Component
public class PaymentProcessor<T> {

	private final Logger log = LoggerFactory.getLogger(PaymentProcessor.class);

	@Autowired
	private CustomPackSubscriptionRepository customPackSubscriptionRepository;

	@Autowired
	private CustomAgentRepository customAgentRepository;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private CustomCityRepository customCityRepository;

	@Autowired
	private CustomStateRepository customStateRepository;

	@Autowired
	private CommonService commonService;

	@Autowired
	private SearchObjectWriter<T> indexWriter;

	private static final ObjectMapper mapper = new ObjectMapper();

	@Value("${bank.accountId}")
	private String ACCOUNT_ID;

	@Value("${bank.secretKey}")
	private String SECRET_KEY;

	@Value("${bank.mode}")
	private String MODE;
	/*
	 * @Autowired VelocityUtility velocityUtility;
	 */
//    @Value("${default.pdf.path}")
//	String defaultPdfPath;

	@Autowired
	private VelocityEngine velocityEngine;

	@Transactional
	public PayResponseView process(PaymentVM payment) throws Exception {

		Long profileId = null;

		// create profile if not created
		if (payment.getProfileId() == null) {
			profileId = createProfile(payment);
			payment.setProfileId(profileId);
		} else {
			
			profileId = payment.getProfileId();
		}

		// Log the payment in the DB
		Long paymentId = save(payment);

		// Prepare & return the payment response view
		PayResponseView view = new PayResponseView();
		
        //Admin-Paper cheque payment 
		/*
		 * if (payment.getChequeNo() == null && payment.getCardNo()!=null ||
		 * !payment.getCardNo().isEmpty()) {
		 */
		if (payment.getTxnType().equalsIgnoreCase("card") || payment.getTxnType().equalsIgnoreCase("ACH") && payment.getChequeNo() == null ) {	
			// Create payment gateway request
			PayRequest req = PaymentHelper.getPayRequest(payment, ACCOUNT_ID, SECRET_KEY, MODE);

			// Process the payment with payment gateway
			PayResponse resp = processPayment(req);

			// Update the payment status
			updatePayment(paymentId, resp);

			view.setMessage(resp.getMessage());
			view.setProfileId(payment.getProfileId());
			view.setStatus(resp.getStatus());

			if (resp.getStatus().equalsIgnoreCase("APPROVED")) {
				view.setCartId(payment.getCartId());
				view.setMessage("Thank you for your payment,Check your email for payment Receipt !");

				// Update subscriptions of entities
				activateSubscriptions(payment.getCartId(), paymentId);

				// update subscriptions of entities to elastic search
				com.mv1.iaa.domain.Agent ag = customAgentRepository.findOneByProfileId(profileId);

//	            boolean success = update2ElasticSearch(payment.getProfileId(), ag.getId());
				boolean success = true;
				if (!success) {
					log.error("Falied to update subscriptions to Elasticsearch ");
				}
			} else {
				view.setMessage("The payment was unsuccessful, Please try again.");
			}
		} else {
			if(payment.getChequeNo() != null && payment.getCardNo()==null || payment.getCardNo().isEmpty())
			{
			view.setProfileId(payment.getProfileId());
			view.setCartId(payment.getCartId());
			view.setStatus("SUCCESS");
			view.setMessage("Thank you for your payment,Check your email for payment Receipt !");

			// Update subscriptions of entities
			activateSubscriptions(payment.getCartId(), paymentId);

			// update subscriptions of entities to elastic search
			com.mv1.iaa.domain.Agent ag = customAgentRepository.findOneByProfileId(profileId);
			// view.setStatus();
			}
			
		}

		return view;
	}

	@SuppressWarnings("unchecked")
	private boolean update2ElasticSearch(Long profileId, Long id) {

		Optional<ProfileDTO> prOpt = profileService.findOne(profileId);

		ProfileDTO profileDTO = null;
		if (prOpt.isPresent()) {
			profileDTO = prOpt.get();
		}

		T val = null;

		if (ProfileType.AGENT.equals(profileDTO.getType())) {
			val = (T) commonService.getAgentSearchVM(profileId);
		} else if (ProfileType.FACILITY.equals(profileDTO.getType())) {
			val = (T) commonService.getFacilitySearchVM(profileId);
		}

		return indexWriter.write2Elasticsearch(val, id);
	}

	@Transactional
	private Long createProfile(PaymentVM vm) throws Exception {

		State state = customStateRepository.findOneByStateCodeIgnoreCase(vm.getState());
		City city = customCityRepository.findOneByNameIgnoreCase(vm.getCity());

		AddressDTO aDto = CommonMapper.addressDTOfromPayment(vm, city.getId(), state.getId());
		aDto = addressService.save(aDto);

		ProfileDTO dto = CommonMapper.profileDTOfrom(vm, aDto.getId());
		dto = profileService.save(dto);

		return dto.getId();
	}

	@Transactional
	private void activateSubscriptions(Long cartId, Long paymentId) {

		Optional<Cart> optCart = cartRepository.findOneWithEagerRelationships(cartId);
		Cart cart = null;
		if (optCart.isPresent()) {
			cart = optCart.get();
			// update subscriptions
			cart.getSubscriptions().stream().forEach(s -> {
				s.setIsActive(true);
				s.setUpdatedAt(ZonedDateTime.now());
				customPackSubscriptionRepository.save(s);
			});
			cart.setIsCompleted(true);
			cart.setPaymentId(paymentId);
			cartRepository.save(cart);
		}
	}

	@Transactional
	private Long save(PaymentVM payment) {

		Optional<ProfileDTO> prOpt = profileService.findOne(payment.getProfileId());

		ProfileDTO profileDTO = null;
		if (prOpt.isPresent()) {
			profileDTO = prOpt.get();
		}

		// Create payment
		
		PaymentDTO pDTO = CommonMapper.fromPaymentVM(payment, profileDTO);
		pDTO = paymentService.save(pDTO);
		return pDTO.getId();
	
		
	}

	private PayResponse processPayment(PayRequest payRequest) {

		log.debug("payment request : " + payRequest);

		PayResponse payResponse = null;
		// Makes the API Request with BluePay
		BluePay payment = payRequest.getPayment();
		try {
			payment.process();
		} catch (Exception ex) {
			log.debug("Exception: " + ex.toString());
			payResponse = PaymentHelper.getErrorPayResponse(payment);
		}

		payResponse = PaymentHelper.getPayResponse(payment);
		log.debug("Recieved payment response : " + payResponse);

		return payResponse;
	}

	@Transactional
	private boolean updatePayment(Long paymentId, PayResponse resp) {

		PayStatus payStatus;
		PaymentDTO paymentDTO = null;

		if (resp.getStatus().equalsIgnoreCase("APPROVED")) {
			payStatus = PayStatus.SUCCESS;
		} else if (resp.getStatus().equalsIgnoreCase("DECLINED")) {
			payStatus = PayStatus.FAILURE;
		} else {
			payStatus = PayStatus.AWAITED;
		}

		Optional<PaymentDTO> optDto = paymentService.findOne(paymentId);

		if (optDto.isPresent()) {
			paymentDTO = optDto.get();
			paymentDTO.setStatus(payStatus);
			paymentDTO.setTxnId(resp.getTxnId());
			paymentDTO.setUpdatedAt(ZonedDateTime.now());

			try {
				paymentDTO.setGatewayResp(mapper.writeValueAsString(resp));
			} catch (Exception e) {
				// do nothing
			}

			paymentDTO = paymentService.save(paymentDTO);
		}

		return payStatus.equals(PayStatus.SUCCESS);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public File createTicket(PaymentReceiptVM receipt) throws IOException, WriterException, Exception {

//		Ticket ticket = order.getTickets().stream().filter(i -> i.getBarcode().equalsIgnoreCase(barcodeId))
//				.collect(Collectors.toList()).get(0);
//		Event event = eventRepo.findOne(order.getEvent().getEventId());
//		Organization organization = orgRepo.findOne(event.getOrg().getOrgId());
//		if(Objects.isNull(organization))
//			throw new WazooException(201, "Organization not found");

//		
		Map<String, Object> props = new HashMap<>();

		/**
		 * check for end date flag
		 * 
		 */

	if(receipt.getAccountNumber()!=null || receipt.getAccountNumber()!="")
	{
		props.put("accountNo", receipt.getAccountNumber());
	}
	else
		if(receipt.getCardNo()!=null || receipt.getCardNo()!="")
		{
			props.put("cardNo", receipt.getCardNo());
		}
		
		props.put("email", receipt.getEmail());
		props.put("fName", receipt.getFirstName());
		props.put("langKey", receipt.getLangKey());
		props.put("lName", receipt.getLastName());
		props.put("packSub", receipt.getPackSubVM());
		props.put("totalAmount", receipt.getTotalAmount());
		props.put("dateTime", receipt.getTransactionTime());
		props.put("uType", receipt.getUserType());

		Date d = new Date();
		DateFormat df = new SimpleDateFormat("yyyy");
		String transactionTime = df.format(d);
		System.out.println("String in dd/MM/yyyy format is: " + transactionTime);
		props.put("copyRight", transactionTime);

		ClassPathResource iaaLogo = new ClassPathResource("images/iaa_logo.png");

		props.put("iaaLogo", iaaLogo.getPath());
		String pdfTemplate = getTemplatetoText("templates/ticket.vm", props);

		DateFormat df2 = new SimpleDateFormat("MM_dd_yyyy");
		String transactionTime2 = df2.format(d);
		String rName = "/receipt_" + transactionTime2;
		props.put("uType", "/receipt_" + transactionTime2);

		CYaHPConverter converter = new CYaHPConverter();
		Map properties = new HashMap();
		properties.put(IHtmlToPdfTransformer.PDF_RENDERER_CLASS, IHtmlToPdfTransformer.FLYINGSAUCER_PDF_RENDERER);

		List headerFooterList = new ArrayList();

//		@Cleanup

		File dir = new File("payment_receipt");
		if (!dir.exists()) {
			dir.mkdirs();
		}

		FileOutputStream fos = new FileOutputStream(new File("payment_receipt".concat(rName).concat(".pdf")));

		String baseUrlPath = new StringBuffer().append("file://").append(System.getProperty("java.io.tmpdir"))
				.toString();

		converter.convertToPdf(pdfTemplate, IHtmlToPdfTransformer.A4P, headerFooterList, baseUrlPath, fos, properties);

		Path filePath = Paths.get("payment_receipt".concat(rName).concat(".pdf"));
		return filePath.toFile();
	}

	public String getTemplatetoText(String templateName, Map<String, Object> props) {
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templateName, "UTF-8", props);
		return text;
	}
}
