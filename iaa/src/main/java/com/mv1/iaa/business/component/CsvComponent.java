package com.mv1.iaa.business.component;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.codahale.metrics.annotation.Timed;
import com.mv1.iaa.business.common.AgentCSVMapper;
import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.common.Util;
import com.mv1.iaa.business.enumeration.AgentEnum;
import com.mv1.iaa.business.enumeration.FacilityEnum;
import com.mv1.iaa.business.exception.Mv1Exception;
import com.mv1.iaa.business.util.CsvValidator;
import com.mv1.iaa.business.view.csv.AgentCsvBean;
import com.mv1.iaa.business.view.csv.CsvResp;
import com.mv1.iaa.business.view.csv.FacilityCsvBean;
import com.mv1.iaa.business.view.web.FacilityCsvSampleVM;
import com.mv1.iaa.business.view.web.req.AgentCsvSampleVM;
import com.mv1.iaa.domain.Address;
import com.mv1.iaa.domain.Agent;
import com.mv1.iaa.domain.AgentExtension;
import com.mv1.iaa.domain.AvailableCarriers;
import com.mv1.iaa.domain.Business;
import com.mv1.iaa.domain.City;
import com.mv1.iaa.domain.Contact;
import com.mv1.iaa.domain.County;
import com.mv1.iaa.domain.Facility;
import com.mv1.iaa.domain.Profile;
import com.mv1.iaa.domain.ProfileCarriers;
import com.mv1.iaa.domain.State;
import com.mv1.iaa.domain.ZipArea;
import com.mv1.iaa.domain.enumeration.ProfileType;
import com.mv1.iaa.repository.AgentExtensionRepository;
import com.mv1.iaa.repository.AvailableCarriersRepository;
import com.mv1.iaa.repository.BusinessRepository;
import com.mv1.iaa.repository.CustomAddressRepository;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.CustomCityRepository;
import com.mv1.iaa.repository.CustomContactRepository;
import com.mv1.iaa.repository.CustomCountyRepository;
import com.mv1.iaa.repository.CustomFacilityRepository;
import com.mv1.iaa.repository.CustomProfileRepository;
import com.mv1.iaa.repository.CustomStateRepository;
import com.mv1.iaa.repository.CustomZipAreaRepository;
import com.mv1.iaa.repository.ProfileCarriersRepository;
import com.mv1.iaa.service.AddressService;
import com.mv1.iaa.service.AgentLicService;
import com.mv1.iaa.service.AgentService;
import com.mv1.iaa.service.BusinessService;
import com.mv1.iaa.service.ContactService;
import com.mv1.iaa.service.FacilityService;
import com.mv1.iaa.service.ProducerService;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.dto.AddressDTO;
import com.mv1.iaa.service.dto.AgentDTO;
import com.mv1.iaa.service.dto.AgentLicDTO;
import com.mv1.iaa.service.dto.BusinessDTO;
import com.mv1.iaa.service.dto.ContactDTO;
import com.mv1.iaa.service.dto.FacilityDTO;
import com.mv1.iaa.service.dto.ProducerDTO;
import com.mv1.iaa.service.dto.ProfileDTO;
import com.mv1.iaa.service.impl.CustomAgentFilterSeviceImpl;

@Component
public class CsvComponent<T> {

	private final Logger log = LoggerFactory.getLogger(CsvComponent.class);

	@Autowired
	private CustomCityRepository customCityRepository;

	@Autowired
	private CustomStateRepository customStateRepository;

	@Autowired
	private CustomAgentRepository customAgentRepository;

	@Autowired
	private CustomFacilityRepository customFacilityRepository;

	@Autowired
	private BusinessRepository businessRepository;

	@Autowired
	private CustomContactRepository customContactRepository;

	@Autowired
	private CustomAddressRepository customAddressRepository;

	@Autowired
	private AddressService addressService;

	@Autowired
	private ProducerService producerService;

	@Autowired
	private AgentLicService agentLicService;

	@Autowired
	private BusinessService businessService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private AgentService agentService;

	@Autowired
	private ContactService contactService;

	@Autowired
	private AgentComponent agentComponent;

	@Autowired
	private SearchObjectWriter<T> indexWriter;

	@Autowired
	private CustomCountyRepository customCountyRepository;

	@Autowired
	private CustomZipAreaRepository customZipAreaRepository;

	@Autowired
	private FacilityService facilityService;

	@Autowired
	private CustomProfileRepository customProfileRepository;

	@Autowired
	private CustomAgentFilterSeviceImpl customAgentFilterSeviceImpl;
	
	
	@Autowired
	private AvailableCarriersRepository availableCarriersRepository;
	
	@Autowired
	private ProfileCarriersRepository profileCarriersRepository;
	
	@Autowired
	private AgentExtensionRepository agentExtensionRepository;

	@Timed
	@Transactional
	private CsvResp bulkProcessAgents(List<AgentCsvBean> beans) throws Exception {

		log.debug("processing bulkAddAgents ... ");

		int insertCount = 0, updateCount = 0;
		CsvResp resp = new CsvResp();

		for (AgentCsvBean vm : beans) {
			Agent agent = customAgentRepository.findOneByNpn(vm.getNpn());
			if (agent != null) {
				updateCount += 1;
			} else {
				insertCount += 1;
			}
			upsertAgent(vm, agent);
		}

		resp.setInsertCount(insertCount);
		resp.setUpdateCount(updateCount);

		return resp;
	}

	private void upsertAgent(AgentCsvBean vm, Agent agent) throws Exception {

		ProfileDTO prEdit = null;
		BusinessDTO bsEdit = null;
		Contact cont = null;
		AddressDTO bAddrEdit = null;
		AgentLicDTO agLicEdit = null;
		ProducerDTO proEdit = null;
		AddressDTO pAddrEdit = null;

		if (agent != null && agent.getId() != null) {
			Optional<AgentDTO> agOpt = agentService.findOne(agent.getId());
			AgentDTO agEdit = agOpt.get();

			Optional<ProfileDTO> prfOpt = profileService.findOne(agEdit.getProfileId());
			prEdit = prfOpt.get();

			Optional<BusinessDTO> bsOpt = businessService.findOne(agEdit.getBusinessId());
			bsEdit = bsOpt.get();

			cont = customContactRepository.findOneByAddressId(bsEdit.getAddressId());

			Optional<AddressDTO> bAddrOpt = addressService.findOne(bsEdit.getAddressId());
			bAddrEdit = bAddrOpt.get();

			Optional<AgentLicDTO> agtLicOpt = agentLicService.findOne(agEdit.getLicId());
			agLicEdit = agtLicOpt.get();

			Optional<ProducerDTO> proOpt = producerService.findOne(agLicEdit.getProducerId());
			proEdit = proOpt.get();

			Optional<AddressDTO> pAddrOpt = addressService.findOne(proEdit.getAddrId());
			pAddrEdit = pAddrOpt.get();
		}

		City pCity = customCityRepository.findOneByNameIgnoreCase(vm.getpCity());
		State pState = customStateRepository.findOneByStateCodeIgnoreCase(vm.getpState());
		County pCounty = customCountyRepository.findOneByNameIgnoreCase(vm.getpCounty());

		// Save producer address
		AddressDTO pAddr = null;

		City pCity1 = new City();
		pCity1 = pCity;
		if (pCity == null || (pCity != null && pCity.getId() == null)) {
			pCity1 = new City();
			pCity1.setName(vm.getpCity().trim());
			pCity1.setStateCode(vm.getpState().trim());
			pCity1 = customCityRepository.save(pCity1);
		}

		State pState1 = new State();
		pState1 = pState;
		if (pState == null || (pState != null && pState.getId() == null)) {
			pState1 = new State();
			pState1.setStateCode(vm.getpState().trim());
			pState1.setName(vm.getpState().trim());
			pState1 = customStateRepository.save(pState1);
		}

		County pCounty1 = new County();
		pCounty1 = pCounty;
		if (vm.getpCounty() != null && (pCounty == null || (pCounty != null && pCounty.getId() == null))) {
			pCounty1 = new County();
			pCounty1.setStateCode(vm.getpState().trim());
			pCounty1.setName(vm.getpCounty().trim());
			pCounty1 = customCountyRepository.save(pCounty1);
		}

		if (pCounty == null || (pCounty != null && pCounty == null)) {
			pAddr = AgentCSVMapper.producerAddressFromVM(vm, pCity1.getId(), pState1.getId(), null);
		} else {
			pAddr = AgentCSVMapper.producerAddressFromVM(vm, pCity1.getId(), pState1.getId(), pCounty1.getId());
		}

		if (pAddrEdit != null) {
			pAddr.setId(pAddrEdit.getId());
			pAddr.setCreatedAt(pAddrEdit.getCreatedAt());
		}
		pAddr = addressService.save(pAddr);

		// Save Producer
		ProducerDTO pDto = AgentCSVMapper.producerFromVM(vm, pAddr.getId());
		if (proEdit != null) {
			pDto.setId(proEdit.getId());
		}
		pDto = producerService.save(pDto);

		// Save license details
		AgentLicDTO agentLicDto = AgentCSVMapper.agentLicFromVM(vm, pDto.getId());
		if (agLicEdit != null) {
			agentLicDto.setId(agLicEdit.getId());
			agentLicDto.setIsActive(true);
			agentLicDto.setCreatedAt(agLicEdit.getCreatedAt());
		}
		agentLicDto = agentLicService.save(agentLicDto);

		// Save business address
		City bCity = customCityRepository.findOneByNameIgnoreCase(vm.getbCity());
		State bState = customStateRepository.findOneByStateCodeIgnoreCase(vm.getbState());
		County bCounty = customCountyRepository.findOneByNameIgnoreCase(vm.getbCounty());

		City bCity1 = new City();
		bCity1 = bCity;
		if (bCity == null || (bCity != null && bCity.getId() == null)) {
			bCity1 = new City();
			bCity1.setName(vm.getbCity().trim());
			bCity1.setStateCode(vm.getbState());
			bCity1 = customCityRepository.save(bCity1);
		}

		State bState1 = new State();
		bState1 = bState;
		if (bState == null || (bState != null && bState.getId() == null)) {
			bState1 = new State();
			bState1.setStateCode(vm.getbState().trim());
			bState1.setName(vm.getbState());
			bState1 = customStateRepository.save(bState1);
		}

		County bCounty1 = new County();
		bCounty1 = bCounty;
		if (vm.getbCounty() != null && (bCounty == null || (bCounty != null && bCounty.getId() == null))) {
			bCounty1 = new County();
			bCounty1.setStateCode(vm.getbState());
			bCounty1.setName(vm.getbCounty().trim());
			bCounty1 = customCountyRepository.save(bCounty1);
		}

		String zipcode = vm.getbZip();
		String[] zipArr = zipcode.split("-");
		for (String zip : zipArr) {
			ZipArea bZip = customZipAreaRepository.findOneByZip(zip);
			if (bZip == null || bZip.getId() == null) {
				bZip = new ZipArea();
				bZip.setZip(zip);
				bZip.setName(vm.getbCity().trim());

				bZip = customZipAreaRepository.save(bZip);
			}
		}

		AddressDTO bAddr;
		if (bCounty == null || (bCounty != null && bCounty.getId() == null)) {
			bAddr = AgentCSVMapper.businessAddressFromVM(vm, bCity1.getId(), bState1.getId(), null);
		} else {
			bAddr = AgentCSVMapper.businessAddressFromVM(vm, bCity1.getId(), bState1.getId(), bCounty1.getId());
		}

		if (bAddrEdit != null) {
			bAddr.setId(bAddrEdit.getId());
			bAddr.setCreatedAt(bAddrEdit.getCreatedAt());
		}
		bAddr = addressService.save(bAddr);

		// Save business Contact
		ContactDTO cDto = AgentCSVMapper.businessContactFromVM(vm, bAddr.getId());
		if (cont != null) {
			cDto.setId(cont.getId());
		}
		cDto = contactService.save(cDto);

		// Save business
		BusinessDTO bDto = AgentCSVMapper.businessDtoFromVM(vm, bAddr.getId());
		if (bsEdit != null) {
			bDto.setId(bsEdit.getId());
			bDto.setCreatedAt(bsEdit.getCreatedAt());
		}
		bDto = businessService.save(bDto);

		// Save profile
		ProfileDTO profileDto = AgentCSVMapper.profileDtoFromVM(vm, bAddr.getId());
		if (prEdit != null) {
			profileDto = prEdit;
			
			profileDto.setPrefEmail(vm.getbEmail());
			profileDto.setFirstName(vm.getFirstName());
			profileDto.setMiddleName(vm.getMiddleName());
			profileDto.setLastName(vm.getLastName());
			profileDto.setSuffix(vm.getSuffix());
			
			profileDto.setIsSpeaksSpanish(prEdit.isIsSpeaksSpanish());
			profileDto.setPhone(prEdit.getPhone());
			profileDto.setPrefMobile(prEdit.getPrefMobile());
			profileDto.setUserId(prEdit.getUserId());
			profileDto.setId(prEdit.getId());
			profileDto.setCreatedAt(prEdit.getCreatedAt());
			profileDto.setSecondPhone(prEdit.getSecondPhone());
			
			profileDto.setInsuranceCompany(vm.getInsuranceCompany());
		}
		profileDto = profileService.save(profileDto);

		
		if(vm.getInsuranceCompany()!=null) {
			AvailableCarriers  carr = availableCarriersRepository.findByCarrierAndShow(vm.getInsuranceCompany(),false);
			if(carr == null) {
				AvailableCarriers c = new AvailableCarriers();
				c.setCarrier(vm.getInsuranceCompany().toString());
				c.setShowInList(false);
				availableCarriersRepository.save(c);
			}
			
			carr = availableCarriersRepository.findByCarrierAndShow(vm.getInsuranceCompany(),false);
			if(carr!= null) {
				ProfileCarriers pc = profileCarriersRepository.findPrimaryCompanyOfProfile(profileDto.getId());
				if(pc == null) {
					 pc = new ProfileCarriers();
				}
//				ProfileCarriers pc = new ProfileCarriers();
				pc.setProfileId(profileDto.getId());
				pc.setCarriersId(carr.getId());
				pc.setIsPrimary(true);
				profileCarriersRepository.save(pc);
			}

		}
		

		// Save agent
		AgentDTO aDto = AgentCSVMapper.agentDtoFromVM(vm, bDto.getId(), agentLicDto.getId(), profileDto.getId(),
				bState1.getId());
		if (agent != null) {
			aDto.setId(agent.getId());
			aDto.setCreatedAt(agent.getCreatedAt());
		/*	if (aDto.getExtension() == null) {
				aDto.setExtension(aDto.getNpn());
			}
			if (aDto.getExtension() != null && aDto.getExtension().isEmpty()) {
				aDto.setExtension(aDto.getNpn());
			}*/
		}
/*
		if (aDto.getExtension() == null) {
			aDto.setExtension(aDto.getNpn());
		}

		if (aDto.getExtension() != null && aDto.getExtension().isEmpty()) {
			aDto.setExtension(aDto.getNpn());
		}*/
		aDto = agentService.save(aDto);
		
		
		Agent a = customAgentRepository.findOneByNpn(vm.getNpn());
		
		AgentExtension ae = agentExtensionRepository.findByAgentAndPrimary(a.getId());
		if(ae!=null) {
			if (vm.getExtension() != null && !vm.getExtension().isEmpty()) {
				ae.setExtension(vm.getExtension());
				agentExtensionRepository.save(ae);
			}
			if (vm.getExtension() == null || vm.getExtension().isEmpty()) { 
				ae.setExtension(aDto.getNpn());
				agentExtensionRepository.save(ae);
			}
			
			if (vm.getSecondExtension() != null && !vm.getSecondExtension().isEmpty()) {
				AgentExtension ae1 = agentExtensionRepository.findByAgentAndSecondary(a.getId());
				if(ae1!=null) {
					ae1.setExtension(aDto.getNpn());
					agentExtensionRepository.save(ae1);
				}else {
					ae1 = new AgentExtension();
					ae1.setAgent(a);
					ae1.setExtension(vm.getSecondExtension());
					ae1.setIsPrimary(false);
					agentExtensionRepository.save(ae1);
				}
			}
			if (vm.getSecondExtension() != null && vm.getSecondExtension().isEmpty()) {
				/*AgentExtension ae1 = agentExtensionRepository.findByAgentAndSecondary(a.getId());
				if(ae1!=null) {
					ae1.setExtension(ae.getExtension());
					agentExtensionRepository.save(ae1);
				}*/
			}
			
			
		}else {
			AgentExtension ae1 = new AgentExtension();
			ae1.setAgent(a);
			ae1.setIsPrimary(true);
			if (vm.getExtension() != null && !vm.getExtension().isEmpty()) {
				ae1.setExtension(vm.getExtension());
			}else {
				ae1.setExtension(aDto.getNpn());
			}
			agentExtensionRepository.save(ae1);
			
			if (vm.getSecondExtension() != null && !vm.getSecondExtension().isEmpty()) {
				AgentExtension ae2 = new AgentExtension();
				ae2.setAgent(a);
				ae2.setIsPrimary(false);
				ae2.setExtension(vm.getSecondExtension());
				agentExtensionRepository.save(ae2);
			}
			
			if (vm.getSecondExtension() != null && vm.getSecondExtension().isEmpty()) {
			/*	AgentExtension ae2 = new AgentExtension();
				ae2.setAgent(a);
				ae2.setIsPrimary(false);
				ae2.setExtension(ae1.getExtension());
				agentExtensionRepository.save(ae2);*/
			}
		}
	
		// upload the agent to elastic-search
		// writeAgent2ElasticIndex(vm, aDto.getId());
	}

	@SuppressWarnings("unchecked")
	private boolean writeAgent2ElasticIndex(AgentCsvBean vm, Long agentId) {
		// upload the agent to elastic-search
		String city = "", state = "";
		if (vm.getbCity() != null) {
			city = vm.getbCity();
		}
		if (vm.getbState() != null) {
			state = vm.getbState();
		}
		com.mv1.iaa.business.view.search.Agent value = AgentCSVMapper.agentSearchVM(vm, city, agentId, state);
		boolean isWritten = indexWriter.write2Elasticsearch((T) value, agentId);
		if (!isWritten) {
			log.debug("Failed to write agent in search index ");
		}
		return isWritten;
	}

	@SuppressWarnings("unchecked")
	private boolean write2ElasticIndex(FacilityCsvBean vm, Long facilityId) {
		// upload the facility to elastic-search
		if (vm.getbZip() == null) {
			// set default zip
			vm.setbZip("27502");
		}

		com.mv1.iaa.business.view.search.Facility value = CommonMapper.facilitySearchVM(vm, facilityId);
		boolean isWritten = indexWriter.write2Elasticsearch((T) value, facilityId);
		if (!isWritten) {
			log.debug("Failed to write agent in search index ");
		}
		return isWritten;
	}

	public CsvResp mapAgentCsv(String path) throws Exception {

		List<AgentCsvBean> beans = Util.readCSV(path, AgentCsvBean.class);
		validateAgentCsv(beans);
		return bulkProcessAgents(beans);
		// return new CsvResp();
	}

	public Date convertToDate(String d, String patternMessage, String fieldName,
			List<com.mv1.iaa.business.view.Error> errors) throws Mv1Exception {
		if (d == null || d == "" || d.isEmpty()) {
			return null;
		}

		String arr[] = d.split("/");
		String ddate = d;
		if (arr[2].length() == 2) {
			// String year = (new Date().getYear()) + "";
			String year = Calendar.getInstance().get(Calendar.YEAR) + "";

			ddate = arr[0] + "/" + arr[1] + "/" + year.charAt(0) + "" + year.charAt(1) + arr[2];
		}
		// DateFormat format = new SimpleDateFormat("MM-dd-yyyy",
		// Locale.ENGLISH);
		String[] DATE_FORMATS = new String[] {
				// "MM/dd/yy",
				"MM/dd/yyyy" };

		if (ddate != null && !ddate.isEmpty()) {
			for (String DATE_FORMAT : DATE_FORMATS) {
				try {
					return new SimpleDateFormat(DATE_FORMAT).parse(ddate);
				} catch (ParseException e) {

					// List<com.mv1.iaa.business.view.Error> errors = new
					// ArrayList<>();
					com.mv1.iaa.business.view.Error error = new com.mv1.iaa.business.view.Error();
					error.setFieldName(fieldName);
					error.setValid(false);
					error.setMessage(patternMessage + "-" + d);
					errors.add(error);

					if (errors.size() > 0) {
						throw new Mv1Exception(errors);
					}

				}
			}

		}
		return null;

	}

	public void validateAgentCsv(List<AgentCsvBean> beans) throws Mv1Exception {
		for (AgentCsvBean agentCsvBean : beans) {
			CsvValidator.validate(5, 9, "[0-9]+", "Invalid Lic Number", agentCsvBean.getLicNumber(),
					AgentEnum.LIC_NUMBER.getValue());
			CsvValidator.validate(5, 9, "[0-9]+", "Invalid NPN", agentCsvBean.getNpn(), AgentEnum.NPN.getValue());
			CsvValidator.validate(2, 30, "[a-zA-Z0-9,-@!&#$'. ]+", "Invalid Last Name", agentCsvBean.getLastName(),
					AgentEnum.LAST_NAME.getValue());
			CsvValidator.validate(1, 30, "[a-zA-Z .'-]+", "Invalid First Name", agentCsvBean.getFirstName(),
					AgentEnum.FIRST_NAME.getValue());
			CsvValidator.validate(1, 30, "[a-zA-Z .'-]+", "Invalid Middle Name", agentCsvBean.getMiddleName(),
					AgentEnum.MIDDLE_NAME.getValue());
			CsvValidator.validate(1, 5, "[a-zA-Z .#']+", "Invalid Suffix", agentCsvBean.getSuffix(),
					AgentEnum.SUFFIX.getValue());
			CsvValidator.validate(2, 300, "[a-zA-Z0-9 ._+$&,-.'!*()@#']+", "Invalid Business Entity Name",
					agentCsvBean.getBusinessName(), AgentEnum.BUSINESS_ENTITY_NAME.getValue());
			CsvValidator.validate(1, 10, "[a-zA-Z0-9 .#']+", "Invalid fein", agentCsvBean.getFein(),
					AgentEnum.FEIN.getValue());
			CsvValidator.validate(5, 30, "[a-zA-Z0-9 .#']+", "Invalid Licence Type", agentCsvBean.getLicType(),
					AgentEnum.LICENSE_TYPE.getValue());
			CsvValidator.validate(2, 300, "[a-zA-Z0-9 $@&*()^!.,#']+", "Invalid Line Of Authority",
					agentCsvBean.getLineAuth(), AgentEnum.LINE_OF_AUTHORITY.getValue());
			CsvValidator.validate(5, 8, "[a-zA-Z]+", "Invalid Licence Status", agentCsvBean.getLicStatus(),
					AgentEnum.LICENSE_STATUS.getValue());

			List<com.mv1.iaa.business.view.Error> errors = new ArrayList<>();
			agentCsvBean.setFirstActiveDateNew(convertToDate(agentCsvBean.getFirstActiveDate(),
					"Invalid First Active Date", AgentEnum.FIRST_ACTIVE_DATE.getValue(), errors));
			agentCsvBean.setEffectiveDateNew(convertToDate(agentCsvBean.getEffectiveDate(), "Invalid Effective Date",
					AgentEnum.EFFECTIVE_DATE.getValue(), errors));
			agentCsvBean.setExpDateNew(convertToDate(agentCsvBean.getExpDate(), "Invalid Expiration Date",
					AgentEnum.EXPIRATION_DATE.getValue(), errors));

			System.out.println("agentCsvBean.getFirstActiveDate()=============" + agentCsvBean.getFirstActiveDateNew());
			/*
			 * agentCsvBean.setFirstActiveDate(convertToDate(agentCsvBean.
			 * getFirstActiveDate()));
			 * agentCsvBean.setEffectiveDate(convertToDate(agentCsvBean.
			 * getEffectiveDate()));
			 * agentCsvBean.setExpDate(convertToDate(agentCsvBean.getExpDate()))
			 * ;
			 */

			// CsvValidator.validate(0, 0, "[0-9]{1,2}/[0-9]{1,2}/[0-9]{2,4}",
			// "Invalid First Active Date",
			// agentCsvBean.getFirstActiveDateNew(),AgentEnum.FIRST_ACTIVE_DATE.getValue());
			// CsvValidator.validate(0, 0, "[0-9]{1,2}/[0-9]{1,2}/[0-9]{2,4}",
			// "Invalid Effective Date",
			// agentCsvBean.getEffectiveDateNew(),AgentEnum.EFFECTIVE_DATE.getValue());
			// CsvValidator.validate(0, 0, "[0-9]{1,2}/[0-9]{1,2}/[0-9]{2,4}",
			// "Invalid Expiration Date",
			// agentCsvBean.getExpDateNew(),AgentEnum.EXPIRATION_DATE.getValue());
			// [0-9]{1,2}/[0-9]{1,2}/[0-9]{2,4}

			CsvValidator.validate(2, 30, "[a-zA-Z]+", "Invalid Domicial Estate", agentCsvBean.getDomState(),
					AgentEnum.DOMICILESTATE.getValue());
			CsvValidator.validate(1, 1, "[YNyn]+", "Invalid Is Resident", agentCsvBean.getResState(),
					AgentEnum.IS_RESIDENT.getValue());
			CsvValidator.validate(1, 100, "[a-zA-Z0-9 .,-@$&!:-_#%()']+", "Invalid MLG Address1", agentCsvBean.getpAddr1(),
					AgentEnum.BUS_ADDRESS1.getValue());
			CsvValidator.validate(1, 50, "[a-zA-Z0-9 .,-@$&!:-_#%()']+", "Invalid MLG Address2", agentCsvBean.getpAddr2(),
					AgentEnum.BUS_ADDRESS2.getValue());
			CsvValidator.validate(1, 50, "[a-zA-Z0-9 .,-@$&!:-_#%()']+", "Invalid MLG Address3", agentCsvBean.getpAddr3(),
					AgentEnum.MLG_ADDRESS3.getValue());
			CsvValidator.validate(2, 30, "[a-zA-Z .,#'-]+", "Invalid MLG City", agentCsvBean.getpCity(),
					AgentEnum.MLG_CITY.getValue());
			CsvValidator.validate(2, 30, "[a-zA-Z .,#'-]+", "Invalid MLG State", agentCsvBean.getpState(),
					AgentEnum.MLG_STATE.getValue());
			CsvValidator.validate(5, 15, "[0-9]{5}", "Invalid MLG Zip", agentCsvBean.getpZip(),
					AgentEnum.MLG_ZIP.getValue());
			CsvValidator.validate(2, 30, "[a-zA-Z .,#'-]+", "Invalid MLG Country", agentCsvBean.getpCountry(),
					AgentEnum.MLG_COUNTRY.getValue());

			CsvValidator.validate(1, 100, "[a-zA-Z0-9 .,-@$&!:%#'()]+", "Invalid BUS Address1", agentCsvBean.getbAddr1(),
					AgentEnum.BUS_ADDRESS1.getValue());
			CsvValidator.validate(1, 50, "[a-zA-Z0-9 .,-@$&!:#%'()]+", "Invalid BUS Address2", agentCsvBean.getbAddr2(),
					AgentEnum.BUS_ADDRESS2.getValue());
			CsvValidator.validate(1, 50, "[a-zA-Z0-9 .,-@$&!:#%'()]+", "Invalid BUS Address3", agentCsvBean.getbAddr3(),
					AgentEnum.BUS_ADDRESS3.getValue());
			CsvValidator.validate(2, 30, "[a-zA-Z .,#'-]+", "Invalid BUS City", agentCsvBean.getbCity(),
					AgentEnum.BUS_CITY.getValue());
			CsvValidator.validate(2, 30, "[a-zA-Z .,#'-]+", "Invalid BUS State", agentCsvBean.getbState(),
					AgentEnum.BUS_STATE.getValue());
			CsvValidator.validate(5, 15, "[0-9]{5}", "Invalid BUS Zip", agentCsvBean.getbZip(),
					AgentEnum.BUS_ZIP.getValue());
			CsvValidator.validate(2, 30, "[a-zA-Z .,#'-]+", "Invalid BUS Country", agentCsvBean.getbCountry(),
					AgentEnum.BUS_COUNTRY.getValue());

			CsvValidator.validate(10, 16, "[a-zA-Z0-9 .()-]+", "Invalid BUS Phone", agentCsvBean.getbPhone(),
					AgentEnum.BUS_PHONE.getValue());
			CsvValidator.validate(10, 50,
					"^[a-zA-z0-9]+([.-]?[a-zA-z0-9+.]+)*@[a-zA-z0-9]+([.-]?[a-zA-z0-9]+)*(.[a-zA-z0-9]{2,3})+$",
					"Invalid BUS Email", agentCsvBean.getbEmail(), AgentEnum.BUS_EMAIL.getValue());
			CsvValidator.validate(1, 1, "[YNyn]+", "Invalid CE Compliancee", agentCsvBean.getIsCECompliant(),
					AgentEnum.CE_COMPLIANCE.getValue());
			CsvValidator.validate(1, 10, "[a-zA-Z .,#']+", "Invalid NAICNO", agentCsvBean.getNaicno(),
					AgentEnum.NAICNO.getValue()); // specialc chars
			// a-zA-Z0-9#@!$_*&^%-_.()'
		}
	}

	public CsvResp mapFacilityCsv(String path) throws Exception {
		// try {
		List<FacilityCsvBean> beans = Util.readCSV(path, FacilityCsvBean.class);
		validateFacilityCsv(beans);
		return bulkProcessFacility(beans);
		/*
		 * } catch (Exception cause) {
		 * log.debug("Processing csv upload is failed ", cause); } return new
		 * CsvResp();
		 */
	}

	public void validateFacilityCsv(List<FacilityCsvBean> beans) throws Mv1Exception {
		for (FacilityCsvBean facilityCsvBean : beans) {
			CsvValidator.validate(2, 100, "[a-zA-Z0-9,-@!&#$'+. ]+", "Invalid Name", facilityCsvBean.getName(),
					FacilityEnum.NAME.getValue());
			CsvValidator.validate(2, 100, "[a-zA-Z0-9 .,-@&$!:#']+", "Invalid Address", facilityCsvBean.getAddress(),
					FacilityEnum.ADDRESS.getValue());
			CsvValidator.validate(2, 30, "[a-zA-Z .,#']+", "Invalid City", facilityCsvBean.getCity(),
					FacilityEnum.CITY.getValue());
			CsvValidator.validate(5, 15, "[0-9]{5}", "Invalid Zip", facilityCsvBean.getZipCode(),
					FacilityEnum.ZIP_CODE.getValue());
			CsvValidator.validate(1, 15, "[a-zA-Z0-9 :-]+", "Invalid Week Day Hours", facilityCsvBean.getWeekDayHours(),
					FacilityEnum.WEEKDAY_HRS.getValue());
			CsvValidator.validate(1, 15, "[a-zA-Z0-9 :-]+", "Invalid Saturday Hours",
					facilityCsvBean.getSaturdayHours(), FacilityEnum.SATURDAY_HRS.getValue());
			CsvValidator.validate(1, 15, "[a-zA-Z0-9 :-]+", "Invalid Sunday Hours", facilityCsvBean.getSundayHours(),
					FacilityEnum.SUNDAT_HRS.getValue());
			CsvValidator.validate(10, 16, "[a-zA-Z0-9 .()-]+", "Invalid Phone", facilityCsvBean.getPhone(),
					FacilityEnum.PHONE.getValue());
		}

	}

	@Timed
	@Transactional
	private CsvResp bulkProcessFacility(List<FacilityCsvBean> beans) throws Exception {

		log.debug("processing bulkProcessFacility... ");

		int insertCount = 0, updateCount = 0, rec = 1;
		CsvResp resp = new CsvResp();

		for (FacilityCsvBean vm : beans) {
			Facility facility = new Facility();
			if (vm.getIsn() != null && vm.getIsn() != "") {
				facility = customFacilityRepository.findOneByIsn(vm.getIsn());
			} else {
				facility.setCreatedAt(ZonedDateTime.now());
			}
			if (facility != null) {
				updateCount += 1;
			} else {
				insertCount += 1;
			}
			upsertFacility(vm, facility);
			rec += 1;
		}

		resp.setInsertCount(insertCount);
		resp.setUpdateCount(updateCount);

		return resp;
	}

	private void upsertFacility(FacilityCsvBean vm, Facility facility) throws Exception {

		Business business = null;
		Address address = null;
		ProfileDTO prEdit = null;

		if (facility != null && facility.getId() != null) {
			Optional<FacilityDTO> facilityOpt = facilityService.findOne(facility.getId());
			FacilityDTO facilityEdit = facilityOpt.get();

			Optional<ProfileDTO> prfOpt = profileService.findOne(facilityEdit.getProfileId());
			prEdit = prfOpt.get();
		}

		if (facility == null) {
			facility = new Facility();
			facility.setCreatedAt(ZonedDateTime.now());
		}

		if (facility.getBusiness() != null) {
			business = facility.getBusiness();
			address = business.getAddress();
			if (address == null) {
				address = new Address();
				address.setCreatedAt(ZonedDateTime.now());
			}
		} else {
			address = new Address();
			address.setCreatedAt(ZonedDateTime.now());

			business = new Business();
			business.setCreatedAt(ZonedDateTime.now());
		}

		Contact contact = null;
		if (address.getId() != null) {
			contact = customContactRepository.findOneByAddressId(address.getId());
		} else {
			contact = new Contact();
		}

		City city = customCityRepository.findOneByNameIgnoreCase(vm.getCity().trim());
		if (Objects.isNull(city)) {
			city = new City();
			city.setName(vm.getCity().trim());
			city = customCityRepository.save(city);
		}

		County county = customCountyRepository.findOneByNameIgnoreCase(vm.getbCounty().trim());
		if (Objects.isNull(county)) {
			county = new County();
			county.setName(vm.getbCounty().trim());
			county = customCountyRepository.save(county);
		}

		State state = customStateRepository.findOneByNameIgnoreCase(vm.getbState().trim());
		if (Objects.isNull(county)) {
			state = new State();
			state.setName(vm.getbState().trim());
			state = customStateRepository.save(state);
		}

		address.setCity(city);
		address.setCounty(county);
		address.setState(state);
		address.setZip(vm.getZipCode() != null ? vm.getZipCode().trim() : "");
		address.setUpdatedAt(ZonedDateTime.now());
		address.setAddr1(vm.getAddress().trim());

		address = customAddressRepository.save(address);

		contact.setOfficePhone(vm.getbPhone());
		contact.setPhone(vm.getPhone());
		contact.setEmail(vm.getbEmail());
		contact.setAddress(address);

		contact = customContactRepository.save(contact);

		// Save business
		business.setUpdatedAt(ZonedDateTime.now());
		business.setBusinessName(vm.getName());
		// business.setBusinessName(vm.getBusinessName());
		business.setAddress(address);

		business = businessRepository.save(business);

		// Save profile
		ProfileDTO profileDto = AgentCSVMapper.profileDtoFromFacilityVM(vm, address.getId());
		if (prEdit != null) {
			profileDto = prEdit;
			profileDto.setIsSpeaksSpanish(prEdit.isIsSpeaksSpanish());
			profileDto.setPhone(vm.getbPhone());
			profileDto.setPrefMobile(prEdit.getPrefMobile());
			profileDto.setUserId(prEdit.getUserId());
			profileDto.setId(prEdit.getId());
			profileDto.setCreatedAt(prEdit.getCreatedAt());
			profileDto.setFirstName(prEdit.getFirstName());
			profileDto.setLastName(prEdit.getLastName());
			profileDto.setPrefEmail(prEdit.getPrefEmail());
			profileDto.setRefBy(prEdit.getRefBy());
			profileDto.setType(prEdit.getType());
		}

		profileDto = profileService.save(profileDto);

		Optional<Profile> profile = customProfileRepository.findById(profileDto.getId());
		Profile pr = profile.get();

		facility.setIsn(vm.getIsn());
		facility.setSaturdayHours(vm.getSaturdayHours());
		facility.setSundayHours(vm.getSundayHours());
		facility.setWeekdayHours(vm.getWeekDayHours());
		facility.setName(StringUtils.hasText(vm.getName()) ? vm.getName().trim() : facility.getName());
		facility.updatedAt(ZonedDateTime.now());
		facility.setIsActive(false);
		facility.setIsDeleted(false);
		facility.setIsVerified(false);
		facility.setBusiness(business);
		facility.setProfile(pr);
		facility.setMechDuty(vm.getMechDuty());
		facility.setInspType(vm.getInspType());
		if (vm.getExtension() != null)
			facility.setExtension(vm.getExtension());
		
		if (facility != null) {
			if (vm.getExtension() == null) {
				facility.setExtension(vm.getIsn());
			}
			if (vm.getExtension() != null && vm.getExtension().isEmpty()) {
				facility.setExtension(vm.getIsn());
			}
		}

		if (facility.getExtension() == null) {
			facility.setExtension(facility.getIsn());
		}

		if (facility.getExtension() != null && facility.getExtension().isEmpty()) {
			facility.setExtension(facility.getIsn());
		}
		
		facility = customFacilityRepository.save(facility);

		// upload the facility to elastic-search
		// write2ElasticIndex(vm, facility.getId());
	}

	public CsvResp deleteAgentCsv(String path) {

		try {
			List<AgentCsvBean> beans = Util.readCSV(path, AgentCsvBean.class);
			Set<String> agentNpn = beans.stream().map(b -> b.getNpn()).collect(Collectors.toSet());
			CsvResp resp = new CsvResp();
			Integer count = agentComponent.deleteAgent(agentNpn);
			if (count > 0) {
				resp.setDeleteCount(count);
			}
			return resp;
		} catch (Exception e) {
			log.debug("Processing csv upload is failed ", e);
		}
		return new CsvResp();
	}

	public File downloadAgentList(String type) {
		if (type.equalsIgnoreCase("agent")) {
			String[] col = new String[] { "LICENSE_NUM", "NPN", "LAST_NAME", "FIRST_NAME", "MIDDLE_NAME", "SUFFIX",
					"BUSINESS_ENTITY_NAME", "FEIN", "LICENSE_TYPE", "LINE_OF_AUTHORITY", "LICENSE_STATUS",
					"FIRST_ACTIVE_DATE", "EFFECTIVE_DATE", "EXPIRATION_DATE", "DOMICILESTATE", "IS_RESIDENT",
					"MLG_ADDRESS1", "MLG_ADDRESS2", "MLG_ADDRESS3", "MLG_CITY", "MLG_STATE", "MLG_ZIP", "MLG_COUNTRY",
					"BUS_ADDRESS1", "BUS_ADDRESS2", "BUS_ADDRESS3", "BUS_CITY", "BUS_STATE", "BUS_ZIP", "BUS_COUNTRY",
					"BUS_PHONE", "BUS_EMAIL", "CE_COMPLIANCE", "NAICNO" };
			List<AgentCsvSampleVM> list = new ArrayList();
			setAgentData(list);
			return customAgentFilterSeviceImpl.writeCSV(col, "agent_upload_sample_", null, AgentCsvSampleVM.class,
					"csv");
		} else {

			String[] col = new String[] { "NAME", "ADDRESS", "CITY", "ZIP CODE", "WEEKDAY HRS", "SATURDAY HRS",
					"SUNDAY HRS", "PHONE", "ISN" };

			return customAgentFilterSeviceImpl.writeCSV(col, "facility_upload_sample_", null, FacilityCsvSampleVM.class,
					"csv");
		}

	}

	public List<AgentCsvSampleVM> setAgentData(List<AgentCsvSampleVM> list) {
		return null;
	}

}
