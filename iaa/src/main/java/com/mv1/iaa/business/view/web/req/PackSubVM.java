package com.mv1.iaa.business.view.web.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PackSubVM {
	private String zip;
	private String packageId;
	private Integer slots;
	private String amount;

	public String getZip() {
		return zip;
	}

	public String getPackageId() {
		return packageId;
	}

	public Integer getSlots() {
		return slots;
	}

	public String getAmount() {
		return amount;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public void setSlots(Integer slots) {
		this.slots = slots;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PackSubVM [zip=" + zip + ", packageId=" + packageId + ", slots=" + slots + ", amount=" + amount + "]";
	}
	
}
