package com.mv1.iaa.business.view.web.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.opencsv.bean.CsvBindByPosition;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FacilityVM {

    private static final String[] keys = new String[] {
            "id",           
            "profileId",
            "name",         
            "weekDayHours", 
            "saturdayHours",
            "sundayHours",  
            "isn",  
            "zipCode",      
            "address",              
            "firstName",  
            "lastName",      
            "mobile",      
            "email",         
            "phone",
            "bPhone",
            "city" ,
            "incAB",
            "certified",
            "preCertified",
            "paymentDate",
		    "county",
		    "state",
		    "mech_duty",
		    "insp_type",
		    "businessName",
		    "bEmail"
           
            
       };
         
   
    

    
    private static final String[] keys1 = new String[] {
    
		    "id",           
		    "profileId",
		    "name",         
		    "weekDayHours", 
		    "saturdayHours",
		    "sundayHours",  
		    "zipCode",      
		    "address",
		    "city" ,
		    "bPhone",
		    "mobile",
		    "isn",  
		    "state",
		    "county",
		    "bEmail",
		    "mech_duty",
		    "insp_type",
		    "firstName",  
		    "lastName",      
		    "prefMobile",   
		    "prefPhone",
		    "prefEmail",         
		    "speaks_spanish" ,
		    "prefCompany",
		    "comments"
    };
/*    private static final String[] keys1 = new String[] {
            "id",  
            "isn", 
            "firstName",  
            "lastName",      
            "mobile",      
            "email",         
            "phone",
            "incAB",
            "certified",
            "preCertified",
            "paymentDate"
       };*/
         
    
    public static String[] getKeys() {
        return keys;
    }

    private Long id;

    private Long profileId;

    private String name;

    private String address;

    private String city;

    @CsvBindByPosition(position = 3)
    private String zipCode;

    @CsvBindByPosition(position = 8)
    private String weekDayHours;

    @CsvBindByPosition(position = 9)
    private String saturdayHours;

    @CsvBindByPosition(position = 10)
    private String sundayHours;

    @CsvBindByPosition(position = 11)
    private String phone;

    @CsvBindByPosition(position = 13)
    private String bPhone;

    @CsvBindByPosition(position = 15)
    private String bEmail;

    private String bCounty;

    @CsvBindByPosition(position = 2)
    private String isn;

    private String type;

    private String contactName;

    private String cPhone;

    private String refBy;
    
    @CsvBindByPosition(position = 0)
    private String firstName;
    
    @CsvBindByPosition(position = 1)
    private String lastName;
    
    private boolean incAB;
    
    private boolean certified;
    
    private boolean preCertified;
    
    private String paymentDate;
    
    @CsvBindByPosition(position = 7)
    private String incStatus;
    
    @CsvBindByPosition(position = 4)
    private String incABName;

    @CsvBindByPosition(position = 5)
    private String certifiedName;
    
    @CsvBindByPosition(position = 6)
    private String preCertifiedName;
    
    private String insp_type;
    private String mech_duty;
    
    private String state;
    private String county;
    
    private boolean speaks_spanish ;
    
    
    private String prefPhone;
    private String prefMobile;
    private String prefEmail;
    private String prefCompany;
    private String comments;
    
    @CsvBindByPosition(position = 12)
    private String mobile;
    
    @CsvBindByPosition(position = 14)
    private String businessName;

    private String addr1;

    private String addr2;

    private String addr3;

    private String bCity;

    private String bState;

    private String bZip;
    

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name != null ? name.trim() : name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address != null ? address.trim() : address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city != null ? city.trim() : city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode != null ? zipCode.trim() : zipCode;
    }

    public String getWeekDayHours() {
        return weekDayHours;
    }

    public void setWeekDayHours(String weekDayHours) {
        this.weekDayHours = weekDayHours != null ? weekDayHours.trim() : weekDayHours;
    }

    public String getSaturdayHours() {
        return saturdayHours;
    }

    public void setSaturdayHours(String saturdayHours) {
        this.saturdayHours = saturdayHours != null ? saturdayHours.trim() : saturdayHours;
    }

    public String getSundayHours() {
        return sundayHours;
    }

    public void setSundayHours(String sundayHours) {
        this.sundayHours = sundayHours != null ? sundayHours.trim() : sundayHours;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone != null ? phone.trim() : phone;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getAddr3() {
        return addr3;
    }

    public void setAddr3(String addr3) {
        this.addr3 = addr3;
    }

    public String getbCity() {
        return bCity;
    }

    public void setbCity(String bCity) {
        this.bCity = bCity;
    }

    public String getbState() {
        return bState;
    }

    public void setbState(String bState) {
        this.bState = bState;
    }

    public String getbZip() {
        return bZip;
    }

    public void setbZip(String bZip) {
        this.bZip = bZip;
    }

    public String getbPhone() {
        return bPhone;
    }

    public void setbPhone(String bPhone) {
        this.bPhone = bPhone;
    }

    public String getbEmail() {
        return bEmail;
    }

    public void setbEmail(String bEmail) {
        this.bEmail = bEmail;
    }

    public String getbCounty() {
        return bCounty;
    }

    public void setbCounty(String bCounty) {
        this.bCounty = bCounty;
    }

    public String getIsn() {
        return isn;
    }

    public void setIsn(String isn) {
        this.isn = isn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getcPhone() {
        return cPhone;
    }

    public void setcPhone(String cPhone) {
        this.cPhone = cPhone;
    }

    public String getRefBy() {
        return refBy;
    }

    public void setRefBy(String refBy) {
        this.refBy = refBy;
    }
    

    public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	

	public static String[] getKeys1() {
		return keys1;
	}

	public boolean isIncAB() {
		return incAB;
	}

	public boolean isCertified() {
		return certified;
	}

	public boolean isPreCertified() {
		return preCertified;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public String getIncStatus() {
		return incStatus;
	}

	public void setIncAB(boolean incAB) {
		this.incAB = incAB;
	}

	public void setCertified(boolean certified) {
		this.certified = certified;
	}

	public void setPreCertified(boolean preCertified) {
		this.preCertified = preCertified;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public void setIncStatus(String incStatus) {
		this.incStatus = incStatus;
	}
	
	public String getIncABName() {
		return incABName;
	}

	public void setIncABName(String incABName) {
		this.incABName = incABName;
	}
	

	public String getCertifiedName() {
		return certifiedName;
	}

	public String getPreCertifiedName() {
		return preCertifiedName;
	}

	public void setCertifiedName(String certifiedName) {
		this.certifiedName = certifiedName;
	}

	public void setPreCertifiedName(String preCertifiedName) {
		this.preCertifiedName = preCertifiedName;
	}
	
	public String getInsp_type() {
		return insp_type;
	}

	public String getMech_duty() {
		return mech_duty;
	}

	public void setInsp_type(String insp_type) {
		this.insp_type = insp_type;
	}

	public void setMech_duty(String mech_duty) {
		this.mech_duty = mech_duty;
	}
	
	public boolean isSpeaks_spanish() {
		return speaks_spanish;
	}

	public void setSpeaks_spanish(boolean speaks_spanish) {
		this.speaks_spanish = speaks_spanish;
	}

	public String getState() {
		return state;
	}

	public String getCounty() {
		return county;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPrefPhone() {
		return prefPhone;
	}

	public String getPrefMobile() {
		return prefMobile;
	}

	public String getPrefEmail() {
		return prefEmail;
	}

	public String getPrefCompany() {
		return prefCompany;
	}

	public String getComments() {
		return comments;
	}

	public void setPrefPhone(String prefPhone) {
		this.prefPhone = prefPhone;
	}

	public void setPrefMobile(String prefMobile) {
		this.prefMobile = prefMobile;
	}

	public void setPrefEmail(String prefEmail) {
		this.prefEmail = prefEmail;
	}

	public void setPrefCompany(String prefCompany) {
		this.prefCompany = prefCompany;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FacilityVM other = (FacilityVM) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

	@Override
	public String toString() {
		return "FacilityVM [id=" + id + ", profileId=" + profileId + ", name=" + name + ", address=" + address
				+ ", city=" + city + ", zipCode=" + zipCode + ", weekDayHours=" + weekDayHours + ", saturdayHours="
				+ saturdayHours + ", sundayHours=" + sundayHours + ", phone=" + phone + ", businessName=" + businessName
				+ ", addr1=" + addr1 + ", addr2=" + addr2 + ", addr3=" + addr3 + ", bCity=" + bCity + ", bState="
				+ bState + ", bZip=" + bZip + ", bPhone=" + bPhone + ", bEmail=" + bEmail + ", bCounty=" + bCounty
				+ ", isn=" + isn + ", type=" + type + ", contactName=" + contactName + ", cPhone=" + cPhone + ", refBy="
				+ refBy + ", firstName=" + firstName + ", lastName=" + lastName + ", incAB=" + incAB + ", certified="
				+ certified + ", preCertified=" + preCertified + ", paymentDate=" + paymentDate + ", incStatus="
				+ incStatus + ", incABName=" + incABName + "]";
	}

 

}
