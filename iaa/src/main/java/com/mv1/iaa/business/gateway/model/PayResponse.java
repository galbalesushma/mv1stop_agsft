package com.mv1.iaa.business.gateway.model;

public class PayResponse {

    private String status;
    private String message;
    private String txnId;
    private String avsResp;
    private String cvv2Resp;
    private String maskedAc;
    private String cardType;
    private String authCode;
    private String achRouting;
    private String achAccount;
    private String docType;
    public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getAchRouting() {
		return achRouting;
	}

	public void setAchRouting(String achRouting) {
		this.achRouting = achRouting;
	}

	public String getAchAccount() {
		return achAccount;
	}

	public void setAchAccount(String achAccount) {
		this.achAccount = achAccount;
	}

	public String getAchAccountType() {
		return achAccountType;
	}

	public void setAchAccountType(String achAccountType) {
		this.achAccountType = achAccountType;
	}

	private String achAccountType;
    
    private boolean isSuccessful;

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public String getTxnId() {
	return txnId;
    }

    public void setTxnId(String txnId) {
	this.txnId = txnId;
    }

    public String getAvsResp() {
	return avsResp;
    }

    public void setAvsResp(String avsResp) {
	this.avsResp = avsResp;
    }

    public String getCvv2Resp() {
	return cvv2Resp;
    }

    public void setCvv2Resp(String cvv2Resp) {
	this.cvv2Resp = cvv2Resp;
    }

    public String getMaskedAc() {
	return maskedAc;
    }

    public void setMaskedAc(String maskedAc) {
	this.maskedAc = maskedAc;
    }

    public String getCardType() {
	return cardType;
    }

    public void setCardType(String cardType) {
	this.cardType = cardType;
    }

    public String getAuthCode() {
	return authCode;
    }

    public void setAuthCode(String authCode) {
	this.authCode = authCode;
    }

    public boolean isSuccessful() {
	return isSuccessful;
    }

    public void setSuccessful(boolean isSuccessful) {
	this.isSuccessful = isSuccessful;
    }

    @Override
    public String toString() {
	return "PayResponse [status=" + status + ", message=" + message + ", txnId=" + txnId + ", avsResp=" + avsResp
		+ ", cvv2Resp=" + cvv2Resp + ", maskedAc=" + maskedAc + ", cardType=" + cardType + ", authCode="
		+ authCode + ", isSuccessful=" + isSuccessful + ", achRouting=" + achRouting+ ", achAccount=" + achAccount+ ", achAccount=" + achAccount + ", docType=" + docType+ "]";
    }

}
