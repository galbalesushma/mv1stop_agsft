package com.mv1.iaa.business.common;

public interface Constants {

    Integer PAGE_LIST_SIZE = 20;
    
    Integer PAGE_LIST_NEW_SIZE = 10;
    
    
    String CDR_FILE_PARSED = "PROCESSED";
    
    String CDR_FILE_FOUND = "NEW";
    
    String CDR_FILE_COMPLETED= "COMPLETED";
    
    String CDR_FILE_FAILED = "FAILED";
}
