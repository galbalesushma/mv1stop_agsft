package com.mv1.iaa.business.view.web.req;

import java.util.List;

public class FacilityIdVM {

    private List<Long> ids;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "FacilityIdVM [ids=" + ids + "]";
    }

}
