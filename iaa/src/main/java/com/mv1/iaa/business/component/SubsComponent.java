package com.mv1.iaa.business.component;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.enumeration.Duration;
import com.mv1.iaa.business.view.web.req.CartVM;
import com.mv1.iaa.business.view.web.req.Item;
import com.mv1.iaa.business.view.web.resp.AvailableItemVM;
import com.mv1.iaa.domain.Cart;
import com.mv1.iaa.domain.enumeration.ProfileType;
import com.mv1.iaa.repository.CartRepository;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.CustomCartRepository;
import com.mv1.iaa.service.CartService;
import com.mv1.iaa.service.PackSubscriptionService;
import com.mv1.iaa.service.PricePackageService;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.ZipAreaService;
import com.mv1.iaa.service.dto.CartDTO;
import com.mv1.iaa.service.dto.PackSubscriptionDTO;
import com.mv1.iaa.service.dto.PricePackageDTO;
import com.mv1.iaa.service.dto.ProfileDTO;
import com.mv1.iaa.service.dto.ZipAreaDTO;
import com.mv1.iaa.service.mapper.CartMapper;
import com.mv1.iaa.service.mapper.PackSubscriptionMapper;

@Component
public class SubsComponent {

	private static final Logger log = LoggerFactory.getLogger(SubsComponent.class);

	@Autowired
	private CartService cartService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private PricePackageService pricePackageService;

	@Autowired
	private PackSubscriptionService packSubscriptionService;

	@Autowired
	private CustomCartRepository customCartRepository;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private PackSubscriptionMapper packSubscriptionMapper;

	@Autowired
	private CartMapper cartMapper;

	@Autowired
	private ZipAreaService zipAreaService;

	@Autowired
	CustomAgentRepository customAgentRepository;

	public List<AvailableItemVM> getAvailableRankAndSlots(String zip, Long profileId) {
		List<AvailableItemVM> list = new LinkedList<>();

		Optional<ProfileDTO> optDto = profileService.findOne(profileId);
		String type = ProfileType.AGENT.toString();
		if (optDto.isPresent()) {
			System.out.println(" optDto.get().getType()" + optDto.get().getType());
			type = optDto.get().getType().toString();
		}

//        Optional<ZipAreaDTO> zipA = zipAreaService.findOne(Long.parseLong(zip));
//        String zipCode = zipA.get().getZip();
//        Object[][] data = customCartRepository.findAvailableItemsByZip(zip, type);

		/*
		 * if(data != null && data.length > 0) { try { for (Object[] row : data) {
		 * AvailableItemVM vm = CommonMapper.fromKeyVal(AvailableItemVM.getKeys(), row,
		 * AvailableItemVM.class); list.add(vm); } } catch (Exception e) {
		 * log.debug("Error while mapping AvailableItemVM ", e); } }
		 */

		String page1 = customCartRepository.findAvailableItemsByZipyPackageId(zip, type, "1");
		AvailableItemVM vm = new AvailableItemVM();
		vm.setRank("ONE");
		if (page1 == null) {
			vm.setSlot(10);
		} else {
			vm.setSlot(10 - Integer.parseInt(page1));
		}

		list.add(vm);

		String page2 = customCartRepository.findAvailableItemsByZipyPackageId(zip, type, "2");
		AvailableItemVM vm1 = new AvailableItemVM();
		vm1.setRank("TWO");
		if (page2 == null) {
			vm1.setSlot(20);
		} else {
			vm1.setSlot(20 - Integer.parseInt(page2));
		}
		list.add(vm1);

		AvailableItemVM vm3 = new AvailableItemVM();
		vm3.setRank("THREE");
		vm3.setSlot(20);
		list.add(vm3);

		return list;
	}

	@Transactional
	public CartVM add2Cart(CartVM vm) {

		// Check if subscriptions are still available for the zip
		if (!checkAvailability(vm)) {
			return null;
		}

		Optional<Cart> optCart = customCartRepository.findOneByProfileIdAndIsCompleted(vm.getProfileId(), false);
		CartDTO dto = null;
		final Set<PackSubscriptionDTO> packSubscriptions = new HashSet<>();

		// Create a new cart if not already present
		if (!optCart.isPresent()) {
			dto = new CartDTO();
			dto.setCreatedAt(ZonedDateTime.now());
			dto.setIsCompleted(false);
			dto.setProfileId(vm.getProfileId());
			dto.setUpdatedAt(ZonedDateTime.now());
			dto.setIsDeleted(false);

			dto = cartService.save(dto);
		} else {
			// else associate retrieved
			dto = cartMapper.toDto(optCart.get());
			dto.setUpdatedAt(ZonedDateTime.now());

			packSubscriptions.addAll(dto.getSubscriptions());
		}

		// Process subscriptions
		List<Item> items = vm.getItems();
		if (items != null && !items.isEmpty()) {
			items.stream().forEach(i -> packSubscriptions
					.add(packSubscriptionService.save(CommonMapper.fromItem(i, vm.getProfileId()))));
		}

		Cart cart = cartRepository.findOneWithEagerRelationships(dto.getId()).get();
		dto = cartMapper.toDto(cart);
		dto.setSubscriptions(packSubscriptions);

		// Update the subscriptions for the cart
		dto = cartService.save(dto);
		CartVM resp = getCartVM(dto.getSubscriptions(), dto.getId(), vm.getProfileId());

		// Calculate & update the total price
		resp.setTotal(calculateTotal(dto.getSubscriptions()));

		return resp;
	}

	@Transactional
	public CartVM getCart(Long profileId) {

		Optional<Cart> optCart = customCartRepository.findOneByProfileIdAndIsCompleted(profileId, false);
		CartDTO dto = null;
		CartVM resp = null;

		if (optCart.isPresent()) {

			dto = cartMapper.toDto(optCart.get());

			Cart cart = cartRepository.findOneWithEagerRelationships(dto.getId()).get();
			dto = cartMapper.toDto(cart);

			resp = getCartVM(dto.getSubscriptions(), dto.getId(), profileId);

			// Calculate & update the total price
			resp.setTotal(calculateTotal(dto.getSubscriptions()));
		}
		return resp;
	}

	private CartVM getCartVM(Set<PackSubscriptionDTO> packs, Long cartId, Long profileId) {
		List<Item> items = new LinkedList<>();
		CartVM v = new CartVM();

		v.setCartId(cartId);
		v.setProfileId(profileId);

		packs.forEach(d -> {
			Optional<ZipAreaDTO> optZip = zipAreaService.findOne(d.getZipAreaId());
			String zip = optZip.isPresent() ? optZip.get().getZip() : "";

			Optional<PricePackageDTO> optPack = pricePackageService.findOne(d.getPackageId());
			String rank = optPack.isPresent() ? optPack.get().getRank().name() : "";

			Item i = new Item();

			i.setCartId(cartId);
			i.setPackageId(d.getPackageId());
			i.setZipAreaId(d.getZipAreaId());
			i.setSlots(d.getSlots());
			i.setDuration(d.getDuration());
			i.setSubsId(d.getId());
			i.setZip(zip);
			i.setRank(rank);

			items.add(i);
		});

		v.setItems(items);
		return v;
	}

	private boolean checkAvailability(CartVM vm) {
		boolean isAvailable = false;
		Map<String, Integer> map = new HashMap<>();

		if (vm.getItems() != null) {
			for (Item i : vm.getItems()) {

				Optional<ZipAreaDTO> optArea = zipAreaService.findOne(i.getZipAreaId());
				List<AvailableItemVM> list = getAvailableRankAndSlots(optArea.get().getZip(), vm.getProfileId());

				if (list != null && !list.isEmpty()) {
					list.stream().forEach(a -> {
						map.put(a.getRank(), a.getSlot());
					});
				}
			}

			for (Item i : vm.getItems()) {
				Optional<PricePackageDTO> pack = pricePackageService.findOne(i.getPackageId());
				isAvailable = i.getSlots() <= map.get(pack.get().getRank().name());
				if (!isAvailable) {
					return isAvailable;
				}
			}
		}
		return isAvailable;
	}

	@Transactional
	public CartVM removeFromCart(CartVM vm) {

		Cart cart = customCartRepository.findOneById(vm.getCartId());

		final Set<PackSubscriptionDTO> packSubscriptions = new HashSet<>();

		// Process subscriptions
		List<Item> items = vm.getItems();
		List<Long> ids = new LinkedList<>();
		if (items != null && !items.isEmpty()) {
			items.stream().forEach(i -> {
				ids.add(i.getSubsId());
			});
		}

		customCartRepository.deleteSubscriptionByIds(vm.getCartId(), ids);

		items.stream().forEach(i -> {
			packSubscriptionService.delete(i.getSubsId());
		});

		// Update the subscriptions for the cart
		cart = customCartRepository.findOneById(vm.getCartId());
		CartDTO dto = cartMapper.toDto(cart);
		cart.getSubscriptions().stream().forEach(s -> packSubscriptions.add(packSubscriptionMapper.toDto(s)));

		CartVM resp = getCartVM(dto.getSubscriptions(), dto.getId(), vm.getProfileId());

		// Calculate & update the total price
		resp.setTotal(calculateTotal(packSubscriptions));

		return resp;
	}
//Modified slot buy cost//
	private String calculateTotal(Set<PackSubscriptionDTO> packs) {

		Long price = 0l;
		for (PackSubscriptionDTO d : packs) {
			Optional<PricePackageDTO> optPp = pricePackageService.findOne(d.getPackageId());
			if (optPp.isPresent()) {
				PricePackageDTO dto = optPp.get();
				Duration duration = Duration.from(d.getDuration());
//                price += dto.getPrice() * d.getSlots() * duration.getMonths(); // prev price was monthy , so * by months
				int carrierCount = 0;
				carrierCount = customAgentRepository.findCountByProfileId(d.getProfileId());
				// carrierCount=CustomAgentRepository;
				if (dto.getId() == 1) {
					if (carrierCount == 0) {
						price += dto.getPrice() * d.getSlots(); // now price by yearly
					} else if (carrierCount >= 1 && carrierCount <= 3) {
						price += 300 * d.getSlots(); // now price by yearly
					} else if (carrierCount >= 4) {
						price += 400 * d.getSlots(); // now price by yearly
					}
				} else if (dto.getId() == 2) {
					if (carrierCount == 0) {
						price += dto.getPrice() * d.getSlots(); // now price by yearly
					} else if (carrierCount >= 1 && carrierCount <= 3) {
						price += 150 * d.getSlots(); // now price by yearly
					} else if (carrierCount >= 4) {
						price += 200 * d.getSlots(); // now price by yearly
					}
				} else {
					price += dto.getPrice() * d.getSlots();
				}
			}
		}
		return price.toString();
	}
	
	/*  //old total slot cost//
	 * private String calculateTotal(Set<PackSubscriptionDTO> packs) {
	 * 
	 * Long price = 0l; for (PackSubscriptionDTO d : packs) {
	 * Optional<PricePackageDTO> optPp =
	 * pricePackageService.findOne(d.getPackageId()); if (optPp.isPresent()) {
	 * PricePackageDTO dto = optPp.get(); Duration duration =
	 * Duration.from(d.getDuration()); // price += dto.getPrice() * d.getSlots() *
	 * duration.getMonths(); // prev price was monthy , so * by months price +=
	 * dto.getPrice() * d.getSlots(); // now price by yearly } }
	 * 
	 * return price.toString(); }
	 */
	
	
}
