package com.mv1.iaa.business.view.web.resp;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileVM {

    // Update the keys array when new attributes are added in the VM
    private static final String[] keys = new String[] { "profileId", "userId", "firstName", "lastName", "type" };

    public static String[] getKeys() {
        return keys;
    }

    private Long profileId;
    private Long userId;
    private String firstName;
    private String lastName;
    private String type;
    private List<Long> carriers;
    

	public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public List<Long> getCarriers() {
		return carriers;
	}

	public void setCarriers(List<Long> carriers) {
		this.carriers = carriers;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((profileId == null) ? 0 : profileId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProfileVM other = (ProfileVM) obj;
        if (profileId == null) {
            if (other.profileId != null)
                return false;
        } else if (!profileId.equals(other.profileId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ProfileVM [profileId=" + profileId + ", userId=" + userId + ", firstName=" + firstName + ", lastName="
                + lastName + ", type=" + type + "]";
    }

}
