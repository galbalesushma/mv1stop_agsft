package com.mv1.iaa.business.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.business.exception.Mv1Exception;
import com.mv1.iaa.business.view.Error;

public class CsvValidator {

	private static final String INVALID_MINLENGTH = "Invalid min length";
	private static final String INVALID_MAXLENGTH = "Invalid max length";

	@Transactional(rollbackFor = { Mv1Exception.class })
	public static List<com.mv1.iaa.business.view.Error> validate(int min, int max, String pattern,
			String patternMessage, String val, String fieldName) throws Mv1Exception {

		if (val!=null && !val.equals("")) {
			val = val.trim();
			List<com.mv1.iaa.business.view.Error> errors = new ArrayList<>();
			if (min != 0 && (val.length() < min)) {
				com.mv1.iaa.business.view.Error error = new Error();
				error.setFieldName(fieldName);
				error.setValid(false);
				error.setMessage(INVALID_MINLENGTH + "-" + fieldName +"-"+val);
				errors.add(error);
			}
			if (max != 0 && (val.length() > max)) {
				com.mv1.iaa.business.view.Error error = new Error();
				error.setFieldName(fieldName);
				error.setValid(false);
				error.setMessage(INVALID_MAXLENGTH + "-" + fieldName+"-"+val);
				errors.add(error);
			}

			if (!pattern.equals("") && !(val.matches(pattern))) {
				com.mv1.iaa.business.view.Error error = new Error();
				error.setFieldName(fieldName);
				error.setValid(false);
				error.setMessage(patternMessage+"-"+val);
				errors.add(error);
			}

			if (errors.size() > 0) {
				throw new Mv1Exception(errors);
			}
			return errors;
		}
		return null;
	}

	@Transactional(rollbackFor = { Mv1Exception.class })
	public static List<com.mv1.iaa.business.view.Error> validate(int min, int max, String pattern,
			String patternMessage, Date val, String fieldName) throws Mv1Exception {
		String value = (val.getMonth() + 1) + "/" + val.getDate() + "/" + val.getYear();
		List<com.mv1.iaa.business.view.Error> errors = new ArrayList<>();
		if (!pattern.equals("") && !(value.matches(pattern))) {
			com.mv1.iaa.business.view.Error error = new Error();
			error.setFieldName(fieldName);
			error.setValid(false);
			error.setMessage(patternMessage+ "-"+value);
			errors.add(error);

		}
		if (errors.size() > 0) {
			throw new Mv1Exception(errors);
		}
		return errors;
	}

	@Transactional(rollbackFor = { Mv1Exception.class })
	public static List<com.mv1.iaa.business.view.Error> validate(int min, int max, String pattern,
			String patternMessage, int val, String fieldName) throws Mv1Exception {
		String value = val + "";
		List<com.mv1.iaa.business.view.Error> errors = validate(min, max, pattern, patternMessage, value, fieldName);

		return errors;

	}
}
