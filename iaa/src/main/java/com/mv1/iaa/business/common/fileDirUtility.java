package com.mv1.iaa.business.common;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class fileDirUtility {
	private final static Logger log = LoggerFactory.getLogger(fileDirUtility.class);

	public static  void listofDirectories(String directoryName, List<DirectoryPath> directoryPaths) {

		try {

			File directory = new File(directoryName);
			// get all the files from a directory
			File[] fList = directory.listFiles();
			for (File file : fList) {
				if (file.isDirectory()) {
					// System.out.println(file.getParentFile());
					DirectoryPath dir = new DirectoryPath();
					dir.setPath(file.getPath());
					directoryPaths.add(dir);
					System.out.println(dir.getPath() + ":" + dir.isProcess());
					listofDirectories(file.getAbsolutePath(), directoryPaths);
				}/*else {
					DirectoryPath dir = new DirectoryPath();
					dir.setPath(directoryName);
					directoryPaths.add(dir);
				}*/
			}
			if(directoryPaths.size() == 0) {
				DirectoryPath dir = new DirectoryPath();
				dir.setPath(directoryName);
				directoryPaths.add(dir);
			}
			
		} catch (Exception e) {
			log.info("Exception find on finding directory to be move to pool." + e.getMessage());
		}

	}

	private static DirectoryPath getDirPath(DirectoryPath file, List<String> syncConfigPath) {

		boolean pathMatch = false;
		for (String path : syncConfigPath) {
			String[] paths = path.split(",");
			if (file.getPath().contains(paths[0])) {
				pathMatch = true;
				file.setParentDirFiterPath(paths[1]);
				break;

			}
		}
		file.setProcess(pathMatch);

		return file;

		/*
		 * //get the stateful session //KieSession kieSession =
		 * kieContainer.newKieSession("rulesSession"); KieServices kieServices =
		 * KieServices.Factory.get(); KieFileSystem kfs =
		 * kieServices.newKieFileSystem(); FileInputStream fis = null; try { fis = new
		 * FileInputStream(droolConfig); } catch (FileNotFoundException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } kfs.write(
		 * "src/main/resources/rules.drl",
		 * kieServices.getResources().newInputStreamResource(fis));
		 * 
		 * KieBuilder kieBuilder = kieServices.newKieBuilder( kfs ).buildAll(); Results
		 * results = kieBuilder.getResults(); if( results.hasMessages(
		 * Message.Level.ERROR ) ){ log.info("kie server error:"+ results.getMessages()
		 * ); throw new IllegalStateException( "### errors ###" ); } KieContainer
		 * kieContainer = kieServices.newKieContainer(
		 * kieServices.getRepository().getDefaultReleaseId() );
		 * 
		 * KieBase kieBase = kieContainer.getKieBase(); KieSession kieSession =
		 * kieContainer.newKieSession(); file.setPath(file.getPath());
		 * kieSession.insert(file); kieSession.fireAllRules(); kieSession.dispose();
		 * return file;
		 */
	}
}
