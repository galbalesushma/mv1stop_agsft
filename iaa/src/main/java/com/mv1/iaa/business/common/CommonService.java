package com.mv1.iaa.business.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mv1.iaa.business.enumeration.Duration;
import com.mv1.iaa.business.view.search.Agent;
import com.mv1.iaa.business.view.search.Facility;
import com.mv1.iaa.business.view.search.Subs;
import com.mv1.iaa.domain.Address;
import com.mv1.iaa.domain.PackSubscription;
import com.mv1.iaa.domain.PricePackage;
import com.mv1.iaa.domain.ZipArea;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.CustomFacilityRepository;
import com.mv1.iaa.repository.CustomPackSubscriptionRepository;
import com.mv1.iaa.repository.CustomZipAreaRepository;
import com.mv1.iaa.repository.PricePackageRepository;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.dto.ProfileDTO;

@Service
public class CommonService {

    private static SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private CustomAgentRepository customAgentRepository;
    
    @Autowired
    private CustomFacilityRepository customFacilityRepository;
    
    @Autowired
    private CustomPackSubscriptionRepository customPackSubscriptionRepository;
    
    @Autowired
    private PricePackageRepository pricePackageRepository;
    
    @Autowired
    private CustomZipAreaRepository customZipAreaRepository;

    public Agent getAgentSearchVM(Long profileId) {
    
        Agent a = new Agent();
        List<Subs> subs = new LinkedList<>();    
        Optional<ProfileDTO> prOpt = profileService.findOne(profileId);

        ProfileDTO profileDTO = null;
        if (prOpt.isPresent()) {
            profileDTO = prOpt.get();
        }
        
        List<PackSubscription> list = customPackSubscriptionRepository.findAllByProfileIdAndIsActive(profileId, true);
        
        list
            .stream()
            .forEach(s -> {
                Subs sub = new Subs();
                Optional<PricePackage> optPp = pricePackageRepository.findById(s.getPackageId());
                PricePackage pp = optPp.get();
                ZipArea zip = customZipAreaRepository.findOneById(s.getZipAreaId());
                int months = Duration.from(s.getDuration()).getMonths();
                long score = s.getSlots() * pp.getPrice() * months;
                
                sub.setDuration(s.getDuration());
                sub.setRank(pp.getRank().name());
                sub.setSlots(s.getSlots());
                sub.setSubsId(s.getId());
                sub.setZip(zip.getZip());
                sub.setScore(score);
                subs.add(sub);
            });
        
        com.mv1.iaa.domain.Agent ag = customAgentRepository.findOneByProfileId(profileId);
        if(ag.getBusiness()!=null){
        	  Address addr = ag.getBusiness().getAddress();
        	   a.setAddress(addr.getAddr1());
        	   if(addr.getCity()!=null){
        		   a.setCity(addr.getCity().getName());
        	   }
        	   if(addr.getState()!=null){
        		   a.setState(addr.getState().getName());
        	   }
               a.setZip(addr.getZip());
        }
      
        
        a.setNpn(ag.getNpn());
        a.setFirst_name(profileDTO.getFirstName());
        a.setId(ag.getId());
        a.setLast_name(profileDTO.getLastName());
        System.out.println("ag.getLic()=========="+ag.getLic());
        //to do - check if agent has licence record , if not - add lic record 
        if(ag.getLic()!=null && ag.getLic().getExpirationDate()!=null){
        	   a.setValid_till(formatter.format(new Date(ag.getLic().getExpirationDate().toInstant().toEpochMilli())) );
        }
     
        a.setSubs(subs);
        
        return a;
    }
    
    public Facility getFacilitySearchVM(Long profileId) {
        
        Facility f = new Facility();
        List<Subs> subs = new LinkedList<>();    
        Optional<ProfileDTO> prOpt = profileService.findOne(profileId);

        ProfileDTO profileDTO = null;
        if (prOpt.isPresent()) {
            profileDTO = prOpt.get();
        }
        
        List<PackSubscription> list = customPackSubscriptionRepository.findAllByProfileIdAndIsActive(profileId, true);
        
        list
            .stream()
            .forEach(s -> {
                Subs sub = new Subs();
                Optional<PricePackage> optPp = pricePackageRepository.findById(s.getPackageId());
                PricePackage pp = optPp.get();
                ZipArea zip = customZipAreaRepository.findOneById(s.getZipAreaId());
                int months = Duration.from(s.getDuration()).getMonths();
                long score = s.getSlots() * pp.getPrice() * months;
                
                sub.setDuration(s.getDuration());
                sub.setRank(pp.getRank().name());
                sub.setSlots(s.getSlots());
                sub.setSubsId(s.getId());
                sub.setZip(zip.getZip());
                sub.setScore(score);
                subs.add(sub);
            });
        
        com.mv1.iaa.domain.Facility facility = customFacilityRepository.findOneByProfileId(profileId);
        Address addr = facility.getBusiness().getAddress();
        
        f.setIsn(facility.getIsn());
        f.setAddress(addr.getAddr1());
        f.setCity(addr.getCity().getName());
        f.setName(profileDTO.getFirstName());
        f.setId(facility.getId());
        f.setState(addr.getState().getName());
        f.setZip(addr.getZip());
        f.setSubs(subs);
        
        return f;
    }
    
    public List<Agent> expandSlots(List<Agent> list) {
        
        return list;
    }
}
