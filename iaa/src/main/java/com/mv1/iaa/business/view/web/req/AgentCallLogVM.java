package com.mv1.iaa.business.view.web.req;

import java.time.ZonedDateTime;

public class AgentCallLogVM {

	private ZonedDateTime callDate;

	private String userNumber;

	private String agentNumber;

	private String userName;
	
	private String type;
	
	private String npn;

	public ZonedDateTime getCallDate() {
		return callDate;
	}

	public void setCallDate(ZonedDateTime callDate) {
		this.callDate = callDate;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	public String getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNpn() {
		return npn;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}

	
	

}
