package com.mv1.iaa.business.view.csv;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.validation.constraints.Pattern;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mv1.iaa.business.aop.CsvData;
import com.opencsv.bean.CsvBindByName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class AgentCsvBean {

	private static List<com.mv1.iaa.business.view.Error> errors = new LinkedList<>();

	@CsvBindByName(column = "LICENSE_NUM")
	/*@NotNull
	@NotEmpty
	@Size(min = 1, max = 9)*/
	@Pattern(regexp = "[0-9]+")
	// @Valid
	private String licNumber;

	@CsvBindByName(column = "NPN")
	private String npn;

	@CsvBindByName(column = "LAST_NAME")
	private String lastName;

	@CsvBindByName(column = "FIRST_NAME")
	private String firstName;

	@CsvBindByName(column = "MIDDLE_NAME")
	private String middleName;

	@CsvBindByName(column = "SUFFIX")
	private String suffix;

	@CsvBindByName(column = "BUSINESS_ENTITY_NAME")
	private String businessName;

	@CsvBindByName(column = "FEIN")
	private String fein;

	@CsvBindByName(column = "LICENSE_TYPE")
	private String licType;

	@CsvBindByName(column = "LINE_OF_AUTHORITY")
	private String lineAuth;

	@CsvBindByName(column = "LICENSE_STATUS")
	private String licStatus;

//	@CsvDate("MM/dd/yyyy")
	@CsvBindByName(column = "FIRST_ACTIVE_DATE")
	private String firstActiveDate;

//	@CsvDate("MM/dd/yyyy")
	@CsvBindByName(column = "EFFECTIVE_DATE")
	private String effectiveDate;

//	@CsvDate("MM/dd/yyyy")
	@CsvBindByName(column = "EXPIRATION_DATE")
	private String expDate;

	@CsvBindByName(column = "DOMICILESTATE")
	private String domState;

	@CsvBindByName(column = "IS_RESIDENT")
	private String resState;

	@CsvBindByName(column = "MLG_ADDRESS1")
	private String pAddr1;

	@CsvBindByName(column = "MLG_ADDRESS2")
	private String pAddr2;

	@CsvBindByName(column = "MLG_ADDRESS3")
	private String pAddr3;

	@CsvBindByName(column = "MLG_CITY")
	private String pCity;

	@CsvBindByName(column = "MLG_STATE")
	private String pState;

	@CsvBindByName(column = "MLG_ZIP")
	private String pZip;

	@CsvBindByName(column = "MLG_COUNTRY")
	private String pCountry;

	@CsvBindByName(column = "BUS_ADDRESS1")
	private String bAddr1;

	@CsvBindByName(column = "BUS_ADDRESS2")
	private String bAddr2;

	@CsvBindByName(column = "BUS_ADDRESS3")
	private String bAddr3;

	@CsvBindByName(column = "BUS_CITY")
	private String bCity;

	@CsvBindByName(column = "BUS_STATE")
	private String bState;

	@CsvBindByName(column = "BUS_ZIP")
	private String bZip;

	@CsvBindByName(column = "BUS_COUNTRY")
	private String bCountry;

	@CsvBindByName(column = "BUS_PHONE")
	private String bPhone;

	@CsvBindByName(column = "BUS_EMAIL")
	private String bEmail;

	@CsvBindByName(column = "CE_COMPLIANCE")
	private String isCECompliant;

	@CsvBindByName(column = "NAICNO")
	private String naicno;

	private String agentType;
	private String bMobile;
	private String speaksSpanish;
	@CsvBindByName(column = "MLG_COUNTY")
	private String pCounty;
	@CsvBindByName(column = "BUS_COUNTY")
	private String bCounty;
	@CsvBindByName(column = "EXTENSION")
	private String extension;
	
	@CsvBindByName(column = "INSURANCE_COMPANY")
	private String insuranceCompany;
	
	@CsvBindByName(column = "SECOND_EXTENSION")
	private String secondExtension;
	
	private Date firstActiveDateNew;
	private Date effectiveDateNew;
	private Date expDateNew;
	
	public String getLicNumber() {
		return licNumber;
	}
	
	public void setLicNumber(String licNumber)   {
		this.licNumber = licNumber;
	}

	public String getNpn() {
		return npn;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getFein() {
		return fein;
	}

	public void setFein(String fein) {
		this.fein = fein;
	}

	public String getLicType() {
		return licType;
	}

	public void setLicType(String licType) {
		this.licType = licType;
	}

	public String getLineAuth() {
		return lineAuth;
	}

	public void setLineAuth(String lineAuth) {
		this.lineAuth = lineAuth;
	}

	public String getLicStatus() {
		return licStatus;
	}

	public void setLicStatus(String licStatus) {
		this.licStatus = licStatus;
	}

	public String getFirstActiveDate() {
		return firstActiveDate;
	}

	public void setFirstActiveDate(String firstActiveDate) {
		this.firstActiveDate = firstActiveDate;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getDomState() {
		return domState;
	}

	public void setDomState(String domState) {
		this.domState = domState;
	}

	public String getResState() {
		return resState;
	}

	public void setResState(String resState) {
		this.resState = resState;
	}

	public String getpAddr1() {
		return pAddr1;
	}

	public void setpAddr1(String pAddr1) {
		this.pAddr1 = pAddr1;
	}

	public String getpAddr2() {
		return pAddr2;
	}

	public void setpAddr2(String pAddr2) {
		this.pAddr2 = pAddr2;
	}

	public String getpAddr3() {
		return pAddr3;
	}

	public void setpAddr3(String pAddr3) {
		this.pAddr3 = pAddr3;
	}

	public String getpCity() {
		return pCity;
	}

	public void setpCity(String pCity) {
		this.pCity = pCity;
	}

	public String getpState() {
		return pState;
	}

	public void setpState(String pState) {
		this.pState = pState;
	}

	public String getpZip() {
		return pZip;
	}

	public void setpZip(String pZip) {
		this.pZip = pZip;
	}

	public String getpCountry() {
		return pCountry;
	}

	public void setpCountry(String pCountry) {
		this.pCountry = pCountry;
	}

	public String getbAddr1() {
		return bAddr1;
	}

	public void setbAddr1(String bAddr1) {
		this.bAddr1 = bAddr1;
	}

	public String getbAddr2() {
		return bAddr2;
	}

	public void setbAddr2(String bAddr2) {
		this.bAddr2 = bAddr2;
	}

	public String getbAddr3() {
		return bAddr3;
	}

	public void setbAddr3(String bAddr3) {
		this.bAddr3 = bAddr3;
	}

	public String getbCity() {
		return bCity;
	}

	public void setbCity(String bCity) {
		this.bCity = bCity;
	}

	public String getbState() {
		return bState;
	}

	public void setbState(String bState) {
		this.bState = bState;
	}

	public String getbZip() {
		return bZip;
	}

	public void setbZip(String bZip) {
		this.bZip = bZip;
	}

	public String getbCountry() {
		return bCountry;
	}

	public void setbCountry(String bCountry) {
		this.bCountry = bCountry;
	}

	public String getbPhone() {
		return bPhone;
	}

	public void setbPhone(String bPhone) {
		this.bPhone = bPhone;
	}

	public String getbEmail() {
		return bEmail;
	}

	public void setbEmail(String bEmail) {
		this.bEmail = bEmail;
	}

	public String getIsCECompliant() {
		return isCECompliant;
	}

	public void setIsCECompliant(String isCECompliant) {
		this.isCECompliant = isCECompliant;
	}

	public String getNaicno() {
		return naicno;
	}

	public void setNaicno(@CsvData String naicno) {
		this.naicno = naicno;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getbMobile() {
		return bMobile;
	}

	public void setbMobile(String bMobile) {
		this.bMobile = bMobile;
	}

	public String getSpeaksSpanish() {
		return speaksSpanish;
	}

	public void setSpeaksSpanish(String speaksSpanish) {
		this.speaksSpanish = speaksSpanish;
	}

	public String getpCounty() {
		return pCounty;
	}

	public void setpCounty(String pCounty) {
		this.pCounty = pCounty;
	}

	public String getbCounty() {
		return bCounty;
	}

	public void setbCounty(String bCounty) {
		this.bCounty = bCounty;
	}

	public Date getFirstActiveDateNew() {
		return firstActiveDateNew;
	}

	public void setFirstActiveDateNew(Date firstActiveDateNew) {
		this.firstActiveDateNew = firstActiveDateNew;
	}
	
	public Date getEffectiveDateNew() {
		return effectiveDateNew;
	}

	public Date getExpDateNew() {
		return expDateNew;
	}

	public void setEffectiveDateNew(Date effectiveDateNew) {
		this.effectiveDateNew = effectiveDateNew;
	}

	public void setExpDateNew(Date expDateNew) {
		this.expDateNew = expDateNew;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}
	
	public String getSecondExtension() {
		return secondExtension;
	}

	public void setSecondExtension(String secondExtension) {
		this.secondExtension = secondExtension;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((npn == null) ? 0 : npn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgentCsvBean other = (AgentCsvBean) obj;
		if (npn == null) {
			if (other.npn != null)
				return false;
		} else if (!npn.equals(other.npn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AgentCsvBean [licNumber=" + licNumber + ", npn=" + npn + ", lastName=" + lastName + ", firstName="
				+ firstName + ", middleName=" + middleName + ", suffix=" + suffix + ", businessName=" + businessName
				+ ", fein=" + fein + ", licType=" + licType + ", lineAuth=" + lineAuth + ", licStatus=" + licStatus
				+ ", firstActiveDate=" + firstActiveDate + ", effectiveDate=" + effectiveDate + ", expDate=" + expDate
				+ ", domState=" + domState + ", resState=" + resState + ", pAddr1=" + pAddr1 + ", pAddr2=" + pAddr2
				+ ", pAddr3=" + pAddr3 + ", pCity=" + pCity + ", pState=" + pState + ", pZip=" + pZip + ", pCountry="
				+ pCountry + ", bAddr1=" + bAddr1 + ", bAddr2=" + bAddr2 + ", bAddr3=" + bAddr3 + ", bCity=" + bCity
				+ ", bState=" + bState + ", bZip=" + bZip + ", bCountry=" + bCountry + ", bPhone=" + bPhone
				+ ", bEmail=" + bEmail + ", isCECompliant=" + isCECompliant + ", naicno=" + naicno + ", agentType="
				+ agentType + ", bMobile=" + bMobile + ", speaksSpanish=" + speaksSpanish + ", pCounty=" + pCounty
				+ ", bCounty=" + bCounty + "]";
	}

}
