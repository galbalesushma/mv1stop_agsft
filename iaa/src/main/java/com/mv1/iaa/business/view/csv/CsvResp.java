package com.mv1.iaa.business.view.csv;

public class CsvResp {

    private Integer updateCount;
    private Integer insertCount;
    private Integer deleteCount;
    
    private String fileName;

    
    public CsvResp() {
	this.updateCount = 0;
	this.insertCount = 0;
	this.deleteCount = 0;
	this.fileName = "";
    }

    public Integer getUpdateCount() {
	return updateCount;
    }

    public void setUpdateCount(Integer updateCount) {
	this.updateCount = updateCount;
    }

    public Integer getInsertCount() {
	return insertCount;
    }

    public void setInsertCount(Integer insertCount) {
	this.insertCount = insertCount;
    }

    public String getFileName() {
	return fileName;
    }

    public void setFileName(String fileName) {
	this.fileName = fileName;
    }

    public Integer getDeleteCount() {
        return deleteCount;
    }

    public void setDeleteCount(Integer deleteCount) {
        this.deleteCount = deleteCount;
    }

    @Override
    public String toString() {
	return "CsvResp [updateCount=" + updateCount + ", insertCount=" + insertCount + ", deleteCount=" + deleteCount
		+ ", fileName=" + fileName + "]";
    }
}
