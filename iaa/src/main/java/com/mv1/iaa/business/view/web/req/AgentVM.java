package com.mv1.iaa.business.view.web.req;

import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mv1.iaa.service.dto.ZipAreaDTO;
import com.opencsv.bean.CsvBindByPosition;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgentVM {

    private Long id;
    private String agentType;
    private String licNumber;
    @CsvBindByPosition(position = 0)
    private String npn;
    private String naicno;
    @CsvBindByPosition(position = 1)
    private String firstName;
    private String middleName;
    @CsvBindByPosition(position = 2)
    private String lastName;
    private String suffix;
    
    @CsvBindByPosition(position = 11)
    private String businessName;
    private String fein;
    private String bAddr1;
    private String bAddr2;
    private String bAddr3;
    private Long bCity;
    private Long bCounty;
    private Long bState;
    private String bZip;
    private String bCountry;
    @CsvBindByPosition(position = 8)
    private String bPhone;
    private String bMobile;
    @CsvBindByPosition(position = 12)
    private String bEmail;
    private String licType;
    private String lineAuth;
    private String licStatus;
    private String firstActiveDate;
    @CsvBindByPosition(position = 5)
    private String effectiveDate;
    private String expDate;
    private Long domState;
    private Boolean resState;
    private String pAddr1;
    private String pAddr2;
    private String pAddr3;
    private Long pCity;
    private Long pCounty;
    private Long pState;
    private String pZip;
    private String pCountry;
    private Boolean isCECompliant;
    private Boolean speaksSpanish;
    private List<ZipAreaDTO> zips;
    
    private boolean incAB;
    private boolean preCertified;
    private boolean certified;
    
    private String bCityName;
    private String bStateName;
    private String pCityName;
    private String pStateName;
    @CsvBindByPosition(position = 9)
    private String prefPhone;
    @CsvBindByPosition(position = 10)
    private String prefMobile;
    private BigInteger isRegistered;
    private String prefEmail;
    private Long profileId;
    private String bCountyName;
    private String pCountyName;
     
   @CsvBindByPosition(position = 4)
    private String incABName;
    @CsvBindByPosition(position = 7)
    private String preCertifiedName;
    @CsvBindByPosition(position = 6)
    private String certifiedName;
    
    @CsvBindByPosition(position = 3)
    private String incStatus;
    
    private String paymentDate;
    
    private String county;
    private String city;
    private String state;
    
    private String dStateName;
    
    private String prefCompany;
    
    private String comments;
    
    private String insuranceCompany;
    
    private List<Long> carriers;
    
    private String secondPhone;
    

    
	public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getAgentType() {
	return agentType;
    }

    public void setAgentType(String agentType) {
	this.agentType = agentType;
    }

    public String getLicNumber() {
	return licNumber;
    }

    public void setLicNumber(String licNumber) {
	this.licNumber = licNumber;
    }

    public String getNpn() {
	return npn;
    }

    public void setNpn(String npn) {
	this.npn = npn;
    }

    public String getNaicno() {
	return naicno;
    }

    public void setNaicno(String naicno) {
	this.naicno = naicno;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getMiddleName() {
	return middleName;
    }

    public void setMiddleName(String middleName) {
	this.middleName = middleName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getSuffix() {
	return suffix;
    }

    public void setSuffix(String suffix) {
	this.suffix = suffix;
    }

    public String getBusinessName() {
	return businessName;
    }

    public void setBusinessName(String businessName) {
	this.businessName = businessName;
    }

    public String getFein() {
	return fein;
    }

    public void setFein(String fein) {
	this.fein = fein;
    }

    public String getbAddr1() {
	return bAddr1;
    }

    public void setbAddr1(String bAddr1) {
	this.bAddr1 = bAddr1;
    }

    public String getbAddr2() {
	return bAddr2;
    }

    public void setbAddr2(String bAddr2) {
	this.bAddr2 = bAddr2;
    }

    public String getbAddr3() {
	return bAddr3;
    }

    public void setbAddr3(String bAddr3) {
	this.bAddr3 = bAddr3;
    }

    public Long getbCity() {
	return bCity;
    }

    public void setbCity(Long bCity) {
	this.bCity = bCity;
    }

    public Long getbState() {
	return bState;
    }

    public void setbState(Long bState) {
	this.bState = bState;
    }

    public String getbZip() {
	return bZip;
    }

    public void setbZip(String bZip) {
	this.bZip = bZip;
    }

    public String getbCountry() {
	return bCountry;
    }

    public void setbCountry(String bCountry) {
	this.bCountry = bCountry;
    }

    public String getbPhone() {
	return bPhone;
    }

    public void setbPhone(String bPhone) {
	this.bPhone = bPhone;
    }

    public String getbMobile() {
	return bMobile;
    }

    public void setbMobile(String bMobile) {
	this.bMobile = bMobile;
    }

    public String getbEmail() {
	return bEmail;
    }

    public void setbEmail(String bEmail) {
	this.bEmail = bEmail;
    }

    public String getLicType() {
	return licType;
    }

    public void setLicType(String licType) {
	this.licType = licType;
    }

    public String getLineAuth() {
	return lineAuth;
    }

    public void setLineAuth(String lineAuth) {
	this.lineAuth = lineAuth;
    }

    public String getLicStatus() {
	return licStatus;
    }

    public void setLicStatus(String licStatus) {
	this.licStatus = licStatus;
    }

    public String getFirstActiveDate() {
	return firstActiveDate;
    }

    public void setFirstActiveDate(String firstActiveDate) {
	this.firstActiveDate = firstActiveDate;
    }

    public String getEffectiveDate() {
	return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
	this.effectiveDate = effectiveDate;
    }

    public String getExpDate() {
	return expDate;
    }

    public void setExpDate(String expDate) {
	this.expDate = expDate;
    }

    public Long getDomState() {
	return domState;
    }

    public void setDomState(Long domState) {
	this.domState = domState;
    }

    public Boolean getResState() {
	return resState;
    }

    public void setResState(Boolean resState) {
	this.resState = resState;
    }

    public String getpAddr1() {
	return pAddr1;
    }

    public void setpAddr1(String pAddr1) {
	this.pAddr1 = pAddr1;
    }

    public String getpAddr2() {
	return pAddr2;
    }

    public void setpAddr2(String pAddr2) {
	this.pAddr2 = pAddr2;
    }

    public String getpAddr3() {
	return pAddr3;
    }

    public void setpAddr3(String pAddr3) {
	this.pAddr3 = pAddr3;
    }

    public Long getpCity() {
	return pCity;
    }

    public void setpCity(Long pCity) {
	this.pCity = pCity;
    }

    public Long getpState() {
	return pState;
    }

    public void setpState(Long pState) {
	this.pState = pState;
    }

    public String getpZip() {
	return pZip;
    }

    public void setpZip(String pZip) {
	this.pZip = pZip;
    }

    public String getpCountry() {
	return pCountry;
    }

    public void setpCountry(String pCountry) {
	this.pCountry = pCountry;
    }

    public Boolean getIsCECompliant() {
	return isCECompliant;
    }

    public void setIsCECompliant(Boolean isCECompliant) {
	this.isCECompliant = isCECompliant;
    }

    public List<ZipAreaDTO> getZips() {
	return zips;
    }

    public void setZips(List<ZipAreaDTO> zips) {
	this.zips = zips;
    }

    public Long getbCounty() {
	return bCounty;
    }

    public void setbCounty(Long bCounty) {
	this.bCounty = bCounty;
    }

    public Long getpCounty() {
	return pCounty;
    }

    public void setpCounty(Long pCounty) {
	this.pCounty = pCounty;
    }

    public Boolean getSpeaksSpanish() {
	return speaksSpanish;
    }

    public void setSpeaksSpanish(Boolean speaksSpanish) {
	this.speaksSpanish = speaksSpanish;
    }

    
    public boolean isIncAB() {
		return incAB;
	}

	public void setIncAB(boolean incAB) {
		this.incAB = incAB;
	}

	public boolean isPreCertified() {
		return preCertified;
	}

	public void setPreCertified(boolean preCertified) {
		this.preCertified = preCertified;
	}

	public boolean isCertified() {
		return certified;
	}

	public void setCertified(boolean certified) {
		this.certified = certified;
	}

	
	public String getbCityName() {
		return bCityName;
	}

	public void setbCityName(String bCityName) {
		this.bCityName = bCityName;
	}


	public String getbStateName() {
		return bStateName;
	}

	public void setbStateName(String bStateName) {
		this.bStateName = bStateName;
	}

	public String getpCityName() {
		return pCityName;
	}

	public void setpCityName(String pCityName) {
		this.pCityName = pCityName;
	}

	public String getpStateName() {
		return pStateName;
	}

	public void setpStateName(String pStateName) {
		this.pStateName = pStateName;
	}
	
	public String getPrefPhone() {
		return prefPhone;
	}

	public void setPrefPhone(String prefPhone) {
		this.prefPhone = prefPhone;
	}

	public String getPrefMobile() {
		return prefMobile;
	}

	public void setPrefMobile(String prefMobile) {
		this.prefMobile = prefMobile;
	}

	public String getPrefEmail() {
		return prefEmail;
	}

	public void setPrefEmail(String prefEmail) {
		this.prefEmail = prefEmail;
	}
	

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getIncStatus() {
		return incStatus;
	}

	public void setIncStatus(String incStatus) {
		this.incStatus = incStatus;
	}

	public String getIncABName() {
		return incABName;
	}

	public String getPreCertifiedName() {
		return preCertifiedName;
	}

	public String getCertifiedName() {
		return certifiedName;
	}

	public void setIncABName(String incABName) {
		this.incABName = incABName;
	}

	public void setPreCertifiedName(String preCertifiedName) {
		this.preCertifiedName = preCertifiedName;
	}

	public void setCertifiedName(String certifiedName) {
		this.certifiedName = certifiedName;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	public String getbCountyName() {
		return bCountyName;
	}

	public String getpCountyName() {
		return pCountyName;
	}

	public void setbCountyName(String bCountyName) {
		this.bCountyName = bCountyName;
	}

	public void setpCountyName(String pCountyName) {
		this.pCountyName = pCountyName;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public String getdStateName() {
		return dStateName;
	}

	public void setdStateName(String dStateName) {
		this.dStateName = dStateName;
	}
	
	public String getPrefCompany() {
		return prefCompany;
	}

	public String getComments() {
		return comments;
	}

	public void setPrefCompany(String prefCompany) {
		this.prefCompany = prefCompany;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	
	public List<Long> getCarriers() {
		return carriers;
	}

	public void setCarriers(List<Long> carriers) {
		this.carriers = carriers;
	}

	public String getSecondPhone() {
		return secondPhone;
	}

	public void setSecondPhone(String secondPhone) {
		this.secondPhone = secondPhone;
	}

	public BigInteger getIsRegistered() {
		return isRegistered;
	}

	public void setIsRegistered(BigInteger isRegistered) {
		this.isRegistered = isRegistered;
	}

	@Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	AgentVM other = (AgentVM) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

	/*@Override
	public String toString() {
		return "AgentVM [id=" + id + ", agentType=" + agentType + ", licNumber=" + licNumber + ", npn=" + npn
				+ ", naicno=" + naicno + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", suffix=" + suffix + ", businessName=" + businessName + ", fein=" + fein + ", bAddr1="
				+ bAddr1 + ", bAddr2=" + bAddr2 + ", bAddr3=" + bAddr3 + ", bCity=" + bCity + ", bCounty=" + bCounty
				+ ", bState=" + bState + ", bZip=" + bZip + ", bCountry=" + bCountry + ", bPhone=" + bPhone
				+ ", bMobile=" + bMobile + ", bEmail=" + bEmail + ", licType=" + licType + ", lineAuth=" + lineAuth
				+ ", licStatus=" + licStatus + ", firstActiveDate=" + firstActiveDate + ", effectiveDate="
				+ effectiveDate + ", expDate=" + expDate + ", domState=" + domState + ", resState=" + resState
				+ ", pAddr1=" + pAddr1 + ", pAddr2=" + pAddr2 + ", pAddr3=" + pAddr3 + ", pCity=" + pCity + ", pCounty="
				+ pCounty + ", pState=" + pState + ", pZip=" + pZip + ", pCountry=" + pCountry + ", isCECompliant="
				+ isCECompliant + ", speaksSpanish=" + speaksSpanish + ", zips=" + zips + ", incAB=" + incAB
				+ ", preCertified=" + preCertified + ", certified=" + certified + ", bCityName=" + bCityName
				+ ", bStateName=" + bStateName + ", pCityName=" + pCityName + ", pStateName=" + pStateName
				+ ", prefPhone=" + prefPhone + ", prefMobile=" + prefMobile + ", prefEmail=" + prefEmail
				+ ", profileId=" + profileId + ", incStatus=" + incStatus + "]";
	}


*/
   
    
}
