package com.mv1.iaa.business.view.web.req;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentReceiptVM {

	private Long id;
	private Long profileId;
	private Long userId;
	private String userType;
	private String totalAmount;
	private Long cartId;
	private String cardNo;
	private String firstName;
	private String lastName;
	private String email;
	private String langKey;
	private String transactionTime;
	private List<PackSubVM> packSubVM;
	private String copyRight;
	private String receiptDate;
	private Long cheque_no;
	private String txn_type;

	private String accountNumber;

	

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Long getCheque_no() {
		return cheque_no;
	}

	public void setCheque_no(Long cheque_no) {
		this.cheque_no = cheque_no;
	}

	public String getTxn_type() {
		return txn_type;
	}

	public void setTxn_type(String txn_type) {
		this.txn_type = txn_type;
	}

	public Long getId() {
		return id;
	}

	public Long getProfileId() {
		return profileId;
	}

	public Long getUserId() {
		return userId;
	}

	public String getUserType() {
		return userType;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public Long getCartId() {
		return cartId;
	}

	public String getCardNo() {
		return cardNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getLangKey() {
		return langKey;
	}

	public List<PackSubVM> getPackSubVM() {
		return packSubVM;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}

	public void setPackSubVM(List<PackSubVM> packSubVM) {
		this.packSubVM = packSubVM;
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}

	public String getCopyRight() {
		return copyRight;
	}

	public void setCopyRight(String copyRight) {
		this.copyRight = copyRight;
	}

	public String getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	@Override
	public String toString() {
		return "PaymentReceiptVM [id=" + id + ", profileId=" + profileId + ", userId=" + userId + ", userType="
				+ userType + ", totalAmount=" + totalAmount + ", cartId=" + cartId + ", cardNo=" + cardNo
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", langKey=" + langKey
				+ ", packSubVM=" + packSubVM + ", txn_type=" + txn_type + ", cheque_no=" + cheque_no + ", accountNumber=" + accountNumber + "]";
	}

}