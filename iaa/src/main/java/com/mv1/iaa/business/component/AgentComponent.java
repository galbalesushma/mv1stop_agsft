package com.mv1.iaa.business.component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.exception.DeleteException;
import com.mv1.iaa.business.view.web.req.AgentVM;
import com.mv1.iaa.domain.Agent;
import com.mv1.iaa.domain.Contact;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.CustomContactRepository;
import com.mv1.iaa.service.AddressService;
import com.mv1.iaa.service.AgentLicService;
import com.mv1.iaa.service.AgentService;
import com.mv1.iaa.service.BusinessService;
import com.mv1.iaa.service.CityService;
import com.mv1.iaa.service.ContactService;
import com.mv1.iaa.service.ProducerService;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.StateService;
import com.mv1.iaa.service.dto.AddressDTO;
import com.mv1.iaa.service.dto.AgentDTO;
import com.mv1.iaa.service.dto.AgentLicDTO;
import com.mv1.iaa.service.dto.BusinessDTO;
import com.mv1.iaa.service.dto.CityDTO;
import com.mv1.iaa.service.dto.ContactDTO;
import com.mv1.iaa.service.dto.ProducerDTO;
import com.mv1.iaa.service.dto.ProfileDTO;
import com.mv1.iaa.service.dto.StateDTO;

@Component
public class AgentComponent {

    private final Logger log = LoggerFactory.getLogger(AgentComponent.class);

    private static final String DELETE_AGENT_BULK = "" + "UPDATE iaa.agent a " + "	INNER JOIN iaa.profile pr "
            + "		ON pr.id = a.profile_id " + "	INNER JOIN iaa.agent_lic al " + "		ON al.id = a.lic_id "
            + "	INNER JOIN iaa.business bs " + "		ON bs.id = a.business_id " + "SET 	a.is_deleted = TRUE, "
            + "	pr.is_deleted = TRUE, " + "	al.is_deleted = TRUE, " + "	bs.is_deleted = TRUE " + "WHERE a.npn = ?";

    @Autowired
    private AddressService addressService;

    @Autowired
    private ProducerService producerService;

    @Autowired
    private AgentLicService agentLicService;

    @Autowired
    private BusinessService businessService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private AgentService agentService;

    @Autowired
    private ContactService contactService;
    
    @Autowired
    private CityService cityService;
    
    @Autowired
    private StateService stateService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private CustomAgentRepository customAgentRepository;

    @Autowired
    private CustomContactRepository customContactRepository;
    
    @Autowired
    private SearchObjectWriter<com.mv1.iaa.business.view.search.Agent> indexWriter; 

    private static String[] keys = new String[] { "id", "npn", "naicno", "fein", "resState", "firstName", "middleName",
            "lastName", "suffix", "businessName", "licNumber", "licType", "lineAuth", "firstActiveDate",
            "effectiveDate", "expDate", "licStatus", "domState", "bAddr1", "bAddr2", "bAddr3", "bZip", "bCountry","bCounty",
            "bCity", "bState", "isCECompliant", "pAddr1", "pAddr2", "pAddr3", "pZip", "pCountry", "pCounty",
            "pCity", "pState", 
            "bPhone", "bMobile","bEmail" ,"incAB","certified" ,"preCertified" ,"speaksSpanish" };

    private static String[] agentSignUpKeys = new String[] { "id", "npn", "firstName", "middleName",
            "lastName", "suffix"};

    
    private static String[] aKeys = new String[] { "id", "npn", "naicno", "fein", "resState", "firstName", "middleName",
            "lastName", "suffix", "businessName", "licNumber", "licType", "lineAuth", "firstActiveDate",
            "effectiveDate", "expDate", "licStatus", "domState", "bAddr1", "bAddr2", "bAddr3", "bZip", "bCountry","bCounty",
            "bCity", "bState", "isCECompliant", "pAddr1", "pAddr2", "pAddr3", "pZip", "pCountry", "pCounty",
            "pCity", "pState", 
            "bPhone", "bMobile","bEmail" ,"speaksSpanish","prefCompany","comments" ,"prefEmail"};

    public List<AgentVM> getAllAgents() {

        List<AgentVM> agents = new LinkedList<>();
        Object[][] agArr = customAgentRepository.getAllAgents();
        Object[][] agArr1 = customAgentRepository.getAllSignUpOnlyAgents();
        if (agArr != null && agArr.length > 0) {
            for (Object[] arr : agArr) {
                try {

                    agents.add(CommonMapper.fromKeyVal(keys, arr, AgentVM.class));
                } catch (Exception e) {
                    log.debug("Exception while mapping all agents data ", e);
                }
            }
        }
        
        List<AgentVM> agents1 = new LinkedList<>();
        if (agArr1 != null && agArr1.length > 0) {
            for (Object[] arr1 : agArr1) {
                try {

                    agents1.add(CommonMapper.fromKeyVal(agentSignUpKeys, arr1, AgentVM.class));
                } catch (Exception e) {
                    log.debug("Exception while mapping all agents data ", e);
                }
            }
        }
        
        if(!agents1.isEmpty()){
        	agents.addAll(agents1);
        }
        return agents;
    }

    public AgentVM getAgent(String npn) {

        Object[][] agArr = customAgentRepository.getAgent(npn);

        if (agArr != null && agArr.length > 0) {
            for (Object[] arr : agArr) {
                try {
                    return CommonMapper.fromKeyVal(aKeys, arr, AgentVM.class);
                } catch (Exception e) {
                    log.debug("Exception while mapping all agents data ", e);
                }
            }
        }
        return null;
    }

    public AgentVM getAgentByProfileId(Long profileId) {

        Object[][] agArr = customAgentRepository.getAgentByProfileId(profileId);

        if (agArr != null && agArr.length > 0) {
            for (Object[] arr : agArr) {
                try {
                    return CommonMapper.fromKeyVal(aKeys, arr, AgentVM.class);
                } catch (Exception e) {
                    log.debug("Exception while mapping all agents data ", e);
                }
            }
        }
        return null;
    }
    
    @Transactional
    public boolean enrollAgent(AgentVM vm) throws Exception {

        // Save producer address
        AddressDTO pAddr = CommonMapper.producerAddressFromVM(vm);
        pAddr = addressService.save(pAddr);

        // Save Producer
        ProducerDTO pDto = CommonMapper.producerFromVM(vm, pAddr.getId());
        pDto = producerService.save(pDto);

        // Save license details
        AgentLicDTO agentLicDto = CommonMapper.agentLicFromVM(vm, pDto.getId());
        agentLicDto = agentLicService.save(agentLicDto);

        // Save business address
        AddressDTO bAddr = CommonMapper.businessAddressFromVM(vm);
        bAddr = addressService.save(bAddr);

        // Save business Contact
        ContactDTO cDto = CommonMapper.businessContactFromVM(vm, bAddr.getId());
        cDto = contactService.save(cDto);

        // Save business
        BusinessDTO bDto = CommonMapper.businessDtoFromVM(vm, bAddr.getId());
        bDto = businessService.save(bDto);

        // Save profile
        ProfileDTO profileDto = CommonMapper.profileDtoFromVM(vm, bAddr.getId());
        profileDto = profileService.save(profileDto);

        // Save agent
        AgentDTO aDto = CommonMapper.agentDtoFromVM(vm, bDto.getId(), agentLicDto.getId(), profileDto.getId());
        aDto = agentService.save(aDto);

        // upload the agent to elastic-search
        write2ElasticIndex(vm, aDto.getId());
        
        return aDto.getId() != null;
    }
    
    private boolean write2ElasticIndex(AgentVM vm, Long agentId) {
        // upload the agent to elastic-search
        Optional<CityDTO> optCity = cityService.findOne(vm.getbCity());
        String city = "", state = "";
        if (optCity.isPresent()) {
            city = optCity.get().getName();
        }
        Optional<StateDTO> optState = stateService.findOne(vm.getbState());
        if (optState.isPresent()) {
            state = optState.get().getStateCode();
        }
        com.mv1.iaa.business.view.search.Agent value = CommonMapper.agentSearchVM(vm, city, agentId, state);
        boolean isWritten = indexWriter.write2Elasticsearch(value, agentId);
        if (!isWritten) {
            log.debug("Failed to write agent in search index ");
        }
        return isWritten;
    }

    @Transactional
    public boolean editAgent(AgentVM vm) throws Exception {

        Optional<AgentDTO> agOpt = agentService.findOne(vm.getId());

        if (!agOpt.isPresent()) {
            return false;
        }

        AgentDTO agEdit = agOpt.get();

        Optional<ProfileDTO> prfOpt = profileService.findOne(agEdit.getProfileId());
        if (!prfOpt.isPresent()) {
            return false;
        }
        ProfileDTO prEdit = prfOpt.get();

        Optional<BusinessDTO> bsOpt = businessService.findOne(agEdit.getBusinessId());
        if (!bsOpt.isPresent()) {
            return false;
        }
        BusinessDTO bsEdit = bsOpt.get();

        Contact cont = customContactRepository.findOneByAddressId(bsEdit.getAddressId());

        Optional<AddressDTO> bAddrOpt = addressService.findOne(bsEdit.getAddressId());
        if (!bAddrOpt.isPresent()) {
            return false;
        }
        AddressDTO bAddrEdit = bAddrOpt.get();

        Optional<AgentLicDTO> agtLicOpt = agentLicService.findOne(agEdit.getLicId());
        if (!agtLicOpt.isPresent()) {
            return false;
        }
        AgentLicDTO agLicEdit = agtLicOpt.get();

        Optional<ProducerDTO> proOpt = producerService.findOne(agLicEdit.getProducerId());
        if (!proOpt.isPresent()) {
            return false;
        }
        ProducerDTO proEdit = proOpt.get();

        Optional<AddressDTO> pAddrOpt = addressService.findOne(proEdit.getAddrId());
        if (!pAddrOpt.isPresent()) {
            return false;
        }
        AddressDTO pAddrEdit = pAddrOpt.get();

        // Save producer address
        AddressDTO pAddr = CommonMapper.producerAddressFromVM(vm);
        pAddr.setId(pAddrEdit.getId());
        pAddr.setCreatedAt(pAddrEdit.getCreatedAt());
        pAddr = addressService.save(pAddr);

        // Save Producer
        ProducerDTO pDto = CommonMapper.producerFromVM(vm, pAddr.getId());
        pDto.setId(proEdit.getId());
        pDto = producerService.save(pDto);

        // Save license details
        AgentLicDTO agentLicDto = CommonMapper.agentLicFromVM(vm, pDto.getId());
        agentLicDto.setId(agLicEdit.getId());
        agentLicDto.setCreatedAt(agLicEdit.getCreatedAt());
        agentLicDto = agentLicService.save(agentLicDto);

        // Save business address
        AddressDTO bAddr = CommonMapper.businessAddressFromVM(vm);
        bAddr.setId(bAddrEdit.getId());
        bAddr.setCreatedAt(bAddrEdit.getCreatedAt());
        bAddr = addressService.save(bAddr);

        // Save business Contact
        ContactDTO cDto = CommonMapper.businessContactFromVM(vm, bAddr.getId());
        cDto.setId(cont.getId());
        cDto = contactService.save(cDto);

        // Save business
        BusinessDTO bDto = CommonMapper.businessDtoFromVM(vm, bAddr.getId());
        bDto.setId(bsEdit.getId());
        bDto.setCreatedAt(bsEdit.getCreatedAt());
        bDto = businessService.save(bDto);

        // Save profile
        ProfileDTO profileDto = CommonMapper.profileDtoFromVM(vm, bAddr.getId());
        profileDto.setId(prEdit.getId());
        profileDto.setCreatedAt(prEdit.getCreatedAt());
        profileDto = profileService.save(profileDto);

        // Save agent
        AgentDTO aDto = CommonMapper.agentDtoFromVM(vm, bDto.getId(), agentLicDto.getId(), profileDto.getId());
        aDto.setId(agEdit.getId());
        aDto.setCreatedAt(agEdit.getCreatedAt());
        aDto = agentService.save(aDto);
        
        // upload the agent to elastic-search
        write2ElasticIndex(vm, aDto.getId());
        
        return aDto.getId() != null;
    }

    @Transactional(rollbackFor = { DeleteException.class })
    public Integer deleteAgent(Set<String> agentNpn) throws DeleteException {

        log.debug("Request to delete Agents : {}", agentNpn);

        List<Agent> agentsFromDBToDelete = customAgentRepository.findByNpnIn(agentNpn);
        if (!agentsFromDBToDelete.isEmpty() && agentNpn.size() == agentsFromDBToDelete.size()) {

            List<Object[]> list = agentNpn.stream().map(i -> new String[] { i }).collect(Collectors.toList());
            try {
                int count[] = jdbcTemplate.batchUpdate(DELETE_AGENT_BULK, list);
                if (count.length == agentNpn.size()) {
                    log.debug("Agents removed {} sucessfully.", agentNpn);
                } else {
                    throw new DeleteException("Failed to delete one or more agent(s)");
                }
                return count.length;
            } catch (Throwable cause) {
                log.error("Failed to remove agents {}", agentNpn);
                throw new DeleteException(cause.getMessage());
            }
        } else {
            log.error("Failed to remove agents {}", agentNpn);
            throw new DeleteException("No matching agents found for given IDs");
        }
    }
}
