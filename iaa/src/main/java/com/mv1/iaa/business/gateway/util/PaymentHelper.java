package com.mv1.iaa.business.gateway.util;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.mv1.iaa.business.gateway.bluepay.BluePay;
import com.mv1.iaa.business.gateway.model.PayRequest;
import com.mv1.iaa.business.gateway.model.PayResponse;
import com.mv1.iaa.business.view.web.req.PaymentVM;

public class PaymentHelper implements PGConst {

	private static final Logger log = LoggerFactory.getLogger(PaymentHelper.class);

	/*
	 * private static final String ACCOUNT_ID = "100618791210"; private static final
	 * String SECRET_KEY = "CSKOCZNVUICPMGFP5FESUFAZC0R7SJCB";
	 * 
	 * private static final String MODE = "TEST";
	 */

//    private static final String MODE = "LIVE";

	public static PayRequest getPayRequest(PaymentVM p, String ACCOUNT_ID, String SECRET_KEY, String MODE) {

		log.debug("Helper getPayRequest : payment {}", p);

		PayRequest payRequest = new PayRequest();

		// Set Customer Information
		HashMap<String, String> customerParams = new HashMap<>();

		log.debug("Helper getPayResponse :ACCOUNT_ID---------------", ACCOUNT_ID);

		log.debug("Helper getPayResponse : SECRET_KEY-------------", SECRET_KEY);

		BluePay payment = new BluePay(ACCOUNT_ID, SECRET_KEY, MODE);

		customerParams.put(FIRST_NAME, p.getFirstName());
		customerParams.put(LAST_NAME, p.getLastName());
		customerParams.put(ADDR1, p.getAddr());
		customerParams.put(ADDR2, p.getStreet());
		customerParams.put(CITY, p.getCity());
		customerParams.put(STATE, p.getState());
		customerParams.put(ZIP, p.getZip());
		customerParams.put(COUNTRY, "USA");
		customerParams.put(PHONE, p.getPhone());
		customerParams.put(EMAIL, p.getEmail() != null ? p.getEmail() : "test@bluepay.com");

		payment.setCustomID1(p.getProfileId().toString());
		payment.setCustomerInformation(customerParams);
		payment.setOrderID(p.getCartId().toString());

		// Set Credit Card Information
		HashMap<String, String> ccParams = new HashMap<>();

		if (p.getTxnType()!=null && p.getTxnType().equalsIgnoreCase("card")) {
			ccParams.put(CARD_NUMBER, p.getCardNo());
			ccParams.put(EXP_DATE, p.getValidTill());
			ccParams.put(CVV2, p.getCvv());
			payment.setCCInformation(ccParams);
		}
//set ACH payment information ,Date 09 Aug 19
		else if (p.getTxnType()!=null && p.getTxnType().equalsIgnoreCase("ACH")) {
			ccParams.put(ACH_ROUTING, p.getRoutingNumber());
			ccParams.put(ACH_ACCOUNT, p.getAccountNumber());
			ccParams.put(ACH_ACCOUNT_TYPE, p.getAccountType());
			ccParams.put(DOC_TYPE, "web");
			payment.setACHInformation(ccParams);
		}

		/*
		 * ccParams.put(CARD_NUMBER, p.getCardNo()); ccParams.put(EXP_DATE,
		 * p.getValidTill()); ccParams.put(CVV2, p.getCvv());
		 * payment.setCCInformation(ccParams);
		 */
		// Set sale amount
		HashMap<String, String> saleParams = new HashMap<>();

		
		/*
		 * if(Float.parseFloat(p.getAmount())%2==0) {
		 * p.setAmount((Float.parseFloat(p.getAmount())+1)+""); }
		 */
		 

		saleParams.put(AMOUNT, p.getAmount());
		payment.sale(saleParams);

		payRequest.setOrderId(p.getCartId().toString());
		payRequest.setPayment(payment);

		return payRequest;
	}

	public static PayResponse getPayResponse(BluePay payment) {

		log.debug("Helper getPayResponse : bluepay payment {}", payment);

		PayResponse payResponse = new PayResponse();

		payResponse.setSuccessful(payment.isSuccessful());
		payResponse.setAuthCode(payment.getAuthCode());
		payResponse.setAvsResp(payment.getAVS());

		payResponse.setMaskedAc(payment.getMaskedPaymentAccount());
		payResponse.setMessage(payment.getMessage());
		payResponse.setStatus(payment.getStatus());
		payResponse.setTxnId(payment.getTransID());
		payResponse.setCardType(payment.getCardType());
		payResponse.setCvv2Resp(payment.getCVV2());
		payResponse.setAchRouting(payment.getACH_ROUTING());
		payResponse.setAchAccountType(payment.getACH_ACCOUNT_TYPE());
		payResponse.setAchAccount(payment.getACH_ACCOUNT());
			
		

		return payResponse;
	}

	public static PayResponse getErrorPayResponse(BluePay payment) {

		log.debug("Helper getErrorPayResponse : bluepay payment {}", payment);

		PayResponse payResponse = new PayResponse();
		payResponse.setSuccessful(false);
		payResponse.setAuthCode(payment.getAuthCode());
		payResponse.setAvsResp(payment.getAVS());
		if (payment.getCardType() != null || payment.getCardType() != "") {
			payResponse.setCardType(payment.getCardType());
			payResponse.setCvv2Resp(payment.getCVV2());
		} else {
			if (payment.getACH_ROUTING() != null || payment.getACH_ROUTING() != "") {
				payResponse.setAchRouting(payment.getACH_ROUTING());
				payResponse.setAchAccountType(payment.getACH_ACCOUNT_TYPE());
				payResponse.setAchAccount(payment.getACH_ACCOUNT());
			}
		}
		payResponse.setMaskedAc(payment.getMaskedPaymentAccount());
		payResponse.setMessage(payment.getMessage());
		payResponse.setStatus(payment.getStatus());
		payResponse.setTxnId(payment.getTransID());

		return payResponse;
	}
}
