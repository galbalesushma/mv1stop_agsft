package com.mv1.iaa.business.gateway.util;

public interface PGConst {

    String FIRST_NAME = "firstName";
    String LAST_NAME = "lastName";
    String ADDR1 = "address1";
    String ADDR2 = "address2";
    String CITY = "city";
    String STATE = "state";
    String ZIP = "zip";
    String COUNTRY = "country";
    String PHONE = "phone";
    String EMAIL = "email";
    String CUST_TOKEN = "customerToken";
    String CARD_NUMBER = "cardNumber";
    String EXP_DATE = "expirationDate";
    String CVV2 = "cvv2";
    String AMOUNT = "amount";
    String ACH_ROUTING="routingNum";
    String ACH_ACCOUNT="accountNum";
    String ACH_ACCOUNT_TYPE="accountType";
    String DOC_TYPE ="docType";
    
}
