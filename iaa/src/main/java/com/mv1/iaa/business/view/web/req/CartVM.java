package com.mv1.iaa.business.view.web.req;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CartVM {

    private Long cartId;
    private Long profileId;

    private List<Item> items;
    private String total;

    public Long getCartId() {
	return cartId;
    }

    public void setCartId(Long cartId) {
	this.cartId = cartId;
    }

    public Long getProfileId() {
	return profileId;
    }

    public void setProfileId(Long profileId) {
	this.profileId = profileId;
    }

    public List<Item> getItems() {
	return items;
    }

    public void setItems(List<Item> items) {
	this.items = items;
    }

    public String getTotal() {
	return total;
    }

    public void setTotal(String total) {
	this.total = total;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cartId == null) ? 0 : cartId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	CartVM other = (CartVM) obj;
	if (cartId == null) {
	    if (other.cartId != null)
		return false;
	} else if (!cartId.equals(other.cartId))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "CartVM [cartId=" + cartId + ", profileId=" + profileId + ", items=" + items + ", total=" + total + "]";
    }

}
