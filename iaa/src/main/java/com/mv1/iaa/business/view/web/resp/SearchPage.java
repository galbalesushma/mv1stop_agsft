package com.mv1.iaa.business.view.web.resp;

import java.util.List;

import com.mv1.iaa.business.view.web.req.SearchVM;

public class SearchPage {

	private Integer total;
	private List<SearchVM> result;
	private Integer totalPages;
	private Long searchId;
	
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public List<SearchVM> getResult() {
		return result;
	}

	public void setResult(List<SearchVM> result) {
		this.result = result;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}
	
	public Long getSearchId() {
		return searchId;
	}

	public void setSearchId(Long searchId) {
		this.searchId = searchId;
	}

	@Override
    public String toString() {
	return "Page [total=" + total + ", result=" + result + "]";
	}
    

}
