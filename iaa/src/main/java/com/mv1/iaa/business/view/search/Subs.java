package com.mv1.iaa.business.view.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Subs {

    private Long subsId;
    private Integer slots;
    private String duration;
    private String zip;
    private String rank;
    private Long score;

    public Long getSubsId() {
        return subsId;
    }

    public void setSubsId(Long subsId) {
        this.subsId = subsId;
    }

    public Integer getSlots() {
        return slots;
    }

    public void setSlots(Integer slots) {
        this.slots = slots;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((subsId == null) ? 0 : subsId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Subs other = (Subs) obj;
        if (subsId == null) {
            if (other.subsId != null)
                return false;
        } else if (!subsId.equals(other.subsId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Subs [subsId=" + subsId + ", slots=" + slots + ", duration=" + duration + ", zip=" + zip + ", rank="
                + rank + ", score=" + score + "]";
    }
}
