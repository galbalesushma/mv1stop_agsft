package com.mv1.iaa.business.view.web.req;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mv1.iaa.web.rest.vm.ManagedUserVM;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgentSignupVM extends ManagedUserVM {

    private String npn;
    private String lang;
    private String preferredEmail;
    private String phone;
    private String preferredMobile;
    private String refBy;
    private String prefCompany;
    private String comments;
    private String insuranceCompany;
    private List<Long> carriers;
	private String secondPhone;
    
    public String getNpn() {
        return npn;
    }

    public void setNpn(String npn) {
        this.npn = npn;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getPreferredEmail() {
        return preferredEmail;
    }

    public void setPreferredEmail(String preferredEmail) {
        this.preferredEmail = preferredEmail;
    }

    public String getPreferredMobile() {
        return preferredMobile;
    }

    public void setPreferredMobile(String preferredMobile) {
        this.preferredMobile = preferredMobile;
    }

    public String getRefBy() {
        return refBy;
    }

    public void setRefBy(String refBy) {
        this.refBy = refBy;
    }

    
    public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getPrefCompany() {
		return prefCompany;
	}

	public String getComments() {
		return comments;
	}

	public void setPrefCompany(String prefCompany) {
		this.prefCompany = prefCompany;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public List<Long> getCarriers() {
		return carriers;
	}

	public void setCarriers(List<Long> carriers) {
		this.carriers = carriers;
	}
	
	public String getSecondPhone() {
		return secondPhone;
	}

	public void setSecondPhone(String secondPhone) {
		this.secondPhone = secondPhone;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((npn == null) ? 0 : npn.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AgentSignupVM other = (AgentSignupVM) obj;
        if (npn == null) {
            if (other.npn != null)
                return false;
        } else if (!npn.equals(other.npn))
            return false;
        return true;
    }

	@Override
	public String toString() {
		return "AgentSignupVM [npn=" + npn + ", lang=" + lang + ", preferredEmail=" + preferredEmail + ", phone="
				+ phone + ", preferredMobile=" + preferredMobile + ", refBy=" + refBy + ", prefCompany=" + prefCompany
				+ ", comments=" + comments + ", insuranceCompany=" + insuranceCompany + ", carriers=" + carriers + "]";
	}

 
}
