package com.mv1.iaa.business.common;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

import com.opencsv.CSVParser;
import com.opencsv.enums.CSVReaderNullFieldIndicator;

public class MyCsvParser extends CSVParser {
    
    
    /**
     * Constructs CSVParser.
     * <p>This constructor sets all necessary parameters for CSVParser, and
     * intentionally has package access so only the builder can use it.</p>
     * 
     * @param separator               The delimiter to use for separating entries
     * @param quotechar               The character to use for quoted elements
     * @param escape                  The character to use for escaping a separator or quote
     * @param strictQuotes            If true, characters outside the quotes are ignored
     * @param ignoreLeadingWhiteSpace If true, white space in front of a quote in a field is ignored
     * @param ignoreQuotations        If true, treat quotations like any other character.
     * @param nullFieldIndicator      Which field content will be returned as null: EMPTY_SEPARATORS, EMPTY_QUOTES,
     *                                BOTH, NEITHER (default)
     * @param errorLocale             Locale for error messages.
     */
    @SuppressWarnings("deprecation")
    MyCsvParser(char separator, char quotechar, char escape, boolean strictQuotes, boolean ignoreLeadingWhiteSpace,
              boolean ignoreQuotations, CSVReaderNullFieldIndicator nullFieldIndicator, Locale errorLocale) {
//	CSVParser(char separator, char quotechar, char escape, boolean strictQuotes, boolean ignoreLeadingWhiteSpace,
//                boolean ignoreQuotations)
	super(separator,
                quotechar,
                escape,
                strictQuotes,
                ignoreLeadingWhiteSpace,
                ignoreQuotations);
    }

    /**
     * Parses an incoming String and returns an array of elements.
     * This method is used when all data is contained in a single line.
     *
     * @param nextLine Line to be parsed.
     * @return The list of elements, or null if nextLine is null
     * @throws IOException If bad things happen during the read
     */
    @Override
    public String[] parseLine(String nextLine) throws IOException {
        return (String[]) Arrays.stream(parseLine(nextLine, false))
        	.map(e -> e != null ? e.trim() : e)
        	.toArray();
    }
}
