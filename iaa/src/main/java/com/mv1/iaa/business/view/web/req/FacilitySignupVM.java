package com.mv1.iaa.business.view.web.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mv1.iaa.web.rest.vm.ManagedUserVM;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FacilitySignupVM extends ManagedUserVM {

    // Inspection station number
    private String isn;
    private String lang;
    private String preferredMobile;
    private String preferredEmail;
    private String refBy;
    private String phone;
    private String prefCompany;
    private String comments;
    private String mechDuty;
    
    public String getIsn() {
        return isn;
    }

    public void setIsn(String isn) {
        this.isn = isn;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getPreferredMobile() {
        return preferredMobile;
    }

    public void setPreferredMobile(String preferredMobile) {
        this.preferredMobile = preferredMobile;
    }

    public String getPreferredEmail() {
        return preferredEmail;
    }

    public void setPreferredEmail(String preferredEmail) {
        this.preferredEmail = preferredEmail;
    }

    public String getRefBy() {
        return refBy;
    }

    public void setRefBy(String refBy) {
        this.refBy = refBy;
    }

    public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getPrefCompany() {
		return prefCompany;
	}

	public String getComments() {
		return comments;
	}

	public void setPrefCompany(String prefCompany) {
		this.prefCompany = prefCompany;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getMechDuty() {
		return mechDuty;
	}

	public void setMechDuty(String mechDuty) {
		this.mechDuty = mechDuty;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((isn == null) ? 0 : isn.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FacilitySignupVM other = (FacilitySignupVM) obj;
        if (isn == null) {
            if (other.isn != null)
                return false;
        } else if (!isn.equals(other.isn))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FacilitySignupVM [isn=" + isn + ", lang=" + lang + ", preferredMobile=" + preferredMobile
                + ", preferredEmail=" + preferredEmail + ", refBy=" + refBy + "]";
    }

}
