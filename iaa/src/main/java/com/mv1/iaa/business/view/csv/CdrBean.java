package com.mv1.iaa.business.view.csv;

import javax.persistence.Column;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.opencsv.bean.CsvBindByName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Validated
public class CdrBean {

	@CsvBindByName(column = "calldate")
	private String callDate;

	@CsvBindByName(column = "clid")
	private String clid;

	@CsvBindByName(column = "src")
	private String src;

	@CsvBindByName(column = "dst")
	private String dst;

	@CsvBindByName(column = "dcontext")
	private String dcontext;

	@CsvBindByName(column = "channel")
	private String channel;

	@CsvBindByName(column = "dstchannel")
	private String dstchannel;

	@CsvBindByName(column = "lastapp")
	private String lastapp;

	@CsvBindByName(column = "lastdata")
	private String lastdata;

	@CsvBindByName(column = "duration")
	private String duration;

	@CsvBindByName(column = "billsec")
	private String billsec;

	@CsvBindByName(column = "disposition")
	private String disposition;

	@CsvBindByName(column = "amaflags")
	private String amaflags;

	@CsvBindByName(column = "accountcode")
	private String accountcode;

	@CsvBindByName(column = "uniqueid")
	private String uniqueid;

	@CsvBindByName(column = "userfield")
	private String userfield;

	@CsvBindByName(column = "did")
	private String did;

	@CsvBindByName(column = "recordingfile")
	private String recordingfile;

	@CsvBindByName(column = "cnum")
	private String cnum;

	@CsvBindByName(column = "cnam")
	private String cnam;

	@CsvBindByName(column = "outbound_cnum")
	private String outbound_cnum;

	@CsvBindByName(column = "outbound_cnam")
	private String outbound_cnam;

	@CsvBindByName(column = "dst_cnam")
	private String dst_cnam;

	@CsvBindByName(column = "linkedid")
	private String linkedid;

	@CsvBindByName(column = "peeraccount")
	private String peeraccount;

	@CsvBindByName(column = "sequence")
	private String sequence;

	public String getCallDate() {
		return callDate;
	}

	public String getClid() {
		return clid;
	}

	public String getSrc() {
		return src;
	}

	public String getDst() {
		return dst;
	}

	public String getDcontext() {
		return dcontext;
	}

	public String getChannel() {
		return channel;
	}

	public String getDstchannel() {
		return dstchannel;
	}

	public String getLastapp() {
		return lastapp;
	}

	public String getLastdata() {
		return lastdata;
	}

	public String getDuration() {
		return duration;
	}

	public String getBillsec() {
		return billsec;
	}

	public String getDisposition() {
		return disposition;
	}

	public String getAmaflags() {
		return amaflags;
	}

	public String getAccountcode() {
		return accountcode;
	}

	public String getUniqueid() {
		return uniqueid;
	}

	public String getUserfield() {
		return userfield;
	}

	public String getDid() {
		return did;
	}

	public String getRecordingfile() {
		return recordingfile;
	}

	public String getCnum() {
		return cnum;
	}

	public String getCnam() {
		return cnam;
	}

	public String getOutbound_cnum() {
		return outbound_cnum;
	}

	public String getOutbound_cnam() {
		return outbound_cnam;
	}

	public String getDst_cnam() {
		return dst_cnam;
	}

	public void setCallDate(String callDate) {
		this.callDate = callDate;
	}

	public void setClid(String clid) {
		this.clid = clid;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}

	public void setDcontext(String dcontext) {
		this.dcontext = dcontext;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public void setDstchannel(String dstchannel) {
		this.dstchannel = dstchannel;
	}

	public void setLastapp(String lastapp) {
		this.lastapp = lastapp;
	}

	public void setLastdata(String lastdata) {
		this.lastdata = lastdata;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setBillsec(String billsec) {
		this.billsec = billsec;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public void setAmaflags(String amaflags) {
		this.amaflags = amaflags;
	}

	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}

	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}

	public void setUserfield(String userfield) {
		this.userfield = userfield;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public void setRecordingfile(String recordingfile) {
		this.recordingfile = recordingfile;
	}

	public void setCnum(String cnum) {
		this.cnum = cnum;
	}

	public void setCnam(String cnam) {
		this.cnam = cnam;
	}

	public void setOutbound_cnum(String outbound_cnum) {
		this.outbound_cnum = outbound_cnum;
	}

	public void setOutbound_cnam(String outbound_cnam) {
		this.outbound_cnam = outbound_cnam;
	}

	public void setDst_cnam(String dst_cnam) {
		this.dst_cnam = dst_cnam;
	}

	public String getLinkedid() {
		return linkedid;
	}

	public void setLinkedid(String linkedid) {
		this.linkedid = linkedid;
	}

	public String getPeeraccount() {
		return peeraccount;
	}

	public void setPeeraccount(String peeraccount) {
		this.peeraccount = peeraccount;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	@Override
	public String toString() {
		return "CdrBean [callDate=" + callDate + ", clid=" + clid + ", src=" + src + ", dst=" + dst + ", dcontext="
				+ dcontext + ", channel=" + channel + ", dstchannel=" + dstchannel + ", lastapp=" + lastapp
				+ ", lastdata=" + lastdata + ", duration=" + duration + ", billsec=" + billsec + ", disposition="
				+ disposition + ", amaflags=" + amaflags + ", accountcode=" + accountcode + ", uniqueid=" + uniqueid
				+ ", userfield=" + userfield + ", did=" + did + ", recordingfile=" + recordingfile + ", cnum=" + cnum
				+ ", cnam=" + cnam + ", outbound_cnum=" + outbound_cnum + ", outbound_cnam=" + outbound_cnam
				+ ", dst_cnam=" + dst_cnam + ", linkedid=" + linkedid + ", peeraccount=" + peeraccount + ", sequence="
				+ sequence + "]";
	}

}
