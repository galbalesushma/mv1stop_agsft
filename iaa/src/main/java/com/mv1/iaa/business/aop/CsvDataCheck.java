package com.mv1.iaa.business.aop;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;

@Aspect
public class CsvDataCheck {

//    @Around("execution(* com.mv1.iaa.business.*.*.*(..))")
    @Before("execution (* com.mv1.iaa.business.view.csv.*.*(..))")
    public void before(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("Inside pointcut .. ");
        Method method = MethodSignature.class.cast(pjp.getSignature()).getMethod();
        Object[] args = pjp.getArgs();
//        StringBuilder data = new StringBuilder();
        Annotation[][] paramAnnots = method.getParameterAnnotations();
        
        for (int argIndex = 0; argIndex < args.length; argIndex++) {
        
            for (Annotation pa : paramAnnots[argIndex]) {
            
                if (!(pa instanceof CsvData)) {
                    continue;
                }
                
                CsvData da = (CsvData) pa;
                if (da.methodName().length() > 0) {
                    
                    Object obj = args[argIndex];
                    System.out.println("val in aop : " + (String) obj);
//                    Method dataMethod = obj.getClass().getMethod(da.methodName());
//                    data.append(dataMethod.invoke(obj));
//                    continue;
                }
//                data.append(args[argIndex]);
            }
        }
        // continue with the setter method
        pjp.proceed();
    }
}
