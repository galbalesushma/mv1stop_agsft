package com.mv1.iaa.business.enumeration;

public enum TxnType {

	PAPER_CHECK,
	CARD_PAYMENT;
}
