package com.mv1.iaa.business.view.web.req;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchVM {

	private String packageId;
	private String zip;
	private String buyZip;
	private String firstName;
	private String lastName;
	private String phone;
	private String slots;
	private String city;
	private String state;
	private String bName;
	private String pref_company;
	private String comments;
	private String npn;
	private String extension;
	private String ce_compliance;
	private int sr;
	private String fullname;
	private boolean isAdded = false;
	private String insurance_company;
	private String addr_1;
	private String addr_2;
	private String addr_3;
    private List<Long> carriers;
	
	  private static final String[] keys = new String[] {
	            "packageId",           
	            "zip",
	            "buyZip", 
	            "firstName",         
	            "lastName",
	            "phone",
	            "slots",
	            "city",
	            "state",
	            "bName",
	            "pref_company",
	            "comments",
	            "npn",
	            "insurance_company",
	            "ce_compliance",
	            "addr_1",
	            "addr_2",
	            "addr_3"
	  };
	  
		
	  private static final String[] faclilityKeys = new String[] {
	            "packageId",           
	            "zip",
	            "buyZip", 
	            "firstName",         
	            "lastName",
	            "phone",
	            "slots",
	            "city",
	            "state",
	            "bName",
	            "pref_company",
	            "comments",
	            "npn",
	            "addr_1",
	            "addr_2",
	            "addr_3"
	  };
	  
	  private static final String[] keysForWithouSignup = new String[] {         
	            "zip",
	            "firstName",         
	            "lastName",
	            "phone",
	            "city",
	            "state",
	            "bName",
	            "pref_company",
	            "comments",
	            "npn"
	  };

	public String getPackageId() {
		return packageId;
	}

	public String getZip() {
		return zip;
	}

	public String getBuyZip() {
		return buyZip;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhone() {
		return phone;
	}

	public String getSlots() {
		return slots;
	}

	public static String[] getKeys() {
		return keys;
	}

	public static String[] getKeysforwithousignup() {
		return keysForWithouSignup;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setBuyZip(String buyZip) {
		this.buyZip = buyZip;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setSlots(String slots) {
		this.slots = slots;
	}
	
	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getbName() {
		return bName;
	}

	public void setbName(String bName) {
		this.bName = bName;
	}
	
	public String getPref_company() {
		return pref_company;
	}

	public String getComments() {
		return comments;
	}

	public void setPref_company(String pref_company) {
		this.pref_company = pref_company;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getNpn() {
		return npn;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	public String getCe_compliance() {
		return ce_compliance;
	}

	public void setCe_compliance(String ce_compliance) {
		this.ce_compliance = ce_compliance;
	}

	public int getSr() {
		return sr;
	}

	public void setSr(int sr) {
		this.sr = sr;
	}
	

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public boolean isAdded() {
		return isAdded;
	}

	public void setAdded(boolean isAdded) {
		this.isAdded = isAdded;
	}

	
	public String getInsurance_company() {
		return insurance_company;
	}

	public void setInsurance_company(String insurance_company) {
		this.insurance_company = insurance_company;
	}
	
	public String getAddr_1() {
		return addr_1;
	}

	public void setAddr_1(String addr_1) {
		this.addr_1 = addr_1;
	}

	public String getAddr_2() {
		return addr_2;
	}

	public void setAddr_2(String addr_2) {
		this.addr_2 = addr_2;
	}

	public String getAddr_3() {
		return addr_3;
	}

	public void setAddr_3(String addr_3) {
		this.addr_3 = addr_3;
	}

	public List<Long> getCarriers() {
		return carriers;
	}

	public void setCarriers(List<Long> carriers) {
		this.carriers = carriers;
	}

	public static String[] getFaclilitykeys() {
		return faclilityKeys;
	}

	@Override
	public String toString() {
		return "SearchVM [packageId=" + packageId + ", zip=" + zip + ", buyZip=" + buyZip + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", phone=" + phone + ", slots=" + slots + ", city=" + city + ", state="
				+ state + ", bName=" + bName + ", pref_company=" + pref_company + ", comments=" + comments + ", npn="
				+ npn + ", extension=" + extension + ", ce_compliance=" + ce_compliance + ", sr=" + sr + ", fullname="
				+ fullname + ", isAdded=" + isAdded + "]";
	}

	


	

}
