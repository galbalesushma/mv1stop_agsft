package com.mv1.iaa.business.component;

import java.io.IOException;
import java.util.Collections;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv1.iaa.business.enumeration.SearchTypes;

@Component
public class SearchObjectWriter <T> {

    private static final Logger log = LoggerFactory.getLogger(SearchObjectWriter.class);

    private static final String elasticServer = "elastic-search";

    private RestClient restClient = RestClient.builder(new HttpHost(elasticServer, 9200, "http")).build();
    
	public boolean write2Elasticsearch(T value, Long id) {

		Response indexResponse = null;
		try {
			// index a document
		    String index = SearchTypes.idxFrom(value.getClass());
		    String indexType = SearchTypes.typeFrom(value.getClass());
			ObjectMapper objectMapper = new ObjectMapper();
			String json = objectMapper.writeValueAsString(value);
			
			log.debug(value.getClass().getName() + " being written to Elasticsearch ... "+ json);
			HttpEntity entity = new NStringEntity(json,ContentType.APPLICATION_JSON);
            String path = "/" + index + "/" + indexType + "/" + id;
			indexResponse = restClient
			.performRequest(
			     "PUT",
				 path,
                 Collections.<String, String>emptyMap(), 
                 entity
            );

		} catch (IOException e) {
			log.error("Error while writing to elastic search index ", e);
			return false;
		}

		return indexResponse.getStatusLine().getStatusCode() == 200;
	}
}
