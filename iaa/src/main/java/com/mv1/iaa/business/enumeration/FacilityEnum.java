package com.mv1.iaa.business.enumeration;

public enum FacilityEnum {

	NAME("Name"),
	ADDRESS("Address"),
	CITY("City"),
	ZIP_CODE("ZIP Code"),
	WEEKDAY_HRS("Weekday Hrs"),
	SATURDAY_HRS("Saturday Hrs"),
	SUNDAT_HRS("Sunday Hrs"),
	PHONE("Phone");	

	
	private String value;

	private FacilityEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
