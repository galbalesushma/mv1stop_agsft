package com.mv1.iaa.business.enumeration;

public enum AgentEnum {

	LIC_NUMBER("Lic Number"),
	LICENSE_NUM("License Number"),
	NPN("NPN"),
	LAST_NAME("Last Name"),
	FIRST_NAME("First Name"),
	MIDDLE_NAME("Middle Name"),
	SUFFIX("Suffix"),
	BUSINESS_ENTITY_NAME("Business Entity Name"),
	FEIN("FEIN"),
	LICENSE_TYPE("License Type"),
	LINE_OF_AUTHORITY("Line Of Authority"),
	LICENSE_STATUS("License Status"),
	FIRST_ACTIVE_DATE("First Active Date"),
	EFFECTIVE_DATE("Effective Date"),
	EXPIRATION_DATE("Expiration Date"),
	DOMICILESTATE("Domicial Estate"),
	IS_RESIDENT("Is Resident"),
	MLG_ADDRESS1("MLG Address1"),
	MLG_ADDRESS2("MLG Address2"),
	MLG_ADDRESS3("MLG Address3"),
	MLG_CITY("MLG City"),
	MLG_STATE("MLG State"),
	MLG_ZIP("MLG State"),
	MLG_COUNTRY("MLG Country"),
	BUS_ADDRESS1("Bus Address1"),	
	BUS_ADDRESS2("Bus Address2"),
	BUS_ADDRESS3("Bus Address3"),	
	BUS_CITY("Bus City"),
	BUS_STATE("Bus State"),
	BUS_ZIP("Bus Zip"),	
	BUS_COUNTRY("Bus Country"),
	BUS_PHONE("Bus Phone"),
	BUS_EMAIL("Bus email"),
	CE_COMPLIANCE("CE Compliance"),
	NAICNO("NAICNO");

	
	private String value;

	private AgentEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
