package com.mv1.iaa.business.view.web.req;

import com.opencsv.bean.CsvBindByPosition;

public class SearchLogsVM {

	@CsvBindByPosition(position = 1)
	private String zip;
	@CsvBindByPosition(position = 2)
	private String phone;
	@CsvBindByPosition(position = 3)
	private String searchType;
	@CsvBindByPosition(position = 4)
	private String email;
	@CsvBindByPosition(position = 0)
	private String fullName;
	@CsvBindByPosition(position = 6)
	private String vehicleType;
	@CsvBindByPosition(position = 7)
	private String vehicleYear;
	@CsvBindByPosition(position = 8)
	private String county;
	@CsvBindByPosition(position = 9)
	private String company;
	@CsvBindByPosition(position = 5)
	private String searchedDate;

	public String getZip() {
		return zip;
	}

	public String getPhone() {
		return phone;
	}

	public String getSearchType() {
		return searchType;
	}

	public String getEmail() {
		return email;
	}

	public String getFullName() {
		return fullName;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public String getVehicleYear() {
		return vehicleYear;
	}

	public String getCounty() {
		return county;
	}

	public String getCompany() {
		return company;
	}

	public String getSearchedDate() {
		return searchedDate;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setVehicleType(String vahicleType) {
		this.vehicleType = vahicleType;
	}

	public void setVehicleYear(String vehicleYear) {
		this.vehicleYear = vehicleYear;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setSearchedDate(String searchedDate) {
		this.searchedDate = searchedDate;
	}

	@Override
	public String toString() {
		return "SearchLogsVM [zip=" + zip + ", phone=" + phone + ", searchType=" + searchType + ", email=" + email
				+ ", fullName=" + fullName + ", vehicleType=" + vehicleType + ", vehicleYear=" + vehicleYear
				+ ", county=" + county + ", company=" + company + ", searchedDate=" + searchedDate + "]";
	}

}
