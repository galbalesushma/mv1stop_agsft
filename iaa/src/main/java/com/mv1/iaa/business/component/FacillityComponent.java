package com.mv1.iaa.business.component;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.view.web.req.FacilityIdVM;
import com.mv1.iaa.business.view.web.req.FacilityVM;
import com.mv1.iaa.domain.Address;
import com.mv1.iaa.domain.Business;
import com.mv1.iaa.domain.City;
import com.mv1.iaa.domain.Contact;
import com.mv1.iaa.domain.Facility;
import com.mv1.iaa.repository.BusinessRepository;
import com.mv1.iaa.repository.CustomAddressRepository;
import com.mv1.iaa.repository.CustomCityRepository;
import com.mv1.iaa.repository.CustomContactRepository;
import com.mv1.iaa.repository.CustomFacilityRepository;

@Component
public class FacillityComponent {

    private final Logger log = LoggerFactory.getLogger(FacillityComponent.class);

    @Autowired
    private CustomCityRepository customCityRepository;

    @Autowired
    private CustomFacilityRepository customFacilityRepository;

    @Autowired
    private CustomContactRepository customContactRepository;

    @Autowired
    private CustomAddressRepository customAddressRepository;
    
    @Autowired
    private BusinessRepository businessRepository;
    
    @Autowired
    private SearchObjectWriter<com.mv1.iaa.business.view.search.Facility> indexWriter;

    public FacilityVM upsertFacility(FacilityVM vm) {

        Facility facility = null;
        Address address = null;
        Business business = null;

        if (vm.getId() != null) {
            Optional<Facility> fsOpt = customFacilityRepository.findById(vm.getId());

            if (fsOpt.isPresent()) {
                facility = fsOpt.get();
            }
        }

        if (facility != null && facility.getId() != null) {
            facility.setUpdatedAt(ZonedDateTime.now());
            business = facility.getBusiness();
            
            if( business != null ) {
                address = business.getAddress();
            }

            if (Objects.isNull(address)) {
                address = new Address();
                address.setCreatedAt(ZonedDateTime.now());
            }
        } else {
            address = new Address();
            address.setCreatedAt(ZonedDateTime.now());
            
            business = new Business();
            business.setCreatedAt(ZonedDateTime.now());
            
            facility = new Facility();
            facility.createdAt(ZonedDateTime.now());
        }

        Contact contact = null; 
        if(address.getId() != null) {
            contact = customContactRepository.findOneByAddressId(address.getId());
        }
        
        if (Objects.isNull(contact)) {
            contact = new Contact();
        }

        City city = customCityRepository.findOneByNameIgnoreCase(vm.getCity().trim());
        if (Objects.isNull(city)) {
            city = new City();
            city.setName(vm.getCity().trim());
            city = customCityRepository.save(city);
        }
        
        address.setCity(city);
        address.setZip(vm.getZipCode() != null ? vm.getZipCode().trim() : "");
        address.setUpdatedAt(ZonedDateTime.now());
        address.setAddr1(vm.getAddress().trim());

        address = customAddressRepository.save(address);

        contact.setPhone(vm.getPhone());
        contact.setAddress(address);

        customContactRepository.save(contact);
        
        // Save business
        business.setUpdatedAt(ZonedDateTime.now());
        business.setBusinessName(vm.getBusinessName());
        business.setAddress(address);
        
        business = businessRepository.save(business);

        facility.setSaturdayHours(vm.getSaturdayHours());
        facility.setSundayHours(vm.getSundayHours());
        facility.setWeekdayHours(vm.getWeekDayHours());
        facility.setName(StringUtils.hasText(vm.getName()) ? vm.getName().trim() : facility.getName());
        facility.updatedAt(ZonedDateTime.now());
        facility.setIsActive(true);
        facility.setIsDeleted(false);
        facility.setIsVerified(true);
        facility.setBusiness(business);

        facility = customFacilityRepository.save(facility);
        vm.setId(facility.getId());
        
        // upload the facility to elastic-search
        write2ElasticIndex(vm);
        
        return vm;
    }
    
    private boolean write2ElasticIndex(FacilityVM vm) {
        // upload the facility to elastic-search
        if(vm.getbZip() == null) {
            // set default zip
            vm.setbZip("27502");
        }
        
        com.mv1.iaa.business.view.search.Facility value = CommonMapper.facilitySearchVM(vm);
        boolean isWritten = indexWriter.write2Elasticsearch(value, vm.getId());
        if (!isWritten) {
            log.debug("Failed to write agent in search index ");
        }
        return isWritten;
    }
    
    public FacilityVM getFacility(Long id) {
    	
    	Long fId = Long.parseLong(customFacilityRepository.findFacilityByProfile(id));
//        Object[][] data = customFacilityRepository.getFacility(id);
    	Object[][] data = customFacilityRepository.getFacility(fId);
                
        if(data != null && data.length > 0) {
            try {
            	System.out.println(FacilityVM.getKeys1().toString()+" data[0]==========================="+ data[0].length);
            	System.out.println("CommonMapper.fromKeyVal(FacilityVM.getKeys1(), data[0], FacilityVM.class)-------"+
            	CommonMapper.fromKeyVal(FacilityVM.getKeys1(), data[0], FacilityVM.class));
                return CommonMapper.fromKeyVal(FacilityVM.getKeys1(), data[0], FacilityVM.class);
            } catch (Exception e) {
                log.debug("Error while mapping FacilityVM data", e);
            }
        }
        
        return null;
    }
    
    public List<FacilityVM> getAllFacilities() {
        
        List<FacilityVM> list = new LinkedList<>();
        Object[][] data = customFacilityRepository.getAllFacilities();
                
        if(data != null && data.length > 0) {
        	System.out.println("length------------------------"+data[0].length);
        	
                 for (Object[] arr : data) {
                     try {

                         list.add(CommonMapper.fromKeyVal(FacilityVM.getKeys(), arr, FacilityVM.class));
                     } catch (Exception e) {
                         log.debug("Exception while mapping all agents data ", e);
                     }
                 }
             
            /*for (int i = 0; i < data[0].length; i++) {
              	System.out.println("data------------------------"+data[0][i]);
                try {
                    list.add(CommonMapper.fromKeyVal(FacilityVM.getKeys(), data[i], FacilityVM.class));
                } catch (Exception e) {
                    log.debug("Error while mapping FacilityVM data", e);
                }
            }*/
        }
        
        return list;
    }
    
    @Transactional
    public Integer deleteFacility(FacilityIdVM vm) {
        return customFacilityRepository.deleteFacilitiesByIds(vm.getIds());
    }
}
