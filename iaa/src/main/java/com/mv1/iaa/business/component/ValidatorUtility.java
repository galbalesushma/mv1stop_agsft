package com.mv1.iaa.business.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

@Component
public class ValidatorUtility {

    @Autowired
    @Qualifier("jsr303Validator")
    private Validator validator;
    
    public void validateEntity(final Object object, BindingResult bindingResult) {
        validator.validate(object, bindingResult);
    }
}
