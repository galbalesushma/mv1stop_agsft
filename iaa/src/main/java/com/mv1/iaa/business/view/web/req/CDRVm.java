package com.mv1.iaa.business.view.web.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.opencsv.bean.CsvBindByPosition;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CDRVm {

	public static final String[] keys = new String[] {

			"id",
			"calldate", 
			"src", 
			"dst", 
			"linkedid", 
			"duration", 
			"agentId", 
			"npn",
			"extension", 
			"agentName",
			"agentNumber"
			
	};
	/*
	"id",
			"calldate", 
			"clid", 
			"src", 
			"dst", 
			"dcontext", 
			"channel", 
			"dstchannel", 
			"lastapp",
			"lastdata", 
			"duration", 
			"billsec", 
			"disposition", 
			"amaflags", 
			"accountcode", 
			"uniqueid",
			"userfield", 
			"did", 
			"recordingfile", 
			"cnum", 
			"cnam", 
			"outbound_cnum", 
			"outbound_cnam",
			"dst_cnam" ,
			"linkedid" ,
			"peeraccount" ,
			"sequence"
	 * 
	 * */

	private Long id;

	@CsvBindByPosition(position = 0)
	private String callDate;

	private String clid;

	@CsvBindByPosition(position = 5)
	private String src;

	@CsvBindByPosition(position = 6)
	private String dst;

	private String dcontext;

	private String channel;

	private String dstchannel;

	private String lastapp;

	private String lastdata;

	@CsvBindByPosition(position = 7)
	private String duration;

	private String billsec;

	private String disposition;

	private String amaflags;

	private String accountcode;

	private String uniqueid;

	private String userfield;

	private String did;

	private String recordingfile;

	private String cnum;

	private String cnam;
	
	private String outbound_cnum;

	private String outbound_cnam;

	private String dst_cnam;
	
	@CsvBindByPosition(position = 8)
	private String linkedid;

	private String peeraccount;

	private String sequence;
	
	@CsvBindByPosition(position = 1)
	private String agentName;

	@CsvBindByPosition(position = 2)
	private String npn;

	@CsvBindByPosition(position = 3)
	private String extension;
	
	private String agentId;

	@CsvBindByPosition(position = 4)
	private String agentNumber;
	
	public static String[] getKeys() {
		return keys;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCallDate() {
		return callDate;
	}

	public void setCallDate(String callDate) {
		this.callDate = callDate;
	}

	public String getClid() {
		return clid;
	}

	public void setClid(String clid) {
		this.clid = clid;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getDst() {
		return dst;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}

	public String getDcontext() {
		return dcontext;
	}

	public void setDcontext(String dcontext) {
		this.dcontext = dcontext;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getDstchannel() {
		return dstchannel;
	}

	public void setDstchannel(String dstchannel) {
		this.dstchannel = dstchannel;
	}

	public String getLastapp() {
		return lastapp;
	}

	public void setLastapp(String lastapp) {
		this.lastapp = lastapp;
	}

	public String getLastdata() {
		return lastdata;
	}

	public void setLastdata(String lastdata) {
		this.lastdata = lastdata;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getBillsec() {
		return billsec;
	}

	public void setBillsec(String billsec) {
		this.billsec = billsec;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public String getAmaflags() {
		return amaflags;
	}

	public void setAmaflags(String amaflags) {
		this.amaflags = amaflags;
	}

	public String getAccountcode() {
		return accountcode;
	}

	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}

	public String getUniqueid() {
		return uniqueid;
	}

	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}

	public String getUserfield() {
		return userfield;
	}

	public void setUserfield(String userfield) {
		this.userfield = userfield;
	}

	public String getDid() {
		return did;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public String getRecordingfile() {
		return recordingfile;
	}

	public void setRecordingfile(String recordingfile) {
		this.recordingfile = recordingfile;
	}

	public String getCnum() {
		return cnum;
	}

	public void setCnum(String cnum) {
		this.cnum = cnum;
	}

	public String getCnam() {
		return cnam;
	}

	public void setCnam(String cnam) {
		this.cnam = cnam;
	}

	public String getOutbound_cnum() {
		return outbound_cnum;
	}

	public void setOutbound_cnum(String outbound_cnum) {
		this.outbound_cnum = outbound_cnum;
	}

	public String getOutbound_cnam() {
		return outbound_cnam;
	}

	public void setOutbound_cnam(String outbound_cnam) {
		this.outbound_cnam = outbound_cnam;
	}

	public String getDst_cnam() {
		return dst_cnam;
	}

	public void setDst_cnam(String dst_cnam) {
		this.dst_cnam = dst_cnam;
	}
	

	public String getLinkedid() {
		return linkedid;
	}

	public void setLinkedid(String linkedid) {
		this.linkedid = linkedid;
	}

	public String getPeeraccount() {
		return peeraccount;
	}

	public void setPeeraccount(String peeraccount) {
		this.peeraccount = peeraccount;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getNpn() {
		return npn;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}

	@Override
	public String toString() {
		return "CDRVm [id=" + id + ", callDate=" + callDate + ", clid=" + clid + ", src=" + src + ", dst=" + dst
				+ ", dcontext=" + dcontext + ", channel=" + channel + ", dstchannel=" + dstchannel + ", lastapp="
				+ lastapp + ", lastdata=" + lastdata + ", duration=" + duration + ", billsec=" + billsec
				+ ", disposition=" + disposition + ", amaflags=" + amaflags + ", accountcode=" + accountcode
				+ ", uniqueid=" + uniqueid + ", userfield=" + userfield + ", did=" + did + ", recordingfile="
				+ recordingfile + ", cnum=" + cnum + ", cnam=" + cnam + ", outbound_cnum=" + outbound_cnum
				+ ", outbound_cnam=" + outbound_cnam + ", dst_cnam=" + dst_cnam + "]";
	}

}
