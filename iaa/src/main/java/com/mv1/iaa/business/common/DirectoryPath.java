package com.mv1.iaa.business.common;

public class DirectoryPath {

	String path;
	boolean isProcess;
	String parentDirFiterPath;

	public String getPath() {
		return path;
	}

	public boolean isProcess() {
		return isProcess;
	}

	public String getParentDirFiterPath() {
		return parentDirFiterPath;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setProcess(boolean isProcess) {
		this.isProcess = isProcess;
	}

	public void setParentDirFiterPath(String parentDirFiterPath) {
		this.parentDirFiterPath = parentDirFiterPath;
	}

}