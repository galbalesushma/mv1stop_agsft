package com.mv1.iaa.business.view.web.req;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileVm {

	private String langKey;
	private String lang;
	private String phone;
	private String preferredMobile;
	private String prefCompany;
	private String comments;
	private String mechDuty;
	private String insuranceCompany;
	private List<Long> carriers;
	private String secondPhone;
	private Boolean isSpeaksSpanish;

	

	public String getLangKey() {
		return langKey;
	}

	public String getLang() {
		return lang;
	}

	public String getPhone() {
		return phone;
	}

	public String getPreferredMobile() {
		return preferredMobile;
	}

	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setPreferredMobile(String preferredMobile) {
		this.preferredMobile = preferredMobile;
	}
	
	public String getPrefCompany() {
		return prefCompany;
	}

	public String getComments() {
		return comments;
	}

	public void setPrefCompany(String prefCompany) {
		this.prefCompany = prefCompany;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getMechDuty() {
		return mechDuty;
	}

	public void setMechDuty(String mechDuty) {
		this.mechDuty = mechDuty;
	}
	
	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public List<Long> getCarriers() {
		return carriers;
	}

	public void setCarriers(List<Long> carriers) {
		this.carriers = carriers;
	}

	public String getSecondPhone() {
		return secondPhone;
	}

	public void setSecondPhone(String secondPhone) {
		this.secondPhone = secondPhone;
	}
	
	public Boolean getIsSpeaksSpanish() {
		return isSpeaksSpanish;
	}

	public void setIsSpeaksSpanish(Boolean isSpeaksSpanish) {
		this.isSpeaksSpanish = isSpeaksSpanish;
	}
	
	@Override
	public String toString() {
		return "ProfileVm [langKey=" + langKey + ", lang=" + lang + ", phone=" + phone + ", preferredMobile="
				+ preferredMobile  +", isSpeaksSpanish = " +"]";
	}

}
