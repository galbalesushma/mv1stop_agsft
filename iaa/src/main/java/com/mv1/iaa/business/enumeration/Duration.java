package com.mv1.iaa.business.enumeration;

public enum Duration {

    MONTHLY(1, 1 ,"MONTHLY"), HALF_YEARLY(2, 6 ,"HALF_YEARLY"), YEARLY(3, 12 ,"YEARLY");

    private Duration(int id, int months, String name) {
        this.id = id;
        this.months = months;
        this.name = name;
    }

    private int id;
    private int months;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public int getMonths() {
        return months;
    }

    public static Duration valueOf(int id) {
        for(Duration d : Duration.values()) {
            if(d.id == id) {
                return d;
            }
        }
        return null;
    }

    public static Duration from(String name) {
        for(Duration d : Duration.values()) {
            if(d.name.equals(name)) {
                return d;
            }
        }
        return null;
    }
}
