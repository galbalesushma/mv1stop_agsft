package com.mv1.iaa.business.view.web.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mv1.iaa.service.dto.ContactDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactVM extends ContactDTO {

    private static final long serialVersionUID = 5950322140680939755L;
    
    private static final String[] keys = new String[] {"id", "email"};
    
    public static String[] getKeys() {
        return keys;
    }

    private String desc;

    public String getDesc() {
	return desc;
    }

    public void setDesc(String desc) {
	this.desc = desc;
    }

    @Override
    public String toString() {
	super.toString();
	return "ContactVM [desc=" + desc + "]";
    }

}
