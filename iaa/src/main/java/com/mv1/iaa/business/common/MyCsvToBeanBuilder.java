package com.mv1.iaa.business.common;

import java.io.Reader;
import java.util.Locale;

import com.opencsv.ICSVParser;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;

public class MyCsvToBeanBuilder<T> extends CsvToBeanBuilder<T> {
    
    private char separator = ICSVParser.DEFAULT_SEPARATOR;
    private char quoteChar = ICSVParser.DEFAULT_QUOTE_CHARACTER;
    private char escapeChar = ICSVParser.DEFAULT_ESCAPE_CHARACTER;
    private boolean strictQuotes = ICSVParser.DEFAULT_STRICT_QUOTES;
    private boolean ignoreLeadingWhiteSpace = ICSVParser.DEFAULT_IGNORE_LEADING_WHITESPACE;
    private boolean ignoreQuotations = ICSVParser.DEFAULT_IGNORE_QUOTATIONS;
    private CSVReaderNullFieldIndicator nullFieldIndicator = CSVReaderNullFieldIndicator.NEITHER;
    private Locale errorLocale = Locale.getDefault();

    public MyCsvToBeanBuilder(Reader reader) {
	super(reader);
    }
    
    /**
     * Builds the {@link CsvToBean} out of the provided information.
     * @return A valid {@link CsvToBean}
     * @throws IllegalStateException If a necesary parameter was not specified.
     *   Currently this means that both the mapping strategy and the bean type
     *   are not set, so it is impossible to determine a mapping strategy.
     */
    /*public CsvToBean<T> build() throws IllegalStateException {
        // Check for errors in the configuration first
        if(mappingStrategy == null && type == null) {
            throw new IllegalStateException(ResourceBundle.getBundle(ICSVParser.DEFAULT_BUNDLE_NAME, errorLocale).getString("strategy.type.missing"));
        }
        
        // Build Parser and Reader
        CsvToBean<T> bean = new CsvToBean<>();
        CSVParser parser = buildParser();
        bean.setCsvReader(buildReader(parser));
        
        // Set variables in CsvToBean itself
        bean.setThrowExceptions(throwExceptions);
        bean.setOrderedResults(orderedResults);
        if(filter != null) { bean.setFilter(filter); }
        
        // Now find the mapping strategy.
        if(mappingStrategy == null) {
            mappingStrategy = OpencsvUtils.determineMappingStrategy(type, errorLocale);
        }
        bean.setMappingStrategy(mappingStrategy);

        // The error locale comes at the end so it can be propagated through all
        // of the components of CsvToBean, rendering the error locale homogeneous.
        bean.setErrorLocale(errorLocale);

        return bean;
    }*/
    
    /*public CsvToBean<T> build() {

        return new MyCsvParser(
                separator,
                quoteChar,
                escapeChar,
                strictQuotes,
                ignoreLeadingWhiteSpace,
                ignoreQuotations,
                nullFieldIndicator,
                errorLocale);
    }*/

}
