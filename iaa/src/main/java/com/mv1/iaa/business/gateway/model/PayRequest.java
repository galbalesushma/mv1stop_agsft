package com.mv1.iaa.business.gateway.model;

import com.mv1.iaa.business.gateway.bluepay.BluePay;

public class PayRequest {

    private String orderId;

    private BluePay payment;

    public PayRequest() {
    }

    public String getOrderId() {
	return orderId;
    }

    public void setOrderId(String orderId) {
	this.orderId = orderId;
    }

    public BluePay getPayment() {
	return payment;
    }

    public void setPayment(BluePay payment) {
	this.payment = payment;
    }
}
