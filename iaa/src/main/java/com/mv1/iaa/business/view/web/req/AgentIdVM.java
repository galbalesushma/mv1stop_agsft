package com.mv1.iaa.business.view.web.req;

import java.util.List;

public class AgentIdVM {

    private List<String> agentNpn;

    public List<String> getAgentNpn() {
	return agentNpn;
    }

    public void setAgentNpn(List<String> agentNpn) {
	this.agentNpn = agentNpn;
    }

    @Override
    public String toString() {
	return "AgentIdVM [agentNpn=" + agentNpn + "]";
    }

}
