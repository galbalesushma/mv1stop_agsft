package com.mv1.iaa.business.view.web.req;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.opencsv.bean.CsvBindByPosition;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentReportVM {

	private Long id;

	private String zip;
	@CsvBindByPosition(position = 2)
	private String amount;
	private int slots;
	private Long profileId;
	@CsvBindByPosition(position = 1)
	private String paymentDate;
	@CsvBindByPosition(position = 0)
	private String userType;
	@CsvBindByPosition(position = 3)
	private String agentFirstName;
	@CsvBindByPosition(position = 4)
	private String agentLastName;
	private String agentEmail;
	private String npn;
	private String packageId;
	@CsvBindByPosition(position = 5)
	private String company;
	private Long stateId;
	private Long cityId;
	private Long countyId;

	@CsvBindByPosition(position = 6)
	private String sales_rep;
	private String isn;
	private Long agentId;
	private String incStatus;
	private Long cartId;
	private String cardNo;
	private String cvv;
	private String validTill;
	private Long userId;
	private String state;
	private String city;
	private String county;
	
	private Long addrId;

	public Long getId() {
		return id;
	}

	public String getZip() {
		return zip;
	}

	public String getAmount() {
		return amount;
	}

	public int getSlots() {
		return slots;
	}

	public Long getProfileId() {
		return profileId;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public String getUserType() {
		return userType;
	}

	public String getAgentFirstName() {
		return agentFirstName;
	}

	public String getAgentLastName() {
		return agentLastName;
	}

	public String getAgentEmail() {
		return agentEmail;
	}

	public String getNpn() {
		return npn;
	}

	public String getPackageId() {
		return packageId;
	}

	public String getCompany() {
		return company;
	}

	public Long getStateId() {
		return stateId;
	}

	public Long getCityId() {
		return cityId;
	}

	public Long getCountyId() {
		return countyId;
	}

	public String getSales_rep() {
		return sales_rep;
	}

	public Long getAgentId() {
		return agentId;
	}

	public String getIncStatus() {
		return incStatus;
	}

	public Long getCartId() {
		return cartId;
	}

	public String getCardNo() {
		return cardNo;
	}

	public String getCvv() {
		return cvv;
	}

	public String getValidTill() {
		return validTill;
	}

	public Long getUserId() {
		return userId;
	}

	public String getIsn() {
		return isn;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setSlots(int slots) {
		this.slots = slots;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public void setAgentFirstName(String agentFirstName) {
		this.agentFirstName = agentFirstName;
	}

	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}

	public void setAgentEmail(String agentEmail) {
		this.agentEmail = agentEmail;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public void setCountyId(Long countyId) {
		this.countyId = countyId;
	}

	public void setSales_rep(String sales_rep) {
		this.sales_rep = sales_rep;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public void setIncStatus(String incStatus) {
		this.incStatus = incStatus;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public void setValidTill(String validTill) {
		this.validTill = validTill;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setIsn(String isn) {
		this.isn = isn;
	}
	
	public String getState() {
		return state;
	}

	public String getCity() {
		return city;
	}

	public String getCounty() {
		return county;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public Long getAddrId() {
		return addrId;
	}

	public void setAddrId(Long addrId) {
		this.addrId = addrId;
	}

	@Override
	public String toString() {
		return "PaymentReportVM [id=" + id + ", zip=" + zip + ", amount=" + amount + ", slots=" + slots + ", profileId="
				+ profileId + ", paymentDate=" + paymentDate + ", userType=" + userType + ", agentFirstName="
				+ agentFirstName + ", agentLastName=" + agentLastName + ", agentEmail=" + agentEmail + ", npn=" + npn
				+ ", packageId=" + packageId + ", company=" + company + ", stateId=" + stateId + ", cityId=" + cityId
				+ ", countyId=" + countyId + ", sales_rep=" + sales_rep + ", agentId=" + agentId + ", incStatus="
				+ incStatus + ", cartId=" + cartId + ", cardNo=" + cardNo + ", cvv=" + cvv + ", validTill=" + validTill
				+ ", userId=" + userId + ", isn=" + isn + "]";
	}

}
