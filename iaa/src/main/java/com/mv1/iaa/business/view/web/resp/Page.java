package com.mv1.iaa.business.view.web.resp;

import java.util.List;

public class Page<T> {

    private Integer total;
    private List<T> result;
    private Integer totalPages;

    public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Integer getTotal() {
	return total;
    }

    public void setTotal(Integer total) {
	this.total = total;
    }

    public List<T> getResult() {
	return result;
    }

    public void setResult(List<T> result) {
	this.result = result;
    }

    @Override
    public String toString() {
	return "Page [total=" + total + ", result=" + result + ",totalPages="+ totalPages + "]";
    }

}
