package com.mv1.iaa.business.view.web.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AvailableItemVM {

    private static final String[] keys = new String[] {"rank", "slot"};

    public static String[] getKeys() {
        return keys;
    }
    
    private String rank;
    private Integer slot;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Integer getSlot() {
        return slot;
    }

    public void setSlot(Integer slot) {
        this.slot = slot;
    }

    @Override
    public String toString() {
        return "AvailableItemVM [rank=" + rank + ", slot=" + slot + "]";
    }

}
