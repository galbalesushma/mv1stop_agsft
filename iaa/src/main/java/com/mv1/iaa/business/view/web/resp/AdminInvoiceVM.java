package com.mv1.iaa.business.view.web.resp;

public class AdminInvoiceVM {

	private Long id;
	private String agentName;
	private String agentNumber;
	private int noOfCalls;
	private String month;
	private int baseCharge;
	private int totalCharge;
	private String npn;
	private boolean agentSignedUp;
	private String linkedId;
	
	public String getLinkedId() {
		return linkedId;
	}

	public void setLinkedId(String linkedId) {
		this.linkedId = linkedId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}

	public int getNoOfCalls() {
		return noOfCalls;
	}

	public void setNoOfCalls(int noOfCalls) {
		this.noOfCalls = noOfCalls;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getBaseCharge() {
		return baseCharge;
	}

	public void setBaseCharge(int baseCharge) {
		this.baseCharge = baseCharge;
	}

	public int getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(int totalCharge) {
		this.totalCharge = totalCharge;
	}

	public String getNpn() {
		return npn;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}

	public boolean isAgentSignedUp() {
		return agentSignedUp;
	}

	public void setAgentSignedUp(boolean agentSignedUp) {
		this.agentSignedUp = agentSignedUp;
	}
	
	

}
