package com.mv1.iaa.business.view.csv;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.opencsv.bean.CsvBindByName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FacilityCsvBean {

    @CsvBindByName(column = "BUS_NAME")
    private String name;

    @CsvBindByName(column = "BUS_ADDRESS")
    private String address;

    @CsvBindByName(column = "CITY")
    private String city;

    @CsvBindByName(column = "ZIP")
    private String zipCode;

    @CsvBindByName(column = "WEEK_HRS")
    private String weekDayHours;

    @CsvBindByName(column = "SAT_HRS")
    private String saturdayHours;

    @CsvBindByName(column = "SUN_HRS")
    private String sundayHours;

    @CsvBindByName(column = "BUS_PHONE")
    private String bPhone;
    
    @CsvBindByName(column = "MOB_PHONE")
    private String phone;
    
    @CsvBindByName(column = "BUS_EMAIL")
    private String bEmail;
    
    @CsvBindByName(column = "ISN")
    private String isn;
    
    @CsvBindByName(column = "STATE")
    private String bState;

    @CsvBindByName(column = "COUNTY")
    private String bCounty;
    
    @CsvBindByName(column = "INSP_TYPE")
    private String inspType;

    @CsvBindByName(column = "MECH_DUTY")
    private String mechDuty;
    
    @CsvBindByName(column = "EXTENSION")
    private String extension;
    
    private String businessName;

    private String addr1;

    private String addr2;

    private String addr3;

    private String bCity;

    private String bZip;

    private String type;

    private String contactName;

    private String cPhone;

    private String refBy;

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name != null ? name.trim() : name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address != null ? address.trim() : address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city != null ? city.trim() : city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode != null ? zipCode.trim() : zipCode;
    }

    public String getWeekDayHours() {
        return weekDayHours;
    }

    public void setWeekDayHours(String weekDayHours) {
        this.weekDayHours = weekDayHours != null ? weekDayHours.trim() : weekDayHours;
    }

    public String getSaturdayHours() {
        return saturdayHours;
    }

    public void setSaturdayHours(String saturdayHours) {
        this.saturdayHours = saturdayHours != null ? saturdayHours.trim() : saturdayHours;
    }

    public String getSundayHours() {
        return sundayHours;
    }

    public void setSundayHours(String sundayHours) {
        this.sundayHours = sundayHours != null ? sundayHours.trim() : sundayHours;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone != null ? phone.trim() : phone;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getAddr3() {
        return addr3;
    }

    public void setAddr3(String addr3) {
        this.addr3 = addr3;
    }

    public String getbCity() {
        return bCity;
    }

    public void setbCity(String bCity) {
        this.bCity = bCity;
    }

    public String getbState() {
        return bState;
    }

    public void setbState(String bState) {
        this.bState = bState;
    }

    public String getbZip() {
        return bZip;
    }

    public void setbZip(String bZip) {
        this.bZip = bZip;
    }

    public String getbPhone() {
        return bPhone;
    }

    public void setbPhone(String bPhone) {
        this.bPhone = bPhone;
    }

    public String getbEmail() {
        return bEmail;
    }

    public void setbEmail(String bEmail) {
        this.bEmail = bEmail;
    }

    public String getbCounty() {
        return bCounty;
    }

    public void setbCounty(String bCounty) {
        this.bCounty = bCounty;
    }

    public String getIsn() {
        return isn;
    }

    public void setIsn(String isn) {
        this.isn = isn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getcPhone() {
        return cPhone;
    }

    public void setcPhone(String cPhone) {
        this.cPhone = cPhone;
    }

    public String getRefBy() {
        return refBy;
    }

    public void setRefBy(String refBy) {
        this.refBy = refBy;
    }

    public String getInspType() {
		return inspType;
	}

	public String getMechDuty() {
		return mechDuty;
	}

	public void setInspType(String inspType) {
		this.inspType = inspType;
	}

	public void setMechDuty(String mechDuty) {
		this.mechDuty = mechDuty;
	}
	
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((isn == null) ? 0 : isn.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FacilityCsvBean other = (FacilityCsvBean) obj;
        if (isn == null) {
            if (other.isn != null)
                return false;
        } else if (!isn.equals(other.isn))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FacilityCsvBean [name=" + name + ", address=" + address + ", city=" + city + ", zipCode=" + zipCode
                + ", weekDayHours=" + weekDayHours + ", saturdayHours=" + saturdayHours + ", sundayHours=" + sundayHours
                + ", phone=" + phone + ", businessName=" + businessName + ", addr1=" + addr1 + ", addr2=" + addr2
                + ", addr3=" + addr3 + ", bCity=" + bCity + ", bState=" + bState + ", bZip=" + bZip + ", bPhone="
                + bPhone + ", bEmail=" + bEmail + ", bCounty=" + bCounty + ", isn=" + isn + ", type=" + type
                + ", contactName=" + contactName + ", cPhone=" + cPhone + ", refBy=" + refBy + "]";
    }

}
