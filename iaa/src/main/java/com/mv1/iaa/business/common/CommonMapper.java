package com.mv1.iaa.business.common;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Random;
import java.util.UUID;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mv1.iaa.business.enumeration.Duration;
import com.mv1.iaa.business.view.csv.FacilityCsvBean;
import com.mv1.iaa.business.view.search.Agent;
import com.mv1.iaa.business.view.search.Facility;
import com.mv1.iaa.business.view.web.req.AgentVM;
import com.mv1.iaa.business.view.web.req.FacilityVM;
import com.mv1.iaa.business.view.web.req.Item;
import com.mv1.iaa.business.view.web.req.PaymentVM;
import com.mv1.iaa.domain.enumeration.PayStatus;
import com.mv1.iaa.domain.enumeration.ProfileType;
import com.mv1.iaa.service.dto.AddressDTO;
import com.mv1.iaa.service.dto.AgentDTO;
import com.mv1.iaa.service.dto.AgentLicDTO;
import com.mv1.iaa.service.dto.BusinessDTO;
import com.mv1.iaa.service.dto.ContactDTO;
import com.mv1.iaa.service.dto.PackSubscriptionDTO;
import com.mv1.iaa.service.dto.PaymentDTO;
import com.mv1.iaa.service.dto.ProducerDTO;
import com.mv1.iaa.service.dto.ProfileDTO;

public class CommonMapper {

    private static final Logger log = LoggerFactory.getLogger(CommonMapper.class);

    private static SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");

    private static ObjectMapper mapper = new ObjectMapper();

    public static PaymentDTO fromPaymentVM(PaymentVM p, ProfileDTO pdto) {

        PaymentDTO d = new PaymentDTO();

        d.setAmount(Long.valueOf(p.getAmount()));
        d.setCartId(p.getCartId());
        d.setCreatedAt(ZonedDateTime.now());
        d.setProfileId(pdto.getId());
        if(p.getTxnType().trim().equalsIgnoreCase("ACH"))
        {
        	
        	d.setRoutingNumber(p.getRoutingNumber());
        	d.setAccountNumber(p.getAccountNumber());
        	d.setAccountType(p.getAccountType());
            d.setTxnType(p.getTxnType());
        }
       
        if(p.getTxnType().trim().equalsIgnoreCase("Paper-cheque"))
        {
        	d.setStatus(PayStatus.SUCCESS);
        	d.setChequeNo(p.getChequeNo());
            d.setTxnType(p.getTxnType());
        }
        else
        {
        	 d.setStatus(PayStatus.INITIATED);
        }
       
        	
        
        d.setUuId(UUID.randomUUID().toString());
        d.setUpdatedAt(ZonedDateTime.now());
        
        return d;
    }

    public static PackSubscriptionDTO fromItem(Item i, Long profileId) {

        PackSubscriptionDTO d = new PackSubscriptionDTO();
        Duration duration = Duration.from(i.getDuration());
        
        d.setCreatedAt(ZonedDateTime.now());
        d.setPackageId(i.getPackageId());
        d.setProfileId(profileId);
        d.setUpdatedAt(ZonedDateTime.now());
        d.setValidTill(ZonedDateTime.now().plusMonths(duration.getMonths()));
        d.setZipAreaId(i.getZipAreaId());
        d.setSlots(i.getSlots());
        d.setDuration(i.getDuration());
        d.setId(i.getSubsId());
        d.setIsDeleted(false);
        d.setIsActive(false);
        
        return d;
    }

    public static <T> T fromKeyVal(String[] keys, Object[] data, Class<T> clazz) throws Exception {
        JSONObject json = new JSONObject();
        for (int i = 0; i < data.length; i++) {
        	Object val = data[i];
			if (val instanceof BigInteger) {
				json.put(keys[i], ((BigInteger) val).longValue());
			} else if ((val) instanceof Boolean) {
				json.put(keys[i], ((Boolean) val));
			} else if ((val) instanceof Timestamp) {
				json.put(keys[i], ((Timestamp) val).toString());
			} else {
				json.put(keys[i], val);
			}
        }
        // log.debug("data from DB before mapping {}", json.toString());
        return mapper.readValue(json.toString(), clazz);
    }

    public static void main(String... args) {
        String[] keys = new String[] { "id", "name", "date" };
        Object[] vals = new Object[] { new Long(1), "Test", new Timestamp(System.currentTimeMillis()) };

        try {
            Test t = fromKeyVal(keys, vals, Test.class);
            System.out.println("Created test object : " + t.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class Test implements Serializable {
        Long id;
        String name;
        String date;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "Test [id=" + id + ", name=" + name + ", date=" + date + "]";
        }
    }
    
    public static AddressDTO addressDTOfromPayment(PaymentVM payment, Long cityId, Long stateId) {

        AddressDTO dto = new AddressDTO();

        dto.setAddr1(payment.getAddr());
        dto.setCityId(cityId);
        dto.setCountry(payment.getCountry());
        dto.setCreatedAt(ZonedDateTime.now());
        dto.setStateId(stateId);
        dto.setAddr2(payment.getStreet());
        dto.setUpdatedAt(ZonedDateTime.now());
        dto.setZip(payment.getZip());

        return dto;
    }

    public static ContactDTO contactDTOfromPayment(PaymentVM payment, Long addressId) {

        ContactDTO dto = new ContactDTO();

        dto.setEmail(payment.getEmail());
        dto.setPhone(payment.getPhone());
        dto.setAddressId(addressId);

        return dto;
    }

    public static ProfileDTO profileDTOfrom(PaymentVM p, Long addressId) {

        ProfileDTO d = new ProfileDTO();

        d.setAddrId(addressId);
        d.setType(ProfileType.valueOf(p.getUserType()));
        d.setFirstName(p.getFirstName());
        d.setLastName(p.getLastName());
        d.setIsActive(false);
        d.setIsDeleted(false);
        d.setLastName(p.getLastName());
        d.setUserId(p.getUserId());
        d.setCreatedAt(ZonedDateTime.now());
        d.setUpdatedAt(ZonedDateTime.now());

        return d;
    }
    
    public static AddressDTO producerAddressFromVM(AgentVM v) {

        AddressDTO a = new AddressDTO();

        a.setAddr1(v.getpAddr1());
        a.setAddr2(v.getpAddr2());
        a.setAddr3(v.getpAddr3());
        a.setCityId(v.getpCity());
        a.setCountry(v.getpCountry());
        a.setCountyId(v.getpCounty());
        a.setCreatedAt(ZonedDateTime.now());
        a.setStateId(v.getpState());
        a.setUpdatedAt(ZonedDateTime.now());
        a.setZip(v.getpZip());

        return a;
    }

    public static AddressDTO businessAddressFromVM(AgentVM v) {

        AddressDTO a = new AddressDTO();

        a.setAddr1(v.getbAddr1());
        a.setAddr2(v.getbAddr2());
        a.setAddr3(v.getbAddr3());
        a.setCityId(v.getbCity());
        a.setCountry(v.getbCountry());
        a.setCountyId(v.getbCounty());
        a.setCreatedAt(ZonedDateTime.now());
        a.setStateId(v.getbState());
        a.setUpdatedAt(ZonedDateTime.now());
        a.setZip(v.getbZip());

        return a;
    }

    public static ContactDTO businessContactFromVM(AgentVM v, Long addressId) {

        ContactDTO c = new ContactDTO();

        c.setAddressId(addressId);
        c.setEmail(v.getbEmail());
        c.setOfficePhone(v.getbPhone());
        c.setPhone(v.getbMobile());

        return c;

    }

    public static BusinessDTO businessDtoFromVM(AgentVM v, Long addressId) {

        BusinessDTO b = new BusinessDTO();

        b.setAddressId(addressId);
        b.setBusinessName(v.getBusinessName());
        b.setIsActive(true);
        b.setIsDeleted(false);
        b.setIsVerified(true);
        b.setUpdatedAt(ZonedDateTime.now());

        return b;
    }

    public static ProfileDTO profileDtoFromVM(AgentVM v, Long addressId) {

        ProfileDTO p = new ProfileDTO();

        p.setAddrId(addressId);
        p.setCreatedAt(ZonedDateTime.now());
        p.setFirstName(v.getFirstName());
        p.setMiddleName(v.getMiddleName());
        p.setLastName(v.getLastName());
        p.setIsActive(false);
        p.setIsDeleted(false);
        p.setIsSpeaksSpanish(v.getSpeaksSpanish());
        p.setSuffix(v.getSuffix());
        p.setType(ProfileType.AGENT);
        p.setUpdatedAt(ZonedDateTime.now());

        return p;
    }

    public static AgentDTO agentDtoFromVM(AgentVM v, Long businessId, Long agentLicId, Long profileId) {

        AgentDTO a = new AgentDTO();

        a.setBusinessId(businessId);
        a.setCreatedAt(ZonedDateTime.now());
        a.setDomicileStateId(v.getDomState());
        a.setFein(v.getFein());
        a.setIsActive(true);
        a.setIsDeleted(false);
        a.setIsVerified(true);
        a.setLicId(agentLicId);
        a.setNpn(v.getNpn());
        a.setNaicno(v.getNaicno());
        a.setProfileId(profileId);
        a.setResidentState(v.getResState());
        a.setUpdatedAt(ZonedDateTime.now());

        return a;
    }
    
    public static Agent agentSearchVM(AgentVM v, String city, Long agentId, String state) {

        Agent a = new Agent();
        
        a.setNpn(v.getNpn());
        a.setAddress(v.getpAddr1());
        a.setCity(city);
        a.setFirst_name(v.getFirstName());
        a.setId(agentId);
        a.setLast_name(v.getLastName());
        a.setState(state);
        a.setValid_till(v.getExpDate());
        a.setZip(v.getbZip());

        return a;
    }
    
    public static Facility facilitySearchVM(FacilityVM v) {

        Facility a = new Facility();
        
        a.setIsn(v.getIsn());
        a.setName(v.getName());
        a.setAddress(v.getAddr1());
        a.setCity(v.getbCity());
        a.setId(v.getId());
        a.setState(v.getbState());
        a.setZip(v.getbZip());

        return a;
    }
    
    public static Facility facilitySearchVM(FacilityCsvBean v, Long facilityId) {

        Facility a = new Facility();
        
        a.setIsn(v.getIsn());
        a.setName(v.getName());
        a.setAddress(v.getAddr1());
        a.setCity(v.getbCity());
        a.setId(facilityId);
        a.setState(v.getbState());
        a.setZip(v.getbZip());

        return a;
    }

    public static ProducerDTO producerFromVM(AgentVM v, Long addressId) {

        ProducerDTO p = new ProducerDTO();

        p.setAddrId(addressId);
        p.setCeCompliance(v.getIsCECompliant());
        p.setName("Producer"); // TODO confirm if producer name will be part of csv/enrollment

        return p;
    }

    public static AgentLicDTO agentLicFromVM(AgentVM v, Long producerId) {

        AgentLicDTO a = new AgentLicDTO();

        a.setAuthority(v.getLineAuth());
        a.setCreatedAt(ZonedDateTime.now());
        a.setEffectiveDate(ZonedDateTime.parse(v.getEffectiveDate()));
        a.setExpirationDate(ZonedDateTime.parse(v.getExpDate()));
        a.setFirstActiveDate(ZonedDateTime.parse(v.getFirstActiveDate()));
        a.setIsActive(true);
        a.setIsDeleted(false);
        a.setLicNumber(v.getLicNumber());
        a.setProducerId(producerId);
        a.setType(v.getLicType());
        a.setUpdatedAt(ZonedDateTime.now());

        return a;
    }
}
