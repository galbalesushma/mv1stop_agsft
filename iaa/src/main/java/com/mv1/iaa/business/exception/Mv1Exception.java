package com.mv1.iaa.business.exception;

import java.util.List;

import org.springframework.stereotype.Component;
@Component
public class Mv1Exception extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6231568802011859857L;

	public List<com.mv1.iaa.business.view.Error> errorList ;

	public Mv1Exception() {
	}

	public Mv1Exception(String message) {
		super(message);
	}

	public Mv1Exception(List<com.mv1.iaa.business.view.Error> erros) {
//		super(""+erros);
		this.errorList = erros;
	}
	
	public Mv1Exception(String message, Throwable cause) {
		super(message, cause);
	}

	public List<com.mv1.iaa.business.view.Error> getErrorList() {
		return errorList;
	}


	

	
}
