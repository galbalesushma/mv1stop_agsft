package com.mv1.iaa.business.exception;

public class UpdateException extends Exception {

	private static final long serialVersionUID = 2547424453943142465L;

	public UpdateException() {
	}

	public UpdateException(String message) {
		super(message);
	}

	public UpdateException(String message, Throwable cause) {
		super(message, cause);
	}

}
