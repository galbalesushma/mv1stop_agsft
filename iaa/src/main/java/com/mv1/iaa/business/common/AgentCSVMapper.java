package com.mv1.iaa.business.common;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import com.mv1.iaa.business.view.csv.AgentCsvBean;
import com.mv1.iaa.business.view.csv.FacilityCsvBean;
import com.mv1.iaa.business.view.search.Agent;
import com.mv1.iaa.domain.enumeration.ProfileType;
import com.mv1.iaa.service.dto.AddressDTO;
import com.mv1.iaa.service.dto.AgentDTO;
import com.mv1.iaa.service.dto.AgentLicDTO;
import com.mv1.iaa.service.dto.BusinessDTO;
import com.mv1.iaa.service.dto.ContactDTO;
import com.mv1.iaa.service.dto.ProducerDTO;
import com.mv1.iaa.service.dto.ProfileDTO;

public class AgentCSVMapper {

	private static SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

	public static AddressDTO producerAddressFromVM(AgentCsvBean v, Long cityId, Long stateId, Long countyId) {

		AddressDTO a = new AddressDTO();

		a.setAddr1(v.getpAddr1());
		a.setAddr2(v.getpAddr2());
		a.setAddr3(v.getpAddr3());
		a.setCityId(cityId);
		a.setCountry(v.getpCountry());
		a.setCreatedAt(ZonedDateTime.now());
		a.setStateId(stateId);
		a.setUpdatedAt(ZonedDateTime.now());
		a.setZip(v.getpZip());
		a.setCountyId(countyId);

		return a;
	}

	public static AddressDTO businessAddressFromVM(AgentCsvBean v, Long cityId, Long stateId, Long countyId) {

		AddressDTO a = new AddressDTO();

		a.setAddr1(v.getbAddr1());
		a.setAddr2(v.getbAddr2());
		a.setAddr3(v.getbAddr3());
		a.setCityId(cityId);
		a.setCountry(v.getbCountry());
		a.setCreatedAt(ZonedDateTime.now());
		a.setStateId(stateId);
		a.setUpdatedAt(ZonedDateTime.now());
		a.setZip(v.getbZip());
		a.setCountyId(countyId);

		return a;
	}

	public static ContactDTO businessContactFromVM(AgentCsvBean v, Long addressId) {

		ContactDTO c = new ContactDTO();

		c.setAddressId(addressId);
		c.setEmail(v.getbEmail());
		c.setOfficePhone(v.getbPhone());
		c.setPhone(v.getbMobile());

		return c;

	}

	public static BusinessDTO businessDtoFromVM(AgentCsvBean v, Long addressId) {

		BusinessDTO b = new BusinessDTO();

		b.setAddressId(addressId);
		b.setBusinessName(v.getBusinessName());
		b.setIsActive(true);
		b.setIsDeleted(false);
		b.setIsVerified(true);
		b.setUpdatedAt(ZonedDateTime.now());

		return b;
	}

	public static ProfileDTO profileDtoFromVM(AgentCsvBean v, Long addressId) {

		ProfileDTO p = new ProfileDTO();

		p.setPrefEmail(v.getbEmail());
		p.setAddrId(addressId);
		p.setCreatedAt(ZonedDateTime.now());
		p.setFirstName(v.getFirstName());
		p.setMiddleName(v.getMiddleName());
		p.setLastName(v.getLastName());
		p.setIsActive(true);
		p.setIsDeleted(false);
		p.setSuffix(v.getSuffix());
		p.setType(ProfileType.AGENT);
		p.setUpdatedAt(ZonedDateTime.now());
		p.setInsuranceCompany(v.getInsuranceCompany());
		
		return p;
	}

	public static AgentDTO agentDtoFromVM(AgentCsvBean v, Long businessId, Long agentLicId, Long profileId,
			Long stateId) {

		AgentDTO a = new AgentDTO();

		a.setBusinessId(businessId);
		a.setCreatedAt(ZonedDateTime.now());
		a.setDomicileStateId(stateId);
		a.setFein(v.getFein());
		a.setIsActive(true);
		a.setIsDeleted(false);
		a.setIsVerified(true);
		a.setLicId(agentLicId);
		a.setNpn(v.getNpn());
		a.setNaicno(v.getNaicno());
		a.setProfileId(profileId);
		a.setResidentState(v.getResState() != null && v.getResState().equalsIgnoreCase("y"));
		a.setUpdatedAt(ZonedDateTime.now());
		/* if (v.getExtension() != null)
			a.setExtension(v.getExtension());*/

		return a;
	}

	public static Agent agentSearchVM(AgentCsvBean v, String city, Long agentId, String state) {

		Agent a = new Agent();

		a.setNpn(v.getNpn());
		a.setAddress(v.getpAddr1());
		a.setCity(city);
		a.setFirst_name(v.getFirstName());
		a.setId(agentId);
		a.setLast_name(v.getLastName());
		a.setState(state);
		if (v.getExpDateNew() != null) {
			a.setValid_till(formatter.format(v.getExpDateNew()));
		}
		a.setZip(v.getbZip());

		return a;
	}

	public static ProducerDTO producerFromVM(AgentCsvBean v, Long addressId) {

		ProducerDTO p = new ProducerDTO();

		p.setAddrId(addressId);
		p.setCeCompliance(v.getIsCECompliant() != null && v.getIsCECompliant().equalsIgnoreCase("y"));
		p.setName("Producer"); // TODO confirm if producer name will be part of
								// csv/enrollment
       
		return p;
	}

	public static AgentLicDTO agentLicFromVM(AgentCsvBean v, Long producerId) {

		AgentLicDTO a = new AgentLicDTO();

		a.setAuthority(v.getLineAuth());
		a.setCreatedAt(ZonedDateTime.now());
		if (v.getEffectiveDateNew() != null) {
			a.setEffectiveDate(v.getEffectiveDateNew().toInstant().atZone(ZoneId.systemDefault()));
		}
		if (v.getExpDateNew() != null) {
			a.setExpirationDate(v.getExpDateNew().toInstant().atZone(ZoneId.systemDefault()));
		}
		if (v.getFirstActiveDateNew() != null) {
			a.setFirstActiveDate(v.getFirstActiveDateNew().toInstant().atZone(ZoneId.systemDefault()));
		}
		a.setIsActive(true);
		a.setIsDeleted(false);
		a.setLicNumber(v.getLicNumber());
		a.setProducerId(producerId);
		a.setType(v.getLicType());
		a.setUpdatedAt(ZonedDateTime.now());

		return a;
	}

	public static ProfileDTO profileDtoFromFacilityVM(FacilityCsvBean v, Long addId) {

		ProfileDTO p = new ProfileDTO();
		p.setFirstName("");
		p.setPrefEmail(v.getbEmail());
		p.setAddrId(addId);
		p.setCreatedAt(ZonedDateTime.now());
		p.setIsActive(true);
		p.setIsDeleted(false);
		p.setType(ProfileType.FACILITY);
		p.setUpdatedAt(ZonedDateTime.now());
        p.setInsuranceCompany("");
        p.setPhone(v.getbPhone());
		return p;
	}

}
