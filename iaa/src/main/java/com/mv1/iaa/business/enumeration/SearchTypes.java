package com.mv1.iaa.business.enumeration;

import com.mv1.iaa.business.view.search.Agent;
import com.mv1.iaa.business.view.search.Facility;

public enum SearchTypes {

	AGENT(Agent.class, "agents", "agent"),
	FACILITY(Facility.class, "facilities", "facility"),
	;
	
	@SuppressWarnings("rawtypes")
    private Class clazz;
	private String index;
	private String type;
	
	private SearchTypes(@SuppressWarnings("rawtypes") Class clazz, String index, String type) {
		this.clazz = clazz;
		this.index = index;
		this.type = type;
	}

	@SuppressWarnings("rawtypes")
    public Class getClazz() {
		return clazz;
	}

	public String getIndex() {
		return index;
	}
	
	public String getType() {
        return type;
    }

    @SuppressWarnings("rawtypes")
    public static Class classFrom(String index) {
		for(SearchTypes t : SearchTypes.values()) {
			if(t.index.equals(index)) {
				return t.clazz;
			}
		}
		return null;
	}
	
    public static SearchTypes fromUserType(String userType) {
        for(SearchTypes t : SearchTypes.values()) {
            if(t.name().equals(userType.toUpperCase())) {
                return t;
            }
        }
        return null;
    }
	
    @SuppressWarnings("rawtypes")
    public static String idxFrom(Class clazz) {
        for(SearchTypes t : SearchTypes.values()) {
            if(t.clazz.equals(clazz)) {
                return t.index;
            }
        }
        return null;
    }
    
    @SuppressWarnings("rawtypes")
    public static String typeFrom(Class clazz) {
        for(SearchTypes t : SearchTypes.values()) {
            if(t.clazz.equals(clazz)) {
                return t.type;
            }
        }
        return null;
    }
}
