package com.mv1.iaa.business.view.search;

public class Holder <T> {

    private int total;
    private T type;

    public int getTotal() {
        return this.total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public T getType() {
        return this.type;
    }

    public void setType(T type) {
        this.type = type;
    }

    public Holder total(int total) {
        this.total = total;
        return this;
    }

    public Holder type(T type) {
        this.type = type;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " total='" + getTotal() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}