package com.mv1.iaa.business.view.web.resp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PayResponseView {

    private Long profileId;
    private String status;
    private String message;
    private Long cartId;

    public Long getProfileId() {
	return profileId;
    }

    public void setProfileId(Long profileId) {
	this.profileId = profileId;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }
    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    @Override
    public String toString() {
	return "PayResponseView [profileId=" + profileId + ", status=" + status + ", message=" + message + ", cartId="
		+ cartId + "]";
    }

}
