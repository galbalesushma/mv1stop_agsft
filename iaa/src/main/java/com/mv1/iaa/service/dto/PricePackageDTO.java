package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

import com.mv1.iaa.domain.enumeration.PageRank;

/**
 * A DTO for the PricePackage entity.
 */
public class PricePackageDTO implements Serializable {

    private Long id;

    private String name;

    private PageRank rank;

    private Long price;

    private String desc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PageRank getRank() {
        return rank;
    }

    public void setRank(PageRank rank) {
        this.rank = rank;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PricePackageDTO pricePackageDTO = (PricePackageDTO) o;
        if (pricePackageDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pricePackageDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PricePackageDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", rank='" + getRank() + "'" +
            ", price=" + getPrice() +
            ", desc='" + getDesc() + "'" +
            "}";
    }
}
