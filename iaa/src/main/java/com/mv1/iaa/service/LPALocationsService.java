package com.mv1.iaa.service;

import com.mv1.iaa.service.dto.LPALocationsDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing LPALocations.
 */
public interface LPALocationsService {

    /**
     * Save a lPALocations.
     *
     * @param lPALocationsDTO the entity to save
     * @return the persisted entity
     */
    LPALocationsDTO save(LPALocationsDTO lPALocationsDTO);

    /**
     * Get all the lPALocations.
     *
     * @return the list of entities
     */
    List<LPALocationsDTO> findAll();


    /**
     * Get the "id" lPALocations.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<LPALocationsDTO> findOne(Long id);

    /**
     * Delete the "id" lPALocations.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    
}
