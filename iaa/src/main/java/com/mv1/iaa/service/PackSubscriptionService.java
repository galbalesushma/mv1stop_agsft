package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import com.mv1.iaa.service.dto.PackSubscriptionCustomDTO;
import com.mv1.iaa.service.dto.PackSubscriptionDTO;

/**
 * Service Interface for managing PackSubscription.
 */
public interface PackSubscriptionService {

    /**
     * Save a packSubscription.
     *
     * @param packSubscriptionDTO the entity to save
     * @return the persisted entity
     */
    PackSubscriptionDTO save(PackSubscriptionDTO packSubscriptionDTO);

    /**
     * Get all the packSubscriptions.
     *
     * @return the list of entities
     */
    List<PackSubscriptionDTO> findAll();


    /**
     * Get the "id" packSubscription.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PackSubscriptionDTO> findOne(Long id);

    /**
     * Delete the "id" packSubscription.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    

    /**
     * Get the "profileId" packSubscription.
     *
     * @param profileId the profileId of the entity
     * @return the entity
     */
    List<PackSubscriptionCustomDTO> findByProfileId(Long profileId);
}
