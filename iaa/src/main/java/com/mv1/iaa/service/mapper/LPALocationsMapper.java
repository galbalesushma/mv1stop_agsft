package com.mv1.iaa.service.mapper;

import com.mv1.iaa.domain.*;
import com.mv1.iaa.service.dto.LPALocationsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LPALocations and its DTO LPALocationsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LPALocationsMapper extends EntityMapper<LPALocationsDTO, LPALocations> {



    default LPALocations fromId(Long id) {
        if (id == null) {
            return null;
        }
        LPALocations lPALocations = new LPALocations();
        lPALocations.setId(id);
        return lPALocations;
    }
}
