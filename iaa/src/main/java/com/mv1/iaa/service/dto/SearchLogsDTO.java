package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the SearchLogs entity.
 */
public class SearchLogsDTO implements Serializable {

    private Long id;

    @NotNull
    private String fullName;

    @NotNull
    private String email;

    @NotNull
    private String phone;

    @NotNull
    private String zip;

    private String searchType;

    private ZonedDateTime searchedDate;
    
    private String vehicleType;
    
    private String vehicleYear;
    
    private String county;
    
    private String company;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public ZonedDateTime getSearchedDate() {
        return searchedDate;
    }

    public void setSearchedDate(ZonedDateTime searchedDate) {
        this.searchedDate = searchedDate;
    }
     

    public String getVehicleType() {
		return vehicleType;
	}

	public String getVehicleYear() {
		return vehicleYear;
	}

	public String getCounty() {
		return county;
	}

	public String getCompany() {
		return company;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public void setVehicleYear(String vehicleYear) {
		this.vehicleYear = vehicleYear;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SearchLogsDTO searchLogsDTO = (SearchLogsDTO) o;
        if (searchLogsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), searchLogsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return "SearchLogsDTO [id=" + id + ", fullName=" + fullName + ", email=" + email + ", phone=" + phone + ", zip="
				+ zip + ", searchType=" + searchType + ", searchedDate=" + searchedDate + ", vehicleType=" + vehicleType
				+ ", vehicleYear=" + vehicleYear + ", county=" + county + ", company=" + company + "]";
	}

   
}
