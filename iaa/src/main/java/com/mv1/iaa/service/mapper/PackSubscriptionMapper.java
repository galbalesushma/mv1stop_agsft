package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.PackSubscription;
import com.mv1.iaa.service.dto.PackSubscriptionDTO;

/**
 * Mapper for the entity PackSubscription and its DTO PackSubscriptionDTO.
 */
@Mapper(componentModel = "spring", uses = {ProfileMapper.class})
public interface PackSubscriptionMapper extends EntityMapper<PackSubscriptionDTO, PackSubscription> {

    @Mapping(source = "profile.id", target = "profileId")
    PackSubscriptionDTO toDto(PackSubscription packSubscription);

    @Mapping(source = "profileId", target = "profile")
    PackSubscription toEntity(PackSubscriptionDTO packSubscriptionDTO);

    default PackSubscription fromId(Long id) {
        if (id == null) {
            return null;
        }
        PackSubscription packSubscription = new PackSubscription();
        packSubscription.setId(id);
        return packSubscription;
    }
}
