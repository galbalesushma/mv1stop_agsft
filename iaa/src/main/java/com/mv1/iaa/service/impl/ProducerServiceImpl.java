package com.mv1.iaa.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.domain.Producer;
import com.mv1.iaa.repository.ProducerRepository;
import com.mv1.iaa.service.ProducerService;
import com.mv1.iaa.service.dto.ProducerDTO;
import com.mv1.iaa.service.mapper.ProducerMapper;

/**
 * Service Implementation for managing Producer.
 */
@Service
@Transactional
public class ProducerServiceImpl implements ProducerService {

    private final Logger log = LoggerFactory.getLogger(ProducerServiceImpl.class);

    private final ProducerRepository producerRepository;

    private final ProducerMapper producerMapper;

    public ProducerServiceImpl(ProducerRepository producerRepository, ProducerMapper producerMapper) {
        this.producerRepository = producerRepository;
        this.producerMapper = producerMapper;
    }

    /**
     * Save a producer.
     *
     * @param producerDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProducerDTO save(ProducerDTO producerDTO) {
        log.debug("Request to save Producer : {}", producerDTO);
        Producer producer = producerMapper.toEntity(producerDTO);
        producer = producerRepository.save(producer);
        return producerMapper.toDto(producer);
    }

    /**
     * Get all the producers.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ProducerDTO> findAll() {
        log.debug("Request to get all Producers");
        return producerRepository.findAll().stream()
            .map(producerMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one producer by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProducerDTO> findOne(Long id) {
        log.debug("Request to get Producer : {}", id);
        return producerRepository.findById(id)
            .map(producerMapper::toDto);
    }

    /**
     * Delete the producer by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Producer : {}", id);
        producerRepository.deleteById(id);
    }
}
