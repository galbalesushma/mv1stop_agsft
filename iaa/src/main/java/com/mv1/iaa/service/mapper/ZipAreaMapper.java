package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;

import com.mv1.iaa.domain.ZipArea;
import com.mv1.iaa.service.dto.ZipAreaDTO;

/**
 * Mapper for the entity ZipArea and its DTO ZipAreaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ZipAreaMapper extends EntityMapper<ZipAreaDTO, ZipArea> {



    default ZipArea fromId(Long id) {
        if (id == null) {
            return null;
        }
        ZipArea zipArea = new ZipArea();
        zipArea.setId(id);
        return zipArea;
    }
}
