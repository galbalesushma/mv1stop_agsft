package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.AgentDistribution;
import com.mv1.iaa.service.dto.AgentDistributionDTO;

/**
 * Mapper for the entity AgentDistribution and its DTO AgentDistributionDTO.
 */
@Mapper(componentModel = "spring", uses = {ZipAreaMapper.class})
public interface AgentDistributionMapper extends EntityMapper<AgentDistributionDTO, AgentDistribution> {

    @Mapping(source = "zip.id", target = "zipId")
    AgentDistributionDTO toDto(AgentDistribution agentDistribution);

    @Mapping(source = "zipId", target = "zip")
    AgentDistribution toEntity(AgentDistributionDTO agentDistributionDTO);

    default AgentDistribution fromId(Long id) {
        if (id == null) {
            return null;
        }
        AgentDistribution agentDistribution = new AgentDistribution();
        agentDistribution.setId(id);
        return agentDistribution;
    }
}
