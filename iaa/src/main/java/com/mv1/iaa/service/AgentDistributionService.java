package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import com.mv1.iaa.service.dto.AgentDistributionDTO;

/**
 * Service Interface for managing AgentDistribution.
 */
public interface AgentDistributionService {

    /**
     * Save a agentDistribution.
     *
     * @param agentDistributionDTO the entity to save
     * @return the persisted entity
     */
    AgentDistributionDTO save(AgentDistributionDTO agentDistributionDTO);

    /**
     * Get all the agentDistributions.
     *
     * @return the list of entities
     */
    List<AgentDistributionDTO> findAll();


    /**
     * Get the "id" agentDistribution.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AgentDistributionDTO> findOne(Long id);

    /**
     * Delete the "id" agentDistribution.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
