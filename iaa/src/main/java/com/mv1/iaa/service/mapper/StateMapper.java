package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;

import com.mv1.iaa.domain.State;
import com.mv1.iaa.service.dto.StateDTO;

/**
 * Mapper for the entity State and its DTO StateDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface StateMapper extends EntityMapper<StateDTO, State> {



    default State fromId(Long id) {
        if (id == null) {
            return null;
        }
        State state = new State();
        state.setId(id);
        return state;
    }
}
