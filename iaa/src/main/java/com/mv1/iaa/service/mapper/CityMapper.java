package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;

import com.mv1.iaa.domain.City;
import com.mv1.iaa.service.dto.CityDTO;

/**
 * Mapper for the entity City and its DTO CityDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CityMapper extends EntityMapper<CityDTO, City> {



    default City fromId(Long id) {
        if (id == null) {
            return null;
        }
        City city = new City();
        city.setId(id);
        return city;
    }
}
