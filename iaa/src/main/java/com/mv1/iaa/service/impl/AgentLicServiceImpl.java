package com.mv1.iaa.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.domain.AgentLic;
import com.mv1.iaa.repository.AgentLicRepository;
import com.mv1.iaa.service.AgentLicService;
import com.mv1.iaa.service.dto.AgentLicDTO;
import com.mv1.iaa.service.mapper.AgentLicMapper;

/**
 * Service Implementation for managing AgentLic.
 */
@Service
@Transactional
public class AgentLicServiceImpl implements AgentLicService {

    private final Logger log = LoggerFactory.getLogger(AgentLicServiceImpl.class);

    private final AgentLicRepository agentLicRepository;

    private final AgentLicMapper agentLicMapper;

    public AgentLicServiceImpl(AgentLicRepository agentLicRepository, AgentLicMapper agentLicMapper) {
        this.agentLicRepository = agentLicRepository;
        this.agentLicMapper = agentLicMapper;
    }

    /**
     * Save a agentLic.
     *
     * @param agentLicDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AgentLicDTO save(AgentLicDTO agentLicDTO) {
        log.debug("Request to save AgentLic : {}", agentLicDTO);
        AgentLic agentLic = agentLicMapper.toEntity(agentLicDTO);
        agentLic = agentLicRepository.save(agentLic);
        return agentLicMapper.toDto(agentLic);
    }

    /**
     * Get all the agentLics.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgentLicDTO> findAll() {
        log.debug("Request to get all AgentLics");
        return agentLicRepository.findAll().stream()
            .map(agentLicMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one agentLic by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AgentLicDTO> findOne(Long id) {
        log.debug("Request to get AgentLic : {}", id);
        return agentLicRepository.findById(id)
            .map(agentLicMapper::toDto);
    }

    /**
     * Delete the agentLic by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AgentLic : {}", id);
        agentLicRepository.deleteById(id);
    }
}
