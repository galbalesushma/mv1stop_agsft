package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Producer entity.
 */
public class ProducerDTO implements Serializable {

    private Long id;

    private String name;

    private Boolean ceCompliance;

    private Long addrId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isCeCompliance() {
        return ceCompliance;
    }

    public void setCeCompliance(Boolean ceCompliance) {
        this.ceCompliance = ceCompliance;
    }

    public Long getAddrId() {
        return addrId;
    }

    public void setAddrId(Long addressId) {
        this.addrId = addressId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProducerDTO producerDTO = (ProducerDTO) o;
        if (producerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), producerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProducerDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ceCompliance='" + isCeCompliance() + "'" +
            ", addr=" + getAddrId() +
            "}";
    }
}
