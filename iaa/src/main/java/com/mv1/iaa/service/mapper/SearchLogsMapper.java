package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;

import com.mv1.iaa.domain.SearchLogs;
import com.mv1.iaa.service.dto.SearchLogsDTO;

/**
 * Mapper for the entity SearchLogs and its DTO SearchLogsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SearchLogsMapper extends EntityMapper<SearchLogsDTO, SearchLogs> {



    default SearchLogs fromId(Long id) {
        if (id == null) {
            return null;
        }
        SearchLogs searchLogs = new SearchLogs();
        searchLogs.setId(id);
        return searchLogs;
    }
}
