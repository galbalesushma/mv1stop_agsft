package com.mv1.iaa.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.business.exception.Mv1Exception;
import com.mv1.iaa.domain.AgentExtension;
import com.mv1.iaa.domain.AvailableCarriers;
import com.mv1.iaa.domain.County;
import com.mv1.iaa.domain.ExtensionCounty;
import com.mv1.iaa.domain.Facility;
import com.mv1.iaa.domain.Profile;
import com.mv1.iaa.domain.ProfileCarriers;
import com.mv1.iaa.domain.enumeration.ProfileType;
import com.mv1.iaa.repository.AgentExtensionRepository;
import com.mv1.iaa.repository.AvailableCarriersRepository;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.CustomCountyRepository;
import com.mv1.iaa.repository.CustomFacilityRepository;
import com.mv1.iaa.repository.ExtensionCountyRepository;
import com.mv1.iaa.repository.ProfileCarriersRepository;
import com.mv1.iaa.repository.ProfileRepository;
import com.mv1.iaa.service.PBXIntegrationService;
import com.mv1.iaa.service.ProfileService;
import com.mv1.iaa.service.dto.ProfileDTO;
import com.mv1.iaa.service.mapper.ProfileMapper;

/**
 * Service Implementation for managing Profile.
 */
@Service
@Transactional
public class ProfileServiceImpl implements ProfileService {

	private final Logger log = LoggerFactory.getLogger(ProfileServiceImpl.class);

	private final ProfileRepository profileRepository;

	private final ProfileMapper profileMapper;

	@Autowired
	private CustomAgentRepository customAgentRepository;

	@Autowired
	private PBXIntegrationService pbxIntegrationService;

	@Autowired
	private CustomFacilityRepository customFacilityRepository;

	@Autowired
	private AvailableCarriersRepository availableCarriersRepository;

	@Autowired
	private ProfileCarriersRepository profileCarriersRepository;

	@Autowired
	private AgentExtensionRepository agentExtensionRepository;

	@Autowired
	private ExtensionCountyRepository extensionCountyRepository;

	@Autowired
	private CustomCountyRepository customCountyRepository;

	public ProfileServiceImpl(ProfileRepository profileRepository, ProfileMapper profileMapper) {
		this.profileRepository = profileRepository;
		this.profileMapper = profileMapper;
	}

	/**
	 * Save a profile.
	 *
	 * @param profileDTO the entity to save
	 * @return the persisted entity
	 * @throws Exception
	 */
	@Override
	public ProfileDTO save(ProfileDTO profileDTO) throws Exception {
		log.debug("Request to save Profile : {}", profileDTO);
		Profile profile = profileMapper.toEntity(profileDTO);
		Profile profileFromDB = new Profile();
		if (profile.getId() != null) {
			profileFromDB = profileRepository.findById(profile.getId()).orElse(null);
		}
		String oldPreferredTelNumber = profileFromDB != null ? profileFromDB.getPhone() : null;

		String oldSecondTelNumber = profileFromDB != null ? profileFromDB.getSecondPhone() : null;

		log.info("Profile from database :" + profileFromDB);
		profile.setPrefCompany(profileDTO.getPrefCompany());
		profile.setComments(profileDTO.getComments());
		profile.setInsuranceCompany(profileDTO.getInsuranceCompany());
		profile.setPhone(profileDTO.getPhone());
		// profile.setIsSpeaksSpanish(profileDTO.getIsSpeaksSpanish());
		profile = profileRepository.save(profile);
		if (profileDTO.getInsuranceCompany() != null && profileDTO.getInsuranceCompany()!="" ) {

			AvailableCarriers carr = availableCarriersRepository
					.findByCarrierAndShow(profileDTO.getInsuranceCompany().toString(), false);
				if (carr == null) {
					AvailableCarriers c = new AvailableCarriers();
					c.setCarrier(profileDTO.getInsuranceCompany());
					c.setShowInList(false);
					availableCarriersRepository.save(c);
				}

				carr = availableCarriersRepository.findByCarrierAndShow(profileDTO.getInsuranceCompany(), false);
				if (carr != null) {
					ProfileCarriers pc = profileCarriersRepository.findPrimaryCompanyOfProfile(profile.getId());
					if (pc == null) {
						pc = new ProfileCarriers();
					}
//				ProfileCarriers pc = new ProfileCarriers();
					pc.setProfileId(profile.getId());
					pc.setCarriersId(carr.getId());
					pc.setIsPrimary(true);
					profileCarriersRepository.save(pc);
				}
			}
		

		if (oldPreferredTelNumber != null && oldPreferredTelNumber != "") {

			if (ProfileType.AGENT.equals(profile.getType())) {
				com.mv1.iaa.domain.Agent agent = customAgentRepository.findAgentByProfileId(profileFromDB.getId());
				if (!profileDTO.getPhone().equals(oldPreferredTelNumber)) {
					AgentExtension ext = agentExtensionRepository.findByAgentAndPrimary(agent.getId());

					if (ext != null) {
						pbxIntegrationService.extensionUpdate(ext.getExtension(), profile.getPhone());
						ext.setPhone(profile.getPhone());
						agentExtensionRepository.save(ext);

					}
					// pbxIntegrationService.extensionUpdate(agent.getExtension(),
					// profile.getPhone());
				}
			} else if (ProfileType.FACILITY.equals(profile.getType())) {

				Facility facility = customFacilityRepository.findFacilityByProfileId(profileFromDB.getId());
				if (!profileDTO.getPhone().equals(oldPreferredTelNumber)) {
					pbxIntegrationService.extensionUpdate(facility.getExtension(), profile.getPhone());
				}

			}
		} else if (profile.getPhone() != null && profile.getPhone() != "") {
			if (ProfileType.AGENT.equals(profile.getType())) {
				com.mv1.iaa.domain.Agent agent = customAgentRepository.findAgentByProfileId(profileFromDB.getId());
				AgentExtension ext = agentExtensionRepository.findByAgentAndPrimary(agent.getId());
				if (ext != null) {
					pbxIntegrationService.extensionUpdate(ext.getExtension(), profile.getPhone());
					ext.setPhone(profile.getPhone());
					agentExtensionRepository.save(ext);
				}
//					pbxIntegrationService.extensionUpdate(agent.getExtension(), profile.getPhone());
			} else if (ProfileType.FACILITY.equals(profile.getType())) {
				Facility facility = customFacilityRepository.findFacilityByProfileId(profileFromDB.getId());
				if (facility != null) {
					pbxIntegrationService.extensionUpdate(facility.getExtension(), profile.getPhone());
				}
			}
		}

		if (profileDTO.getSecondPhone() != null && profileDTO.getSecondPhone() !="") {

			if (ProfileType.AGENT.equals(profile.getType())) {
				com.mv1.iaa.domain.Agent agent = customAgentRepository.findAgentByProfileId(profileFromDB.getId());

				List<ExtensionCounty> counties = extensionCountyRepository.findAll();

				Long countyId = null;
				Boolean countyFound = false;
				if (agent.getBusiness() != null && agent.getBusiness().getAddress() != null
						&& agent.getBusiness().getAddress().getCounty() != null) {
					countyId = agent.getBusiness().getAddress().getCounty().getId();
				}
				if (countyId != null) {
					Optional<County> c = customCountyRepository.findById(countyId);
					if (c.isPresent()) {
//						c.get().getName()
						for (ExtensionCounty extensionCounty : counties) {
							if (extensionCounty.getName().equalsIgnoreCase(c.get().getName())) {
								countyFound = true;
								break;
							}
						}
					}
				}

				if (profileDTO.getSecondPhone() != null && !profileDTO.getSecondPhone().equals(oldSecondTelNumber)) {
					AgentExtension ext = agentExtensionRepository.findByAgentAndSecondary(agent.getId());
					if (ext != null && countyFound) {
						pbxIntegrationService.extensionUpdate(ext.getExtension(), profile.getSecondPhone());
						ext.setPhone(profile.getSecondPhone());
						agentExtensionRepository.save(ext);

					} else {

						// throw for wake county users
						if (countyFound) {
							/*
							 * throw new Mv1Exception(
							 * "Extension already used by another phone, please add extension for phone");
							 */
						}

					}
//					pbxIntegrationService.extensionUpdate(agent.getExtension(), profile.getPhone());
				} else if (profile.getSecondPhone() != null && profile.getSecondPhone() != "") {
					if (ProfileType.AGENT.equals(profile.getType())) {
						AgentExtension ext = agentExtensionRepository.findByAgentAndSecondary(agent.getId());
						if (ext != null && countyFound) {
							pbxIntegrationService.extensionUpdate(ext.getExtension(), profile.getSecondPhone());
							ext.setPhone(profile.getSecondPhone());
							agentExtensionRepository.save(ext);

						} else {
							// throw for wake county users
							if (countyFound) {
								//throw new Mv1Exception("Please add or update extension for phone");
							}

						}
//							pbxIntegrationService.extensionUpdate(agent.getExtension(), profile.getPhone());
					}
				}
			}
		}

		return profileMapper.toDto(profile);
	}

	/**
	 * Get all the profiles.
	 *
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public List<ProfileDTO> findAll() {
		log.debug("Request to get all Profiles");
		return profileRepository.findAll().stream().map(profileMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Get one profile by id.
	 *
	 * @param id the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<ProfileDTO> findOne(Long id) {
		log.debug("Request to get Profile : {}", id);
		Optional<Profile> p = profileRepository.findById(id);
		Optional<ProfileDTO> dto = p.map(profileMapper::toDto);
		Optional<ProfileDTO> d = dto;
		if (p.isPresent()) {
			d.get().setPrefCompany(p.get().getPrefCompany());
			d.get().setComments(p.get().getComments());
			d.get().setInsuranceCompany(p.get().getInsuranceCompany());
			d.get().setPhone(p.get().getPhone());
		}
		return d;
	}

	/**
	 * Delete the profile by id.
	 *
	 * @param id the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Profile : {}", id);
		profileRepository.deleteById(id);
	}
}
