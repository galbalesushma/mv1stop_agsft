package com.mv1.iaa.service;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.mv1.iaa.business.view.web.req.AgentVM;
import com.mv1.iaa.business.view.web.req.FacilityVM;
import com.mv1.iaa.business.view.web.req.PaymentReportVM;
import com.mv1.iaa.business.view.web.req.SearchLogsVM;
import com.mv1.iaa.business.view.web.resp.SearchPage;
import com.mv1.iaa.service.dto.CustomFilterSearchDTO;

@Service
@Transactional
public interface CustomAgentFilterSevice {

	List<String> getStates();

	List<String> cityByState(String search);

	List<String> countiesByState( String search);

	List<String> findAgentByFirstName(String search);

	List<String> findAgentByLastName(String search);

	List<String> findNpnList(String search);

	List<String> findCompanyList(String search);

	List<String> agentZipCode(String search);

	List<AgentVM> getAllAgentsByFilter(String state, String city, String zip, String licStatus, String incAB, String fName, String lName, String company, String npn, String county, List<CustomFilterSearchDTO> customFilterSearchDTOs);

	List<AgentVM> getAllAgentsWithoutFilter(List<CustomFilterSearchDTO> customFilterSearchDTOs, String generalSearch);

	File downloadAgentList(List<AgentVM> list, String fileType);

	List<PaymentReportVM> getPaymentReport(Date fromDate, Date toDate, String zip, String incStatus, String state, String county, String city, String incAB, String fname, String lname, String npn, String isn, String company, Date paymentDate, String utype, String srep, List<CustomFilterSearchDTO> customFilterSearchDTOs, String generalSearch);

	File downloadPaymentReport(List<PaymentReportVM> list,String fileType);

	List<String> findIsnList(String search);

	List<SearchLogsVM> getLogReport(String zip, String phone, String searchType, String email, String fullName, String vehicleType, String vehicleYear, String county, String company, Date searchedDateConvert, Date toDate1, Date fromDate1, List<CustomFilterSearchDTO> customFilterSearchDTOs, String generalSearch);

	File downloadLogReport(List<SearchLogsVM> list, String fileType);

	List<String> findSearchNameList(String search);

	List<String> findSearchEmailList(String search);

	List<String> findSearchPhonelList(String search);

	List<String> findSearchZiplList(String search);

	List<String> findSearchCountylList(String search);

	List<String> findSearchCompanylList(String search);

	List<String> findSearchVehicleYearList(String search);

	List<FacilityVM> getAllFacilitiesByFilter(String city, String zip, String incStatus, String incAB, String fName,
			String lName, String isn, String county, String state, String bName, List<CustomFilterSearchDTO> customFilterSearchDTOs, String generalSearch);

	List<String> getFacilityCities(String search);

	List<String> getFacilityZip(String search);

	List<String> getFacilityFname(String search);

	List<String> getFacilityLname(String search);

	List<String> getFacilityIsn(String search);

	File downloadFacilityList(List<FacilityVM> list, String fileType);


	SearchPage getSearchAgentFacilitySlots(String type, String zip, Integer page, String company, String agent);

	List<String> getFacilityBusinessName(String search);

	List<String> getFacilityCountyName(String search);

	List<String> getFacilityStateName();

	SearchPage getSearchOfInsurance(String type, String zip, Integer page, String company, String agent);

    SearchPage getSearchCustomerServiceByCompany(Integer page, String company);

}
