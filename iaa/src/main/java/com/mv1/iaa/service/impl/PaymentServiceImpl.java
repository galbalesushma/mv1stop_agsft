package com.mv1.iaa.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.domain.Payment;
import com.mv1.iaa.repository.PaymentRepository;
import com.mv1.iaa.service.PaymentService;
import com.mv1.iaa.service.dto.PaymentDTO;
import com.mv1.iaa.service.mapper.PaymentMapper;

/**
 * Service Implementation for managing Payment.
 */
@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

    private final Logger log = LoggerFactory.getLogger(PaymentServiceImpl.class);

    private final PaymentRepository paymentRepository;

    private final PaymentMapper paymentMapper;

    public PaymentServiceImpl(PaymentRepository paymentRepository, PaymentMapper paymentMapper) {
        this.paymentRepository = paymentRepository;
        this.paymentMapper = paymentMapper;
    }

    /**
     * Save a payment.
     *
     * @param paymentDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PaymentDTO save(PaymentDTO paymentDTO) {
        log.debug("Request to save Payment : {}", paymentDTO);
        Payment payment = paymentMapper.toEntity(paymentDTO);
        if(paymentDTO.getTxnType()!=null && paymentDTO.getTxnType().trim().equalsIgnoreCase("Paper-Cheque"))
        {
        	payment.setTxnType(paymentDTO.getTxnType());
        	payment.setChequeNo(paymentDTO.getChequeNo());
        	
        }
        else
        	if(paymentDTO.getTxnType()!=null && paymentDTO.getTxnType().trim().equalsIgnoreCase("ACH"))
        	{
        		
        		payment.setRoutingNumber(paymentDTO.getRoutingNumber());
        		payment.setAccountNumber(paymentDTO.getAccountNumber());
                payment.setAccountType(paymentDTO.getAccountType());
        	}
        payment.setTxnType(paymentDTO.getTxnType());
        payment = paymentRepository.save(payment);
        return paymentMapper.toDto(payment);
    }

    /**
     * Get all the payments.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PaymentDTO> findAll() {
        log.debug("Request to get all Payments");
        return paymentRepository.findAll().stream()
            .map(paymentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one payment by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PaymentDTO> findOne(Long id) {
        log.debug("Request to get Payment : {}", id);
        return paymentRepository.findById(id)
            .map(paymentMapper::toDto);
    }

    /**
     * Delete the payment by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Payment : {}", id);
        paymentRepository.deleteById(id);
    }
}
