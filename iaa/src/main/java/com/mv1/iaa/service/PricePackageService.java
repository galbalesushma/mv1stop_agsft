package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import com.mv1.iaa.service.dto.PricePackageDTO;

/**
 * Service Interface for managing PricePackage.
 */
public interface PricePackageService {

    /**
     * Save a pricePackage.
     *
     * @param pricePackageDTO the entity to save
     * @return the persisted entity
     */
    PricePackageDTO save(PricePackageDTO pricePackageDTO);

    /**
     * Get all the pricePackages.
     *
     * @return the list of entities
     */
    List<PricePackageDTO> findAll();


    /**
     * Get the "id" pricePackage.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PricePackageDTO> findOne(Long id);

    /**
     * Delete the "id" pricePackage.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
