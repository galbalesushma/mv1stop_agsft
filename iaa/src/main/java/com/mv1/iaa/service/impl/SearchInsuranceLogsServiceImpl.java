package com.mv1.iaa.service.impl;

import com.mv1.iaa.service.SearchInsuranceLogsService;
import com.mv1.iaa.domain.SearchInsuranceLogs;
import com.mv1.iaa.repository.SearchInsuranceLogsRepository;
import com.mv1.iaa.service.dto.SearchInsuranceLogsDTO;
import com.mv1.iaa.service.mapper.SearchInsuranceLogsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SearchInsuranceLogs.
 */
@Service
@Transactional
public class SearchInsuranceLogsServiceImpl implements SearchInsuranceLogsService {

    private final Logger log = LoggerFactory.getLogger(SearchInsuranceLogsServiceImpl.class);

    private final SearchInsuranceLogsRepository searchInsuranceLogsRepository;

    private final SearchInsuranceLogsMapper searchInsuranceLogsMapper;

    public SearchInsuranceLogsServiceImpl(SearchInsuranceLogsRepository searchInsuranceLogsRepository, SearchInsuranceLogsMapper searchInsuranceLogsMapper) {
        this.searchInsuranceLogsRepository = searchInsuranceLogsRepository;
        this.searchInsuranceLogsMapper = searchInsuranceLogsMapper;
    }

    /**
     * Save a searchInsuranceLogs.
     *
     * @param searchInsuranceLogsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SearchInsuranceLogsDTO save(SearchInsuranceLogsDTO searchInsuranceLogsDTO) {
        log.debug("Request to save SearchInsuranceLogs : {}", searchInsuranceLogsDTO);

        SearchInsuranceLogs searchInsuranceLogs = searchInsuranceLogsMapper.toEntity(searchInsuranceLogsDTO);
        searchInsuranceLogs = searchInsuranceLogsRepository.save(searchInsuranceLogs);
        return searchInsuranceLogsMapper.toDto(searchInsuranceLogs);
    }

    /**
     * Get all the searchInsuranceLogs.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SearchInsuranceLogsDTO> findAll() {
        log.debug("Request to get all SearchInsuranceLogs");
        return searchInsuranceLogsRepository.findAll().stream()
            .map(searchInsuranceLogsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one searchInsuranceLogs by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SearchInsuranceLogsDTO> findOne(Long id) {
        log.debug("Request to get SearchInsuranceLogs : {}", id);
        return searchInsuranceLogsRepository.findById(id)
            .map(searchInsuranceLogsMapper::toDto);
    }

    /**
     * Delete the searchInsuranceLogs by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SearchInsuranceLogs : {}", id);
        searchInsuranceLogsRepository.deleteById(id);
    }
}
