package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import com.mv1.iaa.service.dto.ProducerDTO;

/**
 * Service Interface for managing Producer.
 */
public interface ProducerService {

    /**
     * Save a producer.
     *
     * @param producerDTO the entity to save
     * @return the persisted entity
     */
    ProducerDTO save(ProducerDTO producerDTO);

    /**
     * Get all the producers.
     *
     * @return the list of entities
     */
    List<ProducerDTO> findAll();


    /**
     * Get the "id" producer.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ProducerDTO> findOne(Long id);

    /**
     * Delete the "id" producer.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
