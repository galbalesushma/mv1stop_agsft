package com.mv1.iaa.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import java.util.Optional;

import java.util.UUID;

import java.util.stream.Collectors;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.view.web.req.AgentVM;
import com.mv1.iaa.business.view.web.req.FacilityVM;
import com.mv1.iaa.business.view.web.req.PaymentReportVM;
import com.mv1.iaa.business.view.web.req.SearchLogsVM;
import com.mv1.iaa.business.view.web.req.SearchVM;
import com.mv1.iaa.business.view.web.resp.SearchPage;
import com.mv1.iaa.domain.Address;
import com.mv1.iaa.domain.Agent;
import com.mv1.iaa.domain.AgentExtension;
import com.mv1.iaa.domain.Business;
import com.mv1.iaa.domain.City;
import com.mv1.iaa.domain.County;
import com.mv1.iaa.domain.CustomerService;
import com.mv1.iaa.domain.ExtensionCounty;
import com.mv1.iaa.domain.ProfileCarriers;
import com.mv1.iaa.domain.State;
import com.mv1.iaa.repository.AgentExtensionRepository;
import com.mv1.iaa.repository.AvailableCarriersRepository;
import com.mv1.iaa.repository.BusinessRepository;
import com.mv1.iaa.repository.CustomAddressRepository;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.CustomCityRepository;
import com.mv1.iaa.repository.CustomCountyRepository;
import com.mv1.iaa.repository.CustomFacilityRepository;
import com.mv1.iaa.repository.CustomSearchLogsRepository;
import com.mv1.iaa.repository.CustomStateRepository;
import com.mv1.iaa.repository.CustomerServiceRepository;
import com.mv1.iaa.repository.ExtensionCountyRepository;
import com.mv1.iaa.repository.ProfileCarriersRepository;
import com.mv1.iaa.repository.StateRepository;
import com.mv1.iaa.service.CustomAgentFilterSevice;
import com.mv1.iaa.service.dto.CustomFilterSearchDTO;
import com.mv1.iaa.service.mapper.CityMapper;
import com.mv1.iaa.service.mapper.CountyMapper;
import com.mv1.iaa.service.mapper.StateMapper;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;
import java.util.Comparator;

@Service
@Transactional
public class CustomAgentFilterSeviceImpl implements CustomAgentFilterSevice {

	@Autowired
	private CustomerServiceRepository customerServiceRepository;

	@Autowired
	StateRepository stateRepository;

	@Autowired
	StateMapper stateMapper;

	@Autowired
	CustomAgentRepository customAgentRepository;

	@Autowired
	CityMapper cityMapper;

	@Autowired
	CountyMapper countyMapper;

	@Autowired
	CustomStateRepository customStateRepository;

	@Autowired
	CustomCityRepository customCityRepository;

	@Autowired
	CustomFacilityRepository customFacilityRepository;

	@Autowired
	CustomSearchLogsRepository customSearchLogsRepository;

	@Autowired
	CustomCountyRepository customCountyRepository;

	@Autowired
	CustomAddressRepository customAddressRepository;

	@Autowired
	BusinessRepository businessRepository;

	@Autowired
	ProfileCarriersRepository profileCarriersRepository;

	@Autowired
	AvailableCarriersRepository availableCarriersRepository;

	@Autowired
	AgentExtensionRepository agentExtensionRepository;

	@Autowired
	ExtensionCountyRepository extensionCountyRepository;

	@Autowired
	public CustomAgentFilterSeviceImpl customAgentFilterSeviceImpl;

	private final Logger log = LoggerFactory.getLogger(CustomAgentFilterSevice.class);

	private static String[] keys = new String[] { "id", "name", "cityCode", "stateCode" };
	private static String[] countryKeys = new String[] { "id", "name", "code", "stateCode" };
	private static String[] agentKeys = new String[] { "id", "npn", "naicno", "fein", "resState", "firstName",
			"middleName", "lastName", "suffix", "businessName", "licNumber", "licType", "lineAuth", "firstActiveDate",
			"effectiveDate", "expDate", "licStatus", "domState", "bAddr1", "bAddr2", "bAddr3", "bZip", "bCountry",
			"isCECompliant", "pAddr1", "pAddr2", "pAddr3", "pZip", "pCountry", "bPhone", "bEmail", "incAB", "certified",
			"preCertified", "profileId", "paymentDate", "prefPhone", "prefMobile", "isRegistered" };

	private static String[] agentSignUpKeys = new String[] { "id", "npn", "firstName", "middleName", "lastName",
			"suffix", "profileId", "incAB", "certified", "preCertified", "paymentDate", "prefPhone", "prefMobile",
			"isRegistered" };

	private static String[] paymentReportKeys = new String[] { "zip", "amount", "slots", "profileId", "paymentDate",
			"userType", "agentFirstName", "agentLastName", "agentEmail", "npn", "packageId", "sales_resp", "isn",
			"addrId" };

	private static String[] searchLogReportKeys = new String[] { "zip", "fullName", "email", "phone", "searchType",
			"searchedDate", "vehicleType", "vehicleYear", "county", "company" };

	public int lastIndex = 0;

	@Override
	public List<String> getStates() {
		// TODO Auto-generated method stub
		/*
		 * List<StateDTO> stateList =
		 * stateRepository.findAll().stream().map(stateMapper::toDto)
		 * .collect(Collectors.toCollection(LinkedList::new)); return stateList;
		 */

		List<String> sList = new ArrayList<>();
		String f = "%" + "AGENT" + "%";
		String likeSearch = "%%";
		sList = customFacilityRepository.findFacilityState(f);
		return sList;
	}

	@Override
	public List<String> cityByState(String search) {
		String likeSearch = search + "%";
		String agent = "%" + "AGENT" + "%";
		List<String> cityList = new ArrayList<>();
		cityList = customAgentRepository.findCityByStateAndSearch(likeSearch, agent);
		return cityList;

	}

	@Override
	public List<String> countiesByState(String search) {
		String likeSearch = search + "%";
		String agent = "%" + "AGENT" + "%";
		List<String> agArr = customAgentRepository.findCountyByStateAndSearch(likeSearch, agent);
		return agArr;
	}

	@Override
	public List<String> findAgentByFirstName(String search) {
		String likeSearch = search + "%";
		List<String> agentList = new ArrayList<>();
		agentList = customAgentRepository.findAllByFirstName(likeSearch);
		return agentList;
	}

	@Override
	public List<String> findAgentByLastName(String search) {
		String likeSearch = search + "%";
		List<String> agentList = new ArrayList<>();
		agentList = customAgentRepository.findAllByLastName(likeSearch);
		return agentList;
	}

	@Override
	public List<String> findNpnList(String search) {
		String likeSearch = search + "%";
		List<String> npnList = new ArrayList<>();
		npnList = customAgentRepository.findAllNpnBySearch(likeSearch);
		return npnList;
	}

	@Override
	public List<String> findCompanyList(String search) {
		String likeSearch = search + "%";
		List<String> companyList = new ArrayList<>();
		companyList = customAgentRepository.findAllCompanyBySearch(likeSearch);
		return companyList;
	}

	@Override
	public List<String> agentZipCode(String search) {
		String likeSearch = search + "%";
		List<String> companyList = new ArrayList<>();
		companyList = customAgentRepository.findAgentZipCodeBySearch(likeSearch);
		return companyList;
	}

	@Override
	public List<AgentVM> getAllAgentsWithoutFilter(List<CustomFilterSearchDTO> customFilterSearchDTOs,
			String generalSearch) {

		List<AgentVM> agents = new LinkedList<>();
		Object[][] agArr = customAgentRepository.getAllAgents();
		Object[][] agArr1 = customAgentRepository.getAllSignUpOnlyAgents();

		if (agArr != null && agArr.length > 0) {
			for (Object[] arr : agArr) {
				try {

					agents.add(CommonMapper.fromKeyVal(agentKeys, arr, AgentVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}
		}

		List<AgentVM> agents1 = new LinkedList<>();
		if (agArr1 != null && agArr1.length > 0) {
			for (Object[] arr1 : agArr1) {
				try {
					agents1.add(CommonMapper.fromKeyVal(agentSignUpKeys, arr1, AgentVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}
		}

		if (!agents1.isEmpty()) {
			agents.addAll(agents1);

		}

		List<AgentVM> agentsList = new ArrayList<>();
		agentsList.addAll(agents);

		for (AgentVM agent : agents) {
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			if (agent.getPaymentDate() != null && agent.getPaymentDate() != "") {
				try {
//					  Date paymentDate = format.parse(agent.getPaymentDate());
					Date paymentDate = format.parse(agent.getPaymentDate());
					if (paymentDate.before(new Date())) {
						agent.setIncStatus("In-Active");
					}
					if (paymentDate.after(new Date())) {
						agent.setIncStatus("Active");
					}

				} catch (ParseException e) {
					System.out.println("Failed to parse date ........");
					return null;
				}
			} else {
				agent.setIncStatus("In-Active");
			}
		}

		for (AgentVM agentVM : agentsList) {
			System.out.println("agentVM.getLicStatus()--------------------" + agentVM.getLicStatus());
			if (agentVM.isCertified() && agentVM.isCertified()) {
				agentVM.setIncABName("Certified");
			} else if (agentVM.isPreCertified() && agentVM.isPreCertified()) {
				agentVM.setIncABName("Pre-Certified");
			} else if (agentVM.isIncAB() && agentVM.isIncAB()) {
				agentVM.setIncABName("INC A/B");
			} else if (agentVM.getLicStatus() != null && agentVM.getLicStatus().equals("Active")) {
				agentVM.setIncABName("Qualified");
			} else {
				agentVM.setIncABName("Non Qualified");
			}
		}

		if (generalSearch != null && !generalSearch.equalsIgnoreCase("")) {

			if (generalSearch.contains(" ")) {
				String[] fullName = generalSearch.split(" ");
				if (fullName.length ==2 ) {
					agentsList = (List<AgentVM>) agentsList.stream()
							.filter(x -> (x.getFirstName().toLowerCase().trim().contains(fullName[0].toLowerCase()))
									&& (x.getLastName().toLowerCase().contains(fullName[1].toLowerCase().trim())))
							.collect(Collectors.toList());
				}
			} else {
				agentsList = (List<AgentVM>) agentsList.stream()
						.filter(x -> (x.getFirstName().toLowerCase().contains(generalSearch.toLowerCase()))
								|| (x.getLastName().toLowerCase().contains(generalSearch.toLowerCase()))
								|| (x.getNpn().toLowerCase().contains(generalSearch.toLowerCase())))
						.collect(Collectors.toList());
			}
//			agents = new ArrayList<>();
			if (customFilterSearchDTOs != null && customFilterSearchDTOs.size() > 0) {
//				agentsList.removeAll(agents);
				agentsList = sortAgentList(agentsList, customFilterSearchDTOs);
			}
		} else {
			if (customFilterSearchDTOs != null && customFilterSearchDTOs.size() > 0) {
				agentsList.removeAll(agents);
				agentsList = sortAgentList(agents, customFilterSearchDTOs);
			}
		}

		return agentsList;
	}

	private List<AgentVM> sortAgentList(List<AgentVM> agents, List<CustomFilterSearchDTO> customFilterSearchDTOs) {

		List<AgentVM> agentsList = new ArrayList<>();

//	        	agentsList  =  null;

		Comparator<AgentVM> sortByNpn = (p, o) -> p.getNpn().compareToIgnoreCase(o.getNpn());
		Comparator<AgentVM> sortByFirstName = (p, o) -> p.getFirstName().compareToIgnoreCase(o.getFirstName());
		Comparator<AgentVM> sortByCertified = (p, o) -> ("" + p.isCertified())
				.compareToIgnoreCase("" + o.isCertified());
		Comparator<AgentVM> sortByPreCertified = (p, o) -> ("" + p.isPreCertified())
				.compareToIgnoreCase("" + o.isPreCertified());

//	        	Comparator<AgentVM> sortByEffDate = Comparator.comparing(AgentVM::getEffectiveDate, Comparator.nullsLast(Comparator.naturalOrder()));

		Comparator<AgentVM> comp = null;
		for (CustomFilterSearchDTO sort : customFilterSearchDTOs) {

			if (sort.getColName().equalsIgnoreCase("npn")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByNpn;
					} else {
						comp = sortByNpn.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByNpn);
					} else {
						comp = comp.thenComparing(sortByNpn).reversed();
					}

				}

			}
			if (sort.getColName().equalsIgnoreCase("firstName")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByFirstName;
					} else {
						comp = sortByFirstName.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByFirstName);
					} else {
						comp = comp.thenComparing(sortByFirstName).reversed();
					}
				}
			}
			if (sort.getColName().equalsIgnoreCase("certified")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByCertified;
					} else {
						comp = sortByCertified.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByCertified);
					} else {
						comp = comp.thenComparing(sortByCertified).reversed();
					}
				}
			}
			if (sort.getColName().equalsIgnoreCase("preCertified")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByCertified;
					} else {
						comp = sortByCertified.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByPreCertified);
					} else {
						comp = comp.thenComparing(sortByPreCertified).reversed();
					}
				}
			}
			if (sort.getColName().equalsIgnoreCase("effDate")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
//								comp = sortByEffDate;
						comp = Comparator.comparing(AgentVM::getEffectiveDate,
								Comparator.nullsLast(Comparator.naturalOrder()));
					} else {
//								comp = sortByEffDate.reversed();
						comp = Comparator.comparing(AgentVM::getEffectiveDate,
								Comparator.nullsLast(Comparator.reverseOrder()));
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(Comparator.comparing(AgentVM::getEffectiveDate,
								Comparator.nullsLast(Comparator.naturalOrder())));
					} else {
						comp = comp.thenComparing(Comparator.comparing(AgentVM::getEffectiveDate,
								Comparator.nullsLast(Comparator.reverseOrder()))).reversed();
					}
				}
			}
		}

		agents.stream().sorted(comp).forEach(agent -> agentsList.add(agent));

		return agentsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgentVM> getAllAgentsByFilter(String state, String city, String zip, String licStatus, String incAB,
			String fName, String lName, String company, String npn, String county,
			List<CustomFilterSearchDTO> customFilterSearchDTOs) {

		if (state == null) {
			state = "";
		}
		if (city == null) {
			city = "";
		}

		if (zip == null) {
			zip = "";
		}

		if (company == null) {
			company = "";
		}

		if (fName == null) {
			fName = "";
		}

		if (lName == null) {
			lName = "";
		}

		if (county == null) {
			county = "";
		}

		List<AgentVM> agents = new LinkedList<>();

		zip = zip + "%";
		company = company + "%";
		fName = "%" + fName + "%";
		lName = "%" + lName + "%";

		Object[][] agArr = customAgentRepository.getAllAgentsByFilter(zip, company, fName, lName);
		if (agArr != null && agArr.length > 0) {
			for (Object[] arr : agArr) {
				try {
					agents.add(CommonMapper.fromKeyVal(agentKeys, arr, AgentVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}
		}

		for (AgentVM agent : agents) {

			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			if (agent.getPaymentDate() != null && !agent.getPaymentDate().isEmpty() && agent.getPaymentDate() != "") {
				try {
					Date paymentDate = format.parse(agent.getPaymentDate());

					if (paymentDate.before(new Date())) {
						agent.setIncStatus("In-Active");
					}
					if (paymentDate.after(new Date())) {
						agent.setIncStatus("Active");
					}

				} catch (ParseException e) {
					System.out.println("Failed to parse date ........");
					return null;
				}
			} else {
				agent.setIncStatus("In-Active");
			}
			System.out.println("agentVM.getLicStatus()--------------------" + agent.getIncStatus());
		}

		if (state != null && !state.isEmpty()) {
			state = "%" + state + "%";
			List<AgentVM> agentsNew = new LinkedList<>();
			List<String> aId = customAgentRepository.findStateOfAgentSearch(state);
			List<String> aIdMailing = customAgentRepository.findMailingStateOfAgentSearch(county);
			if (aId != null && aId.size() > 0) {
				for (AgentVM agent : agents) {
					if (aId.contains(agent.getNpn())) {
						agentsNew.add(agent);
					}
				}
			}
			if (aIdMailing != null && aIdMailing.size() > 0) {
				for (AgentVM agent : agents) {
					if (aIdMailing.contains(agent.getNpn()) && !aId.contains(agent.getNpn())) {
						agentsNew.add(agent);
					}
				}
			}
			agents = agentsNew;
		}

		if (county != null && !county.isEmpty()) {
			county = "%" + county + "%";
			List<AgentVM> agentsNew = new LinkedList<>();
			List<String> aId = customAgentRepository.findCountyOfAgentSearch(county);
			List<String> aIdMailing = customAgentRepository.findMailingCountyOfAgentSearch(county);
			if (aId != null && aId.size() > 0) {
				for (AgentVM agent : agents) {
					if (aId.contains(agent.getNpn())) {
						agentsNew.add(agent);
					}
				}

			}
			if (aIdMailing != null && aIdMailing.size() > 0) {
				for (AgentVM agent : agents) {
					if (aIdMailing.contains(agent.getNpn()) && !aId.contains(agent.getNpn())) {
						agentsNew.add(agent);
					}
				}
			}
			agents = agentsNew;
		}

		if (city != null && !city.isEmpty()) {
			city = "%" + city + "%";
			List<AgentVM> agentsNew = new LinkedList<>();
			List<String> aId = customAgentRepository.findCityOfAgentSearch(city);
			List<String> aIdMailing = customAgentRepository.findMailingCityOfAgentSearch(county);
			if (aId != null && aId.size() > 0) {
				for (AgentVM agent : agents) {
					if (aId.contains(agent.getNpn())) {
						agentsNew.add(agent);
					}
				}
			}

			if (aIdMailing != null && aIdMailing.size() > 0) {
				for (AgentVM agent : agents) {
					if (aIdMailing.contains(agent.getNpn()) && !aId.contains(agent.getNpn())) {
						agentsNew.add(agent);
					}
				}
			}
			agents = agentsNew;
		}

		System.out.println("agents--------------------" + agents);
		if (licStatus != null && licStatus != "" && !licStatus.isEmpty()) {
//			List<AgentVM> agentList = getINStatus(agents,licStatus);
			if (licStatus.trim().equalsIgnoreCase("yes")) {
				agents = (List<AgentVM>) agents.stream().filter(x -> "Active".equalsIgnoreCase(x.getIncStatus()))
						.collect(Collectors.toList());
			}
			if (licStatus.trim().equalsIgnoreCase("no")) {
				agents = (List<AgentVM>) agents.stream().filter(x -> "In-Active".equalsIgnoreCase(x.getIncStatus()))
						.collect(Collectors.toList());
			}

		}

		if (npn != null && npn != "" && !npn.isEmpty()) {
			agents = (List<AgentVM>) agents.stream().filter(x -> x.getNpn().startsWith(npn))
					.collect(Collectors.toList());
		}

		if (incAB != null && incAB.equalsIgnoreCase("member")) {
			agents = (List<AgentVM>) agents.stream().filter(x -> true == (x.isIncAB())).collect(Collectors.toList());

		} else if (incAB != null && incAB.equalsIgnoreCase("certified")) {
			agents = (List<AgentVM>) agents.stream().filter(x -> true == (x.isCertified()))
					.collect(Collectors.toList());

		} else if (incAB != null && incAB.equalsIgnoreCase("preCert")) {
			agents = (List<AgentVM>) agents.stream().filter(x -> true == (x.isPreCertified()))
					.collect(Collectors.toList());
		} else if (incAB != null && incAB.equalsIgnoreCase("qualified")) {
			agents = (List<AgentVM>) agents.stream().filter(x -> "Active".equalsIgnoreCase(x.getLicStatus()))
					.collect(Collectors.toList());

		} else if (incAB != null && incAB.equalsIgnoreCase("nonQualified")) {
			agents = (List<AgentVM>) agents.stream().filter(x -> "Inactive".equalsIgnoreCase(x.getLicStatus()))
					.collect(Collectors.toList());
		}

		List<AgentVM> agentsList = new ArrayList<>();
		agentsList.addAll(agents);

		for (AgentVM agentVM : agentsList) {
			if (agentVM.isCertified() && agentVM.isCertified()) {
				agentVM.setIncABName("Certified");
			} else if (agentVM.isPreCertified() && agentVM.isPreCertified()) {
				agentVM.setIncABName("Pre-Certified");
			} else if (agentVM.isIncAB() && agentVM.isIncAB()) {
				agentVM.setIncABName("INC A/B");
			} else if (agentVM.getLicStatus() != null && agentVM.getLicStatus().equals("Active")) {
				agentVM.setIncABName("Qualified");
			} else {
				agentVM.setIncABName("Non Qualified");
			}
		}

		if (customFilterSearchDTOs != null && customFilterSearchDTOs.size() > 0) {
			agentsList.removeAll(agents);
			agentsList = sortAgentList(agents, customFilterSearchDTOs);
		}

		return agentsList;

	}

	@Override
	public File downloadAgentList(List<AgentVM> list, String fileType) {
		log.debug("Creating CSV of Donations report for req {} ", list.size());
		String[] col = new String[] { "NPN", "First Name", "Last Name", "INC Status", "INC A/B", "Effective Date",
				"Certified", "Pre Certified", "Business Phone", "Pref Phone", "Pref Mobile", "Business Name", "Email" };
		for (AgentVM agent : list) {

			if (agent.isCertified()) {
				agent.setCertifiedName("Yes");
			} else {
				agent.setCertifiedName("No");
			}

			if (agent.isPreCertified()) {
				agent.setPreCertifiedName("Yes");
			} else {
				agent.setPreCertifiedName("No");
			}

		}
		return writeCSV(col, "agent_list_", list, AgentVM.class, fileType);
	}

	@Override
	public File downloadPaymentReport(List<PaymentReportVM> list, String fileType) {
		log.debug("Creating CSV of Donations report for req {} ", list);
		String[] col = new String[] { "Utype", "Payment Date", "Amount", "Fname", "Lname", "Company", "Sales Resp" };

		return writeCSV(col, "payment_report_", list, PaymentReportVM.class, fileType);
	}

	@Override
	public File downloadLogReport(List<SearchLogsVM> list, String fileType) {
		log.debug("Creating CSV of Donations report for req {} ", list);
		String[] col = new String[] { "Full Name", "Zip", "Phone", "Search Type", "Email", "Searched Date",
				"Vehicle Type", "Vehicle Year", "County", "Company" };
		System.out.println("list-------------------------" + list);
		return writeCSV(col, "search_log_report_", list, SearchLogsVM.class, fileType);
	}

	@Override
	public File downloadFacilityList(List<FacilityVM> list, String fileType) {
		log.debug("Creating CSV of Donations report for req {} ", list);
		for (FacilityVM facility : list) {

			if (facility.isIncAB()) {
				facility.setIncABName("Yes");
			} else {
				facility.setIncABName("No");
			}

			if (facility.isCertified()) {
				facility.setCertifiedName("Yes");
			} else {
				facility.setCertifiedName("No");
			}

			if (facility.isPreCertified()) {
				facility.setPreCertifiedName("Yes");
			} else {
				facility.setPreCertifiedName("No");
			}

		}
		String[] col = new String[] { "First Name", "Last Name", "ISN", "Zip", "Inc AB", "Certified", "Pre Certified",
				"Inc Status", "Week Day Hours", "Saturday Hours", "Sunday Hours", "Phone", "Mobile Phone",
				"Busienss Phone", "Business Name", "Email" };
		System.out.println("list-------------------------" + list);
		return writeCSV(col, "facility_report_", list, FacilityVM.class, fileType);
	}

	public <T> File writeCSV(String[] col, String reportName, List<T> responseList, Class<T> type, String fileType) {

		try {
			File dir = new File("csv_reports");
			if (!dir.exists()) {
				dir.mkdirs();
			}
			String fileName;
			if (fileType.equalsIgnoreCase("csv")) {
				fileName = dir.getAbsolutePath() + "/" + reportName + UUID.randomUUID() + ".csv";
			} else {
				fileName = dir.getAbsolutePath() + "/" + reportName + UUID.randomUUID() + ".xlsx";
			}

			Writer writer = new FileWriter(fileName);
			// Write column heads
			writer.append(StringUtils.join(col, ",")).append("\n");
			ColumnPositionMappingStrategy<T> mappingStrategy = new ColumnPositionMappingStrategy<>();
			mappingStrategy.setType(type);

			StatefulBeanToCsv<T> sbc = new StatefulBeanToCsvBuilder<T>(writer).withMappingStrategy(mappingStrategy)
					.withSeparator(CSVWriter.DEFAULT_SEPARATOR).build();

			sbc.write(responseList);
			writer.close();

			return new File(fileName);
		} catch (Exception e) {
			log.debug("Exception while generating csv ", e);
		}
		return null;
	}

	@Override
	public List<PaymentReportVM> getPaymentReport(Date fromDate, Date toDate, String zip, String incStatus,
			String state, String county, String city, String incAB, String fname, String lname, String npn, String isn,
			String company, Date paymentDateSearch, String utype, String srep,
			List<CustomFilterSearchDTO> customFilterSearchDTOs, String generalSearch) {
		List<PaymentReportVM> paymentReport = new LinkedList<>();

//	    srep
		String uTypeNew = utype;

		if (zip == null) {
			zip = "";
		}
		zip = "%" + zip + "%";

		if (fname == null) {
			fname = "";
		}
		fname = "%" + fname + "%";

		if (lname == null) {
			lname = "";
		}
		lname = "%" + lname + "%";

		if (company == null) {
			company = "";
		}
		company = "%" + company + "%";

		if (utype == null) {
			utype = "";
		}
		utype = "%" + utype + "%";

		Object[][] agArr = customAgentRepository.getAgentPaymentRepot(zip, fname, lname, utype);
		if (agArr != null && agArr.length > 0) {

			for (Object[] arr : agArr) {
				try {
					paymentReport.add(CommonMapper.fromKeyVal(paymentReportKeys, arr, PaymentReportVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}
//			List<PaymentReportVM> paymentList = new ArrayList<>();
			for (PaymentReportVM paymentReportVM : paymentReport) {
				if (paymentReportVM.getSales_rep() == null) {
					paymentReportVM.setSales_rep("");
				}
				if (paymentReportVM.getIsn() == null) {
					paymentReportVM.setIsn("0");
				}
			}
//			paymentReport = paymentList;
		}

//		comapny
		for (PaymentReportVM paymentReportVM : paymentReport) {
			Optional<Address> addr = customAddressRepository.findById(paymentReportVM.getAddrId());
			if (addr.get() != null) {
				Address add = addr.get();
				paymentReportVM.setCityId(add.getCity() != null ? add.getCity().getId() : null);
				paymentReportVM.setCountyId(add.getCounty().getId() != null ? add.getCounty().getId() : null);
				paymentReportVM.setStateId(add.getState().getId() != null ? add.getState().getId() : null);
				Business b = businessRepository.findBusinessByAddrId(add.getId());
				paymentReportVM.setCompany(b.getBusinessName());

				if (add.getCounty() != null) {
					County co = customCountyRepository.findById(add.getCounty().getId()).get();
					City c = customCityRepository.findById(add.getCity().getId()).get();
					State s = customStateRepository.findById(add.getState().getId()).get();
					paymentReportVM.setCity(c.getName());
					paymentReportVM.setCounty(co.getName());
					paymentReportVM.setState(s.getName());
				}

			}

		}

		List<PaymentReportVM> paymentReportDateFilter = new LinkedList<>();
		int carrierCount = 0;

		for (PaymentReportVM report : paymentReport) {
			carrierCount = 0;
			carrierCount = customAgentRepository.findCountByProfileId(report.getProfileId());
			if (report.getPackageId().equals("1")) {

				if (carrierCount == 0) {
					report.setAmount(100 * report.getSlots() + "");
				} else if (carrierCount >= 1 && carrierCount <= 3) {
					report.setAmount(300 * report.getSlots() + "");
				} else if (carrierCount >= 4) {
					report.setAmount(400 * report.getSlots() + "");
				}

			} else if (report.getPackageId().equals("2")) {
				if (carrierCount == 0) {
					report.setAmount(50 * report.getSlots() + "");
				} else if (carrierCount >= 1 && carrierCount <= 3) {
					report.setAmount(150 * report.getSlots() + "");
				} else if (carrierCount >= 4) {
					report.setAmount(200 * report.getSlots() + "");
				}
			} else if (report.getPackageId().equals("3")) {
				report.setAmount(25 * report.getSlots() + "");

			}

			DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			Date paymentDate = null;
			try {
				paymentDate = format.parse(report.getPaymentDate());
			} catch (ParseException e) {
				System.out.println("Failed to parse date ........");
				return null;
			}

//			Date paymentDate = new Date(report.getPaymentDate());
			if (paymentDateSearch == null) {
				if (fromDate != null && toDate != null && paymentDate.before(toDate) && paymentDate.after(fromDate)) {
					paymentReportDateFilter.add(report);
				} else if (toDate != null && paymentDate.before(toDate)) {
					paymentReportDateFilter.add(report);
				} else if (fromDate != null && paymentDate.after(fromDate)) {
					paymentReportDateFilter.add(report);
				}
			}

			if (paymentDateSearch != null && paymentDateSearch.equals(paymentDate)) {
				paymentReportDateFilter.add(report);
			}

		}

		if (paymentReportDateFilter.size() > 0 || (fromDate != null || toDate != null || paymentDateSearch != null)) {
			paymentReport = paymentReportDateFilter;
		}

		if (incStatus != null && incStatus.equalsIgnoreCase("incAB")) {
			paymentReport = (List<PaymentReportVM>) paymentReport.stream()
					.filter(x -> "3".equalsIgnoreCase(x.getPackageId())).collect(Collectors.toList());
		} else if (incStatus != null && incStatus.equalsIgnoreCase("certified")) {
			paymentReport = (List<PaymentReportVM>) paymentReport.stream()
					.filter(x -> "1".equalsIgnoreCase(x.getPackageId())).collect(Collectors.toList());
		} else if (incStatus != null && incStatus.equalsIgnoreCase("preCertified")) {
			paymentReport = (List<PaymentReportVM>) paymentReport.stream()
					.filter(x -> "2".equalsIgnoreCase(x.getPackageId())).collect(Collectors.toList());
		}

		if (incAB != null && !incStatus.equalsIgnoreCase("")) {
			paymentReport = (List<PaymentReportVM>) paymentReport.stream()
					.filter(x -> "1".equalsIgnoreCase(x.getPackageId())).collect(Collectors.toList());
		}

		if (state != null && !state.isEmpty()) {
			paymentReport = (List<PaymentReportVM>) paymentReport.stream()
					.filter(x -> x.getState().toLowerCase().startsWith(state.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (county != null && !county.isEmpty()) {
			paymentReport = (List<PaymentReportVM>) paymentReport.stream()
					.filter(x -> x.getCounty().toLowerCase().startsWith(county.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (city != null && !city.isEmpty()) {
			paymentReport = (List<PaymentReportVM>) paymentReport.stream()
					.filter(x -> x.getCity().toLowerCase().startsWith(city.toLowerCase())).collect(Collectors.toList());
		}

		if (srep != null && !srep.isEmpty()) {

			paymentReport = (List<PaymentReportVM>) paymentReport.stream()
					.filter(x -> x.getSales_rep().toLowerCase().startsWith(srep.toLowerCase()))
					.collect(Collectors.toList());

		}

		/*
		 * if(npn!=null && npn!="" && (uTypeNew.equalsIgnoreCase("AGENT") ||
		 * uTypeNew.isEmpty()) ) { paymentReport = (List<PaymentReportVM>)
		 * paymentReport.stream().filter (x ->
		 * x.getNpn().toLowerCase().startsWith(npn.toLowerCase()))
		 * .collect(Collectors.toList());
		 * 
		 * }
		 * 
		 * if(isn!=null && isn!="" && (uTypeNew.equalsIgnoreCase("FACILITY") ||
		 * uTypeNew.isEmpty()) ) { paymentReport = (List<PaymentReportVM>)
		 * paymentReport.stream().filter (x ->
		 * x.getIsn().toLowerCase().startsWith(isn.toLowerCase()))
		 * .collect(Collectors.toList());
		 * 
		 * }
		 */

		if (generalSearch != null && !generalSearch.equalsIgnoreCase("")) {
			paymentReport = (List<PaymentReportVM>) paymentReport.stream()
					.filter(x -> (x.getAgentFirstName() != null
							&& x.getAgentFirstName().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getAgentLastName() != null
									&& x.getAgentLastName().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getNpn() != null && x.getNpn().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getIsn() != null && x.getIsn().toLowerCase().contains(generalSearch.toLowerCase())))
					.collect(Collectors.toList());
//		
		}

		if (customFilterSearchDTOs != null && customFilterSearchDTOs.size() > 0) {
//			 paymentReport.removeAll(agents);
			paymentReport = sortPaymentList(paymentReport, customFilterSearchDTOs);
		}

		return paymentReport;
	}

	private List<PaymentReportVM> sortPaymentList(List<PaymentReportVM> payments,
			List<CustomFilterSearchDTO> customFilterSearchDTOs) {

		List<PaymentReportVM> paymentList = new ArrayList<>();

//	        	agentsList  =  null;

		Comparator<PaymentReportVM> sortByLastName = (p, o) -> p.getAgentLastName()
				.compareToIgnoreCase(o.getAgentLastName());
		Comparator<PaymentReportVM> sortByFirstName = (p, o) -> p.getAgentFirstName()
				.compareToIgnoreCase(o.getAgentFirstName());
		Comparator<PaymentReportVM> sortByUType = (p, o) -> p.getUserType().compareToIgnoreCase(o.getUserType());
//	        	Comparator<PaymentReportVM> sortByPreAmount = (p, o) ->(Integer.parseInt(p.getAmount())).(Integer.parseInt(o.getAmount()));
		Comparator<PaymentReportVM> sortByPreAmount = (p, o) -> Double.compare(Double.parseDouble(p.getAmount()),
				Double.parseDouble(o.getAmount()));

//	        	Comparator<AgentVM> sortByEffDate = Comparator.comparing(AgentVM::getEffectiveDate, Comparator.nullsLast(Comparator.naturalOrder()));

		Comparator<PaymentReportVM> comp = null;
		for (CustomFilterSearchDTO sort : customFilterSearchDTOs) {

			if (sort.getColName().equalsIgnoreCase("amount")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByPreAmount;
					} else {
						comp = sortByPreAmount.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByPreAmount);
					} else {
						comp = comp.thenComparing(sortByPreAmount).reversed();
					}
				}
			}

			if (sort.getColName().equalsIgnoreCase("firstName")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByFirstName;
					} else {
						comp = sortByFirstName.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByFirstName);
					} else {
						comp = comp.thenComparing(sortByFirstName).reversed();
					}
				}
			}
			if (sort.getColName().equalsIgnoreCase("lastName")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByLastName;
					} else {
						comp = sortByLastName.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByLastName);
					} else {
						comp = comp.thenComparing(sortByLastName).reversed();
					}
				}
			}

			if (sort.getColName().equalsIgnoreCase("uType")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByUType;
					} else {
						comp = sortByUType.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByUType);
					} else {
						comp = comp.thenComparing(sortByUType).reversed();
					}
				}
			}

			if (sort.getColName().equalsIgnoreCase("paymentDate")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
//								comp = sortByEffDate;
						comp = Comparator.comparing(PaymentReportVM::getPaymentDate,
								Comparator.nullsLast(Comparator.naturalOrder()));
					} else {
//								comp = sortByEffDate.reversed();
						comp = Comparator.comparing(PaymentReportVM::getPaymentDate,
								Comparator.nullsLast(Comparator.reverseOrder()));
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(Comparator.comparing(PaymentReportVM::getPaymentDate,
								Comparator.nullsLast(Comparator.naturalOrder())));
					} else {
						comp = comp.thenComparing(Comparator.comparing(PaymentReportVM::getPaymentDate,
								Comparator.nullsLast(Comparator.reverseOrder()))).reversed();
					}
				}
			}
		}

		payments.stream().sorted(comp).forEach(agent -> paymentList.add(agent));

		return paymentList;
	}

	@Override
	public List<String> findIsnList(String search) {
		String likeSearch = search + "%";
		List<String> isnList = new ArrayList<>();
		isnList = customFacilityRepository.findAllIsnBySearch(likeSearch);
		return isnList;
	}

	@Override
	public List<SearchLogsVM> getLogReport(String zip, String phone, String searchType, String email, String fullName,
			String vehicleType, String vehicleYear, String county, String company, Date searchedDateConvert,
			Date toDate1, Date fromDate1, List<CustomFilterSearchDTO> customFilterSearchDTOs, String generalSearch) {

		List<SearchLogsVM> searchLogsList = new LinkedList<>();

		if (zip == null) {
			zip = "";
		}

		if (phone == null) {
			phone = "";
		}

		if (fullName == null) {
			fullName = "";
		}

		zip = "%" + zip + "%";
		phone = "%" + phone + "%";
		fullName = "%" + fullName + "%";

		Object[][] agArr = customSearchLogsRepository.getAdminLogReport(zip, phone, fullName);

		if (agArr != null && agArr.length > 0) {

			for (Object[] arr : agArr) {
				try {
					searchLogsList.add(CommonMapper.fromKeyVal(searchLogReportKeys, arr, SearchLogsVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}
		}

		for (SearchLogsVM report : searchLogsList) {
			if (report.getSearchType().equalsIgnoreCase("AGENT")) {
				report.setCompany("NA");
				report.setCounty("NA");
				report.setVehicleType("NA");
				report.setVehicleYear("NA");
			}

			if (report.getSearchType().equalsIgnoreCase("FACILITY")) {
				report.setCompany("NA");
			}

			if (report.getSearchType().equalsIgnoreCase("INSURANCE")) {
				report.setCounty("NA");
				report.setVehicleType("NA");
				report.setVehicleYear("NA");
			}

		}

		if (email != null && !email.isEmpty()) {
			searchLogsList = (List<SearchLogsVM>) searchLogsList.stream()
					.filter(x -> x.getEmail().toLowerCase().startsWith(email.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (vehicleType != null && !vehicleType.isEmpty()) {
			searchLogsList = (List<SearchLogsVM>) searchLogsList.stream()
					.filter(x -> x.getVehicleType().toLowerCase().startsWith(vehicleType.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (vehicleYear != null && !vehicleYear.isEmpty()) {
			searchLogsList = (List<SearchLogsVM>) searchLogsList.stream()
					.filter(x -> x.getVehicleYear().toLowerCase().startsWith(vehicleYear.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (county != null && !county.isEmpty()) {
			searchLogsList = (List<SearchLogsVM>) searchLogsList.stream()
					.filter(x -> x.getCounty().toLowerCase().startsWith(county.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (company != null && !company.isEmpty()) {
			searchLogsList = (List<SearchLogsVM>) searchLogsList.stream()
					.filter(x -> x.getCompany().toLowerCase().startsWith(company.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (searchType != null && !searchType.isEmpty()) {
			String val = "";
			if (searchType.equalsIgnoreCase("AGENT")) {
				val = "AGENT";

			} else if (searchType.equalsIgnoreCase("FACILITY")) {
				val = "FACILITY";

			} else if (searchType.equalsIgnoreCase("INSURANCE")) {
				val = "INSURANCE";
			}

			List<SearchLogsVM> sList = new ArrayList<>();
			for (SearchLogsVM report : searchLogsList) {

				if (report.getSearchType().equalsIgnoreCase(val)) {
					sList.add(report);
				}
			}
			searchLogsList = sList;
		}

		if (searchedDateConvert != null) {
			List<SearchLogsVM> sList = new ArrayList<>();
			for (SearchLogsVM report : searchLogsList) {

				DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
				Date searchedDate = null;
				try {
					searchedDate = format.parse(report.getSearchedDate());
				} catch (ParseException e) {
					System.out.println("Failed to parse date ........");
					return null;
				}
				System.out.println(searchedDate + "searchedDateConvert----------------" + searchedDateConvert);
				if (searchedDateConvert.equals(searchedDate)) {
					sList.add(report);
				}
			}
			searchLogsList = sList;

		}

		if (toDate1 != null || fromDate1 != null) {
			List<SearchLogsVM> sList = new ArrayList<>();
			for (SearchLogsVM report : searchLogsList) {

				DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
				Date searchedDate = null;
				try {
					searchedDate = format.parse(report.getSearchedDate());
				} catch (ParseException e) {
					System.out.println("Failed to parse date ........");
					return null;
				}
				if (toDate1 != null && fromDate1 != null) {
					if (searchedDate.after(fromDate1) && searchedDate.before(toDate1)) {
						sList.add(report);
					}
				}

				if (toDate1 != null && fromDate1 == null) {
					if (searchedDate.before(toDate1)) {
						sList.add(report);
					}
				}

				if (toDate1 == null && fromDate1 != null) {
					if (searchedDate.after(fromDate1)) {
						sList.add(report);
					}
				}

			}
			searchLogsList = sList;
		}

		if (generalSearch != null && !generalSearch.equalsIgnoreCase("")) {
			searchLogsList = (List<SearchLogsVM>) searchLogsList.stream()
					.filter(x -> (x.getFullName() != null
							&& x.getFullName().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getZip() != null && x.getZip().toLowerCase().contains(generalSearch.toLowerCase())))
					.collect(Collectors.toList());
//		
		}
		if (customFilterSearchDTOs != null && customFilterSearchDTOs.size() > 0) {
//				 paymentReport.removeAll(agents);
			searchLogsList = sortLogList(searchLogsList, customFilterSearchDTOs);
		}

		return searchLogsList;
	}

	private List<SearchLogsVM> sortLogList(List<SearchLogsVM> searchRecords,
			List<CustomFilterSearchDTO> customFilterSearchDTOs) {

		List<SearchLogsVM> logList = new ArrayList<>();

//	        	agentsList  =  null;

		Comparator<SearchLogsVM> sortByZip = (p, o) -> p.getZip().compareToIgnoreCase(o.getZip());
		Comparator<SearchLogsVM> sortByFullName = (p, o) -> p.getFullName().compareToIgnoreCase(o.getFullName());
		Comparator<SearchLogsVM> sortBySearchType = (p, o) -> p.getSearchType().compareToIgnoreCase(o.getSearchType());

		Comparator<SearchLogsVM> comp = null;
		for (CustomFilterSearchDTO sort : customFilterSearchDTOs) {

			if (sort.getColName().equalsIgnoreCase("name")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByFullName;
					} else {
						comp = sortByFullName.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByFullName);
					} else {
						comp = comp.thenComparing(sortByFullName).reversed();
					}
				}
			}

			if (sort.getColName().equalsIgnoreCase("searchType")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortBySearchType;
					} else {
						comp = sortBySearchType.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortBySearchType);
					} else {
						comp = comp.thenComparing(sortBySearchType).reversed();
					}
				}
			}
			if (sort.getColName().equalsIgnoreCase("zip")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByZip;
					} else {
						comp = sortByZip.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByZip);
					} else {
						comp = comp.thenComparing(sortByZip).reversed();
					}
				}
			}

			if (sort.getColName().equalsIgnoreCase("searchedDate")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
//								comp = sortByEffDate;
						comp = Comparator.comparing(SearchLogsVM::getSearchedDate,
								Comparator.nullsLast(Comparator.naturalOrder()));
					} else {
//								comp = sortByEffDate.reversed();
						comp = Comparator.comparing(SearchLogsVM::getSearchedDate,
								Comparator.nullsLast(Comparator.reverseOrder()));
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(Comparator.comparing(SearchLogsVM::getSearchedDate,
								Comparator.nullsLast(Comparator.naturalOrder())));
					} else {
						comp = comp.thenComparing(Comparator.comparing(SearchLogsVM::getSearchedDate,
								Comparator.nullsLast(Comparator.reverseOrder()))).reversed();
					}
				}
			}
		}

		searchRecords.stream().sorted(comp).forEach(agent -> logList.add(agent));

		return logList;
	}

	@Override
	public List<String> findSearchNameList(String search) {
		String likeSearch = search + "%";
		List<String> nameList = new ArrayList<>();
		nameList = customSearchLogsRepository.findSearchUsersName(likeSearch);
		return nameList;
	}

	@Override
	public List<String> findSearchEmailList(String search) {
		String likeSearch = search + "%";
		List<String> nameList = new ArrayList<>();
		nameList = customSearchLogsRepository.findSearchEmail(likeSearch);
		return nameList;
	}

	@Override
	public List<String> findSearchPhonelList(String search) {
		String likeSearch = search + "%";
		List<String> nameList = new ArrayList<>();
		nameList = customSearchLogsRepository.findSearchPhone(likeSearch);
		return nameList;
	}

	@Override
	public List<String> findSearchZiplList(String search) {
		String likeSearch = search + "%";
		List<String> nameList = new ArrayList<>();
		nameList = customSearchLogsRepository.findSearchZip(likeSearch);
		return nameList;
	}

	@Override
	public List<String> findSearchCountylList(String search) {
		String likeSearch = search + "%";
		List<String> nameList = new ArrayList<>();
		nameList = customSearchLogsRepository.findSearchCounty(likeSearch);
		return nameList;
	}

	@Override
	public List<String> findSearchCompanylList(String search) {
		String likeSearch = search + "%";
		List<String> nameList = new ArrayList<>();
		nameList = customSearchLogsRepository.findSearchCompany(likeSearch);
		return nameList;
	}

	@Override
	public List<String> findSearchVehicleYearList(String search) {
		String likeSearch = search + "%";
		List<String> nameList = new ArrayList<>();
		nameList = customSearchLogsRepository.findSearchVehicleYear(likeSearch);
		return nameList;
	}

	@Override
	public List<FacilityVM> getAllFacilitiesByFilter(String city, String zip, String incStatus, String incAB,
			String fName, String lName, String isn, String county, String state, String bName,
			List<CustomFilterSearchDTO> customFilterSearchDTOs, String generalSearch) {

		List<FacilityVM> list = new LinkedList<>();
		String type = "%" + "FACILITY" + "%";

		Object[][] data = customFacilityRepository.getAllFacilitiesWithFilter();
		Object[][] agArr1 = customFacilityRepository.getAllSignUpOnlyFacility(type);

		if (data != null && data.length > 0) {
			System.out.println("length------------------------" + data[0].length);

			for (Object[] arr : data) {
				try {

					list.add(CommonMapper.fromKeyVal(FacilityVM.getKeys(), arr, FacilityVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}

		}

		List<FacilityVM> facility1 = new LinkedList<>();
		/*
		 * if (agArr1 != null && agArr1.length > 0) { for (Object[] arr1 : agArr1) { try
		 * { facility1.add(CommonMapper.fromKeyVal(FacilityVM.getKeys1(), arr1,
		 * FacilityVM.class)); } catch (Exception e){
		 * log.debug("Exception while mapping all agents data ", e); } } }
		 */
		if (!facility1.isEmpty()) {
			list.addAll(facility1);
		}

		List<FacilityVM> facilityList = new ArrayList<>();
		facilityList.addAll(list);

		if (city != null && !city.isEmpty()) {
			facilityList = (List<FacilityVM>) facilityList.stream()
					.filter(x -> (x.getCity().toLowerCase()).startsWith(city.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (county != null && !county.isEmpty()) {
			List<FacilityVM> vm = new ArrayList<>();
			for (FacilityVM facility : facilityList) {
				if (facility != null && facility.getCounty() != null && facility.getCounty().startsWith(county)) {
					vm.add(facility);
				}
			}
			facilityList = vm;
			/*
			 * facilityList = (List<FacilityVM>) facilityList.stream().filter (x ->
			 * (x.getCounty().toLowerCase()).startsWith(county.toLowerCase()))
			 * .collect(Collectors.toList());
			 */
		}

		if (state != null && !state.isEmpty()) {
			facilityList = (List<FacilityVM>) facilityList.stream()
					.filter(x -> (x.getState().toLowerCase()).startsWith(state.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (bName != null && !bName.isEmpty()) {
			facilityList = (List<FacilityVM>) facilityList.stream()
					.filter(x -> (x.getName().toLowerCase()).startsWith(bName.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (zip != null && !zip.isEmpty()) {
			facilityList = (List<FacilityVM>) facilityList.stream()
					.filter(x -> (x.getZipCode().toLowerCase()).startsWith(zip.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (fName != null && !fName.isEmpty()) {
			facilityList = (List<FacilityVM>) facilityList.stream()
					.filter(x -> (x.getFirstName().toLowerCase()).contains(fName.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (lName != null && !lName.isEmpty()) {
			facilityList = (List<FacilityVM>) facilityList.stream()
					.filter(x -> (x.getLastName().toLowerCase()).contains(lName.toLowerCase()))
					.collect(Collectors.toList());
		}

		if (isn != null && !isn.isEmpty()) {
			facilityList = (List<FacilityVM>) facilityList.stream()
					.filter(x -> (x.getIsn().toLowerCase()).contains(isn.toLowerCase())).collect(Collectors.toList());
		}

		for (FacilityVM facility : facilityList) {

			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			if (facility.getPaymentDate() != null && facility.getPaymentDate() != "") {
				try {
					Date paymentDate = format.parse(facility.getPaymentDate());

					if (paymentDate.before(new Date())) {
						facility.setIncStatus("In-Active");
					}
					if (paymentDate.after(new Date())) {
						facility.setIncStatus("Active");
					}

				} catch (ParseException e) {
					System.out.println("Failed to parse date ........");
					return null;
				}
			} else {
				facility.setIncStatus("In-Active");
			}
		}

		if (incAB != null && incAB.equalsIgnoreCase("member")) {
			facilityList = (List<FacilityVM>) facilityList.stream().filter(x -> true == (x.isIncAB()))
					.collect(Collectors.toList());

		} else if (incAB != null && incAB.equalsIgnoreCase("certified")) {
			facilityList = (List<FacilityVM>) facilityList.stream().filter(x -> true == (x.isCertified()))
					.collect(Collectors.toList());

		} else if (incAB != null && incAB.equalsIgnoreCase("preCert")) {
			facilityList = (List<FacilityVM>) facilityList.stream().filter(x -> true == (x.isPreCertified()))
					.collect(Collectors.toList());
		}

		/*
		 * else if(incAB!=null && incAB.equalsIgnoreCase("qualified")) { agents =
		 * (List<AgentVM>) agents.stream().filter(x ->
		 * "Active".equalsIgnoreCase(x.getLicStatus())) .collect(Collectors.toList());
		 * 
		 * }else if(incAB!=null && incAB.equalsIgnoreCase("nonQualified")) { agents =
		 * (List<AgentVM>) agents.stream().filter(x ->
		 * "Inactive".equalsIgnoreCase(x.getLicStatus())) .collect(Collectors.toList());
		 * }
		 */

		for (FacilityVM agentVM : facilityList) {
			if (agentVM.isCertified() && agentVM.isCertified()) {
				agentVM.setIncABName("Certified");
			} else if (agentVM.isPreCertified() && agentVM.isPreCertified()) {
				agentVM.setIncABName("Pre-Certified");
			} else if (agentVM.isIncAB() && agentVM.isIncAB()) {
				agentVM.setIncABName("INC A/B");
			}
			/*
			 * else if(agentVM.getLicStatus()!=null &&
			 * agentVM.getLicStatus().equals("Active")) { agentVM.setIncABName("Qualified");
			 * }else { agentVM.setIncABName("Non Qualified"); }
			 */
		}

		List<FacilityVM> facilities = new ArrayList<>();
		facilities.addAll(facilityList);

		if (generalSearch != null && !generalSearch.equalsIgnoreCase("")) {
			facilities = (List<FacilityVM>) facilities.stream()
					.filter(x -> (x.getBusinessName() != null
							&& x.getBusinessName().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getIsn() != null && x.getIsn().toLowerCase().contains(generalSearch.toLowerCase())))
					.collect(Collectors.toList());
		}
//		(x.getLastName().toLowerCase().contains(generalSearch)) || 

		if (customFilterSearchDTOs != null && customFilterSearchDTOs.size() > 0) {
			facilities.removeAll(facilityList);
			facilities = sortFacilityList(facilityList, customFilterSearchDTOs);
		}

		return facilities;
	}

	private List<FacilityVM> sortFacilityList(List<FacilityVM> facilities,
			List<CustomFilterSearchDTO> customFilterSearchDTOs) {

		List<FacilityVM> facilityList = new ArrayList<>();

		Comparator<FacilityVM> sortByIsn = (p, o) -> ("" + p.getIsn()).compareToIgnoreCase("" + o.getIsn());
		Comparator<FacilityVM> sortByFirstName = (p, o) -> p.getFirstName().compareToIgnoreCase(o.getFirstName());
		Comparator<FacilityVM> sortByCertified = (p, o) -> ("" + p.isCertified())
				.compareToIgnoreCase("" + o.isCertified());
		Comparator<FacilityVM> sortByPreCertified = (p, o) -> ("" + p.isPreCertified())
				.compareToIgnoreCase("" + o.isPreCertified());

		Comparator<FacilityVM> comp = null;
		for (CustomFilterSearchDTO sort : customFilterSearchDTOs) {

			if (sort.getColName().equalsIgnoreCase("isn")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByIsn;
					} else {
						comp = sortByIsn.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByIsn);
					} else {
						comp = comp.thenComparing(sortByIsn).reversed();
					}

				}

			}
			if (sort.getColName().equalsIgnoreCase("firstName")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByFirstName;
					} else {
						comp = sortByFirstName.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByFirstName);
					} else {
						comp = comp.thenComparing(sortByFirstName).reversed();
					}
				}
			}
			if (sort.getColName().equalsIgnoreCase("certified")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByCertified;
					} else {
						comp = sortByCertified.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByCertified);
					} else {
						comp = comp.thenComparing(sortByCertified).reversed();
					}
				}
			}
			if (sort.getColName().equalsIgnoreCase("preCertified")) {
				if (comp == null) {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = sortByCertified;
					} else {
						comp = sortByCertified.reversed();
					}
				} else {
					if (sort.getSortType().equalsIgnoreCase("asc")) {
						comp = comp.thenComparing(sortByPreCertified);
					} else {
						comp = comp.thenComparing(sortByPreCertified).reversed();
					}
				}
			}

		}

		facilities.stream().sorted(comp).forEach(facility -> facilityList.add(facility));

		return facilityList;
	}

	@Override
	public List<String> getFacilityCities(String search) {
		String likeSearch = search + "%";
		String type = "%" + "FACILITY" + "%";
		List<String> cityList = new ArrayList<>();
		cityList = customFacilityRepository.findFacilityCity(type, likeSearch);
		return cityList;
	}

	@Override
	public List<String> getFacilityZip(String search) {
		String likeSearch = search + "%";
		String type = "%" + "FACILITY" + "%";
		List<String> zipList = new ArrayList<>();
		zipList = customFacilityRepository.findFacilityZip(type, likeSearch);
		return zipList;
	}

	@Override
	public List<String> getFacilityFname(String search) {
		String likeSearch = search + "%";
		String type = "%" + "FACILITY" + "%";
		List<String> nameList = new ArrayList<>();
		nameList = customFacilityRepository.findFacilityFname(type, likeSearch);
		return nameList;
	}

	@Override
	public List<String> getFacilityLname(String search) {
		String likeSearch = search + "%";
		String type = "%" + "FACILITY" + "%";
		List<String> nameList = new ArrayList<>();
		nameList = customFacilityRepository.findFacilityLname(type, likeSearch);
		return nameList;
	}

	@Override
	public List<String> getFacilityIsn(String search) {
		String likeSearch = search + "%";
		List<String> isnList = new ArrayList<>();
		isnList = customFacilityRepository.findFacilityIsn(likeSearch);
		return isnList;
	}

	@Override
	public List<String> getFacilityBusinessName(String search) {
		String likeSearch = "%" + search + "%";
		List<String> bList = new ArrayList<>();
		bList = customFacilityRepository.findFacilityBusiness(likeSearch);
		return bList;
	}

	@Override
	public List<String> getFacilityCountyName(String search) {
		String likeSearch = "%" + search + "%";
		List<String> cList = new ArrayList<>();
		String f = "%" + "FACILITY" + "%";
		cList = customAgentRepository.findCountyByStateAndSearch(likeSearch, f);
		return cList;
	}

	@Override
	public List<String> getFacilityStateName() {
		List<String> sList = new ArrayList<>();
		String f = "%" + "FACILITY" + "%";
		sList = customFacilityRepository.findFacilityState(f);
		return sList;
	}

	@Override
	public SearchPage getSearchAgentFacilitySlots(String type, String zip, Integer page, String company, String agent) {
		SearchPage sPage = new SearchPage();

		String cName = "%%";
		if (type.equalsIgnoreCase("AGENT") && company != null && !company.isEmpty()) {
			zip = zip;
			cName = company;
		}

		if (type.equalsIgnoreCase("AGENT") && agent != null && !agent.isEmpty()) {

			String ar[] = agent.split(" ");
			String fName = ar[0];
			String lName = ar[1];
			zip = zip; // no zip search for specific agent
			Object[][] data = customAgentRepository.findSerachAgentRecordsFromView(type, fName, lName);

			List<SearchVM> list = new ArrayList<SearchVM>();
			if (data != null && data.length > 0) {
				for (Object[] arr : data) {
					try {
						list.add(CommonMapper.fromKeyVal(SearchVM.getKeys(), arr, SearchVM.class));
					} catch (Exception e) {
						log.debug("Exception while mapping all agents data ", e);
					}
				}

				list = customAgentFilterSeviceImpl.getDistinctNPN(list);
				sPage.setResult(list);
				sPage.setTotal(list.size());
				sPage.setTotalPages((list.size()) / 20);
				for (SearchVM searchVM : sPage.getResult()) {

					/*
					 * String extension =
					 * customAgentRepository.getAgentExtensionFromNPN(searchVM.getNpn());
					 * searchVM.setExtension(extension);
					 */

					Agent a = customAgentRepository.findOneByNpn(searchVM.getNpn());
					AgentExtension ae = agentExtensionRepository.findByAgentAndPrimary(a.getId());

					if (a.getBusiness() != null && a.getBusiness().getId() != null) {
						Business business = businessRepository.findById(a.getBusiness().getId()).get();
						Address addrs = null;
						if (business != null && business.getAddress() != null
								&& business.getAddress().getId() != null) {
							addrs = customAddressRepository.findById(business.getAddress().getId()).get();
							searchVM.setAddr_1(addrs.getAddr1());
							searchVM.setAddr_2(addrs.getAddr2());
							searchVM.setAddr_3(addrs.getAddr3());
						}

					}

					if (ae != null) {
						searchVM.setExtension(ae.getExtension());
					} else {
						searchVM.setExtension("");
					}

					if (type.equalsIgnoreCase("AGENT")) {
//						Agent a =customAgentRepository.findOneByNpn(searchVM.getNpn());
						if (a != null && a.getProfile() != null) {
							List<ProfileCarriers> carr = profileCarriersRepository
									.findAllByProfileId(a.getProfile().getId());
							List<Long> clist = new ArrayList<>();
							for (ProfileCarriers c : carr) {
								if (!c.getIsPrimary()) {
									clist.add(c.getCarriersId());
								}
							}

							searchVM.setCarriers(clist);
						} else {
							searchVM.setCarriers(null);
						}

					}
				}

				if (page == 1 || page == 2) {
					int jj = 1;
					List<SearchVM> slist = sPage.getResult();
					for (SearchVM ele : slist) {
						ele.setSr(jj);
						System.out.println("sr -------------" + ele.getSr());
						jj++;
					}

					List<SearchVM> shuffledList = returnShuffledList(slist);
					sPage.setResult(shuffledList);

				}

				return sPage;
			} else {
				sPage.setResult(new ArrayList<>());
				sPage.setTotal(0);
				sPage.setTotalPages(0);
				return sPage;
			}
		}

		Object[][] data = null;
		if (type.equalsIgnoreCase("AGENT") && company != null && !company.isEmpty()) {
			data = customAgentRepository.findSerachRecordsByZipFromView(zip, cName);
		} else if (type.equalsIgnoreCase("AGENT")) {
			data = customAgentRepository.findSerachRecordsByZipFromView(zip);
		} else if (type.equalsIgnoreCase("FACILITY")) {
			data = customAgentRepository.findSerachFacilityRecordsByZipFromView(zip);
		}

		if ((data == null || (data != null && data.length == 0)) && company != null && !company.isEmpty()) {
			/** Got for default zip code , no agent avilable for zip **/
			// zip = "27512";
		}

		sPage = getSearchResultFor(type, zip, page, company);

		for (SearchVM searchVM : sPage.getResult()) {
			if (type.equalsIgnoreCase("FACILITY")) {
				String extension = customAgentRepository.getFacilityExtensionFromISN(searchVM.getNpn());
				searchVM.setExtension(extension);

			} else {

				/*
				 * String extension =
				 * customAgentRepository.getAgentExtensionFromNPN(searchVM.getNpn());
				 * searchVM.setExtension(extension);
				 */

				Agent a = customAgentRepository.findOneByNpn(searchVM.getNpn());

				if (a.getBusiness() != null && a.getBusiness().getId() != null) {
					Business business = businessRepository.findById(a.getBusiness().getId()).get();
					Address addrs = null;
					if (business != null && business.getAddress() != null && business.getAddress().getId() != null) {
						addrs = customAddressRepository.findById(business.getAddress().getId()).get();
						searchVM.setAddr_1(addrs.getAddr1());
						searchVM.setAddr_2(addrs.getAddr2());
						searchVM.setAddr_3(addrs.getAddr3());
					}

				}

				AgentExtension ae = agentExtensionRepository.findByAgentAndPrimary(a.getId());
				if (ae != null) {
					searchVM.setExtension(ae.getExtension());
				} else {
					searchVM.setExtension("");
				}

				if (type.equalsIgnoreCase("AGENT")) {
//					Agent a =customAgentRepository.findOneByNpn(searchVM.getNpn());
					if (a != null && a.getProfile() != null) {
						List<ProfileCarriers> carr = profileCarriersRepository
								.findAllByProfileId(a.getProfile().getId());
						List<Long> clist = new ArrayList<>();
						for (ProfileCarriers c : carr) {
							if (!c.getIsPrimary()) {
								clist.add(c.getCarriersId());
							}
						}

						searchVM.setCarriers(clist);
					} else {
						searchVM.setCarriers(null);
					}

				}
			}

		}
		sPage.setResult(sPage.getResult());

		System.out.println("result--------------------" + sPage.getResult());

		// shuffle
		if (page == 1 || page == 2) {
			int jj = 1;
			List<SearchVM> list = sPage.getResult();
			for (SearchVM ele : list) {
				ele.setSr(jj);
				System.out.println("sr -------------" + ele.getSr());
				jj++;
			}

			List<SearchVM> shuffledList = returnShuffledList(list);
			shuffledList = customAgentFilterSeviceImpl.getDistinctNPN(shuffledList);
			sPage.setResult(shuffledList);
		}

		return sPage;
	}

	private List<SearchVM> returnShuffledList(List<SearchVM> actualList) {

		List<SearchVM> searchVMs = new ArrayList<>();

		List<String> fullNamesList = actualList.stream().map(i -> i.getFirstName() + " " + i.getLastName())
				.collect(Collectors.toList());

		if (fullNamesList.size() == 10) {
			return actualList;
		} else {

			List<List<SearchVM>> setList = new ArrayList<>();

			for (String fullNameEle : fullNamesList) {
				List<SearchVM> set1 = new ArrayList<>();
				for (SearchVM vm : actualList) {

					if (vm != null) {
						String fullName = vm.getFirstName() + " " + vm.getLastName();
						if (fullNameEle.equals(fullName) && !vm.isAdded()) {
							vm.setAdded(true);
							set1.add(vm);
						}
					}

				}
				if (!set1.isEmpty()) {
					setList.add(set1);
				}
				/*
				 * for (SearchVM set : set1) {
				 * System.out.println("set-------"+set.getFirstName()); }
				 */

			}

			List<SearchVM> outputList = new ArrayList<>();

			int i = 0;
			for (int j = 0; j < setList.size(); j++) {
				if (setList.size() == 1) {
					outputList.addAll(actualList);
					break;
				}
//				System.out.println("set-------"+set.getFirstName());
				if (i < 3) {
					for (int k = 0; k < setList.get(j).size(); k++) {
						// outputList.add(sl);
						if (i < setList.get(j).size()) {
							SearchVM vm = setList.get(j).get(i);
							outputList.add(vm);
							break;
						}
					}
				} else {
					break;
				}

				if (j == (setList.size() - 1)) {
					j = -1;
					i++;
				}

			}

			actualList = outputList;

			System.out.println("final list------------------------" + outputList);

		}

		return actualList;
	}

	/*
	 * private List<SearchVM> returnShuffledList(List<SearchVM>
	 * actualList,List<SearchVM> searchVMs) {
	 * 
	 * 
	 * List<SearchVM> searchVMs1 = new ArrayList<>();
	 * 
	 * 
	 * 
	 * if(!actualList.isEmpty()) { Set<String> fullNames = actualList.stream().map(i
	 * -> i.getFirstName() + " " + i.getLastName()) .collect(Collectors.toSet());
	 * List<String> fullNamesList = new ArrayList<>(fullNames);
	 * 
	 * ListIterator<SearchVM> iter = actualList.listIterator();
	 * 
	 * 
	 * List<SearchVM> removalList = new ArrayList<>();
	 * 
	 * boolean newFlag = true; for (String fullNameEle : fullNamesList) {
	 * 
	 * for (SearchVM ele : actualList) { String fullName = ele.getFirstName() + " "
	 * + ele.getLastName(); newFlag = false; if (fullName.equals(fullNameEle)) {
	 * boolean flag = false; for(SearchVM svm: searchVMs1) { String fn =
	 * svm.getFirstName() + " " + svm.getLastName(); if(fn.equals(fullName)) { flag
	 * = true; } } if(!flag ) { searchVMs1.add(ele); removalList.add(ele); }
	 * 
	 * } } }
	 * 
	 * searchVMs.addAll(searchVMs1);
	 * 
	 * newFlag = false; // actualList.removeAll(removalList);
	 * 
	 * List<SearchVM> list1 = new ArrayList<>(); for (SearchVM searchVM :
	 * actualList) { boolean flag = false; for (SearchVM ele : removalList) {
	 * if(ele.getSr() == searchVM.getSr()) { flag = true; break; } }
	 * 
	 * if(flag == false) list1.add(searchVM);
	 * 
	 * } actualList = list1;
	 * 
	 * 
	 * while(actualList.size() != 0) { returnShuffledList(actualList,searchVMs); }
	 * 
	 * // while(iter.hasNext()){ // String fullName = iter.next().getFirstName() +
	 * " " + iter.next().getLastName(); // String iterFullName =
	 * fullNamesList.get(elementIndex); // // if(fullName.equals(iterFullName)){ //
	 * searchVMs.add(iter.next()); // // if(elementIndex == fullNamesList.size()) //
	 * elementIndex = 0; // else // elementIndex++; // // iter.remove(); // } // //
	 * // }
	 * 
	 * 
	 * }
	 * 
	 * System.out.println("After shuffling..." + searchVMs.size());
	 * searchVMs.forEach(System.out::println);
	 * 
	 * return searchVMs; }
	 */

	public SearchPage getSearchResultFor(String type, String zip, Integer page, String company) {
		int listSize = 0;
		SearchPage spage = new SearchPage();
		Object[][] data = null;

		String cName = "%%";
		if (type.equalsIgnoreCase("AGENT") && company != null && !company.isEmpty()) {
		}
		cName = company;

		if (type.equalsIgnoreCase("AGENT") && company != null && !company.isEmpty()) {
			data = customAgentRepository.findSerachRecordsFromView(zip, type, cName);
		} else if (type.equalsIgnoreCase("AGENT")) {
			data = customAgentRepository.findSerachRecordsFromView(zip, type);
		} else if (type.equalsIgnoreCase("FACILITY")) {
			data = customAgentRepository.findSerachFacilityRecordsFromView(zip, type);
		}

		List<SearchVM> list = new ArrayList<SearchVM>();
		List<SearchVM> originalList = new ArrayList<SearchVM>();
		if (data != null && data.length > 0) {
			for (Object[] arr : data) {
				try {
					if (type.equalsIgnoreCase("FACILITY")) {
						list.add(CommonMapper.fromKeyVal(SearchVM.getFaclilitykeys(), arr, SearchVM.class));
					} else {

						list.add(CommonMapper.fromKeyVal(SearchVM.getKeys(), arr, SearchVM.class));
					}

				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}
		} else {
			spage.setResult(new ArrayList<>());
			spage.setTotal(0);
			spage.setTotalPages(0);
			return spage;
		}

		for (SearchVM searchVM : list) {
			if (searchVM.getPref_company() == null) {
				searchVM.setPref_company("");
			}
			if (searchVM.getComments() == null) {
				searchVM.setComments("");
			}
		}

		list = addSlotsDataToList(list);
		originalList = list;
		listSize = originalList.size();
		System.out.println("list================" + list);

		int count1 = 0;
		int count2 = 0;

		if (type.equalsIgnoreCase("AGENT") && company != null && !company.isEmpty()) {
			count1 = customAgentRepository.findSerachCountForPage1FromViewCompany(cName, zip);
			count2 = customAgentRepository.findSerachCountForPage2FromViewCompany(cName, zip);
		} else if (type.equalsIgnoreCase("AGENT")) {
			count1 = customAgentRepository.findSerachCountForPage1FromView(zip);
			count2 = customAgentRepository.findSerachCountForPage2FromView(zip);
		} else if (type.equalsIgnoreCase("FACILITY")) {
			count1 = customAgentRepository.findSerachFacilityCountForPage1FromView(zip);
			count2 = customAgentRepository.findSerachFacilityCountForPage2FromView(zip);
		}

		originalList = customAgentFilterSeviceImpl.getDistinctNPN(originalList);
		if (page == 1 && (count1 > 0)) {
			list = getPageSlotsData(list, "1", zip);
			spage.setResult(list);
			spage.setTotal(list.size());
			spage.setTotalPages(getTotalPages(originalList, zip, true));
			return spage;
		}

		if (page == 1 && count1 == 0 && count2 > 0) {
			list = getPageSlotsData(list, "2", zip);
			spage.setResult(list);
			spage.setTotal(list.size());
			spage.setTotalPages(getTotalPages(originalList, zip, true));
			return spage;
		}

		if (page == 2 && (count2 > 0) && (count1 > 0)) {
			list = getPageSlotsData(list, "2", zip);
			spage.setResult(list);
			spage.setTotal(list.size());
			spage.setTotalPages(getTotalPages(originalList, zip, true));
			return spage;
		}

		// To get distinct list elements :-23 Aug 2019

		list = customAgentFilterSeviceImpl.getDistinctNPN(list);
		spage.setTotal(list.size());
		// originalList = list;

		if (page >= 2
				&& (((count1 > 0) && (count2 > 0)) || ((count2 == 0 && count1 > 0)) || ((count2 > 0 && count1 == 0)))) {
			List<SearchVM> list1 = new ArrayList<SearchVM>();
			for (int i = 0; i < list.size(); i++) {
				if (!(list.get(i) != null && list.get(i).getPackageId() != null && list.get(i).getBuyZip() != null
						&& list.get(i).getBuyZip().equals(zip)
						&& (list.get(i).getPackageId().equals("1") || list.get(i).getPackageId().equals("2")))) {
					SearchVM agentOutput = new SearchVM();
					String npn = list.get(i).getNpn();
					System.out.println("L-NPN : " + npn);
					agentOutput = list.get(i);
					list1.add(agentOutput);
				}
			}
			// gives start and end index of records
			list = list1;
			if (page == 2) {
				list = getPageDataNew(list, page - 1);
			} else {
				if (count2 != 0 && page == 3) {
					list = getPageDataNew(list, page - 2);
				} else {
					list = getPageDataNew(list, page - 1);
				}
			}

			spage.setResult(list);
			spage.setTotalPages(getTotalPages(originalList, zip, true));
			return spage;

		}

		int countData = 0;
		if (count1 == 0 && count2 > 0) {
			countData = 0;
			list = getPageData(list, page - 2, countData);
			spage.setTotalPages(getTotalPages(originalList, zip, false) + 2);

		} else {

			countData = 10;

			int page1Count = getPage1Count(list, "1");
			int page2Count = getPage1Count(list, "2");

			if (page1Count > 10) {
				lastIndex = 10;
			}
			if (page1Count <= 10) {
				lastIndex = page1Count;
			}
			if (page1Count <= 10 && page2Count >= 20) {
				lastIndex = 20;
			}
			if (page1Count <= 10 && page2Count < 20) {
				lastIndex = page1Count + page2Count;
			}

			if (page == 1 && page1Count > 10) {
				list = getPageData(list, page, 0);

			} else if (page == 1 && page1Count <= 10 && page1Count != 0) {
				list = list.subList(0, page1Count);
			} else if (page == 2 && page1Count <= 10 && page2Count >= 20 && page2Count != 0) {
				list = list.subList(page1Count, 20);
			} else if (page == 2 && page1Count <= 10 && page2Count < 20 && page2Count != 0) {
				list = list.subList(page1Count, page1Count + page2Count);

			} else {
				int page1 = 1;
				if (page1Count <= 10) {
					page1 = 2;
				}

				if (count1 == 0 && count2 == 0) {
					list = getPageData(list, page, 10);
					spage.setTotalPages(getTotalPagesWithoutZip(originalList));
				} else {
					list = getPageDataNewForZip(list, lastIndex - 1, page - page1);
					spage.setTotalPages(getTotalPagesForZip(originalList, page1Count, page2Count));
				}

			}

			if (zip.isEmpty() && company != null && !company.isEmpty()) {
				spage.setTotalPages(getTotalPagesForZip(originalList, page1Count, page2Count));
			}

		}

		if (zip.isEmpty() && (company == null || company.isEmpty())) {
			spage.setTotalPages(getTotalPagesWithoutZip(originalList));
		}

		// list = customAgentFilterSeviceImpl.getDistinctNPN(list);
		spage.setResult(list);
		return spage;

	}

	public int getTotalPagesWithoutZip(List<SearchVM> outputList) {

		double size = outputList.size() - 10;
		System.out.println((size / 20) + "========size------------------------------" + size);
		return (int) java.lang.Math.ceil((size / 20)) + 1;

	}

	public int getTotalPagesForZip(List<SearchVM> outputList, int page1Count, int page2Count) {

		double size = outputList.size() - (page1Count + page2Count);
		System.out.println((size / 20) + "========size------------------------------" + size);
		return (int) java.lang.Math.ceil((size / 20)) + 2;

	}

	private List<SearchVM> getPageDataNewForZip(List<SearchVM> outputList, int lastIndex, int page) {

		List<SearchVM> list1 = new ArrayList<>();
		for (int i = 0; i < outputList.size(); i++) {
			if (i > lastIndex) {
				list1.add(outputList.get(i));
			}
		}

		outputList = list1;

		int pageNew = page;
		if (pageNew == 0) {
			pageNew = 1;
		}
		int end = (pageNew * 20);
		int start = end - 20;
		int size = outputList.size();

		if (size > start) {
			if (end >= size) {
				end = size;
			}

			return outputList.subList(start, end);
		}
		return new ArrayList<>();
	}

	int getPage1Count(List<SearchVM> list, String pageid) {
		List<SearchVM> list1 = new ArrayList<>();
		int count = 0;
		String pCompany = "";
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getPackageId() != null && list.get(i).getPackageId().equals(pageid)
					&& list.get(i).getBuyZip().equals(list.get(i).getZip())) {

				SearchVM vm = list.get(i);
				SearchVM agentOutput = new SearchVM();
				pCompany = customAgentRepository.getInsuranceCompanyByNpn(vm.getNpn());
				agentOutput.setFirstName(vm.getFirstName());
				agentOutput.setbName(vm.getbName());
				agentOutput.setBuyZip(vm.getBuyZip());
				agentOutput.setCe_compliance(vm.getCe_compliance());
				agentOutput.setCity(vm.getCity());
				agentOutput.setComments(vm.getComments());
				agentOutput.setExtension(vm.getExtension());
				agentOutput.setNpn(vm.getNpn());
				agentOutput.setPackageId(vm.getPackageId());
				agentOutput.setPhone(vm.getPhone());
				agentOutput.setSlots(vm.getSlots());
				agentOutput.setZip(vm.getZip());
				agentOutput.setLastName(vm.getLastName());
				agentOutput.setFullname(vm.getFirstName() + " " + vm.getLastName());
				agentOutput.setPref_company(vm.getPref_company());
				agentOutput.setState(vm.getState());
				agentOutput.setInsurance_company(pCompany);
				agentOutput.setAddr_1(vm.getAddr_1());
				agentOutput.setAddr_2(vm.getAddr_2());
				agentOutput.setAddr_3(vm.getAddr_3());
				list1.add(agentOutput);
				count = count + 1;
			}
		}
		return count;

	}

	public int getTotalPages(List<SearchVM> outputList, String zip, boolean zipFlag) {
		List<SearchVM> list1 = getPageSlotsData(outputList, "1", zip);
		List<SearchVM> list2 = getPageSlotsData(outputList, "2", zip);

		double size = outputList.size() - (list1.size() + list2.size());
		System.out.println((size / 20) + "========size------------------------------" + size);
		if (zipFlag) {
			if (list1.size() == 0 || list2.size() == 0) {
				return (int) java.lang.Math.ceil((size / 20)) + 1;
			} else {
				return (int) java.lang.Math.ceil((size / 20)) + 2;
			}
		} else {
			return (int) java.lang.Math.ceil(((size + 10) / 20));
		}

	}

	private List<SearchVM> getPageDataNew(List<SearchVM> outputList, Integer page) {
		int pageNew = page;
		int end = (pageNew * 20);
		int start = end - 20;
		int size = outputList.size();

		if (size > start) {
			if (end >= size) {
				end = size;
			}

			return outputList.subList(start, end);
		}
		return new ArrayList<>();
	}

	private List<SearchVM> getPageData(List<SearchVM> outputList, Integer page, int countData) {
		int pageNew = page;
		int end = (pageNew * 20) - countData;
		int start = end - 20;

		int size = outputList.size();
		if (page == 1) {
			start = 0;
			if (outputList.size() >= 10) {
				end = 10;
			} else {
				end = outputList.size();
			}

			return outputList.subList(start, end);
		}

		if (size > start) {
			if (end >= size) {
				end = size;
			}

			return outputList.subList(start, end);
		}
		return new ArrayList<>();
	}

	List<SearchVM> getPageSlotsData(List<SearchVM> list, String pageId, String zip) {
		List<SearchVM> list1 = new ArrayList<>();
		String pCompany = "";
		for (int i = 0; i < list.size(); i++) {
			if (zip != null && !zip.isEmpty()) {
				if (list.get(i) != null && list.get(i).getPackageId() != null && list.get(i).getBuyZip() != null
						&& list.get(i).getPackageId().equals(pageId) && list.get(i).getBuyZip().equals(zip)) {
					SearchVM vm = list.get(i);

					SearchVM agentOutput = new SearchVM();
					pCompany = customAgentRepository.getInsuranceCompanyByNpn(vm.getNpn());
					agentOutput.setFirstName(vm.getFirstName());
					agentOutput.setbName(vm.getbName());
					agentOutput.setBuyZip(vm.getBuyZip());
					agentOutput.setCe_compliance(vm.getCe_compliance());
					agentOutput.setCity(vm.getCity());
					agentOutput.setComments(vm.getComments());
					agentOutput.setExtension(vm.getExtension());
					agentOutput.setNpn(vm.getNpn());
					agentOutput.setPackageId(vm.getPackageId());
					agentOutput.setPhone(vm.getPhone());
					agentOutput.setSlots(vm.getSlots());
					agentOutput.setZip(vm.getZip());
					agentOutput.setLastName(vm.getLastName());
					agentOutput.setFullname(vm.getFirstName() + " " + vm.getLastName());
					agentOutput.setPref_company(vm.getPref_company());
					agentOutput.setState(vm.getState());
					agentOutput.setInsurance_company(pCompany);
					agentOutput.setAddr_1(vm.getAddr_1());
					agentOutput.setAddr_2(vm.getAddr_2());
					agentOutput.setAddr_3(vm.getAddr_3());
					list1.add(agentOutput);
				}
			} else {
				if (list.get(i) != null && list.get(i).getPackageId() != null && list.get(i).getBuyZip() != null
						&& list.get(i).getPackageId().equals(pageId)) {
					SearchVM vm = list.get(i);

					SearchVM agentOutput = new SearchVM();
					pCompany = customAgentRepository.getInsuranceCompanyByNpn(vm.getNpn());
					agentOutput.setFirstName(vm.getFirstName());
					agentOutput.setbName(vm.getbName());
					agentOutput.setBuyZip(vm.getBuyZip());
					agentOutput.setCe_compliance(vm.getCe_compliance());
					agentOutput.setCity(vm.getCity());
					agentOutput.setComments(vm.getComments());
					agentOutput.setExtension(vm.getExtension());
					agentOutput.setNpn(vm.getNpn());
					agentOutput.setPackageId(vm.getPackageId());
					agentOutput.setPhone(vm.getPhone());
					agentOutput.setSlots(vm.getSlots());
					agentOutput.setZip(vm.getZip());
					agentOutput.setLastName(vm.getLastName());
					agentOutput.setFullname(vm.getFirstName() + " " + vm.getLastName());
					agentOutput.setPref_company(vm.getPref_company());
					agentOutput.setState(vm.getState());
					agentOutput.setInsurance_company(pCompany);
					agentOutput.setAddr_1(vm.getAddr_1());
					agentOutput.setAddr_2(vm.getAddr_2());
					agentOutput.setAddr_3(vm.getAddr_3());

					list1.add(agentOutput);
				}
			}

		}
		return list1;
	}

	/*
	 * public SearchVM setData(SearchVM vm) { SearchVM agentOutput = new SearchVM();
	 * agentOutput.setFirstName(vm.getFirstName());
	 * agentOutput.setbName(vm.getbName()); agentOutput.setBuyZip(vm.getBuyZip());
	 * agentOutput.setCe_compliance(vm.getCe_compliance());
	 * agentOutput.setCity(vm.getCity()); agentOutput.setComments(vm.getComments());
	 * agentOutput.setExtension(vm.getExtension()); agentOutput.setNpn(vm.getNpn());
	 * agentOutput.setPackageId(vm.getPackageId());
	 * agentOutput.setPhone(vm.getPhone()); agentOutput.setSlots(vm.getSlots());
	 * agentOutput.setZip(vm.getZip()); agentOutput.setLastName(vm.getLastName());
	 * agentOutput.setFullname(vm.getFirstName()+" "+vm.getLastName());
	 * agentOutput.setPref_company(vm.getPref_company());
	 * agentOutput.setState(vm.getState()); return agentOutput; }
	 */

	List<SearchVM> addSlotsDataToList(List<SearchVM> outputList) {
		List<SearchVM> newList = new ArrayList<SearchVM>();
		String pCompany = "";
		for (SearchVM sub : outputList) {
			if (sub != null && sub.getSlots() != null) {
				int slots = Integer.parseInt(sub.getSlots());
				for (int i = 0; i < slots; i++) {

					SearchVM vm = sub;
					SearchVM agentOutput = new SearchVM();
					pCompany = customAgentRepository.getInsuranceCompanyByNpn(vm.getNpn());
					agentOutput.setFirstName(vm.getFirstName());
					agentOutput.setbName(vm.getbName());
					agentOutput.setBuyZip(vm.getBuyZip());
					agentOutput.setCe_compliance(vm.getCe_compliance());
					agentOutput.setCity(vm.getCity());
					agentOutput.setComments(vm.getComments());
					agentOutput.setExtension(vm.getExtension());
					agentOutput.setNpn(vm.getNpn());
					agentOutput.setPackageId(vm.getPackageId());
					agentOutput.setPhone(vm.getPhone());
					agentOutput.setSlots(vm.getSlots());
					agentOutput.setZip(vm.getZip());
					agentOutput.setLastName(vm.getLastName());
					agentOutput.setFullname(vm.getFirstName() + " " + vm.getLastName());
					agentOutput.setPref_company(vm.getPref_company());
					agentOutput.setState(vm.getState());
					agentOutput.setInsurance_company(pCompany);
					agentOutput.setAddr_1(vm.getAddr_1());
					agentOutput.setAddr_2(vm.getAddr_2());
					agentOutput.setAddr_3(vm.getAddr_3());
					newList.add(agentOutput);
				}

			} else {

				SearchVM vm = sub;
				SearchVM agentOutput = new SearchVM();
				pCompany = customAgentRepository.getInsuranceCompanyByNpn(vm.getNpn());
				agentOutput.setFirstName(vm.getFirstName());
				agentOutput.setbName(vm.getbName());
				agentOutput.setBuyZip(vm.getBuyZip());
				agentOutput.setCe_compliance(vm.getCe_compliance());
				agentOutput.setCity(vm.getCity());
				agentOutput.setComments(vm.getComments());
				agentOutput.setExtension(vm.getExtension());
				agentOutput.setNpn(vm.getNpn());
				agentOutput.setPackageId(vm.getPackageId());
				agentOutput.setPhone(vm.getPhone());
				agentOutput.setSlots(vm.getSlots());
				agentOutput.setZip(vm.getZip());
				agentOutput.setLastName(vm.getLastName());
				agentOutput.setFullname(vm.getFirstName() + " " + vm.getLastName());
				agentOutput.setPref_company(vm.getPref_company());
				agentOutput.setState(vm.getState());
				agentOutput.setInsurance_company(pCompany);
				agentOutput.setAddr_1(vm.getAddr_1());
				agentOutput.setAddr_2(vm.getAddr_2());
				agentOutput.setAddr_3(vm.getAddr_3());
				newList.add(agentOutput);
			}

		}
		return newList;
	}

	@Override
	public SearchPage getSearchOfInsurance(String type, String zip, Integer page, String company, String agent) {
		// TODO Auto-generated method stub
		SearchPage sPage = new SearchPage();

		Object[][] data = customAgentRepository.findSerachInsuranceRecords();

		List<SearchVM> list = new ArrayList<SearchVM>();
		if (data != null && data.length > 0) {
			for (Object[] arr : data) {
				try {
					list.add(CommonMapper.fromKeyVal(SearchVM.getKeys(), arr, SearchVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}

			List<SearchVM> list1 = new ArrayList<SearchVM>();
			List<SearchVM> agentOutput = new ArrayList<SearchVM>();
			if (zip != null && !zip.isEmpty()) {

				list1 = (List<SearchVM>) list.stream()
						.filter(x -> x.getZip().toLowerCase().equals(zip.toLowerCase())
								&& (x.getBuyZip() != null && !x.getBuyZip().toLowerCase().equals(zip.toLowerCase())))
						.collect(Collectors.toList());
				agentOutput = (List<SearchVM>) list.stream()
						.filter(x -> x.getBuyZip() != null && x.getBuyZip().toLowerCase().equals(zip.toLowerCase()))
						.collect(Collectors.toList());
			}

			List<SearchVM> list2 = new ArrayList<SearchVM>();
			if (company != null && !company.isEmpty()) {
				// List<SearchVM> agentOutput = new ArrayList<SearchVM>();
				list2 = (List<SearchVM>) list.stream()
						.filter(x -> (x.getbName() != null && x.getbName().toLowerCase().equals(company.toLowerCase()))
								|| (x.getPref_company() != null
										&& x.getPref_company().toLowerCase().equals(company.toLowerCase()))
								|| (x.getInsurance_company() != null
										&& x.getInsurance_company().toLowerCase().equals(company.toLowerCase())))
						.collect(Collectors.toList());

			}

			List<SearchVM> list3 = new ArrayList<SearchVM>();
			if (agent != null && !agent.isEmpty()) {
				// List<SearchVM> agentOutput = new ArrayList<SearchVM>();
				String arr[] = agent.split(" ");
				String fname = arr[0];
				String lname = arr[1];
				list3 = (List<SearchVM>) list.stream()
						.filter(x -> (x.getFirstName().toLowerCase().equals(fname.toLowerCase()))
								&& (x.getLastName().toLowerCase().equals(lname.toLowerCase())))
						.collect(Collectors.toList());

			}
			List<SearchVM> list4 = new ArrayList<SearchVM>();
			if (company != null && !company.isEmpty()) {
				List<CustomerService> cm = (List<CustomerService>) customerServiceRepository.findByCompanyName(company);
				if (cm != null) {
					for (CustomerService cs : cm) {
						SearchVM vm = new SearchVM();
						vm.setPref_company(cs.getCompanyName());
						vm.setInsurance_company(cs.getCompanyName());
						vm.setExtension(cs.getCustomerNumber());
						list4.add(vm);
					}
				}

			}

			list = list4;
			list.addAll(list3);
			list.addAll(list2);
			list.addAll(agentOutput);
			list.addAll(list1);
			/*
			 * list = list1; list.addAll(list2); list.addAll(list3);
			 */// Get distinct records
			List<SearchVM> tempList = new ArrayList<SearchVM>();
			tempList = customAgentFilterSeviceImpl.getDistinctNPN(list);
			list = tempList;

			/*
			 * if (company != null && !company.isEmpty()) { List<CustomerService> cm =
			 * (List<CustomerService>) customerServiceRepository.findByCompanyName(company);
			 * if (cm != null) { for (CustomerService cs : cm) { SearchVM vm = new
			 * SearchVM(); vm.setPref_company(cs.getCompanyName());
			 * vm.setInsurance_company(cs.getCompanyName());
			 * vm.setExtension(cs.getCustomerNumber()); list.add(vm); } }
			 * 
			 * }
			 */
			for (SearchVM searchVM : list) {
				/*
				 * String extension =
				 * customAgentRepository.getAgentExtensionFromNPN(searchVM.getNpn());
				 * searchVM.setExtension(extension);
				 */

				// Add check for wake county

				com.mv1.iaa.domain.Agent agentDB = customAgentRepository.findOneByNpn(searchVM.getNpn());

				if (agentDB != null && agentDB.getBusiness() != null && agentDB.getBusiness().getId() != null) {
					Business business = businessRepository.findById(agentDB.getBusiness().getId()).get();
					Address addrs = null;
					if (business != null && business.getAddress() != null && business.getAddress().getId() != null) {
						addrs = customAddressRepository.findById(business.getAddress().getId()).get();
						searchVM.setAddr_1(addrs.getAddr1());
						searchVM.setAddr_2(addrs.getAddr2());
						searchVM.setAddr_3(addrs.getAddr3());
					}

				}

				List<ExtensionCounty> counties = extensionCountyRepository.findAll();

				Long countyId = null;
				Boolean countyFound = false;
				if (agentDB != null && agentDB.getBusiness() != null && agentDB.getBusiness().getAddress() != null
						&& agentDB.getBusiness().getAddress().getCounty() != null) {
					countyId = agentDB.getBusiness().getAddress().getCounty().getId();
				}
				if (countyId != null) {
					Optional<County> c = customCountyRepository.findById(countyId);
					if (c.isPresent()) {
//						c.get().getName()
						for (ExtensionCounty extensionCounty : counties) {
							if (extensionCounty.getName().equalsIgnoreCase(c.get().getName())) {
								countyFound = true;
								break;
							}
						}
					}
				}

				Agent a = customAgentRepository.findOneByNpn(searchVM.getNpn());
				if (a != null) {
					AgentExtension ae = agentExtensionRepository.findByAgentAndSecondary(a.getId());
					if (ae != null && countyFound) {
						searchVM.setExtension(ae.getExtension());
					} else {
						ae = agentExtensionRepository.findByAgentAndPrimary(a.getId());
						if (ae != null) {
							searchVM.setExtension(ae.getExtension());
						} else {
							searchVM.setExtension("");
						}
					}
				}

			}

			sPage.setResult(list);
			sPage.setTotal(list.size());
			sPage.setTotalPages((int) (java.lang.Math.ceil((list.size())) / 20) + 1);

			return sPage;
		}

		return null;
	}

	// get distinct records-2Aug 2019

	List<SearchVM> getDistinctNPN(List<SearchVM> list) {
		List<SearchVM> list1 = new ArrayList<SearchVM>();

		for (SearchVM s : list) {
			String npn = s.getNpn();
			boolean isPresent = list1.stream()
					.anyMatch(n -> n.getNpn() != null && npn.trim().equalsIgnoreCase(n.getNpn().trim()));
			if (!isPresent) {
				list1.add(s);
			}
		}

		return list1;

	}

	/**
	 * get all company names from customer services 10 Sept 19
	 */

	@Override
	public SearchPage getSearchCustomerServiceByCompany(Integer page, String company) {
		SearchPage sPage = new SearchPage();
		List<SearchVM> list = new ArrayList<SearchVM>();
		List<SearchVM> tempList = new ArrayList<SearchVM>();
		if (company != null && !company.isEmpty()) {
			List<CustomerService> cm = (List<CustomerService>) customerServiceRepository.findByCompanyName(company);
			if (cm != null) {
				for (CustomerService cs : cm) {
					SearchVM vm = new SearchVM();
					vm.setPref_company(cs.getCompanyName());
					vm.setInsurance_company(cs.getCompanyName());
					vm.setExtension(cs.getCustomerNumber());
					list.add(vm);
				}
			}
		}
		tempList = customAgentFilterSeviceImpl.getDistinctNPN(list);
		list = tempList;
		sPage.setResult(list);
		sPage.setTotal(list.size());
		sPage.setTotalPages((int) (java.lang.Math.ceil((list.size())) / 20) + 1);
		return sPage;

	}

}
