package com.mv1.iaa.service;

import com.mv1.iaa.service.dto.DMVLocationDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing DMVLocation.
 */
public interface DMVLocationService {

    /**
     * Save a dMVLocation.
     *
     * @param dMVLocationDTO the entity to save
     * @return the persisted entity
     */
    DMVLocationDTO save(DMVLocationDTO dMVLocationDTO);

    /**
     * Get all the dMVLocations.
     *
     * @return the list of entities
     */
    List<DMVLocationDTO> findAll();


    /**
     * Get the "id" dMVLocation.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DMVLocationDTO> findOne(Long id);

    /**
     * Delete the "id" dMVLocation.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
