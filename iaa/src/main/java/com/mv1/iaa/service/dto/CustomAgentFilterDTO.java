package com.mv1.iaa.service.dto;

/**
 * 
 * @author snehal Search filter dto
 *
 */
public class CustomAgentFilterDTO {

	private Long id;
	private String name;
	private String itemName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

}
