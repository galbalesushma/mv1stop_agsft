package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.Business;
import com.mv1.iaa.service.dto.BusinessDTO;

/**
 * Mapper for the entity Business and its DTO BusinessDTO.
 */
@Mapper(componentModel = "spring", uses = {AddressMapper.class})
public interface BusinessMapper extends EntityMapper<BusinessDTO, Business> {

    @Mapping(source = "address.id", target = "addressId")
    BusinessDTO toDto(Business business);

    @Mapping(source = "addressId", target = "address")
    @Mapping(target = "agents", ignore = true)
    @Mapping(target = "facilities", ignore = true)
    Business toEntity(BusinessDTO businessDTO);

    default Business fromId(Long id) {
        if (id == null) {
            return null;
        }
        Business business = new Business();
        business.setId(id);
        return business;
    }
}
