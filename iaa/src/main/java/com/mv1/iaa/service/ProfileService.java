package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import com.mv1.iaa.service.dto.ProfileDTO;

/**
 * Service Interface for managing Profile.
 */
public interface ProfileService {

    /**
     * Save a profile.
     *
     * @param profileDTO the entity to save
     * @return the persisted entity
     * @throws Exception 
     */
    ProfileDTO save(ProfileDTO profileDTO) throws Exception;

    /**
     * Get all the profiles.
     *
     * @return the list of entities
     */
    List<ProfileDTO> findAll();


    /**
     * Get the "id" profile.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ProfileDTO> findOne(Long id);

    /**
     * Delete the "id" profile.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
