package com.mv1.iaa.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.runtime.directive.Foreach;
import org.asteriskjava.manager.ManagerConnection;
import org.asteriskjava.manager.ManagerConnectionFactory;
import org.asteriskjava.manager.action.DbPutAction;
import org.asteriskjava.manager.response.ManagerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.codahale.metrics.annotation.Timed;
import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.business.common.Constants;
import com.mv1.iaa.business.common.DirectoryPath;
import com.mv1.iaa.business.common.Util;
import com.mv1.iaa.business.common.fileDirUtility;
import com.mv1.iaa.business.exception.Mv1Exception;
import com.mv1.iaa.business.view.csv.CdrBean;
import com.mv1.iaa.business.view.csv.CsvResp;
import com.mv1.iaa.business.view.web.req.AgentCallLogVM;
import com.mv1.iaa.business.view.web.req.AgentVM;
import com.mv1.iaa.business.view.web.req.CDRLogVM;
import com.mv1.iaa.business.view.web.req.CDRVm;
import com.mv1.iaa.business.view.web.req.FacilityVM;
import com.mv1.iaa.business.view.web.req.SearchVM;
import com.mv1.iaa.business.view.web.resp.AdminInvoiceVM;
import com.mv1.iaa.domain.Agent;
import com.mv1.iaa.domain.AgentCallLog;
import com.mv1.iaa.domain.CDR;
import com.mv1.iaa.domain.CDRLogs;
import com.mv1.iaa.domain.Profile;
import com.mv1.iaa.repository.AgentCallLogRepository;
import com.mv1.iaa.repository.CDRRepository;
import com.mv1.iaa.repository.CDR_Logs_Repository;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.service.PBXIntegrationService;
import com.mv1.iaa.service.dto.CustomFilterSearchDTO;
import com.mv1.iaa.web.rest.CSVResource;

@Component
public class PBXIntegrationServiceImpl implements PBXIntegrationService {

	private final Logger log = LoggerFactory.getLogger(PBXIntegrationServiceImpl.class);
	@Value("${pbx.global.user}")
	private String pbxUser;

	@Value("${pbx.username}")
	private String pbxUserName;

	@Value("${pbx.password}")
	private String pbxPassword;

	@Value("${pbx.syncDirectoryPath}")
	private String syncDirectoryPath;

	@Value("${pbx.grabFileDirectory}")
	private String grabFileDirectory;

	@Autowired
	private CDR_Logs_Repository cdrLogsRepository;

	@Autowired
	private CDRRepository cdrRepository;

	private EntityManagerFactory emf;

	@Autowired
	private CustomAgentRepository customAgentRepository;

	@Autowired
	private CustomAgentFilterSeviceImpl customAgentFilterSeviceImpl;

	@Autowired
	private AgentCallLogRepository agentCallLogRepository;

	/*
	 * @Autowired private SessionFactory sessionFactory;
	 */

	PBXIntegrationServiceImpl(EntityManagerFactory entityManager) {
		this.emf = entityManager;
	}

	@Override
	public void extensionUpdate(String extension, String telNumber) throws Exception {

		log.info("Extension update start:->>" + extension + "   number :" + telNumber);
		ManagerConnectionFactory factory = new ManagerConnectionFactory(pbxUser, 5038, pbxUserName, pbxPassword);
		ManagerConnection managerConnection = factory.createManagerConnection();
		managerConnection.login();

		DbPutAction dbPutAction = new DbPutAction();
		dbPutAction.setFamily("AMPUSER");
		dbPutAction.setKey(extension + "/followme/grplist");
		// “test telephone - 4087559340#“;
		dbPutAction.setVal(telNumber + "#");

		ManagerResponse dbPutActionResponse = managerConnection.sendAction(dbPutAction, 30000);

		// System.out.println(“dbPutActionResponse :: ” + dbPutActionResponse);

		log.info("Extension pbx:->>" + dbPutActionResponse);
		Thread.sleep(1000);

		managerConnection.logoff();

		if ("Error".equals(dbPutActionResponse.getResponse())) {
			throw new Exception(dbPutActionResponse.getMessage());
		}

	}

	@Override
	public void moveSyncDataToGrab() throws Mv1Exception {
		log.info("Sync data started" + new Date());
		log.info("dir" + syncDirectoryPath.replace("/", File.separator));
		List<DirectoryPath> syncDirectoryPaths = new ArrayList<>();
		// Find list of directory move to pool
		fileDirUtility.listofDirectories(syncDirectoryPath, syncDirectoryPaths);
		CDRLogs fileProcess = null;

		long startTime = System.currentTimeMillis();
		log.info("dire" + syncDirectoryPaths.size());
		for (DirectoryPath paths : syncDirectoryPaths) {
			File csvFolder = new File(paths.getPath() + File.separator);

			File[] csvFiles = csvFolder.listFiles(File::isFile);
			log.info("dir" + syncDirectoryPath.replace("/", File.separator));
			for (File csvFile : csvFiles) {

				Date d = new Date();
				DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
				String transactionTime = df.format(d);
				System.out.println("String in dd/MM/yyyy format is: " + transactionTime);
				String fname = FilenameUtils.removeExtension(csvFile.getName()) + transactionTime + ".csv";

				File file2 = new File(paths.getPath() + File.separator + fname);
				csvFile.renameTo(file2);

				csvFile = new File(paths.getPath() + File.separator + fname);
				if (FilenameUtils.getExtension(csvFile.getName()).equalsIgnoreCase("csv")) {

					File sourceDirectory = new File(csvFolder.getPath());
					System.out.println("sourceDir" + sourceDirectory.getAbsolutePath());
					System.out.println("sourceDir" + sourceDirectory.getPath());

					log.info("source"
							+ sourceDirectory.getPath().replace(syncDirectoryPath.replace("/", File.separator), ""));
					// Target folder as grabfolder + child folder path excluding sync data folder
					File targetDirctory = new File(grabFileDirectory + File.separator
							+ sourceDirectory.getPath().replace(syncDirectoryPath.replace("/", File.separator), ""));

					log.info(targetDirctory.getPath());
					if (!targetDirctory.exists()) {
						targetDirctory.mkdirs();
					}
					System.out.println("time" + csvFile.lastModified());
					// Unique value to existing file copied or not

					fileProcess = cdrLogsRepository.findByFilePath(
							targetDirctory.getAbsolutePath() + File.separator + csvFile.getName(),
							Constants.CDR_FILE_COMPLETED);

					boolean isTransfer = false;
					String status = Constants.CDR_FILE_FOUND;
					// FileProcessStatus status = FileProcessStatus.FILE_SYNC_GRAB;
					if (fileProcess == null) {
						System.out.println("file not exists with" + targetDirctory.getAbsolutePath() + File.separator
								+ csvFile.getName());
						fileProcess = new CDRLogs();
						fileProcess.setFileCreatedAt(csvFile.lastModified());
						fileProcess.setFileLastModifiedAt(csvFile.lastModified());
						isTransfer = true;
					} else {
						if (fileProcess.getFileLastModifiedAt() != csvFile.lastModified()) {
							isTransfer = true;
							fileProcess.setFileLastModifiedAt(csvFile.lastModified());
						} else {
							log.info(" File already copied and not modified :" + fileProcess.getFilePath());
						}
					}
					updateProcess(fileProcess, status, "", startTime);

					if (isTransfer) {
//									 Set from drools rules configuation
						if (paths.getParentDirFiterPath() == null) {
							fileProcess.setParentDirFilterPath(syncDirectoryPath);
						} else {
							fileProcess.setParentDirFilterPath(paths.getParentDirFiterPath());
						}

						try {

							FileUtils.copyFileToDirectory(csvFile, targetDirctory);

						} catch (Exception e) {
							updateProcess(fileProcess, Constants.CDR_FILE_FAILED, "Failed to delete file on server",
									startTime);
							throw new Mv1Exception("Failed to delete file on server");// TODO: handle exception
						}

						fileProcess.setFileLogPath(targetDirctory.getAbsolutePath());
						fileProcess.setFilePath(targetDirctory.getAbsolutePath() + File.separator + csvFile.getName());
						status = Constants.CDR_FILE_PARSED;

						csvFile.setExecutable(true);
						if (csvFile.delete()) {
							log.info("filr deleted from server");
						} else {
							updateProcess(fileProcess, Constants.CDR_FILE_FAILED, "Failed to delete file on server",
									startTime);
							throw new Mv1Exception("Failed to delete file on server");
						}

						updateProcess(fileProcess, status, "", startTime);

					}

				}
			}

		}

		updateCdrRecords(fileProcess, startTime);

	}

	private void updateProcess(CDRLogs fileProcess, String processStatus, String errorMessage, long startTime) {

		long endTime = System.currentTimeMillis();
		fileProcess.setErrorMessage(errorMessage);
		fileProcess.setProcessStatus(processStatus);
		fileProcess.setExecutionTime(endTime - startTime);
		cdrLogsRepository.save(fileProcess);

	}

	private void updateProcessCount(CDRLogs fileProcess, int recordCount) {

		fileProcess.setRecordCount(recordCount);
		cdrLogsRepository.save(fileProcess);

	}

	private void updateCdrRecords(CDRLogs fileProcess, long startTime) throws Mv1Exception {
		File targetDirctory = new File(grabFileDirectory + File.separator);

		if (targetDirctory.isDirectory()) {
			File[] csvFiles = targetDirctory.listFiles(File::isFile);

			for (File csvFile : csvFiles) {

//				String fPath = csvFile.getAbsolutePath() + File.separator + csvFile.getName();
				String fPath = csvFile.getAbsolutePath();
				int fileFound = cdrLogsRepository.getFileCompletedCount(csvFile.getAbsolutePath(),
						Constants.CDR_FILE_COMPLETED);

				System.out.println((csvFile.getAbsolutePath()) + "------completd file status -----------" + fileFound);

				// int fileFound = cdrLogsRepository.getFileCompletedCount(
//						csvFile.getAbsolutePath() + File.separator + csvFile.getName(), Constants.CDR_FILE_PARSED);
//				if(fileProcess == null) {
				fileProcess = cdrLogsRepository.findOneByFilePath(fPath);
//				}

//				int fileFound = 0;
				if (fileFound == 0) {
					File convFile = null;
					try {
						FileInputStream input = new FileInputStream(csvFile);
						MultipartFile multipartFile = new MockMultipartFile("file", csvFile.getName(), "text/plain",
								IOUtils.toByteArray(input));

						convFile = CSVResource.multipartToFile(multipartFile);

						List<CdrBean> rec;
						try {

							rec = mapCdrCsv(convFile.getAbsolutePath());
							updateProcessCount(fileProcess, rec.size());
							CsvResp resp = bulkProcesCdr(rec, fileProcess, startTime);

							convFile.delete();

						} catch (Mv1Exception e) {
							updateProcess(fileProcess, Constants.CDR_FILE_FAILED, e.getMessage(), startTime);
							throw new Mv1Exception(e.getMessage());
						} catch (Exception e) {
							updateProcess(fileProcess, Constants.CDR_FILE_FAILED, "Failed to map csv to columns",
									startTime);
							throw new Mv1Exception("Failed to map csv to columns");
						}
					} catch (IllegalStateException | IOException e1) {
						// TODO Auto-generated catch block
						updateProcess(fileProcess, Constants.CDR_FILE_FAILED, "Failed to convert file to multipart",
								startTime);
						throw new Mv1Exception("Failed to convert file to multipart");
					}
				}

			}
		}

	}

	public List<CdrBean> mapCdrCsv(String path) throws Exception {
		List<CdrBean> beans = Util.readCSV(path, CdrBean.class);
		System.out.println("beans----------------" + beans.get(0));
		return beans;

	}

	@SuppressWarnings("finally")
	@Timed
	@Transactional
	private CsvResp bulkProcesCdr(List<CdrBean> beans, CDRLogs fileProcess, long startTime) throws Mv1Exception {

		log.debug("processing bulkAddAgents ... ");

		CsvResp resp = new CsvResp();

		List<CDR> cdrList = getCDRRecordsFromBean(beans);

		if (cdrList.size() == 0) {
			updateProcess(fileProcess, Constants.CDR_FILE_COMPLETED, "", startTime);
		}

		/*
		 * try { for (int i = 0; i < cdrList.size(); i++) {
		 * cdrRepository.save(cdrList.get(i)); }
		 * updateProcess(fileProcess,Constants.CDR_FILE_COMPLETED,"",startTime);
		 * 
		 * }catch (Exception e) { // TODO: handle exception
		 * updateProcess(fileProcess,Constants.
		 * CDR_FILE_FAILED,"Failed to update file to database.",startTime); throw new
		 * Mv1Exception("Failed to save data in database"); }
		 */

		EntityManager entityManager = emf.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		try {
			cdrList.forEach(item -> entityManager.persist(item));

			entityTransaction.commit();
			updateProcess(fileProcess, Constants.CDR_FILE_COMPLETED, "", startTime);

			return resp;
//        		throw new Mv1Exception("Failed to save data in database");
		} catch (Exception e) {
			// TODO: handle exception
			entityTransaction.rollback();
			updateProcess(fileProcess, Constants.CDR_FILE_FAILED, "Failed to update file to database.", startTime);
			throw new Mv1Exception("Failed to update file to database.");
		} finally {
			entityManager.close();

		}

		/*
		 * int batchSize = 2;
		 * 
		 * EntityManager entityManager = emf.createEntityManager();
		 * 
		 * EntityTransaction entityTransaction = entityManager .getTransaction();
		 * 
		 * try { entityTransaction.begin();
		 * 
		 * for (int i = 0; i < cdrList.size(); i++) { if (i > 0 && i % batchSize == 0) {
		 * entityTransaction.commit(); entityTransaction.begin(); entityManager.clear();
		 * } // if( i == 3) { // throw new
		 * Mv1Exception("Failed to update file to database."); // }
		 * entityManager.persist(cdrList.get(i)); }
		 * 
		 * entityTransaction.commit();
		 * updateProcess(fileProcess,Constants.CDR_FILE_COMPLETED,"",startTime); } catch
		 * (RuntimeException e) { if (entityTransaction.isActive()) {
		 * 
		 * entityTransaction.rollback(); updateProcess(fileProcess,Constants.
		 * CDR_FILE_FAILED,"Failed to update file to database.",startTime);
		 * 
		 * } throw new Mv1Exception("Failed to update file to database."); } finally {
		 * entityManager.close(); }
		 */

//			return resp;

	}

	public List<CDR> getCDRRecordsFromBean(List<CdrBean> beans) throws Mv1Exception {
		List<CDR> cdrList = new ArrayList<>();
		for (CdrBean vm : beans) {

			int count = cdrRepository.isLinkedIdExist(vm.getLinkedid() + "%");
			if (count == 0) {
				CDR cdrRecord = new CDR();
				cdrRecord.setAccountcode(vm.getAccountcode());
				cdrRecord.setAmaflags(vm.getAmaflags());
				cdrRecord.setBillsec(vm.getBillsec());
				cdrRecord.setChannel(vm.getChannel());
				cdrRecord.setClid(vm.getClid());
				cdrRecord.setCnam(vm.getCnam());
				cdrRecord.setCnum(vm.getCnum());
				cdrRecord.setDcontext(vm.getDcontext());
				cdrRecord.setDid(vm.getDid());
				cdrRecord.setDisposition(vm.getDisposition());
				cdrRecord.setDst(vm.getDst());
				cdrRecord.setDst_cnam(vm.getDst_cnam());
				cdrRecord.setDstchannel(vm.getDstchannel());
				cdrRecord.setDuration(vm.getDuration());
				cdrRecord.setLastapp(vm.getLastapp());
				cdrRecord.setLastdata(vm.getLastdata());
				cdrRecord.setOutbound_cnam(vm.getOutbound_cnam());
				cdrRecord.setOutbound_cnum(vm.getOutbound_cnum());
				cdrRecord.setRecordingfile(vm.getRecordingfile());
				cdrRecord.setSrc(vm.getSrc());
				cdrRecord.setUniqueid(vm.getUniqueid());
				cdrRecord.setUserfield(vm.getUserfield());
				cdrRecord.setLinkedid(vm.getLinkedid());
				cdrRecord.setPeeraccount(vm.getPeeraccount());
				cdrRecord.setSequence(vm.getSequence());

				Date d = convertToDate(vm.getCallDate());
				cdrRecord.setCallDate(d.toInstant().atZone(ZoneId.systemDefault()));

				cdrList.add(cdrRecord);
			}

		}
		return cdrList;

	}

	public Date convertToDate(String d) throws Mv1Exception {
		if (d == null || d == "" || d.isEmpty()) {
			return null;
		}

		String arr[] = d.split("-");
		String ddate = d;
		if (arr[2].length() == 2) {
			// String year = (new Date().getYear()) + "";
			String year = Calendar.getInstance().get(Calendar.YEAR) + "";

			ddate = arr[0] + "-" + arr[1] + "-" + year.charAt(0) + "" + year.charAt(1) + arr[2];
		}
		// DateFormat format = new SimpleDateFormat("MM-dd-yyyy",
		// Locale.ENGLISH);
		String[] DATE_FORMATS = new String[] {
				// "MM/dd/yy",
				"yyyy-MM-dd HH:mm:ss" };

		if (ddate != null && !ddate.isEmpty()) {
			for (String DATE_FORMAT : DATE_FORMATS) {
				try {
					return new SimpleDateFormat(DATE_FORMAT).parse(ddate);
				} catch (ParseException e) {
					throw new Mv1Exception("Failed to Parse call date.");

				}
			}

		}
		return null;

	}

	@Override
	public List<CDRVm> getCdrList(Date cdate, String cnum, String cnam, String dst, String src,
			List<CustomFilterSearchDTO> customFilterSearchDTOs, String types, String generalSearch) {
		// TODO Auto-generated method stub

		Object[][] agArr = cdrRepository.getAllCdrViewRecords();

		List<CDRVm> cdrList = new LinkedList<>();
		if (agArr != null && agArr.length > 0) {
			for (Object[] arr : agArr) {
				try {
					System.out.println("arr----------------" + arr.length);
					System.out.println("CDRVm.getKeys()----------------" + CDRVm.getKeys().length);
					System.out.println("arr----------------" + arr[1]);
					cdrList.add(CommonMapper.fromKeyVal(CDRVm.getKeys(), arr, CDRVm.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}
		}

		for (int i = 0; i < cdrList.size(); i++) {
			cdrList.get(i).setCallDate(((Timestamp) agArr[i][1]).toString());
//    		System.out.println("vm--------------------------"+cdrList.get(i).getCallDate());
		}

		Map<String, CDRVm> cdrMap = new HashMap<>();

		if (types.equalsIgnoreCase("unknown")) {
			cdrList = (List<CDRVm>) cdrList.stream().filter(x -> (x.getNpn() == null || x.getNpn() == ""))
					.collect(Collectors.toList());
		} else if (types.equalsIgnoreCase("iaa")) {
			cdrList = (List<CDRVm>) cdrList.stream().filter(x -> (x.getNpn() != null && x.getNpn() != ""))
					.collect(Collectors.toList());
		}

		if (generalSearch != null && !generalSearch.equalsIgnoreCase("")) {
			cdrList = (List<CDRVm>) cdrList.stream()
					.filter(x -> (x.getAgentNumber() != null
							&& x.getAgentNumber().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getSrc() != null && x.getSrc().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getDst() != null && x.getDst().toLowerCase().contains(generalSearch.toLowerCase())))
					.collect(Collectors.toList());
		}

		return cdrList;
	}

	@Override
	public List<CDRLogVM> getCdrLogList(String generalSearch) {
		Object[][] agArr = cdrLogsRepository.getAllCdrRecords();

		List<CDRLogVM> cdrList = new LinkedList<>();
		if (agArr != null && agArr.length > 0) {
			for (Object[] arr : agArr) {
				try {

					cdrList.add(CommonMapper.fromKeyVal(CDRLogVM.getKeys(), arr, CDRLogVM.class));
				} catch (Exception e) {
					log.debug("Exception while mapping all agents data ", e);
				}
			}
		}

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for (CDRLogVM cdr : cdrList) {

			Date copiedDate = new Date(Long.parseLong(cdr.getImportDate()));
			System.out.println("copiedDate----------" + copiedDate);

			String strDate = dateFormat.format(copiedDate);

			cdr.setImportDate(strDate);

			int count = cdr.getRecordCount();
			String message = "";
			if (count > 0 && !cdr.getStatus().equalsIgnoreCase(Constants.CDR_FILE_COMPLETED)) {
				message = "Error while procssing " + count + " reocrds. Please check error file for more details.";
				cdr.setFailureCount(count);
			} else if (cdr.getStatus().equalsIgnoreCase(Constants.CDR_FILE_COMPLETED)) {
				cdr.setSuccessCount(count);
			}

			cdr.setMessage(message);
		}
		if (generalSearch != null && !generalSearch.equalsIgnoreCase("")) {
			cdrList = (List<CDRLogVM>) cdrList.stream().filter(
					x -> (x.getStatus() != null && x.getStatus().toLowerCase().contains(generalSearch.toLowerCase())))
					.collect(Collectors.toList());
		}

		return cdrList;

	}

	@Override
	public void downloadCDRFile(Long fileId, HttpServletRequest request, HttpServletResponse response,
			String generalSearch) throws IOException, Mv1Exception {

		int BUFFER_SIZE = 4096;
//		"/home/snehal/cdr_reports/mv1stopdatapull20190104114451.csv"
		Optional<CDRLogs> log = cdrLogsRepository.findById(fileId);

		if (!log.isPresent()) {
			throw new Mv1Exception("Invalid file Id");
		}
		CDRLogs record = log.get();

		String filePath = record.getFilePath();
		// get absolute path of the application
		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		System.out.println("appPath = " + appPath);

		// construct the complete absolute path of the file
//        String fullPath = appPath + filePath;  
		String fullPath = filePath;
		File downloadFile = new File(fullPath);
		FileInputStream inputStream = new FileInputStream(downloadFile);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}
		System.out.println("MIME type: " + mimeType);

		// set content attributes for the response
		response.setContentType(mimeType);
		response.setContentLength((int) downloadFile.length());

		// set headers for the response
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
		response.setHeader(headerKey, headerValue);

		// get output stream of the response
		OutputStream outStream = response.getOutputStream();

		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead = -1;

		// write bytes read from the input stream into the output stream
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, bytesRead);
		}

		inputStream.close();
		outStream.close();
	}

	@Override
	public File downloadFacilityList(List<CDRVm> list, String fileType) {

		log.debug("Creating CSV of Donations report for req {} ", list);

		String[] col = new String[] { "Call Date", "Agent Name", "NPN", "Extension", "Agent Number", "src", "dst",
				"Duration", "Linked Id" };
		System.out.println("list-------------------------" + list);
		return customAgentFilterSeviceImpl.writeCSV(col, "cdr_report_", list, CDRVm.class, fileType);
	}

	@Override
	public void saveCdrLogsList(AgentCallLogVM agentCallLogVM) {
		AgentCallLog log = new AgentCallLog();
		log.setAgentNumber(agentCallLogVM.getAgentNumber());
		log.setCallDate(agentCallLogVM.getCallDate());
		log.setType(agentCallLogVM.getType());
		log.setUserName(agentCallLogVM.getUserName());
		log.setUserNumber(agentCallLogVM.getUserNumber());
		log.setNpn(agentCallLogVM.getNpn());
		agentCallLogRepository.save(log);
	}

	@Override
	public List<AdminInvoiceVM> getAdminInvoiceList(String generalSearch) {
		// TODO Auto-generated method stub
		List<CDRVm> calllist = new ArrayList<>();
		calllist = getCdrList(null, "", "", "", "", null, "", "");

		List<AgentCallLog> vehicleCalls = agentCallLogRepository.findAll();

		List<AdminInvoiceVM> invoiceList = new ArrayList<>();

//		String[] months = {"Jan","Feb","March","April","May","Jun","July"};

		for (AgentCallLog callLog : vehicleCalls) {

			for (CDRVm call : calllist) {

				if (callLog.getAgentNumber().equalsIgnoreCase(call.getAgentNumber())
						&& callLog.getUserNumber().equalsIgnoreCase(call.getSrc())
						&& Integer.parseInt(call.getDuration()) > 0) {
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
//					String cdate = callLog.getCallDate().toString();
					String cdate = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").format(callLog.getCallDate());

					if (call.getCallDate() != null && !call.getCallDate().isEmpty() && call.getCallDate() != ""
							&& callLog.getCallDate() != null && !cdate.isEmpty() && cdate != "") {
						try {
							Date callDate = format.parse(call.getCallDate());
							Date callLogDate = format.parse(cdate);

							if (callDate.equals(callLogDate)) {

								boolean agentExist = false;
								AdminInvoiceVM existingVm = null;
								for (AdminInvoiceVM agent : invoiceList) {
									// check month and npn
									DateFormat format_month = new SimpleDateFormat("yyyy-MM", Locale.ENGLISH);
//									String aMonth =	DateTimeFormatter.ofPattern("yyyy-MM").format(agent.getMonth());
//									Date aMonth =format_month.parse(agent.getMonth());
									String bMonth = format_month.format(callDate);
									System.out.println("aMonth==============================" + agent.getMonth());
									System.out.println("bMonth==============================" + bMonth);

									if (agent.getNpn().equals(callLog.getNpn()) && agent.getMonth().equals(bMonth)) {

										agentExist = true;
										existingVm = agent;
										break;
									}
								}
								if (agentExist) {
									if ((existingVm.getLinkedId() != null || existingVm.getLinkedId() != " ")
											&& !existingVm.getLinkedId().trim().equals(call.getLinkedid())) {
										existingVm.setNoOfCalls(existingVm.getNoOfCalls() + 1);

										existingVm.setTotalCharge(existingVm.getTotalCharge() + 25);
									}
								} else {
									DateFormat format_month = new SimpleDateFormat("yyyy-MM", Locale.ENGLISH);
									String bMonth1 = format_month.format(callDate);
									System.out.println("bMonth1==============================" + bMonth1);

									AdminInvoiceVM vm = new AdminInvoiceVM();

									if (callLog.getNpn() != null && callLog.getNpn() != "") {
										Long userid = customAgentRepository.checkAgentSignUp(callLog.getNpn());
										if (userid != null) {
											vm.setAgentSignedUp(true);
										} else {
											vm.setAgentSignedUp(false);
										}

									}

									vm.setAgentName(callLog.getUserName());
									vm.setAgentNumber(call.getAgentNumber());
									vm.setBaseCharge(25);
									vm.setMonth(bMonth1.toString());
									vm.setLinkedId(call.getLinkedid());
									vm.setNoOfCalls(1);
									vm.setTotalCharge(25);
									vm.setNpn(callLog.getNpn());
									invoiceList.add(vm);
								}

							}
						} catch (ParseException e) {
							System.out.println("Failed to parse date ........");
							return null;
						}
					}

				}
			}

		}

		if (generalSearch != null && !generalSearch.isEmpty()) {
			invoiceList = (List<AdminInvoiceVM>) invoiceList.stream()
					.filter(x -> (x.getAgentName() != null
							&& x.getAgentName().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getNpn() != null && x.getNpn().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getAgentNumber() != null
									&& x.getAgentNumber().toLowerCase().contains(generalSearch.toLowerCase()))
							|| (x.getMonth() != null && (monthName(Integer.parseInt(x.getMonth().split("-")[1])) + " "
									+ x.getMonth().split("-")[0]).contains(generalSearch.toLowerCase())))
					.collect(Collectors.toList());
		}

		return invoiceList;
	}

	public String monthName(int month) {
		String months[] = { "jan", "feb", "march", "apr", "may", "jun", "july", "augt", "sept", "octr", "nov", "dec" };
		return months[month - 1];
	}

}
