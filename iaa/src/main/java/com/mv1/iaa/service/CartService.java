package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mv1.iaa.service.dto.CartDTO;

/**
 * Service Interface for managing Cart.
 */
public interface CartService {

    /**
     * Save a cart.
     *
     * @param cartDTO the entity to save
     * @return the persisted entity
     */
    CartDTO save(CartDTO cartDTO);

    /**
     * Get all the carts.
     *
     * @return the list of entities
     */
    List<CartDTO> findAll();

    /**
     * Get all the Cart with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    Page<CartDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" cart.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<CartDTO> findOne(Long id);

    /**
     * Delete the "id" cart.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
