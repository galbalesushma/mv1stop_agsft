package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.Facility;
import com.mv1.iaa.service.dto.FacilityDTO;

/**
 * Mapper for the entity Facility and its DTO FacilityDTO.
 */
@Mapper(componentModel = "spring", uses = {ProfileMapper.class, BusinessMapper.class})
public interface FacilityMapper extends EntityMapper<FacilityDTO, Facility> {

    @Mapping(source = "profile.id", target = "profileId")
    @Mapping(source = "business.id", target = "businessId")
    FacilityDTO toDto(Facility facility);

    @Mapping(source = "profileId", target = "profile")
    @Mapping(source = "businessId", target = "business")
    Facility toEntity(FacilityDTO facilityDTO);

    default Facility fromId(Long id) {
        if (id == null) {
            return null;
        }
        Facility facility = new Facility();
        facility.setId(id);
        return facility;
    }
}
