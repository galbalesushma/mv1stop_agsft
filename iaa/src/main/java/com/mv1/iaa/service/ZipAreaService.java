package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import com.mv1.iaa.service.dto.ZipAreaDTO;

/**
 * Service Interface for managing ZipArea.
 */
public interface ZipAreaService {

    /**
     * Save a zipArea.
     *
     * @param zipAreaDTO the entity to save
     * @return the persisted entity
     */
    ZipAreaDTO save(ZipAreaDTO zipAreaDTO);

    /**
     * Get all the zipAreas.
     *
     * @return the list of entities
     */
    List<ZipAreaDTO> findAll();


    /**
     * Get the "id" zipArea.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ZipAreaDTO> findOne(Long id);

    /**
     * Delete the "id" zipArea.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
