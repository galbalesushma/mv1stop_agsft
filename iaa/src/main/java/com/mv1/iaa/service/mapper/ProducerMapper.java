package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.Producer;
import com.mv1.iaa.service.dto.ProducerDTO;

/**
 * Mapper for the entity Producer and its DTO ProducerDTO.
 */
@Mapper(componentModel = "spring", uses = {AddressMapper.class})
public interface ProducerMapper extends EntityMapper<ProducerDTO, Producer> {

    @Mapping(source = "addr.id", target = "addrId")
    ProducerDTO toDto(Producer producer);

    @Mapping(source = "addrId", target = "addr")
    @Mapping(target = "licenses", ignore = true)
    Producer toEntity(ProducerDTO producerDTO);

    default Producer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Producer producer = new Producer();
        producer.setId(id);
        return producer;
    }
}
