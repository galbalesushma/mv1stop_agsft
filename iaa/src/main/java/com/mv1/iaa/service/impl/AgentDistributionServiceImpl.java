package com.mv1.iaa.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.domain.AgentDistribution;
import com.mv1.iaa.repository.AgentDistributionRepository;
import com.mv1.iaa.service.AgentDistributionService;
import com.mv1.iaa.service.dto.AgentDistributionDTO;
import com.mv1.iaa.service.mapper.AgentDistributionMapper;

/**
 * Service Implementation for managing AgentDistribution.
 */
@Service
@Transactional
public class AgentDistributionServiceImpl implements AgentDistributionService {

    private final Logger log = LoggerFactory.getLogger(AgentDistributionServiceImpl.class);

    private final AgentDistributionRepository agentDistributionRepository;

    private final AgentDistributionMapper agentDistributionMapper;

    public AgentDistributionServiceImpl(AgentDistributionRepository agentDistributionRepository, AgentDistributionMapper agentDistributionMapper) {
        this.agentDistributionRepository = agentDistributionRepository;
        this.agentDistributionMapper = agentDistributionMapper;
    }

    /**
     * Save a agentDistribution.
     *
     * @param agentDistributionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AgentDistributionDTO save(AgentDistributionDTO agentDistributionDTO) {
        log.debug("Request to save AgentDistribution : {}", agentDistributionDTO);
        AgentDistribution agentDistribution = agentDistributionMapper.toEntity(agentDistributionDTO);
        agentDistribution = agentDistributionRepository.save(agentDistribution);
        return agentDistributionMapper.toDto(agentDistribution);
    }

    /**
     * Get all the agentDistributions.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgentDistributionDTO> findAll() {
        log.debug("Request to get all AgentDistributions");
        return agentDistributionRepository.findAll().stream()
            .map(agentDistributionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one agentDistribution by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AgentDistributionDTO> findOne(Long id) {
        log.debug("Request to get AgentDistribution : {}", id);
        return agentDistributionRepository.findById(id)
            .map(agentDistributionMapper::toDto);
    }

    /**
     * Delete the agentDistribution by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AgentDistribution : {}", id);
        agentDistributionRepository.deleteById(id);
    }
}
