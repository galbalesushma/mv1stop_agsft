package com.mv1.iaa.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.domain.SearchLogs;
import com.mv1.iaa.repository.SearchLogsRepository;
import com.mv1.iaa.service.SearchLogsService;
import com.mv1.iaa.service.dto.SearchLogsDTO;
import com.mv1.iaa.service.mapper.SearchLogsMapper;

/**
 * Service Implementation for managing SearchLogs.
 */
@Service
@Transactional
public class SearchLogsServiceImpl implements SearchLogsService {

    private final Logger log = LoggerFactory.getLogger(SearchLogsServiceImpl.class);

    private final SearchLogsRepository searchLogsRepository;

    private final SearchLogsMapper searchLogsMapper;

    public SearchLogsServiceImpl(SearchLogsRepository searchLogsRepository, SearchLogsMapper searchLogsMapper) {
        this.searchLogsRepository = searchLogsRepository;
        this.searchLogsMapper = searchLogsMapper;
    }

    /**
     * Save a searchLogs.
     *
     * @param searchLogsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SearchLogsDTO save(SearchLogsDTO searchLogsDTO) {
        log.debug("Request to save SearchLogs : {}", searchLogsDTO);
        SearchLogs searchLogs = searchLogsMapper.toEntity(searchLogsDTO);
        searchLogs = searchLogsRepository.save(searchLogs);
        return searchLogsMapper.toDto(searchLogs);
    }

    /**
     * Get all the searchLogs.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SearchLogsDTO> findAll() {
        log.debug("Request to get all SearchLogs");
        return searchLogsRepository.findAll().stream()
            .map(searchLogsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one searchLogs by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SearchLogsDTO> findOne(Long id) {
        log.debug("Request to get SearchLogs : {}", id);
        return searchLogsRepository.findById(id)
            .map(searchLogsMapper::toDto);
    }

    /**
     * Delete the searchLogs by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SearchLogs : {}", id);
        searchLogsRepository.deleteById(id);
    }
}
