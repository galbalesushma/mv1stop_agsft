package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.Agent;
import com.mv1.iaa.service.dto.AgentDTO;

/**
 * Mapper for the entity Agent and its DTO AgentDTO.
 */
@Mapper(componentModel = "spring", uses = {ProfileMapper.class, AgentLicMapper.class, StateMapper.class, BusinessMapper.class})
public interface AgentMapper extends EntityMapper<AgentDTO, Agent> {

    @Mapping(source = "profile.id", target = "profileId")
    @Mapping(source = "lic.id", target = "licId")
    @Mapping(source = "domicileState.id", target = "domicileStateId")
    @Mapping(source = "business.id", target = "businessId")
    AgentDTO toDto(Agent agent);

    @Mapping(source = "profileId", target = "profile")
    @Mapping(source = "licId", target = "lic")
    @Mapping(target = "types", ignore = true)
    @Mapping(source = "domicileStateId", target = "domicileState")
    @Mapping(source = "businessId", target = "business")
    Agent toEntity(AgentDTO agentDTO);

    default Agent fromId(Long id) {
        if (id == null) {
            return null;
        }
        Agent agent = new Agent();
        agent.setId(id);
        return agent;
    }
}
