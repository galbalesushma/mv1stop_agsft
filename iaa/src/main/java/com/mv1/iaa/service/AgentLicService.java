package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import com.mv1.iaa.service.dto.AgentLicDTO;

/**
 * Service Interface for managing AgentLic.
 */
public interface AgentLicService {

    /**
     * Save a agentLic.
     *
     * @param agentLicDTO the entity to save
     * @return the persisted entity
     */
    AgentLicDTO save(AgentLicDTO agentLicDTO);

    /**
     * Get all the agentLics.
     *
     * @return the list of entities
     */
    List<AgentLicDTO> findAll();


    /**
     * Get the "id" agentLic.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AgentLicDTO> findOne(Long id);

    /**
     * Delete the "id" agentLic.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
