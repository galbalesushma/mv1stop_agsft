package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Agent entity.
 */
public class AgentDTO implements Serializable {

	private Long id;

	@NotNull
	private String npn;

	private String naicno;

	private String fein;

	private Boolean residentState;

	private ZonedDateTime createdAt;

	private ZonedDateTime updatedAt;

	private ZonedDateTime verifiedAt;

	private Boolean isVerified;

	private Boolean isActive;

	private Boolean isDeleted;

	private Long profileId;

	private Long licId;

	private Long domicileStateId;

	private Long businessId;

	private String extension;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNpn() {
		return npn;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}

	public String getNaicno() {
		return naicno;
	}

	public void setNaicno(String naicno) {
		this.naicno = naicno;
	}

	public String getFein() {
		return fein;
	}

	public void setFein(String fein) {
		this.fein = fein;
	}

	public Boolean isResidentState() {
		return residentState;
	}

	public void setResidentState(Boolean residentState) {
		this.residentState = residentState;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public ZonedDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(ZonedDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public ZonedDateTime getVerifiedAt() {
		return verifiedAt;
	}

	public void setVerifiedAt(ZonedDateTime verifiedAt) {
		this.verifiedAt = verifiedAt;
	}

	public Boolean isIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Boolean isIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean isIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public Long getLicId() {
		return licId;
	}

	public void setLicId(Long agentLicId) {
		this.licId = agentLicId;
	}

	public Long getDomicileStateId() {
		return domicileStateId;
	}

	public void setDomicileStateId(Long stateId) {
		this.domicileStateId = stateId;
	}

	public Long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		AgentDTO agentDTO = (AgentDTO) o;
		if (agentDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), agentDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "AgentDTO [id=" + id + ", npn=" + npn + ", naicno=" + naicno + ", fein=" + fein + ", residentState="
				+ residentState + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", verifiedAt=" + verifiedAt
				+ ", isVerified=" + isVerified + ", isActive=" + isActive + ", isDeleted=" + isDeleted + ", profileId="
				+ profileId + ", licId=" + licId + ", domicileStateId=" + domicileStateId + ", businessId=" + businessId
				+ ", extension=" + extension + "]";
	}
}
