
package com.mv1.iaa.service;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mv1.iaa.business.exception.Mv1Exception;
import com.mv1.iaa.business.view.web.req.AgentCallLogVM;
import com.mv1.iaa.business.view.web.req.CDRLogVM;
import com.mv1.iaa.business.view.web.req.CDRVm;
import com.mv1.iaa.business.view.web.resp.AdminInvoiceVM;
import com.mv1.iaa.service.dto.CustomFilterSearchDTO;

public interface PBXIntegrationService {

	public  void extensionUpdate(String extension, String telNumber) throws Exception;

	public void moveSyncDataToGrab() throws Mv1Exception;

	public List<CDRVm> getCdrList(Date cdate, String cnum, String cnam, String dst, String src,
			List<CustomFilterSearchDTO> customFilterSearchDTO, String types, String generalSearch);

	public List<CDRLogVM> getCdrLogList(String generalSearch);

	public void downloadCDRFile(Long fileId, HttpServletRequest request, HttpServletResponse response, String generalSearch) 
			throws IOException, Mv1Exception;

	public File downloadFacilityList(List<CDRVm> list, String fileType);

	public void saveCdrLogsList(AgentCallLogVM agentCallLogVM);

	public List<AdminInvoiceVM> getAdminInvoiceList(String generalSearch);
}
