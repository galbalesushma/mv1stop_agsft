package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the DMVLocation entity.
 */
public class DMVLocationDTO implements Serializable {

    private Long id;

    private String name;

    private String address;

    private String city;

    private String state;

    private String postalCode;

    private String country;

    private String mainPhone;

    private String faxNumber;

    private String weekdaysHrs;

    private String saturdayHrs;

    private String directions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMainPhone() {
        return mainPhone;
    }

    public void setMainPhone(String mainPhone) {
        this.mainPhone = mainPhone;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getWeekdaysHrs() {
        return weekdaysHrs;
    }

    public void setWeekdaysHrs(String weekdaysHrs) {
        this.weekdaysHrs = weekdaysHrs;
    }

    public String getSaturdayHrs() {
        return saturdayHrs;
    }

    public void setSaturdayHrs(String saturdayHrs) {
        this.saturdayHrs = saturdayHrs;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DMVLocationDTO dMVLocationDTO = (DMVLocationDTO) o;
        if (dMVLocationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dMVLocationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DMVLocationDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", country='" + getCountry() + "'" +
            ", mainPhone='" + getMainPhone() + "'" +
            ", faxNumber='" + getFaxNumber() + "'" +
            ", weekdaysHrs='" + getWeekdaysHrs() + "'" +
            ", saturdayHrs='" + getSaturdayHrs() + "'" +
            ", directions='" + getDirections() + "'" +
            "}";
    }
}
