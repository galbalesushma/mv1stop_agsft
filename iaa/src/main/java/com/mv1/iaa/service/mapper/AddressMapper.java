package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.Address;
import com.mv1.iaa.service.dto.AddressDTO;

/**
 * Mapper for the entity Address and its DTO AddressDTO.
 */
@Mapper(componentModel = "spring", uses = {CityMapper.class, CountyMapper.class, StateMapper.class})
public interface AddressMapper extends EntityMapper<AddressDTO, Address> {

    @Mapping(source = "city.id", target = "cityId")
    @Mapping(source = "county.id", target = "countyId")
    @Mapping(source = "state.id", target = "stateId")
    AddressDTO toDto(Address address);

    @Mapping(target = "contacts", ignore = true)
    @Mapping(source = "cityId", target = "city")
    @Mapping(source = "countyId", target = "county")
    @Mapping(source = "stateId", target = "state")
    Address toEntity(AddressDTO addressDTO);

    default Address fromId(Long id) {
        if (id == null) {
            return null;
        }
        Address address = new Address();
        address.setId(id);
        return address;
    }
}
