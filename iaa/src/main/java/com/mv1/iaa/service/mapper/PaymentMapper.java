package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;

import com.mv1.iaa.domain.Payment;
import com.mv1.iaa.service.dto.PaymentDTO;

/**
 * Mapper for the entity Payment and its DTO PaymentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PaymentMapper extends EntityMapper<PaymentDTO, Payment> {



    default Payment fromId(Long id) {
        if (id == null) {
            return null;
        }
        Payment payment = new Payment();
        payment.setId(id);
        return payment;
    }
}
