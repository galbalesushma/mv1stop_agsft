package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.AgentLic;
import com.mv1.iaa.service.dto.AgentLicDTO;

/**
 * Mapper for the entity AgentLic and its DTO AgentLicDTO.
 */
@Mapper(componentModel = "spring", uses = {ProducerMapper.class})
public interface AgentLicMapper extends EntityMapper<AgentLicDTO, AgentLic> {

    @Mapping(source = "producer.id", target = "producerId")
    AgentLicDTO toDto(AgentLic agentLic);

    @Mapping(source = "producerId", target = "producer")
    AgentLic toEntity(AgentLicDTO agentLicDTO);

    default AgentLic fromId(Long id) {
        if (id == null) {
            return null;
        }
        AgentLic agentLic = new AgentLic();
        agentLic.setId(id);
        return agentLic;
    }
}
