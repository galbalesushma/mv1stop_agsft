package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import com.mv1.iaa.service.dto.BusinessDTO;

/**
 * Service Interface for managing Business.
 */
public interface BusinessService {

    /**
     * Save a business.
     *
     * @param businessDTO the entity to save
     * @return the persisted entity
     */
    BusinessDTO save(BusinessDTO businessDTO);

    /**
     * Get all the businesses.
     *
     * @return the list of entities
     */
    List<BusinessDTO> findAll();


    /**
     * Get the "id" business.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<BusinessDTO> findOne(Long id);

    /**
     * Delete the "id" business.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
