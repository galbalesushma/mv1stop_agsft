package com.mv1.iaa.service.mapper;

import com.mv1.iaa.domain.*;
import com.mv1.iaa.service.dto.DMVLocationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DMVLocation and its DTO DMVLocationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DMVLocationMapper extends EntityMapper<DMVLocationDTO, DMVLocation> {



    default DMVLocation fromId(Long id) {
        if (id == null) {
            return null;
        }
        DMVLocation dMVLocation = new DMVLocation();
        dMVLocation.setId(id);
        return dMVLocation;
    }
}
