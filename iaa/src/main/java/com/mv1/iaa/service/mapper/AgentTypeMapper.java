package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.AgentType;
import com.mv1.iaa.service.dto.AgentTypeDTO;

/**
 * Mapper for the entity AgentType and its DTO AgentTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {AgentMapper.class})
public interface AgentTypeMapper extends EntityMapper<AgentTypeDTO, AgentType> {

    @Mapping(source = "agent.id", target = "agentId")
    AgentTypeDTO toDto(AgentType agentType);

    @Mapping(source = "agentId", target = "agent")
    AgentType toEntity(AgentTypeDTO agentTypeDTO);

    default AgentType fromId(Long id) {
        if (id == null) {
            return null;
        }
        AgentType agentType = new AgentType();
        agentType.setId(id);
        return agentType;
    }
}
