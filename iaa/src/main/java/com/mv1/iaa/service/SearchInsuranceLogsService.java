package com.mv1.iaa.service;

import com.mv1.iaa.service.dto.SearchInsuranceLogsDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing SearchInsuranceLogs.
 */
public interface SearchInsuranceLogsService {

    /**
     * Save a searchInsuranceLogs.
     *
     * @param searchInsuranceLogsDTO the entity to save
     * @return the persisted entity
     */
    SearchInsuranceLogsDTO save(SearchInsuranceLogsDTO searchInsuranceLogsDTO);

    /**
     * Get all the searchInsuranceLogs.
     *
     * @return the list of entities
     */
    List<SearchInsuranceLogsDTO> findAll();


    /**
     * Get the "id" searchInsuranceLogs.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SearchInsuranceLogsDTO> findOne(Long id);

    /**
     * Delete the "id" searchInsuranceLogs.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
