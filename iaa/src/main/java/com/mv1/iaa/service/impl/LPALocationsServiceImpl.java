package com.mv1.iaa.service.impl;

import com.mv1.iaa.service.LPALocationsService;
import com.mv1.iaa.domain.LPALocations;
import com.mv1.iaa.repository.LPALocationsRepository;
import com.mv1.iaa.service.dto.LPALocationsDTO;
import com.mv1.iaa.service.mapper.LPALocationsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing LPALocations.
 */
@Service
@Transactional
public class LPALocationsServiceImpl implements LPALocationsService {

    private final Logger log = LoggerFactory.getLogger(LPALocationsServiceImpl.class);

    private final LPALocationsRepository lPALocationsRepository;

    private final LPALocationsMapper lPALocationsMapper;

    public LPALocationsServiceImpl(LPALocationsRepository lPALocationsRepository, LPALocationsMapper lPALocationsMapper) {
        this.lPALocationsRepository = lPALocationsRepository;
        this.lPALocationsMapper = lPALocationsMapper;
    }

    /**
     * Save a lPALocations.
     *
     * @param lPALocationsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LPALocationsDTO save(LPALocationsDTO lPALocationsDTO) {
        log.debug("Request to save LPALocations : {}", lPALocationsDTO);

        LPALocations lPALocations = lPALocationsMapper.toEntity(lPALocationsDTO);
        lPALocations = lPALocationsRepository.save(lPALocations);
        return lPALocationsMapper.toDto(lPALocations);
    }

    /**
     * Get all the lPALocations.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<LPALocationsDTO> findAll() {
        log.debug("Request to get all LPALocations");
        return lPALocationsRepository.findAll().stream()
            .map(lPALocationsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one lPALocations by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LPALocationsDTO> findOne(Long id) {
        log.debug("Request to get LPALocations : {}", id);
        return lPALocationsRepository.findById(id)
            .map(lPALocationsMapper::toDto);
    }

    /**
     * Delete the lPALocations by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LPALocations : {}", id);
        lPALocationsRepository.deleteById(id);
    }
    
}
