package com.mv1.iaa.service.impl;

import com.mv1.iaa.service.DMVLocationService;
import com.mv1.iaa.domain.DMVLocation;
import com.mv1.iaa.repository.DMVLocationRepository;
import com.mv1.iaa.service.dto.DMVLocationDTO;
import com.mv1.iaa.service.mapper.DMVLocationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing DMVLocation.
 */
@Service
@Transactional
public class DMVLocationServiceImpl implements DMVLocationService {

    private final Logger log = LoggerFactory.getLogger(DMVLocationServiceImpl.class);

    private final DMVLocationRepository dMVLocationRepository;

    private final DMVLocationMapper dMVLocationMapper;

    public DMVLocationServiceImpl(DMVLocationRepository dMVLocationRepository, DMVLocationMapper dMVLocationMapper) {
        this.dMVLocationRepository = dMVLocationRepository;
        this.dMVLocationMapper = dMVLocationMapper;
    }

    /**
     * Save a dMVLocation.
     *
     * @param dMVLocationDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DMVLocationDTO save(DMVLocationDTO dMVLocationDTO) {
        log.debug("Request to save DMVLocation : {}", dMVLocationDTO);

        DMVLocation dMVLocation = dMVLocationMapper.toEntity(dMVLocationDTO);
        dMVLocation = dMVLocationRepository.save(dMVLocation);
        return dMVLocationMapper.toDto(dMVLocation);
    }

    /**
     * Get all the dMVLocations.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DMVLocationDTO> findAll() {
        log.debug("Request to get all DMVLocations");
        return dMVLocationRepository.findAll().stream()
            .map(dMVLocationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one dMVLocation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DMVLocationDTO> findOne(Long id) {
        log.debug("Request to get DMVLocation : {}", id);
        return dMVLocationRepository.findById(id)
            .map(dMVLocationMapper::toDto);
    }

    /**
     * Delete the dMVLocation by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DMVLocation : {}", id);
        dMVLocationRepository.deleteById(id);
    }
}
