package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;

import com.mv1.iaa.domain.County;
import com.mv1.iaa.service.dto.CountyDTO;

/**
 * Mapper for the entity County and its DTO CountyDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CountyMapper extends EntityMapper<CountyDTO, County> {



    default County fromId(Long id) {
        if (id == null) {
            return null;
        }
        County county = new County();
        county.setId(id);
        return county;
    }
}
