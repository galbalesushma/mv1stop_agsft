package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SearchInsuranceLogs entity.
 */
public class SearchInsuranceLogsDTO implements Serializable {

    private Long id;

    private Long agentId;

    private Long profileId;

    private Long lpaId;

    private Long dmvId;

    private Long searchLogId;

    private String home;

    private String phone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Long getLpaId() {
        return lpaId;
    }

    public void setLpaId(Long lpaId) {
        this.lpaId = lpaId;
    }

    public Long getDmvId() {
        return dmvId;
    }

    public void setDmvId(Long dmvId) {
        this.dmvId = dmvId;
    }

    public Long getSearchLogId() {
        return searchLogId;
    }

    public void setSearchLogId(Long searchLogId) {
        this.searchLogId = searchLogId;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SearchInsuranceLogsDTO searchInsuranceLogsDTO = (SearchInsuranceLogsDTO) o;
        if (searchInsuranceLogsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), searchInsuranceLogsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SearchInsuranceLogsDTO{" +
            "id=" + getId() +
            ", agentId=" + getAgentId() +
            ", profileId=" + getProfileId() +
            ", lpaId=" + getLpaId() +
            ", dmvId=" + getDmvId() +
            ", searchLogId=" + getSearchLogId() +
            ", home='" + getHome() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }
}
