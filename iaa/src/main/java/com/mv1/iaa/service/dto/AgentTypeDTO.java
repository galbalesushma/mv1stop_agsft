package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AgentType entity.
 */
public class AgentTypeDTO implements Serializable {

    private Long id;

    private String name;

    private Long zipAreaId;

    private Long agentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getZipAreaId() {
        return zipAreaId;
    }

    public void setZipAreaId(Long zipAreaId) {
        this.zipAreaId = zipAreaId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AgentTypeDTO agentTypeDTO = (AgentTypeDTO) o;
        if (agentTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), agentTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AgentTypeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", zipAreaId=" + getZipAreaId() +
            ", agent=" + getAgentId() +
            "}";
    }
}
