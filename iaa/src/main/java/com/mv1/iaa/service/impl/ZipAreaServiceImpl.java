package com.mv1.iaa.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.domain.ZipArea;
import com.mv1.iaa.repository.ZipAreaRepository;
import com.mv1.iaa.service.ZipAreaService;
import com.mv1.iaa.service.dto.ZipAreaDTO;
import com.mv1.iaa.service.mapper.ZipAreaMapper;

/**
 * Service Implementation for managing ZipArea.
 */
@Service
@Transactional
public class ZipAreaServiceImpl implements ZipAreaService {

    private final Logger log = LoggerFactory.getLogger(ZipAreaServiceImpl.class);

    private final ZipAreaRepository zipAreaRepository;

    private final ZipAreaMapper zipAreaMapper;

    public ZipAreaServiceImpl(ZipAreaRepository zipAreaRepository, ZipAreaMapper zipAreaMapper) {
        this.zipAreaRepository = zipAreaRepository;
        this.zipAreaMapper = zipAreaMapper;
    }

    /**
     * Save a zipArea.
     *
     * @param zipAreaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ZipAreaDTO save(ZipAreaDTO zipAreaDTO) {
        log.debug("Request to save ZipArea : {}", zipAreaDTO);
        ZipArea zipArea = zipAreaMapper.toEntity(zipAreaDTO);
        zipArea = zipAreaRepository.save(zipArea);
        return zipAreaMapper.toDto(zipArea);
    }

    /**
     * Get all the zipAreas.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ZipAreaDTO> findAll() {
        log.debug("Request to get all ZipAreas");
        return zipAreaRepository.findAll().stream()
            .map(zipAreaMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one zipArea by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ZipAreaDTO> findOne(Long id) {
        log.debug("Request to get ZipArea : {}", id);
        return zipAreaRepository.findById(id)
            .map(zipAreaMapper::toDto);
    }

    /**
     * Delete the zipArea by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ZipArea : {}", id);
        zipAreaRepository.deleteById(id);
    }
}
