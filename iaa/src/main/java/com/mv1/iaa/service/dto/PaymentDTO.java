package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import com.mv1.iaa.domain.enumeration.PayStatus;

/**
 * A DTO for the Payment entity.
 */
public class PaymentDTO implements Serializable {

	private Long id;

	@NotNull
	private Long amount;

	private String txnId;

	@NotNull
	private String uuId;

	@NotNull
	private PayStatus status;

	private String gatewayResp;

	private Long profileId;

	private Long cartId;

	private ZonedDateTime createdAt;

	private ZonedDateTime updatedAt;

	private Boolean isDeleted;

	private Long chequeNo;

	private String txnType;

	private String routingNumber;

	private String accountNumber;

	private String accountType;

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public Long getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(Long chequeNo) {
		this.chequeNo = chequeNo;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getUuId() {
		return uuId;
	}

	public void setUuId(String uuId) {
		this.uuId = uuId;
	}

	public PayStatus getStatus() {
		return status;
	}

	public void setStatus(PayStatus status) {
		this.status = status;
	}

	public String getGatewayResp() {
		return gatewayResp;
	}

	public void setGatewayResp(String gatewayResp) {
		this.gatewayResp = gatewayResp;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public ZonedDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(ZonedDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Boolean isIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		PaymentDTO paymentDTO = (PaymentDTO) o;
		if (paymentDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), paymentDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "PaymentDTO{" + "id=" + getId() + ", amount=" + getAmount() + ", txnId='" + getTxnId() + "'" + ", uuId='"
				+ getUuId() + "'" + ", status='" + getStatus() + "'" + ", gatewayResp='" + getGatewayResp() + "'"
				+ ", profileId=" + getProfileId() + ", cartId=" + getCartId() + ", createdAt='" + getCreatedAt() + "'"
				+ ", updatedAt='" + getUpdatedAt() + "'" + ", isDeleted='" + isIsDeleted() + "'" + ", txnType='"
				+ getTxnType() + "'" + ", chequeNo='" + getChequeNo() + "'" + ", routingNumber='" + getRoutingNumber()
				+ "'" + ", accountNumber='" + getAccountNumber() + "'" + ", accountType='" + getAccountType() + "'"
				+ "}";
	}
}
