package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;

import com.mv1.iaa.domain.PricePackage;
import com.mv1.iaa.service.dto.PricePackageDTO;

/**
 * Mapper for the entity PricePackage and its DTO PricePackageDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PricePackageMapper extends EntityMapper<PricePackageDTO, PricePackage> {



    default PricePackage fromId(Long id) {
        if (id == null) {
            return null;
        }
        PricePackage pricePackage = new PricePackage();
        pricePackage.setId(id);
        return pricePackage;
    }
}
