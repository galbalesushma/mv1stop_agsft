package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AgentDistribution entity.
 */
public class AgentDistributionDTO implements Serializable {

    private Long id;

    private Integer slot;

    private String type;

    private Long zipId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSlot() {
        return slot;
    }

    public void setSlot(Integer slot) {
        this.slot = slot;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getZipId() {
        return zipId;
    }

    public void setZipId(Long zipAreaId) {
        this.zipId = zipAreaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AgentDistributionDTO agentDistributionDTO = (AgentDistributionDTO) o;
        if (agentDistributionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), agentDistributionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AgentDistributionDTO{" +
            "id=" + getId() +
            ", slot=" + getSlot() +
            ", type='" + getType() + "'" +
            ", zip=" + getZipId() +
            "}";
    }
}
