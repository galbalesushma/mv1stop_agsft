package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ZipArea entity.
 */
public class ZipAreaDTO implements Serializable {

    private Long id;

    private String name;

    private String zip;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ZipAreaDTO zipAreaDTO = (ZipAreaDTO) o;
        if (zipAreaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), zipAreaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ZipAreaDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", zip='" + getZip() + "'" +
            "}";
    }
}
