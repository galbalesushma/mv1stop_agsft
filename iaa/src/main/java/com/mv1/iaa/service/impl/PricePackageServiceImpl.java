package com.mv1.iaa.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.domain.PricePackage;
import com.mv1.iaa.repository.PricePackageRepository;
import com.mv1.iaa.service.PricePackageService;
import com.mv1.iaa.service.dto.PricePackageDTO;
import com.mv1.iaa.service.mapper.PricePackageMapper;

/**
 * Service Implementation for managing PricePackage.
 */
@Service
@Transactional
public class PricePackageServiceImpl implements PricePackageService {

    private final Logger log = LoggerFactory.getLogger(PricePackageServiceImpl.class);

    private final PricePackageRepository pricePackageRepository;

    private final PricePackageMapper pricePackageMapper;

    public PricePackageServiceImpl(PricePackageRepository pricePackageRepository, PricePackageMapper pricePackageMapper) {
        this.pricePackageRepository = pricePackageRepository;
        this.pricePackageMapper = pricePackageMapper;
    }

    /**
     * Save a pricePackage.
     *
     * @param pricePackageDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PricePackageDTO save(PricePackageDTO pricePackageDTO) {
        log.debug("Request to save PricePackage : {}", pricePackageDTO);
        PricePackage pricePackage = pricePackageMapper.toEntity(pricePackageDTO);
        pricePackage = pricePackageRepository.save(pricePackage);
        return pricePackageMapper.toDto(pricePackage);
    }

    /**
     * Get all the pricePackages.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PricePackageDTO> findAll() {
        log.debug("Request to get all PricePackages");
        return pricePackageRepository.findAll().stream()
            .map(pricePackageMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one pricePackage by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PricePackageDTO> findOne(Long id) {
        log.debug("Request to get PricePackage : {}", id);
        return pricePackageRepository.findById(id)
            .map(pricePackageMapper::toDto);
    }

    /**
     * Delete the pricePackage by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PricePackage : {}", id);
        pricePackageRepository.deleteById(id);
    }
}
