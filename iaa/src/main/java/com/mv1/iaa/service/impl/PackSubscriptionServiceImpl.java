package com.mv1.iaa.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mv1.iaa.business.common.CommonMapper;
import com.mv1.iaa.domain.PackSubscription;
import com.mv1.iaa.repository.CustomAgentRepository;
import com.mv1.iaa.repository.PackSubscriptionRepository;
import com.mv1.iaa.service.PackSubscriptionService;
import com.mv1.iaa.service.dto.PackSubscriptionCustomDTO;
import com.mv1.iaa.service.dto.PackSubscriptionDTO;
import com.mv1.iaa.service.mapper.PackSubscriptionMapper;

/**
 * Service Implementation for managing PackSubscription.
 */
@Service
@Transactional
public class PackSubscriptionServiceImpl implements PackSubscriptionService {

    private final Logger log = LoggerFactory.getLogger(PackSubscriptionServiceImpl.class);

    private final PackSubscriptionRepository packSubscriptionRepository;
    
    @Autowired
    private CustomAgentRepository customAgentRepository;

    private final PackSubscriptionMapper packSubscriptionMapper;

    public PackSubscriptionServiceImpl(PackSubscriptionRepository packSubscriptionRepository, PackSubscriptionMapper packSubscriptionMapper) {
        this.packSubscriptionRepository = packSubscriptionRepository;
        this.packSubscriptionMapper = packSubscriptionMapper;
    }

    private static String[] keys = new String[] {"packageId","slots","duration","createdAt" ,"validTill", "zipAreaId","profileId","zipcode"};

    /**
     * Save a packSubscription.
     *
     * @param packSubscriptionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PackSubscriptionDTO save(PackSubscriptionDTO packSubscriptionDTO) {
        log.debug("Request to save PackSubscription : {}", packSubscriptionDTO);
        PackSubscription packSubscription = packSubscriptionMapper.toEntity(packSubscriptionDTO);
        packSubscription = packSubscriptionRepository.save(packSubscription);
        return packSubscriptionMapper.toDto(packSubscription);
    }

    /**
     * Get all the packSubscriptions.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PackSubscriptionDTO> findAll() {
        log.debug("Request to get all PackSubscriptions");
        return packSubscriptionRepository.findAll().stream()
            .map(packSubscriptionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one packSubscription by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PackSubscriptionDTO> findOne(Long id) {
        log.debug("Request to get PackSubscription : {}", id);
        return packSubscriptionRepository.findById(id)
            .map(packSubscriptionMapper::toDto);
    }

    /**
     * Delete the packSubscription by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PackSubscription : {}", id);
        packSubscriptionRepository.deleteById(id);
    }
    
    /**
     * Get one packSubscription by id.
     *
     * @param id the id of the entity
     * @return the entity
     */

    public List<PackSubscriptionCustomDTO> findByProfileId(Long profileId) {

        Object[][] agArr = customAgentRepository.findByProfileId(profileId);
        List<PackSubscriptionCustomDTO> sub = new ArrayList<>();
        if (agArr != null && agArr.length > 0) {
            for (Object[] data : agArr) {
                try {
                /*	 for (int i = 0; i < data.length; i++) {
                         Object val = data[i];
                         if ((val) instanceof Timestamp) {
                        	 System.out.println(" data[i] ==========================="+ data[i] );
                        	 ZonedDateTime fromTimeZone = ZonedDateTime.ofInstant(Instant.ofEpochMilli(((Timestamp) val).getTime()), ZoneOffset.UTC);
                        	 data[i] =fromTimeZone;
                        	 System.out.println(" fromTimeZone ==========================="+ fromTimeZone );
                    	 	if (data[i] instanceof ZonedDateTime) {
                    	 		System.out.println("data[i] is instance of ZonedDateTime");
                    	 	} else {
                    	 		System.out.println("data[i] is NOT instance of ZonedDateTime");
                    	 	}
                         }
                	 }*/
                	sub.add(CommonMapper.fromKeyVal(keys, data, PackSubscriptionCustomDTO.class));
                    
                } catch (Exception e) {
                    log.debug("Exception while mapping all agents data ", e);
                }
            }
        }
        return sub;
    }
   

}
