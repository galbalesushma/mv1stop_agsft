package com.mv1.iaa.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Set;

import com.mv1.iaa.domain.AvailableCarriers;
import com.mv1.iaa.domain.enumeration.ProfileType;

/**
 * A DTO for the Profile entity.
 */
public class ProfileDTO implements Serializable {

    private Long id;

    private String firstName;

    private String middleName;

    private String lastName;

    private String prefMobile;

    private String prefEmail;

    private String refBy;

    private String suffix;

    private ProfileType type;

    private Boolean isSpeaksSpanish;

    public Boolean getIsSpeaksSpanish() {
		return isSpeaksSpanish;
	}

	private Long userId;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private Boolean isActive;

    private Boolean isDeleted;

    private Long addrId;

    private String phone;
    
    private String prefCompany;

    private String comments;
    
    private String insuranceCompany;
    
    private Set<AvailableCarriers> carriers;

    private String secondPhone;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPrefMobile() {
        return prefMobile;
    }

    public void setPrefMobile(String prefMobile) {
        this.prefMobile = prefMobile;
    }

    public String getPrefEmail() {
        return prefEmail;
    }

    public void setPrefEmail(String prefEmail) {
        this.prefEmail = prefEmail;
    }

    public String getRefBy() {
        return refBy;
    }

    public void setRefBy(String refBy) {
        this.refBy = refBy;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public ProfileType getType() {
        return type;
    }

    public void setType(ProfileType type) {
        this.type = type;
    }

    public Boolean isIsSpeaksSpanish() {
        return isSpeaksSpanish;
    }

    public void setIsSpeaksSpanish(Boolean isSpeaksSpanish) {
        this.isSpeaksSpanish = isSpeaksSpanish;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getAddrId() {
        return addrId;
    }

    public void setAddrId(Long addressId) {
        this.addrId = addressId;
    }
    
    public String getPrefCompany() {
		return prefCompany;
	}

	public String getComments() {
		return comments;
	}

	public void setPrefCompany(String prefCompany) {
		this.prefCompany = prefCompany;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}
	
	

	public Set<AvailableCarriers> getCarriers() {
		return carriers;
	}

	public void setCarriers(Set<AvailableCarriers> carriers) {
		this.carriers = carriers;
	}

	
	public String getSecondPhone() {
		return secondPhone;
	}

	public void setSecondPhone(String secondPhone) {
		this.secondPhone = secondPhone;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProfileDTO profileDTO = (ProfileDTO) o;
        if (profileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), profileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProfileDTO{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", middleName='" + getMiddleName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", prefMobile='" + getPrefMobile() + "'" +
            ", prefEmail='" + getPrefEmail() + "'" +
            ", refBy='" + getRefBy() + "'" +
            ", suffix='" + getSuffix() + "'" +
            ", type='" + getType() + "'" +
            ", isSpeaksSpanish='" + isIsSpeaksSpanish() + "'" +
            ", userId=" + getUserId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            ", phone='" + getPhone() + "'" +
            ", addr=" + getAddrId() +
            "}";
    }
}
