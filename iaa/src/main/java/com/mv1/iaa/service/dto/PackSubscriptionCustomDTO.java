package com.mv1.iaa.service.dto;

import java.io.Serializable;

/**
 * A DTO for the PackSubscription entity.
 */
public class PackSubscriptionCustomDTO implements Serializable {

	private Long id;

	private Long packageId;

	private Long zipAreaId;

	private Integer slots;

	private String duration;

	private String createdAt;

	private String updatedAt;

	private String validTill;

	private Boolean isActive;

	private Boolean isDeleted;

	private Long profileId;

	private String zipcode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public Long getZipAreaId() {
		return zipAreaId;
	}

	public void setZipAreaId(Long zipAreaId) {
		this.zipAreaId = zipAreaId;
	}

	public Integer getSlots() {
		return slots;
	}

	public void setSlots(Integer slots) {
		this.slots = slots;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public String getValidTill() {
		return validTill;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public Long getProfileId() {
		return profileId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setValidTill(String validTill) {
		this.validTill = validTill;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	@Override
	public String toString() {
		return "PackSubscriptionCustomDTO [id=" + id + ", packageId=" + packageId + ", zipAreaId=" + zipAreaId
				+ ", slots=" + slots + ", duration=" + duration + ", createdAt=" + createdAt + ", updatedAt="
				+ updatedAt + ", validTill=" + validTill + ", isActive=" + isActive + ", isDeleted=" + isDeleted
				+ ", profileId=" + profileId + ", zipcode=" + zipcode + "]";
	}

}
