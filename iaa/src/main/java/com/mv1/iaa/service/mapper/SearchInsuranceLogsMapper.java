package com.mv1.iaa.service.mapper;

import com.mv1.iaa.domain.*;
import com.mv1.iaa.service.dto.SearchInsuranceLogsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SearchInsuranceLogs and its DTO SearchInsuranceLogsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SearchInsuranceLogsMapper extends EntityMapper<SearchInsuranceLogsDTO, SearchInsuranceLogs> {



    default SearchInsuranceLogs fromId(Long id) {
        if (id == null) {
            return null;
        }
        SearchInsuranceLogs searchInsuranceLogs = new SearchInsuranceLogs();
        searchInsuranceLogs.setId(id);
        return searchInsuranceLogs;
    }
}
