package com.mv1.iaa.service.dto;

public class CustomFilterSearchDTO {

	private String colName;
	
	private String sortType;

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	@Override
	public String toString() {
		return "CustomFilterSearchDTO [colName=" + colName + ", sortType=" + sortType + "]";
	}

	
	

	
}

