package com.mv1.iaa.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.mv1.iaa.domain.Profile;
import com.mv1.iaa.service.dto.ProfileDTO;

/**
 * Mapper for the entity Profile and its DTO ProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {AddressMapper.class})
public interface ProfileMapper extends EntityMapper<ProfileDTO, Profile> {

    @Mapping(source = "addr.id", target = "addrId")
    ProfileDTO toDto(Profile profile);

    @Mapping(source = "addrId", target = "addr")
    @Mapping(target = "carts", ignore = true)
    Profile toEntity(ProfileDTO profileDTO);

    default Profile fromId(Long id) {
        if (id == null) {
            return null;
        }
        Profile profile = new Profile();
        profile.setId(id);
        return profile;
    }
}
