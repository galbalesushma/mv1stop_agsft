package com.mv1.iaa.service;

import java.util.List;
import java.util.Optional;

import com.mv1.iaa.service.dto.SearchLogsDTO;

/**
 * Service Interface for managing SearchLogs.
 */
public interface SearchLogsService {

    /**
     * Save a searchLogs.
     *
     * @param searchLogsDTO the entity to save
     * @return the persisted entity
     */
    SearchLogsDTO save(SearchLogsDTO searchLogsDTO);

    /**
     * Get all the searchLogs.
     *
     * @return the list of entities
     */
    List<SearchLogsDTO> findAll();


    /**
     * Get the "id" searchLogs.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SearchLogsDTO> findOne(Long id);

    /**
     * Delete the "id" searchLogs.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
