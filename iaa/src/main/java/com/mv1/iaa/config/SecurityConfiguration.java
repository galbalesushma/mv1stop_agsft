package com.mv1.iaa.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CorsFilter;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

import com.mv1.iaa.security.AuthoritiesConstants;
import com.mv1.iaa.security.jwt.JWTConfigurer;
import com.mv1.iaa.security.jwt.TokenProvider;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import(SecurityProblemSupport.class)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final UserDetailsService userDetailsService;

    private final TokenProvider tokenProvider;

    private final CorsFilter corsFilter;

    private final SecurityProblemSupport problemSupport;

    public SecurityConfiguration(AuthenticationManagerBuilder authenticationManagerBuilder, UserDetailsService userDetailsService, TokenProvider tokenProvider, CorsFilter corsFilter, SecurityProblemSupport problemSupport) {
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userDetailsService = userDetailsService;
        this.tokenProvider = tokenProvider;
        this.corsFilter = corsFilter;
        this.problemSupport = problemSupport;
    }

    @PostConstruct
    public void init() {
        try {
            authenticationManagerBuilder
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
        } catch (Exception e) {
            throw new BeanInitializationException("Security configuration failed", e);
        }
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/app/**/*.{js,html}")
            .antMatchers("/api/open/**")
            .antMatchers("/api/search/**")
            .antMatchers("/api/clickToCall/**")
            .antMatchers("/api/insurance/companyList/**")
            .antMatchers("/api/agent/companyList/**")
            .antMatchers("/api/insurance/companyListByName/**")
            .antMatchers("/api/agent/companyListByLastName/**")
            .antMatchers("/api/insurance/LpaList/**")
            .antMatchers("/api/insurance/DmvList/**")
            .antMatchers("/api/search-insurance-logs/**")
            .antMatchers("/api//search/insurance/list/db/**")
            .antMatchers("/api/agent/call/log/**")
            /*.antMatchers("/api/csv/**")
            .antMatchers("/api/facility/**")
            .antMatchers("/api/payment/**")
            .antMatchers("/api/cart/**")
            .antMatchers("/api/agent/**")*/
            .antMatchers("/i18n/**")
            .antMatchers("/content/**")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .addFilterBefore(corsFilter, CsrfFilter.class)
            .exceptionHandling()
            .authenticationEntryPoint(problemSupport)
            .accessDeniedHandler(problemSupport)
        .and()
            .csrf()
            .disable()
            .headers()
            .frameOptions()
            .disable()
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            .antMatchers("/api/register").permitAll()
            .antMatchers("/api/activate").permitAll()
            .antMatchers("/api/open/**").permitAll()
            .antMatchers("/api/search/**").permitAll()
            .antMatchers("/api/clickToCall/**").permitAll()
            /*.antMatchers("/api/csv/**").permitAll()
            .antMatchers("/api/facility/**").permitAll()
            .antMatchers("/api/payment/**").permitAll()
            .antMatchers("/api/cart/**").permitAll()
            .antMatchers("/api/agent/**").permitAll()*/
            .antMatchers("/api/csv/open/download/sample").permitAll()
            .antMatchers("/api/authenticate").permitAll()
            .antMatchers("/api/account/reset-password/init/**").permitAll()
            .antMatchers("/api/account/reset-password/finish").permitAll()
            .antMatchers("/api/**").authenticated()
            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/info").permitAll()
            .antMatchers("/api/insurance/companyList/**").permitAll()
            .antMatchers("/api/agent/companyList/**").permitAll()
            .antMatchers("/api/insurance/companyListByName/**").permitAll()
            .antMatchers("/api/agent/companyListByLastName/**").permitAll()
            .antMatchers("/api/insurance/LpaList/**").permitAll()
            .antMatchers("/api/insurance/DmvList/**").permitAll()
            .antMatchers("/api/search-insurance-logs/**").permitAll()
            .antMatchers("/api//search/insurance/list/db/**").permitAll()
            .antMatchers("/api/agent/call/log/**").permitAll()
            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
            
            
        .and()
            .apply(securityConfigurerAdapter());

    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }
}
