package com.mv1.iaa.config;

import java.time.Duration;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.mv1.iaa.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.mv1.iaa.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Profile.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Address.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Contact.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Facility.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Agent.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Payment.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.ZipArea.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.AgentDistribution.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.PricePackage.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Cart.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Cart.class.getName() + ".pricePacks", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Cart.class.getName() + ".zips", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Profile.class.getName() + ".carts", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Address.class.getName() + ".contacts", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Payment.class.getName() + ".pricePacks", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Business.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Business.class.getName() + ".agents", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.State.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.County.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.City.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Producer.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Producer.class.getName() + ".licenses", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.AgentLic.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.AgentType.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Agent.class.getName() + ".types", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.PackSubscription.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Cart.class.getName() + ".subscriptions", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.Business.class.getName() + ".facilities", jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.SearchLogs.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.DMVLocation.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.LPALocations.class.getName(), jcacheConfiguration);
            cm.createCache(com.mv1.iaa.domain.SearchInsuranceLogs.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
