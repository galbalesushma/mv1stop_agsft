package com.mv1.iaa.domain.enumeration;

/**
 * The LicenseType enumeration.
 */
public enum LicenseType {
    INSURANCE_PRODUCER
}
