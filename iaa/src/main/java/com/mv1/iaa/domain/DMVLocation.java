package com.mv1.iaa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DMVLocation.
 */
@Entity
@Table(name = "dmv_location")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DMVLocation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "country")
    private String country;

    @Column(name = "main_phone")
    private String mainPhone;

    @Column(name = "fax_number")
    private String faxNumber;

    @Column(name = "weekdays_hrs")
    private String weekdaysHrs;

    @Column(name = "saturday_hrs")
    private String saturdayHrs;

    @Column(name = "directions")
    private String directions;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public DMVLocation name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public DMVLocation address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public DMVLocation city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public DMVLocation state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public DMVLocation postalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public DMVLocation country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMainPhone() {
        return mainPhone;
    }

    public DMVLocation mainPhone(String mainPhone) {
        this.mainPhone = mainPhone;
        return this;
    }

    public void setMainPhone(String mainPhone) {
        this.mainPhone = mainPhone;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public DMVLocation faxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
        return this;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getWeekdaysHrs() {
        return weekdaysHrs;
    }

    public DMVLocation weekdaysHrs(String weekdaysHrs) {
        this.weekdaysHrs = weekdaysHrs;
        return this;
    }

    public void setWeekdaysHrs(String weekdaysHrs) {
        this.weekdaysHrs = weekdaysHrs;
    }

    public String getSaturdayHrs() {
        return saturdayHrs;
    }

    public DMVLocation saturdayHrs(String saturdayHrs) {
        this.saturdayHrs = saturdayHrs;
        return this;
    }

    public void setSaturdayHrs(String saturdayHrs) {
        this.saturdayHrs = saturdayHrs;
    }

    public String getDirections() {
        return directions;
    }

    public DMVLocation directions(String directions) {
        this.directions = directions;
        return this;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DMVLocation dMVLocation = (DMVLocation) o;
        if (dMVLocation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dMVLocation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DMVLocation{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", country='" + getCountry() + "'" +
            ", mainPhone='" + getMainPhone() + "'" +
            ", faxNumber='" + getFaxNumber() + "'" +
            ", weekdaysHrs='" + getWeekdaysHrs() + "'" +
            ", saturdayHrs='" + getSaturdayHrs() + "'" +
            ", directions='" + getDirections() + "'" +
            "}";
    }
}
