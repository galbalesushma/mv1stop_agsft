package com.mv1.iaa.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Agent.
 */
@Entity
@Table(name = "agent")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Agent implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "npn", nullable = false)
	private String npn;

	@Column(name = "naicno")
	private String naicno;

	@Column(name = "fein")
	private String fein;

	@Column(name = "resident_state")
	private Boolean residentState;

	@Column(name = "created_at")
	private ZonedDateTime createdAt;

	@Column(name = "updated_at")
	private ZonedDateTime updatedAt;

	@Column(name = "verified_at")
	private ZonedDateTime verifiedAt;

	@Column(name = "is_verified")
	private Boolean isVerified;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_deleted")
	private Boolean isDeleted;

	@OneToOne
	@JoinColumn(unique = true)
	private Profile profile;

	@OneToOne
	@JoinColumn(unique = true)
	private AgentLic lic;

	@OneToMany(mappedBy = "agent")
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<AgentType> types = new HashSet<>();

	@ManyToOne
	@JsonIgnoreProperties("")
	private State domicileState;

	@ManyToOne
	@JsonIgnoreProperties("agents")
	private Business business;

	@Column(name = "extension")
	private String extension;
	
	@OneToMany(mappedBy = "agent")
	private Set<AgentExtension> agentExtensions = new HashSet<>();

	
	
	public String getExtension() {
		return extension;
	}


	public void setExtension(String extension) {
		this.extension = extension;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNpn() {
		return npn;
	}

	public Agent npn(String npn) {
		this.npn = npn;
		return this;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}

	public String getNaicno() {
		return naicno;
	}

	public Agent naicno(String naicno) {
		this.naicno = naicno;
		return this;
	}

	public void setNaicno(String naicno) {
		this.naicno = naicno;
	}

	public String getFein() {
		return fein;
	}

	public Agent fein(String fein) {
		this.fein = fein;
		return this;
	}

	public void setFein(String fein) {
		this.fein = fein;
	}

	public Boolean isResidentState() {
		return residentState;
	}

	public Agent residentState(Boolean residentState) {
		this.residentState = residentState;
		return this;
	}

	public void setResidentState(Boolean residentState) {
		this.residentState = residentState;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public Agent createdAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public ZonedDateTime getUpdatedAt() {
		return updatedAt;
	}

	public Agent updatedAt(ZonedDateTime updatedAt) {
		this.updatedAt = updatedAt;
		return this;
	}

	public void setUpdatedAt(ZonedDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public ZonedDateTime getVerifiedAt() {
		return verifiedAt;
	}

	public Agent verifiedAt(ZonedDateTime verifiedAt) {
		this.verifiedAt = verifiedAt;
		return this;
	}

	public void setVerifiedAt(ZonedDateTime verifiedAt) {
		this.verifiedAt = verifiedAt;
	}

	public Boolean isIsVerified() {
		return isVerified;
	}

	public Agent isVerified(Boolean isVerified) {
		this.isVerified = isVerified;
		return this;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Boolean isIsActive() {
		return isActive;
	}

	public Agent isActive(Boolean isActive) {
		this.isActive = isActive;
		return this;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean isIsDeleted() {
		return isDeleted;
	}

	public Agent isDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
		return this;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Profile getProfile() {
		return profile;
	}

	public Agent profile(Profile profile) {
		this.profile = profile;
		return this;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public AgentLic getLic() {
		return lic;
	}

	public Agent lic(AgentLic agentLic) {
		this.lic = agentLic;
		return this;
	}

	public void setLic(AgentLic agentLic) {
		this.lic = agentLic;
	}

	public Set<AgentType> getTypes() {
		return types;
	}

	public Agent types(Set<AgentType> agentTypes) {
		this.types = agentTypes;
		return this;
	}

	public Agent addTypes(AgentType agentType) {
		this.types.add(agentType);
		agentType.setAgent(this);
		return this;
	}

	public Agent removeTypes(AgentType agentType) {
		this.types.remove(agentType);
		agentType.setAgent(null);
		return this;
	}

	public void setTypes(Set<AgentType> agentTypes) {
		this.types = agentTypes;
	}

	public State getDomicileState() {
		return domicileState;
	}

	public Agent domicileState(State state) {
		this.domicileState = state;
		return this;
	}

	public void setDomicileState(State state) {
		this.domicileState = state;
	}

	public Business getBusiness() {
		return business;
	}

	public Agent business(Business business) {
		this.business = business;
		return this;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}
	
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	public Set<AgentExtension> getAgentExtensions() {
		return agentExtensions;
	}

	public void setAgentExtensions(Set<AgentExtension> agentExtensions) {
		this.agentExtensions = agentExtensions;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Agent agent = (Agent) o;
		if (agent.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), agent.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Agent [id=" + id + ", npn=" + npn + ", naicno=" + naicno + ", fein=" + fein + ", residentState="
				+ residentState + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", verifiedAt=" + verifiedAt
				+ ", isVerified=" + isVerified + ", isActive=" + isActive + ", isDeleted=" + isDeleted + ", profile="
				+ profile + ", lic=" + lic + ", types=" + types + ", domicileState=" + domicileState + ", business="
				+ business + ", extension=" + extension + "]";
	}
}
