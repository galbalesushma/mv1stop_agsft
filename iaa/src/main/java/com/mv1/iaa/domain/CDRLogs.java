package com.mv1.iaa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cdr_logs")
public class CDRLogs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4516140807780268287L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "file_path")
	String filePath;

	@Column(name = "parent_dir_filter_path")
	String parentDirFilterPath;

	@Column(name = "file_created_at")
	long fileCreatedAt;

	@Column(name = "file_last_modified_at")
	long fileLastModifiedAt;

	@Column(name = "file_log_path")
	String fileLogPath;

	@Column(name = "process_status")
	String processStatus;

	@Column(name = "error_message")
	String errorMessage;
	
	@Column(name = "record_count")
	int recordCount;
	
	@Column(name = "execution_time")
	long executionTime;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getParentDirFilterPath() {
		return parentDirFilterPath;
	}

	public long getFileCreatedAt() {
		return fileCreatedAt;
	}

	public long getFileLastModifiedAt() {
		return fileLastModifiedAt;
	}

	public String getFileLogPath() {
		return fileLogPath;
	}

	public String getProcessStatus() {
		return processStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setParentDirFilterPath(String parentDirFilterPath) {
		this.parentDirFilterPath = parentDirFilterPath;
	}

	public void setFileCreatedAt(long fileCreatedAt) {
		this.fileCreatedAt = fileCreatedAt;
	}

	public void setFileLastModifiedAt(long fileLastModifiedAt) {
		this.fileLastModifiedAt = fileLastModifiedAt;
	}

	public void setFileLogPath(String fileLogPath) {
		this.fileLogPath = fileLogPath;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	
	public long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((errorMessage == null) ? 0 : errorMessage.hashCode());
		result = prime * result + (int) (fileCreatedAt ^ (fileCreatedAt >>> 32));
		result = prime * result + (int) (fileLastModifiedAt ^ (fileLastModifiedAt >>> 32));
		result = prime * result + ((fileLogPath == null) ? 0 : fileLogPath.hashCode());
		result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((parentDirFilterPath == null) ? 0 : parentDirFilterPath.hashCode());
		result = prime * result + ((processStatus == null) ? 0 : processStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CDRLogs other = (CDRLogs) obj;
		if (errorMessage == null) {
			if (other.errorMessage != null)
				return false;
		} else if (!errorMessage.equals(other.errorMessage))
			return false;
		if (fileCreatedAt != other.fileCreatedAt)
			return false;
		if (fileLastModifiedAt != other.fileLastModifiedAt)
			return false;
		if (fileLogPath == null) {
			if (other.fileLogPath != null)
				return false;
		} else if (!fileLogPath.equals(other.fileLogPath))
			return false;
		if (filePath == null) {
			if (other.filePath != null)
				return false;
		} else if (!filePath.equals(other.filePath))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (parentDirFilterPath == null) {
			if (other.parentDirFilterPath != null)
				return false;
		} else if (!parentDirFilterPath.equals(other.parentDirFilterPath))
			return false;
		if (processStatus == null) {
			if (other.processStatus != null)
				return false;
		} else if (!processStatus.equals(other.processStatus))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CDRLogs [id=" + id + ", filePath=" + filePath + ", parentDirFilterPath=" + parentDirFilterPath
				+ ", fileCreatedAt=" + fileCreatedAt + ", fileLastModifiedAt=" + fileLastModifiedAt + ", fileLogPath="
				+ fileLogPath + ", processStatus=" + processStatus + ", errorMessage=" + errorMessage + "]";
	}

}
