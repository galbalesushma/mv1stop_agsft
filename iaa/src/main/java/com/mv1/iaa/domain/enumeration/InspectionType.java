package com.mv1.iaa.domain.enumeration;

/**
 * The InspectionType enumeration.
 */
public enum InspectionType {
    EMISSION, SAFETY
}
