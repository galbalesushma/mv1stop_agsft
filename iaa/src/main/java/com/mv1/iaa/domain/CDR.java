
package com.mv1.iaa.domain;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cdr")
public class CDR {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "calldate")
	private ZonedDateTime callDate;

	@Column(name = "clid")
	private String clid;

	@Column(name = "src")
	private String src;

	@Column(name = "dst")
	private String dst;

	@Column(name = "dcontext")
	private String dcontext;

	@Column(name = "channel")
	private String channel;

	@Column(name = "dstchannel")
	private String dstchannel;

	@Column(name = "lastapp")
	private String lastapp;

	@Column(name = "lastdata")
	private String lastdata;

	@Column(name = "duration")
	private String duration;

	@Column(name = "billsec")
	private String billsec;

	@Column(name = "disposition")
	private String disposition;

	@Column(name = "amaflags")
	private String amaflags;

	@Column(name = "accountcode")
	private String accountcode;

	@Column(name = "uniqueid")
	private String uniqueid;

	@Column(name = "userfield")
	private String userfield;

	@Column(name = "did")
	private String did;

	@Column(name = "recordingfile")
	private String recordingfile;

	@Column(name = "cnum")
	private String cnum;

	@Column(name = "cnam")
	private String cnam;

	@Column(name = "outbound_cnum")
	private String outbound_cnum;

	@Column(name = "outbound_cnam")
	private String outbound_cnam;

	@Column(name = "dst_cnam")
	private String dst_cnam;

	@Column(name = "linkedid")
	private String linkedid;

	@Column(name = "peeraccount")
	private String peeraccount;

	@Column(name = "sequence")
	private String sequence;

	public Long getId() {
		return id;
	}

	public ZonedDateTime getCallDate() {
		return callDate;
	}

	public String getClid() {
		return clid;
	}

	public String getSrc() {
		return src;
	}

	public String getDst() {
		return dst;
	}

	public String getDcontext() {
		return dcontext;
	}

	public String getChannel() {
		return channel;
	}

	public String getDstchannel() {
		return dstchannel;
	}

	public String getLastapp() {
		return lastapp;
	}

	public String getLastdata() {
		return lastdata;
	}

	public String getDuration() {
		return duration;
	}

	public String getBillsec() {
		return billsec;
	}

	public String getDisposition() {
		return disposition;
	}

	public String getAmaflags() {
		return amaflags;
	}

	public String getAccountcode() {
		return accountcode;
	}

	public String getUniqueid() {
		return uniqueid;
	}

	public String getUserfield() {
		return userfield;
	}

	public String getDid() {
		return did;
	}

	public String getRecordingfile() {
		return recordingfile;
	}

	public String getCnum() {
		return cnum;
	}

	public String getCnam() {
		return cnam;
	}

	public String getOutbound_cnum() {
		return outbound_cnum;
	}

	public String getOutbound_cnam() {
		return outbound_cnam;
	}

	public String getDst_cnam() {
		return dst_cnam;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCallDate(ZonedDateTime callDate) {
		this.callDate = callDate;
	}

	public void setClid(String clid) {
		this.clid = clid;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}

	public void setDcontext(String dcontext) {
		this.dcontext = dcontext;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public void setDstchannel(String dstchannel) {
		this.dstchannel = dstchannel;
	}

	public void setLastapp(String lastapp) {
		this.lastapp = lastapp;
	}

	public void setLastdata(String lastdata) {
		this.lastdata = lastdata;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setBillsec(String billsec) {
		this.billsec = billsec;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public void setAmaflags(String amaflags) {
		this.amaflags = amaflags;
	}

	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}

	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}

	public void setUserfield(String userfield) {
		this.userfield = userfield;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public void setRecordingfile(String recordingfile) {
		this.recordingfile = recordingfile;
	}

	public void setCnum(String cnum) {
		this.cnum = cnum;
	}

	public void setCnam(String cnam) {
		this.cnam = cnam;
	}

	public void setOutbound_cnum(String outbound_cnum) {
		this.outbound_cnum = outbound_cnum;
	}

	public void setOutbound_cnam(String outbound_cnam) {
		this.outbound_cnam = outbound_cnam;
	}

	public void setDst_cnam(String dst_cnam) {
		this.dst_cnam = dst_cnam;
	}

	public String getLinkedid() {
		return linkedid;
	}

	public void setLinkedid(String linkedid) {
		this.linkedid = linkedid;
	}

	public String getPeeraccount() {
		return peeraccount;
	}

	public void setPeeraccount(String peeraccount) {
		this.peeraccount = peeraccount;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	@Override
	public String toString() {
		return "CDR [id=" + id + ", callDate=" + callDate + ", clid=" + clid + ", src=" + src + ", dst=" + dst
				+ ", dcontext=" + dcontext + ", channel=" + channel + ", dstchannel=" + dstchannel + ", lastapp="
				+ lastapp + ", lastdata=" + lastdata + ", duration=" + duration + ", billsec=" + billsec
				+ ", disposition=" + disposition + ", amaflags=" + amaflags + ", accountcode=" + accountcode
				+ ", uniqueid=" + uniqueid + ", userfield=" + userfield + ", did=" + did + ", recordingfile="
				+ recordingfile + ", cnum=" + cnum + ", cnam=" + cnam + ", outbound_cnum=" + outbound_cnum
				+ ", outbound_cnam=" + outbound_cnam + ", dst_cnam=" + dst_cnam + ", linkedid=" + linkedid
				+ ", peeraccount=" + peeraccount + ", sequence=" + sequence + "]";
	}

}
