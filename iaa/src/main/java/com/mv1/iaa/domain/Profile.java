package com.mv1.iaa.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mv1.iaa.domain.enumeration.ProfileType;

/**
 * A Profile.
 */
@Entity
@Table(name = "profile")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "pref_mobile")
    private String prefMobile;

    @Column(name = "pref_email")
    private String prefEmail;

    @Column(name = "ref_by")
    private String refBy;

    @Column(name = "suffix")
    private String suffix;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private ProfileType type;

    @Column(name = "is_speaks_spanish")
    private Boolean isSpeaksSpanish;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Column(name = "phone")
    private String phone;

    @OneToOne
    @JoinColumn(unique = true)
    private Address addr;
    
    @Column(name = "pref_company")
    private String prefCompany;
    
    @Column(name = "comments")
    private String comments;
    
    @Column(name = "insurance_company")
    private String insuranceCompany;
    
    @OneToMany(mappedBy = "profile")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Cart> carts = new HashSet<>();


    @ManyToMany
//    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "profile_carriers",
               joinColumns = @JoinColumn(name = "profile_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "carriers_id", referencedColumnName = "id"))
    private Set<AvailableCarriers> carriers = new HashSet<>();
    
    
    @Column(name = "second_phone")
    private String secondPhone;
    
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Profile firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Profile middleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public Profile lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPrefMobile() {
        return prefMobile;
    }

    public Profile prefMobile(String prefMobile) {
        this.prefMobile = prefMobile;
        return this;
    }

    public void setPrefMobile(String prefMobile) {
        this.prefMobile = prefMobile;
    }

    public String getPrefEmail() {
        return prefEmail;
    }

    public Profile prefEmail(String prefEmail) {
        this.prefEmail = prefEmail;
        return this;
    }

    public void setPrefEmail(String prefEmail) {
        this.prefEmail = prefEmail;
    }

    public String getRefBy() {
        return refBy;
    }

    public Profile refBy(String refBy) {
        this.refBy = refBy;
        return this;
    }

    public void setRefBy(String refBy) {
        this.refBy = refBy;
    }

    public String getSuffix() {
        return suffix;
    }

    public Profile suffix(String suffix) {
        this.suffix = suffix;
        return this;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public ProfileType getType() {
        return type;
    }

    public Profile type(ProfileType type) {
        this.type = type;
        return this;
    }

    public void setType(ProfileType type) {
        this.type = type;
    }

    public Boolean isIsSpeaksSpanish() {
        return isSpeaksSpanish;
    }

    public Profile isSpeaksSpanish(Boolean isSpeaksSpanish) {
        this.isSpeaksSpanish = isSpeaksSpanish;
        return this;
    }

    public void setIsSpeaksSpanish(Boolean isSpeaksSpanish) {
        this.isSpeaksSpanish = isSpeaksSpanish;
    }

    public Long getUserId() {
        return userId;
    }

    public Profile userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Profile createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Profile updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public Profile isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public Profile isDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getPhone() {
        return phone;
    }

    public Profile phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address getAddr() {
        return addr;
    }

    public Profile addr(Address address) {
        this.addr = address;
        return this;
    }

    public void setAddr(Address address) {
        this.addr = address;
    }

    public Set<Cart> getCarts() {
        return carts;
    }

    public Profile carts(Set<Cart> carts) {
        this.carts = carts;
        return this;
    }

    public Profile addCarts(Cart cart) {
        this.carts.add(cart);
        cart.setProfile(this);
        return this;
    }

    public Profile removeCarts(Cart cart) {
        this.carts.remove(cart);
        cart.setProfile(null);
        return this;
    }

    public void setCarts(Set<Cart> carts) {
        this.carts = carts;
    }
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public String getPrefCompany() {
		return prefCompany;
	}

	public String getComments() {
		return comments;
	}

	public void setPrefCompany(String prefCompany) {
		this.prefCompany = prefCompany;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public Set<AvailableCarriers> getCarriers() {
		return carriers;
	}

	public void setCarriers(Set<AvailableCarriers> carriers) {
		this.carriers = carriers;
	}

	public String getSecondPhone() {
		return secondPhone;
	}

	public void setSecondPhone(String secondPhone) {
		this.secondPhone = secondPhone;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Profile profile = (Profile) o;
        if (profile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), profile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Profile{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", middleName='" + getMiddleName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", prefMobile='" + getPrefMobile() + "'" +
            ", prefEmail='" + getPrefEmail() + "'" +
            ", refBy='" + getRefBy() + "'" +
            ", suffix='" + getSuffix() + "'" +
            ", type='" + getType() + "'" +
            ", isSpeaksSpanish='" + isIsSpeaksSpanish() + "'" +
            ", userId=" + getUserId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }
}
