package com.mv1.iaa.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "available_carriers")
public class AvailableCarriers {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

    
	@Column(name = "carrier")
	private String carrier;
	
	@Column(name = "show_in_list")
	private boolean showInList;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public boolean isShowInList() {
		return showInList;
	}

	public void setShowInList(boolean showInList) {
		this.showInList = showInList;
	}

}
