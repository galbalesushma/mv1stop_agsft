package com.mv1.iaa.domain;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "agent_call_log")
public class AgentCallLog {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "calldate")
	private ZonedDateTime callDate;

	@Column(name = "user_number")
	private String userNumber;

	@Column(name = "agent_number")
	private String agentNumber;

	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "npn")
	private String npn;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getCallDate() {
		return callDate;
	}

	public void setCallDate(ZonedDateTime callDate) {
		this.callDate = callDate;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	public String getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNpn() {
		return npn;
	}

	public void setNpn(String npn) {
		this.npn = npn;
	}


	
}
