package com.mv1.iaa.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A SearchLogs.
 */
@Entity
@Table(name = "search_logs")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SearchLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "full_name", nullable = false)
    private String fullName;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "phone", nullable = false)
    private String phone;

    @NotNull
    @Column(name = "zip", nullable = false)
    private String zip;

    @Column(name = "search_type")
    private String searchType;

    @Column(name = "searched_date")
    private ZonedDateTime searchedDate;
    
    @Column(name = "vehicle_type", nullable = false)
    private String vehicleType;

    @Column(name = "vehicle_year", nullable = false)
    private String vehicleYear;

    @Column(name = "county", nullable = false)
    private String county;

    @Column(name = "company", nullable = false)
    private String company;
    

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public SearchLogs fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public SearchLogs email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public SearchLogs phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZip() {
        return zip;
    }

    public SearchLogs zip(String zip) {
        this.zip = zip;
        return this;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getSearchType() {
        return searchType;
    }

    public SearchLogs searchType(String searchType) {
        this.searchType = searchType;
        return this;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public ZonedDateTime getSearchedDate() {
        return searchedDate;
    }

    public SearchLogs searchedDate(ZonedDateTime searchedDate) {
        this.searchedDate = searchedDate;
        return this;
    }

    public void setSearchedDate(ZonedDateTime searchedDate) {
        this.searchedDate = searchedDate;
    }
    
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public String getVehicleYear() {
		return vehicleYear;
	}

	public String getCounty() {
		return county;
	}

	public String getCompany() {
		return company;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public void setVehicleYear(String vehicleYear) {
		this.vehicleYear = vehicleYear;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SearchLogs searchLogs = (SearchLogs) o;
        if (searchLogs.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), searchLogs.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return "SearchLogs [id=" + id + ", fullName=" + fullName + ", email=" + email + ", phone=" + phone + ", zip="
				+ zip + ", searchType=" + searchType + ", searchedDate=" + searchedDate + ", vehicleType=" + vehicleType
				+ ", vehicleYear=" + vehicleYear + ", county=" + county + ", company=" + company + "]";
	}

}
