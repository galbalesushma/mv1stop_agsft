package com.mv1.iaa.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mv1.iaa.domain.enumeration.InspectionType;

/**
 * A Facility.
 */
@Entity
@Table(name = "facility")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Facility implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "isn")
	private String isn;

	@Enumerated(EnumType.STRING)
	@Column(name = "jhi_type")
	private InspectionType type;

	@Column(name = "website")
	private String website;

	@Column(name = "weekday_hours")
	private String weekdayHours;

	@Column(name = "saturday_hours")
	private String saturdayHours;

	@Column(name = "sunday_hours")
	private String sundayHours;

	@Column(name = "created_at")
	private ZonedDateTime createdAt;

	@Column(name = "updated_at")
	private ZonedDateTime updatedAt;

	@Column(name = "verified_at")
	private ZonedDateTime verifiedAt;

	@Column(name = "is_verified")
	private Boolean isVerified;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_deleted")
	private Boolean isDeleted;

	@OneToOne
	@JoinColumn(unique = true)
	private Profile profile;

	@Column(name = "insp_type")
	private String inspType;

	@Column(name = "mech_duty")
	private String mechDuty;

	@ManyToOne
	@JsonIgnoreProperties("facilities")
	private Business business;

	@Column(name = "extension")
	private String extension;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Facility name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsn() {
		return isn;
	}

	public Facility isn(String isn) {
		this.isn = isn;
		return this;
	}

	public void setIsn(String isn) {
		this.isn = isn;
	}

	public InspectionType getType() {
		return type;
	}

	public Facility type(InspectionType type) {
		this.type = type;
		return this;
	}

	public void setType(InspectionType type) {
		this.type = type;
	}

	public String getWebsite() {
		return website;
	}

	public Facility website(String website) {
		this.website = website;
		return this;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getWeekdayHours() {
		return weekdayHours;
	}

	public Facility weekdayHours(String weekdayHours) {
		this.weekdayHours = weekdayHours;
		return this;
	}

	public void setWeekdayHours(String weekdayHours) {
		this.weekdayHours = weekdayHours;
	}

	public String getSaturdayHours() {
		return saturdayHours;
	}

	public Facility saturdayHours(String saturdayHours) {
		this.saturdayHours = saturdayHours;
		return this;
	}

	public void setSaturdayHours(String saturdayHours) {
		this.saturdayHours = saturdayHours;
	}

	public String getSundayHours() {
		return sundayHours;
	}

	public Facility sundayHours(String sundayHours) {
		this.sundayHours = sundayHours;
		return this;
	}

	public void setSundayHours(String sundayHours) {
		this.sundayHours = sundayHours;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public Facility createdAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public ZonedDateTime getUpdatedAt() {
		return updatedAt;
	}

	public Facility updatedAt(ZonedDateTime updatedAt) {
		this.updatedAt = updatedAt;
		return this;
	}

	public void setUpdatedAt(ZonedDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public ZonedDateTime getVerifiedAt() {
		return verifiedAt;
	}

	public Facility verifiedAt(ZonedDateTime verifiedAt) {
		this.verifiedAt = verifiedAt;
		return this;
	}

	public void setVerifiedAt(ZonedDateTime verifiedAt) {
		this.verifiedAt = verifiedAt;
	}

	public Boolean isIsVerified() {
		return isVerified;
	}

	public Facility isVerified(Boolean isVerified) {
		this.isVerified = isVerified;
		return this;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Boolean isIsActive() {
		return isActive;
	}

	public Facility isActive(Boolean isActive) {
		this.isActive = isActive;
		return this;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean isIsDeleted() {
		return isDeleted;
	}

	public Facility isDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
		return this;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Profile getProfile() {
		return profile;
	}

	public Facility profile(Profile profile) {
		this.profile = profile;
		return this;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Business getBusiness() {
		return business;
	}

	public Facility business(Business business) {
		this.business = business;
		return this;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	public Boolean getIsVerified() {
		return isVerified;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public String getInspType() {
		return inspType;
	}

	public String getMechDuty() {
		return mechDuty;
	}

	public void setInspType(String inspType) {
		this.inspType = inspType;
	}

	public void setMechDuty(String mechDuty) {
		this.mechDuty = mechDuty;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Facility facility = (Facility) o;
		if (facility.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), facility.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Facility [id=" + id + ", name=" + name + ", isn=" + isn + ", type=" + type + ", website=" + website
				+ ", weekdayHours=" + weekdayHours + ", saturdayHours=" + saturdayHours + ", sundayHours=" + sundayHours
				+ ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", verifiedAt=" + verifiedAt
				+ ", isVerified=" + isVerified + ", isActive=" + isActive + ", isDeleted=" + isDeleted + ", profile="
				+ profile + ", inspType=" + inspType + ", mechDuty=" + mechDuty + ", business=" + business
				+ ", extension=" + extension + "]";
	}
}
