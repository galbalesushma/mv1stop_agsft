package com.mv1.iaa.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Cart.
 */
@Entity
@Table(name = "cart")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Cart implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "payment_id")
    private Long paymentId;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "is_completed")
    private Boolean isCompleted;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "cart_subscriptions",
               joinColumns = @JoinColumn(name = "carts_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "subscriptions_id", referencedColumnName = "id"))
    private Set<PackSubscription> subscriptions = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("carts")
    private Profile profile;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public Cart paymentId(Long paymentId) {
        this.paymentId = paymentId;
        return this;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Cart createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Cart updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isIsCompleted() {
        return isCompleted;
    }

    public Cart isCompleted(Boolean isCompleted) {
        this.isCompleted = isCompleted;
        return this;
    }

    public void setIsCompleted(Boolean isCompleted) {
        this.isCompleted = isCompleted;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public Cart isDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Set<PackSubscription> getSubscriptions() {
        return subscriptions;
    }

    public Cart subscriptions(Set<PackSubscription> packSubscriptions) {
        this.subscriptions = packSubscriptions;
        return this;
    }

    public Cart addSubscriptions(PackSubscription packSubscription) {
        this.subscriptions.add(packSubscription);
        return this;
    }

    public Cart removeSubscriptions(PackSubscription packSubscription) {
        this.subscriptions.remove(packSubscription);
        return this;
    }

    public void setSubscriptions(Set<PackSubscription> packSubscriptions) {
        this.subscriptions = packSubscriptions;
    }

    public Profile getProfile() {
        return profile;
    }

    public Cart profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Cart cart = (Cart) o;
        if (cart.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cart.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Cart{" +
            "id=" + getId() +
            ", paymentId=" + getPaymentId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", isCompleted='" + isIsCompleted() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            "}";
    }
}
