package com.mv1.iaa.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A AgentLic.
 */
@Entity
@Table(name = "agent_lic")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AgentLic implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "lic_number")
    private String licNumber;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "authority")
    private String authority;

    @Column(name = "first_active_date")
    private ZonedDateTime firstActiveDate;

    @Column(name = "effective_date")
    private ZonedDateTime effectiveDate;

    @Column(name = "expiration_date")
    private ZonedDateTime expirationDate;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @ManyToOne
    @JsonIgnoreProperties("licenses")
    private Producer producer;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicNumber() {
        return licNumber;
    }

    public AgentLic licNumber(String licNumber) {
        this.licNumber = licNumber;
        return this;
    }

    public void setLicNumber(String licNumber) {
        this.licNumber = licNumber;
    }

    public String getType() {
        return type;
    }

    public AgentLic type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthority() {
        return authority;
    }

    public AgentLic authority(String authority) {
        this.authority = authority;
        return this;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public ZonedDateTime getFirstActiveDate() {
        return firstActiveDate;
    }

    public AgentLic firstActiveDate(ZonedDateTime firstActiveDate) {
        this.firstActiveDate = firstActiveDate;
        return this;
    }

    public void setFirstActiveDate(ZonedDateTime firstActiveDate) {
        this.firstActiveDate = firstActiveDate;
    }

    public ZonedDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public AgentLic effectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
        return this;
    }

    public void setEffectiveDate(ZonedDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public ZonedDateTime getExpirationDate() {
        return expirationDate;
    }

    public AgentLic expirationDate(ZonedDateTime expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public void setExpirationDate(ZonedDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public AgentLic createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public AgentLic updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public AgentLic isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public AgentLic isDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Producer getProducer() {
        return producer;
    }

    public AgentLic producer(Producer producer) {
        this.producer = producer;
        return this;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AgentLic agentLic = (AgentLic) o;
        if (agentLic.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), agentLic.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AgentLic{" +
            "id=" + getId() +
            ", licNumber='" + getLicNumber() + "'" +
            ", type='" + getType() + "'" +
            ", authority='" + getAuthority() + "'" +
            ", firstActiveDate='" + getFirstActiveDate() + "'" +
            ", effectiveDate='" + getEffectiveDate() + "'" +
            ", expirationDate='" + getExpirationDate() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            "}";
    }
}
