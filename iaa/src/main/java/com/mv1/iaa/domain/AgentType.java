package com.mv1.iaa.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A AgentType.
 */
@Entity
@Table(name = "agent_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AgentType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "zip_area_id")
    private Long zipAreaId;

    @ManyToOne
    @JsonIgnoreProperties("types")
    private Agent agent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public AgentType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getZipAreaId() {
        return zipAreaId;
    }

    public AgentType zipAreaId(Long zipAreaId) {
        this.zipAreaId = zipAreaId;
        return this;
    }

    public void setZipAreaId(Long zipAreaId) {
        this.zipAreaId = zipAreaId;
    }

    public Agent getAgent() {
        return agent;
    }

    public AgentType agent(Agent agent) {
        this.agent = agent;
        return this;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AgentType agentType = (AgentType) o;
        if (agentType.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), agentType.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AgentType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", zipAreaId=" + getZipAreaId() +
            "}";
    }
}
