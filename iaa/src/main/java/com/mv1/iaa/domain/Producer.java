package com.mv1.iaa.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Producer.
 */
@Entity
@Table(name = "producer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Producer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "ce_compliance")
    private Boolean ceCompliance;

    @OneToOne
    @JoinColumn(unique = true)
    private Address addr;

    @OneToMany(mappedBy = "producer")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AgentLic> licenses = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Producer name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isCeCompliance() {
        return ceCompliance;
    }

    public Producer ceCompliance(Boolean ceCompliance) {
        this.ceCompliance = ceCompliance;
        return this;
    }

    public void setCeCompliance(Boolean ceCompliance) {
        this.ceCompliance = ceCompliance;
    }

    public Address getAddr() {
        return addr;
    }

    public Producer addr(Address address) {
        this.addr = address;
        return this;
    }

    public void setAddr(Address address) {
        this.addr = address;
    }

    public Set<AgentLic> getLicenses() {
        return licenses;
    }

    public Producer licenses(Set<AgentLic> agentLics) {
        this.licenses = agentLics;
        return this;
    }

    public Producer addLicenses(AgentLic agentLic) {
        this.licenses.add(agentLic);
        agentLic.setProducer(this);
        return this;
    }

    public Producer removeLicenses(AgentLic agentLic) {
        this.licenses.remove(agentLic);
        agentLic.setProducer(null);
        return this;
    }

    public void setLicenses(Set<AgentLic> agentLics) {
        this.licenses = agentLics;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Producer producer = (Producer) o;
        if (producer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), producer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Producer{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ceCompliance='" + isCeCompliance() + "'" +
            "}";
    }
}
