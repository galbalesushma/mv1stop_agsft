package com.mv1.iaa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A SearchInsuranceLogs.
 */
@Entity
@Table(name = "search_insurance_logs")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SearchInsuranceLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "agent_id")
    private Long agentId;

    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "lpa_id")
    private Long lpaId;

    @Column(name = "dmv_id")
    private Long dmvId;

    @Column(name = "search_log_id")
    private Long searchLogId;

    @Column(name = "home")
    private String home;

    @Column(name = "phone")
    private String phone;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgentId() {
        return agentId;
    }

    public SearchInsuranceLogs agentId(Long agentId) {
        this.agentId = agentId;
        return this;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getProfileId() {
        return profileId;
    }

    public SearchInsuranceLogs profileId(Long profileId) {
        this.profileId = profileId;
        return this;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Long getLpaId() {
        return lpaId;
    }

    public SearchInsuranceLogs lpaId(Long lpaId) {
        this.lpaId = lpaId;
        return this;
    }

    public void setLpaId(Long lpaId) {
        this.lpaId = lpaId;
    }

    public Long getDmvId() {
        return dmvId;
    }

    public SearchInsuranceLogs dmvId(Long dmvId) {
        this.dmvId = dmvId;
        return this;
    }

    public void setDmvId(Long dmvId) {
        this.dmvId = dmvId;
    }

    public Long getSearchLogId() {
        return searchLogId;
    }

    public SearchInsuranceLogs searchLogId(Long searchLogId) {
        this.searchLogId = searchLogId;
        return this;
    }

    public void setSearchLogId(Long searchLogId) {
        this.searchLogId = searchLogId;
    }

    public String getHome() {
        return home;
    }

    public SearchInsuranceLogs home(String home) {
        this.home = home;
        return this;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getPhone() {
        return phone;
    }

    public SearchInsuranceLogs phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SearchInsuranceLogs searchInsuranceLogs = (SearchInsuranceLogs) o;
        if (searchInsuranceLogs.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), searchInsuranceLogs.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SearchInsuranceLogs{" +
            "id=" + getId() +
            ", agentId=" + getAgentId() +
            ", profileId=" + getProfileId() +
            ", lpaId=" + getLpaId() +
            ", dmvId=" + getDmvId() +
            ", searchLogId=" + getSearchLogId() +
            ", home='" + getHome() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }
}
