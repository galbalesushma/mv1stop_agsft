package com.mv1.iaa.domain.enumeration;

/**
 * The PageRank enumeration.
 */
public enum PageRank {
    ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN
}
