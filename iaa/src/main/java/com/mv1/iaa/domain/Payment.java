package com.mv1.iaa.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mv1.iaa.domain.enumeration.PayStatus;

/**
 * A Payment.
 */
@Entity
@Table(name = "payment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Payment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Long amount;

    @Column(name = "txn_id")
    private String txnId;

    @NotNull
    @Column(name = "uu_id", nullable = false)
    private String uuId;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private PayStatus status;

    @Column(name = "gateway_resp")
    private String gatewayResp;

    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "cart_id")
    private Long cartId;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
    
    @Column(name="cheque_no")
    private Long chequeNo;
    
    @Column(name="txn_type")
    private String txnType;
    
    @Column(name="routing_no")
    private String routingNumber;
	
    @Column(name="account_no")
    private String accountNumber;
    
    @Column(name="account_type")
	private String accountType;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public Payment amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getTxnId() {
        return txnId;
    }

    public Payment txnId(String txnId) {
        this.txnId = txnId;
        return this;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getUuId() {
        return uuId;
    }

    public Payment uuId(String uuId) {
        this.uuId = uuId;
        return this;
    }

    public void setUuId(String uuId) {
        this.uuId = uuId;
    }

    public PayStatus getStatus() {
        return status;
    }

    public Payment status(PayStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(PayStatus status) {
        this.status = status;
    }

    public String getGatewayResp() {
        return gatewayResp;
    }

    public Payment gatewayResp(String gatewayResp) {
        this.gatewayResp = gatewayResp;
        return this;
    }

    public void setGatewayResp(String gatewayResp) {
        this.gatewayResp = gatewayResp;
    }

    public Long getProfileId() {
        return profileId;
    }

    public Payment profileId(Long profileId) {
        this.profileId = profileId;
        return this;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Long getCartId() {
        return cartId;
    }

    public Payment cartId(Long cartId) {
        this.cartId = cartId;
        return this;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Payment createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Payment updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public Payment isDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    
    public Long getChequeNo() {
		return chequeNo;
	}
    
    public Payment chequeNo(Long chequeNo) {
        this.chequeNo = chequeNo;
        return this;
    }

	public void setChequeNo(Long chequeNo) {
		this.chequeNo = chequeNo;
	}

	public String getTxnType() {
		return txnType;
	}
	
	public Payment txnType(String txnType) {
        this.txnType = txnType;
        return this;
    }

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	
	
	public String getRoutingNumber() {
		return routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}
	
	

	public String getAccountType() {
		return accountType;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public Payment routingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
		return this;
	}

	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Payment accountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
		return this;
	}
	
	
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	public Payment accountType(String accountType) {
		this.accountType = accountType;
		return this ;
	}
	
	
	
	
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Payment payment = (Payment) o;
        if (payment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), payment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Payment{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", txnId='" + getTxnId() + "'" +
            ", uuId='" + getUuId() + "'" +
            ", status='" + getStatus() + "'" +
            ", gatewayResp='" + getGatewayResp() + "'" +
            ", profileId=" + getProfileId() +
            ", cartId=" + getCartId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            ", txnType='" + getTxnType() + "'" +
            ", chequeNo='" + getChequeNo() + "'" +
			", routingNumber='" + getRoutingNumber() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" + 
			", accountType='" + getAccountType() + "'" +
            "}";
    }

	
}
