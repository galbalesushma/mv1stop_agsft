package com.mv1.iaa.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A LPALocations.
 */
@Entity
@Table(name = "lpa_location")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LPALocations implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "branch")
    private String branch;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "country")
    private String country;

    @Column(name = "main_phone")
    private String mainPhone;

    @Column(name = "fax_number")
    private String faxNumber;

    @Column(name = "weekdays_hrs")
    private String weekdaysHrs;

    @Column(name = "saturday_hrs")
    private String saturdayHrs;

    @Column(name = "directions")
    private String directions;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranch() {
        return branch;
    }

    public LPALocations branch(String branch) {
        this.branch = branch;
        return this;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAddress() {
        return address;
    }

    public LPALocations address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public LPALocations city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public LPALocations state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public LPALocations postalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public LPALocations country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMainPhone() {
        return mainPhone;
    }

    public LPALocations mainPhone(String mainPhone) {
        this.mainPhone = mainPhone;
        return this;
    }

    public void setMainPhone(String mainPhone) {
        this.mainPhone = mainPhone;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public LPALocations faxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
        return this;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getWeekdaysHrs() {
        return weekdaysHrs;
    }

    public LPALocations weekdaysHrs(String weekdaysHrs) {
        this.weekdaysHrs = weekdaysHrs;
        return this;
    }

    public void setWeekdaysHrs(String weekdaysHrs) {
        this.weekdaysHrs = weekdaysHrs;
    }

    public String getSaturdayHrs() {
        return saturdayHrs;
    }

    public LPALocations saturdayHrs(String saturdayHrs) {
        this.saturdayHrs = saturdayHrs;
        return this;
    }

    public void setSaturdayHrs(String saturdayHrs) {
        this.saturdayHrs = saturdayHrs;
    }

    public String getDirections() {
        return directions;
    }

    public LPALocations directions(String directions) {
        this.directions = directions;
        return this;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LPALocations lPALocations = (LPALocations) o;
        if (lPALocations.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lPALocations.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LPALocations{" +
            "id=" + getId() +
            ", branch='" + getBranch() + "'" +
            ", address='" + getAddress() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", country='" + getCountry() + "'" +
            ", mainPhone='" + getMainPhone() + "'" +
            ", faxNumber='" + getFaxNumber() + "'" +
            ", weekdaysHrs='" + getWeekdaysHrs() + "'" +
            ", saturdayHrs='" + getSaturdayHrs() + "'" +
            ", directions='" + getDirections() + "'" +
            "}";
    }
}
