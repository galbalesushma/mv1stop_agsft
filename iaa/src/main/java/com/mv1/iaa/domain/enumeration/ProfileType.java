package com.mv1.iaa.domain.enumeration;

/**
 * The ProfileType enumeration.
 */
public enum ProfileType {
    AGENT, FACILITY, ADMIN
}
