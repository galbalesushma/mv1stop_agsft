package com.mv1.iaa.domain.enumeration;

/**
 * The PayStatus enumeration.
 */
public enum PayStatus {
    INITIATED, SUCCESS, FAILURE, AWAITED
}
