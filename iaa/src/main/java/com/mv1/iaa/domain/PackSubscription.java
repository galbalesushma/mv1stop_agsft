package com.mv1.iaa.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A PackSubscription.
 */
@Entity
@Table(name = "pack_subscription")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PackSubscription implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "package_id")
    private Long packageId;

    @Column(name = "zip_area_id")
    private Long zipAreaId;

    @Column(name = "slots")
    private Integer slots;

    @Column(name = "duration")
    private String duration;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "valid_till")
    private ZonedDateTime validTill;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Profile profile;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPackageId() {
        return packageId;
    }

    public PackSubscription packageId(Long packageId) {
        this.packageId = packageId;
        return this;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }

    public Long getZipAreaId() {
        return zipAreaId;
    }

    public PackSubscription zipAreaId(Long zipAreaId) {
        this.zipAreaId = zipAreaId;
        return this;
    }

    public void setZipAreaId(Long zipAreaId) {
        this.zipAreaId = zipAreaId;
    }

    public Integer getSlots() {
        return slots;
    }

    public PackSubscription slots(Integer slots) {
        this.slots = slots;
        return this;
    }

    public void setSlots(Integer slots) {
        this.slots = slots;
    }

    public String getDuration() {
        return duration;
    }

    public PackSubscription duration(String duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public PackSubscription createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public PackSubscription updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ZonedDateTime getValidTill() {
        return validTill;
    }

    public PackSubscription validTill(ZonedDateTime validTill) {
        this.validTill = validTill;
        return this;
    }

    public void setValidTill(ZonedDateTime validTill) {
        this.validTill = validTill;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public PackSubscription isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean isIsDeleted() {
        return isDeleted;
    }

    public PackSubscription isDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Profile getProfile() {
        return profile;
    }

    public PackSubscription profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PackSubscription packSubscription = (PackSubscription) o;
        if (packSubscription.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), packSubscription.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PackSubscription{" +
            "id=" + getId() +
            ", packageId=" + getPackageId() +
            ", zipAreaId=" + getZipAreaId() +
            ", slots=" + getSlots() +
            ", duration='" + getDuration() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", validTill='" + getValidTill() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", isDeleted='" + isIsDeleted() + "'" +
            "}";
    }
}
