package com.mv1.iaa.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.mv1.iaa.domain.enumeration.PageRank;

/**
 * A PricePackage.
 */
@Entity
@Table(name = "price_package")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PricePackage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_rank")
    private PageRank rank;

    @Column(name = "price")
    private Long price;

    @Column(name = "jhi_desc")
    private String desc;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public PricePackage name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PageRank getRank() {
        return rank;
    }

    public PricePackage rank(PageRank rank) {
        this.rank = rank;
        return this;
    }

    public void setRank(PageRank rank) {
        this.rank = rank;
    }

    public Long getPrice() {
        return price;
    }

    public PricePackage price(Long price) {
        this.price = price;
        return this;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public PricePackage desc(String desc) {
        this.desc = desc;
        return this;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PricePackage pricePackage = (PricePackage) o;
        if (pricePackage.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pricePackage.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PricePackage{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", rank='" + getRank() + "'" +
            ", price=" + getPrice() +
            ", desc='" + getDesc() + "'" +
            "}";
    }
}
