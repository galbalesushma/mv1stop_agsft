package com.mv1.test.junit;
//
//import static org.junit.Assert.assertEquals;
//
//import java.io.BufferedReader;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
////
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.junit.runners.Parameterized;
//import org.junit.runners.Parameterized.Parameters;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.web.client.RestTemplate;
//
////
//import com.mv1.iaa.business.view.web.GenericResp;
//import com.mv1.iaa.business.view.web.req.AgentSignupVM;
//
//@RunWith(value = Parameterized.class)
//@SpringApplicationConfiguration(classes = ApplicationAndConfiguration.class)
//@WebAppConfiguration
//@RunWith(SpringJUnit4ClassRunner.class)
public class AgentRegistration {

	// @MockBean
	// UserRepository userRepository;
	//
	// @MockBean
	// private WebApplicationContext webApplicationContext;
	//
	// private MockMvc mvc;
	//
	// @Before
	// public void setup() throws Exception {
	// mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	// }
	//
	// private final UserRepository userRepository;
	//
	// public AgentRegistration(UserRepository userRepository) {
	// this.userRepository = userRepository; }
	//
	//
/*
	  @Test public void testAgentRegistration() throws IOException {
		  System.out.println("in test method"); List<AgentSignupVM> records =
		  getTestData(); System.out.println("records=----" + records); for
		  (AgentSignupVM agentSignupVM : records) { //
			System.out.println("dto--" + agentSignupVM.toString());
			System.out.println("dto--" + agentSignupVM.getLogin());

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<GenericResp> response = restTemplate
					.postForEntity("http://localhost:8080/api/open/agent/signup", agentSignupVM, GenericResp.class);
			System.out.println("response--" + response);
			assertEquals("failure -failed to add agent", 200, response.getStatusCode());
		  
		  }
	  
	  }
	 
	@Parameters
	public static List<AgentSignupVM> getTestData() throws IOException {
		String fileName = "/home/agsuser/Desktop/agentCSVData.csv";
		List<AgentSignupVM> records = new ArrayList<AgentSignupVM>();
		String record;
		BufferedReader file = new BufferedReader(new FileReader(fileName));
		while ((record = file.readLine()) != null) {
			String fields[] = record.split(",");

			// records.add(fields);
			AgentSignupVM vm = new AgentSignupVM();
			vm.setLogin(fields[0]);
			vm.setPassword(fields[1]);
			vm.setFirstName(fields[2]);
			vm.setLastName(fields[3]);
			vm.setEmail(fields[4]);
			vm.setPreferredEmail(fields[5]);
			vm.setPreferredMobile(fields[6]);
			vm.setRefBy(fields[7]);
			vm.setNpn(fields[8]);
			vm.setLang(fields[9]);

			records.add(vm);
		}
		file.close();
		return records;
	}
*/
	/*
	  @Test public void testActivateAgent() throws IOException {
	  System.out.println("in testActivateAgent"); List<AgentSignupVM> records =
	  getTestData(); System.out.println("testActivateAgent records=----" +
	  records); for (AgentSignupVM agentSignupVM : records) { //
	  System.out.println("dto--"+agentSignupVM.toString());
	  System.out.println("dto-$$$$$$$$-" + agentSignupVM.getEmail()); Long l =
	  new Long(30); Optional<User> user = userRepository.findById(l);
	  System.out.println("user--------"+user); if (user.isPresent()) {
	  System.out.println("Present"); }else { System.out.println("not Present");
	  }
	  
	  RestTemplate restTemplate = new RestTemplate();
	  ResponseEntity<GenericResp> response = restTemplate.getForEntity(
	  "http://localhost:8080/api/open/agent/activate?key=" +
	  user.get().getActivationKey(), GenericResp.class);
	  System.out.println("response--" + response);
	  assertEquals("failure -failed to activate agent", 200,
	  response.getStatusCode());
	  
	  }
	  
	  }
	 

	@Test
	public void testAddToCart() throws IOException {
		System.out.println("in testAddToCart");
		List<CartVM> records = getCartData();
		System.out.println("getCartData =----" + records);
		for (CartVM cartVM : records) {
			// System.out.println("dto--"+agentSignupVM.toString());
			System.out.println("dto--" + cartVM.getProfileId());

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZ2VudCIsImF1dGgiOiJST0xFX1VTRVIiLCJleHAiOjE1NDIxOTcyNDh9.R8U_mA1U_56o1LlRIz6yGnK2Rti5Dhy7iFWjTRM3y3IlYJrGvdxPzFx__jc3_RBmjx7qKiQPuwixG2qLrIZGxQ");

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<GenericResp> response = restTemplate.postForEntity("http://localhost:8080/api/cart/add",
					cartVM,GenericResp.class,headers);
			System.out.println("response--" + response);
			assertEquals("failure -failed to add cartVM", 200, response.getStatusCode());

		}

	}

	@Parameters
	public static List<CartVM> getCartData() throws IOException {
		String fileName = "/home/agsuser/Desktop/addCart.csv";
		List<CartVM> records = new ArrayList<CartVM>();
		String record;
		BufferedReader file = new BufferedReader(new FileReader(fileName));
		while ((record = file.readLine()) != null) {
			String fields[] = record.split(",");

			// records.add(fields);
			CartVM vm = new CartVM();
			Long l = new Long(fields[0]);
			vm.setProfileId(l);
			String duration = fields[1];
			Long packageId = new Long(fields[2]);
			int slot = Integer.parseInt(fields[3]);
			Long zipAreaId = new Long(fields[4]);
			// List<String>
			List<Item> items = new ArrayList<>();
			Item item = new Item();
			item.setDuration(duration);
			item.setPackageId(packageId);
			item.setSlots(slot);
			item.setZipAreaId(zipAreaId);

			vm.setItems(items);
			records.add(vm);
		}
		file.close();
		return records;
	}*/
}
