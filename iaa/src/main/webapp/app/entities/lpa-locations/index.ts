export * from './lpa-locations.service';
export * from './lpa-locations-update.component';
export * from './lpa-locations-delete-dialog.component';
export * from './lpa-locations-detail.component';
export * from './lpa-locations.component';
export * from './lpa-locations.route';
