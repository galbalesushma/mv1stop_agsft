import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILPALocations } from 'app/shared/model/lpa-locations.model';
import { LPALocationsService } from './lpa-locations.service';

@Component({
    selector: 'jhi-lpa-locations-delete-dialog',
    templateUrl: './lpa-locations-delete-dialog.component.html'
})
export class LPALocationsDeleteDialogComponent {
    lPALocations: ILPALocations;

    constructor(
        private lPALocationsService: LPALocationsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.lPALocationsService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'lPALocationsListModification',
                content: 'Deleted an lPALocations'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-lpa-locations-delete-popup',
    template: ''
})
export class LPALocationsDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ lPALocations }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(LPALocationsDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.lPALocations = lPALocations;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
