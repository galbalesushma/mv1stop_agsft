import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { LPALocations } from 'app/shared/model/lpa-locations.model';
import { LPALocationsService } from './lpa-locations.service';
import { LPALocationsComponent } from './lpa-locations.component';
import { LPALocationsDetailComponent } from './lpa-locations-detail.component';
import { LPALocationsUpdateComponent } from './lpa-locations-update.component';
import { LPALocationsDeletePopupComponent } from './lpa-locations-delete-dialog.component';
import { ILPALocations } from 'app/shared/model/lpa-locations.model';

@Injectable({ providedIn: 'root' })
export class LPALocationsResolve implements Resolve<ILPALocations> {
    constructor(private service: LPALocationsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<LPALocations> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<LPALocations>) => response.ok),
                map((lPALocations: HttpResponse<LPALocations>) => lPALocations.body)
            );
        }
        return of(new LPALocations());
    }
}

export const lPALocationsRoute: Routes = [
    {
        path: 'lpa-locations',
        component: LPALocationsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.lPALocations.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'lpa-locations/:id/view',
        component: LPALocationsDetailComponent,
        resolve: {
            lPALocations: LPALocationsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.lPALocations.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'lpa-locations/new',
        component: LPALocationsUpdateComponent,
        resolve: {
            lPALocations: LPALocationsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.lPALocations.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'lpa-locations/:id/edit',
        component: LPALocationsUpdateComponent,
        resolve: {
            lPALocations: LPALocationsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.lPALocations.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const lPALocationsPopupRoute: Routes = [
    {
        path: 'lpa-locations/:id/delete',
        component: LPALocationsDeletePopupComponent,
        resolve: {
            lPALocations: LPALocationsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.lPALocations.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
