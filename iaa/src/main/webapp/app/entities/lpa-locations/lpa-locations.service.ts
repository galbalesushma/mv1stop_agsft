import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ILPALocations } from 'app/shared/model/lpa-locations.model';

type EntityResponseType = HttpResponse<ILPALocations>;
type EntityArrayResponseType = HttpResponse<ILPALocations[]>;

@Injectable({ providedIn: 'root' })
export class LPALocationsService {
    public resourceUrl = SERVER_API_URL + 'api/lpa-locations';

    constructor(private http: HttpClient) {}

    create(lPALocations: ILPALocations): Observable<EntityResponseType> {
        return this.http.post<ILPALocations>(this.resourceUrl, lPALocations, { observe: 'response' });
    }

    update(lPALocations: ILPALocations): Observable<EntityResponseType> {
        return this.http.put<ILPALocations>(this.resourceUrl, lPALocations, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ILPALocations>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ILPALocations[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
