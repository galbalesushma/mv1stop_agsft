import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILPALocations } from 'app/shared/model/lpa-locations.model';

@Component({
    selector: 'jhi-lpa-locations-detail',
    templateUrl: './lpa-locations-detail.component.html'
})
export class LPALocationsDetailComponent implements OnInit {
    lPALocations: ILPALocations;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ lPALocations }) => {
            this.lPALocations = lPALocations;
        });
    }

    previousState() {
        window.history.back();
    }
}
