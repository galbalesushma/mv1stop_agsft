import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    LPALocationsComponent,
    LPALocationsDetailComponent,
    LPALocationsUpdateComponent,
    LPALocationsDeletePopupComponent,
    LPALocationsDeleteDialogComponent,
    lPALocationsRoute,
    lPALocationsPopupRoute
} from './';

const ENTITY_STATES = [...lPALocationsRoute, ...lPALocationsPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        LPALocationsComponent,
        LPALocationsDetailComponent,
        LPALocationsUpdateComponent,
        LPALocationsDeleteDialogComponent,
        LPALocationsDeletePopupComponent
    ],
    entryComponents: [
        LPALocationsComponent,
        LPALocationsUpdateComponent,
        LPALocationsDeleteDialogComponent,
        LPALocationsDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaLPALocationsModule {}
