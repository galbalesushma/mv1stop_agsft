import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ILPALocations } from 'app/shared/model/lpa-locations.model';
import { LPALocationsService } from './lpa-locations.service';

@Component({
    selector: 'jhi-lpa-locations-update',
    templateUrl: './lpa-locations-update.component.html'
})
export class LPALocationsUpdateComponent implements OnInit {
    lPALocations: ILPALocations;
    isSaving: boolean;

    constructor(private lPALocationsService: LPALocationsService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ lPALocations }) => {
            this.lPALocations = lPALocations;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.lPALocations.id !== undefined) {
            this.subscribeToSaveResponse(this.lPALocationsService.update(this.lPALocations));
        } else {
            this.subscribeToSaveResponse(this.lPALocationsService.create(this.lPALocations));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ILPALocations>>) {
        result.subscribe((res: HttpResponse<ILPALocations>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
