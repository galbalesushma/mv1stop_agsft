import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentMv } from 'app/shared/model/agent-mv.model';

@Component({
    selector: 'jhi-agent-mv-detail',
    templateUrl: './agent-mv-detail.component.html'
})
export class AgentMvDetailComponent implements OnInit {
    agent: IAgentMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ agent }) => {
            this.agent = agent;
        });
    }

    previousState() {
        window.history.back();
    }
}
