import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAgentMv } from 'app/shared/model/agent-mv.model';
import { Principal } from 'app/core';
import { AgentMvService } from './agent-mv.service';

@Component({
    selector: 'jhi-agent-mv',
    templateUrl: './agent-mv.component.html'
})
export class AgentMvComponent implements OnInit, OnDestroy {
    agents: IAgentMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private agentService: AgentMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.agentService.query().subscribe(
            (res: HttpResponse<IAgentMv[]>) => {
                this.agents = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAgents();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAgentMv) {
        return item.id;
    }

    registerChangeInAgents() {
        this.eventSubscriber = this.eventManager.subscribe('agentListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
