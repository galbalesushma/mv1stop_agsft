import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IAgentMv } from 'app/shared/model/agent-mv.model';
import { AgentMvService } from './agent-mv.service';
import { IProfileMv } from 'app/shared/model/profile-mv.model';
import { ProfileMvService } from 'app/entities/profile-mv';
import { IAgentLicMv } from 'app/shared/model/agent-lic-mv.model';
import { AgentLicMvService } from 'app/entities/agent-lic-mv';
import { IStateMv } from 'app/shared/model/state-mv.model';
import { StateMvService } from 'app/entities/state-mv';
import { IBusinessMv } from 'app/shared/model/business-mv.model';
import { BusinessMvService } from 'app/entities/business-mv';

@Component({
    selector: 'jhi-agent-mv-update',
    templateUrl: './agent-mv-update.component.html'
})
export class AgentMvUpdateComponent implements OnInit {
    private _agent: IAgentMv;
    isSaving: boolean;

    profiles: IProfileMv[];

    lics: IAgentLicMv[];

    states: IStateMv[];

    businesses: IBusinessMv[];
    createdAt: string;
    updatedAt: string;
    verifiedAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private agentService: AgentMvService,
        private profileService: ProfileMvService,
        private agentLicService: AgentLicMvService,
        private stateService: StateMvService,
        private businessService: BusinessMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ agent }) => {
            this.agent = agent;
        });
        this.profileService.query({ filter: 'agent-is-null' }).subscribe(
            (res: HttpResponse<IProfileMv[]>) => {
                if (!this.agent.profileId) {
                    this.profiles = res.body;
                } else {
                    this.profileService.find(this.agent.profileId).subscribe(
                        (subRes: HttpResponse<IProfileMv>) => {
                            this.profiles = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.agentLicService.query({ filter: 'agent-is-null' }).subscribe(
            (res: HttpResponse<IAgentLicMv[]>) => {
                if (!this.agent.licId) {
                    this.lics = res.body;
                } else {
                    this.agentLicService.find(this.agent.licId).subscribe(
                        (subRes: HttpResponse<IAgentLicMv>) => {
                            this.lics = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.stateService.query().subscribe(
            (res: HttpResponse<IStateMv[]>) => {
                this.states = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.businessService.query().subscribe(
            (res: HttpResponse<IBusinessMv[]>) => {
                this.businesses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.agent.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.agent.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        this.agent.verifiedAt = moment(this.verifiedAt, DATE_TIME_FORMAT);
        if (this.agent.id !== undefined) {
            this.subscribeToSaveResponse(this.agentService.update(this.agent));
        } else {
            this.subscribeToSaveResponse(this.agentService.create(this.agent));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAgentMv>>) {
        result.subscribe((res: HttpResponse<IAgentMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProfileById(index: number, item: IProfileMv) {
        return item.id;
    }

    trackAgentLicById(index: number, item: IAgentLicMv) {
        return item.id;
    }

    trackStateById(index: number, item: IStateMv) {
        return item.id;
    }

    trackBusinessById(index: number, item: IBusinessMv) {
        return item.id;
    }
    get agent() {
        return this._agent;
    }

    set agent(agent: IAgentMv) {
        this._agent = agent;
        this.createdAt = moment(agent.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(agent.updatedAt).format(DATE_TIME_FORMAT);
        this.verifiedAt = moment(agent.verifiedAt).format(DATE_TIME_FORMAT);
    }
}
