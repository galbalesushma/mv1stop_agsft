import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAgentMv } from 'app/shared/model/agent-mv.model';

type EntityResponseType = HttpResponse<IAgentMv>;
type EntityArrayResponseType = HttpResponse<IAgentMv[]>;

@Injectable({ providedIn: 'root' })
export class AgentMvService {
    private resourceUrl = SERVER_API_URL + 'api/agents';

    constructor(private http: HttpClient) {}

    create(agent: IAgentMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(agent);
        return this.http
            .post<IAgentMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(agent: IAgentMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(agent);
        return this.http
            .put<IAgentMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IAgentMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IAgentMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(agent: IAgentMv): IAgentMv {
        const copy: IAgentMv = Object.assign({}, agent, {
            createdAt: agent.createdAt != null && agent.createdAt.isValid() ? agent.createdAt.toJSON() : null,
            updatedAt: agent.updatedAt != null && agent.updatedAt.isValid() ? agent.updatedAt.toJSON() : null,
            verifiedAt: agent.verifiedAt != null && agent.verifiedAt.isValid() ? agent.verifiedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        res.body.verifiedAt = res.body.verifiedAt != null ? moment(res.body.verifiedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((agent: IAgentMv) => {
            agent.createdAt = agent.createdAt != null ? moment(agent.createdAt) : null;
            agent.updatedAt = agent.updatedAt != null ? moment(agent.updatedAt) : null;
            agent.verifiedAt = agent.verifiedAt != null ? moment(agent.verifiedAt) : null;
        });
        return res;
    }
}
