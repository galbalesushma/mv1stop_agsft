import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    AgentMvComponent,
    AgentMvDetailComponent,
    AgentMvUpdateComponent,
    AgentMvDeletePopupComponent,
    AgentMvDeleteDialogComponent,
    agentRoute,
    agentPopupRoute
} from './';

const ENTITY_STATES = [...agentRoute, ...agentPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AgentMvComponent,
        AgentMvDetailComponent,
        AgentMvUpdateComponent,
        AgentMvDeleteDialogComponent,
        AgentMvDeletePopupComponent
    ],
    entryComponents: [AgentMvComponent, AgentMvUpdateComponent, AgentMvDeleteDialogComponent, AgentMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaAgentMvModule {}
