import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AgentMv } from 'app/shared/model/agent-mv.model';
import { AgentMvService } from './agent-mv.service';
import { AgentMvComponent } from './agent-mv.component';
import { AgentMvDetailComponent } from './agent-mv-detail.component';
import { AgentMvUpdateComponent } from './agent-mv-update.component';
import { AgentMvDeletePopupComponent } from './agent-mv-delete-dialog.component';
import { IAgentMv } from 'app/shared/model/agent-mv.model';

@Injectable({ providedIn: 'root' })
export class AgentMvResolve implements Resolve<IAgentMv> {
    constructor(private service: AgentMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((agent: HttpResponse<AgentMv>) => agent.body));
        }
        return of(new AgentMv());
    }
}

export const agentRoute: Routes = [
    {
        path: 'agent-mv',
        component: AgentMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-mv/:id/view',
        component: AgentMvDetailComponent,
        resolve: {
            agent: AgentMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-mv/new',
        component: AgentMvUpdateComponent,
        resolve: {
            agent: AgentMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-mv/:id/edit',
        component: AgentMvUpdateComponent,
        resolve: {
            agent: AgentMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agent.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const agentPopupRoute: Routes = [
    {
        path: 'agent-mv/:id/delete',
        component: AgentMvDeletePopupComponent,
        resolve: {
            agent: AgentMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
