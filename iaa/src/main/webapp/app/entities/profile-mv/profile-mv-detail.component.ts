import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProfileMv } from 'app/shared/model/profile-mv.model';

@Component({
    selector: 'jhi-profile-mv-detail',
    templateUrl: './profile-mv-detail.component.html'
})
export class ProfileMvDetailComponent implements OnInit {
    profile: IProfileMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ profile }) => {
            this.profile = profile;
        });
    }

    previousState() {
        window.history.back();
    }
}
