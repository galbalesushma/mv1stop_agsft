import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IProfileMv } from 'app/shared/model/profile-mv.model';

type EntityResponseType = HttpResponse<IProfileMv>;
type EntityArrayResponseType = HttpResponse<IProfileMv[]>;

@Injectable({ providedIn: 'root' })
export class ProfileMvService {
    private resourceUrl = SERVER_API_URL + 'api/profiles';

    constructor(private http: HttpClient) {}

    create(profile: IProfileMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(profile);
        return this.http
            .post<IProfileMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(profile: IProfileMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(profile);
        return this.http
            .put<IProfileMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IProfileMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IProfileMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(profile: IProfileMv): IProfileMv {
        const copy: IProfileMv = Object.assign({}, profile, {
            createdAt: profile.createdAt != null && profile.createdAt.isValid() ? profile.createdAt.toJSON() : null,
            updatedAt: profile.updatedAt != null && profile.updatedAt.isValid() ? profile.updatedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((profile: IProfileMv) => {
            profile.createdAt = profile.createdAt != null ? moment(profile.createdAt) : null;
            profile.updatedAt = profile.updatedAt != null ? moment(profile.updatedAt) : null;
        });
        return res;
    }
}
