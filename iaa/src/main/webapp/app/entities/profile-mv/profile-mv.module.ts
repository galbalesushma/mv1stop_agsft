import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    ProfileMvComponent,
    ProfileMvDetailComponent,
    ProfileMvUpdateComponent,
    ProfileMvDeletePopupComponent,
    ProfileMvDeleteDialogComponent,
    profileRoute,
    profilePopupRoute
} from './';

const ENTITY_STATES = [...profileRoute, ...profilePopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ProfileMvComponent,
        ProfileMvDetailComponent,
        ProfileMvUpdateComponent,
        ProfileMvDeleteDialogComponent,
        ProfileMvDeletePopupComponent
    ],
    entryComponents: [ProfileMvComponent, ProfileMvUpdateComponent, ProfileMvDeleteDialogComponent, ProfileMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaProfileMvModule {}
