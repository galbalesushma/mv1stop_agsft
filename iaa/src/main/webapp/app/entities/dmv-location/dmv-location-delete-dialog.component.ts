import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDMVLocation } from 'app/shared/model/dmv-location.model';
import { DMVLocationService } from './dmv-location.service';

@Component({
    selector: 'jhi-dmv-location-delete-dialog',
    templateUrl: './dmv-location-delete-dialog.component.html'
})
export class DMVLocationDeleteDialogComponent {
    dMVLocation: IDMVLocation;

    constructor(
        private dMVLocationService: DMVLocationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dMVLocationService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'dMVLocationListModification',
                content: 'Deleted an dMVLocation'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-dmv-location-delete-popup',
    template: ''
})
export class DMVLocationDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ dMVLocation }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DMVLocationDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.dMVLocation = dMVLocation;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
