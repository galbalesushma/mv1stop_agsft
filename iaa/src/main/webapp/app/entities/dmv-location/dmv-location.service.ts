import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDMVLocation } from 'app/shared/model/dmv-location.model';

type EntityResponseType = HttpResponse<IDMVLocation>;
type EntityArrayResponseType = HttpResponse<IDMVLocation[]>;

@Injectable({ providedIn: 'root' })
export class DMVLocationService {
    public resourceUrl = SERVER_API_URL + 'api/dmv-locations';

    constructor(private http: HttpClient) {}

    create(dMVLocation: IDMVLocation): Observable<EntityResponseType> {
        return this.http.post<IDMVLocation>(this.resourceUrl, dMVLocation, { observe: 'response' });
    }

    update(dMVLocation: IDMVLocation): Observable<EntityResponseType> {
        return this.http.put<IDMVLocation>(this.resourceUrl, dMVLocation, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IDMVLocation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IDMVLocation[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
