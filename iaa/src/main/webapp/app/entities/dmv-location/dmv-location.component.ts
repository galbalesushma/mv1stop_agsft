import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDMVLocation } from 'app/shared/model/dmv-location.model';
import { Principal } from 'app/core';
import { DMVLocationService } from './dmv-location.service';

@Component({
    selector: 'jhi-dmv-location',
    templateUrl: './dmv-location.component.html'
})
export class DMVLocationComponent implements OnInit, OnDestroy {
    dMVLocations: IDMVLocation[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private dMVLocationService: DMVLocationService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.dMVLocationService.query().subscribe(
            (res: HttpResponse<IDMVLocation[]>) => {
                this.dMVLocations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInDMVLocations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IDMVLocation) {
        return item.id;
    }

    registerChangeInDMVLocations() {
        this.eventSubscriber = this.eventManager.subscribe('dMVLocationListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
