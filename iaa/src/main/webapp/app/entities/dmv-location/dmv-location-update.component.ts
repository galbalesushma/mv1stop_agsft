import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IDMVLocation } from 'app/shared/model/dmv-location.model';
import { DMVLocationService } from './dmv-location.service';

@Component({
    selector: 'jhi-dmv-location-update',
    templateUrl: './dmv-location-update.component.html'
})
export class DMVLocationUpdateComponent implements OnInit {
    dMVLocation: IDMVLocation;
    isSaving: boolean;

    constructor(private dMVLocationService: DMVLocationService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ dMVLocation }) => {
            this.dMVLocation = dMVLocation;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.dMVLocation.id !== undefined) {
            this.subscribeToSaveResponse(this.dMVLocationService.update(this.dMVLocation));
        } else {
            this.subscribeToSaveResponse(this.dMVLocationService.create(this.dMVLocation));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDMVLocation>>) {
        result.subscribe((res: HttpResponse<IDMVLocation>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
