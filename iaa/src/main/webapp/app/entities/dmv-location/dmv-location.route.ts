import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DMVLocation } from 'app/shared/model/dmv-location.model';
import { DMVLocationService } from './dmv-location.service';
import { DMVLocationComponent } from './dmv-location.component';
import { DMVLocationDetailComponent } from './dmv-location-detail.component';
import { DMVLocationUpdateComponent } from './dmv-location-update.component';
import { DMVLocationDeletePopupComponent } from './dmv-location-delete-dialog.component';
import { IDMVLocation } from 'app/shared/model/dmv-location.model';

@Injectable({ providedIn: 'root' })
export class DMVLocationResolve implements Resolve<IDMVLocation> {
    constructor(private service: DMVLocationService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<DMVLocation> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<DMVLocation>) => response.ok),
                map((dMVLocation: HttpResponse<DMVLocation>) => dMVLocation.body)
            );
        }
        return of(new DMVLocation());
    }
}

export const dMVLocationRoute: Routes = [
    {
        path: 'dmv-location',
        component: DMVLocationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.dMVLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dmv-location/:id/view',
        component: DMVLocationDetailComponent,
        resolve: {
            dMVLocation: DMVLocationResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.dMVLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dmv-location/new',
        component: DMVLocationUpdateComponent,
        resolve: {
            dMVLocation: DMVLocationResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.dMVLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dmv-location/:id/edit',
        component: DMVLocationUpdateComponent,
        resolve: {
            dMVLocation: DMVLocationResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.dMVLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dMVLocationPopupRoute: Routes = [
    {
        path: 'dmv-location/:id/delete',
        component: DMVLocationDeletePopupComponent,
        resolve: {
            dMVLocation: DMVLocationResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.dMVLocation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
