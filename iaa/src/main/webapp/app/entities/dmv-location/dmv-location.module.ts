import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    DMVLocationComponent,
    DMVLocationDetailComponent,
    DMVLocationUpdateComponent,
    DMVLocationDeletePopupComponent,
    DMVLocationDeleteDialogComponent,
    dMVLocationRoute,
    dMVLocationPopupRoute
} from './';

const ENTITY_STATES = [...dMVLocationRoute, ...dMVLocationPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DMVLocationComponent,
        DMVLocationDetailComponent,
        DMVLocationUpdateComponent,
        DMVLocationDeleteDialogComponent,
        DMVLocationDeletePopupComponent
    ],
    entryComponents: [DMVLocationComponent, DMVLocationUpdateComponent, DMVLocationDeleteDialogComponent, DMVLocationDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaDMVLocationModule {}
