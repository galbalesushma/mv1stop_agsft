import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDMVLocation } from 'app/shared/model/dmv-location.model';

@Component({
    selector: 'jhi-dmv-location-detail',
    templateUrl: './dmv-location-detail.component.html'
})
export class DMVLocationDetailComponent implements OnInit {
    dMVLocation: IDMVLocation;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ dMVLocation }) => {
            this.dMVLocation = dMVLocation;
        });
    }

    previousState() {
        window.history.back();
    }
}
