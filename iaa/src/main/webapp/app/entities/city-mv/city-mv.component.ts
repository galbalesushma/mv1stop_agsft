import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICityMv } from 'app/shared/model/city-mv.model';
import { Principal } from 'app/core';
import { CityMvService } from './city-mv.service';

@Component({
    selector: 'jhi-city-mv',
    templateUrl: './city-mv.component.html'
})
export class CityMvComponent implements OnInit, OnDestroy {
    cities: ICityMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private cityService: CityMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.cityService.query().subscribe(
            (res: HttpResponse<ICityMv[]>) => {
                this.cities = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCities();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICityMv) {
        return item.id;
    }

    registerChangeInCities() {
        this.eventSubscriber = this.eventManager.subscribe('cityListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
