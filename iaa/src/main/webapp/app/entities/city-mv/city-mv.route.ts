import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CityMv } from 'app/shared/model/city-mv.model';
import { CityMvService } from './city-mv.service';
import { CityMvComponent } from './city-mv.component';
import { CityMvDetailComponent } from './city-mv-detail.component';
import { CityMvUpdateComponent } from './city-mv-update.component';
import { CityMvDeletePopupComponent } from './city-mv-delete-dialog.component';
import { ICityMv } from 'app/shared/model/city-mv.model';

@Injectable({ providedIn: 'root' })
export class CityMvResolve implements Resolve<ICityMv> {
    constructor(private service: CityMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((city: HttpResponse<CityMv>) => city.body));
        }
        return of(new CityMv());
    }
}

export const cityRoute: Routes = [
    {
        path: 'city-mv',
        component: CityMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.city.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'city-mv/:id/view',
        component: CityMvDetailComponent,
        resolve: {
            city: CityMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.city.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'city-mv/new',
        component: CityMvUpdateComponent,
        resolve: {
            city: CityMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.city.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'city-mv/:id/edit',
        component: CityMvUpdateComponent,
        resolve: {
            city: CityMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.city.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cityPopupRoute: Routes = [
    {
        path: 'city-mv/:id/delete',
        component: CityMvDeletePopupComponent,
        resolve: {
            city: CityMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.city.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
