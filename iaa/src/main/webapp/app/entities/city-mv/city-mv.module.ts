import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    CityMvComponent,
    CityMvDetailComponent,
    CityMvUpdateComponent,
    CityMvDeletePopupComponent,
    CityMvDeleteDialogComponent,
    cityRoute,
    cityPopupRoute
} from './';

const ENTITY_STATES = [...cityRoute, ...cityPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [CityMvComponent, CityMvDetailComponent, CityMvUpdateComponent, CityMvDeleteDialogComponent, CityMvDeletePopupComponent],
    entryComponents: [CityMvComponent, CityMvUpdateComponent, CityMvDeleteDialogComponent, CityMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaCityMvModule {}
