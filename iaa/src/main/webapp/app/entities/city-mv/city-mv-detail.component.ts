import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICityMv } from 'app/shared/model/city-mv.model';

@Component({
    selector: 'jhi-city-mv-detail',
    templateUrl: './city-mv-detail.component.html'
})
export class CityMvDetailComponent implements OnInit {
    city: ICityMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ city }) => {
            this.city = city;
        });
    }

    previousState() {
        window.history.back();
    }
}
