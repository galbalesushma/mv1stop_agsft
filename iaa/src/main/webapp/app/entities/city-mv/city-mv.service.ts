import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICityMv } from 'app/shared/model/city-mv.model';

type EntityResponseType = HttpResponse<ICityMv>;
type EntityArrayResponseType = HttpResponse<ICityMv[]>;

@Injectable({ providedIn: 'root' })
export class CityMvService {
    private resourceUrl = SERVER_API_URL + 'api/cities';

    constructor(private http: HttpClient) {}

    create(city: ICityMv): Observable<EntityResponseType> {
        return this.http.post<ICityMv>(this.resourceUrl, city, { observe: 'response' });
    }

    update(city: ICityMv): Observable<EntityResponseType> {
        return this.http.put<ICityMv>(this.resourceUrl, city, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICityMv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICityMv[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
