import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICityMv } from 'app/shared/model/city-mv.model';
import { CityMvService } from './city-mv.service';

@Component({
    selector: 'jhi-city-mv-update',
    templateUrl: './city-mv-update.component.html'
})
export class CityMvUpdateComponent implements OnInit {
    private _city: ICityMv;
    isSaving: boolean;

    constructor(private cityService: CityMvService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ city }) => {
            this.city = city;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.city.id !== undefined) {
            this.subscribeToSaveResponse(this.cityService.update(this.city));
        } else {
            this.subscribeToSaveResponse(this.cityService.create(this.city));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICityMv>>) {
        result.subscribe((res: HttpResponse<ICityMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get city() {
        return this._city;
    }

    set city(city: ICityMv) {
        this._city = city;
    }
}
