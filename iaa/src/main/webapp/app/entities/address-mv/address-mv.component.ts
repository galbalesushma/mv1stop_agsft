import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAddressMv } from 'app/shared/model/address-mv.model';
import { Principal } from 'app/core';
import { AddressMvService } from './address-mv.service';

@Component({
    selector: 'jhi-address-mv',
    templateUrl: './address-mv.component.html'
})
export class AddressMvComponent implements OnInit, OnDestroy {
    addresses: IAddressMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private addressService: AddressMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.addressService.query().subscribe(
            (res: HttpResponse<IAddressMv[]>) => {
                this.addresses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAddresses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAddressMv) {
        return item.id;
    }

    registerChangeInAddresses() {
        this.eventSubscriber = this.eventManager.subscribe('addressListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
