import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAddressMv } from 'app/shared/model/address-mv.model';

@Component({
    selector: 'jhi-address-mv-detail',
    templateUrl: './address-mv-detail.component.html'
})
export class AddressMvDetailComponent implements OnInit {
    address: IAddressMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ address }) => {
            this.address = address;
        });
    }

    previousState() {
        window.history.back();
    }
}
