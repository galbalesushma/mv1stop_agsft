import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAddressMv } from 'app/shared/model/address-mv.model';

type EntityResponseType = HttpResponse<IAddressMv>;
type EntityArrayResponseType = HttpResponse<IAddressMv[]>;

@Injectable({ providedIn: 'root' })
export class AddressMvService {
    private resourceUrl = SERVER_API_URL + 'api/addresses';

    constructor(private http: HttpClient) {}

    create(address: IAddressMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(address);
        return this.http
            .post<IAddressMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(address: IAddressMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(address);
        return this.http
            .put<IAddressMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IAddressMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IAddressMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(address: IAddressMv): IAddressMv {
        const copy: IAddressMv = Object.assign({}, address, {
            createdAt: address.createdAt != null && address.createdAt.isValid() ? address.createdAt.toJSON() : null,
            updatedAt: address.updatedAt != null && address.updatedAt.isValid() ? address.updatedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((address: IAddressMv) => {
            address.createdAt = address.createdAt != null ? moment(address.createdAt) : null;
            address.updatedAt = address.updatedAt != null ? moment(address.updatedAt) : null;
        });
        return res;
    }
}
