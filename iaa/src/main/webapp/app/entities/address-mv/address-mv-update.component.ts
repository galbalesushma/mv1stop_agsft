import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IAddressMv } from 'app/shared/model/address-mv.model';
import { AddressMvService } from './address-mv.service';
import { ICityMv } from 'app/shared/model/city-mv.model';
import { CityMvService } from 'app/entities/city-mv';
import { ICountyMv } from 'app/shared/model/county-mv.model';
import { CountyMvService } from 'app/entities/county-mv';
import { IStateMv } from 'app/shared/model/state-mv.model';
import { StateMvService } from 'app/entities/state-mv';

@Component({
    selector: 'jhi-address-mv-update',
    templateUrl: './address-mv-update.component.html'
})
export class AddressMvUpdateComponent implements OnInit {
    private _address: IAddressMv;
    isSaving: boolean;

    cities: ICityMv[];

    counties: ICountyMv[];

    states: IStateMv[];
    createdAt: string;
    updatedAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private addressService: AddressMvService,
        private cityService: CityMvService,
        private countyService: CountyMvService,
        private stateService: StateMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ address }) => {
            this.address = address;
        });
        this.cityService.query().subscribe(
            (res: HttpResponse<ICityMv[]>) => {
                this.cities = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.countyService.query().subscribe(
            (res: HttpResponse<ICountyMv[]>) => {
                this.counties = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.stateService.query().subscribe(
            (res: HttpResponse<IStateMv[]>) => {
                this.states = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.address.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.address.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        if (this.address.id !== undefined) {
            this.subscribeToSaveResponse(this.addressService.update(this.address));
        } else {
            this.subscribeToSaveResponse(this.addressService.create(this.address));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAddressMv>>) {
        result.subscribe((res: HttpResponse<IAddressMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCityById(index: number, item: ICityMv) {
        return item.id;
    }

    trackCountyById(index: number, item: ICountyMv) {
        return item.id;
    }

    trackStateById(index: number, item: IStateMv) {
        return item.id;
    }
    get address() {
        return this._address;
    }

    set address(address: IAddressMv) {
        this._address = address;
        this.createdAt = moment(address.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(address.updatedAt).format(DATE_TIME_FORMAT);
    }
}
