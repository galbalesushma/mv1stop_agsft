import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { IaaZipAreaMvModule } from './zip-area-mv/zip-area-mv.module';
import { IaaAgentDistributionMvModule } from './agent-distribution-mv/agent-distribution-mv.module';
import { IaaPricePackageMvModule } from './price-package-mv/price-package-mv.module';
import { IaaCartMvModule } from './cart-mv/cart-mv.module';
import { IaaAgentMvModule } from './agent-mv/agent-mv.module';
import { IaaProfileMvModule } from './profile-mv/profile-mv.module';
import { IaaAddressMvModule } from './address-mv/address-mv.module';
import { IaaContactMvModule } from './contact-mv/contact-mv.module';
import { IaaFacilityMvModule } from './facility-mv/facility-mv.module';
import { IaaPaymentMvModule } from './payment-mv/payment-mv.module';
import { IaaBusinessMvModule } from './business-mv/business-mv.module';
import { IaaStateMvModule } from './state-mv/state-mv.module';
import { IaaCountyMvModule } from './county-mv/county-mv.module';
import { IaaCityMvModule } from './city-mv/city-mv.module';
import { IaaProducerMvModule } from './producer-mv/producer-mv.module';
import { IaaAgentLicMvModule } from './agent-lic-mv/agent-lic-mv.module';
import { IaaAgentTypeMvModule } from './agent-type-mv/agent-type-mv.module';
import { IaaPackSubscriptionMvModule } from './pack-subscription-mv/pack-subscription-mv.module';
import { IaaSearchLogsModule } from './search-logs/search-logs.module';
import { IaaDMVLocationModule } from './dmv-location/dmv-location.module';
import { IaaLPALocationsModule } from './lpa-locations/lpa-locations.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        IaaZipAreaMvModule,
        IaaAgentDistributionMvModule,
        IaaPricePackageMvModule,
        IaaCartMvModule,
        IaaAgentMvModule,
        IaaProfileMvModule,
        IaaAddressMvModule,
        IaaContactMvModule,
        IaaFacilityMvModule,
        IaaPaymentMvModule,
        IaaBusinessMvModule,
        IaaStateMvModule,
        IaaCountyMvModule,
        IaaCityMvModule,
        IaaProducerMvModule,
        IaaAgentLicMvModule,
        IaaAgentTypeMvModule,
        IaaPackSubscriptionMvModule,
        IaaSearchLogsModule,
        IaaDMVLocationModule,
        IaaLPALocationsModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaEntityModule {}
