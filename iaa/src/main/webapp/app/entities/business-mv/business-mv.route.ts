import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BusinessMv } from 'app/shared/model/business-mv.model';
import { BusinessMvService } from './business-mv.service';
import { BusinessMvComponent } from './business-mv.component';
import { BusinessMvDetailComponent } from './business-mv-detail.component';
import { BusinessMvUpdateComponent } from './business-mv-update.component';
import { BusinessMvDeletePopupComponent } from './business-mv-delete-dialog.component';
import { IBusinessMv } from 'app/shared/model/business-mv.model';

@Injectable({ providedIn: 'root' })
export class BusinessMvResolve implements Resolve<IBusinessMv> {
    constructor(private service: BusinessMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((business: HttpResponse<BusinessMv>) => business.body));
        }
        return of(new BusinessMv());
    }
}

export const businessRoute: Routes = [
    {
        path: 'business-mv',
        component: BusinessMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.business.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'business-mv/:id/view',
        component: BusinessMvDetailComponent,
        resolve: {
            business: BusinessMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.business.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'business-mv/new',
        component: BusinessMvUpdateComponent,
        resolve: {
            business: BusinessMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.business.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'business-mv/:id/edit',
        component: BusinessMvUpdateComponent,
        resolve: {
            business: BusinessMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.business.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const businessPopupRoute: Routes = [
    {
        path: 'business-mv/:id/delete',
        component: BusinessMvDeletePopupComponent,
        resolve: {
            business: BusinessMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.business.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
