import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    BusinessMvComponent,
    BusinessMvDetailComponent,
    BusinessMvUpdateComponent,
    BusinessMvDeletePopupComponent,
    BusinessMvDeleteDialogComponent,
    businessRoute,
    businessPopupRoute
} from './';

const ENTITY_STATES = [...businessRoute, ...businessPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        BusinessMvComponent,
        BusinessMvDetailComponent,
        BusinessMvUpdateComponent,
        BusinessMvDeleteDialogComponent,
        BusinessMvDeletePopupComponent
    ],
    entryComponents: [BusinessMvComponent, BusinessMvUpdateComponent, BusinessMvDeleteDialogComponent, BusinessMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaBusinessMvModule {}
