import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IBusinessMv } from 'app/shared/model/business-mv.model';
import { BusinessMvService } from './business-mv.service';
import { IAddressMv } from 'app/shared/model/address-mv.model';
import { AddressMvService } from 'app/entities/address-mv';

@Component({
    selector: 'jhi-business-mv-update',
    templateUrl: './business-mv-update.component.html'
})
export class BusinessMvUpdateComponent implements OnInit {
    private _business: IBusinessMv;
    isSaving: boolean;

    addresses: IAddressMv[];
    createdAt: string;
    updatedAt: string;
    verifiedAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private businessService: BusinessMvService,
        private addressService: AddressMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ business }) => {
            this.business = business;
        });
        this.addressService.query({ filter: 'business-is-null' }).subscribe(
            (res: HttpResponse<IAddressMv[]>) => {
                if (!this.business.addressId) {
                    this.addresses = res.body;
                } else {
                    this.addressService.find(this.business.addressId).subscribe(
                        (subRes: HttpResponse<IAddressMv>) => {
                            this.addresses = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.business.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.business.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        this.business.verifiedAt = moment(this.verifiedAt, DATE_TIME_FORMAT);
        if (this.business.id !== undefined) {
            this.subscribeToSaveResponse(this.businessService.update(this.business));
        } else {
            this.subscribeToSaveResponse(this.businessService.create(this.business));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IBusinessMv>>) {
        result.subscribe((res: HttpResponse<IBusinessMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackAddressById(index: number, item: IAddressMv) {
        return item.id;
    }
    get business() {
        return this._business;
    }

    set business(business: IBusinessMv) {
        this._business = business;
        this.createdAt = moment(business.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(business.updatedAt).format(DATE_TIME_FORMAT);
        this.verifiedAt = moment(business.verifiedAt).format(DATE_TIME_FORMAT);
    }
}
