import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IBusinessMv } from 'app/shared/model/business-mv.model';
import { Principal } from 'app/core';
import { BusinessMvService } from './business-mv.service';

@Component({
    selector: 'jhi-business-mv',
    templateUrl: './business-mv.component.html'
})
export class BusinessMvComponent implements OnInit, OnDestroy {
    businesses: IBusinessMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private businessService: BusinessMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.businessService.query().subscribe(
            (res: HttpResponse<IBusinessMv[]>) => {
                this.businesses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInBusinesses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IBusinessMv) {
        return item.id;
    }

    registerChangeInBusinesses() {
        this.eventSubscriber = this.eventManager.subscribe('businessListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
