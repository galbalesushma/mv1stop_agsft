import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBusinessMv } from 'app/shared/model/business-mv.model';

@Component({
    selector: 'jhi-business-mv-detail',
    templateUrl: './business-mv-detail.component.html'
})
export class BusinessMvDetailComponent implements OnInit {
    business: IBusinessMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ business }) => {
            this.business = business;
        });
    }

    previousState() {
        window.history.back();
    }
}
