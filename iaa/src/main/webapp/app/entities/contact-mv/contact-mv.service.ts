import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IContactMv } from 'app/shared/model/contact-mv.model';

type EntityResponseType = HttpResponse<IContactMv>;
type EntityArrayResponseType = HttpResponse<IContactMv[]>;

@Injectable({ providedIn: 'root' })
export class ContactMvService {
    private resourceUrl = SERVER_API_URL + 'api/contacts';

    constructor(private http: HttpClient) {}

    create(contact: IContactMv): Observable<EntityResponseType> {
        return this.http.post<IContactMv>(this.resourceUrl, contact, { observe: 'response' });
    }

    update(contact: IContactMv): Observable<EntityResponseType> {
        return this.http.put<IContactMv>(this.resourceUrl, contact, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IContactMv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IContactMv[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
