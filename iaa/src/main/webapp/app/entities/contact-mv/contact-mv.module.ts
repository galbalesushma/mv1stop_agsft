import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    ContactMvComponent,
    ContactMvDetailComponent,
    ContactMvUpdateComponent,
    ContactMvDeletePopupComponent,
    ContactMvDeleteDialogComponent,
    contactRoute,
    contactPopupRoute
} from './';

const ENTITY_STATES = [...contactRoute, ...contactPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ContactMvComponent,
        ContactMvDetailComponent,
        ContactMvUpdateComponent,
        ContactMvDeleteDialogComponent,
        ContactMvDeletePopupComponent
    ],
    entryComponents: [ContactMvComponent, ContactMvUpdateComponent, ContactMvDeleteDialogComponent, ContactMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaContactMvModule {}
