import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IContactMv } from 'app/shared/model/contact-mv.model';
import { ContactMvService } from './contact-mv.service';
import { IAddressMv } from 'app/shared/model/address-mv.model';
import { AddressMvService } from 'app/entities/address-mv';

@Component({
    selector: 'jhi-contact-mv-update',
    templateUrl: './contact-mv-update.component.html'
})
export class ContactMvUpdateComponent implements OnInit {
    private _contact: IContactMv;
    isSaving: boolean;

    addresses: IAddressMv[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private contactService: ContactMvService,
        private addressService: AddressMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ contact }) => {
            this.contact = contact;
        });
        this.addressService.query().subscribe(
            (res: HttpResponse<IAddressMv[]>) => {
                this.addresses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.contact.id !== undefined) {
            this.subscribeToSaveResponse(this.contactService.update(this.contact));
        } else {
            this.subscribeToSaveResponse(this.contactService.create(this.contact));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IContactMv>>) {
        result.subscribe((res: HttpResponse<IContactMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackAddressById(index: number, item: IAddressMv) {
        return item.id;
    }
    get contact() {
        return this._contact;
    }

    set contact(contact: IContactMv) {
        this._contact = contact;
    }
}
