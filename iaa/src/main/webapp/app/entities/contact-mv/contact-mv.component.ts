import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IContactMv } from 'app/shared/model/contact-mv.model';
import { Principal } from 'app/core';
import { ContactMvService } from './contact-mv.service';

@Component({
    selector: 'jhi-contact-mv',
    templateUrl: './contact-mv.component.html'
})
export class ContactMvComponent implements OnInit, OnDestroy {
    contacts: IContactMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private contactService: ContactMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.contactService.query().subscribe(
            (res: HttpResponse<IContactMv[]>) => {
                this.contacts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInContacts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IContactMv) {
        return item.id;
    }

    registerChangeInContacts() {
        this.eventSubscriber = this.eventManager.subscribe('contactListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
