import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CartMv } from 'app/shared/model/cart-mv.model';
import { CartMvService } from './cart-mv.service';
import { CartMvComponent } from './cart-mv.component';
import { CartMvDetailComponent } from './cart-mv-detail.component';
import { CartMvUpdateComponent } from './cart-mv-update.component';
import { CartMvDeletePopupComponent } from './cart-mv-delete-dialog.component';
import { ICartMv } from 'app/shared/model/cart-mv.model';

@Injectable({ providedIn: 'root' })
export class CartMvResolve implements Resolve<ICartMv> {
    constructor(private service: CartMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((cart: HttpResponse<CartMv>) => cart.body));
        }
        return of(new CartMv());
    }
}

export const cartRoute: Routes = [
    {
        path: 'cart-mv',
        component: CartMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.cart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'cart-mv/:id/view',
        component: CartMvDetailComponent,
        resolve: {
            cart: CartMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.cart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'cart-mv/new',
        component: CartMvUpdateComponent,
        resolve: {
            cart: CartMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.cart.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'cart-mv/:id/edit',
        component: CartMvUpdateComponent,
        resolve: {
            cart: CartMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.cart.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cartPopupRoute: Routes = [
    {
        path: 'cart-mv/:id/delete',
        component: CartMvDeletePopupComponent,
        resolve: {
            cart: CartMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.cart.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
