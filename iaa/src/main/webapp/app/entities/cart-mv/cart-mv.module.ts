import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    CartMvComponent,
    CartMvDetailComponent,
    CartMvUpdateComponent,
    CartMvDeletePopupComponent,
    CartMvDeleteDialogComponent,
    cartRoute,
    cartPopupRoute
} from './';

const ENTITY_STATES = [...cartRoute, ...cartPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [CartMvComponent, CartMvDetailComponent, CartMvUpdateComponent, CartMvDeleteDialogComponent, CartMvDeletePopupComponent],
    entryComponents: [CartMvComponent, CartMvUpdateComponent, CartMvDeleteDialogComponent, CartMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaCartMvModule {}
