import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICartMv } from 'app/shared/model/cart-mv.model';

type EntityResponseType = HttpResponse<ICartMv>;
type EntityArrayResponseType = HttpResponse<ICartMv[]>;

@Injectable({ providedIn: 'root' })
export class CartMvService {
    private resourceUrl = SERVER_API_URL + 'api/carts';

    constructor(private http: HttpClient) {}

    create(cart: ICartMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(cart);
        return this.http
            .post<ICartMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(cart: ICartMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(cart);
        return this.http
            .put<ICartMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ICartMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ICartMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(cart: ICartMv): ICartMv {
        const copy: ICartMv = Object.assign({}, cart, {
            createdAt: cart.createdAt != null && cart.createdAt.isValid() ? cart.createdAt.toJSON() : null,
            updatedAt: cart.updatedAt != null && cart.updatedAt.isValid() ? cart.updatedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((cart: ICartMv) => {
            cart.createdAt = cart.createdAt != null ? moment(cart.createdAt) : null;
            cart.updatedAt = cart.updatedAt != null ? moment(cart.updatedAt) : null;
        });
        return res;
    }
}
