import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICartMv } from 'app/shared/model/cart-mv.model';

@Component({
    selector: 'jhi-cart-mv-detail',
    templateUrl: './cart-mv-detail.component.html'
})
export class CartMvDetailComponent implements OnInit {
    cart: ICartMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ cart }) => {
            this.cart = cart;
        });
    }

    previousState() {
        window.history.back();
    }
}
