import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { ICartMv } from 'app/shared/model/cart-mv.model';
import { CartMvService } from './cart-mv.service';
import { IPackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';
import { PackSubscriptionMvService } from 'app/entities/pack-subscription-mv';
import { IProfileMv } from 'app/shared/model/profile-mv.model';
import { ProfileMvService } from 'app/entities/profile-mv';

@Component({
    selector: 'jhi-cart-mv-update',
    templateUrl: './cart-mv-update.component.html'
})
export class CartMvUpdateComponent implements OnInit {
    private _cart: ICartMv;
    isSaving: boolean;

    packsubscriptions: IPackSubscriptionMv[];

    profiles: IProfileMv[];
    createdAt: string;
    updatedAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private cartService: CartMvService,
        private packSubscriptionService: PackSubscriptionMvService,
        private profileService: ProfileMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ cart }) => {
            this.cart = cart;
        });
        this.packSubscriptionService.query().subscribe(
            (res: HttpResponse<IPackSubscriptionMv[]>) => {
                this.packsubscriptions = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.profileService.query().subscribe(
            (res: HttpResponse<IProfileMv[]>) => {
                this.profiles = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.cart.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.cart.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        if (this.cart.id !== undefined) {
            this.subscribeToSaveResponse(this.cartService.update(this.cart));
        } else {
            this.subscribeToSaveResponse(this.cartService.create(this.cart));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICartMv>>) {
        result.subscribe((res: HttpResponse<ICartMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPackSubscriptionById(index: number, item: IPackSubscriptionMv) {
        return item.id;
    }

    trackProfileById(index: number, item: IProfileMv) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
    get cart() {
        return this._cart;
    }

    set cart(cart: ICartMv) {
        this._cart = cart;
        this.createdAt = moment(cart.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(cart.updatedAt).format(DATE_TIME_FORMAT);
    }
}
