import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICountyMv } from 'app/shared/model/county-mv.model';

@Component({
    selector: 'jhi-county-mv-detail',
    templateUrl: './county-mv-detail.component.html'
})
export class CountyMvDetailComponent implements OnInit {
    county: ICountyMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ county }) => {
            this.county = county;
        });
    }

    previousState() {
        window.history.back();
    }
}
