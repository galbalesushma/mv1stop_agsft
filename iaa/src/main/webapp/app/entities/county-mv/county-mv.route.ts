import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CountyMv } from 'app/shared/model/county-mv.model';
import { CountyMvService } from './county-mv.service';
import { CountyMvComponent } from './county-mv.component';
import { CountyMvDetailComponent } from './county-mv-detail.component';
import { CountyMvUpdateComponent } from './county-mv-update.component';
import { CountyMvDeletePopupComponent } from './county-mv-delete-dialog.component';
import { ICountyMv } from 'app/shared/model/county-mv.model';

@Injectable({ providedIn: 'root' })
export class CountyMvResolve implements Resolve<ICountyMv> {
    constructor(private service: CountyMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((county: HttpResponse<CountyMv>) => county.body));
        }
        return of(new CountyMv());
    }
}

export const countyRoute: Routes = [
    {
        path: 'county-mv',
        component: CountyMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.county.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'county-mv/:id/view',
        component: CountyMvDetailComponent,
        resolve: {
            county: CountyMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.county.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'county-mv/new',
        component: CountyMvUpdateComponent,
        resolve: {
            county: CountyMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.county.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'county-mv/:id/edit',
        component: CountyMvUpdateComponent,
        resolve: {
            county: CountyMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.county.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const countyPopupRoute: Routes = [
    {
        path: 'county-mv/:id/delete',
        component: CountyMvDeletePopupComponent,
        resolve: {
            county: CountyMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.county.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
