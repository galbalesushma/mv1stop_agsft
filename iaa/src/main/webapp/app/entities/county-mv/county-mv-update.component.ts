import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICountyMv } from 'app/shared/model/county-mv.model';
import { CountyMvService } from './county-mv.service';

@Component({
    selector: 'jhi-county-mv-update',
    templateUrl: './county-mv-update.component.html'
})
export class CountyMvUpdateComponent implements OnInit {
    private _county: ICountyMv;
    isSaving: boolean;

    constructor(private countyService: CountyMvService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ county }) => {
            this.county = county;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.county.id !== undefined) {
            this.subscribeToSaveResponse(this.countyService.update(this.county));
        } else {
            this.subscribeToSaveResponse(this.countyService.create(this.county));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICountyMv>>) {
        result.subscribe((res: HttpResponse<ICountyMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get county() {
        return this._county;
    }

    set county(county: ICountyMv) {
        this._county = county;
    }
}
