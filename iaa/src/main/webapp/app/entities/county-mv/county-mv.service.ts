import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICountyMv } from 'app/shared/model/county-mv.model';

type EntityResponseType = HttpResponse<ICountyMv>;
type EntityArrayResponseType = HttpResponse<ICountyMv[]>;

@Injectable({ providedIn: 'root' })
export class CountyMvService {
    private resourceUrl = SERVER_API_URL + 'api/counties';

    constructor(private http: HttpClient) {}

    create(county: ICountyMv): Observable<EntityResponseType> {
        return this.http.post<ICountyMv>(this.resourceUrl, county, { observe: 'response' });
    }

    update(county: ICountyMv): Observable<EntityResponseType> {
        return this.http.put<ICountyMv>(this.resourceUrl, county, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICountyMv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICountyMv[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
