import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICountyMv } from 'app/shared/model/county-mv.model';
import { Principal } from 'app/core';
import { CountyMvService } from './county-mv.service';

@Component({
    selector: 'jhi-county-mv',
    templateUrl: './county-mv.component.html'
})
export class CountyMvComponent implements OnInit, OnDestroy {
    counties: ICountyMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private countyService: CountyMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.countyService.query().subscribe(
            (res: HttpResponse<ICountyMv[]>) => {
                this.counties = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCounties();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICountyMv) {
        return item.id;
    }

    registerChangeInCounties() {
        this.eventSubscriber = this.eventManager.subscribe('countyListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
