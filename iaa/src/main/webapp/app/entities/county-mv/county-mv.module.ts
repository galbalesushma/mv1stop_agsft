import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    CountyMvComponent,
    CountyMvDetailComponent,
    CountyMvUpdateComponent,
    CountyMvDeletePopupComponent,
    CountyMvDeleteDialogComponent,
    countyRoute,
    countyPopupRoute
} from './';

const ENTITY_STATES = [...countyRoute, ...countyPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CountyMvComponent,
        CountyMvDetailComponent,
        CountyMvUpdateComponent,
        CountyMvDeleteDialogComponent,
        CountyMvDeletePopupComponent
    ],
    entryComponents: [CountyMvComponent, CountyMvUpdateComponent, CountyMvDeleteDialogComponent, CountyMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaCountyMvModule {}
