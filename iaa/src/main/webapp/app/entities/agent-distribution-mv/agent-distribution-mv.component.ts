import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';
import { Principal } from 'app/core';
import { AgentDistributionMvService } from './agent-distribution-mv.service';

@Component({
    selector: 'jhi-agent-distribution-mv',
    templateUrl: './agent-distribution-mv.component.html'
})
export class AgentDistributionMvComponent implements OnInit, OnDestroy {
    agentDistributions: IAgentDistributionMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private agentDistributionService: AgentDistributionMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.agentDistributionService.query().subscribe(
            (res: HttpResponse<IAgentDistributionMv[]>) => {
                this.agentDistributions = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAgentDistributions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAgentDistributionMv) {
        return item.id;
    }

    registerChangeInAgentDistributions() {
        this.eventSubscriber = this.eventManager.subscribe('agentDistributionListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
