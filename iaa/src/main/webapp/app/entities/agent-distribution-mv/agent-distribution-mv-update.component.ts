import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IAgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';
import { AgentDistributionMvService } from './agent-distribution-mv.service';
import { IZipAreaMv } from 'app/shared/model/zip-area-mv.model';
import { ZipAreaMvService } from 'app/entities/zip-area-mv';

@Component({
    selector: 'jhi-agent-distribution-mv-update',
    templateUrl: './agent-distribution-mv-update.component.html'
})
export class AgentDistributionMvUpdateComponent implements OnInit {
    private _agentDistribution: IAgentDistributionMv;
    isSaving: boolean;

    zips: IZipAreaMv[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private agentDistributionService: AgentDistributionMvService,
        private zipAreaService: ZipAreaMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ agentDistribution }) => {
            this.agentDistribution = agentDistribution;
        });
        this.zipAreaService.query({ filter: 'agentdistribution-is-null' }).subscribe(
            (res: HttpResponse<IZipAreaMv[]>) => {
                if (!this.agentDistribution.zipId) {
                    this.zips = res.body;
                } else {
                    this.zipAreaService.find(this.agentDistribution.zipId).subscribe(
                        (subRes: HttpResponse<IZipAreaMv>) => {
                            this.zips = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.agentDistribution.id !== undefined) {
            this.subscribeToSaveResponse(this.agentDistributionService.update(this.agentDistribution));
        } else {
            this.subscribeToSaveResponse(this.agentDistributionService.create(this.agentDistribution));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAgentDistributionMv>>) {
        result.subscribe((res: HttpResponse<IAgentDistributionMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackZipAreaById(index: number, item: IZipAreaMv) {
        return item.id;
    }
    get agentDistribution() {
        return this._agentDistribution;
    }

    set agentDistribution(agentDistribution: IAgentDistributionMv) {
        this._agentDistribution = agentDistribution;
    }
}
