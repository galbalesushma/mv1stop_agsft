import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';

type EntityResponseType = HttpResponse<IAgentDistributionMv>;
type EntityArrayResponseType = HttpResponse<IAgentDistributionMv[]>;

@Injectable({ providedIn: 'root' })
export class AgentDistributionMvService {
    private resourceUrl = SERVER_API_URL + 'api/agent-distributions';

    constructor(private http: HttpClient) {}

    create(agentDistribution: IAgentDistributionMv): Observable<EntityResponseType> {
        return this.http.post<IAgentDistributionMv>(this.resourceUrl, agentDistribution, { observe: 'response' });
    }

    update(agentDistribution: IAgentDistributionMv): Observable<EntityResponseType> {
        return this.http.put<IAgentDistributionMv>(this.resourceUrl, agentDistribution, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IAgentDistributionMv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IAgentDistributionMv[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
