import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';
import { AgentDistributionMvService } from './agent-distribution-mv.service';
import { AgentDistributionMvComponent } from './agent-distribution-mv.component';
import { AgentDistributionMvDetailComponent } from './agent-distribution-mv-detail.component';
import { AgentDistributionMvUpdateComponent } from './agent-distribution-mv-update.component';
import { AgentDistributionMvDeletePopupComponent } from './agent-distribution-mv-delete-dialog.component';
import { IAgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';

@Injectable({ providedIn: 'root' })
export class AgentDistributionMvResolve implements Resolve<IAgentDistributionMv> {
    constructor(private service: AgentDistributionMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((agentDistribution: HttpResponse<AgentDistributionMv>) => agentDistribution.body));
        }
        return of(new AgentDistributionMv());
    }
}

export const agentDistributionRoute: Routes = [
    {
        path: 'agent-distribution-mv',
        component: AgentDistributionMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentDistribution.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-distribution-mv/:id/view',
        component: AgentDistributionMvDetailComponent,
        resolve: {
            agentDistribution: AgentDistributionMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentDistribution.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-distribution-mv/new',
        component: AgentDistributionMvUpdateComponent,
        resolve: {
            agentDistribution: AgentDistributionMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentDistribution.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-distribution-mv/:id/edit',
        component: AgentDistributionMvUpdateComponent,
        resolve: {
            agentDistribution: AgentDistributionMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentDistribution.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const agentDistributionPopupRoute: Routes = [
    {
        path: 'agent-distribution-mv/:id/delete',
        component: AgentDistributionMvDeletePopupComponent,
        resolve: {
            agentDistribution: AgentDistributionMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentDistribution.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
