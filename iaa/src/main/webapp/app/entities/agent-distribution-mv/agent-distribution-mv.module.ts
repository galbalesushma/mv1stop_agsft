import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    AgentDistributionMvComponent,
    AgentDistributionMvDetailComponent,
    AgentDistributionMvUpdateComponent,
    AgentDistributionMvDeletePopupComponent,
    AgentDistributionMvDeleteDialogComponent,
    agentDistributionRoute,
    agentDistributionPopupRoute
} from './';

const ENTITY_STATES = [...agentDistributionRoute, ...agentDistributionPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AgentDistributionMvComponent,
        AgentDistributionMvDetailComponent,
        AgentDistributionMvUpdateComponent,
        AgentDistributionMvDeleteDialogComponent,
        AgentDistributionMvDeletePopupComponent
    ],
    entryComponents: [
        AgentDistributionMvComponent,
        AgentDistributionMvUpdateComponent,
        AgentDistributionMvDeleteDialogComponent,
        AgentDistributionMvDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaAgentDistributionMvModule {}
