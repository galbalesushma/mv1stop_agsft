import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';
import { AgentDistributionMvService } from './agent-distribution-mv.service';

@Component({
    selector: 'jhi-agent-distribution-mv-delete-dialog',
    templateUrl: './agent-distribution-mv-delete-dialog.component.html'
})
export class AgentDistributionMvDeleteDialogComponent {
    agentDistribution: IAgentDistributionMv;

    constructor(
        private agentDistributionService: AgentDistributionMvService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.agentDistributionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'agentDistributionListModification',
                content: 'Deleted an agentDistribution'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-agent-distribution-mv-delete-popup',
    template: ''
})
export class AgentDistributionMvDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ agentDistribution }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AgentDistributionMvDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.agentDistribution = agentDistribution;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
