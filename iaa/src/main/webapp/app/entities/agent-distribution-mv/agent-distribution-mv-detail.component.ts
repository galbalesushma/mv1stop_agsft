import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentDistributionMv } from 'app/shared/model/agent-distribution-mv.model';

@Component({
    selector: 'jhi-agent-distribution-mv-detail',
    templateUrl: './agent-distribution-mv-detail.component.html'
})
export class AgentDistributionMvDetailComponent implements OnInit {
    agentDistribution: IAgentDistributionMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ agentDistribution }) => {
            this.agentDistribution = agentDistribution;
        });
    }

    previousState() {
        window.history.back();
    }
}
