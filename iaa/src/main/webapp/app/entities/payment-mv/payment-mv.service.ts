import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPaymentMv } from 'app/shared/model/payment-mv.model';

type EntityResponseType = HttpResponse<IPaymentMv>;
type EntityArrayResponseType = HttpResponse<IPaymentMv[]>;

@Injectable({ providedIn: 'root' })
export class PaymentMvService {
    private resourceUrl = SERVER_API_URL + 'api/payments';

    constructor(private http: HttpClient) {}

    create(payment: IPaymentMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(payment);
        return this.http
            .post<IPaymentMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(payment: IPaymentMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(payment);
        return this.http
            .put<IPaymentMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IPaymentMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IPaymentMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(payment: IPaymentMv): IPaymentMv {
        const copy: IPaymentMv = Object.assign({}, payment, {
            createdAt: payment.createdAt != null && payment.createdAt.isValid() ? payment.createdAt.toJSON() : null,
            updatedAt: payment.updatedAt != null && payment.updatedAt.isValid() ? payment.updatedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((payment: IPaymentMv) => {
            payment.createdAt = payment.createdAt != null ? moment(payment.createdAt) : null;
            payment.updatedAt = payment.updatedAt != null ? moment(payment.updatedAt) : null;
        });
        return res;
    }
}
