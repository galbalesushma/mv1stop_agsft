import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IPaymentMv } from 'app/shared/model/payment-mv.model';
import { PaymentMvService } from './payment-mv.service';

@Component({
    selector: 'jhi-payment-mv-update',
    templateUrl: './payment-mv-update.component.html'
})
export class PaymentMvUpdateComponent implements OnInit {
    private _payment: IPaymentMv;
    isSaving: boolean;
    createdAt: string;
    updatedAt: string;

    constructor(private paymentService: PaymentMvService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ payment }) => {
            this.payment = payment;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.payment.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.payment.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        if (this.payment.id !== undefined) {
            this.subscribeToSaveResponse(this.paymentService.update(this.payment));
        } else {
            this.subscribeToSaveResponse(this.paymentService.create(this.payment));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPaymentMv>>) {
        result.subscribe((res: HttpResponse<IPaymentMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get payment() {
        return this._payment;
    }

    set payment(payment: IPaymentMv) {
        this._payment = payment;
        this.createdAt = moment(payment.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(payment.updatedAt).format(DATE_TIME_FORMAT);
    }
}
