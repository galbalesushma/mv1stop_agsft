import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PaymentMv } from 'app/shared/model/payment-mv.model';
import { PaymentMvService } from './payment-mv.service';
import { PaymentMvComponent } from './payment-mv.component';
import { PaymentMvDetailComponent } from './payment-mv-detail.component';
import { PaymentMvUpdateComponent } from './payment-mv-update.component';
import { PaymentMvDeletePopupComponent } from './payment-mv-delete-dialog.component';
import { IPaymentMv } from 'app/shared/model/payment-mv.model';

@Injectable({ providedIn: 'root' })
export class PaymentMvResolve implements Resolve<IPaymentMv> {
    constructor(private service: PaymentMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((payment: HttpResponse<PaymentMv>) => payment.body));
        }
        return of(new PaymentMv());
    }
}

export const paymentRoute: Routes = [
    {
        path: 'payment-mv',
        component: PaymentMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-mv/:id/view',
        component: PaymentMvDetailComponent,
        resolve: {
            payment: PaymentMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-mv/new',
        component: PaymentMvUpdateComponent,
        resolve: {
            payment: PaymentMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'payment-mv/:id/edit',
        component: PaymentMvUpdateComponent,
        resolve: {
            payment: PaymentMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const paymentPopupRoute: Routes = [
    {
        path: 'payment-mv/:id/delete',
        component: PaymentMvDeletePopupComponent,
        resolve: {
            payment: PaymentMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.payment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
