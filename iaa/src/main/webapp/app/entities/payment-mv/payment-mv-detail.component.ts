import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPaymentMv } from 'app/shared/model/payment-mv.model';

@Component({
    selector: 'jhi-payment-mv-detail',
    templateUrl: './payment-mv-detail.component.html'
})
export class PaymentMvDetailComponent implements OnInit {
    payment: IPaymentMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ payment }) => {
            this.payment = payment;
        });
    }

    previousState() {
        window.history.back();
    }
}
