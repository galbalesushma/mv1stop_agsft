import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    PaymentMvComponent,
    PaymentMvDetailComponent,
    PaymentMvUpdateComponent,
    PaymentMvDeletePopupComponent,
    PaymentMvDeleteDialogComponent,
    paymentRoute,
    paymentPopupRoute
} from './';

const ENTITY_STATES = [...paymentRoute, ...paymentPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PaymentMvComponent,
        PaymentMvDetailComponent,
        PaymentMvUpdateComponent,
        PaymentMvDeleteDialogComponent,
        PaymentMvDeletePopupComponent
    ],
    entryComponents: [PaymentMvComponent, PaymentMvUpdateComponent, PaymentMvDeleteDialogComponent, PaymentMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaPaymentMvModule {}
