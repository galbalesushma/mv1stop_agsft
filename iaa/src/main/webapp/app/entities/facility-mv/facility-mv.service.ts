import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IFacilityMv } from 'app/shared/model/facility-mv.model';

type EntityResponseType = HttpResponse<IFacilityMv>;
type EntityArrayResponseType = HttpResponse<IFacilityMv[]>;

@Injectable({ providedIn: 'root' })
export class FacilityMvService {
    private resourceUrl = SERVER_API_URL + 'api/facilities';

    constructor(private http: HttpClient) {}

    create(facility: IFacilityMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(facility);
        return this.http
            .post<IFacilityMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(facility: IFacilityMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(facility);
        return this.http
            .put<IFacilityMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IFacilityMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IFacilityMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(facility: IFacilityMv): IFacilityMv {
        const copy: IFacilityMv = Object.assign({}, facility, {
            createdAt: facility.createdAt != null && facility.createdAt.isValid() ? facility.createdAt.toJSON() : null,
            updatedAt: facility.updatedAt != null && facility.updatedAt.isValid() ? facility.updatedAt.toJSON() : null,
            verifiedAt: facility.verifiedAt != null && facility.verifiedAt.isValid() ? facility.verifiedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        res.body.verifiedAt = res.body.verifiedAt != null ? moment(res.body.verifiedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((facility: IFacilityMv) => {
            facility.createdAt = facility.createdAt != null ? moment(facility.createdAt) : null;
            facility.updatedAt = facility.updatedAt != null ? moment(facility.updatedAt) : null;
            facility.verifiedAt = facility.verifiedAt != null ? moment(facility.verifiedAt) : null;
        });
        return res;
    }
}
