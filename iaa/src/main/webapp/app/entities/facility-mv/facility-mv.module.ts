import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    FacilityMvComponent,
    FacilityMvDetailComponent,
    FacilityMvUpdateComponent,
    FacilityMvDeletePopupComponent,
    FacilityMvDeleteDialogComponent,
    facilityRoute,
    facilityPopupRoute
} from './';

const ENTITY_STATES = [...facilityRoute, ...facilityPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        FacilityMvComponent,
        FacilityMvDetailComponent,
        FacilityMvUpdateComponent,
        FacilityMvDeleteDialogComponent,
        FacilityMvDeletePopupComponent
    ],
    entryComponents: [FacilityMvComponent, FacilityMvUpdateComponent, FacilityMvDeleteDialogComponent, FacilityMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaFacilityMvModule {}
