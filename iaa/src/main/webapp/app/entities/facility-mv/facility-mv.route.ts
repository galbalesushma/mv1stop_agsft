import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { FacilityMv } from 'app/shared/model/facility-mv.model';
import { FacilityMvService } from './facility-mv.service';
import { FacilityMvComponent } from './facility-mv.component';
import { FacilityMvDetailComponent } from './facility-mv-detail.component';
import { FacilityMvUpdateComponent } from './facility-mv-update.component';
import { FacilityMvDeletePopupComponent } from './facility-mv-delete-dialog.component';
import { IFacilityMv } from 'app/shared/model/facility-mv.model';

@Injectable({ providedIn: 'root' })
export class FacilityMvResolve implements Resolve<IFacilityMv> {
    constructor(private service: FacilityMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((facility: HttpResponse<FacilityMv>) => facility.body));
        }
        return of(new FacilityMv());
    }
}

export const facilityRoute: Routes = [
    {
        path: 'facility-mv',
        component: FacilityMvComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'iaaApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'facility-mv/:id/view',
        component: FacilityMvDetailComponent,
        resolve: {
            facility: FacilityMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'facility-mv/new',
        component: FacilityMvUpdateComponent,
        resolve: {
            facility: FacilityMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'facility-mv/:id/edit',
        component: FacilityMvUpdateComponent,
        resolve: {
            facility: FacilityMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const facilityPopupRoute: Routes = [
    {
        path: 'facility-mv/:id/delete',
        component: FacilityMvDeletePopupComponent,
        resolve: {
            facility: FacilityMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.facility.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
