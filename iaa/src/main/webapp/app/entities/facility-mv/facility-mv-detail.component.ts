import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFacilityMv } from 'app/shared/model/facility-mv.model';

@Component({
    selector: 'jhi-facility-mv-detail',
    templateUrl: './facility-mv-detail.component.html'
})
export class FacilityMvDetailComponent implements OnInit {
    facility: IFacilityMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ facility }) => {
            this.facility = facility;
        });
    }

    previousState() {
        window.history.back();
    }
}
