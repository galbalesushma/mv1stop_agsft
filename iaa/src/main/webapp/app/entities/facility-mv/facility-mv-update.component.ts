import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IFacilityMv } from 'app/shared/model/facility-mv.model';
import { FacilityMvService } from './facility-mv.service';
import { IProfileMv } from 'app/shared/model/profile-mv.model';
import { ProfileMvService } from 'app/entities/profile-mv';
import { IBusinessMv } from 'app/shared/model/business-mv.model';
import { BusinessMvService } from 'app/entities/business-mv';

@Component({
    selector: 'jhi-facility-mv-update',
    templateUrl: './facility-mv-update.component.html'
})
export class FacilityMvUpdateComponent implements OnInit {
    private _facility: IFacilityMv;
    isSaving: boolean;

    profiles: IProfileMv[];

    businesses: IBusinessMv[];
    createdAt: string;
    updatedAt: string;
    verifiedAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private facilityService: FacilityMvService,
        private profileService: ProfileMvService,
        private businessService: BusinessMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ facility }) => {
            this.facility = facility;
        });
        this.profileService.query({ filter: 'facility-is-null' }).subscribe(
            (res: HttpResponse<IProfileMv[]>) => {
                if (!this.facility.profileId) {
                    this.profiles = res.body;
                } else {
                    this.profileService.find(this.facility.profileId).subscribe(
                        (subRes: HttpResponse<IProfileMv>) => {
                            this.profiles = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.businessService.query().subscribe(
            (res: HttpResponse<IBusinessMv[]>) => {
                this.businesses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.facility.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.facility.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        this.facility.verifiedAt = moment(this.verifiedAt, DATE_TIME_FORMAT);
        if (this.facility.id !== undefined) {
            this.subscribeToSaveResponse(this.facilityService.update(this.facility));
        } else {
            this.subscribeToSaveResponse(this.facilityService.create(this.facility));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IFacilityMv>>) {
        result.subscribe((res: HttpResponse<IFacilityMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProfileById(index: number, item: IProfileMv) {
        return item.id;
    }

    trackBusinessById(index: number, item: IBusinessMv) {
        return item.id;
    }
    get facility() {
        return this._facility;
    }

    set facility(facility: IFacilityMv) {
        this._facility = facility;
        this.createdAt = moment(facility.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(facility.updatedAt).format(DATE_TIME_FORMAT);
        this.verifiedAt = moment(facility.verifiedAt).format(DATE_TIME_FORMAT);
    }
}
