import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFacilityMv } from 'app/shared/model/facility-mv.model';
import { FacilityMvService } from './facility-mv.service';

@Component({
    selector: 'jhi-facility-mv-delete-dialog',
    templateUrl: './facility-mv-delete-dialog.component.html'
})
export class FacilityMvDeleteDialogComponent {
    facility: IFacilityMv;

    constructor(private facilityService: FacilityMvService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.facilityService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'facilityListModification',
                content: 'Deleted an facility'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-facility-mv-delete-popup',
    template: ''
})
export class FacilityMvDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ facility }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(FacilityMvDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.facility = facility;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
