import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    ProducerMvComponent,
    ProducerMvDetailComponent,
    ProducerMvUpdateComponent,
    ProducerMvDeletePopupComponent,
    ProducerMvDeleteDialogComponent,
    producerRoute,
    producerPopupRoute
} from './';

const ENTITY_STATES = [...producerRoute, ...producerPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ProducerMvComponent,
        ProducerMvDetailComponent,
        ProducerMvUpdateComponent,
        ProducerMvDeleteDialogComponent,
        ProducerMvDeletePopupComponent
    ],
    entryComponents: [ProducerMvComponent, ProducerMvUpdateComponent, ProducerMvDeleteDialogComponent, ProducerMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaProducerMvModule {}
