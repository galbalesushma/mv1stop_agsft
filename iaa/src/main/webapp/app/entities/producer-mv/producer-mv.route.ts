import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProducerMv } from 'app/shared/model/producer-mv.model';
import { ProducerMvService } from './producer-mv.service';
import { ProducerMvComponent } from './producer-mv.component';
import { ProducerMvDetailComponent } from './producer-mv-detail.component';
import { ProducerMvUpdateComponent } from './producer-mv-update.component';
import { ProducerMvDeletePopupComponent } from './producer-mv-delete-dialog.component';
import { IProducerMv } from 'app/shared/model/producer-mv.model';

@Injectable({ providedIn: 'root' })
export class ProducerMvResolve implements Resolve<IProducerMv> {
    constructor(private service: ProducerMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((producer: HttpResponse<ProducerMv>) => producer.body));
        }
        return of(new ProducerMv());
    }
}

export const producerRoute: Routes = [
    {
        path: 'producer-mv',
        component: ProducerMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.producer.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'producer-mv/:id/view',
        component: ProducerMvDetailComponent,
        resolve: {
            producer: ProducerMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.producer.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'producer-mv/new',
        component: ProducerMvUpdateComponent,
        resolve: {
            producer: ProducerMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.producer.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'producer-mv/:id/edit',
        component: ProducerMvUpdateComponent,
        resolve: {
            producer: ProducerMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.producer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const producerPopupRoute: Routes = [
    {
        path: 'producer-mv/:id/delete',
        component: ProducerMvDeletePopupComponent,
        resolve: {
            producer: ProducerMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.producer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
