import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProducerMv } from 'app/shared/model/producer-mv.model';

@Component({
    selector: 'jhi-producer-mv-detail',
    templateUrl: './producer-mv-detail.component.html'
})
export class ProducerMvDetailComponent implements OnInit {
    producer: IProducerMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ producer }) => {
            this.producer = producer;
        });
    }

    previousState() {
        window.history.back();
    }
}
