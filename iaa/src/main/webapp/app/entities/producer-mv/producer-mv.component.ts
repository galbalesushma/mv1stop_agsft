import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IProducerMv } from 'app/shared/model/producer-mv.model';
import { Principal } from 'app/core';
import { ProducerMvService } from './producer-mv.service';

@Component({
    selector: 'jhi-producer-mv',
    templateUrl: './producer-mv.component.html'
})
export class ProducerMvComponent implements OnInit, OnDestroy {
    producers: IProducerMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private producerService: ProducerMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.producerService.query().subscribe(
            (res: HttpResponse<IProducerMv[]>) => {
                this.producers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInProducers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IProducerMv) {
        return item.id;
    }

    registerChangeInProducers() {
        this.eventSubscriber = this.eventManager.subscribe('producerListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
