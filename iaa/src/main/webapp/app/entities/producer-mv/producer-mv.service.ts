import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IProducerMv } from 'app/shared/model/producer-mv.model';

type EntityResponseType = HttpResponse<IProducerMv>;
type EntityArrayResponseType = HttpResponse<IProducerMv[]>;

@Injectable({ providedIn: 'root' })
export class ProducerMvService {
    private resourceUrl = SERVER_API_URL + 'api/producers';

    constructor(private http: HttpClient) {}

    create(producer: IProducerMv): Observable<EntityResponseType> {
        return this.http.post<IProducerMv>(this.resourceUrl, producer, { observe: 'response' });
    }

    update(producer: IProducerMv): Observable<EntityResponseType> {
        return this.http.put<IProducerMv>(this.resourceUrl, producer, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IProducerMv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IProducerMv[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
