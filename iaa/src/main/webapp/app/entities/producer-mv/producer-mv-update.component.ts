import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IProducerMv } from 'app/shared/model/producer-mv.model';
import { ProducerMvService } from './producer-mv.service';
import { IAddressMv } from 'app/shared/model/address-mv.model';
import { AddressMvService } from 'app/entities/address-mv';

@Component({
    selector: 'jhi-producer-mv-update',
    templateUrl: './producer-mv-update.component.html'
})
export class ProducerMvUpdateComponent implements OnInit {
    private _producer: IProducerMv;
    isSaving: boolean;

    addrs: IAddressMv[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private producerService: ProducerMvService,
        private addressService: AddressMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ producer }) => {
            this.producer = producer;
        });
        this.addressService.query({ filter: 'producer-is-null' }).subscribe(
            (res: HttpResponse<IAddressMv[]>) => {
                if (!this.producer.addrId) {
                    this.addrs = res.body;
                } else {
                    this.addressService.find(this.producer.addrId).subscribe(
                        (subRes: HttpResponse<IAddressMv>) => {
                            this.addrs = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.producer.id !== undefined) {
            this.subscribeToSaveResponse(this.producerService.update(this.producer));
        } else {
            this.subscribeToSaveResponse(this.producerService.create(this.producer));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IProducerMv>>) {
        result.subscribe((res: HttpResponse<IProducerMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackAddressById(index: number, item: IAddressMv) {
        return item.id;
    }
    get producer() {
        return this._producer;
    }

    set producer(producer: IProducerMv) {
        this._producer = producer;
    }
}
