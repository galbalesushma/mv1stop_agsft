import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';
import { PackSubscriptionMvService } from './pack-subscription-mv.service';

@Component({
    selector: 'jhi-pack-subscription-mv-delete-dialog',
    templateUrl: './pack-subscription-mv-delete-dialog.component.html'
})
export class PackSubscriptionMvDeleteDialogComponent {
    packSubscription: IPackSubscriptionMv;

    constructor(
        private packSubscriptionService: PackSubscriptionMvService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.packSubscriptionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'packSubscriptionListModification',
                content: 'Deleted an packSubscription'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pack-subscription-mv-delete-popup',
    template: ''
})
export class PackSubscriptionMvDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ packSubscription }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PackSubscriptionMvDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.packSubscription = packSubscription;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
