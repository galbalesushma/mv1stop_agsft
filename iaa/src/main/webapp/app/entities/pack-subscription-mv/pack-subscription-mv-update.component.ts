import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IPackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';
import { PackSubscriptionMvService } from './pack-subscription-mv.service';
import { IProfileMv } from 'app/shared/model/profile-mv.model';
import { ProfileMvService } from 'app/entities/profile-mv';

@Component({
    selector: 'jhi-pack-subscription-mv-update',
    templateUrl: './pack-subscription-mv-update.component.html'
})
export class PackSubscriptionMvUpdateComponent implements OnInit {
    private _packSubscription: IPackSubscriptionMv;
    isSaving: boolean;

    profiles: IProfileMv[];
    createdAt: string;
    updatedAt: string;
    validTill: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private packSubscriptionService: PackSubscriptionMvService,
        private profileService: ProfileMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ packSubscription }) => {
            this.packSubscription = packSubscription;
        });
        this.profileService.query().subscribe(
            (res: HttpResponse<IProfileMv[]>) => {
                this.profiles = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.packSubscription.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.packSubscription.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        this.packSubscription.validTill = moment(this.validTill, DATE_TIME_FORMAT);
        if (this.packSubscription.id !== undefined) {
            this.subscribeToSaveResponse(this.packSubscriptionService.update(this.packSubscription));
        } else {
            this.subscribeToSaveResponse(this.packSubscriptionService.create(this.packSubscription));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPackSubscriptionMv>>) {
        result.subscribe((res: HttpResponse<IPackSubscriptionMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProfileById(index: number, item: IProfileMv) {
        return item.id;
    }
    get packSubscription() {
        return this._packSubscription;
    }

    set packSubscription(packSubscription: IPackSubscriptionMv) {
        this._packSubscription = packSubscription;
        this.createdAt = moment(packSubscription.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(packSubscription.updatedAt).format(DATE_TIME_FORMAT);
        this.validTill = moment(packSubscription.validTill).format(DATE_TIME_FORMAT);
    }
}
