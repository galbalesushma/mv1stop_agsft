import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';

@Component({
    selector: 'jhi-pack-subscription-mv-detail',
    templateUrl: './pack-subscription-mv-detail.component.html'
})
export class PackSubscriptionMvDetailComponent implements OnInit {
    packSubscription: IPackSubscriptionMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ packSubscription }) => {
            this.packSubscription = packSubscription;
        });
    }

    previousState() {
        window.history.back();
    }
}
