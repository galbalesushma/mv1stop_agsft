import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';

type EntityResponseType = HttpResponse<IPackSubscriptionMv>;
type EntityArrayResponseType = HttpResponse<IPackSubscriptionMv[]>;

@Injectable({ providedIn: 'root' })
export class PackSubscriptionMvService {
    private resourceUrl = SERVER_API_URL + 'api/pack-subscriptions';

    constructor(private http: HttpClient) {}

    create(packSubscription: IPackSubscriptionMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(packSubscription);
        return this.http
            .post<IPackSubscriptionMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(packSubscription: IPackSubscriptionMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(packSubscription);
        return this.http
            .put<IPackSubscriptionMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IPackSubscriptionMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IPackSubscriptionMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(packSubscription: IPackSubscriptionMv): IPackSubscriptionMv {
        const copy: IPackSubscriptionMv = Object.assign({}, packSubscription, {
            createdAt:
                packSubscription.createdAt != null && packSubscription.createdAt.isValid() ? packSubscription.createdAt.toJSON() : null,
            updatedAt:
                packSubscription.updatedAt != null && packSubscription.updatedAt.isValid() ? packSubscription.updatedAt.toJSON() : null,
            validTill:
                packSubscription.validTill != null && packSubscription.validTill.isValid() ? packSubscription.validTill.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        res.body.validTill = res.body.validTill != null ? moment(res.body.validTill) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((packSubscription: IPackSubscriptionMv) => {
            packSubscription.createdAt = packSubscription.createdAt != null ? moment(packSubscription.createdAt) : null;
            packSubscription.updatedAt = packSubscription.updatedAt != null ? moment(packSubscription.updatedAt) : null;
            packSubscription.validTill = packSubscription.validTill != null ? moment(packSubscription.validTill) : null;
        });
        return res;
    }
}
