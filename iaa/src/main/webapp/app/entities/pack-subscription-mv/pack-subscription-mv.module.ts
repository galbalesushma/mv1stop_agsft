import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    PackSubscriptionMvComponent,
    PackSubscriptionMvDetailComponent,
    PackSubscriptionMvUpdateComponent,
    PackSubscriptionMvDeletePopupComponent,
    PackSubscriptionMvDeleteDialogComponent,
    packSubscriptionRoute,
    packSubscriptionPopupRoute
} from './';

const ENTITY_STATES = [...packSubscriptionRoute, ...packSubscriptionPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PackSubscriptionMvComponent,
        PackSubscriptionMvDetailComponent,
        PackSubscriptionMvUpdateComponent,
        PackSubscriptionMvDeleteDialogComponent,
        PackSubscriptionMvDeletePopupComponent
    ],
    entryComponents: [
        PackSubscriptionMvComponent,
        PackSubscriptionMvUpdateComponent,
        PackSubscriptionMvDeleteDialogComponent,
        PackSubscriptionMvDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaPackSubscriptionMvModule {}
