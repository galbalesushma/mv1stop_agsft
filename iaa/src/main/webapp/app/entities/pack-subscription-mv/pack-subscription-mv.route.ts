import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';
import { PackSubscriptionMvService } from './pack-subscription-mv.service';
import { PackSubscriptionMvComponent } from './pack-subscription-mv.component';
import { PackSubscriptionMvDetailComponent } from './pack-subscription-mv-detail.component';
import { PackSubscriptionMvUpdateComponent } from './pack-subscription-mv-update.component';
import { PackSubscriptionMvDeletePopupComponent } from './pack-subscription-mv-delete-dialog.component';
import { IPackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';

@Injectable({ providedIn: 'root' })
export class PackSubscriptionMvResolve implements Resolve<IPackSubscriptionMv> {
    constructor(private service: PackSubscriptionMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((packSubscription: HttpResponse<PackSubscriptionMv>) => packSubscription.body));
        }
        return of(new PackSubscriptionMv());
    }
}

export const packSubscriptionRoute: Routes = [
    {
        path: 'pack-subscription-mv',
        component: PackSubscriptionMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.packSubscription.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pack-subscription-mv/:id/view',
        component: PackSubscriptionMvDetailComponent,
        resolve: {
            packSubscription: PackSubscriptionMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.packSubscription.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pack-subscription-mv/new',
        component: PackSubscriptionMvUpdateComponent,
        resolve: {
            packSubscription: PackSubscriptionMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.packSubscription.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'pack-subscription-mv/:id/edit',
        component: PackSubscriptionMvUpdateComponent,
        resolve: {
            packSubscription: PackSubscriptionMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.packSubscription.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const packSubscriptionPopupRoute: Routes = [
    {
        path: 'pack-subscription-mv/:id/delete',
        component: PackSubscriptionMvDeletePopupComponent,
        resolve: {
            packSubscription: PackSubscriptionMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.packSubscription.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
