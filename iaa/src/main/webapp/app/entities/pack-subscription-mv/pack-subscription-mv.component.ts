import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IPackSubscriptionMv } from 'app/shared/model/pack-subscription-mv.model';
import { Principal } from 'app/core';
import { PackSubscriptionMvService } from './pack-subscription-mv.service';

@Component({
    selector: 'jhi-pack-subscription-mv',
    templateUrl: './pack-subscription-mv.component.html'
})
export class PackSubscriptionMvComponent implements OnInit, OnDestroy {
    packSubscriptions: IPackSubscriptionMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private packSubscriptionService: PackSubscriptionMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.packSubscriptionService.query().subscribe(
            (res: HttpResponse<IPackSubscriptionMv[]>) => {
                this.packSubscriptions = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInPackSubscriptions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPackSubscriptionMv) {
        return item.id;
    }

    registerChangeInPackSubscriptions() {
        this.eventSubscriber = this.eventManager.subscribe('packSubscriptionListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
