import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IStateMv } from 'app/shared/model/state-mv.model';

type EntityResponseType = HttpResponse<IStateMv>;
type EntityArrayResponseType = HttpResponse<IStateMv[]>;

@Injectable({ providedIn: 'root' })
export class StateMvService {
    private resourceUrl = SERVER_API_URL + 'api/states';

    constructor(private http: HttpClient) {}

    create(state: IStateMv): Observable<EntityResponseType> {
        return this.http.post<IStateMv>(this.resourceUrl, state, { observe: 'response' });
    }

    update(state: IStateMv): Observable<EntityResponseType> {
        return this.http.put<IStateMv>(this.resourceUrl, state, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IStateMv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IStateMv[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
