import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { StateMv } from 'app/shared/model/state-mv.model';
import { StateMvService } from './state-mv.service';
import { StateMvComponent } from './state-mv.component';
import { StateMvDetailComponent } from './state-mv-detail.component';
import { StateMvUpdateComponent } from './state-mv-update.component';
import { StateMvDeletePopupComponent } from './state-mv-delete-dialog.component';
import { IStateMv } from 'app/shared/model/state-mv.model';

@Injectable({ providedIn: 'root' })
export class StateMvResolve implements Resolve<IStateMv> {
    constructor(private service: StateMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((state: HttpResponse<StateMv>) => state.body));
        }
        return of(new StateMv());
    }
}

export const stateRoute: Routes = [
    {
        path: 'state-mv',
        component: StateMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.state.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'state-mv/:id/view',
        component: StateMvDetailComponent,
        resolve: {
            state: StateMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.state.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'state-mv/new',
        component: StateMvUpdateComponent,
        resolve: {
            state: StateMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.state.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'state-mv/:id/edit',
        component: StateMvUpdateComponent,
        resolve: {
            state: StateMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.state.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const statePopupRoute: Routes = [
    {
        path: 'state-mv/:id/delete',
        component: StateMvDeletePopupComponent,
        resolve: {
            state: StateMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.state.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
