import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    StateMvComponent,
    StateMvDetailComponent,
    StateMvUpdateComponent,
    StateMvDeletePopupComponent,
    StateMvDeleteDialogComponent,
    stateRoute,
    statePopupRoute
} from './';

const ENTITY_STATES = [...stateRoute, ...statePopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        StateMvComponent,
        StateMvDetailComponent,
        StateMvUpdateComponent,
        StateMvDeleteDialogComponent,
        StateMvDeletePopupComponent
    ],
    entryComponents: [StateMvComponent, StateMvUpdateComponent, StateMvDeleteDialogComponent, StateMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaStateMvModule {}
