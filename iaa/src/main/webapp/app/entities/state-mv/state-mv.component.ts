import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IStateMv } from 'app/shared/model/state-mv.model';
import { Principal } from 'app/core';
import { StateMvService } from './state-mv.service';

@Component({
    selector: 'jhi-state-mv',
    templateUrl: './state-mv.component.html'
})
export class StateMvComponent implements OnInit, OnDestroy {
    states: IStateMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private stateService: StateMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.stateService.query().subscribe(
            (res: HttpResponse<IStateMv[]>) => {
                this.states = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInStates();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IStateMv) {
        return item.id;
    }

    registerChangeInStates() {
        this.eventSubscriber = this.eventManager.subscribe('stateListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
