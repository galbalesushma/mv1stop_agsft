import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IStateMv } from 'app/shared/model/state-mv.model';

@Component({
    selector: 'jhi-state-mv-detail',
    templateUrl: './state-mv-detail.component.html'
})
export class StateMvDetailComponent implements OnInit {
    state: IStateMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ state }) => {
            this.state = state;
        });
    }

    previousState() {
        window.history.back();
    }
}
