import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IStateMv } from 'app/shared/model/state-mv.model';
import { StateMvService } from './state-mv.service';

@Component({
    selector: 'jhi-state-mv-update',
    templateUrl: './state-mv-update.component.html'
})
export class StateMvUpdateComponent implements OnInit {
    private _state: IStateMv;
    isSaving: boolean;

    constructor(private stateService: StateMvService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ state }) => {
            this.state = state;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.state.id !== undefined) {
            this.subscribeToSaveResponse(this.stateService.update(this.state));
        } else {
            this.subscribeToSaveResponse(this.stateService.create(this.state));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IStateMv>>) {
        result.subscribe((res: HttpResponse<IStateMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get state() {
        return this._state;
    }

    set state(state: IStateMv) {
        this._state = state;
    }
}
