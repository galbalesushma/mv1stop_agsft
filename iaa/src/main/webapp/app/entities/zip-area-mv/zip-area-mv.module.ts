import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    ZipAreaMvComponent,
    ZipAreaMvDetailComponent,
    ZipAreaMvUpdateComponent,
    ZipAreaMvDeletePopupComponent,
    ZipAreaMvDeleteDialogComponent,
    zipAreaRoute,
    zipAreaPopupRoute
} from './';

const ENTITY_STATES = [...zipAreaRoute, ...zipAreaPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ZipAreaMvComponent,
        ZipAreaMvDetailComponent,
        ZipAreaMvUpdateComponent,
        ZipAreaMvDeleteDialogComponent,
        ZipAreaMvDeletePopupComponent
    ],
    entryComponents: [ZipAreaMvComponent, ZipAreaMvUpdateComponent, ZipAreaMvDeleteDialogComponent, ZipAreaMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaZipAreaMvModule {}
