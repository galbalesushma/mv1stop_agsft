import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ZipAreaMv } from 'app/shared/model/zip-area-mv.model';
import { ZipAreaMvService } from './zip-area-mv.service';
import { ZipAreaMvComponent } from './zip-area-mv.component';
import { ZipAreaMvDetailComponent } from './zip-area-mv-detail.component';
import { ZipAreaMvUpdateComponent } from './zip-area-mv-update.component';
import { ZipAreaMvDeletePopupComponent } from './zip-area-mv-delete-dialog.component';
import { IZipAreaMv } from 'app/shared/model/zip-area-mv.model';

@Injectable({ providedIn: 'root' })
export class ZipAreaMvResolve implements Resolve<IZipAreaMv> {
    constructor(private service: ZipAreaMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((zipArea: HttpResponse<ZipAreaMv>) => zipArea.body));
        }
        return of(new ZipAreaMv());
    }
}

export const zipAreaRoute: Routes = [
    {
        path: 'zip-area-mv',
        component: ZipAreaMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.zipArea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'zip-area-mv/:id/view',
        component: ZipAreaMvDetailComponent,
        resolve: {
            zipArea: ZipAreaMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.zipArea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'zip-area-mv/new',
        component: ZipAreaMvUpdateComponent,
        resolve: {
            zipArea: ZipAreaMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.zipArea.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'zip-area-mv/:id/edit',
        component: ZipAreaMvUpdateComponent,
        resolve: {
            zipArea: ZipAreaMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.zipArea.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const zipAreaPopupRoute: Routes = [
    {
        path: 'zip-area-mv/:id/delete',
        component: ZipAreaMvDeletePopupComponent,
        resolve: {
            zipArea: ZipAreaMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.zipArea.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
