import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IZipAreaMv } from 'app/shared/model/zip-area-mv.model';

type EntityResponseType = HttpResponse<IZipAreaMv>;
type EntityArrayResponseType = HttpResponse<IZipAreaMv[]>;

@Injectable({ providedIn: 'root' })
export class ZipAreaMvService {
    private resourceUrl = SERVER_API_URL + 'api/zip-areas';

    constructor(private http: HttpClient) {}

    create(zipArea: IZipAreaMv): Observable<EntityResponseType> {
        return this.http.post<IZipAreaMv>(this.resourceUrl, zipArea, { observe: 'response' });
    }

    update(zipArea: IZipAreaMv): Observable<EntityResponseType> {
        return this.http.put<IZipAreaMv>(this.resourceUrl, zipArea, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IZipAreaMv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IZipAreaMv[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
