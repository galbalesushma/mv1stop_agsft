import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IZipAreaMv } from 'app/shared/model/zip-area-mv.model';
import { ZipAreaMvService } from './zip-area-mv.service';

@Component({
    selector: 'jhi-zip-area-mv-update',
    templateUrl: './zip-area-mv-update.component.html'
})
export class ZipAreaMvUpdateComponent implements OnInit {
    private _zipArea: IZipAreaMv;
    isSaving: boolean;

    constructor(private zipAreaService: ZipAreaMvService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ zipArea }) => {
            this.zipArea = zipArea;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.zipArea.id !== undefined) {
            this.subscribeToSaveResponse(this.zipAreaService.update(this.zipArea));
        } else {
            this.subscribeToSaveResponse(this.zipAreaService.create(this.zipArea));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IZipAreaMv>>) {
        result.subscribe((res: HttpResponse<IZipAreaMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get zipArea() {
        return this._zipArea;
    }

    set zipArea(zipArea: IZipAreaMv) {
        this._zipArea = zipArea;
    }
}
