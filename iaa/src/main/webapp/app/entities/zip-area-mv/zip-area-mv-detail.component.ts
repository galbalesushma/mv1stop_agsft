import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IZipAreaMv } from 'app/shared/model/zip-area-mv.model';

@Component({
    selector: 'jhi-zip-area-mv-detail',
    templateUrl: './zip-area-mv-detail.component.html'
})
export class ZipAreaMvDetailComponent implements OnInit {
    zipArea: IZipAreaMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ zipArea }) => {
            this.zipArea = zipArea;
        });
    }

    previousState() {
        window.history.back();
    }
}
