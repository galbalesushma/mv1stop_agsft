import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IZipAreaMv } from 'app/shared/model/zip-area-mv.model';
import { ZipAreaMvService } from './zip-area-mv.service';

@Component({
    selector: 'jhi-zip-area-mv-delete-dialog',
    templateUrl: './zip-area-mv-delete-dialog.component.html'
})
export class ZipAreaMvDeleteDialogComponent {
    zipArea: IZipAreaMv;

    constructor(private zipAreaService: ZipAreaMvService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.zipAreaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'zipAreaListModification',
                content: 'Deleted an zipArea'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-zip-area-mv-delete-popup',
    template: ''
})
export class ZipAreaMvDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ zipArea }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ZipAreaMvDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.zipArea = zipArea;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
