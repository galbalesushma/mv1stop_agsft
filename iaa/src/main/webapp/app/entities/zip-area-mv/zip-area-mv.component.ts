import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IZipAreaMv } from 'app/shared/model/zip-area-mv.model';
import { Principal } from 'app/core';
import { ZipAreaMvService } from './zip-area-mv.service';

@Component({
    selector: 'jhi-zip-area-mv',
    templateUrl: './zip-area-mv.component.html'
})
export class ZipAreaMvComponent implements OnInit, OnDestroy {
    zipAreas: IZipAreaMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private zipAreaService: ZipAreaMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.zipAreaService.query().subscribe(
            (res: HttpResponse<IZipAreaMv[]>) => {
                this.zipAreas = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInZipAreas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IZipAreaMv) {
        return item.id;
    }

    registerChangeInZipAreas() {
        this.eventSubscriber = this.eventManager.subscribe('zipAreaListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
