import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAgentLicMv } from 'app/shared/model/agent-lic-mv.model';

type EntityResponseType = HttpResponse<IAgentLicMv>;
type EntityArrayResponseType = HttpResponse<IAgentLicMv[]>;

@Injectable({ providedIn: 'root' })
export class AgentLicMvService {
    private resourceUrl = SERVER_API_URL + 'api/agent-lics';

    constructor(private http: HttpClient) {}

    create(agentLic: IAgentLicMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(agentLic);
        return this.http
            .post<IAgentLicMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(agentLic: IAgentLicMv): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(agentLic);
        return this.http
            .put<IAgentLicMv>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IAgentLicMv>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IAgentLicMv[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(agentLic: IAgentLicMv): IAgentLicMv {
        const copy: IAgentLicMv = Object.assign({}, agentLic, {
            firstActiveDate:
                agentLic.firstActiveDate != null && agentLic.firstActiveDate.isValid() ? agentLic.firstActiveDate.toJSON() : null,
            effectiveDate: agentLic.effectiveDate != null && agentLic.effectiveDate.isValid() ? agentLic.effectiveDate.toJSON() : null,
            expirationDate: agentLic.expirationDate != null && agentLic.expirationDate.isValid() ? agentLic.expirationDate.toJSON() : null,
            createdAt: agentLic.createdAt != null && agentLic.createdAt.isValid() ? agentLic.createdAt.toJSON() : null,
            updatedAt: agentLic.updatedAt != null && agentLic.updatedAt.isValid() ? agentLic.updatedAt.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.firstActiveDate = res.body.firstActiveDate != null ? moment(res.body.firstActiveDate) : null;
        res.body.effectiveDate = res.body.effectiveDate != null ? moment(res.body.effectiveDate) : null;
        res.body.expirationDate = res.body.expirationDate != null ? moment(res.body.expirationDate) : null;
        res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
        res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((agentLic: IAgentLicMv) => {
            agentLic.firstActiveDate = agentLic.firstActiveDate != null ? moment(agentLic.firstActiveDate) : null;
            agentLic.effectiveDate = agentLic.effectiveDate != null ? moment(agentLic.effectiveDate) : null;
            agentLic.expirationDate = agentLic.expirationDate != null ? moment(agentLic.expirationDate) : null;
            agentLic.createdAt = agentLic.createdAt != null ? moment(agentLic.createdAt) : null;
            agentLic.updatedAt = agentLic.updatedAt != null ? moment(agentLic.updatedAt) : null;
        });
        return res;
    }
}
