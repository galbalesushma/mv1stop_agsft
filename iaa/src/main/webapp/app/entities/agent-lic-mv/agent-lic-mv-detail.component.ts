import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentLicMv } from 'app/shared/model/agent-lic-mv.model';

@Component({
    selector: 'jhi-agent-lic-mv-detail',
    templateUrl: './agent-lic-mv-detail.component.html'
})
export class AgentLicMvDetailComponent implements OnInit {
    agentLic: IAgentLicMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ agentLic }) => {
            this.agentLic = agentLic;
        });
    }

    previousState() {
        window.history.back();
    }
}
