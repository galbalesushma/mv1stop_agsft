import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IAgentLicMv } from 'app/shared/model/agent-lic-mv.model';
import { AgentLicMvService } from './agent-lic-mv.service';
import { IProducerMv } from 'app/shared/model/producer-mv.model';
import { ProducerMvService } from 'app/entities/producer-mv';

@Component({
    selector: 'jhi-agent-lic-mv-update',
    templateUrl: './agent-lic-mv-update.component.html'
})
export class AgentLicMvUpdateComponent implements OnInit {
    private _agentLic: IAgentLicMv;
    isSaving: boolean;

    producers: IProducerMv[];
    firstActiveDate: string;
    effectiveDate: string;
    expirationDate: string;
    createdAt: string;
    updatedAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private agentLicService: AgentLicMvService,
        private producerService: ProducerMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ agentLic }) => {
            this.agentLic = agentLic;
        });
        this.producerService.query().subscribe(
            (res: HttpResponse<IProducerMv[]>) => {
                this.producers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.agentLic.firstActiveDate = moment(this.firstActiveDate, DATE_TIME_FORMAT);
        this.agentLic.effectiveDate = moment(this.effectiveDate, DATE_TIME_FORMAT);
        this.agentLic.expirationDate = moment(this.expirationDate, DATE_TIME_FORMAT);
        this.agentLic.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        this.agentLic.updatedAt = moment(this.updatedAt, DATE_TIME_FORMAT);
        if (this.agentLic.id !== undefined) {
            this.subscribeToSaveResponse(this.agentLicService.update(this.agentLic));
        } else {
            this.subscribeToSaveResponse(this.agentLicService.create(this.agentLic));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAgentLicMv>>) {
        result.subscribe((res: HttpResponse<IAgentLicMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProducerById(index: number, item: IProducerMv) {
        return item.id;
    }
    get agentLic() {
        return this._agentLic;
    }

    set agentLic(agentLic: IAgentLicMv) {
        this._agentLic = agentLic;
        this.firstActiveDate = moment(agentLic.firstActiveDate).format(DATE_TIME_FORMAT);
        this.effectiveDate = moment(agentLic.effectiveDate).format(DATE_TIME_FORMAT);
        this.expirationDate = moment(agentLic.expirationDate).format(DATE_TIME_FORMAT);
        this.createdAt = moment(agentLic.createdAt).format(DATE_TIME_FORMAT);
        this.updatedAt = moment(agentLic.updatedAt).format(DATE_TIME_FORMAT);
    }
}
