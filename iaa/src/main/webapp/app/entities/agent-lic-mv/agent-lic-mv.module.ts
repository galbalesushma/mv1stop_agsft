import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    AgentLicMvComponent,
    AgentLicMvDetailComponent,
    AgentLicMvUpdateComponent,
    AgentLicMvDeletePopupComponent,
    AgentLicMvDeleteDialogComponent,
    agentLicRoute,
    agentLicPopupRoute
} from './';

const ENTITY_STATES = [...agentLicRoute, ...agentLicPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AgentLicMvComponent,
        AgentLicMvDetailComponent,
        AgentLicMvUpdateComponent,
        AgentLicMvDeleteDialogComponent,
        AgentLicMvDeletePopupComponent
    ],
    entryComponents: [AgentLicMvComponent, AgentLicMvUpdateComponent, AgentLicMvDeleteDialogComponent, AgentLicMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaAgentLicMvModule {}
