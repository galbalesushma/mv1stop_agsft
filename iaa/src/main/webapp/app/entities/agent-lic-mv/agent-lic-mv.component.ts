import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAgentLicMv } from 'app/shared/model/agent-lic-mv.model';
import { Principal } from 'app/core';
import { AgentLicMvService } from './agent-lic-mv.service';

@Component({
    selector: 'jhi-agent-lic-mv',
    templateUrl: './agent-lic-mv.component.html'
})
export class AgentLicMvComponent implements OnInit, OnDestroy {
    agentLics: IAgentLicMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private agentLicService: AgentLicMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.agentLicService.query().subscribe(
            (res: HttpResponse<IAgentLicMv[]>) => {
                this.agentLics = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAgentLics();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAgentLicMv) {
        return item.id;
    }

    registerChangeInAgentLics() {
        this.eventSubscriber = this.eventManager.subscribe('agentLicListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
