import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentLicMv } from 'app/shared/model/agent-lic-mv.model';
import { AgentLicMvService } from './agent-lic-mv.service';

@Component({
    selector: 'jhi-agent-lic-mv-delete-dialog',
    templateUrl: './agent-lic-mv-delete-dialog.component.html'
})
export class AgentLicMvDeleteDialogComponent {
    agentLic: IAgentLicMv;

    constructor(private agentLicService: AgentLicMvService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.agentLicService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'agentLicListModification',
                content: 'Deleted an agentLic'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-agent-lic-mv-delete-popup',
    template: ''
})
export class AgentLicMvDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ agentLic }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AgentLicMvDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.agentLic = agentLic;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
