import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AgentLicMv } from 'app/shared/model/agent-lic-mv.model';
import { AgentLicMvService } from './agent-lic-mv.service';
import { AgentLicMvComponent } from './agent-lic-mv.component';
import { AgentLicMvDetailComponent } from './agent-lic-mv-detail.component';
import { AgentLicMvUpdateComponent } from './agent-lic-mv-update.component';
import { AgentLicMvDeletePopupComponent } from './agent-lic-mv-delete-dialog.component';
import { IAgentLicMv } from 'app/shared/model/agent-lic-mv.model';

@Injectable({ providedIn: 'root' })
export class AgentLicMvResolve implements Resolve<IAgentLicMv> {
    constructor(private service: AgentLicMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((agentLic: HttpResponse<AgentLicMv>) => agentLic.body));
        }
        return of(new AgentLicMv());
    }
}

export const agentLicRoute: Routes = [
    {
        path: 'agent-lic-mv',
        component: AgentLicMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentLic.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-lic-mv/:id/view',
        component: AgentLicMvDetailComponent,
        resolve: {
            agentLic: AgentLicMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentLic.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-lic-mv/new',
        component: AgentLicMvUpdateComponent,
        resolve: {
            agentLic: AgentLicMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentLic.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-lic-mv/:id/edit',
        component: AgentLicMvUpdateComponent,
        resolve: {
            agentLic: AgentLicMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentLic.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const agentLicPopupRoute: Routes = [
    {
        path: 'agent-lic-mv/:id/delete',
        component: AgentLicMvDeletePopupComponent,
        resolve: {
            agentLic: AgentLicMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentLic.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
