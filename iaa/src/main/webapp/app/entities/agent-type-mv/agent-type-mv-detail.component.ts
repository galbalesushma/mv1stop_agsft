import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentTypeMv } from 'app/shared/model/agent-type-mv.model';

@Component({
    selector: 'jhi-agent-type-mv-detail',
    templateUrl: './agent-type-mv-detail.component.html'
})
export class AgentTypeMvDetailComponent implements OnInit {
    agentType: IAgentTypeMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ agentType }) => {
            this.agentType = agentType;
        });
    }

    previousState() {
        window.history.back();
    }
}
