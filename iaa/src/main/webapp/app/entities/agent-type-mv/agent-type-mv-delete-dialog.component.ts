import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentTypeMv } from 'app/shared/model/agent-type-mv.model';
import { AgentTypeMvService } from './agent-type-mv.service';

@Component({
    selector: 'jhi-agent-type-mv-delete-dialog',
    templateUrl: './agent-type-mv-delete-dialog.component.html'
})
export class AgentTypeMvDeleteDialogComponent {
    agentType: IAgentTypeMv;

    constructor(private agentTypeService: AgentTypeMvService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.agentTypeService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'agentTypeListModification',
                content: 'Deleted an agentType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-agent-type-mv-delete-popup',
    template: ''
})
export class AgentTypeMvDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ agentType }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AgentTypeMvDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.agentType = agentType;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
