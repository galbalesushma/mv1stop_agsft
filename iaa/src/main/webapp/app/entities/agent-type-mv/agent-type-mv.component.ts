import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAgentTypeMv } from 'app/shared/model/agent-type-mv.model';
import { Principal } from 'app/core';
import { AgentTypeMvService } from './agent-type-mv.service';

@Component({
    selector: 'jhi-agent-type-mv',
    templateUrl: './agent-type-mv.component.html'
})
export class AgentTypeMvComponent implements OnInit, OnDestroy {
    agentTypes: IAgentTypeMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private agentTypeService: AgentTypeMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.agentTypeService.query().subscribe(
            (res: HttpResponse<IAgentTypeMv[]>) => {
                this.agentTypes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAgentTypes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAgentTypeMv) {
        return item.id;
    }

    registerChangeInAgentTypes() {
        this.eventSubscriber = this.eventManager.subscribe('agentTypeListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
