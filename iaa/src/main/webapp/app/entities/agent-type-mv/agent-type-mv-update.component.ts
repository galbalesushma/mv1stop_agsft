import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IAgentTypeMv } from 'app/shared/model/agent-type-mv.model';
import { AgentTypeMvService } from './agent-type-mv.service';
import { IAgentMv } from 'app/shared/model/agent-mv.model';
import { AgentMvService } from 'app/entities/agent-mv';

@Component({
    selector: 'jhi-agent-type-mv-update',
    templateUrl: './agent-type-mv-update.component.html'
})
export class AgentTypeMvUpdateComponent implements OnInit {
    private _agentType: IAgentTypeMv;
    isSaving: boolean;

    agents: IAgentMv[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private agentTypeService: AgentTypeMvService,
        private agentService: AgentMvService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ agentType }) => {
            this.agentType = agentType;
        });
        this.agentService.query().subscribe(
            (res: HttpResponse<IAgentMv[]>) => {
                this.agents = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.agentType.id !== undefined) {
            this.subscribeToSaveResponse(this.agentTypeService.update(this.agentType));
        } else {
            this.subscribeToSaveResponse(this.agentTypeService.create(this.agentType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAgentTypeMv>>) {
        result.subscribe((res: HttpResponse<IAgentTypeMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackAgentById(index: number, item: IAgentMv) {
        return item.id;
    }
    get agentType() {
        return this._agentType;
    }

    set agentType(agentType: IAgentTypeMv) {
        this._agentType = agentType;
    }
}
