import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    AgentTypeMvComponent,
    AgentTypeMvDetailComponent,
    AgentTypeMvUpdateComponent,
    AgentTypeMvDeletePopupComponent,
    AgentTypeMvDeleteDialogComponent,
    agentTypeRoute,
    agentTypePopupRoute
} from './';

const ENTITY_STATES = [...agentTypeRoute, ...agentTypePopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AgentTypeMvComponent,
        AgentTypeMvDetailComponent,
        AgentTypeMvUpdateComponent,
        AgentTypeMvDeleteDialogComponent,
        AgentTypeMvDeletePopupComponent
    ],
    entryComponents: [AgentTypeMvComponent, AgentTypeMvUpdateComponent, AgentTypeMvDeleteDialogComponent, AgentTypeMvDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaAgentTypeMvModule {}
