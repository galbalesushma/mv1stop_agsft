import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AgentTypeMv } from 'app/shared/model/agent-type-mv.model';
import { AgentTypeMvService } from './agent-type-mv.service';
import { AgentTypeMvComponent } from './agent-type-mv.component';
import { AgentTypeMvDetailComponent } from './agent-type-mv-detail.component';
import { AgentTypeMvUpdateComponent } from './agent-type-mv-update.component';
import { AgentTypeMvDeletePopupComponent } from './agent-type-mv-delete-dialog.component';
import { IAgentTypeMv } from 'app/shared/model/agent-type-mv.model';

@Injectable({ providedIn: 'root' })
export class AgentTypeMvResolve implements Resolve<IAgentTypeMv> {
    constructor(private service: AgentTypeMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((agentType: HttpResponse<AgentTypeMv>) => agentType.body));
        }
        return of(new AgentTypeMv());
    }
}

export const agentTypeRoute: Routes = [
    {
        path: 'agent-type-mv',
        component: AgentTypeMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-type-mv/:id/view',
        component: AgentTypeMvDetailComponent,
        resolve: {
            agentType: AgentTypeMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-type-mv/new',
        component: AgentTypeMvUpdateComponent,
        resolve: {
            agentType: AgentTypeMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentType.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'agent-type-mv/:id/edit',
        component: AgentTypeMvUpdateComponent,
        resolve: {
            agentType: AgentTypeMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const agentTypePopupRoute: Routes = [
    {
        path: 'agent-type-mv/:id/delete',
        component: AgentTypeMvDeletePopupComponent,
        resolve: {
            agentType: AgentTypeMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.agentType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
