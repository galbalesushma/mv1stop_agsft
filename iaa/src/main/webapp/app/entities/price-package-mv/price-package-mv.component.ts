import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IPricePackageMv } from 'app/shared/model/price-package-mv.model';
import { Principal } from 'app/core';
import { PricePackageMvService } from './price-package-mv.service';

@Component({
    selector: 'jhi-price-package-mv',
    templateUrl: './price-package-mv.component.html'
})
export class PricePackageMvComponent implements OnInit, OnDestroy {
    pricePackages: IPricePackageMv[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private pricePackageService: PricePackageMvService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.pricePackageService.query().subscribe(
            (res: HttpResponse<IPricePackageMv[]>) => {
                this.pricePackages = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInPricePackages();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPricePackageMv) {
        return item.id;
    }

    registerChangeInPricePackages() {
        this.eventSubscriber = this.eventManager.subscribe('pricePackageListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
