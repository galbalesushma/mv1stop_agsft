import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PricePackageMv } from 'app/shared/model/price-package-mv.model';
import { PricePackageMvService } from './price-package-mv.service';
import { PricePackageMvComponent } from './price-package-mv.component';
import { PricePackageMvDetailComponent } from './price-package-mv-detail.component';
import { PricePackageMvUpdateComponent } from './price-package-mv-update.component';
import { PricePackageMvDeletePopupComponent } from './price-package-mv-delete-dialog.component';
import { IPricePackageMv } from 'app/shared/model/price-package-mv.model';

@Injectable({ providedIn: 'root' })
export class PricePackageMvResolve implements Resolve<IPricePackageMv> {
    constructor(private service: PricePackageMvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((pricePackage: HttpResponse<PricePackageMv>) => pricePackage.body));
        }
        return of(new PricePackageMv());
    }
}

export const pricePackageRoute: Routes = [
    {
        path: 'price-package-mv',
        component: PricePackageMvComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'price-package-mv/:id/view',
        component: PricePackageMvDetailComponent,
        resolve: {
            pricePackage: PricePackageMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'price-package-mv/new',
        component: PricePackageMvUpdateComponent,
        resolve: {
            pricePackage: PricePackageMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'price-package-mv/:id/edit',
        component: PricePackageMvUpdateComponent,
        resolve: {
            pricePackage: PricePackageMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pricePackagePopupRoute: Routes = [
    {
        path: 'price-package-mv/:id/delete',
        component: PricePackageMvDeletePopupComponent,
        resolve: {
            pricePackage: PricePackageMvResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.pricePackage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
