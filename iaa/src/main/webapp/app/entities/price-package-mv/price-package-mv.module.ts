import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    PricePackageMvComponent,
    PricePackageMvDetailComponent,
    PricePackageMvUpdateComponent,
    PricePackageMvDeletePopupComponent,
    PricePackageMvDeleteDialogComponent,
    pricePackageRoute,
    pricePackagePopupRoute
} from './';

const ENTITY_STATES = [...pricePackageRoute, ...pricePackagePopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PricePackageMvComponent,
        PricePackageMvDetailComponent,
        PricePackageMvUpdateComponent,
        PricePackageMvDeleteDialogComponent,
        PricePackageMvDeletePopupComponent
    ],
    entryComponents: [
        PricePackageMvComponent,
        PricePackageMvUpdateComponent,
        PricePackageMvDeleteDialogComponent,
        PricePackageMvDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaPricePackageMvModule {}
