import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPricePackageMv } from 'app/shared/model/price-package-mv.model';
import { PricePackageMvService } from './price-package-mv.service';

@Component({
    selector: 'jhi-price-package-mv-delete-dialog',
    templateUrl: './price-package-mv-delete-dialog.component.html'
})
export class PricePackageMvDeleteDialogComponent {
    pricePackage: IPricePackageMv;

    constructor(
        private pricePackageService: PricePackageMvService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pricePackageService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'pricePackageListModification',
                content: 'Deleted an pricePackage'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-price-package-mv-delete-popup',
    template: ''
})
export class PricePackageMvDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pricePackage }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PricePackageMvDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.pricePackage = pricePackage;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
