import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IPricePackageMv } from 'app/shared/model/price-package-mv.model';
import { PricePackageMvService } from './price-package-mv.service';

@Component({
    selector: 'jhi-price-package-mv-update',
    templateUrl: './price-package-mv-update.component.html'
})
export class PricePackageMvUpdateComponent implements OnInit {
    private _pricePackage: IPricePackageMv;
    isSaving: boolean;

    constructor(private pricePackageService: PricePackageMvService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ pricePackage }) => {
            this.pricePackage = pricePackage;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.pricePackage.id !== undefined) {
            this.subscribeToSaveResponse(this.pricePackageService.update(this.pricePackage));
        } else {
            this.subscribeToSaveResponse(this.pricePackageService.create(this.pricePackage));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPricePackageMv>>) {
        result.subscribe((res: HttpResponse<IPricePackageMv>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get pricePackage() {
        return this._pricePackage;
    }

    set pricePackage(pricePackage: IPricePackageMv) {
        this._pricePackage = pricePackage;
    }
}
