import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPricePackageMv } from 'app/shared/model/price-package-mv.model';

type EntityResponseType = HttpResponse<IPricePackageMv>;
type EntityArrayResponseType = HttpResponse<IPricePackageMv[]>;

@Injectable({ providedIn: 'root' })
export class PricePackageMvService {
    private resourceUrl = SERVER_API_URL + 'api/price-packages';

    constructor(private http: HttpClient) {}

    create(pricePackage: IPricePackageMv): Observable<EntityResponseType> {
        return this.http.post<IPricePackageMv>(this.resourceUrl, pricePackage, { observe: 'response' });
    }

    update(pricePackage: IPricePackageMv): Observable<EntityResponseType> {
        return this.http.put<IPricePackageMv>(this.resourceUrl, pricePackage, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IPricePackageMv>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPricePackageMv[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
