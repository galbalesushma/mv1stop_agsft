import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPricePackageMv } from 'app/shared/model/price-package-mv.model';

@Component({
    selector: 'jhi-price-package-mv-detail',
    templateUrl: './price-package-mv-detail.component.html'
})
export class PricePackageMvDetailComponent implements OnInit {
    pricePackage: IPricePackageMv;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pricePackage }) => {
            this.pricePackage = pricePackage;
        });
    }

    previousState() {
        window.history.back();
    }
}
