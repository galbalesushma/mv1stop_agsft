import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { SearchLogs } from 'app/shared/model/search-logs.model';
import { SearchLogsService } from './search-logs.service';
import { SearchLogsComponent } from './search-logs.component';
import { SearchLogsDetailComponent } from './search-logs-detail.component';
import { SearchLogsUpdateComponent } from './search-logs-update.component';
import { SearchLogsDeletePopupComponent } from './search-logs-delete-dialog.component';
import { ISearchLogs } from 'app/shared/model/search-logs.model';

@Injectable({ providedIn: 'root' })
export class SearchLogsResolve implements Resolve<ISearchLogs> {
    constructor(private service: SearchLogsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((searchLogs: HttpResponse<SearchLogs>) => searchLogs.body));
        }
        return of(new SearchLogs());
    }
}

export const searchLogsRoute: Routes = [
    {
        path: 'search-logs',
        component: SearchLogsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.searchLogs.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'search-logs/:id/view',
        component: SearchLogsDetailComponent,
        resolve: {
            searchLogs: SearchLogsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.searchLogs.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'search-logs/new',
        component: SearchLogsUpdateComponent,
        resolve: {
            searchLogs: SearchLogsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.searchLogs.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'search-logs/:id/edit',
        component: SearchLogsUpdateComponent,
        resolve: {
            searchLogs: SearchLogsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.searchLogs.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const searchLogsPopupRoute: Routes = [
    {
        path: 'search-logs/:id/delete',
        component: SearchLogsDeletePopupComponent,
        resolve: {
            searchLogs: SearchLogsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iaaApp.searchLogs.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
