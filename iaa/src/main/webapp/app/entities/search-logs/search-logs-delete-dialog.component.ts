import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISearchLogs } from 'app/shared/model/search-logs.model';
import { SearchLogsService } from './search-logs.service';

@Component({
    selector: 'jhi-search-logs-delete-dialog',
    templateUrl: './search-logs-delete-dialog.component.html'
})
export class SearchLogsDeleteDialogComponent {
    searchLogs: ISearchLogs;

    constructor(private searchLogsService: SearchLogsService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.searchLogsService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'searchLogsListModification',
                content: 'Deleted an searchLogs'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-search-logs-delete-popup',
    template: ''
})
export class SearchLogsDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ searchLogs }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SearchLogsDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.searchLogs = searchLogs;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
