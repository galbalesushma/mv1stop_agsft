import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISearchLogs } from 'app/shared/model/search-logs.model';
import { SearchLogsService } from './search-logs.service';

@Component({
    selector: 'jhi-search-logs-update',
    templateUrl: './search-logs-update.component.html'
})
export class SearchLogsUpdateComponent implements OnInit {
    private _searchLogs: ISearchLogs;
    isSaving: boolean;
    searchedDate: string;

    constructor(private searchLogsService: SearchLogsService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ searchLogs }) => {
            this.searchLogs = searchLogs;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.searchLogs.searchedDate = moment(this.searchedDate, DATE_TIME_FORMAT);
        if (this.searchLogs.id !== undefined) {
            this.subscribeToSaveResponse(this.searchLogsService.update(this.searchLogs));
        } else {
            this.subscribeToSaveResponse(this.searchLogsService.create(this.searchLogs));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ISearchLogs>>) {
        result.subscribe((res: HttpResponse<ISearchLogs>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get searchLogs() {
        return this._searchLogs;
    }

    set searchLogs(searchLogs: ISearchLogs) {
        this._searchLogs = searchLogs;
        this.searchedDate = moment(searchLogs.searchedDate).format(DATE_TIME_FORMAT);
    }
}
