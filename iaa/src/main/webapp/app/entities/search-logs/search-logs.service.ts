import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISearchLogs } from 'app/shared/model/search-logs.model';

type EntityResponseType = HttpResponse<ISearchLogs>;
type EntityArrayResponseType = HttpResponse<ISearchLogs[]>;

@Injectable({ providedIn: 'root' })
export class SearchLogsService {
    private resourceUrl = SERVER_API_URL + 'api/search-logs';

    constructor(private http: HttpClient) {}

    create(searchLogs: ISearchLogs): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(searchLogs);
        return this.http
            .post<ISearchLogs>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(searchLogs: ISearchLogs): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(searchLogs);
        return this.http
            .put<ISearchLogs>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ISearchLogs>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ISearchLogs[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(searchLogs: ISearchLogs): ISearchLogs {
        const copy: ISearchLogs = Object.assign({}, searchLogs, {
            searchedDate: searchLogs.searchedDate != null && searchLogs.searchedDate.isValid() ? searchLogs.searchedDate.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.searchedDate = res.body.searchedDate != null ? moment(res.body.searchedDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((searchLogs: ISearchLogs) => {
            searchLogs.searchedDate = searchLogs.searchedDate != null ? moment(searchLogs.searchedDate) : null;
        });
        return res;
    }
}
