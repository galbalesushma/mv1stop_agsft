import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISearchLogs } from 'app/shared/model/search-logs.model';

@Component({
    selector: 'jhi-search-logs-detail',
    templateUrl: './search-logs-detail.component.html'
})
export class SearchLogsDetailComponent implements OnInit {
    searchLogs: ISearchLogs;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ searchLogs }) => {
            this.searchLogs = searchLogs;
        });
    }

    previousState() {
        window.history.back();
    }
}
