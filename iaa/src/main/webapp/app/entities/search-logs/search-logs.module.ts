import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IaaSharedModule } from 'app/shared';
import {
    SearchLogsComponent,
    SearchLogsDetailComponent,
    SearchLogsUpdateComponent,
    SearchLogsDeletePopupComponent,
    SearchLogsDeleteDialogComponent,
    searchLogsRoute,
    searchLogsPopupRoute
} from './';

const ENTITY_STATES = [...searchLogsRoute, ...searchLogsPopupRoute];

@NgModule({
    imports: [IaaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SearchLogsComponent,
        SearchLogsDetailComponent,
        SearchLogsUpdateComponent,
        SearchLogsDeleteDialogComponent,
        SearchLogsDeletePopupComponent
    ],
    entryComponents: [SearchLogsComponent, SearchLogsUpdateComponent, SearchLogsDeleteDialogComponent, SearchLogsDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IaaSearchLogsModule {}
