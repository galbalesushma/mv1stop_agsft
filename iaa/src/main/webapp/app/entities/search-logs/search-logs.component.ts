import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISearchLogs } from 'app/shared/model/search-logs.model';
import { Principal } from 'app/core';
import { SearchLogsService } from './search-logs.service';

@Component({
    selector: 'jhi-search-logs',
    templateUrl: './search-logs.component.html'
})
export class SearchLogsComponent implements OnInit, OnDestroy {
    searchLogs: ISearchLogs[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private searchLogsService: SearchLogsService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.searchLogsService.query().subscribe(
            (res: HttpResponse<ISearchLogs[]>) => {
                this.searchLogs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSearchLogs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISearchLogs) {
        return item.id;
    }

    registerChangeInSearchLogs() {
        this.eventSubscriber = this.eventManager.subscribe('searchLogsListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
