import { Moment } from 'moment';
import { IContactMv } from 'app/shared/model//contact-mv.model';

export interface IAddressMv {
    id?: number;
    addr1?: string;
    addr2?: string;
    addr3?: string;
    zip?: string;
    region?: string;
    country?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    contacts?: IContactMv[];
    cityId?: number;
    countyId?: number;
    stateId?: number;
}

export class AddressMv implements IAddressMv {
    constructor(
        public id?: number,
        public addr1?: string,
        public addr2?: string,
        public addr3?: string,
        public zip?: string,
        public region?: string,
        public country?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public contacts?: IContactMv[],
        public cityId?: number,
        public countyId?: number,
        public stateId?: number
    ) {}
}
