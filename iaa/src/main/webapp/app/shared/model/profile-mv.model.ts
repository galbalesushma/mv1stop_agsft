import { Moment } from 'moment';
import { ICartMv } from 'app/shared/model//cart-mv.model';

export const enum ProfileType {
    AGENT = 'AGENT',
    FACILITY = 'FACILITY',
    ADMIN = 'ADMIN'
}

export interface IProfileMv {
    id?: number;
    firstName?: string;
    middleName?: string;
    lastName?: string;
    prefMobile?: string;
    prefEmail?: string;
    refBy?: string;
    suffix?: string;
    type?: ProfileType;
    isSpeaksSpanish?: boolean;
    userId?: number;
    createdAt?: Moment;
    updatedAt?: Moment;
    isActive?: boolean;
    isDeleted?: boolean;
    phone?: string;
    addrId?: number;
    carts?: ICartMv[];
}

export class ProfileMv implements IProfileMv {
    constructor(
        public id?: number,
        public firstName?: string,
        public middleName?: string,
        public lastName?: string,
        public prefMobile?: string,
        public prefEmail?: string,
        public refBy?: string,
        public suffix?: string,
        public type?: ProfileType,
        public isSpeaksSpanish?: boolean,
        public userId?: number,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public phone?: string,
        public addrId?: number,
        public carts?: ICartMv[]
    ) {
        this.isSpeaksSpanish = this.isSpeaksSpanish || false;
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
