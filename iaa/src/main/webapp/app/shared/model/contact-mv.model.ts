export interface IContactMv {
    id?: number;
    phone?: string;
    officePhone?: string;
    email?: string;
    addressId?: number;
}

export class ContactMv implements IContactMv {
    constructor(public id?: number, public phone?: string, public officePhone?: string, public email?: string, public addressId?: number) {}
}
