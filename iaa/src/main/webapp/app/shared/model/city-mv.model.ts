export interface ICityMv {
    id?: number;
    name?: string;
    cityCode?: string;
    stateCode?: string;
}

export class CityMv implements ICityMv {
    constructor(public id?: number, public name?: string, public cityCode?: string, public stateCode?: string) {}
}
