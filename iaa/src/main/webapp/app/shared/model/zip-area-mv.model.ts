export interface IZipAreaMv {
    id?: number;
    name?: string;
    zip?: string;
}

export class ZipAreaMv implements IZipAreaMv {
    constructor(public id?: number, public name?: string, public zip?: string) {}
}
