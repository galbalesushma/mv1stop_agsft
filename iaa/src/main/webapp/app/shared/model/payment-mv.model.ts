import { Moment } from 'moment';

export const enum PayStatus {
    INITIATED = 'INITIATED',
    SUCCESS = 'SUCCESS',
    FAILURE = 'FAILURE',
    AWAITED = 'AWAITED'
}

export interface IPaymentMv {
    id?: number;
    amount?: number;
    txnId?: string;
    uuId?: string;
    status?: PayStatus;
    gatewayResp?: string;
    profileId?: number;
    cartId?: number;
    createdAt?: Moment;
    updatedAt?: Moment;
    isDeleted?: boolean;
}

export class PaymentMv implements IPaymentMv {
    constructor(
        public id?: number,
        public amount?: number,
        public txnId?: string,
        public uuId?: string,
        public status?: PayStatus,
        public gatewayResp?: string,
        public profileId?: number,
        public cartId?: number,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public isDeleted?: boolean
    ) {
        this.isDeleted = this.isDeleted || false;
    }
}
