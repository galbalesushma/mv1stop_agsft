export interface IAgentDistributionMv {
    id?: number;
    slot?: number;
    type?: string;
    zipId?: number;
}

export class AgentDistributionMv implements IAgentDistributionMv {
    constructor(public id?: number, public slot?: number, public type?: string, public zipId?: number) {}
}
