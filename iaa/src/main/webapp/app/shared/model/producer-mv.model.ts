import { IAgentLicMv } from 'app/shared/model//agent-lic-mv.model';

export interface IProducerMv {
    id?: number;
    name?: string;
    ceCompliance?: boolean;
    addrId?: number;
    licenses?: IAgentLicMv[];
}

export class ProducerMv implements IProducerMv {
    constructor(
        public id?: number,
        public name?: string,
        public ceCompliance?: boolean,
        public addrId?: number,
        public licenses?: IAgentLicMv[]
    ) {
        this.ceCompliance = this.ceCompliance || false;
    }
}
