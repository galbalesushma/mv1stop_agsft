import { Moment } from 'moment';

export interface IPackSubscriptionMv {
    id?: number;
    packageId?: number;
    zipAreaId?: number;
    slots?: number;
    duration?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    validTill?: Moment;
    isActive?: boolean;
    isDeleted?: boolean;
    profileId?: number;
}

export class PackSubscriptionMv implements IPackSubscriptionMv {
    constructor(
        public id?: number,
        public packageId?: number,
        public zipAreaId?: number,
        public slots?: number,
        public duration?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public validTill?: Moment,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public profileId?: number
    ) {
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
