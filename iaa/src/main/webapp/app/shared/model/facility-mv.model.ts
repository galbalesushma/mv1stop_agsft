import { Moment } from 'moment';

export const enum InspectionType {
    EMISSION = 'EMISSION',
    SAFETY = 'SAFETY'
}

export interface IFacilityMv {
    id?: number;
    name?: string;
    isn?: string;
    type?: InspectionType;
    website?: string;
    weekdayHours?: string;
    saturdayHours?: string;
    sundayHours?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    verifiedAt?: Moment;
    isVerified?: boolean;
    isActive?: boolean;
    isDeleted?: boolean;
    profileId?: number;
    businessId?: number;
}

export class FacilityMv implements IFacilityMv {
    constructor(
        public id?: number,
        public name?: string,
        public isn?: string,
        public type?: InspectionType,
        public website?: string,
        public weekdayHours?: string,
        public saturdayHours?: string,
        public sundayHours?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public verifiedAt?: Moment,
        public isVerified?: boolean,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public profileId?: number,
        public businessId?: number
    ) {
        this.isVerified = this.isVerified || false;
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
