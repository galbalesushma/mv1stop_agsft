export const enum PageRank {
    ONE = 'ONE',
    TWO = 'TWO',
    THREE = 'THREE',
    FOUR = 'FOUR',
    FIVE = 'FIVE',
    SIX = 'SIX',
    SEVEN = 'SEVEN',
    EIGHT = 'EIGHT',
    NINE = 'NINE',
    TEN = 'TEN'
}

export interface IPricePackageMv {
    id?: number;
    name?: string;
    rank?: PageRank;
    price?: number;
    desc?: string;
}

export class PricePackageMv implements IPricePackageMv {
    constructor(public id?: number, public name?: string, public rank?: PageRank, public price?: number, public desc?: string) {}
}
