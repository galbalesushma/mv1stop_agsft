import { Moment } from 'moment';

export interface ISearchLogs {
    id?: number;
    fullName?: string;
    email?: string;
    phone?: string;
    zip?: string;
    searchType?: string;
    searchedDate?: Moment;
}

export class SearchLogs implements ISearchLogs {
    constructor(
        public id?: number,
        public fullName?: string,
        public email?: string,
        public phone?: string,
        public zip?: string,
        public searchType?: string,
        public searchedDate?: Moment
    ) {}
}
