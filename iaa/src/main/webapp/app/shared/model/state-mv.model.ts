export interface IStateMv {
    id?: number;
    name?: string;
    stateCode?: string;
}

export class StateMv implements IStateMv {
    constructor(public id?: number, public name?: string, public stateCode?: string) {}
}
