export interface IAgentTypeMv {
    id?: number;
    name?: string;
    zipAreaId?: number;
    agentId?: number;
}

export class AgentTypeMv implements IAgentTypeMv {
    constructor(public id?: number, public name?: string, public zipAreaId?: number, public agentId?: number) {}
}
