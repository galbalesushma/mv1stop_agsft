import { Moment } from 'moment';
import { IPackSubscriptionMv } from 'app/shared/model//pack-subscription-mv.model';

export interface ICartMv {
    id?: number;
    paymentId?: number;
    createdAt?: Moment;
    updatedAt?: Moment;
    isCompleted?: boolean;
    isDeleted?: boolean;
    subscriptions?: IPackSubscriptionMv[];
    profileId?: number;
}

export class CartMv implements ICartMv {
    constructor(
        public id?: number,
        public paymentId?: number,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public isCompleted?: boolean,
        public isDeleted?: boolean,
        public subscriptions?: IPackSubscriptionMv[],
        public profileId?: number
    ) {
        this.isCompleted = this.isCompleted || false;
        this.isDeleted = this.isDeleted || false;
    }
}
