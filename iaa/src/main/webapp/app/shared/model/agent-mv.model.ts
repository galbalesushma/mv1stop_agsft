import { Moment } from 'moment';
import { IAgentTypeMv } from 'app/shared/model//agent-type-mv.model';

export interface IAgentMv {
    id?: number;
    npn?: string;
    naicno?: string;
    fein?: string;
    residentState?: boolean;
    createdAt?: Moment;
    updatedAt?: Moment;
    verifiedAt?: Moment;
    isVerified?: boolean;
    isActive?: boolean;
    isDeleted?: boolean;
    profileId?: number;
    licId?: number;
    types?: IAgentTypeMv[];
    domicileStateId?: number;
    businessId?: number;
}

export class AgentMv implements IAgentMv {
    constructor(
        public id?: number,
        public npn?: string,
        public naicno?: string,
        public fein?: string,
        public residentState?: boolean,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public verifiedAt?: Moment,
        public isVerified?: boolean,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public profileId?: number,
        public licId?: number,
        public types?: IAgentTypeMv[],
        public domicileStateId?: number,
        public businessId?: number
    ) {
        this.residentState = this.residentState || false;
        this.isVerified = this.isVerified || false;
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
