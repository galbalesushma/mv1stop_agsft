export interface ICountyMv {
    id?: number;
    name?: string;
    code?: string;
    stateCode?: string;
}

export class CountyMv implements ICountyMv {
    constructor(public id?: number, public name?: string, public code?: string, public stateCode?: string) {}
}
