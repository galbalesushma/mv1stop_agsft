import { Moment } from 'moment';
import { IAgentMv } from 'app/shared/model//agent-mv.model';
import { IFacilityMv } from 'app/shared/model//facility-mv.model';

export interface IBusinessMv {
    id?: number;
    businessName?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    verifiedAt?: Moment;
    isVerified?: boolean;
    isActive?: boolean;
    isDeleted?: boolean;
    addressId?: number;
    agents?: IAgentMv[];
    facilities?: IFacilityMv[];
}

export class BusinessMv implements IBusinessMv {
    constructor(
        public id?: number,
        public businessName?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public verifiedAt?: Moment,
        public isVerified?: boolean,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public addressId?: number,
        public agents?: IAgentMv[],
        public facilities?: IFacilityMv[]
    ) {
        this.isVerified = this.isVerified || false;
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
