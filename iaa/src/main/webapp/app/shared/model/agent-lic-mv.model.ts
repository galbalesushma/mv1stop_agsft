import { Moment } from 'moment';

export interface IAgentLicMv {
    id?: number;
    licNumber?: string;
    type?: string;
    authority?: string;
    firstActiveDate?: Moment;
    effectiveDate?: Moment;
    expirationDate?: Moment;
    createdAt?: Moment;
    updatedAt?: Moment;
    isActive?: boolean;
    isDeleted?: boolean;
    producerId?: number;
}

export class AgentLicMv implements IAgentLicMv {
    constructor(
        public id?: number,
        public licNumber?: string,
        public type?: string,
        public authority?: string,
        public firstActiveDate?: Moment,
        public effectiveDate?: Moment,
        public expirationDate?: Moment,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public isActive?: boolean,
        public isDeleted?: boolean,
        public producerId?: number
    ) {
        this.isActive = this.isActive || false;
        this.isDeleted = this.isDeleted || false;
    }
}
