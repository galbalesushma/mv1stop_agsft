export interface IDMVLocation {
    id?: number;
    name?: string;
    address?: string;
    city?: string;
    state?: string;
    postalCode?: string;
    country?: string;
    mainPhone?: string;
    faxNumber?: string;
    weekdaysHrs?: string;
    saturdayHrs?: string;
    directions?: string;
}

export class DMVLocation implements IDMVLocation {
    constructor(
        public id?: number,
        public name?: string,
        public address?: string,
        public city?: string,
        public state?: string,
        public postalCode?: string,
        public country?: string,
        public mainPhone?: string,
        public faxNumber?: string,
        public weekdaysHrs?: string,
        public saturdayHrs?: string,
        public directions?: string
    ) {}
}
